/**
 */
package miniuml.tests;

import fjava.tests.FjavaTests;
import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>Miniuml</b></em>' model.
 * <!-- end-user-doc -->
 * @generated
 */
public class MiniumlAllTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new MiniumlAllTests("Miniuml Tests");
		suite.addTest(MiniumlTests.suite());
		suite.addTest(FjavaTests.suite());
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MiniumlAllTests(String name) {
		super(name);
	}

} //MiniumlAllTests
