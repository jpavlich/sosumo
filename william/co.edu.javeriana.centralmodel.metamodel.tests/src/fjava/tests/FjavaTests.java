/**
 */
package fjava.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>fjava</b></em>' package.
 * <!-- end-user-doc -->
 * @generated
 */
public class FjavaTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new FjavaTests("fjava Tests");
		suite.addTestSuite(ProgramTest.class);
		suite.addTestSuite(ClassTest.class);
		suite.addTestSuite(FieldTest.class);
		suite.addTestSuite(MethodTest.class);
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FjavaTests(String name) {
		super(name);
	}

} //FjavaTests
