/**
 */
package featherweightjava.impl;

import featherweightjava.FieldAccessChild;
import featherweightjava.FwjavaPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Field Access Child</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class FieldAccessChildImpl extends MinimalEObjectImpl.Container implements FieldAccessChild {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FieldAccessChildImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FwjavaPackage.Literals.FIELD_ACCESS_CHILD;
	}

} //FieldAccessChildImpl
