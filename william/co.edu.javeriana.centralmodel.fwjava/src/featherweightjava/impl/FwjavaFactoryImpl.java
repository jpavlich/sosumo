/**
 */
package featherweightjava.impl;

import featherweightjava.Cast;
import featherweightjava.Constructor;
import featherweightjava.ConstructorCall;
import featherweightjava.Field;
import featherweightjava.FieldAccess;
import featherweightjava.FieldInitialisiation;
import featherweightjava.FwjavaFactory;
import featherweightjava.FwjavaPackage;
import featherweightjava.Method;
import featherweightjava.MethodCall;
import featherweightjava.Parameter;
import featherweightjava.ParameterAccess;
import featherweightjava.This;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FwjavaFactoryImpl extends EFactoryImpl implements FwjavaFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static FwjavaFactory init() {
		try {
			FwjavaFactory theFwjavaFactory = (FwjavaFactory)EPackage.Registry.INSTANCE.getEFactory(FwjavaPackage.eNS_URI);
			if (theFwjavaFactory != null) {
				return theFwjavaFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new FwjavaFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FwjavaFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case FwjavaPackage.CLASS: return createClass();
			case FwjavaPackage.CONSTRUCTOR: return createConstructor();
			case FwjavaPackage.PARAMETER: return createParameter();
			case FwjavaPackage.FIELD_INITIALISIATION: return createFieldInitialisiation();
			case FwjavaPackage.METHOD: return createMethod();
			case FwjavaPackage.FIELD: return createField();
			case FwjavaPackage.CONSTRUCTOR_CALL: return createConstructorCall();
			case FwjavaPackage.FIELD_ACCESS: return createFieldAccess();
			case FwjavaPackage.PARAMETER_ACCESS: return createParameterAccess();
			case FwjavaPackage.THIS: return createThis();
			case FwjavaPackage.METHOD_CALL: return createMethodCall();
			case FwjavaPackage.CAST: return createCast();
			case FwjavaPackage.PACKAGE: return createPackage();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public featherweightjava.Class createClass() {
		ClassImpl class_ = new ClassImpl();
		return class_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Constructor createConstructor() {
		ConstructorImpl constructor = new ConstructorImpl();
		return constructor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Parameter createParameter() {
		ParameterImpl parameter = new ParameterImpl();
		return parameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FieldInitialisiation createFieldInitialisiation() {
		FieldInitialisiationImpl fieldInitialisiation = new FieldInitialisiationImpl();
		return fieldInitialisiation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Method createMethod() {
		MethodImpl method = new MethodImpl();
		return method;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Field createField() {
		FieldImpl field = new FieldImpl();
		return field;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstructorCall createConstructorCall() {
		ConstructorCallImpl constructorCall = new ConstructorCallImpl();
		return constructorCall;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FieldAccess createFieldAccess() {
		FieldAccessImpl fieldAccess = new FieldAccessImpl();
		return fieldAccess;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterAccess createParameterAccess() {
		ParameterAccessImpl parameterAccess = new ParameterAccessImpl();
		return parameterAccess;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public This createThis() {
		ThisImpl this_ = new ThisImpl();
		return this_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MethodCall createMethodCall() {
		MethodCallImpl methodCall = new MethodCallImpl();
		return methodCall;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Cast createCast() {
		CastImpl cast = new CastImpl();
		return cast;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public featherweightjava.Package createPackage() {
		PackageImpl package_ = new PackageImpl();
		return package_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FwjavaPackage getFwjavaPackage() {
		return (FwjavaPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static FwjavaPackage getPackage() {
		return FwjavaPackage.eINSTANCE;
	}

} //FwjavaFactoryImpl
