/**
 */
package featherweightjava;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Field Access Child</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see featherweightjava.FwjavaPackage#getFieldAccessChild()
 * @model abstract="true"
 * @generated
 */
public interface FieldAccessChild extends EObject {
} // FieldAccessChild
