/**
 */
package featherweightjava;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link featherweightjava.Parameter#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see featherweightjava.FwjavaPackage#getParameter()
 * @model
 * @generated
 */
public interface Parameter extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(featherweightjava.Class)
	 * @see featherweightjava.FwjavaPackage#getParameter_Type()
	 * @model required="true"
	 * @generated
	 */
	featherweightjava.Class getType();

	/**
	 * Sets the value of the '{@link featherweightjava.Parameter#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(featherweightjava.Class value);

} // Parameter
