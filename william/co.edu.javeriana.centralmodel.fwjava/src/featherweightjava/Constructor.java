/**
 */
package featherweightjava;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constructor</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link featherweightjava.Constructor#getParameters <em>Parameters</em>}</li>
 *   <li>{@link featherweightjava.Constructor#getInitialisations <em>Initialisations</em>}</li>
 * </ul>
 *
 * @see featherweightjava.FwjavaPackage#getConstructor()
 * @model
 * @generated
 */
public interface Constructor extends Member {
	/**
	 * Returns the value of the '<em><b>Parameters</b></em>' containment reference list.
	 * The list contents are of type {@link featherweightjava.Parameter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameters</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameters</em>' containment reference list.
	 * @see featherweightjava.FwjavaPackage#getConstructor_Parameters()
	 * @model containment="true"
	 * @generated
	 */
	EList<Parameter> getParameters();

	/**
	 * Returns the value of the '<em><b>Initialisations</b></em>' containment reference list.
	 * The list contents are of type {@link featherweightjava.FieldInitialisiation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initialisations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initialisations</em>' containment reference list.
	 * @see featherweightjava.FwjavaPackage#getConstructor_Initialisations()
	 * @model containment="true"
	 * @generated
	 */
	EList<FieldInitialisiation> getInitialisations();

} // Constructor
