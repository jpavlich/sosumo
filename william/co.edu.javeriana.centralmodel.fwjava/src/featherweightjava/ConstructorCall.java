/**
 */
package featherweightjava;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constructor Call</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link featherweightjava.ConstructorCall#getType <em>Type</em>}</li>
 *   <li>{@link featherweightjava.ConstructorCall#getArguments <em>Arguments</em>}</li>
 * </ul>
 *
 * @see featherweightjava.FwjavaPackage#getConstructorCall()
 * @model
 * @generated
 */
public interface ConstructorCall extends Expression {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(featherweightjava.Class)
	 * @see featherweightjava.FwjavaPackage#getConstructorCall_Type()
	 * @model required="true"
	 * @generated
	 */
	featherweightjava.Class getType();

	/**
	 * Sets the value of the '{@link featherweightjava.ConstructorCall#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(featherweightjava.Class value);

	/**
	 * Returns the value of the '<em><b>Arguments</b></em>' containment reference list.
	 * The list contents are of type {@link featherweightjava.Expression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Arguments</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Arguments</em>' containment reference list.
	 * @see featherweightjava.FwjavaPackage#getConstructorCall_Arguments()
	 * @model containment="true"
	 * @generated
	 */
	EList<Expression> getArguments();

} // ConstructorCall
