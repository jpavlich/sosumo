/**
 */
package featherweightjava;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Member</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see featherweightjava.FwjavaPackage#getMember()
 * @model abstract="true"
 * @generated
 */
public interface Member extends NamedElement {
} // Member
