/**
 */
package featherweightjava;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>This</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see featherweightjava.FwjavaPackage#getThis()
 * @model
 * @generated
 */
public interface This extends Expression, FieldAccessChild {
} // This
