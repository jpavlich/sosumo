/**
 */
package featherweightjava;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>JClass</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link featherweightjava.JClass#getMembers <em>Members</em>}</li>
 *   <li>{@link featherweightjava.JClass#getSuperclass <em>Superclass</em>}</li>
 * </ul>
 *
 * @see featherweightjava.FwjavaPackage#getJClass()
 * @model
 * @generated
 */
public interface JClass extends JNamedElement {
	/**
	 * Returns the value of the '<em><b>Members</b></em>' containment reference list.
	 * The list contents are of type {@link featherweightjava.Member}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Members</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Members</em>' containment reference list.
	 * @see featherweightjava.FwjavaPackage#getJClass_Members()
	 * @model containment="true"
	 * @generated
	 */
	EList<Member> getMembers();

	/**
	 * Returns the value of the '<em><b>Superclass</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Superclass</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Superclass</em>' reference.
	 * @see #setSuperclass(JClass)
	 * @see featherweightjava.FwjavaPackage#getJClass_Superclass()
	 * @model required="true"
	 * @generated
	 */
	JClass getSuperclass();

	/**
	 * Sets the value of the '{@link featherweightjava.JClass#getSuperclass <em>Superclass</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Superclass</em>' reference.
	 * @see #getSuperclass()
	 * @generated
	 */
	void setSuperclass(JClass value);

} // JClass
