/**
 */
package miniuml.impl;

import java.util.Collection;

import miniuml.Attribute;
import miniuml.MiniumlPackage;
import miniuml.Operation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Class</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link miniuml.impl.ClassImpl#getAttributes <em>Attributes</em>}</li>
 *   <li>{@link miniuml.impl.ClassImpl#getParents <em>Parents</em>}</li>
 *   <li>{@link miniuml.impl.ClassImpl#getOperations <em>Operations</em>}</li>
 *   <li>{@link miniuml.impl.ClassImpl#isIsAbstrsct <em>Is Abstrsct</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ClassImpl extends TypeImpl implements miniuml.Class {
	/**
	 * The cached value of the '{@link #getAttributes() <em>Attributes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttributes()
	 * @generated
	 * @ordered
	 */
	protected EList<Attribute> attributes;

	/**
	 * The cached value of the '{@link #getParents() <em>Parents</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParents()
	 * @generated
	 * @ordered
	 */
	protected miniuml.Class parents;

	/**
	 * The cached value of the '{@link #getOperations() <em>Operations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperations()
	 * @generated
	 * @ordered
	 */
	protected EList<Operation> operations;

	/**
	 * The default value of the '{@link #isIsAbstrsct() <em>Is Abstrsct</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsAbstrsct()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_ABSTRSCT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsAbstrsct() <em>Is Abstrsct</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsAbstrsct()
	 * @generated
	 * @ordered
	 */
	protected boolean isAbstrsct = IS_ABSTRSCT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClassImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MiniumlPackage.Literals.CLASS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Attribute> getAttributes() {
		if (attributes == null) {
			attributes = new EObjectContainmentEList<Attribute>(Attribute.class, this, MiniumlPackage.CLASS__ATTRIBUTES);
		}
		return attributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public miniuml.Class getParents() {
		if (parents != null && parents.eIsProxy()) {
			InternalEObject oldParents = (InternalEObject)parents;
			parents = (miniuml.Class)eResolveProxy(oldParents);
			if (parents != oldParents) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MiniumlPackage.CLASS__PARENTS, oldParents, parents));
			}
		}
		return parents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public miniuml.Class basicGetParents() {
		return parents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParents(miniuml.Class newParents) {
		miniuml.Class oldParents = parents;
		parents = newParents;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiniumlPackage.CLASS__PARENTS, oldParents, parents));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Operation> getOperations() {
		if (operations == null) {
			operations = new EObjectContainmentEList<Operation>(Operation.class, this, MiniumlPackage.CLASS__OPERATIONS);
		}
		return operations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsAbstrsct() {
		return isAbstrsct;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsAbstrsct(boolean newIsAbstrsct) {
		boolean oldIsAbstrsct = isAbstrsct;
		isAbstrsct = newIsAbstrsct;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiniumlPackage.CLASS__IS_ABSTRSCT, oldIsAbstrsct, isAbstrsct));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MiniumlPackage.CLASS__ATTRIBUTES:
				return ((InternalEList<?>)getAttributes()).basicRemove(otherEnd, msgs);
			case MiniumlPackage.CLASS__OPERATIONS:
				return ((InternalEList<?>)getOperations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MiniumlPackage.CLASS__ATTRIBUTES:
				return getAttributes();
			case MiniumlPackage.CLASS__PARENTS:
				if (resolve) return getParents();
				return basicGetParents();
			case MiniumlPackage.CLASS__OPERATIONS:
				return getOperations();
			case MiniumlPackage.CLASS__IS_ABSTRSCT:
				return isIsAbstrsct();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MiniumlPackage.CLASS__ATTRIBUTES:
				getAttributes().clear();
				getAttributes().addAll((Collection<? extends Attribute>)newValue);
				return;
			case MiniumlPackage.CLASS__PARENTS:
				setParents((miniuml.Class)newValue);
				return;
			case MiniumlPackage.CLASS__OPERATIONS:
				getOperations().clear();
				getOperations().addAll((Collection<? extends Operation>)newValue);
				return;
			case MiniumlPackage.CLASS__IS_ABSTRSCT:
				setIsAbstrsct((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MiniumlPackage.CLASS__ATTRIBUTES:
				getAttributes().clear();
				return;
			case MiniumlPackage.CLASS__PARENTS:
				setParents((miniuml.Class)null);
				return;
			case MiniumlPackage.CLASS__OPERATIONS:
				getOperations().clear();
				return;
			case MiniumlPackage.CLASS__IS_ABSTRSCT:
				setIsAbstrsct(IS_ABSTRSCT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MiniumlPackage.CLASS__ATTRIBUTES:
				return attributes != null && !attributes.isEmpty();
			case MiniumlPackage.CLASS__PARENTS:
				return parents != null;
			case MiniumlPackage.CLASS__OPERATIONS:
				return operations != null && !operations.isEmpty();
			case MiniumlPackage.CLASS__IS_ABSTRSCT:
				return isAbstrsct != IS_ABSTRSCT_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (isAbstrsct: ");
		result.append(isAbstrsct);
		result.append(')');
		return result.toString();
	}

} //ClassImpl
