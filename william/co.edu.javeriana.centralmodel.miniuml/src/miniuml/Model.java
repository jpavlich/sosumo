/**
 */
package miniuml;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link miniuml.Model#getClasses <em>Classes</em>}</li>
 *   <li>{@link miniuml.Model#getPackages <em>Packages</em>}</li>
 *   <li>{@link miniuml.Model#getAssociations <em>Associations</em>}</li>
 * </ul>
 *
 * @see miniuml.MiniumlPackage#getModel()
 * @model
 * @generated
 */
public interface Model extends miniuml.Package {
	/**
	 * Returns the value of the '<em><b>Classes</b></em>' containment reference list.
	 * The list contents are of type {@link miniuml.Class}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Classes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classes</em>' containment reference list.
	 * @see miniuml.MiniumlPackage#getModel_Classes()
	 * @model containment="true"
	 * @generated
	 */
	EList<miniuml.Class> getClasses();

	/**
	 * Returns the value of the '<em><b>Packages</b></em>' containment reference list.
	 * The list contents are of type {@link miniuml.Package}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Packages</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Packages</em>' containment reference list.
	 * @see miniuml.MiniumlPackage#getModel_Packages()
	 * @model containment="true"
	 * @generated
	 */
	EList<miniuml.Package> getPackages();

	/**
	 * Returns the value of the '<em><b>Associations</b></em>' containment reference list.
	 * The list contents are of type {@link miniuml.Association}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Associations</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Associations</em>' containment reference list.
	 * @see miniuml.MiniumlPackage#getModel_Associations()
	 * @model containment="true"
	 * @generated
	 */
	EList<Association> getAssociations();

} // Model
