/**
 */
package miniuml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Primitive Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see miniuml.MiniumlPackage#getPrimitiveType()
 * @model
 * @generated
 */
public interface PrimitiveType extends Type {
} // PrimitiveType
