/**
 */
package miniuml;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UClass</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link miniuml.UClass#getAttributes <em>Attributes</em>}</li>
 *   <li>{@link miniuml.UClass#getParents <em>Parents</em>}</li>
 *   <li>{@link miniuml.UClass#getOperations <em>Operations</em>}</li>
 *   <li>{@link miniuml.UClass#isIsAbstrsct <em>Is Abstrsct</em>}</li>
 * </ul>
 *
 * @see miniuml.MiniumlPackage#getUClass()
 * @model
 * @generated
 */
public interface UClass extends Type {
	/**
	 * Returns the value of the '<em><b>Attributes</b></em>' containment reference list.
	 * The list contents are of type {@link miniuml.Attribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attributes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attributes</em>' containment reference list.
	 * @see miniuml.MiniumlPackage#getUClass_Attributes()
	 * @model containment="true"
	 * @generated
	 */
	EList<Attribute> getAttributes();

	/**
	 * Returns the value of the '<em><b>Parents</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parents</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parents</em>' reference.
	 * @see #setParents(UClass)
	 * @see miniuml.MiniumlPackage#getUClass_Parents()
	 * @model
	 * @generated
	 */
	UClass getParents();

	/**
	 * Sets the value of the '{@link miniuml.UClass#getParents <em>Parents</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parents</em>' reference.
	 * @see #getParents()
	 * @generated
	 */
	void setParents(UClass value);

	/**
	 * Returns the value of the '<em><b>Operations</b></em>' containment reference list.
	 * The list contents are of type {@link miniuml.Operation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations</em>' containment reference list.
	 * @see miniuml.MiniumlPackage#getUClass_Operations()
	 * @model containment="true"
	 * @generated
	 */
	EList<Operation> getOperations();

	/**
	 * Returns the value of the '<em><b>Is Abstrsct</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Abstrsct</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Abstrsct</em>' attribute.
	 * @see #setIsAbstrsct(boolean)
	 * @see miniuml.MiniumlPackage#getUClass_IsAbstrsct()
	 * @model
	 * @generated
	 */
	boolean isIsAbstrsct();

	/**
	 * Sets the value of the '{@link miniuml.UClass#isIsAbstrsct <em>Is Abstrsct</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Abstrsct</em>' attribute.
	 * @see #isIsAbstrsct()
	 * @generated
	 */
	void setIsAbstrsct(boolean value);

} // UClass
