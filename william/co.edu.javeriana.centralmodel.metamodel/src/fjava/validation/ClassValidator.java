/**
 *
 * $Id$
 */
package fjava.validation;

import fjava.Field;
import fjava.Method;

import org.eclipse.emf.common.util.EList;

/**
 * A sample validator interface for {@link fjava.Class}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface ClassValidator {
	boolean validate();

	boolean validateName(String value);
	boolean validateExtends(fjava.Class value);
	boolean validateFields(EList<Field> value);
	boolean validateMethods(EList<Method> value);
}
