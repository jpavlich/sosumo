/**
 */
package fjava;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EModelElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Program</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fjava.Program#getClasses <em>Classes</em>}</li>
 *   <li>{@link fjava.Program#getMain <em>Main</em>}</li>
 * </ul>
 *
 * @see fjava.FjavaPackage#getProgram()
 * @model
 * @generated
 */
public interface Program extends EModelElement {
	/**
	 * Returns the value of the '<em><b>Classes</b></em>' containment reference list.
	 * The list contents are of type {@link fjava.Class}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Classes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classes</em>' containment reference list.
	 * @see fjava.FjavaPackage#getProgram_Classes()
	 * @model containment="true"
	 * @generated
	 */
	EList<fjava.Class> getClasses();

	/**
	 * Returns the value of the '<em><b>Main</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Main</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Main</em>' containment reference.
	 * @see #setMain(Expression)
	 * @see fjava.FjavaPackage#getProgram_Main()
	 * @model containment="true"
	 * @generated
	 */
	Expression getMain();

	/**
	 * Sets the value of the '{@link fjava.Program#getMain <em>Main</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Main</em>' containment reference.
	 * @see #getMain()
	 * @generated
	 */
	void setMain(Expression value);

} // Program
