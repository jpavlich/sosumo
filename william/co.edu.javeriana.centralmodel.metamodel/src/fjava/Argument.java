/**
 */
package fjava;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Argument</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fjava.FjavaPackage#getArgument()
 * @model
 * @generated
 */
public interface Argument extends EObject {
} // Argument
