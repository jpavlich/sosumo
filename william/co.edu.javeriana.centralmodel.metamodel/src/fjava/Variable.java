/**
 */
package fjava;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fjava.Variable#getParamref <em>Paramref</em>}</li>
 * </ul>
 *
 * @see fjava.FjavaPackage#getVariable()
 * @model
 * @generated
 */
public interface Variable extends Expression {
	/**
	 * Returns the value of the '<em><b>Paramref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Paramref</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Paramref</em>' reference.
	 * @see #setParamref(Parameter)
	 * @see fjava.FjavaPackage#getVariable_Paramref()
	 * @model
	 * @generated
	 */
	Parameter getParamref();

	/**
	 * Sets the value of the '{@link fjava.Variable#getParamref <em>Paramref</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Paramref</em>' reference.
	 * @see #getParamref()
	 * @generated
	 */
	void setParamref(Parameter value);

} // Variable
