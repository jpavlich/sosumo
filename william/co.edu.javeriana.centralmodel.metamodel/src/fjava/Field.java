/**
 */
package fjava;

import org.eclipse.emf.ecore.EModelElement;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Field</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fjava.FjavaPackage#getField()
 * @model
 * @generated
 */
public interface Field extends TypedElement, EModelElement {
} // Field
