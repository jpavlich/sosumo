/**
 */
package miniuml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UType</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see miniuml.MiniumlPackage#getUType()
 * @model abstract="true"
 * @generated
 */
public interface UType extends UNamedElement {
} // UType
