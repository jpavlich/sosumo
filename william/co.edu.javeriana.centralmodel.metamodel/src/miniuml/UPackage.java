/**
 */
package miniuml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UPackage</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see miniuml.MiniumlPackage#getUPackage()
 * @model
 * @generated
 */
public interface UPackage extends UNamedElement {
} // UPackage
