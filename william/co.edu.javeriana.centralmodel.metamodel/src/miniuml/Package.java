/**
 */
package miniuml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Package</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see miniuml.MiniumlPackage#getPackage()
 * @model
 * @generated
 */
public interface Package extends NamedElement {
} // Package
