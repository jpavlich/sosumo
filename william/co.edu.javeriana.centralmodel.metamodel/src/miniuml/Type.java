/**
 */
package miniuml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see miniuml.MiniumlPackage#getType()
 * @model abstract="true"
 * @generated
 */
public interface Type extends NamedElement {
} // Type
