/**
 */
package miniuml.impl;

import fjava.FjavaPackage;

import fjava.impl.FjavaPackageImpl;

import miniuml.Association;
import miniuml.Attribute;
import miniuml.MiniumlFactory;
import miniuml.MiniumlPackage;
import miniuml.Model;
import miniuml.NamedElement;
import miniuml.Operation;
import miniuml.Parameter;
import miniuml.PrimitiveType;
import miniuml.Type;
import miniuml.VisibilityType;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MiniumlPackageImpl extends EPackageImpl implements MiniumlPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass classEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass namedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass typeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parameterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass primitiveTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass packageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass associationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum visibilityTypeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see miniuml.MiniumlPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private MiniumlPackageImpl() {
		super(eNS_URI, MiniumlFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link MiniumlPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static MiniumlPackage init() {
		if (isInited) return (MiniumlPackage)EPackage.Registry.INSTANCE.getEPackage(MiniumlPackage.eNS_URI);

		// Obtain or create and register package
		MiniumlPackageImpl theMiniumlPackage = (MiniumlPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof MiniumlPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new MiniumlPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		FjavaPackageImpl theFjavaPackage = (FjavaPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(FjavaPackage.eNS_URI) instanceof FjavaPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(FjavaPackage.eNS_URI) : FjavaPackage.eINSTANCE);

		// Create package meta-data objects
		theMiniumlPackage.createPackageContents();
		theFjavaPackage.createPackageContents();

		// Initialize created meta-data
		theMiniumlPackage.initializePackageContents();
		theFjavaPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theMiniumlPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(MiniumlPackage.eNS_URI, theMiniumlPackage);
		return theMiniumlPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClass_() {
		return classEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClass_Attributes() {
		return (EReference)classEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClass_Parents() {
		return (EReference)classEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClass_Operations() {
		return (EReference)classEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClass_IsAbstrsct() {
		return (EAttribute)classEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNamedElement() {
		return namedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedElement_Name() {
		return (EAttribute)namedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttribute() {
		return attributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAttribute_IsStatic() {
		return (EAttribute)attributeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttribute_Type() {
		return (EReference)attributeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAttribute_Visibility() {
		return (EAttribute)attributeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getModel() {
		return modelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModel_Classes() {
		return (EReference)modelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModel_Packages() {
		return (EReference)modelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModel_Associations() {
		return (EReference)modelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperation() {
		return operationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperation_IsStatic() {
		return (EAttribute)operationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperation_ReturnType() {
		return (EReference)operationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperation_Parameters() {
		return (EReference)operationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperation_Visibility() {
		return (EAttribute)operationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getType() {
		return typeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getParameter() {
		return parameterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getParameter_Type() {
		return (EReference)parameterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPrimitiveType() {
		return primitiveTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPackage() {
		return packageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssociation() {
		return associationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssociation_Label() {
		return (EAttribute)associationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssociation_Begin() {
		return (EReference)associationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssociation_End() {
		return (EReference)associationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getVisibilityType() {
		return visibilityTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MiniumlFactory getMiniumlFactory() {
		return (MiniumlFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		classEClass = createEClass(CLASS);
		createEReference(classEClass, CLASS__ATTRIBUTES);
		createEReference(classEClass, CLASS__PARENTS);
		createEReference(classEClass, CLASS__OPERATIONS);
		createEAttribute(classEClass, CLASS__IS_ABSTRSCT);

		namedElementEClass = createEClass(NAMED_ELEMENT);
		createEAttribute(namedElementEClass, NAMED_ELEMENT__NAME);

		attributeEClass = createEClass(ATTRIBUTE);
		createEAttribute(attributeEClass, ATTRIBUTE__IS_STATIC);
		createEReference(attributeEClass, ATTRIBUTE__TYPE);
		createEAttribute(attributeEClass, ATTRIBUTE__VISIBILITY);

		modelEClass = createEClass(MODEL);
		createEReference(modelEClass, MODEL__CLASSES);
		createEReference(modelEClass, MODEL__PACKAGES);
		createEReference(modelEClass, MODEL__ASSOCIATIONS);

		operationEClass = createEClass(OPERATION);
		createEAttribute(operationEClass, OPERATION__IS_STATIC);
		createEReference(operationEClass, OPERATION__RETURN_TYPE);
		createEReference(operationEClass, OPERATION__PARAMETERS);
		createEAttribute(operationEClass, OPERATION__VISIBILITY);

		typeEClass = createEClass(TYPE);

		parameterEClass = createEClass(PARAMETER);
		createEReference(parameterEClass, PARAMETER__TYPE);

		primitiveTypeEClass = createEClass(PRIMITIVE_TYPE);

		packageEClass = createEClass(PACKAGE);

		associationEClass = createEClass(ASSOCIATION);
		createEAttribute(associationEClass, ASSOCIATION__LABEL);
		createEReference(associationEClass, ASSOCIATION__BEGIN);
		createEReference(associationEClass, ASSOCIATION__END);

		// Create enums
		visibilityTypeEEnum = createEEnum(VISIBILITY_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		classEClass.getESuperTypes().add(this.getType());
		classEClass.getESuperTypes().add(theEcorePackage.getEModelElement());
		attributeEClass.getESuperTypes().add(this.getNamedElement());
		attributeEClass.getESuperTypes().add(theEcorePackage.getEModelElement());
		modelEClass.getESuperTypes().add(this.getPackage());
		modelEClass.getESuperTypes().add(theEcorePackage.getEModelElement());
		operationEClass.getESuperTypes().add(this.getNamedElement());
		operationEClass.getESuperTypes().add(theEcorePackage.getEModelElement());
		typeEClass.getESuperTypes().add(this.getNamedElement());
		parameterEClass.getESuperTypes().add(this.getNamedElement());
		primitiveTypeEClass.getESuperTypes().add(this.getType());
		packageEClass.getESuperTypes().add(this.getNamedElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(classEClass, miniuml.Class.class, "Class", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getClass_Attributes(), this.getAttribute(), null, "attributes", null, 0, -1, miniuml.Class.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClass_Parents(), this.getClass_(), null, "parents", null, 0, 1, miniuml.Class.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClass_Operations(), this.getOperation(), null, "operations", null, 0, -1, miniuml.Class.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getClass_IsAbstrsct(), ecorePackage.getEBoolean(), "isAbstrsct", null, 0, 1, miniuml.Class.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(namedElementEClass, NamedElement.class, "NamedElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNamedElement_Name(), ecorePackage.getEString(), "name", null, 0, 1, NamedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(attributeEClass, Attribute.class, "Attribute", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAttribute_IsStatic(), ecorePackage.getEBoolean(), "isStatic", null, 0, 1, Attribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAttribute_Type(), this.getType(), null, "type", null, 1, 1, Attribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAttribute_Visibility(), this.getVisibilityType(), "visibility", null, 0, 1, Attribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(modelEClass, Model.class, "Model", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getModel_Classes(), this.getClass_(), null, "classes", null, 0, -1, Model.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModel_Packages(), this.getPackage(), null, "packages", null, 0, -1, Model.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModel_Associations(), this.getAssociation(), null, "associations", null, 0, -1, Model.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(operationEClass, Operation.class, "Operation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOperation_IsStatic(), ecorePackage.getEBoolean(), "isStatic", null, 0, 1, Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperation_ReturnType(), this.getType(), null, "returnType", null, 0, 1, Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperation_Parameters(), this.getParameter(), null, "parameters", null, 0, -1, Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperation_Visibility(), this.getVisibilityType(), "visibility", null, 0, 1, Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(typeEClass, Type.class, "Type", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(parameterEClass, Parameter.class, "Parameter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getParameter_Type(), this.getType(), null, "type", null, 1, 1, Parameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(primitiveTypeEClass, PrimitiveType.class, "PrimitiveType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(packageEClass, miniuml.Package.class, "Package", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(associationEClass, Association.class, "Association", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAssociation_Label(), ecorePackage.getEString(), "label", null, 0, 1, Association.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssociation_Begin(), this.getClass_(), null, "begin", null, 0, 1, Association.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssociation_End(), this.getClass_(), null, "end", null, 0, 1, Association.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(visibilityTypeEEnum, VisibilityType.class, "VisibilityType");
		addEEnumLiteral(visibilityTypeEEnum, VisibilityType.PUBLIC);
		addEEnumLiteral(visibilityTypeEEnum, VisibilityType.PRIVATE);
		addEEnumLiteral(visibilityTypeEEnum, VisibilityType.PROTECTED);
		addEEnumLiteral(visibilityTypeEEnum, VisibilityType.PACKAGE);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// Class2Class
		createClass2ClassAnnotations();
		// Class_attributes2Class_fields
		createClass_attributes2Class_fieldsAnnotations();
		// PrimitiveType_name2Class_name
		createPrimitiveType_name2Class_nameAnnotations();
		// Attribute_name2Field_name
		createAttribute_name2Field_nameAnnotations();
		// Operation_name2Method_name
		createOperation_name2Method_nameAnnotations();
		// Attribute2Field
		createAttribute2FieldAnnotations();
		// Attribute_type2Field_type
		createAttribute_type2Field_typeAnnotations();
		// Model2Program
		createModel2ProgramAnnotations();
		// Model_classes2Program_classes
		createModel_classes2Program_classesAnnotations();
		// Operation2Method
		createOperation2MethodAnnotations();
		// Operation_returnType2Method_returntype
		createOperation_returnType2Method_returntypeAnnotations();
		// Operation_parameters2Method_params
		createOperation_parameters2Method_paramsAnnotations();
		// PrimitiveType2Class
		createPrimitiveType2ClassAnnotations();
	}

	/**
	 * Initializes the annotations for <b>Class2Class</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createClass2ClassAnnotations() {
		String source = "Class2Class";	
		addAnnotation
		  (classEClass, 
		   source, 
		   new String[] {
		   },
		   new URI[] {
			 URI.createURI(FjavaPackage.eNS_URI).appendFragment("//Class")
		   });
	}

	/**
	 * Initializes the annotations for <b>Class_attributes2Class_fields</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createClass_attributes2Class_fieldsAnnotations() {
		String source = "Class_attributes2Class_fields";	
		addAnnotation
		  (getClass_Attributes(), 
		   source, 
		   new String[] {
		   },
		   new URI[] {
			 URI.createURI(FjavaPackage.eNS_URI).appendFragment("//Class"),
			 URI.createURI(FjavaPackage.eNS_URI).appendFragment("//Class/fields")
		   });
	}

	/**
	 * Initializes the annotations for <b>PrimitiveType_name2Class_name</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createPrimitiveType_name2Class_nameAnnotations() {
		String source = "PrimitiveType_name2Class_name";	
		addAnnotation
		  (getNamedElement_Name(), 
		   source, 
		   new String[] {
		   },
		   new URI[] {
			 URI.createURI(FjavaPackage.eNS_URI).appendFragment("//Class"),
			 URI.createURI(FjavaPackage.eNS_URI).appendFragment("//Class/name")
		   });
	}

	/**
	 * Initializes the annotations for <b>Attribute_name2Field_name</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createAttribute_name2Field_nameAnnotations() {
		String source = "Attribute_name2Field_name";	
		addAnnotation
		  (getNamedElement_Name(), 
		   source, 
		   new String[] {
		   },
		   new URI[] {
			 URI.createURI(FjavaPackage.eNS_URI).appendFragment("//Field"),
			 URI.createURI(FjavaPackage.eNS_URI).appendFragment("//TypedElement/name")
		   });
	}

	/**
	 * Initializes the annotations for <b>Operation_name2Method_name</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOperation_name2Method_nameAnnotations() {
		String source = "Operation_name2Method_name";	
		addAnnotation
		  (getNamedElement_Name(), 
		   source, 
		   new String[] {
		   },
		   new URI[] {
			 URI.createURI(FjavaPackage.eNS_URI).appendFragment("//Method"),
			 URI.createURI(FjavaPackage.eNS_URI).appendFragment("//Method/name")
		   });
	}

	/**
	 * Initializes the annotations for <b>Attribute2Field</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createAttribute2FieldAnnotations() {
		String source = "Attribute2Field";	
		addAnnotation
		  (attributeEClass, 
		   source, 
		   new String[] {
		   },
		   new URI[] {
			 URI.createURI(FjavaPackage.eNS_URI).appendFragment("//Field")
		   });
	}

	/**
	 * Initializes the annotations for <b>Attribute_type2Field_type</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createAttribute_type2Field_typeAnnotations() {
		String source = "Attribute_type2Field_type";	
		addAnnotation
		  (getAttribute_Type(), 
		   source, 
		   new String[] {
		   },
		   new URI[] {
			 URI.createURI(FjavaPackage.eNS_URI).appendFragment("//Field"),
			 URI.createURI(FjavaPackage.eNS_URI).appendFragment("//TypedElement/type")
		   });
	}

	/**
	 * Initializes the annotations for <b>Model2Program</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createModel2ProgramAnnotations() {
		String source = "Model2Program";	
		addAnnotation
		  (modelEClass, 
		   source, 
		   new String[] {
		   },
		   new URI[] {
			 URI.createURI(FjavaPackage.eNS_URI).appendFragment("//Program")
		   });
	}

	/**
	 * Initializes the annotations for <b>Model_classes2Program_classes</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createModel_classes2Program_classesAnnotations() {
		String source = "Model_classes2Program_classes";	
		addAnnotation
		  (getModel_Classes(), 
		   source, 
		   new String[] {
		   },
		   new URI[] {
			 URI.createURI(FjavaPackage.eNS_URI).appendFragment("//Program"),
			 URI.createURI(FjavaPackage.eNS_URI).appendFragment("//Program/classes")
		   });
	}

	/**
	 * Initializes the annotations for <b>Operation2Method</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOperation2MethodAnnotations() {
		String source = "Operation2Method";	
		addAnnotation
		  (operationEClass, 
		   source, 
		   new String[] {
		   },
		   new URI[] {
			 URI.createURI(FjavaPackage.eNS_URI).appendFragment("//Method")
		   });
	}

	/**
	 * Initializes the annotations for <b>Operation_returnType2Method_returntype</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOperation_returnType2Method_returntypeAnnotations() {
		String source = "Operation_returnType2Method_returntype";	
		addAnnotation
		  (getOperation_ReturnType(), 
		   source, 
		   new String[] {
		   },
		   new URI[] {
			 URI.createURI(FjavaPackage.eNS_URI).appendFragment("//Method"),
			 URI.createURI(FjavaPackage.eNS_URI).appendFragment("//Method/returntype")
		   });
	}

	/**
	 * Initializes the annotations for <b>Operation_parameters2Method_params</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOperation_parameters2Method_paramsAnnotations() {
		String source = "Operation_parameters2Method_params";	
		addAnnotation
		  (getOperation_Parameters(), 
		   source, 
		   new String[] {
		   },
		   new URI[] {
			 URI.createURI(FjavaPackage.eNS_URI).appendFragment("//Method"),
			 URI.createURI(FjavaPackage.eNS_URI).appendFragment("//Method/params")
		   });
	}

	/**
	 * Initializes the annotations for <b>PrimitiveType2Class</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createPrimitiveType2ClassAnnotations() {
		String source = "PrimitiveType2Class";	
		addAnnotation
		  (primitiveTypeEClass, 
		   source, 
		   new String[] {
		   },
		   new URI[] {
			 URI.createURI(FjavaPackage.eNS_URI).appendFragment("//Class")
		   });
	}

} //MiniumlPackageImpl
