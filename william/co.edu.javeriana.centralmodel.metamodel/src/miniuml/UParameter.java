/**
 */
package miniuml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UParameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link miniuml.UParameter#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see miniuml.MiniumlPackage#getUParameter()
 * @model
 * @generated
 */
public interface UParameter extends UNamedElement {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(UType)
	 * @see miniuml.MiniumlPackage#getUParameter_Type()
	 * @model required="true"
	 * @generated
	 */
	UType getType();

	/**
	 * Sets the value of the '{@link miniuml.UParameter#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(UType value);

} // UParameter
