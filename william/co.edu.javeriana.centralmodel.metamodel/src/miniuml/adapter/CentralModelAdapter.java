package miniuml.adapter;

import java.lang.reflect.Method;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EFactory;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.resource.Resource;

import fjava.FjavaFactory;
import fjava.FjavaPackage;
import miniuml.MiniumlFactory;
import miniuml.MiniumlPackage;


public class CentralModelAdapter extends AdapterImpl {
	private boolean inProgress = false;
	private EFactory[] factories = new EFactory[] { MiniumlFactory.eINSTANCE, FjavaFactory.eINSTANCE };

	private EPackage[] packages = new EPackage[] { MiniumlPackage.eINSTANCE, FjavaPackage.eINSTANCE };
	public enum CorrespondenceType {
		UNIDIRECTIONAL, BIDIRECTIONAL;
	}

	@Override
	public void notifyChanged(Notification notification) {
		EObject notifier = (EObject) notification.getNotifier();
		
		if (notifier.eResource() != null && !((Resource.Internal) notifier.eResource()).isLoading() && !inProgress) {
			inProgress = true;
			switch (notification.getEventType()) {
			case Notification.ADD:
				addElement(notification);
				break;
			case Notification.REMOVE:
				removeElement(notification);
				break;
			case Notification.SET:
				setElement(notification);
				break;
			default:
				break;
			}
			inProgress = false;
		}
	}
	
	

	private EObject create(EClass c) {
		try {
			EFactory factory = getFactory(c);
			Method creator = factory.getClass().getMethod("create" + c.getName(), new Class<?>[0]);
			return (EObject) creator.invoke(factory, new Object[0]);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	

	private EFactory getFactory(EClass c) {
		EPackage pkg = (EPackage) c.eContainer();
		if (pkg == packages[0]) {
			return factories[0];
		} else {
			return factories[1];
		}
	}

	private EObject getOpposite(EObject obj) {
		EModelElement modelElement = (EModelElement) obj;
		if (modelElement.getEAnnotations().size() > 0) {
			EAnnotation annotation = modelElement.getEAnnotations().get(0);
			/*
			 * If there's feature in the correspondence should always should return it
			 */
			return annotation.getReferences().get(annotation.getReferences().size() - 1);
		} else {
			return null;
		}

	}
	

	private EObject getOpposite(EObject obj, EObject oppositeContainer) {
		EModelElement modelElement = (EModelElement) obj;
		if (modelElement.getEAnnotations().size() > 0) {
			for (EAnnotation annotation : modelElement.getEAnnotations()) {
				ENamedElement oppositeReferenceClass = (ENamedElement) annotation.getReferences().get(0);
				/*
				 * Opposite feature
				 */
				if (annotation.getReferences().size() > 1) {
					ENamedElement oppositeReferenceFeature = (ENamedElement) annotation.getReferences().get(1);
					if (oppositeContainer.getClass().getInterfaces()[0].getSimpleName().equals(oppositeReferenceClass.getName()))
						return oppositeReferenceFeature;
				}
				/*
				 * Opposite class
				 */
				else {
					if (((ENamedElement) oppositeContainer).getName().equals(oppositeReferenceClass.getName()))
						return oppositeReferenceClass;
				}
			}
		} else {
			return null;
		}
		return null;
	}

	private void setOppositeValue(EObject obj, EObject oppositeContainer, Object value) {
		EModelElement modelElement = (EModelElement) obj;
		if (modelElement.getEAnnotations().size() > 0) {
			for (EAnnotation annotation : modelElement.getEAnnotations()) {
				ENamedElement oppositeReferenceClass = (ENamedElement) annotation.getReferences().get(0);
				ENamedElement oppositeReferenceFeature = (ENamedElement) annotation.getReferences().get(1);
				if (oppositeContainer.getClass().getInterfaces()[0].getSimpleName().equals(oppositeReferenceClass.getName()))
					oppositeContainer.eSet((EStructuralFeature) oppositeReferenceFeature, value);
			}
		}
	}

	private EAnnotation createAnnotation(EObject reference, String resource) {
		EAnnotation annotation = EcoreFactory.eINSTANCE.createEAnnotation();
		annotation.setSource(resource);
		annotation.getReferences().add(reference);
		return annotation;
	}

	private void setElement(Notification notification) {
		System.out.println(notification.getEventType() + " " + notification.getNewValue());
		EObject container = (EObject) notification.getNotifier();
		EStructuralFeature feature = (EStructuralFeature) notification.getFeature();
		Object value = notification.getNewValue();

		EObject oppositeContainer = (EObject) getOpposite(container);
		if (getAnnotations(container).get(0).getSource().contains(CorrespondenceType.BIDIRECTIONAL.toString())
				|| getAnnotations(container).get(0).getSource().contains(CorrespondenceType.UNIDIRECTIONAL.toString())) {
			setOppositeValue(feature, oppositeContainer, value);
		}
	}
	
	private EList<EAnnotation> getAnnotations(EObject obj) {
		EList<EAnnotation> eAnnotations = null;
		if (obj instanceof EModelElement) {
			eAnnotations = ((EModelElement) obj).getEAnnotations();
		}
		if (obj instanceof fjava.Class) {
			eAnnotations = ((fjava.Class) obj).getEAnnotations();
		}
		if (obj instanceof fjava.Field) {
			eAnnotations = ((fjava.Field) obj).getEAnnotations();
		}
		return eAnnotations;
	}

	private void addElement(Notification notification) {
		System.out.println(notification.getEventType() + " " + notification.getNewValue());
		EObject container = (EObject) notification.getNotifier();
		EStructuralFeature feature = (EStructuralFeature) notification.getFeature();
		EObject value = (EObject) notification.getNewValue();

		EObject oppositeContainer = (EObject) getOpposite(container);//getOppositeContainer(value);
		EStructuralFeature oppositeFeature = (EStructuralFeature) getOpposite(feature, oppositeContainer);
		EClass oppositeClass = (EClass) getOpposite(value.eClass(), oppositeFeature.getEType());//Retornar una lista de objetos para que sean creados al otro lado
		EObject oppositeValue = create(oppositeClass);
		
		createModelAnnotations(feature, value, oppositeValue);		
		
		@SuppressWarnings("unchecked")		
		EList<Object> rightList = (EList<Object>) oppositeContainer.eGet(oppositeFeature);
		rightList.add(oppositeValue);
	}

	private void createModelAnnotations(EStructuralFeature feature, EObject value, EObject oppositeValue) {
				
				EClass oppositeOfOpposite = (EClass) getOpposite(oppositeValue.eClass(), feature.getEType());
				String source;
				String oppositeSource;		
				if (oppositeOfOpposite != null) {
					source = CorrespondenceType.BIDIRECTIONAL + "_" + value.toString() + "2" + oppositeValue.toString();
					oppositeSource = CorrespondenceType.BIDIRECTIONAL + "_" + oppositeValue.toString() + "2" + value.toString();
				}
				else {
					source = CorrespondenceType.UNIDIRECTIONAL + "_" + value.toString() + "2" + oppositeValue.toString();
					oppositeSource = oppositeValue.toString() + "2" + value.toString();	
				}
				EAnnotation annotation = createAnnotation((EModelElement) oppositeValue, source);
				EList<EAnnotation> annotations = getAnnotations(value);
				annotations.add(annotation);
				
				EAnnotation oppositeAnnotation = createAnnotation((EModelElement) value, oppositeSource);
				EList<EAnnotation> eOppositeAnnotations = getAnnotations(oppositeValue);
				eOppositeAnnotations.add(oppositeAnnotation);
	}

	@SuppressWarnings("unused")
	private EObject getOppositeContainer(EObject obj) {
		EModelElement modelElement = (EModelElement) obj;
		if (modelElement.getEAnnotations() != null) {
			for (EAnnotation annotation : modelElement.getEAnnotations()) {
				for (EObject refernce : annotation.getReferences()) {
					return refernce.eContainer();
				}
			}
		}
		return null;
	}
	
	private void removeElement(Notification notification) {
		System.out.println(notification.getEventType() + " " + notification.getOldValue());
		EObject container = (EObject) notification.getNotifier();
		EStructuralFeature feature = (EStructuralFeature) notification.getFeature();
		
		EObject value = (EObject) notification.getOldValue();
		
		EObject oppositeContainer = (EObject) getOpposite(container);//getOppositeContainer(value);
		EStructuralFeature oppositeFeature = (EStructuralFeature) getOpposite(feature);//, oppositeContainer)
		EObject oppositeClass = getOpposite(value);//Debe retornar una lista de los elementos del otro lado a eliminar
		
		if (getAnnotations(value).get(0).getSource().contains(CorrespondenceType.BIDIRECTIONAL.toString())
				|| getAnnotations(value).get(0).getSource().contains(CorrespondenceType.UNIDIRECTIONAL.toString())) {
			@SuppressWarnings("unchecked")
			EList<Object> rightList = (EList<Object>) oppositeContainer.eGet(oppositeFeature);
			// rightList.remove(oppositeValue);
			rightList.remove(oppositeClass);
		}
	}
}