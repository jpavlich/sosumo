/**
 */
package miniuml;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EModelElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link miniuml.Class#getAttributes <em>Attributes</em>}</li>
 *   <li>{@link miniuml.Class#getParents <em>Parents</em>}</li>
 *   <li>{@link miniuml.Class#getOperations <em>Operations</em>}</li>
 *   <li>{@link miniuml.Class#isIsAbstrsct <em>Is Abstrsct</em>}</li>
 * </ul>
 *
 * @see miniuml.MiniumlPackage#getClass_()
 * @model
 * @generated
 */
public interface Class extends Type, EModelElement {
	/**
	 * Returns the value of the '<em><b>Attributes</b></em>' containment reference list.
	 * The list contents are of type {@link miniuml.Attribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attributes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attributes</em>' containment reference list.
	 * @see miniuml.MiniumlPackage#getClass_Attributes()
	 * @model containment="true"
	 * @generated
	 */
	EList<Attribute> getAttributes();

	/**
	 * Returns the value of the '<em><b>Parents</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parents</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parents</em>' reference.
	 * @see #setParents(Class)
	 * @see miniuml.MiniumlPackage#getClass_Parents()
	 * @model
	 * @generated
	 */
	Class getParents();

	/**
	 * Sets the value of the '{@link miniuml.Class#getParents <em>Parents</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parents</em>' reference.
	 * @see #getParents()
	 * @generated
	 */
	void setParents(Class value);

	/**
	 * Returns the value of the '<em><b>Operations</b></em>' containment reference list.
	 * The list contents are of type {@link miniuml.Operation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations</em>' containment reference list.
	 * @see miniuml.MiniumlPackage#getClass_Operations()
	 * @model containment="true"
	 * @generated
	 */
	EList<Operation> getOperations();

	/**
	 * Returns the value of the '<em><b>Is Abstrsct</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Abstrsct</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Abstrsct</em>' attribute.
	 * @see #setIsAbstrsct(boolean)
	 * @see miniuml.MiniumlPackage#getClass_IsAbstrsct()
	 * @model
	 * @generated
	 */
	boolean isIsAbstrsct();

	/**
	 * Sets the value of the '{@link miniuml.Class#isIsAbstrsct <em>Is Abstrsct</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Abstrsct</em>' attribute.
	 * @see #isIsAbstrsct()
	 * @generated
	 */
	void setIsAbstrsct(boolean value);

} // Class
