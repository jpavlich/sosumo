/**
 *
 * $Id$
 */
package miniuml.validation;

import miniuml.Type;
import miniuml.UParameter;
import miniuml.UType;
import miniuml.VisibilityType;

import org.eclipse.emf.common.util.EList;

/**
 * A sample validator interface for {@link miniuml.Operation}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface OperationValidator {
	boolean validate();

	boolean validateIsStatic(boolean value);
	boolean validateReturnType(Type value);

	boolean validateReturnType(UType value);
	boolean validateParameters(EList<UParameter> value);
	boolean validateVisibility(VisibilityType value);
}
