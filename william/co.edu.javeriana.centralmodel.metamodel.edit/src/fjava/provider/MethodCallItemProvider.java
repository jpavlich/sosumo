/**
 */
package fjava.provider;


import fjava.FjavaFactory;
import fjava.FjavaPackage;
import fjava.MethodCall;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fjava.MethodCall} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MethodCallItemProvider extends MessageItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MethodCallItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addNamePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MethodCall_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MethodCall_name_feature", "_UI_MethodCall_type"),
				 FjavaPackage.Literals.METHOD_CALL__NAME,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(FjavaPackage.Literals.METHOD_CALL__ARGS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns MethodCall.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/MethodCall"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_MethodCall_type");
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MethodCall.class)) {
			case FjavaPackage.METHOD_CALL__ARGS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(FjavaPackage.Literals.METHOD_CALL__ARGS,
				 FjavaFactory.eINSTANCE.createArgument()));

		newChildDescriptors.add
			(createChildParameter
				(FjavaPackage.Literals.METHOD_CALL__ARGS,
				 FjavaFactory.eINSTANCE.createExpression()));

		newChildDescriptors.add
			(createChildParameter
				(FjavaPackage.Literals.METHOD_CALL__ARGS,
				 FjavaFactory.eINSTANCE.createThis()));

		newChildDescriptors.add
			(createChildParameter
				(FjavaPackage.Literals.METHOD_CALL__ARGS,
				 FjavaFactory.eINSTANCE.createVariable()));

		newChildDescriptors.add
			(createChildParameter
				(FjavaPackage.Literals.METHOD_CALL__ARGS,
				 FjavaFactory.eINSTANCE.createNew()));

		newChildDescriptors.add
			(createChildParameter
				(FjavaPackage.Literals.METHOD_CALL__ARGS,
				 FjavaFactory.eINSTANCE.createCast()));

		newChildDescriptors.add
			(createChildParameter
				(FjavaPackage.Literals.METHOD_CALL__ARGS,
				 FjavaFactory.eINSTANCE.createConstant()));

		newChildDescriptors.add
			(createChildParameter
				(FjavaPackage.Literals.METHOD_CALL__ARGS,
				 FjavaFactory.eINSTANCE.createStringConstant()));

		newChildDescriptors.add
			(createChildParameter
				(FjavaPackage.Literals.METHOD_CALL__ARGS,
				 FjavaFactory.eINSTANCE.createIntConstant()));

		newChildDescriptors.add
			(createChildParameter
				(FjavaPackage.Literals.METHOD_CALL__ARGS,
				 FjavaFactory.eINSTANCE.createBoolConstant()));

		newChildDescriptors.add
			(createChildParameter
				(FjavaPackage.Literals.METHOD_CALL__ARGS,
				 FjavaFactory.eINSTANCE.createSelection()));
	}

}
