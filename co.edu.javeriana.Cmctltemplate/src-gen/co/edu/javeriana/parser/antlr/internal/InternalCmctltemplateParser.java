package co.edu.javeriana.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import co.edu.javeriana.services.CmctltemplateGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalCmctltemplateParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_CHARACTER", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'import'", "';'", "'\\u00ABtemplate'", "'['", "','", "']'", "'\\u00BB'", "'\\u00ABendTemplate\\u00BB'", "'\\u00AB'", "'if'", "'\\u00ABendIf\\u00BB'", "'for'", "'\\u00ABendFor\\u00BB'", "'.'", "'true'", "'false'", "'.*'"
    };
    public static final int RULE_ID=4;
    public static final int T__28=28;
    public static final int T__27=27;
    public static final int T__26=26;
    public static final int T__25=25;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=11;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int RULE_CHARACTER=5;
    public static final int RULE_SL_COMMENT=9;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=8;
    public static final int T__19=19;
    public static final int RULE_STRING=7;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=6;
    public static final int RULE_WS=10;

    // delegates
    // delegators


        public InternalCmctltemplateParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalCmctltemplateParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalCmctltemplateParser.tokenNames; }
    public String getGrammarFileName() { return "../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g"; }



     	private CmctltemplateGrammarAccess grammarAccess;
     	
        public InternalCmctltemplateParser(TokenStream input, CmctltemplateGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Template";	
       	}
       	
       	@Override
       	protected CmctltemplateGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleTemplate"
    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:67:1: entryRuleTemplate returns [EObject current=null] : iv_ruleTemplate= ruleTemplate EOF ;
    public final EObject entryRuleTemplate() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTemplate = null;


        try {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:68:2: (iv_ruleTemplate= ruleTemplate EOF )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:69:2: iv_ruleTemplate= ruleTemplate EOF
            {
             newCompositeNode(grammarAccess.getTemplateRule()); 
            pushFollow(FOLLOW_ruleTemplate_in_entryRuleTemplate75);
            iv_ruleTemplate=ruleTemplate();

            state._fsp--;

             current =iv_ruleTemplate; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleTemplate85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTemplate"


    // $ANTLR start "ruleTemplate"
    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:76:1: ruleTemplate returns [EObject current=null] : ( ( (lv_imports_0_0= ruleImport ) )* ( (lv_templateFunctions_1_0= ruleTemplateFunction ) )* ) ;
    public final EObject ruleTemplate() throws RecognitionException {
        EObject current = null;

        EObject lv_imports_0_0 = null;

        EObject lv_templateFunctions_1_0 = null;


         enterRule(); 
            
        try {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:79:28: ( ( ( (lv_imports_0_0= ruleImport ) )* ( (lv_templateFunctions_1_0= ruleTemplateFunction ) )* ) )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:80:1: ( ( (lv_imports_0_0= ruleImport ) )* ( (lv_templateFunctions_1_0= ruleTemplateFunction ) )* )
            {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:80:1: ( ( (lv_imports_0_0= ruleImport ) )* ( (lv_templateFunctions_1_0= ruleTemplateFunction ) )* )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:80:2: ( (lv_imports_0_0= ruleImport ) )* ( (lv_templateFunctions_1_0= ruleTemplateFunction ) )*
            {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:80:2: ( (lv_imports_0_0= ruleImport ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==12) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:81:1: (lv_imports_0_0= ruleImport )
            	    {
            	    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:81:1: (lv_imports_0_0= ruleImport )
            	    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:82:3: lv_imports_0_0= ruleImport
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getTemplateAccess().getImportsImportParserRuleCall_0_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleImport_in_ruleTemplate131);
            	    lv_imports_0_0=ruleImport();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getTemplateRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"imports",
            	            		lv_imports_0_0, 
            	            		"Import");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:98:3: ( (lv_templateFunctions_1_0= ruleTemplateFunction ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==14) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:99:1: (lv_templateFunctions_1_0= ruleTemplateFunction )
            	    {
            	    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:99:1: (lv_templateFunctions_1_0= ruleTemplateFunction )
            	    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:100:3: lv_templateFunctions_1_0= ruleTemplateFunction
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getTemplateAccess().getTemplateFunctionsTemplateFunctionParserRuleCall_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleTemplateFunction_in_ruleTemplate153);
            	    lv_templateFunctions_1_0=ruleTemplateFunction();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getTemplateRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"templateFunctions",
            	            		lv_templateFunctions_1_0, 
            	            		"TemplateFunction");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTemplate"


    // $ANTLR start "entryRuleImport"
    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:124:1: entryRuleImport returns [EObject current=null] : iv_ruleImport= ruleImport EOF ;
    public final EObject entryRuleImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImport = null;


        try {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:125:2: (iv_ruleImport= ruleImport EOF )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:126:2: iv_ruleImport= ruleImport EOF
            {
             newCompositeNode(grammarAccess.getImportRule()); 
            pushFollow(FOLLOW_ruleImport_in_entryRuleImport190);
            iv_ruleImport=ruleImport();

            state._fsp--;

             current =iv_ruleImport; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleImport200); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:133:1: ruleImport returns [EObject current=null] : (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) otherlv_2= ';' ) ;
    public final EObject ruleImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        AntlrDatatypeRuleToken lv_importedNamespace_1_0 = null;


         enterRule(); 
            
        try {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:136:28: ( (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) otherlv_2= ';' ) )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:137:1: (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) otherlv_2= ';' )
            {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:137:1: (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) otherlv_2= ';' )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:137:3: otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) otherlv_2= ';'
            {
            otherlv_0=(Token)match(input,12,FOLLOW_12_in_ruleImport237); 

                	newLeafNode(otherlv_0, grammarAccess.getImportAccess().getImportKeyword_0());
                
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:141:1: ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:142:1: (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard )
            {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:142:1: (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:143:3: lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard
            {
             
            	        newCompositeNode(grammarAccess.getImportAccess().getImportedNamespaceQualifiedNameWithWildcardParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleQualifiedNameWithWildcard_in_ruleImport258);
            lv_importedNamespace_1_0=ruleQualifiedNameWithWildcard();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getImportRule());
            	        }
                   		set(
                   			current, 
                   			"importedNamespace",
                    		lv_importedNamespace_1_0, 
                    		"QualifiedNameWithWildcard");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_2=(Token)match(input,13,FOLLOW_13_in_ruleImport270); 

                	newLeafNode(otherlv_2, grammarAccess.getImportAccess().getSemicolonKeyword_2());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleTemplateFunction"
    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:171:1: entryRuleTemplateFunction returns [EObject current=null] : iv_ruleTemplateFunction= ruleTemplateFunction EOF ;
    public final EObject entryRuleTemplateFunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTemplateFunction = null;


        try {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:172:2: (iv_ruleTemplateFunction= ruleTemplateFunction EOF )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:173:2: iv_ruleTemplateFunction= ruleTemplateFunction EOF
            {
             newCompositeNode(grammarAccess.getTemplateFunctionRule()); 
            pushFollow(FOLLOW_ruleTemplateFunction_in_entryRuleTemplateFunction306);
            iv_ruleTemplateFunction=ruleTemplateFunction();

            state._fsp--;

             current =iv_ruleTemplateFunction; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleTemplateFunction316); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTemplateFunction"


    // $ANTLR start "ruleTemplateFunction"
    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:180:1: ruleTemplateFunction returns [EObject current=null] : (otherlv_0= '\\u00ABtemplate' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '[' ( (lv_parameters_3_0= ruleParameter ) ) (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameter ) ) )* otherlv_6= ']' otherlv_7= '\\u00BB' ( (lv_statements_8_0= ruleStatement ) )* otherlv_9= '\\u00ABendTemplate\\u00BB' ) ;
    public final EObject ruleTemplateFunction() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        EObject lv_parameters_3_0 = null;

        EObject lv_parameters_5_0 = null;

        EObject lv_statements_8_0 = null;


         enterRule(); 
            
        try {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:183:28: ( (otherlv_0= '\\u00ABtemplate' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '[' ( (lv_parameters_3_0= ruleParameter ) ) (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameter ) ) )* otherlv_6= ']' otherlv_7= '\\u00BB' ( (lv_statements_8_0= ruleStatement ) )* otherlv_9= '\\u00ABendTemplate\\u00BB' ) )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:184:1: (otherlv_0= '\\u00ABtemplate' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '[' ( (lv_parameters_3_0= ruleParameter ) ) (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameter ) ) )* otherlv_6= ']' otherlv_7= '\\u00BB' ( (lv_statements_8_0= ruleStatement ) )* otherlv_9= '\\u00ABendTemplate\\u00BB' )
            {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:184:1: (otherlv_0= '\\u00ABtemplate' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '[' ( (lv_parameters_3_0= ruleParameter ) ) (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameter ) ) )* otherlv_6= ']' otherlv_7= '\\u00BB' ( (lv_statements_8_0= ruleStatement ) )* otherlv_9= '\\u00ABendTemplate\\u00BB' )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:184:3: otherlv_0= '\\u00ABtemplate' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '[' ( (lv_parameters_3_0= ruleParameter ) ) (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameter ) ) )* otherlv_6= ']' otherlv_7= '\\u00BB' ( (lv_statements_8_0= ruleStatement ) )* otherlv_9= '\\u00ABendTemplate\\u00BB'
            {
            otherlv_0=(Token)match(input,14,FOLLOW_14_in_ruleTemplateFunction353); 

                	newLeafNode(otherlv_0, grammarAccess.getTemplateFunctionAccess().getTemplateKeyword_0());
                
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:188:1: ( (lv_name_1_0= RULE_ID ) )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:189:1: (lv_name_1_0= RULE_ID )
            {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:189:1: (lv_name_1_0= RULE_ID )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:190:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleTemplateFunction370); 

            			newLeafNode(lv_name_1_0, grammarAccess.getTemplateFunctionAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getTemplateFunctionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,15,FOLLOW_15_in_ruleTemplateFunction387); 

                	newLeafNode(otherlv_2, grammarAccess.getTemplateFunctionAccess().getLeftSquareBracketKeyword_2());
                
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:210:1: ( (lv_parameters_3_0= ruleParameter ) )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:211:1: (lv_parameters_3_0= ruleParameter )
            {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:211:1: (lv_parameters_3_0= ruleParameter )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:212:3: lv_parameters_3_0= ruleParameter
            {
             
            	        newCompositeNode(grammarAccess.getTemplateFunctionAccess().getParametersParameterParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_ruleParameter_in_ruleTemplateFunction408);
            lv_parameters_3_0=ruleParameter();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getTemplateFunctionRule());
            	        }
                   		add(
                   			current, 
                   			"parameters",
                    		lv_parameters_3_0, 
                    		"Parameter");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:228:2: (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameter ) ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==16) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:228:4: otherlv_4= ',' ( (lv_parameters_5_0= ruleParameter ) )
            	    {
            	    otherlv_4=(Token)match(input,16,FOLLOW_16_in_ruleTemplateFunction421); 

            	        	newLeafNode(otherlv_4, grammarAccess.getTemplateFunctionAccess().getCommaKeyword_4_0());
            	        
            	    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:232:1: ( (lv_parameters_5_0= ruleParameter ) )
            	    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:233:1: (lv_parameters_5_0= ruleParameter )
            	    {
            	    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:233:1: (lv_parameters_5_0= ruleParameter )
            	    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:234:3: lv_parameters_5_0= ruleParameter
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getTemplateFunctionAccess().getParametersParameterParserRuleCall_4_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleParameter_in_ruleTemplateFunction442);
            	    lv_parameters_5_0=ruleParameter();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getTemplateFunctionRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"parameters",
            	            		lv_parameters_5_0, 
            	            		"Parameter");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            otherlv_6=(Token)match(input,17,FOLLOW_17_in_ruleTemplateFunction456); 

                	newLeafNode(otherlv_6, grammarAccess.getTemplateFunctionAccess().getRightSquareBracketKeyword_5());
                
            otherlv_7=(Token)match(input,18,FOLLOW_18_in_ruleTemplateFunction468); 

                	newLeafNode(otherlv_7, grammarAccess.getTemplateFunctionAccess().getRightPointingDoubleAngleQuotationMarkKeyword_6());
                
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:258:1: ( (lv_statements_8_0= ruleStatement ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( ((LA4_0>=RULE_ID && LA4_0<=RULE_CHARACTER)||LA4_0==20) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:259:1: (lv_statements_8_0= ruleStatement )
            	    {
            	    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:259:1: (lv_statements_8_0= ruleStatement )
            	    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:260:3: lv_statements_8_0= ruleStatement
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getTemplateFunctionAccess().getStatementsStatementParserRuleCall_7_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleStatement_in_ruleTemplateFunction489);
            	    lv_statements_8_0=ruleStatement();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getTemplateFunctionRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"statements",
            	            		lv_statements_8_0, 
            	            		"Statement");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            otherlv_9=(Token)match(input,19,FOLLOW_19_in_ruleTemplateFunction502); 

                	newLeafNode(otherlv_9, grammarAccess.getTemplateFunctionAccess().getEndTemplateKeyword_8());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTemplateFunction"


    // $ANTLR start "entryRuleStatement"
    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:288:1: entryRuleStatement returns [EObject current=null] : iv_ruleStatement= ruleStatement EOF ;
    public final EObject entryRuleStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStatement = null;


        try {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:289:2: (iv_ruleStatement= ruleStatement EOF )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:290:2: iv_ruleStatement= ruleStatement EOF
            {
             newCompositeNode(grammarAccess.getStatementRule()); 
            pushFollow(FOLLOW_ruleStatement_in_entryRuleStatement538);
            iv_ruleStatement=ruleStatement();

            state._fsp--;

             current =iv_ruleStatement; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleStatement548); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStatement"


    // $ANTLR start "ruleStatement"
    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:297:1: ruleStatement returns [EObject current=null] : ( ( () this_CHARACTER_1= RULE_CHARACTER ) | this_ID_2= RULE_ID | this_TemplateStatement_3= ruleTemplateStatement | this_TemplateReference_4= ruleTemplateReference ) ;
    public final EObject ruleStatement() throws RecognitionException {
        EObject current = null;

        Token this_CHARACTER_1=null;
        Token this_ID_2=null;
        EObject this_TemplateStatement_3 = null;

        EObject this_TemplateReference_4 = null;


         enterRule(); 
            
        try {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:300:28: ( ( ( () this_CHARACTER_1= RULE_CHARACTER ) | this_ID_2= RULE_ID | this_TemplateStatement_3= ruleTemplateStatement | this_TemplateReference_4= ruleTemplateReference ) )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:301:1: ( ( () this_CHARACTER_1= RULE_CHARACTER ) | this_ID_2= RULE_ID | this_TemplateStatement_3= ruleTemplateStatement | this_TemplateReference_4= ruleTemplateReference )
            {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:301:1: ( ( () this_CHARACTER_1= RULE_CHARACTER ) | this_ID_2= RULE_ID | this_TemplateStatement_3= ruleTemplateStatement | this_TemplateReference_4= ruleTemplateReference )
            int alt5=4;
            switch ( input.LA(1) ) {
            case RULE_CHARACTER:
                {
                alt5=1;
                }
                break;
            case RULE_ID:
                {
                alt5=2;
                }
                break;
            case 20:
                {
                int LA5_3 = input.LA(2);

                if ( (LA5_3==21||LA5_3==23) ) {
                    alt5=3;
                }
                else if ( (LA5_3==RULE_ID) ) {
                    alt5=4;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 5, 3, input);

                    throw nvae;
                }
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:301:2: ( () this_CHARACTER_1= RULE_CHARACTER )
                    {
                    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:301:2: ( () this_CHARACTER_1= RULE_CHARACTER )
                    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:301:3: () this_CHARACTER_1= RULE_CHARACTER
                    {
                    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:301:3: ()
                    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:302:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getStatementAccess().getStatementAction_0_0(),
                                current);
                        

                    }

                    this_CHARACTER_1=(Token)match(input,RULE_CHARACTER,FOLLOW_RULE_CHARACTER_in_ruleStatement594); 
                     
                        newLeafNode(this_CHARACTER_1, grammarAccess.getStatementAccess().getCHARACTERTerminalRuleCall_0_1()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:312:6: this_ID_2= RULE_ID
                    {
                    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleStatement611); 
                     
                        newLeafNode(this_ID_2, grammarAccess.getStatementAccess().getIDTerminalRuleCall_1()); 
                        

                    }
                    break;
                case 3 :
                    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:318:5: this_TemplateStatement_3= ruleTemplateStatement
                    {
                     
                            newCompositeNode(grammarAccess.getStatementAccess().getTemplateStatementParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_ruleTemplateStatement_in_ruleStatement638);
                    this_TemplateStatement_3=ruleTemplateStatement();

                    state._fsp--;

                     
                            current = this_TemplateStatement_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:328:5: this_TemplateReference_4= ruleTemplateReference
                    {
                     
                            newCompositeNode(grammarAccess.getStatementAccess().getTemplateReferenceParserRuleCall_3()); 
                        
                    pushFollow(FOLLOW_ruleTemplateReference_in_ruleStatement665);
                    this_TemplateReference_4=ruleTemplateReference();

                    state._fsp--;

                     
                            current = this_TemplateReference_4; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStatement"


    // $ANTLR start "entryRuleTemplateStatement"
    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:344:1: entryRuleTemplateStatement returns [EObject current=null] : iv_ruleTemplateStatement= ruleTemplateStatement EOF ;
    public final EObject entryRuleTemplateStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTemplateStatement = null;


        try {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:345:2: (iv_ruleTemplateStatement= ruleTemplateStatement EOF )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:346:2: iv_ruleTemplateStatement= ruleTemplateStatement EOF
            {
             newCompositeNode(grammarAccess.getTemplateStatementRule()); 
            pushFollow(FOLLOW_ruleTemplateStatement_in_entryRuleTemplateStatement700);
            iv_ruleTemplateStatement=ruleTemplateStatement();

            state._fsp--;

             current =iv_ruleTemplateStatement; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleTemplateStatement710); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTemplateStatement"


    // $ANTLR start "ruleTemplateStatement"
    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:353:1: ruleTemplateStatement returns [EObject current=null] : (this_IfTemplate_0= ruleIfTemplate | this_ForTemplate_1= ruleForTemplate ) ;
    public final EObject ruleTemplateStatement() throws RecognitionException {
        EObject current = null;

        EObject this_IfTemplate_0 = null;

        EObject this_ForTemplate_1 = null;


         enterRule(); 
            
        try {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:356:28: ( (this_IfTemplate_0= ruleIfTemplate | this_ForTemplate_1= ruleForTemplate ) )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:357:1: (this_IfTemplate_0= ruleIfTemplate | this_ForTemplate_1= ruleForTemplate )
            {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:357:1: (this_IfTemplate_0= ruleIfTemplate | this_ForTemplate_1= ruleForTemplate )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==20) ) {
                int LA6_1 = input.LA(2);

                if ( (LA6_1==23) ) {
                    alt6=2;
                }
                else if ( (LA6_1==21) ) {
                    alt6=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 6, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:358:5: this_IfTemplate_0= ruleIfTemplate
                    {
                     
                            newCompositeNode(grammarAccess.getTemplateStatementAccess().getIfTemplateParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleIfTemplate_in_ruleTemplateStatement757);
                    this_IfTemplate_0=ruleIfTemplate();

                    state._fsp--;

                     
                            current = this_IfTemplate_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:368:5: this_ForTemplate_1= ruleForTemplate
                    {
                     
                            newCompositeNode(grammarAccess.getTemplateStatementAccess().getForTemplateParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleForTemplate_in_ruleTemplateStatement784);
                    this_ForTemplate_1=ruleForTemplate();

                    state._fsp--;

                     
                            current = this_ForTemplate_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTemplateStatement"


    // $ANTLR start "entryRuleIfTemplate"
    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:384:1: entryRuleIfTemplate returns [EObject current=null] : iv_ruleIfTemplate= ruleIfTemplate EOF ;
    public final EObject entryRuleIfTemplate() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIfTemplate = null;


        try {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:385:2: (iv_ruleIfTemplate= ruleIfTemplate EOF )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:386:2: iv_ruleIfTemplate= ruleIfTemplate EOF
            {
             newCompositeNode(grammarAccess.getIfTemplateRule()); 
            pushFollow(FOLLOW_ruleIfTemplate_in_entryRuleIfTemplate819);
            iv_ruleIfTemplate=ruleIfTemplate();

            state._fsp--;

             current =iv_ruleIfTemplate; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleIfTemplate829); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIfTemplate"


    // $ANTLR start "ruleIfTemplate"
    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:393:1: ruleIfTemplate returns [EObject current=null] : ( () otherlv_1= '\\u00AB' otherlv_2= 'if' ( (lv_templateCondition_3_0= ruleBoolValue ) ) otherlv_4= '\\u00BB' ( (lv_statements_5_0= ruleStatement ) )* otherlv_6= '\\u00ABendIf\\u00BB' ) ;
    public final EObject ruleIfTemplate() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_templateCondition_3_0 = null;

        EObject lv_statements_5_0 = null;


         enterRule(); 
            
        try {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:396:28: ( ( () otherlv_1= '\\u00AB' otherlv_2= 'if' ( (lv_templateCondition_3_0= ruleBoolValue ) ) otherlv_4= '\\u00BB' ( (lv_statements_5_0= ruleStatement ) )* otherlv_6= '\\u00ABendIf\\u00BB' ) )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:397:1: ( () otherlv_1= '\\u00AB' otherlv_2= 'if' ( (lv_templateCondition_3_0= ruleBoolValue ) ) otherlv_4= '\\u00BB' ( (lv_statements_5_0= ruleStatement ) )* otherlv_6= '\\u00ABendIf\\u00BB' )
            {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:397:1: ( () otherlv_1= '\\u00AB' otherlv_2= 'if' ( (lv_templateCondition_3_0= ruleBoolValue ) ) otherlv_4= '\\u00BB' ( (lv_statements_5_0= ruleStatement ) )* otherlv_6= '\\u00ABendIf\\u00BB' )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:397:2: () otherlv_1= '\\u00AB' otherlv_2= 'if' ( (lv_templateCondition_3_0= ruleBoolValue ) ) otherlv_4= '\\u00BB' ( (lv_statements_5_0= ruleStatement ) )* otherlv_6= '\\u00ABendIf\\u00BB'
            {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:397:2: ()
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:398:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getIfTemplateAccess().getIfTemplateAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,20,FOLLOW_20_in_ruleIfTemplate875); 

                	newLeafNode(otherlv_1, grammarAccess.getIfTemplateAccess().getLeftPointingDoubleAngleQuotationMarkKeyword_1());
                
            otherlv_2=(Token)match(input,21,FOLLOW_21_in_ruleIfTemplate887); 

                	newLeafNode(otherlv_2, grammarAccess.getIfTemplateAccess().getIfKeyword_2());
                
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:411:1: ( (lv_templateCondition_3_0= ruleBoolValue ) )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:412:1: (lv_templateCondition_3_0= ruleBoolValue )
            {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:412:1: (lv_templateCondition_3_0= ruleBoolValue )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:413:3: lv_templateCondition_3_0= ruleBoolValue
            {
             
            	        newCompositeNode(grammarAccess.getIfTemplateAccess().getTemplateConditionBoolValueParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_ruleBoolValue_in_ruleIfTemplate908);
            lv_templateCondition_3_0=ruleBoolValue();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getIfTemplateRule());
            	        }
                   		add(
                   			current, 
                   			"templateCondition",
                    		lv_templateCondition_3_0, 
                    		"BoolValue");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_4=(Token)match(input,18,FOLLOW_18_in_ruleIfTemplate920); 

                	newLeafNode(otherlv_4, grammarAccess.getIfTemplateAccess().getRightPointingDoubleAngleQuotationMarkKeyword_4());
                
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:433:1: ( (lv_statements_5_0= ruleStatement ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( ((LA7_0>=RULE_ID && LA7_0<=RULE_CHARACTER)||LA7_0==20) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:434:1: (lv_statements_5_0= ruleStatement )
            	    {
            	    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:434:1: (lv_statements_5_0= ruleStatement )
            	    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:435:3: lv_statements_5_0= ruleStatement
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getIfTemplateAccess().getStatementsStatementParserRuleCall_5_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleStatement_in_ruleIfTemplate941);
            	    lv_statements_5_0=ruleStatement();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getIfTemplateRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"statements",
            	            		lv_statements_5_0, 
            	            		"Statement");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            otherlv_6=(Token)match(input,22,FOLLOW_22_in_ruleIfTemplate954); 

                	newLeafNode(otherlv_6, grammarAccess.getIfTemplateAccess().getEndIfKeyword_6());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIfTemplate"


    // $ANTLR start "entryRuleForTemplate"
    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:463:1: entryRuleForTemplate returns [EObject current=null] : iv_ruleForTemplate= ruleForTemplate EOF ;
    public final EObject entryRuleForTemplate() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleForTemplate = null;


        try {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:464:2: (iv_ruleForTemplate= ruleForTemplate EOF )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:465:2: iv_ruleForTemplate= ruleForTemplate EOF
            {
             newCompositeNode(grammarAccess.getForTemplateRule()); 
            pushFollow(FOLLOW_ruleForTemplate_in_entryRuleForTemplate990);
            iv_ruleForTemplate=ruleForTemplate();

            state._fsp--;

             current =iv_ruleForTemplate; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleForTemplate1000); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleForTemplate"


    // $ANTLR start "ruleForTemplate"
    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:472:1: ruleForTemplate returns [EObject current=null] : ( () otherlv_1= '\\u00AB' otherlv_2= 'for' otherlv_3= '\\u00BB' ( (lv_statements_4_0= ruleStatement ) )* otherlv_5= '\\u00ABendFor\\u00BB' ) ;
    public final EObject ruleForTemplate() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_statements_4_0 = null;


         enterRule(); 
            
        try {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:475:28: ( ( () otherlv_1= '\\u00AB' otherlv_2= 'for' otherlv_3= '\\u00BB' ( (lv_statements_4_0= ruleStatement ) )* otherlv_5= '\\u00ABendFor\\u00BB' ) )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:476:1: ( () otherlv_1= '\\u00AB' otherlv_2= 'for' otherlv_3= '\\u00BB' ( (lv_statements_4_0= ruleStatement ) )* otherlv_5= '\\u00ABendFor\\u00BB' )
            {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:476:1: ( () otherlv_1= '\\u00AB' otherlv_2= 'for' otherlv_3= '\\u00BB' ( (lv_statements_4_0= ruleStatement ) )* otherlv_5= '\\u00ABendFor\\u00BB' )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:476:2: () otherlv_1= '\\u00AB' otherlv_2= 'for' otherlv_3= '\\u00BB' ( (lv_statements_4_0= ruleStatement ) )* otherlv_5= '\\u00ABendFor\\u00BB'
            {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:476:2: ()
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:477:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getForTemplateAccess().getForTemplateAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,20,FOLLOW_20_in_ruleForTemplate1046); 

                	newLeafNode(otherlv_1, grammarAccess.getForTemplateAccess().getLeftPointingDoubleAngleQuotationMarkKeyword_1());
                
            otherlv_2=(Token)match(input,23,FOLLOW_23_in_ruleForTemplate1058); 

                	newLeafNode(otherlv_2, grammarAccess.getForTemplateAccess().getForKeyword_2());
                
            otherlv_3=(Token)match(input,18,FOLLOW_18_in_ruleForTemplate1070); 

                	newLeafNode(otherlv_3, grammarAccess.getForTemplateAccess().getRightPointingDoubleAngleQuotationMarkKeyword_3());
                
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:494:1: ( (lv_statements_4_0= ruleStatement ) )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( ((LA8_0>=RULE_ID && LA8_0<=RULE_CHARACTER)||LA8_0==20) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:495:1: (lv_statements_4_0= ruleStatement )
            	    {
            	    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:495:1: (lv_statements_4_0= ruleStatement )
            	    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:496:3: lv_statements_4_0= ruleStatement
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getForTemplateAccess().getStatementsStatementParserRuleCall_4_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleStatement_in_ruleForTemplate1091);
            	    lv_statements_4_0=ruleStatement();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getForTemplateRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"statements",
            	            		lv_statements_4_0, 
            	            		"Statement");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

            otherlv_5=(Token)match(input,24,FOLLOW_24_in_ruleForTemplate1104); 

                	newLeafNode(otherlv_5, grammarAccess.getForTemplateAccess().getEndForKeyword_5());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleForTemplate"


    // $ANTLR start "entryRuleTemplateReference"
    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:524:1: entryRuleTemplateReference returns [EObject current=null] : iv_ruleTemplateReference= ruleTemplateReference EOF ;
    public final EObject entryRuleTemplateReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTemplateReference = null;


        try {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:525:2: (iv_ruleTemplateReference= ruleTemplateReference EOF )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:526:2: iv_ruleTemplateReference= ruleTemplateReference EOF
            {
             newCompositeNode(grammarAccess.getTemplateReferenceRule()); 
            pushFollow(FOLLOW_ruleTemplateReference_in_entryRuleTemplateReference1140);
            iv_ruleTemplateReference=ruleTemplateReference();

            state._fsp--;

             current =iv_ruleTemplateReference; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleTemplateReference1150); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTemplateReference"


    // $ANTLR start "ruleTemplateReference"
    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:533:1: ruleTemplateReference returns [EObject current=null] : ( ( (lv_call_0_0= '\\u00AB' ) ) ( (lv_type_1_0= ruleReference ) ) otherlv_2= '\\u00BB' ) ;
    public final EObject ruleTemplateReference() throws RecognitionException {
        EObject current = null;

        Token lv_call_0_0=null;
        Token otherlv_2=null;
        EObject lv_type_1_0 = null;


         enterRule(); 
            
        try {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:536:28: ( ( ( (lv_call_0_0= '\\u00AB' ) ) ( (lv_type_1_0= ruleReference ) ) otherlv_2= '\\u00BB' ) )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:537:1: ( ( (lv_call_0_0= '\\u00AB' ) ) ( (lv_type_1_0= ruleReference ) ) otherlv_2= '\\u00BB' )
            {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:537:1: ( ( (lv_call_0_0= '\\u00AB' ) ) ( (lv_type_1_0= ruleReference ) ) otherlv_2= '\\u00BB' )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:537:2: ( (lv_call_0_0= '\\u00AB' ) ) ( (lv_type_1_0= ruleReference ) ) otherlv_2= '\\u00BB'
            {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:537:2: ( (lv_call_0_0= '\\u00AB' ) )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:538:1: (lv_call_0_0= '\\u00AB' )
            {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:538:1: (lv_call_0_0= '\\u00AB' )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:539:3: lv_call_0_0= '\\u00AB'
            {
            lv_call_0_0=(Token)match(input,20,FOLLOW_20_in_ruleTemplateReference1193); 

                    newLeafNode(lv_call_0_0, grammarAccess.getTemplateReferenceAccess().getCallLeftPointingDoubleAngleQuotationMarkKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getTemplateReferenceRule());
            	        }
                   		setWithLastConsumed(current, "call", lv_call_0_0, "\u00AB");
            	    

            }


            }

            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:552:2: ( (lv_type_1_0= ruleReference ) )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:553:1: (lv_type_1_0= ruleReference )
            {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:553:1: (lv_type_1_0= ruleReference )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:554:3: lv_type_1_0= ruleReference
            {
             
            	        newCompositeNode(grammarAccess.getTemplateReferenceAccess().getTypeReferenceParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleReference_in_ruleTemplateReference1227);
            lv_type_1_0=ruleReference();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getTemplateReferenceRule());
            	        }
                   		set(
                   			current, 
                   			"type",
                    		lv_type_1_0, 
                    		"Reference");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_2=(Token)match(input,18,FOLLOW_18_in_ruleTemplateReference1239); 

                	newLeafNode(otherlv_2, grammarAccess.getTemplateReferenceAccess().getRightPointingDoubleAngleQuotationMarkKeyword_2());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTemplateReference"


    // $ANTLR start "entryRuleETypedElement"
    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:584:1: entryRuleETypedElement returns [EObject current=null] : iv_ruleETypedElement= ruleETypedElement EOF ;
    public final EObject entryRuleETypedElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleETypedElement = null;


        try {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:585:2: (iv_ruleETypedElement= ruleETypedElement EOF )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:586:2: iv_ruleETypedElement= ruleETypedElement EOF
            {
             newCompositeNode(grammarAccess.getETypedElementRule()); 
            pushFollow(FOLLOW_ruleETypedElement_in_entryRuleETypedElement1277);
            iv_ruleETypedElement=ruleETypedElement();

            state._fsp--;

             current =iv_ruleETypedElement; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleETypedElement1287); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleETypedElement"


    // $ANTLR start "ruleETypedElement"
    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:593:1: ruleETypedElement returns [EObject current=null] : this_Parameter_0= ruleParameter ;
    public final EObject ruleETypedElement() throws RecognitionException {
        EObject current = null;

        EObject this_Parameter_0 = null;


         enterRule(); 
            
        try {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:596:28: (this_Parameter_0= ruleParameter )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:598:5: this_Parameter_0= ruleParameter
            {
             
                    newCompositeNode(grammarAccess.getETypedElementAccess().getParameterParserRuleCall()); 
                
            pushFollow(FOLLOW_ruleParameter_in_ruleETypedElement1333);
            this_Parameter_0=ruleParameter();

            state._fsp--;

             
                    current = this_Parameter_0; 
                    afterParserOrEnumRuleCall();
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleETypedElement"


    // $ANTLR start "entryRuleParameter"
    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:614:1: entryRuleParameter returns [EObject current=null] : iv_ruleParameter= ruleParameter EOF ;
    public final EObject entryRuleParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParameter = null;


        try {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:615:2: (iv_ruleParameter= ruleParameter EOF )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:616:2: iv_ruleParameter= ruleParameter EOF
            {
             newCompositeNode(grammarAccess.getParameterRule()); 
            pushFollow(FOLLOW_ruleParameter_in_entryRuleParameter1367);
            iv_ruleParameter=ruleParameter();

            state._fsp--;

             current =iv_ruleParameter; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleParameter1377); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParameter"


    // $ANTLR start "ruleParameter"
    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:623:1: ruleParameter returns [EObject current=null] : ( ( ( ruleQUALIFIED_NAME ) ) ( (lv_name_1_0= RULE_ID ) ) ) ;
    public final EObject ruleParameter() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;

         enterRule(); 
            
        try {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:626:28: ( ( ( ( ruleQUALIFIED_NAME ) ) ( (lv_name_1_0= RULE_ID ) ) ) )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:627:1: ( ( ( ruleQUALIFIED_NAME ) ) ( (lv_name_1_0= RULE_ID ) ) )
            {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:627:1: ( ( ( ruleQUALIFIED_NAME ) ) ( (lv_name_1_0= RULE_ID ) ) )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:627:2: ( ( ruleQUALIFIED_NAME ) ) ( (lv_name_1_0= RULE_ID ) )
            {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:627:2: ( ( ruleQUALIFIED_NAME ) )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:628:1: ( ruleQUALIFIED_NAME )
            {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:628:1: ( ruleQUALIFIED_NAME )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:629:3: ruleQUALIFIED_NAME
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getParameterRule());
            	        }
                    
             
            	        newCompositeNode(grammarAccess.getParameterAccess().getETypeEClassifierCrossReference_0_0()); 
            	    
            pushFollow(FOLLOW_ruleQUALIFIED_NAME_in_ruleParameter1425);
            ruleQUALIFIED_NAME();

            state._fsp--;

             
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:642:2: ( (lv_name_1_0= RULE_ID ) )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:643:1: (lv_name_1_0= RULE_ID )
            {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:643:1: (lv_name_1_0= RULE_ID )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:644:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleParameter1442); 

            			newLeafNode(lv_name_1_0, grammarAccess.getParameterAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getParameterRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParameter"


    // $ANTLR start "entryRuleReference"
    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:668:1: entryRuleReference returns [EObject current=null] : iv_ruleReference= ruleReference EOF ;
    public final EObject entryRuleReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReference = null;


        try {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:669:2: (iv_ruleReference= ruleReference EOF )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:670:2: iv_ruleReference= ruleReference EOF
            {
             newCompositeNode(grammarAccess.getReferenceRule()); 
            pushFollow(FOLLOW_ruleReference_in_entryRuleReference1483);
            iv_ruleReference=ruleReference();

            state._fsp--;

             current =iv_ruleReference; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleReference1493); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReference"


    // $ANTLR start "ruleReference"
    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:677:1: ruleReference returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) ( (lv_tail_1_0= ruleReferenceTail ) )? ) ;
    public final EObject ruleReference() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_tail_1_0 = null;


         enterRule(); 
            
        try {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:680:28: ( ( ( (otherlv_0= RULE_ID ) ) ( (lv_tail_1_0= ruleReferenceTail ) )? ) )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:681:1: ( ( (otherlv_0= RULE_ID ) ) ( (lv_tail_1_0= ruleReferenceTail ) )? )
            {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:681:1: ( ( (otherlv_0= RULE_ID ) ) ( (lv_tail_1_0= ruleReferenceTail ) )? )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:681:2: ( (otherlv_0= RULE_ID ) ) ( (lv_tail_1_0= ruleReferenceTail ) )?
            {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:681:2: ( (otherlv_0= RULE_ID ) )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:682:1: (otherlv_0= RULE_ID )
            {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:682:1: (otherlv_0= RULE_ID )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:683:3: otherlv_0= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getReferenceRule());
            	        }
                    
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleReference1538); 

            		newLeafNode(otherlv_0, grammarAccess.getReferenceAccess().getReferencedElementETypedElementCrossReference_0_0()); 
            	

            }


            }

            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:694:2: ( (lv_tail_1_0= ruleReferenceTail ) )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==25) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:695:1: (lv_tail_1_0= ruleReferenceTail )
                    {
                    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:695:1: (lv_tail_1_0= ruleReferenceTail )
                    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:696:3: lv_tail_1_0= ruleReferenceTail
                    {
                     
                    	        newCompositeNode(grammarAccess.getReferenceAccess().getTailReferenceTailParserRuleCall_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleReferenceTail_in_ruleReference1559);
                    lv_tail_1_0=ruleReferenceTail();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getReferenceRule());
                    	        }
                           		set(
                           			current, 
                           			"tail",
                            		lv_tail_1_0, 
                            		"ReferenceTail");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReference"


    // $ANTLR start "entryRuleReferenceTail"
    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:720:1: entryRuleReferenceTail returns [EObject current=null] : iv_ruleReferenceTail= ruleReferenceTail EOF ;
    public final EObject entryRuleReferenceTail() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReferenceTail = null;


        try {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:721:2: (iv_ruleReferenceTail= ruleReferenceTail EOF )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:722:2: iv_ruleReferenceTail= ruleReferenceTail EOF
            {
             newCompositeNode(grammarAccess.getReferenceTailRule()); 
            pushFollow(FOLLOW_ruleReferenceTail_in_entryRuleReferenceTail1596);
            iv_ruleReferenceTail=ruleReferenceTail();

            state._fsp--;

             current =iv_ruleReferenceTail; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleReferenceTail1606); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReferenceTail"


    // $ANTLR start "ruleReferenceTail"
    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:729:1: ruleReferenceTail returns [EObject current=null] : (otherlv_0= '.' ( (otherlv_1= RULE_ID ) ) ( (lv_tail_2_0= ruleReferenceTail ) )? ) ;
    public final EObject ruleReferenceTail() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        EObject lv_tail_2_0 = null;


         enterRule(); 
            
        try {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:732:28: ( (otherlv_0= '.' ( (otherlv_1= RULE_ID ) ) ( (lv_tail_2_0= ruleReferenceTail ) )? ) )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:733:1: (otherlv_0= '.' ( (otherlv_1= RULE_ID ) ) ( (lv_tail_2_0= ruleReferenceTail ) )? )
            {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:733:1: (otherlv_0= '.' ( (otherlv_1= RULE_ID ) ) ( (lv_tail_2_0= ruleReferenceTail ) )? )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:733:3: otherlv_0= '.' ( (otherlv_1= RULE_ID ) ) ( (lv_tail_2_0= ruleReferenceTail ) )?
            {
            otherlv_0=(Token)match(input,25,FOLLOW_25_in_ruleReferenceTail1643); 

                	newLeafNode(otherlv_0, grammarAccess.getReferenceTailAccess().getFullStopKeyword_0());
                
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:737:1: ( (otherlv_1= RULE_ID ) )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:738:1: (otherlv_1= RULE_ID )
            {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:738:1: (otherlv_1= RULE_ID )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:739:3: otherlv_1= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getReferenceTailRule());
            	        }
                    
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleReferenceTail1663); 

            		newLeafNode(otherlv_1, grammarAccess.getReferenceTailAccess().getReferencedElementETypedElementCrossReference_1_0()); 
            	

            }


            }

            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:750:2: ( (lv_tail_2_0= ruleReferenceTail ) )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==25) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:751:1: (lv_tail_2_0= ruleReferenceTail )
                    {
                    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:751:1: (lv_tail_2_0= ruleReferenceTail )
                    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:752:3: lv_tail_2_0= ruleReferenceTail
                    {
                     
                    	        newCompositeNode(grammarAccess.getReferenceTailAccess().getTailReferenceTailParserRuleCall_2_0()); 
                    	    
                    pushFollow(FOLLOW_ruleReferenceTail_in_ruleReferenceTail1684);
                    lv_tail_2_0=ruleReferenceTail();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getReferenceTailRule());
                    	        }
                           		set(
                           			current, 
                           			"tail",
                            		lv_tail_2_0, 
                            		"ReferenceTail");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReferenceTail"


    // $ANTLR start "entryRuleBoolValue"
    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:776:1: entryRuleBoolValue returns [EObject current=null] : iv_ruleBoolValue= ruleBoolValue EOF ;
    public final EObject entryRuleBoolValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBoolValue = null;


        try {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:777:2: (iv_ruleBoolValue= ruleBoolValue EOF )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:778:2: iv_ruleBoolValue= ruleBoolValue EOF
            {
             newCompositeNode(grammarAccess.getBoolValueRule()); 
            pushFollow(FOLLOW_ruleBoolValue_in_entryRuleBoolValue1721);
            iv_ruleBoolValue=ruleBoolValue();

            state._fsp--;

             current =iv_ruleBoolValue; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleBoolValue1731); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBoolValue"


    // $ANTLR start "ruleBoolValue"
    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:785:1: ruleBoolValue returns [EObject current=null] : ( ( (lv_type_0_1= 'true' | lv_type_0_2= 'false' ) ) ) ;
    public final EObject ruleBoolValue() throws RecognitionException {
        EObject current = null;

        Token lv_type_0_1=null;
        Token lv_type_0_2=null;

         enterRule(); 
            
        try {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:788:28: ( ( ( (lv_type_0_1= 'true' | lv_type_0_2= 'false' ) ) ) )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:789:1: ( ( (lv_type_0_1= 'true' | lv_type_0_2= 'false' ) ) )
            {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:789:1: ( ( (lv_type_0_1= 'true' | lv_type_0_2= 'false' ) ) )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:790:1: ( (lv_type_0_1= 'true' | lv_type_0_2= 'false' ) )
            {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:790:1: ( (lv_type_0_1= 'true' | lv_type_0_2= 'false' ) )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:791:1: (lv_type_0_1= 'true' | lv_type_0_2= 'false' )
            {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:791:1: (lv_type_0_1= 'true' | lv_type_0_2= 'false' )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==26) ) {
                alt11=1;
            }
            else if ( (LA11_0==27) ) {
                alt11=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }
            switch (alt11) {
                case 1 :
                    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:792:3: lv_type_0_1= 'true'
                    {
                    lv_type_0_1=(Token)match(input,26,FOLLOW_26_in_ruleBoolValue1775); 

                            newLeafNode(lv_type_0_1, grammarAccess.getBoolValueAccess().getTypeTrueKeyword_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBoolValueRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_0_1, null);
                    	    

                    }
                    break;
                case 2 :
                    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:804:8: lv_type_0_2= 'false'
                    {
                    lv_type_0_2=(Token)match(input,27,FOLLOW_27_in_ruleBoolValue1804); 

                            newLeafNode(lv_type_0_2, grammarAccess.getBoolValueAccess().getTypeFalseKeyword_0_1());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBoolValueRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_0_2, null);
                    	    

                    }
                    break;

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBoolValue"


    // $ANTLR start "entryRuleQualifiedNameWithWildcard"
    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:827:1: entryRuleQualifiedNameWithWildcard returns [String current=null] : iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF ;
    public final String entryRuleQualifiedNameWithWildcard() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedNameWithWildcard = null;


        try {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:828:2: (iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:829:2: iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameWithWildcardRule()); 
            pushFollow(FOLLOW_ruleQualifiedNameWithWildcard_in_entryRuleQualifiedNameWithWildcard1856);
            iv_ruleQualifiedNameWithWildcard=ruleQualifiedNameWithWildcard();

            state._fsp--;

             current =iv_ruleQualifiedNameWithWildcard.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQualifiedNameWithWildcard1867); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedNameWithWildcard"


    // $ANTLR start "ruleQualifiedNameWithWildcard"
    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:836:1: ruleQualifiedNameWithWildcard returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_QUALIFIED_NAME_0= ruleQUALIFIED_NAME (kw= '.*' )? ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedNameWithWildcard() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_QUALIFIED_NAME_0 = null;


         enterRule(); 
            
        try {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:839:28: ( (this_QUALIFIED_NAME_0= ruleQUALIFIED_NAME (kw= '.*' )? ) )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:840:1: (this_QUALIFIED_NAME_0= ruleQUALIFIED_NAME (kw= '.*' )? )
            {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:840:1: (this_QUALIFIED_NAME_0= ruleQUALIFIED_NAME (kw= '.*' )? )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:841:5: this_QUALIFIED_NAME_0= ruleQUALIFIED_NAME (kw= '.*' )?
            {
             
                    newCompositeNode(grammarAccess.getQualifiedNameWithWildcardAccess().getQUALIFIED_NAMEParserRuleCall_0()); 
                
            pushFollow(FOLLOW_ruleQUALIFIED_NAME_in_ruleQualifiedNameWithWildcard1914);
            this_QUALIFIED_NAME_0=ruleQUALIFIED_NAME();

            state._fsp--;


            		current.merge(this_QUALIFIED_NAME_0);
                
             
                    afterParserOrEnumRuleCall();
                
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:851:1: (kw= '.*' )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==28) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:852:2: kw= '.*'
                    {
                    kw=(Token)match(input,28,FOLLOW_28_in_ruleQualifiedNameWithWildcard1933); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getQualifiedNameWithWildcardAccess().getFullStopAsteriskKeyword_1()); 
                        

                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedNameWithWildcard"


    // $ANTLR start "entryRuleQUALIFIED_NAME"
    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:865:1: entryRuleQUALIFIED_NAME returns [String current=null] : iv_ruleQUALIFIED_NAME= ruleQUALIFIED_NAME EOF ;
    public final String entryRuleQUALIFIED_NAME() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQUALIFIED_NAME = null;


        try {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:866:2: (iv_ruleQUALIFIED_NAME= ruleQUALIFIED_NAME EOF )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:867:2: iv_ruleQUALIFIED_NAME= ruleQUALIFIED_NAME EOF
            {
             newCompositeNode(grammarAccess.getQUALIFIED_NAMERule()); 
            pushFollow(FOLLOW_ruleQUALIFIED_NAME_in_entryRuleQUALIFIED_NAME1976);
            iv_ruleQUALIFIED_NAME=ruleQUALIFIED_NAME();

            state._fsp--;

             current =iv_ruleQUALIFIED_NAME.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQUALIFIED_NAME1987); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQUALIFIED_NAME"


    // $ANTLR start "ruleQUALIFIED_NAME"
    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:874:1: ruleQUALIFIED_NAME returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleQUALIFIED_NAME() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;

         enterRule(); 
            
        try {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:877:28: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:878:1: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:878:1: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:878:6: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleQUALIFIED_NAME2027); 

            		current.merge(this_ID_0);
                
             
                newLeafNode(this_ID_0, grammarAccess.getQUALIFIED_NAMEAccess().getIDTerminalRuleCall_0()); 
                
            // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:885:1: (kw= '.' this_ID_2= RULE_ID )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==25) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // ../co.edu.javeriana.Cmctltemplate/src-gen/co/edu/javeriana/parser/antlr/internal/InternalCmctltemplate.g:886:2: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,25,FOLLOW_25_in_ruleQUALIFIED_NAME2046); 

            	            current.merge(kw);
            	            newLeafNode(kw, grammarAccess.getQUALIFIED_NAMEAccess().getFullStopKeyword_1_0()); 
            	        
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleQUALIFIED_NAME2061); 

            	    		current.merge(this_ID_2);
            	        
            	     
            	        newLeafNode(this_ID_2, grammarAccess.getQUALIFIED_NAMEAccess().getIDTerminalRuleCall_1_1()); 
            	        

            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQUALIFIED_NAME"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleTemplate_in_entryRuleTemplate75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTemplate85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleImport_in_ruleTemplate131 = new BitSet(new long[]{0x0000000000005002L});
    public static final BitSet FOLLOW_ruleTemplateFunction_in_ruleTemplate153 = new BitSet(new long[]{0x0000000000004002L});
    public static final BitSet FOLLOW_ruleImport_in_entryRuleImport190 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleImport200 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_ruleImport237 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleQualifiedNameWithWildcard_in_ruleImport258 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_ruleImport270 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTemplateFunction_in_entryRuleTemplateFunction306 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTemplateFunction316 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_ruleTemplateFunction353 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleTemplateFunction370 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_ruleTemplateFunction387 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleParameter_in_ruleTemplateFunction408 = new BitSet(new long[]{0x0000000000030000L});
    public static final BitSet FOLLOW_16_in_ruleTemplateFunction421 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleParameter_in_ruleTemplateFunction442 = new BitSet(new long[]{0x0000000000030000L});
    public static final BitSet FOLLOW_17_in_ruleTemplateFunction456 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_ruleTemplateFunction468 = new BitSet(new long[]{0x0000000000180030L});
    public static final BitSet FOLLOW_ruleStatement_in_ruleTemplateFunction489 = new BitSet(new long[]{0x0000000000180030L});
    public static final BitSet FOLLOW_19_in_ruleTemplateFunction502 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStatement_in_entryRuleStatement538 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleStatement548 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_CHARACTER_in_ruleStatement594 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleStatement611 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTemplateStatement_in_ruleStatement638 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTemplateReference_in_ruleStatement665 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTemplateStatement_in_entryRuleTemplateStatement700 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTemplateStatement710 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIfTemplate_in_ruleTemplateStatement757 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleForTemplate_in_ruleTemplateStatement784 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIfTemplate_in_entryRuleIfTemplate819 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIfTemplate829 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_ruleIfTemplate875 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_ruleIfTemplate887 = new BitSet(new long[]{0x000000000C000000L});
    public static final BitSet FOLLOW_ruleBoolValue_in_ruleIfTemplate908 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_ruleIfTemplate920 = new BitSet(new long[]{0x0000000000500030L});
    public static final BitSet FOLLOW_ruleStatement_in_ruleIfTemplate941 = new BitSet(new long[]{0x0000000000500030L});
    public static final BitSet FOLLOW_22_in_ruleIfTemplate954 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleForTemplate_in_entryRuleForTemplate990 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleForTemplate1000 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_ruleForTemplate1046 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_23_in_ruleForTemplate1058 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_ruleForTemplate1070 = new BitSet(new long[]{0x0000000001100030L});
    public static final BitSet FOLLOW_ruleStatement_in_ruleForTemplate1091 = new BitSet(new long[]{0x0000000001100030L});
    public static final BitSet FOLLOW_24_in_ruleForTemplate1104 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTemplateReference_in_entryRuleTemplateReference1140 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTemplateReference1150 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_ruleTemplateReference1193 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleReference_in_ruleTemplateReference1227 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_ruleTemplateReference1239 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleETypedElement_in_entryRuleETypedElement1277 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleETypedElement1287 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleParameter_in_ruleETypedElement1333 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleParameter_in_entryRuleParameter1367 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleParameter1377 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQUALIFIED_NAME_in_ruleParameter1425 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleParameter1442 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleReference_in_entryRuleReference1483 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleReference1493 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleReference1538 = new BitSet(new long[]{0x0000000002000002L});
    public static final BitSet FOLLOW_ruleReferenceTail_in_ruleReference1559 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleReferenceTail_in_entryRuleReferenceTail1596 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleReferenceTail1606 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_ruleReferenceTail1643 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleReferenceTail1663 = new BitSet(new long[]{0x0000000002000002L});
    public static final BitSet FOLLOW_ruleReferenceTail_in_ruleReferenceTail1684 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBoolValue_in_entryRuleBoolValue1721 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBoolValue1731 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_ruleBoolValue1775 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_ruleBoolValue1804 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedNameWithWildcard_in_entryRuleQualifiedNameWithWildcard1856 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQualifiedNameWithWildcard1867 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQUALIFIED_NAME_in_ruleQualifiedNameWithWildcard1914 = new BitSet(new long[]{0x0000000010000002L});
    public static final BitSet FOLLOW_28_in_ruleQualifiedNameWithWildcard1933 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQUALIFIED_NAME_in_entryRuleQUALIFIED_NAME1976 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQUALIFIED_NAME1987 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleQUALIFIED_NAME2027 = new BitSet(new long[]{0x0000000002000002L});
    public static final BitSet FOLLOW_25_in_ruleQUALIFIED_NAME2046 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleQUALIFIED_NAME2061 = new BitSet(new long[]{0x0000000002000002L});

}