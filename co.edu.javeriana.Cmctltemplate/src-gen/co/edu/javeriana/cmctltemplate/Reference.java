/**
 */
package co.edu.javeriana.cmctltemplate;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.ETypedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link co.edu.javeriana.cmctltemplate.Reference#getReferencedElement <em>Referenced Element</em>}</li>
 *   <li>{@link co.edu.javeriana.cmctltemplate.Reference#getTail <em>Tail</em>}</li>
 * </ul>
 *
 * @see co.edu.javeriana.cmctltemplate.CmctltemplatePackage#getReference()
 * @model
 * @generated
 */
public interface Reference extends EObject
{
  /**
   * Returns the value of the '<em><b>Referenced Element</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Referenced Element</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Referenced Element</em>' reference.
   * @see #setReferencedElement(ETypedElement)
   * @see co.edu.javeriana.cmctltemplate.CmctltemplatePackage#getReference_ReferencedElement()
   * @model
   * @generated
   */
  ETypedElement getReferencedElement();

  /**
   * Sets the value of the '{@link co.edu.javeriana.cmctltemplate.Reference#getReferencedElement <em>Referenced Element</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Referenced Element</em>' reference.
   * @see #getReferencedElement()
   * @generated
   */
  void setReferencedElement(ETypedElement value);

  /**
   * Returns the value of the '<em><b>Tail</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Tail</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Tail</em>' containment reference.
   * @see #setTail(Reference)
   * @see co.edu.javeriana.cmctltemplate.CmctltemplatePackage#getReference_Tail()
   * @model containment="true"
   * @generated
   */
  Reference getTail();

  /**
   * Sets the value of the '{@link co.edu.javeriana.cmctltemplate.Reference#getTail <em>Tail</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Tail</em>' containment reference.
   * @see #getTail()
   * @generated
   */
  void setTail(Reference value);

} // Reference
