/**
 */
package co.edu.javeriana.cmctltemplate;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Template Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link co.edu.javeriana.cmctltemplate.TemplateStatement#getStatements <em>Statements</em>}</li>
 * </ul>
 *
 * @see co.edu.javeriana.cmctltemplate.CmctltemplatePackage#getTemplateStatement()
 * @model
 * @generated
 */
public interface TemplateStatement extends Statement
{
  /**
   * Returns the value of the '<em><b>Statements</b></em>' containment reference list.
   * The list contents are of type {@link co.edu.javeriana.cmctltemplate.Statement}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Statements</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Statements</em>' containment reference list.
   * @see co.edu.javeriana.cmctltemplate.CmctltemplatePackage#getTemplateStatement_Statements()
   * @model containment="true"
   * @generated
   */
  EList<Statement> getStatements();

} // TemplateStatement
