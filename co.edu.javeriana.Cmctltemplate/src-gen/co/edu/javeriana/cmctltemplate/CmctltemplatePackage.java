/**
 */
package co.edu.javeriana.cmctltemplate;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see co.edu.javeriana.cmctltemplate.CmctltemplateFactory
 * @model kind="package"
 * @generated
 */
public interface CmctltemplatePackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "cmctltemplate";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.edu.co/javeriana/Cmctltemplate";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "cmctltemplate";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  CmctltemplatePackage eINSTANCE = co.edu.javeriana.cmctltemplate.impl.CmctltemplatePackageImpl.init();

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctltemplate.impl.TemplateImpl <em>Template</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctltemplate.impl.TemplateImpl
   * @see co.edu.javeriana.cmctltemplate.impl.CmctltemplatePackageImpl#getTemplate()
   * @generated
   */
  int TEMPLATE = 0;

  /**
   * The feature id for the '<em><b>Imports</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE__IMPORTS = 0;

  /**
   * The feature id for the '<em><b>Template Functions</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE__TEMPLATE_FUNCTIONS = 1;

  /**
   * The number of structural features of the '<em>Template</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctltemplate.impl.ImportImpl <em>Import</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctltemplate.impl.ImportImpl
   * @see co.edu.javeriana.cmctltemplate.impl.CmctltemplatePackageImpl#getImport()
   * @generated
   */
  int IMPORT = 1;

  /**
   * The feature id for the '<em><b>Imported Namespace</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT__IMPORTED_NAMESPACE = 0;

  /**
   * The number of structural features of the '<em>Import</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctltemplate.impl.TemplateFunctionImpl <em>Template Function</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctltemplate.impl.TemplateFunctionImpl
   * @see co.edu.javeriana.cmctltemplate.impl.CmctltemplatePackageImpl#getTemplateFunction()
   * @generated
   */
  int TEMPLATE_FUNCTION = 2;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_FUNCTION__NAME = 0;

  /**
   * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_FUNCTION__PARAMETERS = 1;

  /**
   * The feature id for the '<em><b>Statements</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_FUNCTION__STATEMENTS = 2;

  /**
   * The number of structural features of the '<em>Template Function</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_FUNCTION_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctltemplate.impl.StatementImpl <em>Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctltemplate.impl.StatementImpl
   * @see co.edu.javeriana.cmctltemplate.impl.CmctltemplatePackageImpl#getStatement()
   * @generated
   */
  int STATEMENT = 3;

  /**
   * The number of structural features of the '<em>Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctltemplate.impl.TemplateStatementImpl <em>Template Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctltemplate.impl.TemplateStatementImpl
   * @see co.edu.javeriana.cmctltemplate.impl.CmctltemplatePackageImpl#getTemplateStatement()
   * @generated
   */
  int TEMPLATE_STATEMENT = 4;

  /**
   * The feature id for the '<em><b>Statements</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_STATEMENT__STATEMENTS = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Template Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctltemplate.impl.IfTemplateImpl <em>If Template</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctltemplate.impl.IfTemplateImpl
   * @see co.edu.javeriana.cmctltemplate.impl.CmctltemplatePackageImpl#getIfTemplate()
   * @generated
   */
  int IF_TEMPLATE = 5;

  /**
   * The feature id for the '<em><b>Statements</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_TEMPLATE__STATEMENTS = TEMPLATE_STATEMENT__STATEMENTS;

  /**
   * The feature id for the '<em><b>Template Condition</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_TEMPLATE__TEMPLATE_CONDITION = TEMPLATE_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>If Template</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_TEMPLATE_FEATURE_COUNT = TEMPLATE_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctltemplate.impl.ForTemplateImpl <em>For Template</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctltemplate.impl.ForTemplateImpl
   * @see co.edu.javeriana.cmctltemplate.impl.CmctltemplatePackageImpl#getForTemplate()
   * @generated
   */
  int FOR_TEMPLATE = 6;

  /**
   * The feature id for the '<em><b>Statements</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOR_TEMPLATE__STATEMENTS = TEMPLATE_STATEMENT__STATEMENTS;

  /**
   * The number of structural features of the '<em>For Template</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOR_TEMPLATE_FEATURE_COUNT = TEMPLATE_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctltemplate.impl.TemplateReferenceImpl <em>Template Reference</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctltemplate.impl.TemplateReferenceImpl
   * @see co.edu.javeriana.cmctltemplate.impl.CmctltemplatePackageImpl#getTemplateReference()
   * @generated
   */
  int TEMPLATE_REFERENCE = 7;

  /**
   * The feature id for the '<em><b>Call</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_REFERENCE__CALL = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_REFERENCE__TYPE = STATEMENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Template Reference</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_REFERENCE_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctltemplate.impl.ParameterImpl <em>Parameter</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctltemplate.impl.ParameterImpl
   * @see co.edu.javeriana.cmctltemplate.impl.CmctltemplatePackageImpl#getParameter()
   * @generated
   */
  int PARAMETER = 8;

  /**
   * The feature id for the '<em><b>EAnnotations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER__EANNOTATIONS = EcorePackage.ETYPED_ELEMENT__EANNOTATIONS;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER__NAME = EcorePackage.ETYPED_ELEMENT__NAME;

  /**
   * The feature id for the '<em><b>Ordered</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER__ORDERED = EcorePackage.ETYPED_ELEMENT__ORDERED;

  /**
   * The feature id for the '<em><b>Unique</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER__UNIQUE = EcorePackage.ETYPED_ELEMENT__UNIQUE;

  /**
   * The feature id for the '<em><b>Lower Bound</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER__LOWER_BOUND = EcorePackage.ETYPED_ELEMENT__LOWER_BOUND;

  /**
   * The feature id for the '<em><b>Upper Bound</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER__UPPER_BOUND = EcorePackage.ETYPED_ELEMENT__UPPER_BOUND;

  /**
   * The feature id for the '<em><b>Many</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER__MANY = EcorePackage.ETYPED_ELEMENT__MANY;

  /**
   * The feature id for the '<em><b>Required</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER__REQUIRED = EcorePackage.ETYPED_ELEMENT__REQUIRED;

  /**
   * The feature id for the '<em><b>EType</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER__ETYPE = EcorePackage.ETYPED_ELEMENT__ETYPE;

  /**
   * The feature id for the '<em><b>EGeneric Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER__EGENERIC_TYPE = EcorePackage.ETYPED_ELEMENT__EGENERIC_TYPE;

  /**
   * The number of structural features of the '<em>Parameter</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER_FEATURE_COUNT = EcorePackage.ETYPED_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctltemplate.impl.ReferenceImpl <em>Reference</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctltemplate.impl.ReferenceImpl
   * @see co.edu.javeriana.cmctltemplate.impl.CmctltemplatePackageImpl#getReference()
   * @generated
   */
  int REFERENCE = 9;

  /**
   * The feature id for the '<em><b>Referenced Element</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REFERENCE__REFERENCED_ELEMENT = 0;

  /**
   * The feature id for the '<em><b>Tail</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REFERENCE__TAIL = 1;

  /**
   * The number of structural features of the '<em>Reference</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REFERENCE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctltemplate.impl.BoolValueImpl <em>Bool Value</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctltemplate.impl.BoolValueImpl
   * @see co.edu.javeriana.cmctltemplate.impl.CmctltemplatePackageImpl#getBoolValue()
   * @generated
   */
  int BOOL_VALUE = 10;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOL_VALUE__TYPE = 0;

  /**
   * The number of structural features of the '<em>Bool Value</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOL_VALUE_FEATURE_COUNT = 1;


  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctltemplate.Template <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Template</em>'.
   * @see co.edu.javeriana.cmctltemplate.Template
   * @generated
   */
  EClass getTemplate();

  /**
   * Returns the meta object for the containment reference list '{@link co.edu.javeriana.cmctltemplate.Template#getImports <em>Imports</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Imports</em>'.
   * @see co.edu.javeriana.cmctltemplate.Template#getImports()
   * @see #getTemplate()
   * @generated
   */
  EReference getTemplate_Imports();

  /**
   * Returns the meta object for the containment reference list '{@link co.edu.javeriana.cmctltemplate.Template#getTemplateFunctions <em>Template Functions</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Template Functions</em>'.
   * @see co.edu.javeriana.cmctltemplate.Template#getTemplateFunctions()
   * @see #getTemplate()
   * @generated
   */
  EReference getTemplate_TemplateFunctions();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctltemplate.Import <em>Import</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Import</em>'.
   * @see co.edu.javeriana.cmctltemplate.Import
   * @generated
   */
  EClass getImport();

  /**
   * Returns the meta object for the attribute '{@link co.edu.javeriana.cmctltemplate.Import#getImportedNamespace <em>Imported Namespace</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Imported Namespace</em>'.
   * @see co.edu.javeriana.cmctltemplate.Import#getImportedNamespace()
   * @see #getImport()
   * @generated
   */
  EAttribute getImport_ImportedNamespace();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctltemplate.TemplateFunction <em>Template Function</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Template Function</em>'.
   * @see co.edu.javeriana.cmctltemplate.TemplateFunction
   * @generated
   */
  EClass getTemplateFunction();

  /**
   * Returns the meta object for the attribute '{@link co.edu.javeriana.cmctltemplate.TemplateFunction#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see co.edu.javeriana.cmctltemplate.TemplateFunction#getName()
   * @see #getTemplateFunction()
   * @generated
   */
  EAttribute getTemplateFunction_Name();

  /**
   * Returns the meta object for the containment reference list '{@link co.edu.javeriana.cmctltemplate.TemplateFunction#getParameters <em>Parameters</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Parameters</em>'.
   * @see co.edu.javeriana.cmctltemplate.TemplateFunction#getParameters()
   * @see #getTemplateFunction()
   * @generated
   */
  EReference getTemplateFunction_Parameters();

  /**
   * Returns the meta object for the containment reference list '{@link co.edu.javeriana.cmctltemplate.TemplateFunction#getStatements <em>Statements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Statements</em>'.
   * @see co.edu.javeriana.cmctltemplate.TemplateFunction#getStatements()
   * @see #getTemplateFunction()
   * @generated
   */
  EReference getTemplateFunction_Statements();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctltemplate.Statement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Statement</em>'.
   * @see co.edu.javeriana.cmctltemplate.Statement
   * @generated
   */
  EClass getStatement();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctltemplate.TemplateStatement <em>Template Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Template Statement</em>'.
   * @see co.edu.javeriana.cmctltemplate.TemplateStatement
   * @generated
   */
  EClass getTemplateStatement();

  /**
   * Returns the meta object for the containment reference list '{@link co.edu.javeriana.cmctltemplate.TemplateStatement#getStatements <em>Statements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Statements</em>'.
   * @see co.edu.javeriana.cmctltemplate.TemplateStatement#getStatements()
   * @see #getTemplateStatement()
   * @generated
   */
  EReference getTemplateStatement_Statements();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctltemplate.IfTemplate <em>If Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>If Template</em>'.
   * @see co.edu.javeriana.cmctltemplate.IfTemplate
   * @generated
   */
  EClass getIfTemplate();

  /**
   * Returns the meta object for the containment reference list '{@link co.edu.javeriana.cmctltemplate.IfTemplate#getTemplateCondition <em>Template Condition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Template Condition</em>'.
   * @see co.edu.javeriana.cmctltemplate.IfTemplate#getTemplateCondition()
   * @see #getIfTemplate()
   * @generated
   */
  EReference getIfTemplate_TemplateCondition();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctltemplate.ForTemplate <em>For Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>For Template</em>'.
   * @see co.edu.javeriana.cmctltemplate.ForTemplate
   * @generated
   */
  EClass getForTemplate();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctltemplate.TemplateReference <em>Template Reference</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Template Reference</em>'.
   * @see co.edu.javeriana.cmctltemplate.TemplateReference
   * @generated
   */
  EClass getTemplateReference();

  /**
   * Returns the meta object for the attribute '{@link co.edu.javeriana.cmctltemplate.TemplateReference#getCall <em>Call</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Call</em>'.
   * @see co.edu.javeriana.cmctltemplate.TemplateReference#getCall()
   * @see #getTemplateReference()
   * @generated
   */
  EAttribute getTemplateReference_Call();

  /**
   * Returns the meta object for the containment reference '{@link co.edu.javeriana.cmctltemplate.TemplateReference#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see co.edu.javeriana.cmctltemplate.TemplateReference#getType()
   * @see #getTemplateReference()
   * @generated
   */
  EReference getTemplateReference_Type();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctltemplate.Parameter <em>Parameter</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Parameter</em>'.
   * @see co.edu.javeriana.cmctltemplate.Parameter
   * @generated
   */
  EClass getParameter();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctltemplate.Reference <em>Reference</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Reference</em>'.
   * @see co.edu.javeriana.cmctltemplate.Reference
   * @generated
   */
  EClass getReference();

  /**
   * Returns the meta object for the reference '{@link co.edu.javeriana.cmctltemplate.Reference#getReferencedElement <em>Referenced Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Referenced Element</em>'.
   * @see co.edu.javeriana.cmctltemplate.Reference#getReferencedElement()
   * @see #getReference()
   * @generated
   */
  EReference getReference_ReferencedElement();

  /**
   * Returns the meta object for the containment reference '{@link co.edu.javeriana.cmctltemplate.Reference#getTail <em>Tail</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Tail</em>'.
   * @see co.edu.javeriana.cmctltemplate.Reference#getTail()
   * @see #getReference()
   * @generated
   */
  EReference getReference_Tail();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctltemplate.BoolValue <em>Bool Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Bool Value</em>'.
   * @see co.edu.javeriana.cmctltemplate.BoolValue
   * @generated
   */
  EClass getBoolValue();

  /**
   * Returns the meta object for the attribute '{@link co.edu.javeriana.cmctltemplate.BoolValue#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see co.edu.javeriana.cmctltemplate.BoolValue#getType()
   * @see #getBoolValue()
   * @generated
   */
  EAttribute getBoolValue_Type();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  CmctltemplateFactory getCmctltemplateFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctltemplate.impl.TemplateImpl <em>Template</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctltemplate.impl.TemplateImpl
     * @see co.edu.javeriana.cmctltemplate.impl.CmctltemplatePackageImpl#getTemplate()
     * @generated
     */
    EClass TEMPLATE = eINSTANCE.getTemplate();

    /**
     * The meta object literal for the '<em><b>Imports</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TEMPLATE__IMPORTS = eINSTANCE.getTemplate_Imports();

    /**
     * The meta object literal for the '<em><b>Template Functions</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TEMPLATE__TEMPLATE_FUNCTIONS = eINSTANCE.getTemplate_TemplateFunctions();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctltemplate.impl.ImportImpl <em>Import</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctltemplate.impl.ImportImpl
     * @see co.edu.javeriana.cmctltemplate.impl.CmctltemplatePackageImpl#getImport()
     * @generated
     */
    EClass IMPORT = eINSTANCE.getImport();

    /**
     * The meta object literal for the '<em><b>Imported Namespace</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute IMPORT__IMPORTED_NAMESPACE = eINSTANCE.getImport_ImportedNamespace();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctltemplate.impl.TemplateFunctionImpl <em>Template Function</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctltemplate.impl.TemplateFunctionImpl
     * @see co.edu.javeriana.cmctltemplate.impl.CmctltemplatePackageImpl#getTemplateFunction()
     * @generated
     */
    EClass TEMPLATE_FUNCTION = eINSTANCE.getTemplateFunction();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TEMPLATE_FUNCTION__NAME = eINSTANCE.getTemplateFunction_Name();

    /**
     * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TEMPLATE_FUNCTION__PARAMETERS = eINSTANCE.getTemplateFunction_Parameters();

    /**
     * The meta object literal for the '<em><b>Statements</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TEMPLATE_FUNCTION__STATEMENTS = eINSTANCE.getTemplateFunction_Statements();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctltemplate.impl.StatementImpl <em>Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctltemplate.impl.StatementImpl
     * @see co.edu.javeriana.cmctltemplate.impl.CmctltemplatePackageImpl#getStatement()
     * @generated
     */
    EClass STATEMENT = eINSTANCE.getStatement();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctltemplate.impl.TemplateStatementImpl <em>Template Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctltemplate.impl.TemplateStatementImpl
     * @see co.edu.javeriana.cmctltemplate.impl.CmctltemplatePackageImpl#getTemplateStatement()
     * @generated
     */
    EClass TEMPLATE_STATEMENT = eINSTANCE.getTemplateStatement();

    /**
     * The meta object literal for the '<em><b>Statements</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TEMPLATE_STATEMENT__STATEMENTS = eINSTANCE.getTemplateStatement_Statements();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctltemplate.impl.IfTemplateImpl <em>If Template</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctltemplate.impl.IfTemplateImpl
     * @see co.edu.javeriana.cmctltemplate.impl.CmctltemplatePackageImpl#getIfTemplate()
     * @generated
     */
    EClass IF_TEMPLATE = eINSTANCE.getIfTemplate();

    /**
     * The meta object literal for the '<em><b>Template Condition</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IF_TEMPLATE__TEMPLATE_CONDITION = eINSTANCE.getIfTemplate_TemplateCondition();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctltemplate.impl.ForTemplateImpl <em>For Template</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctltemplate.impl.ForTemplateImpl
     * @see co.edu.javeriana.cmctltemplate.impl.CmctltemplatePackageImpl#getForTemplate()
     * @generated
     */
    EClass FOR_TEMPLATE = eINSTANCE.getForTemplate();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctltemplate.impl.TemplateReferenceImpl <em>Template Reference</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctltemplate.impl.TemplateReferenceImpl
     * @see co.edu.javeriana.cmctltemplate.impl.CmctltemplatePackageImpl#getTemplateReference()
     * @generated
     */
    EClass TEMPLATE_REFERENCE = eINSTANCE.getTemplateReference();

    /**
     * The meta object literal for the '<em><b>Call</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TEMPLATE_REFERENCE__CALL = eINSTANCE.getTemplateReference_Call();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TEMPLATE_REFERENCE__TYPE = eINSTANCE.getTemplateReference_Type();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctltemplate.impl.ParameterImpl <em>Parameter</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctltemplate.impl.ParameterImpl
     * @see co.edu.javeriana.cmctltemplate.impl.CmctltemplatePackageImpl#getParameter()
     * @generated
     */
    EClass PARAMETER = eINSTANCE.getParameter();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctltemplate.impl.ReferenceImpl <em>Reference</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctltemplate.impl.ReferenceImpl
     * @see co.edu.javeriana.cmctltemplate.impl.CmctltemplatePackageImpl#getReference()
     * @generated
     */
    EClass REFERENCE = eINSTANCE.getReference();

    /**
     * The meta object literal for the '<em><b>Referenced Element</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference REFERENCE__REFERENCED_ELEMENT = eINSTANCE.getReference_ReferencedElement();

    /**
     * The meta object literal for the '<em><b>Tail</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference REFERENCE__TAIL = eINSTANCE.getReference_Tail();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctltemplate.impl.BoolValueImpl <em>Bool Value</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctltemplate.impl.BoolValueImpl
     * @see co.edu.javeriana.cmctltemplate.impl.CmctltemplatePackageImpl#getBoolValue()
     * @generated
     */
    EClass BOOL_VALUE = eINSTANCE.getBoolValue();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BOOL_VALUE__TYPE = eINSTANCE.getBoolValue_Type();

  }

} //CmctltemplatePackage
