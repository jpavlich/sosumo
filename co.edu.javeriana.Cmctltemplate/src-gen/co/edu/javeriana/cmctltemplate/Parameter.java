/**
 */
package co.edu.javeriana.cmctltemplate;

import org.eclipse.emf.ecore.ETypedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see co.edu.javeriana.cmctltemplate.CmctltemplatePackage#getParameter()
 * @model
 * @generated
 */
public interface Parameter extends ETypedElement
{
} // Parameter
