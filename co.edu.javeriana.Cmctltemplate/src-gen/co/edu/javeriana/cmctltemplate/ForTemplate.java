/**
 */
package co.edu.javeriana.cmctltemplate;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>For Template</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see co.edu.javeriana.cmctltemplate.CmctltemplatePackage#getForTemplate()
 * @model
 * @generated
 */
public interface ForTemplate extends TemplateStatement
{
} // ForTemplate
