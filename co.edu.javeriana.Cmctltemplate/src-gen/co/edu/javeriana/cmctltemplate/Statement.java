/**
 */
package co.edu.javeriana.cmctltemplate;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see co.edu.javeriana.cmctltemplate.CmctltemplatePackage#getStatement()
 * @model
 * @generated
 */
public interface Statement extends EObject
{
} // Statement
