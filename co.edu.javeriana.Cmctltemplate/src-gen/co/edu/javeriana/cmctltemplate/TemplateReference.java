/**
 */
package co.edu.javeriana.cmctltemplate;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Template Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link co.edu.javeriana.cmctltemplate.TemplateReference#getCall <em>Call</em>}</li>
 *   <li>{@link co.edu.javeriana.cmctltemplate.TemplateReference#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see co.edu.javeriana.cmctltemplate.CmctltemplatePackage#getTemplateReference()
 * @model
 * @generated
 */
public interface TemplateReference extends Statement
{
  /**
   * Returns the value of the '<em><b>Call</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Call</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Call</em>' attribute.
   * @see #setCall(String)
   * @see co.edu.javeriana.cmctltemplate.CmctltemplatePackage#getTemplateReference_Call()
   * @model
   * @generated
   */
  String getCall();

  /**
   * Sets the value of the '{@link co.edu.javeriana.cmctltemplate.TemplateReference#getCall <em>Call</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Call</em>' attribute.
   * @see #getCall()
   * @generated
   */
  void setCall(String value);

  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(Reference)
   * @see co.edu.javeriana.cmctltemplate.CmctltemplatePackage#getTemplateReference_Type()
   * @model containment="true"
   * @generated
   */
  Reference getType();

  /**
   * Sets the value of the '{@link co.edu.javeriana.cmctltemplate.TemplateReference#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(Reference value);

} // TemplateReference
