/**
 */
package co.edu.javeriana.cmctltemplate;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Template Function</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link co.edu.javeriana.cmctltemplate.TemplateFunction#getName <em>Name</em>}</li>
 *   <li>{@link co.edu.javeriana.cmctltemplate.TemplateFunction#getParameters <em>Parameters</em>}</li>
 *   <li>{@link co.edu.javeriana.cmctltemplate.TemplateFunction#getStatements <em>Statements</em>}</li>
 * </ul>
 *
 * @see co.edu.javeriana.cmctltemplate.CmctltemplatePackage#getTemplateFunction()
 * @model
 * @generated
 */
public interface TemplateFunction extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see co.edu.javeriana.cmctltemplate.CmctltemplatePackage#getTemplateFunction_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link co.edu.javeriana.cmctltemplate.TemplateFunction#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Parameters</b></em>' containment reference list.
   * The list contents are of type {@link co.edu.javeriana.cmctltemplate.Parameter}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Parameters</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Parameters</em>' containment reference list.
   * @see co.edu.javeriana.cmctltemplate.CmctltemplatePackage#getTemplateFunction_Parameters()
   * @model containment="true"
   * @generated
   */
  EList<Parameter> getParameters();

  /**
   * Returns the value of the '<em><b>Statements</b></em>' containment reference list.
   * The list contents are of type {@link co.edu.javeriana.cmctltemplate.Statement}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Statements</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Statements</em>' containment reference list.
   * @see co.edu.javeriana.cmctltemplate.CmctltemplatePackage#getTemplateFunction_Statements()
   * @model containment="true"
   * @generated
   */
  EList<Statement> getStatements();

} // TemplateFunction
