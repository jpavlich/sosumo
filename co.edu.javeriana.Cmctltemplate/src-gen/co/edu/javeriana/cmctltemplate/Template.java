/**
 */
package co.edu.javeriana.cmctltemplate;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Template</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link co.edu.javeriana.cmctltemplate.Template#getImports <em>Imports</em>}</li>
 *   <li>{@link co.edu.javeriana.cmctltemplate.Template#getTemplateFunctions <em>Template Functions</em>}</li>
 * </ul>
 *
 * @see co.edu.javeriana.cmctltemplate.CmctltemplatePackage#getTemplate()
 * @model
 * @generated
 */
public interface Template extends EObject
{
  /**
   * Returns the value of the '<em><b>Imports</b></em>' containment reference list.
   * The list contents are of type {@link co.edu.javeriana.cmctltemplate.Import}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Imports</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Imports</em>' containment reference list.
   * @see co.edu.javeriana.cmctltemplate.CmctltemplatePackage#getTemplate_Imports()
   * @model containment="true"
   * @generated
   */
  EList<Import> getImports();

  /**
   * Returns the value of the '<em><b>Template Functions</b></em>' containment reference list.
   * The list contents are of type {@link co.edu.javeriana.cmctltemplate.TemplateFunction}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Template Functions</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Template Functions</em>' containment reference list.
   * @see co.edu.javeriana.cmctltemplate.CmctltemplatePackage#getTemplate_TemplateFunctions()
   * @model containment="true"
   * @generated
   */
  EList<TemplateFunction> getTemplateFunctions();

} // Template
