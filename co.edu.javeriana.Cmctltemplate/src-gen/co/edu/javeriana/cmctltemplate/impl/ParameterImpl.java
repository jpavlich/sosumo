/**
 */
package co.edu.javeriana.cmctltemplate.impl;

import co.edu.javeriana.cmctltemplate.CmctltemplatePackage;
import co.edu.javeriana.cmctltemplate.Parameter;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ETypedElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ParameterImpl extends ETypedElementImpl implements Parameter
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ParameterImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return CmctltemplatePackage.Literals.PARAMETER;
  }

} //ParameterImpl
