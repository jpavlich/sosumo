/**
 */
package co.edu.javeriana.cmctltemplate.impl;

import co.edu.javeriana.cmctltemplate.BoolValue;
import co.edu.javeriana.cmctltemplate.CmctltemplatePackage;
import co.edu.javeriana.cmctltemplate.IfTemplate;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>If Template</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link co.edu.javeriana.cmctltemplate.impl.IfTemplateImpl#getTemplateCondition <em>Template Condition</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IfTemplateImpl extends TemplateStatementImpl implements IfTemplate
{
  /**
   * The cached value of the '{@link #getTemplateCondition() <em>Template Condition</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTemplateCondition()
   * @generated
   * @ordered
   */
  protected EList<BoolValue> templateCondition;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected IfTemplateImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return CmctltemplatePackage.Literals.IF_TEMPLATE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<BoolValue> getTemplateCondition()
  {
    if (templateCondition == null)
    {
      templateCondition = new EObjectContainmentEList<BoolValue>(BoolValue.class, this, CmctltemplatePackage.IF_TEMPLATE__TEMPLATE_CONDITION);
    }
    return templateCondition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case CmctltemplatePackage.IF_TEMPLATE__TEMPLATE_CONDITION:
        return ((InternalEList<?>)getTemplateCondition()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case CmctltemplatePackage.IF_TEMPLATE__TEMPLATE_CONDITION:
        return getTemplateCondition();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case CmctltemplatePackage.IF_TEMPLATE__TEMPLATE_CONDITION:
        getTemplateCondition().clear();
        getTemplateCondition().addAll((Collection<? extends BoolValue>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case CmctltemplatePackage.IF_TEMPLATE__TEMPLATE_CONDITION:
        getTemplateCondition().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case CmctltemplatePackage.IF_TEMPLATE__TEMPLATE_CONDITION:
        return templateCondition != null && !templateCondition.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //IfTemplateImpl
