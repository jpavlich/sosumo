/**
 */
package co.edu.javeriana.cmctltemplate.impl;

import co.edu.javeriana.cmctltemplate.CmctltemplatePackage;
import co.edu.javeriana.cmctltemplate.Import;
import co.edu.javeriana.cmctltemplate.Template;
import co.edu.javeriana.cmctltemplate.TemplateFunction;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Template</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link co.edu.javeriana.cmctltemplate.impl.TemplateImpl#getImports <em>Imports</em>}</li>
 *   <li>{@link co.edu.javeriana.cmctltemplate.impl.TemplateImpl#getTemplateFunctions <em>Template Functions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TemplateImpl extends MinimalEObjectImpl.Container implements Template
{
  /**
   * The cached value of the '{@link #getImports() <em>Imports</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getImports()
   * @generated
   * @ordered
   */
  protected EList<Import> imports;

  /**
   * The cached value of the '{@link #getTemplateFunctions() <em>Template Functions</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTemplateFunctions()
   * @generated
   * @ordered
   */
  protected EList<TemplateFunction> templateFunctions;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TemplateImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return CmctltemplatePackage.Literals.TEMPLATE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Import> getImports()
  {
    if (imports == null)
    {
      imports = new EObjectContainmentEList<Import>(Import.class, this, CmctltemplatePackage.TEMPLATE__IMPORTS);
    }
    return imports;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TemplateFunction> getTemplateFunctions()
  {
    if (templateFunctions == null)
    {
      templateFunctions = new EObjectContainmentEList<TemplateFunction>(TemplateFunction.class, this, CmctltemplatePackage.TEMPLATE__TEMPLATE_FUNCTIONS);
    }
    return templateFunctions;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case CmctltemplatePackage.TEMPLATE__IMPORTS:
        return ((InternalEList<?>)getImports()).basicRemove(otherEnd, msgs);
      case CmctltemplatePackage.TEMPLATE__TEMPLATE_FUNCTIONS:
        return ((InternalEList<?>)getTemplateFunctions()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case CmctltemplatePackage.TEMPLATE__IMPORTS:
        return getImports();
      case CmctltemplatePackage.TEMPLATE__TEMPLATE_FUNCTIONS:
        return getTemplateFunctions();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case CmctltemplatePackage.TEMPLATE__IMPORTS:
        getImports().clear();
        getImports().addAll((Collection<? extends Import>)newValue);
        return;
      case CmctltemplatePackage.TEMPLATE__TEMPLATE_FUNCTIONS:
        getTemplateFunctions().clear();
        getTemplateFunctions().addAll((Collection<? extends TemplateFunction>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case CmctltemplatePackage.TEMPLATE__IMPORTS:
        getImports().clear();
        return;
      case CmctltemplatePackage.TEMPLATE__TEMPLATE_FUNCTIONS:
        getTemplateFunctions().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case CmctltemplatePackage.TEMPLATE__IMPORTS:
        return imports != null && !imports.isEmpty();
      case CmctltemplatePackage.TEMPLATE__TEMPLATE_FUNCTIONS:
        return templateFunctions != null && !templateFunctions.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //TemplateImpl
