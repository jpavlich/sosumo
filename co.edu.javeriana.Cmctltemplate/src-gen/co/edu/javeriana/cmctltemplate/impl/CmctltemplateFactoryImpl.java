/**
 */
package co.edu.javeriana.cmctltemplate.impl;

import co.edu.javeriana.cmctltemplate.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CmctltemplateFactoryImpl extends EFactoryImpl implements CmctltemplateFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static CmctltemplateFactory init()
  {
    try
    {
      CmctltemplateFactory theCmctltemplateFactory = (CmctltemplateFactory)EPackage.Registry.INSTANCE.getEFactory(CmctltemplatePackage.eNS_URI);
      if (theCmctltemplateFactory != null)
      {
        return theCmctltemplateFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new CmctltemplateFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CmctltemplateFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case CmctltemplatePackage.TEMPLATE: return createTemplate();
      case CmctltemplatePackage.IMPORT: return createImport();
      case CmctltemplatePackage.TEMPLATE_FUNCTION: return createTemplateFunction();
      case CmctltemplatePackage.STATEMENT: return createStatement();
      case CmctltemplatePackage.TEMPLATE_STATEMENT: return createTemplateStatement();
      case CmctltemplatePackage.IF_TEMPLATE: return createIfTemplate();
      case CmctltemplatePackage.FOR_TEMPLATE: return createForTemplate();
      case CmctltemplatePackage.TEMPLATE_REFERENCE: return createTemplateReference();
      case CmctltemplatePackage.PARAMETER: return createParameter();
      case CmctltemplatePackage.REFERENCE: return createReference();
      case CmctltemplatePackage.BOOL_VALUE: return createBoolValue();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Template createTemplate()
  {
    TemplateImpl template = new TemplateImpl();
    return template;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Import createImport()
  {
    ImportImpl import_ = new ImportImpl();
    return import_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TemplateFunction createTemplateFunction()
  {
    TemplateFunctionImpl templateFunction = new TemplateFunctionImpl();
    return templateFunction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Statement createStatement()
  {
    StatementImpl statement = new StatementImpl();
    return statement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TemplateStatement createTemplateStatement()
  {
    TemplateStatementImpl templateStatement = new TemplateStatementImpl();
    return templateStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IfTemplate createIfTemplate()
  {
    IfTemplateImpl ifTemplate = new IfTemplateImpl();
    return ifTemplate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ForTemplate createForTemplate()
  {
    ForTemplateImpl forTemplate = new ForTemplateImpl();
    return forTemplate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TemplateReference createTemplateReference()
  {
    TemplateReferenceImpl templateReference = new TemplateReferenceImpl();
    return templateReference;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Parameter createParameter()
  {
    ParameterImpl parameter = new ParameterImpl();
    return parameter;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Reference createReference()
  {
    ReferenceImpl reference = new ReferenceImpl();
    return reference;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BoolValue createBoolValue()
  {
    BoolValueImpl boolValue = new BoolValueImpl();
    return boolValue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CmctltemplatePackage getCmctltemplatePackage()
  {
    return (CmctltemplatePackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static CmctltemplatePackage getPackage()
  {
    return CmctltemplatePackage.eINSTANCE;
  }

} //CmctltemplateFactoryImpl
