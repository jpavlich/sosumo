/**
 */
package co.edu.javeriana.cmctljava;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link co.edu.javeriana.cmctljava.Reference#getReferencedElement <em>Referenced Element</em>}</li>
 *   <li>{@link co.edu.javeriana.cmctljava.Reference#getTail <em>Tail</em>}</li>
 * </ul>
 *
 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getReference()
 * @model
 * @generated
 */
public interface Reference extends Statement, Expression
{
	/**
	 * Returns the value of the '<em><b>Referenced Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Referenced Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Referenced Element</em>' reference.
	 * @see #setReferencedElement(TypedElement)
	 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getReference_ReferencedElement()
	 * @model
	 * @generated
	 */
	TypedElement getReferencedElement();

	/**
	 * Sets the value of the '{@link co.edu.javeriana.cmctljava.Reference#getReferencedElement <em>Referenced Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Referenced Element</em>' reference.
	 * @see #getReferencedElement()
	 * @generated
	 */
	void setReferencedElement(TypedElement value);

	/**
	 * Returns the value of the '<em><b>Tail</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tail</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tail</em>' containment reference.
	 * @see #setTail(Reference)
	 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getReference_Tail()
	 * @model containment="true"
	 * @generated
	 */
	Reference getTail();

	/**
	 * Sets the value of the '{@link co.edu.javeriana.cmctljava.Reference#getTail <em>Tail</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tail</em>' containment reference.
	 * @see #getTail()
	 * @generated
	 */
	void setTail(Reference value);

} // Reference
