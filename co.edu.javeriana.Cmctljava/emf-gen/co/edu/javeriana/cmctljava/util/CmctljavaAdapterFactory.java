/**
 */
package co.edu.javeriana.cmctljava.util;

import co.edu.javeriana.cmctljava.AbstractElement;
import co.edu.javeriana.cmctljava.Assignment;
import co.edu.javeriana.cmctljava.Attribute;
import co.edu.javeriana.cmctljava.BinaryOperator;
import co.edu.javeriana.cmctljava.BoolValue;
import co.edu.javeriana.cmctljava.ClassReference;
import co.edu.javeriana.cmctljava.CmctljavaPackage;
import co.edu.javeriana.cmctljava.Expression;
import co.edu.javeriana.cmctljava.Feature;
import co.edu.javeriana.cmctljava.For;
import co.edu.javeriana.cmctljava.If;
import co.edu.javeriana.cmctljava.Import;
import co.edu.javeriana.cmctljava.IntValue;
import co.edu.javeriana.cmctljava.Interface;
import co.edu.javeriana.cmctljava.LiteralValue;
import co.edu.javeriana.cmctljava.NewInstance;
import co.edu.javeriana.cmctljava.NullValue;
import co.edu.javeriana.cmctljava.Operation;
import co.edu.javeriana.cmctljava.OperationReference;
import co.edu.javeriana.cmctljava.OperationSignature;
import co.edu.javeriana.cmctljava.Parameter;
import co.edu.javeriana.cmctljava.Program;
import co.edu.javeriana.cmctljava.Reference;
import co.edu.javeriana.cmctljava.Statement;
import co.edu.javeriana.cmctljava.StringValue;
import co.edu.javeriana.cmctljava.Type;
import co.edu.javeriana.cmctljava.TypedElement;
import co.edu.javeriana.cmctljava.UnaryOperator;
import co.edu.javeriana.cmctljava.Variable;
import co.edu.javeriana.cmctljava.variableReference;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see co.edu.javeriana.cmctljava.CmctljavaPackage
 * @generated
 */
public class CmctljavaAdapterFactory extends AdapterFactoryImpl
{
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static CmctljavaPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CmctljavaAdapterFactory()
	{
		if (modelPackage == null)
		{
			modelPackage = CmctljavaPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object)
	{
		if (object == modelPackage)
		{
			return true;
		}
		if (object instanceof EObject)
		{
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CmctljavaSwitch<Adapter> modelSwitch =
		new CmctljavaSwitch<Adapter>()
		{
			@Override
			public Adapter caseProgram(Program object)
			{
				return createProgramAdapter();
			}
			@Override
			public Adapter casePackage(co.edu.javeriana.cmctljava.Package object)
			{
				return createPackageAdapter();
			}
			@Override
			public Adapter caseAbstractElement(AbstractElement object)
			{
				return createAbstractElementAdapter();
			}
			@Override
			public Adapter caseImport(Import object)
			{
				return createImportAdapter();
			}
			@Override
			public Adapter caseTypedElement(TypedElement object)
			{
				return createTypedElementAdapter();
			}
			@Override
			public Adapter caseClass(co.edu.javeriana.cmctljava.Class object)
			{
				return createClassAdapter();
			}
			@Override
			public Adapter caseInterface(Interface object)
			{
				return createInterfaceAdapter();
			}
			@Override
			public Adapter caseFeature(Feature object)
			{
				return createFeatureAdapter();
			}
			@Override
			public Adapter caseAttribute(Attribute object)
			{
				return createAttributeAdapter();
			}
			@Override
			public Adapter caseOperation(Operation object)
			{
				return createOperationAdapter();
			}
			@Override
			public Adapter caseOperationSignature(OperationSignature object)
			{
				return createOperationSignatureAdapter();
			}
			@Override
			public Adapter caseParameter(Parameter object)
			{
				return createParameterAdapter();
			}
			@Override
			public Adapter caseVariable(Variable object)
			{
				return createVariableAdapter();
			}
			@Override
			public Adapter caseType(Type object)
			{
				return createTypeAdapter();
			}
			@Override
			public Adapter caseStatement(Statement object)
			{
				return createStatementAdapter();
			}
			@Override
			public Adapter caseIf(If object)
			{
				return createIfAdapter();
			}
			@Override
			public Adapter caseFor(For object)
			{
				return createForAdapter();
			}
			@Override
			public Adapter caseReference(Reference object)
			{
				return createReferenceAdapter();
			}
			@Override
			public Adapter caseClassReference(ClassReference object)
			{
				return createClassReferenceAdapter();
			}
			@Override
			public Adapter caseOperationReference(OperationReference object)
			{
				return createOperationReferenceAdapter();
			}
			@Override
			public Adapter casevariableReference(variableReference object)
			{
				return createvariableReferenceAdapter();
			}
			@Override
			public Adapter caseExpression(Expression object)
			{
				return createExpressionAdapter();
			}
			@Override
			public Adapter caseLiteralValue(LiteralValue object)
			{
				return createLiteralValueAdapter();
			}
			@Override
			public Adapter caseNewInstance(NewInstance object)
			{
				return createNewInstanceAdapter();
			}
			@Override
			public Adapter caseStringValue(StringValue object)
			{
				return createStringValueAdapter();
			}
			@Override
			public Adapter caseBoolValue(BoolValue object)
			{
				return createBoolValueAdapter();
			}
			@Override
			public Adapter caseIntValue(IntValue object)
			{
				return createIntValueAdapter();
			}
			@Override
			public Adapter caseNullValue(NullValue object)
			{
				return createNullValueAdapter();
			}
			@Override
			public Adapter caseBinaryOperator(BinaryOperator object)
			{
				return createBinaryOperatorAdapter();
			}
			@Override
			public Adapter caseAssignment(Assignment object)
			{
				return createAssignmentAdapter();
			}
			@Override
			public Adapter caseUnaryOperator(UnaryOperator object)
			{
				return createUnaryOperatorAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object)
			{
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target)
	{
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link co.edu.javeriana.cmctljava.Program <em>Program</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see co.edu.javeriana.cmctljava.Program
	 * @generated
	 */
	public Adapter createProgramAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link co.edu.javeriana.cmctljava.Package <em>Package</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see co.edu.javeriana.cmctljava.Package
	 * @generated
	 */
	public Adapter createPackageAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link co.edu.javeriana.cmctljava.AbstractElement <em>Abstract Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see co.edu.javeriana.cmctljava.AbstractElement
	 * @generated
	 */
	public Adapter createAbstractElementAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link co.edu.javeriana.cmctljava.Import <em>Import</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see co.edu.javeriana.cmctljava.Import
	 * @generated
	 */
	public Adapter createImportAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link co.edu.javeriana.cmctljava.TypedElement <em>Typed Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see co.edu.javeriana.cmctljava.TypedElement
	 * @generated
	 */
	public Adapter createTypedElementAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link co.edu.javeriana.cmctljava.Class <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see co.edu.javeriana.cmctljava.Class
	 * @generated
	 */
	public Adapter createClassAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link co.edu.javeriana.cmctljava.Interface <em>Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see co.edu.javeriana.cmctljava.Interface
	 * @generated
	 */
	public Adapter createInterfaceAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link co.edu.javeriana.cmctljava.Feature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see co.edu.javeriana.cmctljava.Feature
	 * @generated
	 */
	public Adapter createFeatureAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link co.edu.javeriana.cmctljava.Attribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see co.edu.javeriana.cmctljava.Attribute
	 * @generated
	 */
	public Adapter createAttributeAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link co.edu.javeriana.cmctljava.Operation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see co.edu.javeriana.cmctljava.Operation
	 * @generated
	 */
	public Adapter createOperationAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link co.edu.javeriana.cmctljava.OperationSignature <em>Operation Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see co.edu.javeriana.cmctljava.OperationSignature
	 * @generated
	 */
	public Adapter createOperationSignatureAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link co.edu.javeriana.cmctljava.Parameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see co.edu.javeriana.cmctljava.Parameter
	 * @generated
	 */
	public Adapter createParameterAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link co.edu.javeriana.cmctljava.Variable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see co.edu.javeriana.cmctljava.Variable
	 * @generated
	 */
	public Adapter createVariableAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link co.edu.javeriana.cmctljava.Type <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see co.edu.javeriana.cmctljava.Type
	 * @generated
	 */
	public Adapter createTypeAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link co.edu.javeriana.cmctljava.Statement <em>Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see co.edu.javeriana.cmctljava.Statement
	 * @generated
	 */
	public Adapter createStatementAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link co.edu.javeriana.cmctljava.If <em>If</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see co.edu.javeriana.cmctljava.If
	 * @generated
	 */
	public Adapter createIfAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link co.edu.javeriana.cmctljava.For <em>For</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see co.edu.javeriana.cmctljava.For
	 * @generated
	 */
	public Adapter createForAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link co.edu.javeriana.cmctljava.Reference <em>Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see co.edu.javeriana.cmctljava.Reference
	 * @generated
	 */
	public Adapter createReferenceAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link co.edu.javeriana.cmctljava.ClassReference <em>Class Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see co.edu.javeriana.cmctljava.ClassReference
	 * @generated
	 */
	public Adapter createClassReferenceAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link co.edu.javeriana.cmctljava.OperationReference <em>Operation Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see co.edu.javeriana.cmctljava.OperationReference
	 * @generated
	 */
	public Adapter createOperationReferenceAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link co.edu.javeriana.cmctljava.variableReference <em>variable Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see co.edu.javeriana.cmctljava.variableReference
	 * @generated
	 */
	public Adapter createvariableReferenceAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link co.edu.javeriana.cmctljava.Expression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see co.edu.javeriana.cmctljava.Expression
	 * @generated
	 */
	public Adapter createExpressionAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link co.edu.javeriana.cmctljava.LiteralValue <em>Literal Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see co.edu.javeriana.cmctljava.LiteralValue
	 * @generated
	 */
	public Adapter createLiteralValueAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link co.edu.javeriana.cmctljava.NewInstance <em>New Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see co.edu.javeriana.cmctljava.NewInstance
	 * @generated
	 */
	public Adapter createNewInstanceAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link co.edu.javeriana.cmctljava.StringValue <em>String Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see co.edu.javeriana.cmctljava.StringValue
	 * @generated
	 */
	public Adapter createStringValueAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link co.edu.javeriana.cmctljava.BoolValue <em>Bool Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see co.edu.javeriana.cmctljava.BoolValue
	 * @generated
	 */
	public Adapter createBoolValueAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link co.edu.javeriana.cmctljava.IntValue <em>Int Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see co.edu.javeriana.cmctljava.IntValue
	 * @generated
	 */
	public Adapter createIntValueAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link co.edu.javeriana.cmctljava.NullValue <em>Null Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see co.edu.javeriana.cmctljava.NullValue
	 * @generated
	 */
	public Adapter createNullValueAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link co.edu.javeriana.cmctljava.BinaryOperator <em>Binary Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see co.edu.javeriana.cmctljava.BinaryOperator
	 * @generated
	 */
	public Adapter createBinaryOperatorAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link co.edu.javeriana.cmctljava.Assignment <em>Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see co.edu.javeriana.cmctljava.Assignment
	 * @generated
	 */
	public Adapter createAssignmentAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link co.edu.javeriana.cmctljava.UnaryOperator <em>Unary Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see co.edu.javeriana.cmctljava.UnaryOperator
	 * @generated
	 */
	public Adapter createUnaryOperatorAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter()
	{
		return null;
	}

} //CmctljavaAdapterFactory
