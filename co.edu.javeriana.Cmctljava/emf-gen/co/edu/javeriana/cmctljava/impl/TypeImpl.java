/**
 */
package co.edu.javeriana.cmctljava.impl;

import co.edu.javeriana.cmctljava.CmctljavaPackage;
import co.edu.javeriana.cmctljava.SimpleType;
import co.edu.javeriana.cmctljava.Type;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link co.edu.javeriana.cmctljava.impl.TypeImpl#getClass_ <em>Class</em>}</li>
 *   <li>{@link co.edu.javeriana.cmctljava.impl.TypeImpl#getSympleType <em>Symple Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TypeImpl extends MinimalEObjectImpl.Container implements Type
{
	/**
	 * The cached value of the '{@link #getClass_() <em>Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClass_()
	 * @generated
	 * @ordered
	 */
	protected co.edu.javeriana.cmctljava.Class class_;

	/**
	 * The default value of the '{@link #getSympleType() <em>Symple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSympleType()
	 * @generated
	 * @ordered
	 */
	protected static final SimpleType SYMPLE_TYPE_EDEFAULT = SimpleType.INT;

	/**
	 * The cached value of the '{@link #getSympleType() <em>Symple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSympleType()
	 * @generated
	 * @ordered
	 */
	protected SimpleType sympleType = SYMPLE_TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TypeImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return CmctljavaPackage.Literals.TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public co.edu.javeriana.cmctljava.Class getClass_()
	{
		if (class_ != null && class_.eIsProxy())
		{
			InternalEObject oldClass = (InternalEObject)class_;
			class_ = (co.edu.javeriana.cmctljava.Class)eResolveProxy(oldClass);
			if (class_ != oldClass)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CmctljavaPackage.TYPE__CLASS, oldClass, class_));
			}
		}
		return class_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public co.edu.javeriana.cmctljava.Class basicGetClass()
	{
		return class_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClass(co.edu.javeriana.cmctljava.Class newClass)
	{
		co.edu.javeriana.cmctljava.Class oldClass = class_;
		class_ = newClass;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CmctljavaPackage.TYPE__CLASS, oldClass, class_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleType getSympleType()
	{
		return sympleType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSympleType(SimpleType newSympleType)
	{
		SimpleType oldSympleType = sympleType;
		sympleType = newSympleType == null ? SYMPLE_TYPE_EDEFAULT : newSympleType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CmctljavaPackage.TYPE__SYMPLE_TYPE, oldSympleType, sympleType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
			case CmctljavaPackage.TYPE__CLASS:
				if (resolve) return getClass_();
				return basicGetClass();
			case CmctljavaPackage.TYPE__SYMPLE_TYPE:
				return getSympleType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
			case CmctljavaPackage.TYPE__CLASS:
				setClass((co.edu.javeriana.cmctljava.Class)newValue);
				return;
			case CmctljavaPackage.TYPE__SYMPLE_TYPE:
				setSympleType((SimpleType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
			case CmctljavaPackage.TYPE__CLASS:
				setClass((co.edu.javeriana.cmctljava.Class)null);
				return;
			case CmctljavaPackage.TYPE__SYMPLE_TYPE:
				setSympleType(SYMPLE_TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
			case CmctljavaPackage.TYPE__CLASS:
				return class_ != null;
			case CmctljavaPackage.TYPE__SYMPLE_TYPE:
				return sympleType != SYMPLE_TYPE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (sympleType: ");
		result.append(sympleType);
		result.append(')');
		return result.toString();
	}

} //TypeImpl
