/**
 */
package co.edu.javeriana.cmctljava.impl;

import co.edu.javeriana.cmctljava.AbstractElement;
import co.edu.javeriana.cmctljava.Accessibility;
import co.edu.javeriana.cmctljava.Assignment;
import co.edu.javeriana.cmctljava.Attribute;
import co.edu.javeriana.cmctljava.BinaryOperator;
import co.edu.javeriana.cmctljava.BoolValue;
import co.edu.javeriana.cmctljava.ClassReference;
import co.edu.javeriana.cmctljava.CmctljavaFactory;
import co.edu.javeriana.cmctljava.CmctljavaPackage;
import co.edu.javeriana.cmctljava.Expression;
import co.edu.javeriana.cmctljava.Feature;
import co.edu.javeriana.cmctljava.For;
import co.edu.javeriana.cmctljava.If;
import co.edu.javeriana.cmctljava.Import;
import co.edu.javeriana.cmctljava.IntValue;
import co.edu.javeriana.cmctljava.Interface;
import co.edu.javeriana.cmctljava.LiteralValue;
import co.edu.javeriana.cmctljava.NewInstance;
import co.edu.javeriana.cmctljava.NullValue;
import co.edu.javeriana.cmctljava.Operation;
import co.edu.javeriana.cmctljava.OperationReference;
import co.edu.javeriana.cmctljava.OperationSignature;
import co.edu.javeriana.cmctljava.Parameter;
import co.edu.javeriana.cmctljava.Program;
import co.edu.javeriana.cmctljava.Reference;
import co.edu.javeriana.cmctljava.SimpleType;
import co.edu.javeriana.cmctljava.Statement;
import co.edu.javeriana.cmctljava.StringValue;
import co.edu.javeriana.cmctljava.Type;
import co.edu.javeriana.cmctljava.UnaryOperator;
import co.edu.javeriana.cmctljava.Variable;
import co.edu.javeriana.cmctljava.variableReference;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CmctljavaFactoryImpl extends EFactoryImpl implements CmctljavaFactory
{
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static CmctljavaFactory init()
	{
		try
		{
			CmctljavaFactory theCmctljavaFactory = (CmctljavaFactory)EPackage.Registry.INSTANCE.getEFactory(CmctljavaPackage.eNS_URI);
			if (theCmctljavaFactory != null)
			{
				return theCmctljavaFactory;
			}
		}
		catch (Exception exception)
		{
			EcorePlugin.INSTANCE.log(exception);
		}
		return new CmctljavaFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CmctljavaFactoryImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass)
	{
		switch (eClass.getClassifierID())
		{
			case CmctljavaPackage.PROGRAM: return createProgram();
			case CmctljavaPackage.PACKAGE: return createPackage();
			case CmctljavaPackage.ABSTRACT_ELEMENT: return createAbstractElement();
			case CmctljavaPackage.IMPORT: return createImport();
			case CmctljavaPackage.CLASS: return createClass();
			case CmctljavaPackage.INTERFACE: return createInterface();
			case CmctljavaPackage.FEATURE: return createFeature();
			case CmctljavaPackage.ATTRIBUTE: return createAttribute();
			case CmctljavaPackage.OPERATION: return createOperation();
			case CmctljavaPackage.OPERATION_SIGNATURE: return createOperationSignature();
			case CmctljavaPackage.PARAMETER: return createParameter();
			case CmctljavaPackage.VARIABLE: return createVariable();
			case CmctljavaPackage.TYPE: return createType();
			case CmctljavaPackage.STATEMENT: return createStatement();
			case CmctljavaPackage.IF: return createIf();
			case CmctljavaPackage.FOR: return createFor();
			case CmctljavaPackage.REFERENCE: return createReference();
			case CmctljavaPackage.CLASS_REFERENCE: return createClassReference();
			case CmctljavaPackage.OPERATION_REFERENCE: return createOperationReference();
			case CmctljavaPackage.VARIABLE_REFERENCE: return createvariableReference();
			case CmctljavaPackage.EXPRESSION: return createExpression();
			case CmctljavaPackage.LITERAL_VALUE: return createLiteralValue();
			case CmctljavaPackage.NEW_INSTANCE: return createNewInstance();
			case CmctljavaPackage.STRING_VALUE: return createStringValue();
			case CmctljavaPackage.BOOL_VALUE: return createBoolValue();
			case CmctljavaPackage.INT_VALUE: return createIntValue();
			case CmctljavaPackage.NULL_VALUE: return createNullValue();
			case CmctljavaPackage.BINARY_OPERATOR: return createBinaryOperator();
			case CmctljavaPackage.ASSIGNMENT: return createAssignment();
			case CmctljavaPackage.UNARY_OPERATOR: return createUnaryOperator();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue)
	{
		switch (eDataType.getClassifierID())
		{
			case CmctljavaPackage.ACCESSIBILITY:
				return createAccessibilityFromString(eDataType, initialValue);
			case CmctljavaPackage.SIMPLE_TYPE:
				return createSimpleTypeFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue)
	{
		switch (eDataType.getClassifierID())
		{
			case CmctljavaPackage.ACCESSIBILITY:
				return convertAccessibilityToString(eDataType, instanceValue);
			case CmctljavaPackage.SIMPLE_TYPE:
				return convertSimpleTypeToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Program createProgram()
	{
		ProgramImpl program = new ProgramImpl();
		return program;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public co.edu.javeriana.cmctljava.Package createPackage()
	{
		PackageImpl package_ = new PackageImpl();
		return package_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractElement createAbstractElement()
	{
		AbstractElementImpl abstractElement = new AbstractElementImpl();
		return abstractElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Import createImport()
	{
		ImportImpl import_ = new ImportImpl();
		return import_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public co.edu.javeriana.cmctljava.Class createClass()
	{
		ClassImpl class_ = new ClassImpl();
		return class_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interface createInterface()
	{
		InterfaceImpl interface_ = new InterfaceImpl();
		return interface_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Feature createFeature()
	{
		FeatureImpl feature = new FeatureImpl();
		return feature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attribute createAttribute()
	{
		AttributeImpl attribute = new AttributeImpl();
		return attribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operation createOperation()
	{
		OperationImpl operation = new OperationImpl();
		return operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationSignature createOperationSignature()
	{
		OperationSignatureImpl operationSignature = new OperationSignatureImpl();
		return operationSignature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Parameter createParameter()
	{
		ParameterImpl parameter = new ParameterImpl();
		return parameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Variable createVariable()
	{
		VariableImpl variable = new VariableImpl();
		return variable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type createType()
	{
		TypeImpl type = new TypeImpl();
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Statement createStatement()
	{
		StatementImpl statement = new StatementImpl();
		return statement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public If createIf()
	{
		IfImpl if_ = new IfImpl();
		return if_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public For createFor()
	{
		ForImpl for_ = new ForImpl();
		return for_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Reference createReference()
	{
		ReferenceImpl reference = new ReferenceImpl();
		return reference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassReference createClassReference()
	{
		ClassReferenceImpl classReference = new ClassReferenceImpl();
		return classReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationReference createOperationReference()
	{
		OperationReferenceImpl operationReference = new OperationReferenceImpl();
		return operationReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public variableReference createvariableReference()
	{
		variableReferenceImpl variableReference = new variableReferenceImpl();
		return variableReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression createExpression()
	{
		ExpressionImpl expression = new ExpressionImpl();
		return expression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LiteralValue createLiteralValue()
	{
		LiteralValueImpl literalValue = new LiteralValueImpl();
		return literalValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NewInstance createNewInstance()
	{
		NewInstanceImpl newInstance = new NewInstanceImpl();
		return newInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StringValue createStringValue()
	{
		StringValueImpl stringValue = new StringValueImpl();
		return stringValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BoolValue createBoolValue()
	{
		BoolValueImpl boolValue = new BoolValueImpl();
		return boolValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntValue createIntValue()
	{
		IntValueImpl intValue = new IntValueImpl();
		return intValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NullValue createNullValue()
	{
		NullValueImpl nullValue = new NullValueImpl();
		return nullValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BinaryOperator createBinaryOperator()
	{
		BinaryOperatorImpl binaryOperator = new BinaryOperatorImpl();
		return binaryOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Assignment createAssignment()
	{
		AssignmentImpl assignment = new AssignmentImpl();
		return assignment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnaryOperator createUnaryOperator()
	{
		UnaryOperatorImpl unaryOperator = new UnaryOperatorImpl();
		return unaryOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Accessibility createAccessibilityFromString(EDataType eDataType, String initialValue)
	{
		Accessibility result = Accessibility.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAccessibilityToString(EDataType eDataType, Object instanceValue)
	{
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleType createSimpleTypeFromString(EDataType eDataType, String initialValue)
	{
		SimpleType result = SimpleType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSimpleTypeToString(EDataType eDataType, Object instanceValue)
	{
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CmctljavaPackage getCmctljavaPackage()
	{
		return (CmctljavaPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static CmctljavaPackage getPackage()
	{
		return CmctljavaPackage.eINSTANCE;
	}

} //CmctljavaFactoryImpl
