/**
 */
package co.edu.javeriana.cmctljava;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Literal Value</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getLiteralValue()
 * @model
 * @generated
 */
public interface LiteralValue extends Expression
{
} // LiteralValue
