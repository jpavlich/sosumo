/**
 */
package co.edu.javeriana.cmctljava;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operation Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link co.edu.javeriana.cmctljava.OperationReference#getParameters <em>Parameters</em>}</li>
 * </ul>
 *
 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getOperationReference()
 * @model
 * @generated
 */
public interface OperationReference extends Reference
{
	/**
	 * Returns the value of the '<em><b>Parameters</b></em>' containment reference list.
	 * The list contents are of type {@link co.edu.javeriana.cmctljava.Expression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameters</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameters</em>' containment reference list.
	 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getOperationReference_Parameters()
	 * @model containment="true"
	 * @generated
	 */
	EList<Expression> getParameters();

} // OperationReference
