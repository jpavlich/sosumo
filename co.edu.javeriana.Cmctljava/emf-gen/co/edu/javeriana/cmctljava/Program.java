/**
 */
package co.edu.javeriana.cmctljava;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Program</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link co.edu.javeriana.cmctljava.Program#getComponents <em>Components</em>}</li>
 * </ul>
 *
 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getProgram()
 * @model
 * @generated
 */
public interface Program extends EObject
{
	/**
	 * Returns the value of the '<em><b>Components</b></em>' containment reference list.
	 * The list contents are of type {@link co.edu.javeriana.cmctljava.Package}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Components</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Components</em>' containment reference list.
	 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getProgram_Components()
	 * @model containment="true"
	 * @generated
	 */
	EList<co.edu.javeriana.cmctljava.Package> getComponents();

} // Program
