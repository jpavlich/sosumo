/**
 */
package co.edu.javeriana.cmctljava;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getExpression()
 * @model
 * @generated
 */
public interface Expression extends EObject
{
} // Expression
