/**
 */
package co.edu.javeriana.cmctljava;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link co.edu.javeriana.cmctljava.ClassReference#getReferencedElement <em>Referenced Element</em>}</li>
 * </ul>
 *
 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getClassReference()
 * @model
 * @generated
 */
public interface ClassReference extends EObject
{
	/**
	 * Returns the value of the '<em><b>Referenced Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Referenced Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Referenced Element</em>' reference.
	 * @see #setReferencedElement(co.edu.javeriana.cmctljava.Class)
	 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getClassReference_ReferencedElement()
	 * @model
	 * @generated
	 */
	co.edu.javeriana.cmctljava.Class getReferencedElement();

	/**
	 * Sets the value of the '{@link co.edu.javeriana.cmctljava.ClassReference#getReferencedElement <em>Referenced Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Referenced Element</em>' reference.
	 * @see #getReferencedElement()
	 * @generated
	 */
	void setReferencedElement(co.edu.javeriana.cmctljava.Class value);

} // ClassReference
