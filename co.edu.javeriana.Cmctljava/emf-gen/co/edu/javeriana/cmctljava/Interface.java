/**
 */
package co.edu.javeriana.cmctljava;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interface</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link co.edu.javeriana.cmctljava.Interface#getVisibility <em>Visibility</em>}</li>
 *   <li>{@link co.edu.javeriana.cmctljava.Interface#getName <em>Name</em>}</li>
 *   <li>{@link co.edu.javeriana.cmctljava.Interface#getMethods <em>Methods</em>}</li>
 * </ul>
 *
 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getInterface()
 * @model
 * @generated
 */
public interface Interface extends AbstractElement
{
	/**
	 * Returns the value of the '<em><b>Visibility</b></em>' attribute.
	 * The literals are from the enumeration {@link co.edu.javeriana.cmctljava.Accessibility}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Visibility</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Visibility</em>' attribute.
	 * @see co.edu.javeriana.cmctljava.Accessibility
	 * @see #setVisibility(Accessibility)
	 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getInterface_Visibility()
	 * @model
	 * @generated
	 */
	Accessibility getVisibility();

	/**
	 * Sets the value of the '{@link co.edu.javeriana.cmctljava.Interface#getVisibility <em>Visibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Visibility</em>' attribute.
	 * @see co.edu.javeriana.cmctljava.Accessibility
	 * @see #getVisibility()
	 * @generated
	 */
	void setVisibility(Accessibility value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getInterface_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link co.edu.javeriana.cmctljava.Interface#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Methods</b></em>' containment reference list.
	 * The list contents are of type {@link co.edu.javeriana.cmctljava.OperationSignature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Methods</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Methods</em>' containment reference list.
	 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getInterface_Methods()
	 * @model containment="true"
	 * @generated
	 */
	EList<OperationSignature> getMethods();

} // Interface
