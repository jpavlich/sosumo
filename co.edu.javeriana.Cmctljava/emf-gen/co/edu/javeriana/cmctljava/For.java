/**
 */
package co.edu.javeriana.cmctljava;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>For</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link co.edu.javeriana.cmctljava.For#getVariable <em>Variable</em>}</li>
 *   <li>{@link co.edu.javeriana.cmctljava.For#getComparison <em>Comparison</em>}</li>
 *   <li>{@link co.edu.javeriana.cmctljava.For#getIterator <em>Iterator</em>}</li>
 *   <li>{@link co.edu.javeriana.cmctljava.For#getBody <em>Body</em>}</li>
 * </ul>
 *
 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getFor()
 * @model
 * @generated
 */
public interface For extends Statement
{
	/**
	 * Returns the value of the '<em><b>Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variable</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variable</em>' containment reference.
	 * @see #setVariable(Variable)
	 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getFor_Variable()
	 * @model containment="true"
	 * @generated
	 */
	Variable getVariable();

	/**
	 * Sets the value of the '{@link co.edu.javeriana.cmctljava.For#getVariable <em>Variable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Variable</em>' containment reference.
	 * @see #getVariable()
	 * @generated
	 */
	void setVariable(Variable value);

	/**
	 * Returns the value of the '<em><b>Comparison</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comparison</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comparison</em>' containment reference.
	 * @see #setComparison(Expression)
	 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getFor_Comparison()
	 * @model containment="true"
	 * @generated
	 */
	Expression getComparison();

	/**
	 * Sets the value of the '{@link co.edu.javeriana.cmctljava.For#getComparison <em>Comparison</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Comparison</em>' containment reference.
	 * @see #getComparison()
	 * @generated
	 */
	void setComparison(Expression value);

	/**
	 * Returns the value of the '<em><b>Iterator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iterator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iterator</em>' containment reference.
	 * @see #setIterator(Statement)
	 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getFor_Iterator()
	 * @model containment="true"
	 * @generated
	 */
	Statement getIterator();

	/**
	 * Sets the value of the '{@link co.edu.javeriana.cmctljava.For#getIterator <em>Iterator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iterator</em>' containment reference.
	 * @see #getIterator()
	 * @generated
	 */
	void setIterator(Statement value);

	/**
	 * Returns the value of the '<em><b>Body</b></em>' containment reference list.
	 * The list contents are of type {@link co.edu.javeriana.cmctljava.Statement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body</em>' containment reference list.
	 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getFor_Body()
	 * @model containment="true"
	 * @generated
	 */
	EList<Statement> getBody();

} // For
