/**
 */
package co.edu.javeriana.cmctljava;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>variable Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link co.edu.javeriana.cmctljava.variableReference#getReferencedElement <em>Referenced Element</em>}</li>
 * </ul>
 *
 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getvariableReference()
 * @model
 * @generated
 */
public interface variableReference extends EObject
{
	/**
	 * Returns the value of the '<em><b>Referenced Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Referenced Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Referenced Element</em>' reference.
	 * @see #setReferencedElement(Variable)
	 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getvariableReference_ReferencedElement()
	 * @model
	 * @generated
	 */
	Variable getReferencedElement();

	/**
	 * Sets the value of the '{@link co.edu.javeriana.cmctljava.variableReference#getReferencedElement <em>Referenced Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Referenced Element</em>' reference.
	 * @see #getReferencedElement()
	 * @generated
	 */
	void setReferencedElement(Variable value);

} // variableReference
