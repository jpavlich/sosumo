/**
 */
package co.edu.javeriana.cmctljava;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link co.edu.javeriana.cmctljava.Type#getClass_ <em>Class</em>}</li>
 *   <li>{@link co.edu.javeriana.cmctljava.Type#getSympleType <em>Symple Type</em>}</li>
 * </ul>
 *
 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getType()
 * @model
 * @generated
 */
public interface Type extends EObject
{
	/**
	 * Returns the value of the '<em><b>Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class</em>' reference.
	 * @see #setClass(co.edu.javeriana.cmctljava.Class)
	 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getType_Class()
	 * @model
	 * @generated
	 */
	co.edu.javeriana.cmctljava.Class getClass_();

	/**
	 * Sets the value of the '{@link co.edu.javeriana.cmctljava.Type#getClass_ <em>Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Class</em>' reference.
	 * @see #getClass_()
	 * @generated
	 */
	void setClass(co.edu.javeriana.cmctljava.Class value);

	/**
	 * Returns the value of the '<em><b>Symple Type</b></em>' attribute.
	 * The literals are from the enumeration {@link co.edu.javeriana.cmctljava.SimpleType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Symple Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Symple Type</em>' attribute.
	 * @see co.edu.javeriana.cmctljava.SimpleType
	 * @see #setSympleType(SimpleType)
	 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getType_SympleType()
	 * @model
	 * @generated
	 */
	SimpleType getSympleType();

	/**
	 * Sets the value of the '{@link co.edu.javeriana.cmctljava.Type#getSympleType <em>Symple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Symple Type</em>' attribute.
	 * @see co.edu.javeriana.cmctljava.SimpleType
	 * @see #getSympleType()
	 * @generated
	 */
	void setSympleType(SimpleType value);

} // Type
