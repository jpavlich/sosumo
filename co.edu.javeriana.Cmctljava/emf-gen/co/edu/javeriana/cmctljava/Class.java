/**
 */
package co.edu.javeriana.cmctljava;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link co.edu.javeriana.cmctljava.Class#getVisibility <em>Visibility</em>}</li>
 *   <li>{@link co.edu.javeriana.cmctljava.Class#getName <em>Name</em>}</li>
 *   <li>{@link co.edu.javeriana.cmctljava.Class#getHeritage <em>Heritage</em>}</li>
 *   <li>{@link co.edu.javeriana.cmctljava.Class#getInterface <em>Interface</em>}</li>
 *   <li>{@link co.edu.javeriana.cmctljava.Class#getFeatures <em>Features</em>}</li>
 * </ul>
 *
 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getClass_()
 * @model
 * @generated
 */
public interface Class extends AbstractElement
{
	/**
	 * Returns the value of the '<em><b>Visibility</b></em>' attribute.
	 * The literals are from the enumeration {@link co.edu.javeriana.cmctljava.Accessibility}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Visibility</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Visibility</em>' attribute.
	 * @see co.edu.javeriana.cmctljava.Accessibility
	 * @see #setVisibility(Accessibility)
	 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getClass_Visibility()
	 * @model
	 * @generated
	 */
	Accessibility getVisibility();

	/**
	 * Sets the value of the '{@link co.edu.javeriana.cmctljava.Class#getVisibility <em>Visibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Visibility</em>' attribute.
	 * @see co.edu.javeriana.cmctljava.Accessibility
	 * @see #getVisibility()
	 * @generated
	 */
	void setVisibility(Accessibility value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getClass_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link co.edu.javeriana.cmctljava.Class#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Heritage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Heritage</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Heritage</em>' reference.
	 * @see #setHeritage(Class)
	 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getClass_Heritage()
	 * @model
	 * @generated
	 */
	Class getHeritage();

	/**
	 * Sets the value of the '{@link co.edu.javeriana.cmctljava.Class#getHeritage <em>Heritage</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Heritage</em>' reference.
	 * @see #getHeritage()
	 * @generated
	 */
	void setHeritage(Class value);

	/**
	 * Returns the value of the '<em><b>Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interface</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interface</em>' reference.
	 * @see #setInterface(Interface)
	 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getClass_Interface()
	 * @model
	 * @generated
	 */
	Interface getInterface();

	/**
	 * Sets the value of the '{@link co.edu.javeriana.cmctljava.Class#getInterface <em>Interface</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interface</em>' reference.
	 * @see #getInterface()
	 * @generated
	 */
	void setInterface(Interface value);

	/**
	 * Returns the value of the '<em><b>Features</b></em>' containment reference list.
	 * The list contents are of type {@link co.edu.javeriana.cmctljava.Feature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Features</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Features</em>' containment reference list.
	 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getClass_Features()
	 * @model containment="true"
	 * @generated
	 */
	EList<Feature> getFeatures();

} // Class
