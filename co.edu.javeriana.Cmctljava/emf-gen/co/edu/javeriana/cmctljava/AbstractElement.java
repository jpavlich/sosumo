/**
 */
package co.edu.javeriana.cmctljava;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getAbstractElement()
 * @model
 * @generated
 */
public interface AbstractElement extends EObject
{
} // AbstractElement
