/**
 */
package co.edu.javeriana.cmctljava;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Int Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link co.edu.javeriana.cmctljava.IntValue#getLiteral <em>Literal</em>}</li>
 * </ul>
 *
 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getIntValue()
 * @model
 * @generated
 */
public interface IntValue extends LiteralValue
{
	/**
	 * Returns the value of the '<em><b>Literal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Literal</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Literal</em>' attribute.
	 * @see #setLiteral(int)
	 * @see co.edu.javeriana.cmctljava.CmctljavaPackage#getIntValue_Literal()
	 * @model
	 * @generated
	 */
	int getLiteral();

	/**
	 * Sets the value of the '{@link co.edu.javeriana.cmctljava.IntValue#getLiteral <em>Literal</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Literal</em>' attribute.
	 * @see #getLiteral()
	 * @generated
	 */
	void setLiteral(int value);

} // IntValue
