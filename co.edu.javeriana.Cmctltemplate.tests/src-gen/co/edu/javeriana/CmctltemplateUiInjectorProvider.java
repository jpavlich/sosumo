/*
 * generated by Xtext
 */
package co.edu.javeriana;

import org.eclipse.xtext.junit4.IInjectorProvider;

import com.google.inject.Injector;

public class CmctltemplateUiInjectorProvider implements IInjectorProvider {
	
	@Override
	public Injector getInjector() {
		return co.edu.javeriana.ui.internal.CmctltemplateActivator.getInstance().getInjector("co.edu.javeriana.Cmctltemplate");
	}
	
}
