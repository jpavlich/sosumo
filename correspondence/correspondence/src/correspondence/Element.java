/**
 */
package correspondence;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see correspondence.CorrespondencePackage#getElement()
 * @model abstract="true"
 * @generated
 */
public interface Element extends EObject {
} // Element
