/**
 */
package correspondence;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lower Correspondence</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see correspondence.CorrespondencePackage#getLowerCorrespondence()
 * @model
 * @generated
 */
public interface LowerCorrespondence extends Correspondence {
} // LowerCorrespondence
