/**
 */
package correspondence;

import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Package Ref</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link correspondence.PackageRef#getReferencedPackage <em>Referenced Package</em>}</li>
 * </ul>
 *
 * @see correspondence.CorrespondencePackage#getPackageRef()
 * @model
 * @generated
 */
public interface PackageRef extends Term {
	/**
	 * Returns the value of the '<em><b>Referenced Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Referenced Package</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Referenced Package</em>' reference.
	 * @see #setReferencedPackage(EPackage)
	 * @see correspondence.CorrespondencePackage#getPackageRef_ReferencedPackage()
	 * @model
	 * @generated
	 */
	EPackage getReferencedPackage();

	/**
	 * Sets the value of the '{@link correspondence.PackageRef#getReferencedPackage <em>Referenced Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Referenced Package</em>' reference.
	 * @see #getReferencedPackage()
	 * @generated
	 */
	void setReferencedPackage(EPackage value);

} // PackageRef
