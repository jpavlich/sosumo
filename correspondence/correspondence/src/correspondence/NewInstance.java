/**
 */
package correspondence;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>New Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see correspondence.CorrespondencePackage#getNewInstance()
 * @model
 * @generated
 */
public interface NewInstance extends Instance {
} // NewInstance
