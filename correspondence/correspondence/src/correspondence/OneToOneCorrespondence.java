/**
 */
package correspondence;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>One To One Correspondence</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see correspondence.CorrespondencePackage#getOneToOneCorrespondence()
 * @model
 * @generated
 */
public interface OneToOneCorrespondence extends Correspondence {
} // OneToOneCorrespondence
