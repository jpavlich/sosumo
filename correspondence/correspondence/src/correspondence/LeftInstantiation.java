/**
 */
package correspondence;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Left Instantiation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see correspondence.CorrespondencePackage#getLeftInstantiation()
 * @model
 * @generated
 */
public interface LeftInstantiation extends Instantiation {
} // LeftInstantiation
