/**
 */
package correspondence;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Right Instantiation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see correspondence.CorrespondencePackage#getRightInstantiation()
 * @model
 * @generated
 */
public interface RightInstantiation extends Instantiation {
} // RightInstantiation
