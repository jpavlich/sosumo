/**
 */
package correspondence;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Nothing</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see correspondence.CorrespondencePackage#getNothing()
 * @model
 * @generated
 */
public interface Nothing extends Term {
} // Nothing
