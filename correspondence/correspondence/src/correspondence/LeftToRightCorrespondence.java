/**
 */
package correspondence;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Left To Right Correspondence</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see correspondence.CorrespondencePackage#getLeftToRightCorrespondence()
 * @model
 * @generated
 */
public interface LeftToRightCorrespondence extends Correspondence {
} // LeftToRightCorrespondence
