/**
 */
package correspondence;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>This Ref</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see correspondence.CorrespondencePackage#getThisRef()
 * @model
 * @generated
 */
public interface ThisRef extends Term {
} // ThisRef
