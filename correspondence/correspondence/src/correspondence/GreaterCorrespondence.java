/**
 */
package correspondence;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Greater Correspondence</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see correspondence.CorrespondencePackage#getGreaterCorrespondence()
 * @model
 * @generated
 */
public interface GreaterCorrespondence extends Correspondence {
} // GreaterCorrespondence
