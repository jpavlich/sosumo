/**
 */
package correspondence;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Term</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see correspondence.CorrespondencePackage#getTerm()
 * @model abstract="true"
 * @generated
 */
public interface Term extends EObject {
} // Term
