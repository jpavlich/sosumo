/**
 */
package correspondence;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature Correspondence</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link correspondence.FeatureCorrespondence#getLeft <em>Left</em>}</li>
 *   <li>{@link correspondence.FeatureCorrespondence#getRight <em>Right</em>}</li>
 * </ul>
 *
 * @see correspondence.CorrespondencePackage#getFeatureCorrespondence()
 * @model
 * @generated
 */
public interface FeatureCorrespondence extends EObject {
	/**
	 * Returns the value of the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Left</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left</em>' containment reference.
	 * @see #setLeft(Function)
	 * @see correspondence.CorrespondencePackage#getFeatureCorrespondence_Left()
	 * @model containment="true"
	 * @generated
	 */
	Function getLeft();

	/**
	 * Sets the value of the '{@link correspondence.FeatureCorrespondence#getLeft <em>Left</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left</em>' containment reference.
	 * @see #getLeft()
	 * @generated
	 */
	void setLeft(Function value);

	/**
	 * Returns the value of the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right</em>' containment reference.
	 * @see #setRight(Function)
	 * @see correspondence.CorrespondencePackage#getFeatureCorrespondence_Right()
	 * @model containment="true"
	 * @generated
	 */
	Function getRight();

	/**
	 * Sets the value of the '{@link correspondence.FeatureCorrespondence#getRight <em>Right</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right</em>' containment reference.
	 * @see #getRight()
	 * @generated
	 */
	void setRight(Function value);

} // FeatureCorrespondence
