/**
 */
package correspondence;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Existing Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see correspondence.CorrespondencePackage#getExistingInstance()
 * @model
 * @generated
 */
public interface ExistingInstance extends Instance {
} // ExistingInstance
