/**
 */
package correspondence.impl;

import correspondence.CorrespondencePackage;
import correspondence.LeftToRightCorrespondence;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Left To Right Correspondence</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class LeftToRightCorrespondenceImpl extends CorrespondenceImpl implements LeftToRightCorrespondence {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LeftToRightCorrespondenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorrespondencePackage.Literals.LEFT_TO_RIGHT_CORRESPONDENCE;
	}

} //LeftToRightCorrespondenceImpl
