/**
 */
package correspondence.impl;

import correspondence.CorrespondencePackage;
import correspondence.ExistingInstance;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Existing Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ExistingInstanceImpl extends InstanceImpl implements ExistingInstance {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExistingInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorrespondencePackage.Literals.EXISTING_INSTANCE;
	}

} //ExistingInstanceImpl
