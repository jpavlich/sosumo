/**
 */
package correspondence.impl;

import correspondence.BidirectionalCorrespondence;
import correspondence.CorrespondencePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Bidirectional Correspondence</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BidirectionalCorrespondenceImpl extends CorrespondenceImpl implements BidirectionalCorrespondence {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BidirectionalCorrespondenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorrespondencePackage.Literals.BIDIRECTIONAL_CORRESPONDENCE;
	}

} //BidirectionalCorrespondenceImpl
