/**
 */
package correspondence.impl;

import correspondence.CorrespondencePackage;
import correspondence.NewInstance;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>New Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class NewInstanceImpl extends InstanceImpl implements NewInstance {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NewInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorrespondencePackage.Literals.NEW_INSTANCE;
	}

} //NewInstanceImpl
