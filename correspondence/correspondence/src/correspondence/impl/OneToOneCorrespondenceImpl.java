/**
 */
package correspondence.impl;

import correspondence.CorrespondencePackage;
import correspondence.OneToOneCorrespondence;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>One To One Correspondence</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class OneToOneCorrespondenceImpl extends CorrespondenceImpl implements OneToOneCorrespondence {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OneToOneCorrespondenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorrespondencePackage.Literals.ONE_TO_ONE_CORRESPONDENCE;
	}

} //OneToOneCorrespondenceImpl
