/**
 */
package correspondence.impl;

import correspondence.CorrespondencePackage;
import correspondence.ThisRef;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>This Ref</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ThisRefImpl extends TermImpl implements ThisRef {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ThisRefImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorrespondencePackage.Literals.THIS_REF;
	}

} //ThisRefImpl
