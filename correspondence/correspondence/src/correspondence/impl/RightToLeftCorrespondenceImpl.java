/**
 */
package correspondence.impl;

import correspondence.CorrespondencePackage;
import correspondence.RightToLeftCorrespondence;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Right To Left Correspondence</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RightToLeftCorrespondenceImpl extends CorrespondenceImpl implements RightToLeftCorrespondence {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RightToLeftCorrespondenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorrespondencePackage.Literals.RIGHT_TO_LEFT_CORRESPONDENCE;
	}

} //RightToLeftCorrespondenceImpl
