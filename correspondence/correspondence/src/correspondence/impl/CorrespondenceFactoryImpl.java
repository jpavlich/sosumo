/**
 */
package correspondence.impl;

import correspondence.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CorrespondenceFactoryImpl extends EFactoryImpl implements CorrespondenceFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static CorrespondenceFactory init() {
		try {
			CorrespondenceFactory theCorrespondenceFactory = (CorrespondenceFactory)EPackage.Registry.INSTANCE.getEFactory(CorrespondencePackage.eNS_URI);
			if (theCorrespondenceFactory != null) {
				return theCorrespondenceFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new CorrespondenceFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CorrespondenceFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case CorrespondencePackage.CORRESPONDENCES: return createCorrespondences();
			case CorrespondencePackage.IMPORT: return createImport();
			case CorrespondencePackage.FUNCTION: return createFunction();
			case CorrespondencePackage.CLASS_REF: return createClassRef();
			case CorrespondencePackage.FEATURE_REF: return createFeatureRef();
			case CorrespondencePackage.STRING_VALUE: return createStringValue();
			case CorrespondencePackage.INT_VALUE: return createIntValue();
			case CorrespondencePackage.BIDIRECTIONAL_CORRESPONDENCE: return createBidirectionalCorrespondence();
			case CorrespondencePackage.ONE_TO_ONE_CORRESPONDENCE: return createOneToOneCorrespondence();
			case CorrespondencePackage.RIGHT_TO_LEFT_CORRESPONDENCE: return createRightToLeftCorrespondence();
			case CorrespondencePackage.LEFT_TO_RIGHT_CORRESPONDENCE: return createLeftToRightCorrespondence();
			case CorrespondencePackage.PACKAGE_REF: return createPackageRef();
			case CorrespondencePackage.EXISTING_INSTANCE: return createExistingInstance();
			case CorrespondencePackage.ASSIGNMENT: return createAssignment();
			case CorrespondencePackage.NEW_INSTANCE: return createNewInstance();
			case CorrespondencePackage.COLLECTION_VALUE: return createCollectionValue();
			case CorrespondencePackage.THIS_REF: return createThisRef();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Correspondences createCorrespondences() {
		CorrespondencesImpl correspondences = new CorrespondencesImpl();
		return correspondences;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Import createImport() {
		ImportImpl import_ = new ImportImpl();
		return import_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Function createFunction() {
		FunctionImpl function = new FunctionImpl();
		return function;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassRef createClassRef() {
		ClassRefImpl classRef = new ClassRefImpl();
		return classRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureRef createFeatureRef() {
		FeatureRefImpl featureRef = new FeatureRefImpl();
		return featureRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StringValue createStringValue() {
		StringValueImpl stringValue = new StringValueImpl();
		return stringValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntValue createIntValue() {
		IntValueImpl intValue = new IntValueImpl();
		return intValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BidirectionalCorrespondence createBidirectionalCorrespondence() {
		BidirectionalCorrespondenceImpl bidirectionalCorrespondence = new BidirectionalCorrespondenceImpl();
		return bidirectionalCorrespondence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OneToOneCorrespondence createOneToOneCorrespondence() {
		OneToOneCorrespondenceImpl oneToOneCorrespondence = new OneToOneCorrespondenceImpl();
		return oneToOneCorrespondence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RightToLeftCorrespondence createRightToLeftCorrespondence() {
		RightToLeftCorrespondenceImpl rightToLeftCorrespondence = new RightToLeftCorrespondenceImpl();
		return rightToLeftCorrespondence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LeftToRightCorrespondence createLeftToRightCorrespondence() {
		LeftToRightCorrespondenceImpl leftToRightCorrespondence = new LeftToRightCorrespondenceImpl();
		return leftToRightCorrespondence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PackageRef createPackageRef() {
		PackageRefImpl packageRef = new PackageRefImpl();
		return packageRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExistingInstance createExistingInstance() {
		ExistingInstanceImpl existingInstance = new ExistingInstanceImpl();
		return existingInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Assignment createAssignment() {
		AssignmentImpl assignment = new AssignmentImpl();
		return assignment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NewInstance createNewInstance() {
		NewInstanceImpl newInstance = new NewInstanceImpl();
		return newInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CollectionValue createCollectionValue() {
		CollectionValueImpl collectionValue = new CollectionValueImpl();
		return collectionValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ThisRef createThisRef() {
		ThisRefImpl thisRef = new ThisRefImpl();
		return thisRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CorrespondencePackage getCorrespondencePackage() {
		return (CorrespondencePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static CorrespondencePackage getPackage() {
		return CorrespondencePackage.eINSTANCE;
	}

} //CorrespondenceFactoryImpl
