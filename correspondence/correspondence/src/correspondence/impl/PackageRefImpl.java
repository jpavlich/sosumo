/**
 */
package correspondence.impl;

import correspondence.CorrespondencePackage;
import correspondence.PackageRef;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Package Ref</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link correspondence.impl.PackageRefImpl#getReferencedPackage <em>Referenced Package</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PackageRefImpl extends TermImpl implements PackageRef {
	/**
	 * The cached value of the '{@link #getReferencedPackage() <em>Referenced Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferencedPackage()
	 * @generated
	 * @ordered
	 */
	protected EPackage referencedPackage;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PackageRefImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorrespondencePackage.Literals.PACKAGE_REF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EPackage getReferencedPackage() {
		if (referencedPackage != null && referencedPackage.eIsProxy()) {
			InternalEObject oldReferencedPackage = (InternalEObject)referencedPackage;
			referencedPackage = (EPackage)eResolveProxy(oldReferencedPackage);
			if (referencedPackage != oldReferencedPackage) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorrespondencePackage.PACKAGE_REF__REFERENCED_PACKAGE, oldReferencedPackage, referencedPackage));
			}
		}
		return referencedPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EPackage basicGetReferencedPackage() {
		return referencedPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferencedPackage(EPackage newReferencedPackage) {
		EPackage oldReferencedPackage = referencedPackage;
		referencedPackage = newReferencedPackage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorrespondencePackage.PACKAGE_REF__REFERENCED_PACKAGE, oldReferencedPackage, referencedPackage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CorrespondencePackage.PACKAGE_REF__REFERENCED_PACKAGE:
				if (resolve) return getReferencedPackage();
				return basicGetReferencedPackage();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CorrespondencePackage.PACKAGE_REF__REFERENCED_PACKAGE:
				setReferencedPackage((EPackage)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CorrespondencePackage.PACKAGE_REF__REFERENCED_PACKAGE:
				setReferencedPackage((EPackage)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CorrespondencePackage.PACKAGE_REF__REFERENCED_PACKAGE:
				return referencedPackage != null;
		}
		return super.eIsSet(featureID);
	}

} //PackageRefImpl
