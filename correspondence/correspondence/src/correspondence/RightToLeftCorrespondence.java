/**
 */
package correspondence;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Right To Left Correspondence</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see correspondence.CorrespondencePackage#getRightToLeftCorrespondence()
 * @model
 * @generated
 */
public interface RightToLeftCorrespondence extends Correspondence {
} // RightToLeftCorrespondence
