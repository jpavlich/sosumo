/**
 */
package correspondence;

import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature Ref</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link correspondence.FeatureRef#getReferencedFeature <em>Referenced Feature</em>}</li>
 *   <li>{@link correspondence.FeatureRef#getInstance <em>Instance</em>}</li>
 * </ul>
 *
 * @see correspondence.CorrespondencePackage#getFeatureRef()
 * @model
 * @generated
 */
public interface FeatureRef extends Term {
	/**
	 * Returns the value of the '<em><b>Referenced Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Referenced Feature</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Referenced Feature</em>' reference.
	 * @see #setReferencedFeature(EStructuralFeature)
	 * @see correspondence.CorrespondencePackage#getFeatureRef_ReferencedFeature()
	 * @model
	 * @generated
	 */
	EStructuralFeature getReferencedFeature();

	/**
	 * Sets the value of the '{@link correspondence.FeatureRef#getReferencedFeature <em>Referenced Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Referenced Feature</em>' reference.
	 * @see #getReferencedFeature()
	 * @generated
	 */
	void setReferencedFeature(EStructuralFeature value);

	/**
	 * Returns the value of the '<em><b>Instance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Instance</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instance</em>' containment reference.
	 * @see #setInstance(Instance)
	 * @see correspondence.CorrespondencePackage#getFeatureRef_Instance()
	 * @model containment="true"
	 * @generated
	 */
	Instance getInstance();

	/**
	 * Sets the value of the '{@link correspondence.FeatureRef#getInstance <em>Instance</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Instance</em>' containment reference.
	 * @see #getInstance()
	 * @generated
	 */
	void setInstance(Instance value);

} // FeatureRef
