/**
 */
package correspondence;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Collection Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link correspondence.CollectionValue#getContents <em>Contents</em>}</li>
 * </ul>
 *
 * @see correspondence.CorrespondencePackage#getCollectionValue()
 * @model
 * @generated
 */
public interface CollectionValue extends Value {
	/**
	 * Returns the value of the '<em><b>Contents</b></em>' containment reference list.
	 * The list contents are of type {@link correspondence.Term}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contents</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contents</em>' containment reference list.
	 * @see correspondence.CorrespondencePackage#getCollectionValue_Contents()
	 * @model containment="true"
	 * @generated
	 */
	EList<Term> getContents();

} // CollectionValue
