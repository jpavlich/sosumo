/**
 */
package correspondence;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bidirectional Correspondence</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see correspondence.CorrespondencePackage#getBidirectionalCorrespondence()
 * @model
 * @generated
 */
public interface BidirectionalCorrespondence extends Correspondence {
} // BidirectionalCorrespondence
