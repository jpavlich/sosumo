package correspondence.xtext.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import correspondence.xtext.services.CorrespondenceGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalCorrespondenceParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'import'", "'--'", "'{'", "'}'", "'=='", "'->'", "'<-'", "'+'", "'.'", "'existing'", "'new'", "'='", "'this'", "'['", "']'", "','", "'*'"
    };
    public static final int RULE_ID=4;
    public static final int T__27=27;
    public static final int T__26=26;
    public static final int T__25=25;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__19=19;
    public static final int RULE_STRING=5;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=6;
    public static final int RULE_WS=9;

    // delegates
    // delegators


        public InternalCorrespondenceParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalCorrespondenceParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalCorrespondenceParser.tokenNames; }
    public String getGrammarFileName() { return "../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g"; }


     
     	private CorrespondenceGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(CorrespondenceGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleCorrespondences"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:61:1: entryRuleCorrespondences : ruleCorrespondences EOF ;
    public final void entryRuleCorrespondences() throws RecognitionException {
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:62:1: ( ruleCorrespondences EOF )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:63:1: ruleCorrespondences EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCorrespondencesRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleCorrespondences_in_entryRuleCorrespondences67);
            ruleCorrespondences();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCorrespondencesRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleCorrespondences74); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCorrespondences"


    // $ANTLR start "ruleCorrespondences"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:70:1: ruleCorrespondences : ( ( rule__Correspondences__Group__0 ) ) ;
    public final void ruleCorrespondences() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:74:2: ( ( ( rule__Correspondences__Group__0 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:75:1: ( ( rule__Correspondences__Group__0 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:75:1: ( ( rule__Correspondences__Group__0 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:76:1: ( rule__Correspondences__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCorrespondencesAccess().getGroup()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:77:1: ( rule__Correspondences__Group__0 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:77:2: rule__Correspondences__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Correspondences__Group__0_in_ruleCorrespondences100);
            rule__Correspondences__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCorrespondencesAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCorrespondences"


    // $ANTLR start "entryRuleImport"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:89:1: entryRuleImport : ruleImport EOF ;
    public final void entryRuleImport() throws RecognitionException {
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:90:1: ( ruleImport EOF )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:91:1: ruleImport EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImportRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleImport_in_entryRuleImport127);
            ruleImport();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getImportRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleImport134); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:98:1: ruleImport : ( ( rule__Import__Group__0 ) ) ;
    public final void ruleImport() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:102:2: ( ( ( rule__Import__Group__0 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:103:1: ( ( rule__Import__Group__0 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:103:1: ( ( rule__Import__Group__0 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:104:1: ( rule__Import__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImportAccess().getGroup()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:105:1: ( rule__Import__Group__0 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:105:2: rule__Import__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Import__Group__0_in_ruleImport160);
            rule__Import__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getImportAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleModelCorrespondence"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:117:1: entryRuleModelCorrespondence : ruleModelCorrespondence EOF ;
    public final void entryRuleModelCorrespondence() throws RecognitionException {
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:118:1: ( ruleModelCorrespondence EOF )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:119:1: ruleModelCorrespondence EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModelCorrespondenceRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleModelCorrespondence_in_entryRuleModelCorrespondence187);
            ruleModelCorrespondence();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getModelCorrespondenceRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleModelCorrespondence194); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleModelCorrespondence"


    // $ANTLR start "ruleModelCorrespondence"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:126:1: ruleModelCorrespondence : ( ( rule__ModelCorrespondence__Group__0 ) ) ;
    public final void ruleModelCorrespondence() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:130:2: ( ( ( rule__ModelCorrespondence__Group__0 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:131:1: ( ( rule__ModelCorrespondence__Group__0 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:131:1: ( ( rule__ModelCorrespondence__Group__0 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:132:1: ( rule__ModelCorrespondence__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModelCorrespondenceAccess().getGroup()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:133:1: ( rule__ModelCorrespondence__Group__0 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:133:2: rule__ModelCorrespondence__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__ModelCorrespondence__Group__0_in_ruleModelCorrespondence220);
            rule__ModelCorrespondence__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getModelCorrespondenceAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleModelCorrespondence"


    // $ANTLR start "entryRuleModelFunction"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:145:1: entryRuleModelFunction : ruleModelFunction EOF ;
    public final void entryRuleModelFunction() throws RecognitionException {
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:146:1: ( ruleModelFunction EOF )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:147:1: ruleModelFunction EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModelFunctionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleModelFunction_in_entryRuleModelFunction247);
            ruleModelFunction();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getModelFunctionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleModelFunction254); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleModelFunction"


    // $ANTLR start "ruleModelFunction"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:154:1: ruleModelFunction : ( ( rule__ModelFunction__TermsAssignment ) ) ;
    public final void ruleModelFunction() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:158:2: ( ( ( rule__ModelFunction__TermsAssignment ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:159:1: ( ( rule__ModelFunction__TermsAssignment ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:159:1: ( ( rule__ModelFunction__TermsAssignment ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:160:1: ( rule__ModelFunction__TermsAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModelFunctionAccess().getTermsAssignment()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:161:1: ( rule__ModelFunction__TermsAssignment )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:161:2: rule__ModelFunction__TermsAssignment
            {
            pushFollow(FollowSets000.FOLLOW_rule__ModelFunction__TermsAssignment_in_ruleModelFunction280);
            rule__ModelFunction__TermsAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getModelFunctionAccess().getTermsAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleModelFunction"


    // $ANTLR start "entryRulePackageRef"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:173:1: entryRulePackageRef : rulePackageRef EOF ;
    public final void entryRulePackageRef() throws RecognitionException {
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:174:1: ( rulePackageRef EOF )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:175:1: rulePackageRef EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPackageRefRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_rulePackageRef_in_entryRulePackageRef307);
            rulePackageRef();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getPackageRefRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRulePackageRef314); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePackageRef"


    // $ANTLR start "rulePackageRef"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:182:1: rulePackageRef : ( ( rule__PackageRef__ReferencedPackageAssignment ) ) ;
    public final void rulePackageRef() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:186:2: ( ( ( rule__PackageRef__ReferencedPackageAssignment ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:187:1: ( ( rule__PackageRef__ReferencedPackageAssignment ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:187:1: ( ( rule__PackageRef__ReferencedPackageAssignment ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:188:1: ( rule__PackageRef__ReferencedPackageAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPackageRefAccess().getReferencedPackageAssignment()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:189:1: ( rule__PackageRef__ReferencedPackageAssignment )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:189:2: rule__PackageRef__ReferencedPackageAssignment
            {
            pushFollow(FollowSets000.FOLLOW_rule__PackageRef__ReferencedPackageAssignment_in_rulePackageRef340);
            rule__PackageRef__ReferencedPackageAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getPackageRefAccess().getReferencedPackageAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePackageRef"


    // $ANTLR start "entryRuleClassCorrespondence"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:201:1: entryRuleClassCorrespondence : ruleClassCorrespondence EOF ;
    public final void entryRuleClassCorrespondence() throws RecognitionException {
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:202:1: ( ruleClassCorrespondence EOF )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:203:1: ruleClassCorrespondence EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassCorrespondenceRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleClassCorrespondence_in_entryRuleClassCorrespondence367);
            ruleClassCorrespondence();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassCorrespondenceRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleClassCorrespondence374); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleClassCorrespondence"


    // $ANTLR start "ruleClassCorrespondence"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:210:1: ruleClassCorrespondence : ( ( rule__ClassCorrespondence__Group__0 ) ) ;
    public final void ruleClassCorrespondence() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:214:2: ( ( ( rule__ClassCorrespondence__Group__0 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:215:1: ( ( rule__ClassCorrespondence__Group__0 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:215:1: ( ( rule__ClassCorrespondence__Group__0 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:216:1: ( rule__ClassCorrespondence__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassCorrespondenceAccess().getGroup()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:217:1: ( rule__ClassCorrespondence__Group__0 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:217:2: rule__ClassCorrespondence__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group__0_in_ruleClassCorrespondence400);
            rule__ClassCorrespondence__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassCorrespondenceAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleClassCorrespondence"


    // $ANTLR start "entryRuleClassFunction"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:229:1: entryRuleClassFunction : ruleClassFunction EOF ;
    public final void entryRuleClassFunction() throws RecognitionException {
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:230:1: ( ruleClassFunction EOF )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:231:1: ruleClassFunction EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassFunctionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleClassFunction_in_entryRuleClassFunction427);
            ruleClassFunction();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassFunctionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleClassFunction434); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleClassFunction"


    // $ANTLR start "ruleClassFunction"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:238:1: ruleClassFunction : ( ( rule__ClassFunction__TermsAssignment ) ) ;
    public final void ruleClassFunction() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:242:2: ( ( ( rule__ClassFunction__TermsAssignment ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:243:1: ( ( rule__ClassFunction__TermsAssignment ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:243:1: ( ( rule__ClassFunction__TermsAssignment ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:244:1: ( rule__ClassFunction__TermsAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassFunctionAccess().getTermsAssignment()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:245:1: ( rule__ClassFunction__TermsAssignment )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:245:2: rule__ClassFunction__TermsAssignment
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassFunction__TermsAssignment_in_ruleClassFunction460);
            rule__ClassFunction__TermsAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassFunctionAccess().getTermsAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleClassFunction"


    // $ANTLR start "entryRuleClassTerm"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:257:1: entryRuleClassTerm : ruleClassTerm EOF ;
    public final void entryRuleClassTerm() throws RecognitionException {
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:258:1: ( ruleClassTerm EOF )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:259:1: ruleClassTerm EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassTermRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleClassTerm_in_entryRuleClassTerm487);
            ruleClassTerm();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassTermRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleClassTerm494); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleClassTerm"


    // $ANTLR start "ruleClassTerm"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:266:1: ruleClassTerm : ( ( rule__ClassTerm__Alternatives ) ) ;
    public final void ruleClassTerm() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:270:2: ( ( ( rule__ClassTerm__Alternatives ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:271:1: ( ( rule__ClassTerm__Alternatives ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:271:1: ( ( rule__ClassTerm__Alternatives ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:272:1: ( rule__ClassTerm__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassTermAccess().getAlternatives()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:273:1: ( rule__ClassTerm__Alternatives )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:273:2: rule__ClassTerm__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassTerm__Alternatives_in_ruleClassTerm520);
            rule__ClassTerm__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassTermAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleClassTerm"


    // $ANTLR start "entryRuleClassRef"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:285:1: entryRuleClassRef : ruleClassRef EOF ;
    public final void entryRuleClassRef() throws RecognitionException {
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:286:1: ( ruleClassRef EOF )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:287:1: ruleClassRef EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassRefRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleClassRef_in_entryRuleClassRef547);
            ruleClassRef();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassRefRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleClassRef554); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleClassRef"


    // $ANTLR start "ruleClassRef"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:294:1: ruleClassRef : ( ( rule__ClassRef__ReferencedClassAssignment ) ) ;
    public final void ruleClassRef() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:298:2: ( ( ( rule__ClassRef__ReferencedClassAssignment ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:299:1: ( ( rule__ClassRef__ReferencedClassAssignment ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:299:1: ( ( rule__ClassRef__ReferencedClassAssignment ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:300:1: ( rule__ClassRef__ReferencedClassAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassRefAccess().getReferencedClassAssignment()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:301:1: ( rule__ClassRef__ReferencedClassAssignment )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:301:2: rule__ClassRef__ReferencedClassAssignment
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassRef__ReferencedClassAssignment_in_ruleClassRef580);
            rule__ClassRef__ReferencedClassAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassRefAccess().getReferencedClassAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleClassRef"


    // $ANTLR start "entryRuleFeatureCorrespondence"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:313:1: entryRuleFeatureCorrespondence : ruleFeatureCorrespondence EOF ;
    public final void entryRuleFeatureCorrespondence() throws RecognitionException {
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:314:1: ( ruleFeatureCorrespondence EOF )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:315:1: ruleFeatureCorrespondence EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureCorrespondenceRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleFeatureCorrespondence_in_entryRuleFeatureCorrespondence607);
            ruleFeatureCorrespondence();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureCorrespondenceRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFeatureCorrespondence614); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFeatureCorrespondence"


    // $ANTLR start "ruleFeatureCorrespondence"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:322:1: ruleFeatureCorrespondence : ( ( rule__FeatureCorrespondence__Alternatives ) ) ;
    public final void ruleFeatureCorrespondence() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:326:2: ( ( ( rule__FeatureCorrespondence__Alternatives ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:327:1: ( ( rule__FeatureCorrespondence__Alternatives ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:327:1: ( ( rule__FeatureCorrespondence__Alternatives ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:328:1: ( rule__FeatureCorrespondence__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureCorrespondenceAccess().getAlternatives()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:329:1: ( rule__FeatureCorrespondence__Alternatives )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:329:2: rule__FeatureCorrespondence__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Alternatives_in_ruleFeatureCorrespondence640);
            rule__FeatureCorrespondence__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureCorrespondenceAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFeatureCorrespondence"


    // $ANTLR start "entryRuleFeatureFunction"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:341:1: entryRuleFeatureFunction : ruleFeatureFunction EOF ;
    public final void entryRuleFeatureFunction() throws RecognitionException {
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:342:1: ( ruleFeatureFunction EOF )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:343:1: ruleFeatureFunction EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureFunctionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleFeatureFunction_in_entryRuleFeatureFunction667);
            ruleFeatureFunction();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureFunctionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFeatureFunction674); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFeatureFunction"


    // $ANTLR start "ruleFeatureFunction"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:350:1: ruleFeatureFunction : ( ( rule__FeatureFunction__Group__0 ) ) ;
    public final void ruleFeatureFunction() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:354:2: ( ( ( rule__FeatureFunction__Group__0 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:355:1: ( ( rule__FeatureFunction__Group__0 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:355:1: ( ( rule__FeatureFunction__Group__0 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:356:1: ( rule__FeatureFunction__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureFunctionAccess().getGroup()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:357:1: ( rule__FeatureFunction__Group__0 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:357:2: rule__FeatureFunction__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureFunction__Group__0_in_ruleFeatureFunction700);
            rule__FeatureFunction__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureFunctionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFeatureFunction"


    // $ANTLR start "entryRuleFeatureTerm"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:369:1: entryRuleFeatureTerm : ruleFeatureTerm EOF ;
    public final void entryRuleFeatureTerm() throws RecognitionException {
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:370:1: ( ruleFeatureTerm EOF )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:371:1: ruleFeatureTerm EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureTermRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleFeatureTerm_in_entryRuleFeatureTerm727);
            ruleFeatureTerm();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureTermRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFeatureTerm734); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFeatureTerm"


    // $ANTLR start "ruleFeatureTerm"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:378:1: ruleFeatureTerm : ( ( rule__FeatureTerm__Alternatives ) ) ;
    public final void ruleFeatureTerm() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:382:2: ( ( ( rule__FeatureTerm__Alternatives ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:383:1: ( ( rule__FeatureTerm__Alternatives ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:383:1: ( ( rule__FeatureTerm__Alternatives ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:384:1: ( rule__FeatureTerm__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureTermAccess().getAlternatives()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:385:1: ( rule__FeatureTerm__Alternatives )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:385:2: rule__FeatureTerm__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureTerm__Alternatives_in_ruleFeatureTerm760);
            rule__FeatureTerm__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureTermAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFeatureTerm"


    // $ANTLR start "entryRuleFeatureRef"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:397:1: entryRuleFeatureRef : ruleFeatureRef EOF ;
    public final void entryRuleFeatureRef() throws RecognitionException {
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:398:1: ( ruleFeatureRef EOF )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:399:1: ruleFeatureRef EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureRefRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleFeatureRef_in_entryRuleFeatureRef787);
            ruleFeatureRef();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureRefRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFeatureRef794); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFeatureRef"


    // $ANTLR start "ruleFeatureRef"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:406:1: ruleFeatureRef : ( ( rule__FeatureRef__Group__0 ) ) ;
    public final void ruleFeatureRef() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:410:2: ( ( ( rule__FeatureRef__Group__0 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:411:1: ( ( rule__FeatureRef__Group__0 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:411:1: ( ( rule__FeatureRef__Group__0 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:412:1: ( rule__FeatureRef__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureRefAccess().getGroup()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:413:1: ( rule__FeatureRef__Group__0 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:413:2: rule__FeatureRef__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureRef__Group__0_in_ruleFeatureRef820);
            rule__FeatureRef__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureRefAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFeatureRef"


    // $ANTLR start "entryRuleInstance"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:425:1: entryRuleInstance : ruleInstance EOF ;
    public final void entryRuleInstance() throws RecognitionException {
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:426:1: ( ruleInstance EOF )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:427:1: ruleInstance EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInstanceRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleInstance_in_entryRuleInstance847);
            ruleInstance();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getInstanceRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleInstance854); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInstance"


    // $ANTLR start "ruleInstance"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:434:1: ruleInstance : ( ( rule__Instance__Group__0 ) ) ;
    public final void ruleInstance() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:438:2: ( ( ( rule__Instance__Group__0 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:439:1: ( ( rule__Instance__Group__0 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:439:1: ( ( rule__Instance__Group__0 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:440:1: ( rule__Instance__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInstanceAccess().getGroup()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:441:1: ( rule__Instance__Group__0 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:441:2: rule__Instance__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Instance__Group__0_in_ruleInstance880);
            rule__Instance__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getInstanceAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInstance"


    // $ANTLR start "entryRuleAssignment"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:453:1: entryRuleAssignment : ruleAssignment EOF ;
    public final void entryRuleAssignment() throws RecognitionException {
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:454:1: ( ruleAssignment EOF )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:455:1: ruleAssignment EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAssignmentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleAssignment_in_entryRuleAssignment907);
            ruleAssignment();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAssignmentRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleAssignment914); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAssignment"


    // $ANTLR start "ruleAssignment"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:462:1: ruleAssignment : ( ( rule__Assignment__Group__0 ) ) ;
    public final void ruleAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:466:2: ( ( ( rule__Assignment__Group__0 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:467:1: ( ( rule__Assignment__Group__0 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:467:1: ( ( rule__Assignment__Group__0 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:468:1: ( rule__Assignment__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAssignmentAccess().getGroup()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:469:1: ( rule__Assignment__Group__0 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:469:2: rule__Assignment__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Assignment__Group__0_in_ruleAssignment940);
            rule__Assignment__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAssignmentAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAssignment"


    // $ANTLR start "entryRuleTerm"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:481:1: entryRuleTerm : ruleTerm EOF ;
    public final void entryRuleTerm() throws RecognitionException {
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:482:1: ( ruleTerm EOF )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:483:1: ruleTerm EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTermRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleTerm_in_entryRuleTerm967);
            ruleTerm();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTermRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleTerm974); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTerm"


    // $ANTLR start "ruleTerm"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:490:1: ruleTerm : ( ( rule__Term__Alternatives ) ) ;
    public final void ruleTerm() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:494:2: ( ( ( rule__Term__Alternatives ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:495:1: ( ( rule__Term__Alternatives ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:495:1: ( ( rule__Term__Alternatives ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:496:1: ( rule__Term__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTermAccess().getAlternatives()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:497:1: ( rule__Term__Alternatives )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:497:2: rule__Term__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__Term__Alternatives_in_ruleTerm1000);
            rule__Term__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTermAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTerm"


    // $ANTLR start "entryRuleThisRef"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:509:1: entryRuleThisRef : ruleThisRef EOF ;
    public final void entryRuleThisRef() throws RecognitionException {
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:510:1: ( ruleThisRef EOF )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:511:1: ruleThisRef EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getThisRefRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleThisRef_in_entryRuleThisRef1027);
            ruleThisRef();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getThisRefRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleThisRef1034); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleThisRef"


    // $ANTLR start "ruleThisRef"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:518:1: ruleThisRef : ( ( rule__ThisRef__Group__0 ) ) ;
    public final void ruleThisRef() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:522:2: ( ( ( rule__ThisRef__Group__0 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:523:1: ( ( rule__ThisRef__Group__0 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:523:1: ( ( rule__ThisRef__Group__0 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:524:1: ( rule__ThisRef__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getThisRefAccess().getGroup()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:525:1: ( rule__ThisRef__Group__0 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:525:2: rule__ThisRef__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__ThisRef__Group__0_in_ruleThisRef1060);
            rule__ThisRef__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getThisRefAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleThisRef"


    // $ANTLR start "entryRuleValue"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:537:1: entryRuleValue : ruleValue EOF ;
    public final void entryRuleValue() throws RecognitionException {
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:538:1: ( ruleValue EOF )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:539:1: ruleValue EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleValue_in_entryRuleValue1087);
            ruleValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getValueRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleValue1094); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleValue"


    // $ANTLR start "ruleValue"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:546:1: ruleValue : ( ( rule__Value__Alternatives ) ) ;
    public final void ruleValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:550:2: ( ( ( rule__Value__Alternatives ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:551:1: ( ( rule__Value__Alternatives ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:551:1: ( ( rule__Value__Alternatives ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:552:1: ( rule__Value__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getValueAccess().getAlternatives()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:553:1: ( rule__Value__Alternatives )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:553:2: rule__Value__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__Value__Alternatives_in_ruleValue1120);
            rule__Value__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getValueAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleValue"


    // $ANTLR start "entryRuleCollectionValue"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:565:1: entryRuleCollectionValue : ruleCollectionValue EOF ;
    public final void entryRuleCollectionValue() throws RecognitionException {
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:566:1: ( ruleCollectionValue EOF )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:567:1: ruleCollectionValue EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleCollectionValue_in_entryRuleCollectionValue1147);
            ruleCollectionValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionValueRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleCollectionValue1154); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCollectionValue"


    // $ANTLR start "ruleCollectionValue"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:574:1: ruleCollectionValue : ( ( rule__CollectionValue__Group__0 ) ) ;
    public final void ruleCollectionValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:578:2: ( ( ( rule__CollectionValue__Group__0 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:579:1: ( ( rule__CollectionValue__Group__0 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:579:1: ( ( rule__CollectionValue__Group__0 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:580:1: ( rule__CollectionValue__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionValueAccess().getGroup()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:581:1: ( rule__CollectionValue__Group__0 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:581:2: rule__CollectionValue__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__CollectionValue__Group__0_in_ruleCollectionValue1180);
            rule__CollectionValue__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionValueAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCollectionValue"


    // $ANTLR start "entryRuleStringValue"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:593:1: entryRuleStringValue : ruleStringValue EOF ;
    public final void entryRuleStringValue() throws RecognitionException {
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:594:1: ( ruleStringValue EOF )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:595:1: ruleStringValue EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStringValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleStringValue_in_entryRuleStringValue1207);
            ruleStringValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getStringValueRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleStringValue1214); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStringValue"


    // $ANTLR start "ruleStringValue"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:602:1: ruleStringValue : ( ( rule__StringValue__ValueAssignment ) ) ;
    public final void ruleStringValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:606:2: ( ( ( rule__StringValue__ValueAssignment ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:607:1: ( ( rule__StringValue__ValueAssignment ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:607:1: ( ( rule__StringValue__ValueAssignment ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:608:1: ( rule__StringValue__ValueAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStringValueAccess().getValueAssignment()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:609:1: ( rule__StringValue__ValueAssignment )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:609:2: rule__StringValue__ValueAssignment
            {
            pushFollow(FollowSets000.FOLLOW_rule__StringValue__ValueAssignment_in_ruleStringValue1240);
            rule__StringValue__ValueAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getStringValueAccess().getValueAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStringValue"


    // $ANTLR start "entryRuleIntValue"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:621:1: entryRuleIntValue : ruleIntValue EOF ;
    public final void entryRuleIntValue() throws RecognitionException {
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:622:1: ( ruleIntValue EOF )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:623:1: ruleIntValue EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIntValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleIntValue_in_entryRuleIntValue1267);
            ruleIntValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getIntValueRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleIntValue1274); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIntValue"


    // $ANTLR start "ruleIntValue"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:630:1: ruleIntValue : ( ( rule__IntValue__ValueAssignment ) ) ;
    public final void ruleIntValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:634:2: ( ( ( rule__IntValue__ValueAssignment ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:635:1: ( ( rule__IntValue__ValueAssignment ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:635:1: ( ( rule__IntValue__ValueAssignment ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:636:1: ( rule__IntValue__ValueAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIntValueAccess().getValueAssignment()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:637:1: ( rule__IntValue__ValueAssignment )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:637:2: rule__IntValue__ValueAssignment
            {
            pushFollow(FollowSets000.FOLLOW_rule__IntValue__ValueAssignment_in_ruleIntValue1300);
            rule__IntValue__ValueAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getIntValueAccess().getValueAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIntValue"


    // $ANTLR start "entryRuleQualifiedName"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:649:1: entryRuleQualifiedName : ruleQualifiedName EOF ;
    public final void entryRuleQualifiedName() throws RecognitionException {
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:650:1: ( ruleQualifiedName EOF )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:651:1: ruleQualifiedName EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getQualifiedNameRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleQualifiedName_in_entryRuleQualifiedName1327);
            ruleQualifiedName();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getQualifiedNameRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleQualifiedName1334); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:658:1: ruleQualifiedName : ( ( rule__QualifiedName__Group__0 ) ) ;
    public final void ruleQualifiedName() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:662:2: ( ( ( rule__QualifiedName__Group__0 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:663:1: ( ( rule__QualifiedName__Group__0 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:663:1: ( ( rule__QualifiedName__Group__0 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:664:1: ( rule__QualifiedName__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getQualifiedNameAccess().getGroup()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:665:1: ( rule__QualifiedName__Group__0 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:665:2: rule__QualifiedName__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__QualifiedName__Group__0_in_ruleQualifiedName1360);
            rule__QualifiedName__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getQualifiedNameAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "rule__ClassCorrespondence__Alternatives_0"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:679:1: rule__ClassCorrespondence__Alternatives_0 : ( ( ( rule__ClassCorrespondence__Group_0_0__0 ) ) | ( ( rule__ClassCorrespondence__Group_0_1__0 ) ) | ( ( rule__ClassCorrespondence__Group_0_2__0 ) ) | ( ( rule__ClassCorrespondence__Group_0_3__0 ) ) );
    public final void rule__ClassCorrespondence__Alternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:683:1: ( ( ( rule__ClassCorrespondence__Group_0_0__0 ) ) | ( ( rule__ClassCorrespondence__Group_0_1__0 ) ) | ( ( rule__ClassCorrespondence__Group_0_2__0 ) ) | ( ( rule__ClassCorrespondence__Group_0_3__0 ) ) )
            int alt1=4;
            switch ( input.LA(1) ) {
            case RULE_ID:
                {
                int LA1_1 = input.LA(2);

                if ( (synpred1_InternalCorrespondence()) ) {
                    alt1=1;
                }
                else if ( (synpred2_InternalCorrespondence()) ) {
                    alt1=2;
                }
                else if ( (synpred3_InternalCorrespondence()) ) {
                    alt1=3;
                }
                else if ( (true) ) {
                    alt1=4;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return ;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 1, 1, input);

                    throw nvae;
                }
                }
                break;
            case 20:
                {
                int LA1_2 = input.LA(2);

                if ( (synpred1_InternalCorrespondence()) ) {
                    alt1=1;
                }
                else if ( (synpred2_InternalCorrespondence()) ) {
                    alt1=2;
                }
                else if ( (synpred3_InternalCorrespondence()) ) {
                    alt1=3;
                }
                else if ( (true) ) {
                    alt1=4;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return ;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 1, 2, input);

                    throw nvae;
                }
                }
                break;
            case 21:
                {
                int LA1_3 = input.LA(2);

                if ( (synpred1_InternalCorrespondence()) ) {
                    alt1=1;
                }
                else if ( (synpred2_InternalCorrespondence()) ) {
                    alt1=2;
                }
                else if ( (synpred3_InternalCorrespondence()) ) {
                    alt1=3;
                }
                else if ( (true) ) {
                    alt1=4;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return ;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 1, 3, input);

                    throw nvae;
                }
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:684:1: ( ( rule__ClassCorrespondence__Group_0_0__0 ) )
                    {
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:684:1: ( ( rule__ClassCorrespondence__Group_0_0__0 ) )
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:685:1: ( rule__ClassCorrespondence__Group_0_0__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getClassCorrespondenceAccess().getGroup_0_0()); 
                    }
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:686:1: ( rule__ClassCorrespondence__Group_0_0__0 )
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:686:2: rule__ClassCorrespondence__Group_0_0__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_0__0_in_rule__ClassCorrespondence__Alternatives_01398);
                    rule__ClassCorrespondence__Group_0_0__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getClassCorrespondenceAccess().getGroup_0_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:690:6: ( ( rule__ClassCorrespondence__Group_0_1__0 ) )
                    {
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:690:6: ( ( rule__ClassCorrespondence__Group_0_1__0 ) )
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:691:1: ( rule__ClassCorrespondence__Group_0_1__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getClassCorrespondenceAccess().getGroup_0_1()); 
                    }
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:692:1: ( rule__ClassCorrespondence__Group_0_1__0 )
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:692:2: rule__ClassCorrespondence__Group_0_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_1__0_in_rule__ClassCorrespondence__Alternatives_01416);
                    rule__ClassCorrespondence__Group_0_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getClassCorrespondenceAccess().getGroup_0_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:696:6: ( ( rule__ClassCorrespondence__Group_0_2__0 ) )
                    {
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:696:6: ( ( rule__ClassCorrespondence__Group_0_2__0 ) )
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:697:1: ( rule__ClassCorrespondence__Group_0_2__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getClassCorrespondenceAccess().getGroup_0_2()); 
                    }
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:698:1: ( rule__ClassCorrespondence__Group_0_2__0 )
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:698:2: rule__ClassCorrespondence__Group_0_2__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_2__0_in_rule__ClassCorrespondence__Alternatives_01434);
                    rule__ClassCorrespondence__Group_0_2__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getClassCorrespondenceAccess().getGroup_0_2()); 
                    }

                    }


                    }
                    break;
                case 4 :
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:702:6: ( ( rule__ClassCorrespondence__Group_0_3__0 ) )
                    {
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:702:6: ( ( rule__ClassCorrespondence__Group_0_3__0 ) )
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:703:1: ( rule__ClassCorrespondence__Group_0_3__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getClassCorrespondenceAccess().getGroup_0_3()); 
                    }
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:704:1: ( rule__ClassCorrespondence__Group_0_3__0 )
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:704:2: rule__ClassCorrespondence__Group_0_3__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_3__0_in_rule__ClassCorrespondence__Alternatives_01452);
                    rule__ClassCorrespondence__Group_0_3__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getClassCorrespondenceAccess().getGroup_0_3()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Alternatives_0"


    // $ANTLR start "rule__ClassTerm__Alternatives"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:713:1: rule__ClassTerm__Alternatives : ( ( ruleClassRef ) | ( ruleInstance ) );
    public final void rule__ClassTerm__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:717:1: ( ( ruleClassRef ) | ( ruleInstance ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==RULE_ID) ) {
                alt2=1;
            }
            else if ( ((LA2_0>=20 && LA2_0<=21)) ) {
                alt2=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:718:1: ( ruleClassRef )
                    {
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:718:1: ( ruleClassRef )
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:719:1: ruleClassRef
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getClassTermAccess().getClassRefParserRuleCall_0()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleClassRef_in_rule__ClassTerm__Alternatives1485);
                    ruleClassRef();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getClassTermAccess().getClassRefParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:724:6: ( ruleInstance )
                    {
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:724:6: ( ruleInstance )
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:725:1: ruleInstance
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getClassTermAccess().getInstanceParserRuleCall_1()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleInstance_in_rule__ClassTerm__Alternatives1502);
                    ruleInstance();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getClassTermAccess().getInstanceParserRuleCall_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassTerm__Alternatives"


    // $ANTLR start "rule__FeatureCorrespondence__Alternatives"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:735:1: rule__FeatureCorrespondence__Alternatives : ( ( ( rule__FeatureCorrespondence__Group_0__0 ) ) | ( ( rule__FeatureCorrespondence__Group_1__0 ) ) | ( ( rule__FeatureCorrespondence__Group_2__0 ) ) | ( ( rule__FeatureCorrespondence__Group_3__0 ) ) );
    public final void rule__FeatureCorrespondence__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:739:1: ( ( ( rule__FeatureCorrespondence__Group_0__0 ) ) | ( ( rule__FeatureCorrespondence__Group_1__0 ) ) | ( ( rule__FeatureCorrespondence__Group_2__0 ) ) | ( ( rule__FeatureCorrespondence__Group_3__0 ) ) )
            int alt3=4;
            alt3 = dfa3.predict(input);
            switch (alt3) {
                case 1 :
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:740:1: ( ( rule__FeatureCorrespondence__Group_0__0 ) )
                    {
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:740:1: ( ( rule__FeatureCorrespondence__Group_0__0 ) )
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:741:1: ( rule__FeatureCorrespondence__Group_0__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getFeatureCorrespondenceAccess().getGroup_0()); 
                    }
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:742:1: ( rule__FeatureCorrespondence__Group_0__0 )
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:742:2: rule__FeatureCorrespondence__Group_0__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_0__0_in_rule__FeatureCorrespondence__Alternatives1534);
                    rule__FeatureCorrespondence__Group_0__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getFeatureCorrespondenceAccess().getGroup_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:746:6: ( ( rule__FeatureCorrespondence__Group_1__0 ) )
                    {
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:746:6: ( ( rule__FeatureCorrespondence__Group_1__0 ) )
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:747:1: ( rule__FeatureCorrespondence__Group_1__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getFeatureCorrespondenceAccess().getGroup_1()); 
                    }
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:748:1: ( rule__FeatureCorrespondence__Group_1__0 )
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:748:2: rule__FeatureCorrespondence__Group_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_1__0_in_rule__FeatureCorrespondence__Alternatives1552);
                    rule__FeatureCorrespondence__Group_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getFeatureCorrespondenceAccess().getGroup_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:752:6: ( ( rule__FeatureCorrespondence__Group_2__0 ) )
                    {
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:752:6: ( ( rule__FeatureCorrespondence__Group_2__0 ) )
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:753:1: ( rule__FeatureCorrespondence__Group_2__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getFeatureCorrespondenceAccess().getGroup_2()); 
                    }
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:754:1: ( rule__FeatureCorrespondence__Group_2__0 )
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:754:2: rule__FeatureCorrespondence__Group_2__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_2__0_in_rule__FeatureCorrespondence__Alternatives1570);
                    rule__FeatureCorrespondence__Group_2__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getFeatureCorrespondenceAccess().getGroup_2()); 
                    }

                    }


                    }
                    break;
                case 4 :
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:758:6: ( ( rule__FeatureCorrespondence__Group_3__0 ) )
                    {
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:758:6: ( ( rule__FeatureCorrespondence__Group_3__0 ) )
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:759:1: ( rule__FeatureCorrespondence__Group_3__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getFeatureCorrespondenceAccess().getGroup_3()); 
                    }
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:760:1: ( rule__FeatureCorrespondence__Group_3__0 )
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:760:2: rule__FeatureCorrespondence__Group_3__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_3__0_in_rule__FeatureCorrespondence__Alternatives1588);
                    rule__FeatureCorrespondence__Group_3__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getFeatureCorrespondenceAccess().getGroup_3()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__Alternatives"


    // $ANTLR start "rule__FeatureTerm__Alternatives"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:769:1: rule__FeatureTerm__Alternatives : ( ( ruleFeatureRef ) | ( ruleValue ) | ( ruleInstance ) );
    public final void rule__FeatureTerm__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:773:1: ( ( ruleFeatureRef ) | ( ruleValue ) | ( ruleInstance ) )
            int alt4=3;
            switch ( input.LA(1) ) {
            case 20:
                {
                int LA4_1 = input.LA(2);

                if ( (synpred8_InternalCorrespondence()) ) {
                    alt4=1;
                }
                else if ( (true) ) {
                    alt4=3;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return ;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 4, 1, input);

                    throw nvae;
                }
                }
                break;
            case 21:
                {
                int LA4_2 = input.LA(2);

                if ( (synpred8_InternalCorrespondence()) ) {
                    alt4=1;
                }
                else if ( (true) ) {
                    alt4=3;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return ;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 4, 2, input);

                    throw nvae;
                }
                }
                break;
            case RULE_ID:
                {
                alt4=1;
                }
                break;
            case RULE_STRING:
            case RULE_INT:
            case 24:
                {
                alt4=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:774:1: ( ruleFeatureRef )
                    {
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:774:1: ( ruleFeatureRef )
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:775:1: ruleFeatureRef
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getFeatureTermAccess().getFeatureRefParserRuleCall_0()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleFeatureRef_in_rule__FeatureTerm__Alternatives1621);
                    ruleFeatureRef();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getFeatureTermAccess().getFeatureRefParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:780:6: ( ruleValue )
                    {
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:780:6: ( ruleValue )
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:781:1: ruleValue
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getFeatureTermAccess().getValueParserRuleCall_1()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleValue_in_rule__FeatureTerm__Alternatives1638);
                    ruleValue();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getFeatureTermAccess().getValueParserRuleCall_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:786:6: ( ruleInstance )
                    {
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:786:6: ( ruleInstance )
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:787:1: ruleInstance
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getFeatureTermAccess().getInstanceParserRuleCall_2()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleInstance_in_rule__FeatureTerm__Alternatives1655);
                    ruleInstance();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getFeatureTermAccess().getInstanceParserRuleCall_2()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureTerm__Alternatives"


    // $ANTLR start "rule__Instance__Alternatives_0"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:797:1: rule__Instance__Alternatives_0 : ( ( ( rule__Instance__Group_0_0__0 ) ) | ( ( rule__Instance__Group_0_1__0 ) ) );
    public final void rule__Instance__Alternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:801:1: ( ( ( rule__Instance__Group_0_0__0 ) ) | ( ( rule__Instance__Group_0_1__0 ) ) )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==20) ) {
                alt5=1;
            }
            else if ( (LA5_0==21) ) {
                alt5=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:802:1: ( ( rule__Instance__Group_0_0__0 ) )
                    {
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:802:1: ( ( rule__Instance__Group_0_0__0 ) )
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:803:1: ( rule__Instance__Group_0_0__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getInstanceAccess().getGroup_0_0()); 
                    }
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:804:1: ( rule__Instance__Group_0_0__0 )
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:804:2: rule__Instance__Group_0_0__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Instance__Group_0_0__0_in_rule__Instance__Alternatives_01687);
                    rule__Instance__Group_0_0__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getInstanceAccess().getGroup_0_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:808:6: ( ( rule__Instance__Group_0_1__0 ) )
                    {
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:808:6: ( ( rule__Instance__Group_0_1__0 ) )
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:809:1: ( rule__Instance__Group_0_1__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getInstanceAccess().getGroup_0_1()); 
                    }
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:810:1: ( rule__Instance__Group_0_1__0 )
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:810:2: rule__Instance__Group_0_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Instance__Group_0_1__0_in_rule__Instance__Alternatives_01705);
                    rule__Instance__Group_0_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getInstanceAccess().getGroup_0_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instance__Alternatives_0"


    // $ANTLR start "rule__Term__Alternatives"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:819:1: rule__Term__Alternatives : ( ( ruleValue ) | ( ( ruleInstance ) ) | ( ruleThisRef ) );
    public final void rule__Term__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:823:1: ( ( ruleValue ) | ( ( ruleInstance ) ) | ( ruleThisRef ) )
            int alt6=3;
            switch ( input.LA(1) ) {
            case RULE_STRING:
            case RULE_INT:
            case 24:
                {
                alt6=1;
                }
                break;
            case 20:
            case 21:
                {
                alt6=2;
                }
                break;
            case 23:
                {
                alt6=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:824:1: ( ruleValue )
                    {
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:824:1: ( ruleValue )
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:825:1: ruleValue
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTermAccess().getValueParserRuleCall_0()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleValue_in_rule__Term__Alternatives1738);
                    ruleValue();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTermAccess().getValueParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:830:6: ( ( ruleInstance ) )
                    {
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:830:6: ( ( ruleInstance ) )
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:831:1: ( ruleInstance )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTermAccess().getInstanceParserRuleCall_1()); 
                    }
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:832:1: ( ruleInstance )
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:832:3: ruleInstance
                    {
                    pushFollow(FollowSets000.FOLLOW_ruleInstance_in_rule__Term__Alternatives1756);
                    ruleInstance();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTermAccess().getInstanceParserRuleCall_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:836:6: ( ruleThisRef )
                    {
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:836:6: ( ruleThisRef )
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:837:1: ruleThisRef
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTermAccess().getThisRefParserRuleCall_2()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleThisRef_in_rule__Term__Alternatives1774);
                    ruleThisRef();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTermAccess().getThisRefParserRuleCall_2()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Term__Alternatives"


    // $ANTLR start "rule__Value__Alternatives"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:847:1: rule__Value__Alternatives : ( ( ruleStringValue ) | ( ruleIntValue ) | ( ruleCollectionValue ) );
    public final void rule__Value__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:851:1: ( ( ruleStringValue ) | ( ruleIntValue ) | ( ruleCollectionValue ) )
            int alt7=3;
            switch ( input.LA(1) ) {
            case RULE_STRING:
                {
                alt7=1;
                }
                break;
            case RULE_INT:
                {
                alt7=2;
                }
                break;
            case 24:
                {
                alt7=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }

            switch (alt7) {
                case 1 :
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:852:1: ( ruleStringValue )
                    {
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:852:1: ( ruleStringValue )
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:853:1: ruleStringValue
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getValueAccess().getStringValueParserRuleCall_0()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleStringValue_in_rule__Value__Alternatives1806);
                    ruleStringValue();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getValueAccess().getStringValueParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:858:6: ( ruleIntValue )
                    {
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:858:6: ( ruleIntValue )
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:859:1: ruleIntValue
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getValueAccess().getIntValueParserRuleCall_1()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleIntValue_in_rule__Value__Alternatives1823);
                    ruleIntValue();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getValueAccess().getIntValueParserRuleCall_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:864:6: ( ruleCollectionValue )
                    {
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:864:6: ( ruleCollectionValue )
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:865:1: ruleCollectionValue
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getValueAccess().getCollectionValueParserRuleCall_2()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleCollectionValue_in_rule__Value__Alternatives1840);
                    ruleCollectionValue();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getValueAccess().getCollectionValueParserRuleCall_2()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value__Alternatives"


    // $ANTLR start "rule__Correspondences__Group__0"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:877:1: rule__Correspondences__Group__0 : rule__Correspondences__Group__0__Impl rule__Correspondences__Group__1 ;
    public final void rule__Correspondences__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:881:1: ( rule__Correspondences__Group__0__Impl rule__Correspondences__Group__1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:882:2: rule__Correspondences__Group__0__Impl rule__Correspondences__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Correspondences__Group__0__Impl_in_rule__Correspondences__Group__01870);
            rule__Correspondences__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Correspondences__Group__1_in_rule__Correspondences__Group__01873);
            rule__Correspondences__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Correspondences__Group__0"


    // $ANTLR start "rule__Correspondences__Group__0__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:889:1: rule__Correspondences__Group__0__Impl : ( ( rule__Correspondences__ImportsAssignment_0 )* ) ;
    public final void rule__Correspondences__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:893:1: ( ( ( rule__Correspondences__ImportsAssignment_0 )* ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:894:1: ( ( rule__Correspondences__ImportsAssignment_0 )* )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:894:1: ( ( rule__Correspondences__ImportsAssignment_0 )* )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:895:1: ( rule__Correspondences__ImportsAssignment_0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCorrespondencesAccess().getImportsAssignment_0()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:896:1: ( rule__Correspondences__ImportsAssignment_0 )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==11) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:896:2: rule__Correspondences__ImportsAssignment_0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Correspondences__ImportsAssignment_0_in_rule__Correspondences__Group__0__Impl1900);
            	    rule__Correspondences__ImportsAssignment_0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCorrespondencesAccess().getImportsAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Correspondences__Group__0__Impl"


    // $ANTLR start "rule__Correspondences__Group__1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:906:1: rule__Correspondences__Group__1 : rule__Correspondences__Group__1__Impl ;
    public final void rule__Correspondences__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:910:1: ( rule__Correspondences__Group__1__Impl )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:911:2: rule__Correspondences__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Correspondences__Group__1__Impl_in_rule__Correspondences__Group__11931);
            rule__Correspondences__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Correspondences__Group__1"


    // $ANTLR start "rule__Correspondences__Group__1__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:917:1: rule__Correspondences__Group__1__Impl : ( ( rule__Correspondences__CorrespondencesAssignment_1 ) ) ;
    public final void rule__Correspondences__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:921:1: ( ( ( rule__Correspondences__CorrespondencesAssignment_1 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:922:1: ( ( rule__Correspondences__CorrespondencesAssignment_1 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:922:1: ( ( rule__Correspondences__CorrespondencesAssignment_1 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:923:1: ( rule__Correspondences__CorrespondencesAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCorrespondencesAccess().getCorrespondencesAssignment_1()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:924:1: ( rule__Correspondences__CorrespondencesAssignment_1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:924:2: rule__Correspondences__CorrespondencesAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Correspondences__CorrespondencesAssignment_1_in_rule__Correspondences__Group__1__Impl1958);
            rule__Correspondences__CorrespondencesAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCorrespondencesAccess().getCorrespondencesAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Correspondences__Group__1__Impl"


    // $ANTLR start "rule__Import__Group__0"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:938:1: rule__Import__Group__0 : rule__Import__Group__0__Impl rule__Import__Group__1 ;
    public final void rule__Import__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:942:1: ( rule__Import__Group__0__Impl rule__Import__Group__1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:943:2: rule__Import__Group__0__Impl rule__Import__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Import__Group__0__Impl_in_rule__Import__Group__01992);
            rule__Import__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Import__Group__1_in_rule__Import__Group__01995);
            rule__Import__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0"


    // $ANTLR start "rule__Import__Group__0__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:950:1: rule__Import__Group__0__Impl : ( 'import' ) ;
    public final void rule__Import__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:954:1: ( ( 'import' ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:955:1: ( 'import' )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:955:1: ( 'import' )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:956:1: 'import'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImportAccess().getImportKeyword_0()); 
            }
            match(input,11,FollowSets000.FOLLOW_11_in_rule__Import__Group__0__Impl2023); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getImportAccess().getImportKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0__Impl"


    // $ANTLR start "rule__Import__Group__1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:969:1: rule__Import__Group__1 : rule__Import__Group__1__Impl ;
    public final void rule__Import__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:973:1: ( rule__Import__Group__1__Impl )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:974:2: rule__Import__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Import__Group__1__Impl_in_rule__Import__Group__12054);
            rule__Import__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1"


    // $ANTLR start "rule__Import__Group__1__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:980:1: rule__Import__Group__1__Impl : ( ( rule__Import__ImportURIAssignment_1 ) ) ;
    public final void rule__Import__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:984:1: ( ( ( rule__Import__ImportURIAssignment_1 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:985:1: ( ( rule__Import__ImportURIAssignment_1 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:985:1: ( ( rule__Import__ImportURIAssignment_1 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:986:1: ( rule__Import__ImportURIAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImportAccess().getImportURIAssignment_1()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:987:1: ( rule__Import__ImportURIAssignment_1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:987:2: rule__Import__ImportURIAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Import__ImportURIAssignment_1_in_rule__Import__Group__1__Impl2081);
            rule__Import__ImportURIAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getImportAccess().getImportURIAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1__Impl"


    // $ANTLR start "rule__ModelCorrespondence__Group__0"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1001:1: rule__ModelCorrespondence__Group__0 : rule__ModelCorrespondence__Group__0__Impl rule__ModelCorrespondence__Group__1 ;
    public final void rule__ModelCorrespondence__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1005:1: ( rule__ModelCorrespondence__Group__0__Impl rule__ModelCorrespondence__Group__1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1006:2: rule__ModelCorrespondence__Group__0__Impl rule__ModelCorrespondence__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__ModelCorrespondence__Group__0__Impl_in_rule__ModelCorrespondence__Group__02115);
            rule__ModelCorrespondence__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__ModelCorrespondence__Group__1_in_rule__ModelCorrespondence__Group__02118);
            rule__ModelCorrespondence__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModelCorrespondence__Group__0"


    // $ANTLR start "rule__ModelCorrespondence__Group__0__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1013:1: rule__ModelCorrespondence__Group__0__Impl : ( () ) ;
    public final void rule__ModelCorrespondence__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1017:1: ( ( () ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1018:1: ( () )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1018:1: ( () )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1019:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModelCorrespondenceAccess().getBidirectionalCorrespondenceAction_0()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1020:1: ()
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1022:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getModelCorrespondenceAccess().getBidirectionalCorrespondenceAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModelCorrespondence__Group__0__Impl"


    // $ANTLR start "rule__ModelCorrespondence__Group__1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1032:1: rule__ModelCorrespondence__Group__1 : rule__ModelCorrespondence__Group__1__Impl rule__ModelCorrespondence__Group__2 ;
    public final void rule__ModelCorrespondence__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1036:1: ( rule__ModelCorrespondence__Group__1__Impl rule__ModelCorrespondence__Group__2 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1037:2: rule__ModelCorrespondence__Group__1__Impl rule__ModelCorrespondence__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__ModelCorrespondence__Group__1__Impl_in_rule__ModelCorrespondence__Group__12176);
            rule__ModelCorrespondence__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__ModelCorrespondence__Group__2_in_rule__ModelCorrespondence__Group__12179);
            rule__ModelCorrespondence__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModelCorrespondence__Group__1"


    // $ANTLR start "rule__ModelCorrespondence__Group__1__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1044:1: rule__ModelCorrespondence__Group__1__Impl : ( ( rule__ModelCorrespondence__LeftAssignment_1 ) ) ;
    public final void rule__ModelCorrespondence__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1048:1: ( ( ( rule__ModelCorrespondence__LeftAssignment_1 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1049:1: ( ( rule__ModelCorrespondence__LeftAssignment_1 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1049:1: ( ( rule__ModelCorrespondence__LeftAssignment_1 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1050:1: ( rule__ModelCorrespondence__LeftAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModelCorrespondenceAccess().getLeftAssignment_1()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1051:1: ( rule__ModelCorrespondence__LeftAssignment_1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1051:2: rule__ModelCorrespondence__LeftAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__ModelCorrespondence__LeftAssignment_1_in_rule__ModelCorrespondence__Group__1__Impl2206);
            rule__ModelCorrespondence__LeftAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getModelCorrespondenceAccess().getLeftAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModelCorrespondence__Group__1__Impl"


    // $ANTLR start "rule__ModelCorrespondence__Group__2"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1061:1: rule__ModelCorrespondence__Group__2 : rule__ModelCorrespondence__Group__2__Impl rule__ModelCorrespondence__Group__3 ;
    public final void rule__ModelCorrespondence__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1065:1: ( rule__ModelCorrespondence__Group__2__Impl rule__ModelCorrespondence__Group__3 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1066:2: rule__ModelCorrespondence__Group__2__Impl rule__ModelCorrespondence__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__ModelCorrespondence__Group__2__Impl_in_rule__ModelCorrespondence__Group__22236);
            rule__ModelCorrespondence__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__ModelCorrespondence__Group__3_in_rule__ModelCorrespondence__Group__22239);
            rule__ModelCorrespondence__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModelCorrespondence__Group__2"


    // $ANTLR start "rule__ModelCorrespondence__Group__2__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1073:1: rule__ModelCorrespondence__Group__2__Impl : ( '--' ) ;
    public final void rule__ModelCorrespondence__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1077:1: ( ( '--' ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1078:1: ( '--' )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1078:1: ( '--' )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1079:1: '--'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModelCorrespondenceAccess().getHyphenMinusHyphenMinusKeyword_2()); 
            }
            match(input,12,FollowSets000.FOLLOW_12_in_rule__ModelCorrespondence__Group__2__Impl2267); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getModelCorrespondenceAccess().getHyphenMinusHyphenMinusKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModelCorrespondence__Group__2__Impl"


    // $ANTLR start "rule__ModelCorrespondence__Group__3"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1092:1: rule__ModelCorrespondence__Group__3 : rule__ModelCorrespondence__Group__3__Impl rule__ModelCorrespondence__Group__4 ;
    public final void rule__ModelCorrespondence__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1096:1: ( rule__ModelCorrespondence__Group__3__Impl rule__ModelCorrespondence__Group__4 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1097:2: rule__ModelCorrespondence__Group__3__Impl rule__ModelCorrespondence__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__ModelCorrespondence__Group__3__Impl_in_rule__ModelCorrespondence__Group__32298);
            rule__ModelCorrespondence__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__ModelCorrespondence__Group__4_in_rule__ModelCorrespondence__Group__32301);
            rule__ModelCorrespondence__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModelCorrespondence__Group__3"


    // $ANTLR start "rule__ModelCorrespondence__Group__3__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1104:1: rule__ModelCorrespondence__Group__3__Impl : ( ( rule__ModelCorrespondence__RightAssignment_3 ) ) ;
    public final void rule__ModelCorrespondence__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1108:1: ( ( ( rule__ModelCorrespondence__RightAssignment_3 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1109:1: ( ( rule__ModelCorrespondence__RightAssignment_3 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1109:1: ( ( rule__ModelCorrespondence__RightAssignment_3 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1110:1: ( rule__ModelCorrespondence__RightAssignment_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModelCorrespondenceAccess().getRightAssignment_3()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1111:1: ( rule__ModelCorrespondence__RightAssignment_3 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1111:2: rule__ModelCorrespondence__RightAssignment_3
            {
            pushFollow(FollowSets000.FOLLOW_rule__ModelCorrespondence__RightAssignment_3_in_rule__ModelCorrespondence__Group__3__Impl2328);
            rule__ModelCorrespondence__RightAssignment_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getModelCorrespondenceAccess().getRightAssignment_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModelCorrespondence__Group__3__Impl"


    // $ANTLR start "rule__ModelCorrespondence__Group__4"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1121:1: rule__ModelCorrespondence__Group__4 : rule__ModelCorrespondence__Group__4__Impl rule__ModelCorrespondence__Group__5 ;
    public final void rule__ModelCorrespondence__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1125:1: ( rule__ModelCorrespondence__Group__4__Impl rule__ModelCorrespondence__Group__5 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1126:2: rule__ModelCorrespondence__Group__4__Impl rule__ModelCorrespondence__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_rule__ModelCorrespondence__Group__4__Impl_in_rule__ModelCorrespondence__Group__42358);
            rule__ModelCorrespondence__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__ModelCorrespondence__Group__5_in_rule__ModelCorrespondence__Group__42361);
            rule__ModelCorrespondence__Group__5();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModelCorrespondence__Group__4"


    // $ANTLR start "rule__ModelCorrespondence__Group__4__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1133:1: rule__ModelCorrespondence__Group__4__Impl : ( '{' ) ;
    public final void rule__ModelCorrespondence__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1137:1: ( ( '{' ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1138:1: ( '{' )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1138:1: ( '{' )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1139:1: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModelCorrespondenceAccess().getLeftCurlyBracketKeyword_4()); 
            }
            match(input,13,FollowSets000.FOLLOW_13_in_rule__ModelCorrespondence__Group__4__Impl2389); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getModelCorrespondenceAccess().getLeftCurlyBracketKeyword_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModelCorrespondence__Group__4__Impl"


    // $ANTLR start "rule__ModelCorrespondence__Group__5"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1152:1: rule__ModelCorrespondence__Group__5 : rule__ModelCorrespondence__Group__5__Impl rule__ModelCorrespondence__Group__6 ;
    public final void rule__ModelCorrespondence__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1156:1: ( rule__ModelCorrespondence__Group__5__Impl rule__ModelCorrespondence__Group__6 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1157:2: rule__ModelCorrespondence__Group__5__Impl rule__ModelCorrespondence__Group__6
            {
            pushFollow(FollowSets000.FOLLOW_rule__ModelCorrespondence__Group__5__Impl_in_rule__ModelCorrespondence__Group__52420);
            rule__ModelCorrespondence__Group__5__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__ModelCorrespondence__Group__6_in_rule__ModelCorrespondence__Group__52423);
            rule__ModelCorrespondence__Group__6();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModelCorrespondence__Group__5"


    // $ANTLR start "rule__ModelCorrespondence__Group__5__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1164:1: rule__ModelCorrespondence__Group__5__Impl : ( ( rule__ModelCorrespondence__ChildrenAssignment_5 )* ) ;
    public final void rule__ModelCorrespondence__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1168:1: ( ( ( rule__ModelCorrespondence__ChildrenAssignment_5 )* ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1169:1: ( ( rule__ModelCorrespondence__ChildrenAssignment_5 )* )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1169:1: ( ( rule__ModelCorrespondence__ChildrenAssignment_5 )* )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1170:1: ( rule__ModelCorrespondence__ChildrenAssignment_5 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModelCorrespondenceAccess().getChildrenAssignment_5()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1171:1: ( rule__ModelCorrespondence__ChildrenAssignment_5 )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==RULE_ID||(LA9_0>=20 && LA9_0<=21)) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1171:2: rule__ModelCorrespondence__ChildrenAssignment_5
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__ModelCorrespondence__ChildrenAssignment_5_in_rule__ModelCorrespondence__Group__5__Impl2450);
            	    rule__ModelCorrespondence__ChildrenAssignment_5();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getModelCorrespondenceAccess().getChildrenAssignment_5()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModelCorrespondence__Group__5__Impl"


    // $ANTLR start "rule__ModelCorrespondence__Group__6"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1181:1: rule__ModelCorrespondence__Group__6 : rule__ModelCorrespondence__Group__6__Impl ;
    public final void rule__ModelCorrespondence__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1185:1: ( rule__ModelCorrespondence__Group__6__Impl )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1186:2: rule__ModelCorrespondence__Group__6__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__ModelCorrespondence__Group__6__Impl_in_rule__ModelCorrespondence__Group__62481);
            rule__ModelCorrespondence__Group__6__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModelCorrespondence__Group__6"


    // $ANTLR start "rule__ModelCorrespondence__Group__6__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1192:1: rule__ModelCorrespondence__Group__6__Impl : ( '}' ) ;
    public final void rule__ModelCorrespondence__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1196:1: ( ( '}' ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1197:1: ( '}' )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1197:1: ( '}' )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1198:1: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModelCorrespondenceAccess().getRightCurlyBracketKeyword_6()); 
            }
            match(input,14,FollowSets000.FOLLOW_14_in_rule__ModelCorrespondence__Group__6__Impl2509); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getModelCorrespondenceAccess().getRightCurlyBracketKeyword_6()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModelCorrespondence__Group__6__Impl"


    // $ANTLR start "rule__ClassCorrespondence__Group__0"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1225:1: rule__ClassCorrespondence__Group__0 : rule__ClassCorrespondence__Group__0__Impl rule__ClassCorrespondence__Group__1 ;
    public final void rule__ClassCorrespondence__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1229:1: ( rule__ClassCorrespondence__Group__0__Impl rule__ClassCorrespondence__Group__1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1230:2: rule__ClassCorrespondence__Group__0__Impl rule__ClassCorrespondence__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group__0__Impl_in_rule__ClassCorrespondence__Group__02554);
            rule__ClassCorrespondence__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group__1_in_rule__ClassCorrespondence__Group__02557);
            rule__ClassCorrespondence__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group__0"


    // $ANTLR start "rule__ClassCorrespondence__Group__0__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1237:1: rule__ClassCorrespondence__Group__0__Impl : ( ( rule__ClassCorrespondence__Alternatives_0 ) ) ;
    public final void rule__ClassCorrespondence__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1241:1: ( ( ( rule__ClassCorrespondence__Alternatives_0 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1242:1: ( ( rule__ClassCorrespondence__Alternatives_0 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1242:1: ( ( rule__ClassCorrespondence__Alternatives_0 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1243:1: ( rule__ClassCorrespondence__Alternatives_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassCorrespondenceAccess().getAlternatives_0()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1244:1: ( rule__ClassCorrespondence__Alternatives_0 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1244:2: rule__ClassCorrespondence__Alternatives_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Alternatives_0_in_rule__ClassCorrespondence__Group__0__Impl2584);
            rule__ClassCorrespondence__Alternatives_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassCorrespondenceAccess().getAlternatives_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group__0__Impl"


    // $ANTLR start "rule__ClassCorrespondence__Group__1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1254:1: rule__ClassCorrespondence__Group__1 : rule__ClassCorrespondence__Group__1__Impl rule__ClassCorrespondence__Group__2 ;
    public final void rule__ClassCorrespondence__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1258:1: ( rule__ClassCorrespondence__Group__1__Impl rule__ClassCorrespondence__Group__2 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1259:2: rule__ClassCorrespondence__Group__1__Impl rule__ClassCorrespondence__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group__1__Impl_in_rule__ClassCorrespondence__Group__12614);
            rule__ClassCorrespondence__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group__2_in_rule__ClassCorrespondence__Group__12617);
            rule__ClassCorrespondence__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group__1"


    // $ANTLR start "rule__ClassCorrespondence__Group__1__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1266:1: rule__ClassCorrespondence__Group__1__Impl : ( '{' ) ;
    public final void rule__ClassCorrespondence__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1270:1: ( ( '{' ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1271:1: ( '{' )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1271:1: ( '{' )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1272:1: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassCorrespondenceAccess().getLeftCurlyBracketKeyword_1()); 
            }
            match(input,13,FollowSets000.FOLLOW_13_in_rule__ClassCorrespondence__Group__1__Impl2645); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassCorrespondenceAccess().getLeftCurlyBracketKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group__1__Impl"


    // $ANTLR start "rule__ClassCorrespondence__Group__2"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1285:1: rule__ClassCorrespondence__Group__2 : rule__ClassCorrespondence__Group__2__Impl rule__ClassCorrespondence__Group__3 ;
    public final void rule__ClassCorrespondence__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1289:1: ( rule__ClassCorrespondence__Group__2__Impl rule__ClassCorrespondence__Group__3 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1290:2: rule__ClassCorrespondence__Group__2__Impl rule__ClassCorrespondence__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group__2__Impl_in_rule__ClassCorrespondence__Group__22676);
            rule__ClassCorrespondence__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group__3_in_rule__ClassCorrespondence__Group__22679);
            rule__ClassCorrespondence__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group__2"


    // $ANTLR start "rule__ClassCorrespondence__Group__2__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1297:1: rule__ClassCorrespondence__Group__2__Impl : ( ( rule__ClassCorrespondence__ChildrenAssignment_2 )* ) ;
    public final void rule__ClassCorrespondence__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1301:1: ( ( ( rule__ClassCorrespondence__ChildrenAssignment_2 )* ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1302:1: ( ( rule__ClassCorrespondence__ChildrenAssignment_2 )* )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1302:1: ( ( rule__ClassCorrespondence__ChildrenAssignment_2 )* )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1303:1: ( rule__ClassCorrespondence__ChildrenAssignment_2 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassCorrespondenceAccess().getChildrenAssignment_2()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1304:1: ( rule__ClassCorrespondence__ChildrenAssignment_2 )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( ((LA10_0>=RULE_ID && LA10_0<=RULE_INT)||(LA10_0>=20 && LA10_0<=21)||LA10_0==24) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1304:2: rule__ClassCorrespondence__ChildrenAssignment_2
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__ChildrenAssignment_2_in_rule__ClassCorrespondence__Group__2__Impl2706);
            	    rule__ClassCorrespondence__ChildrenAssignment_2();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassCorrespondenceAccess().getChildrenAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group__2__Impl"


    // $ANTLR start "rule__ClassCorrespondence__Group__3"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1314:1: rule__ClassCorrespondence__Group__3 : rule__ClassCorrespondence__Group__3__Impl ;
    public final void rule__ClassCorrespondence__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1318:1: ( rule__ClassCorrespondence__Group__3__Impl )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1319:2: rule__ClassCorrespondence__Group__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group__3__Impl_in_rule__ClassCorrespondence__Group__32737);
            rule__ClassCorrespondence__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group__3"


    // $ANTLR start "rule__ClassCorrespondence__Group__3__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1325:1: rule__ClassCorrespondence__Group__3__Impl : ( '}' ) ;
    public final void rule__ClassCorrespondence__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1329:1: ( ( '}' ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1330:1: ( '}' )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1330:1: ( '}' )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1331:1: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassCorrespondenceAccess().getRightCurlyBracketKeyword_3()); 
            }
            match(input,14,FollowSets000.FOLLOW_14_in_rule__ClassCorrespondence__Group__3__Impl2765); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassCorrespondenceAccess().getRightCurlyBracketKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group__3__Impl"


    // $ANTLR start "rule__ClassCorrespondence__Group_0_0__0"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1352:1: rule__ClassCorrespondence__Group_0_0__0 : rule__ClassCorrespondence__Group_0_0__0__Impl rule__ClassCorrespondence__Group_0_0__1 ;
    public final void rule__ClassCorrespondence__Group_0_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1356:1: ( rule__ClassCorrespondence__Group_0_0__0__Impl rule__ClassCorrespondence__Group_0_0__1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1357:2: rule__ClassCorrespondence__Group_0_0__0__Impl rule__ClassCorrespondence__Group_0_0__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_0__0__Impl_in_rule__ClassCorrespondence__Group_0_0__02804);
            rule__ClassCorrespondence__Group_0_0__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_0__1_in_rule__ClassCorrespondence__Group_0_0__02807);
            rule__ClassCorrespondence__Group_0_0__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group_0_0__0"


    // $ANTLR start "rule__ClassCorrespondence__Group_0_0__0__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1364:1: rule__ClassCorrespondence__Group_0_0__0__Impl : ( () ) ;
    public final void rule__ClassCorrespondence__Group_0_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1368:1: ( ( () ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1369:1: ( () )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1369:1: ( () )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1370:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassCorrespondenceAccess().getBidirectionalCorrespondenceAction_0_0_0()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1371:1: ()
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1373:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassCorrespondenceAccess().getBidirectionalCorrespondenceAction_0_0_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group_0_0__0__Impl"


    // $ANTLR start "rule__ClassCorrespondence__Group_0_0__1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1383:1: rule__ClassCorrespondence__Group_0_0__1 : rule__ClassCorrespondence__Group_0_0__1__Impl rule__ClassCorrespondence__Group_0_0__2 ;
    public final void rule__ClassCorrespondence__Group_0_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1387:1: ( rule__ClassCorrespondence__Group_0_0__1__Impl rule__ClassCorrespondence__Group_0_0__2 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1388:2: rule__ClassCorrespondence__Group_0_0__1__Impl rule__ClassCorrespondence__Group_0_0__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_0__1__Impl_in_rule__ClassCorrespondence__Group_0_0__12865);
            rule__ClassCorrespondence__Group_0_0__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_0__2_in_rule__ClassCorrespondence__Group_0_0__12868);
            rule__ClassCorrespondence__Group_0_0__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group_0_0__1"


    // $ANTLR start "rule__ClassCorrespondence__Group_0_0__1__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1395:1: rule__ClassCorrespondence__Group_0_0__1__Impl : ( ( rule__ClassCorrespondence__LeftAssignment_0_0_1 ) ) ;
    public final void rule__ClassCorrespondence__Group_0_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1399:1: ( ( ( rule__ClassCorrespondence__LeftAssignment_0_0_1 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1400:1: ( ( rule__ClassCorrespondence__LeftAssignment_0_0_1 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1400:1: ( ( rule__ClassCorrespondence__LeftAssignment_0_0_1 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1401:1: ( rule__ClassCorrespondence__LeftAssignment_0_0_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassCorrespondenceAccess().getLeftAssignment_0_0_1()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1402:1: ( rule__ClassCorrespondence__LeftAssignment_0_0_1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1402:2: rule__ClassCorrespondence__LeftAssignment_0_0_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__LeftAssignment_0_0_1_in_rule__ClassCorrespondence__Group_0_0__1__Impl2895);
            rule__ClassCorrespondence__LeftAssignment_0_0_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassCorrespondenceAccess().getLeftAssignment_0_0_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group_0_0__1__Impl"


    // $ANTLR start "rule__ClassCorrespondence__Group_0_0__2"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1412:1: rule__ClassCorrespondence__Group_0_0__2 : rule__ClassCorrespondence__Group_0_0__2__Impl rule__ClassCorrespondence__Group_0_0__3 ;
    public final void rule__ClassCorrespondence__Group_0_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1416:1: ( rule__ClassCorrespondence__Group_0_0__2__Impl rule__ClassCorrespondence__Group_0_0__3 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1417:2: rule__ClassCorrespondence__Group_0_0__2__Impl rule__ClassCorrespondence__Group_0_0__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_0__2__Impl_in_rule__ClassCorrespondence__Group_0_0__22925);
            rule__ClassCorrespondence__Group_0_0__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_0__3_in_rule__ClassCorrespondence__Group_0_0__22928);
            rule__ClassCorrespondence__Group_0_0__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group_0_0__2"


    // $ANTLR start "rule__ClassCorrespondence__Group_0_0__2__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1424:1: rule__ClassCorrespondence__Group_0_0__2__Impl : ( '--' ) ;
    public final void rule__ClassCorrespondence__Group_0_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1428:1: ( ( '--' ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1429:1: ( '--' )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1429:1: ( '--' )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1430:1: '--'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassCorrespondenceAccess().getHyphenMinusHyphenMinusKeyword_0_0_2()); 
            }
            match(input,12,FollowSets000.FOLLOW_12_in_rule__ClassCorrespondence__Group_0_0__2__Impl2956); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassCorrespondenceAccess().getHyphenMinusHyphenMinusKeyword_0_0_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group_0_0__2__Impl"


    // $ANTLR start "rule__ClassCorrespondence__Group_0_0__3"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1443:1: rule__ClassCorrespondence__Group_0_0__3 : rule__ClassCorrespondence__Group_0_0__3__Impl ;
    public final void rule__ClassCorrespondence__Group_0_0__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1447:1: ( rule__ClassCorrespondence__Group_0_0__3__Impl )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1448:2: rule__ClassCorrespondence__Group_0_0__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_0__3__Impl_in_rule__ClassCorrespondence__Group_0_0__32987);
            rule__ClassCorrespondence__Group_0_0__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group_0_0__3"


    // $ANTLR start "rule__ClassCorrespondence__Group_0_0__3__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1454:1: rule__ClassCorrespondence__Group_0_0__3__Impl : ( ( rule__ClassCorrespondence__RightAssignment_0_0_3 ) ) ;
    public final void rule__ClassCorrespondence__Group_0_0__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1458:1: ( ( ( rule__ClassCorrespondence__RightAssignment_0_0_3 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1459:1: ( ( rule__ClassCorrespondence__RightAssignment_0_0_3 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1459:1: ( ( rule__ClassCorrespondence__RightAssignment_0_0_3 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1460:1: ( rule__ClassCorrespondence__RightAssignment_0_0_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassCorrespondenceAccess().getRightAssignment_0_0_3()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1461:1: ( rule__ClassCorrespondence__RightAssignment_0_0_3 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1461:2: rule__ClassCorrespondence__RightAssignment_0_0_3
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__RightAssignment_0_0_3_in_rule__ClassCorrespondence__Group_0_0__3__Impl3014);
            rule__ClassCorrespondence__RightAssignment_0_0_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassCorrespondenceAccess().getRightAssignment_0_0_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group_0_0__3__Impl"


    // $ANTLR start "rule__ClassCorrespondence__Group_0_1__0"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1479:1: rule__ClassCorrespondence__Group_0_1__0 : rule__ClassCorrespondence__Group_0_1__0__Impl rule__ClassCorrespondence__Group_0_1__1 ;
    public final void rule__ClassCorrespondence__Group_0_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1483:1: ( rule__ClassCorrespondence__Group_0_1__0__Impl rule__ClassCorrespondence__Group_0_1__1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1484:2: rule__ClassCorrespondence__Group_0_1__0__Impl rule__ClassCorrespondence__Group_0_1__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_1__0__Impl_in_rule__ClassCorrespondence__Group_0_1__03052);
            rule__ClassCorrespondence__Group_0_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_1__1_in_rule__ClassCorrespondence__Group_0_1__03055);
            rule__ClassCorrespondence__Group_0_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group_0_1__0"


    // $ANTLR start "rule__ClassCorrespondence__Group_0_1__0__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1491:1: rule__ClassCorrespondence__Group_0_1__0__Impl : ( () ) ;
    public final void rule__ClassCorrespondence__Group_0_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1495:1: ( ( () ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1496:1: ( () )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1496:1: ( () )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1497:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassCorrespondenceAccess().getOneToOneCorrespondenceAction_0_1_0()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1498:1: ()
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1500:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassCorrespondenceAccess().getOneToOneCorrespondenceAction_0_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group_0_1__0__Impl"


    // $ANTLR start "rule__ClassCorrespondence__Group_0_1__1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1510:1: rule__ClassCorrespondence__Group_0_1__1 : rule__ClassCorrespondence__Group_0_1__1__Impl rule__ClassCorrespondence__Group_0_1__2 ;
    public final void rule__ClassCorrespondence__Group_0_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1514:1: ( rule__ClassCorrespondence__Group_0_1__1__Impl rule__ClassCorrespondence__Group_0_1__2 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1515:2: rule__ClassCorrespondence__Group_0_1__1__Impl rule__ClassCorrespondence__Group_0_1__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_1__1__Impl_in_rule__ClassCorrespondence__Group_0_1__13113);
            rule__ClassCorrespondence__Group_0_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_1__2_in_rule__ClassCorrespondence__Group_0_1__13116);
            rule__ClassCorrespondence__Group_0_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group_0_1__1"


    // $ANTLR start "rule__ClassCorrespondence__Group_0_1__1__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1522:1: rule__ClassCorrespondence__Group_0_1__1__Impl : ( ( rule__ClassCorrespondence__LeftAssignment_0_1_1 ) ) ;
    public final void rule__ClassCorrespondence__Group_0_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1526:1: ( ( ( rule__ClassCorrespondence__LeftAssignment_0_1_1 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1527:1: ( ( rule__ClassCorrespondence__LeftAssignment_0_1_1 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1527:1: ( ( rule__ClassCorrespondence__LeftAssignment_0_1_1 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1528:1: ( rule__ClassCorrespondence__LeftAssignment_0_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassCorrespondenceAccess().getLeftAssignment_0_1_1()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1529:1: ( rule__ClassCorrespondence__LeftAssignment_0_1_1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1529:2: rule__ClassCorrespondence__LeftAssignment_0_1_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__LeftAssignment_0_1_1_in_rule__ClassCorrespondence__Group_0_1__1__Impl3143);
            rule__ClassCorrespondence__LeftAssignment_0_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassCorrespondenceAccess().getLeftAssignment_0_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group_0_1__1__Impl"


    // $ANTLR start "rule__ClassCorrespondence__Group_0_1__2"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1539:1: rule__ClassCorrespondence__Group_0_1__2 : rule__ClassCorrespondence__Group_0_1__2__Impl rule__ClassCorrespondence__Group_0_1__3 ;
    public final void rule__ClassCorrespondence__Group_0_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1543:1: ( rule__ClassCorrespondence__Group_0_1__2__Impl rule__ClassCorrespondence__Group_0_1__3 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1544:2: rule__ClassCorrespondence__Group_0_1__2__Impl rule__ClassCorrespondence__Group_0_1__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_1__2__Impl_in_rule__ClassCorrespondence__Group_0_1__23173);
            rule__ClassCorrespondence__Group_0_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_1__3_in_rule__ClassCorrespondence__Group_0_1__23176);
            rule__ClassCorrespondence__Group_0_1__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group_0_1__2"


    // $ANTLR start "rule__ClassCorrespondence__Group_0_1__2__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1551:1: rule__ClassCorrespondence__Group_0_1__2__Impl : ( '==' ) ;
    public final void rule__ClassCorrespondence__Group_0_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1555:1: ( ( '==' ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1556:1: ( '==' )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1556:1: ( '==' )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1557:1: '=='
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassCorrespondenceAccess().getEqualsSignEqualsSignKeyword_0_1_2()); 
            }
            match(input,15,FollowSets000.FOLLOW_15_in_rule__ClassCorrespondence__Group_0_1__2__Impl3204); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassCorrespondenceAccess().getEqualsSignEqualsSignKeyword_0_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group_0_1__2__Impl"


    // $ANTLR start "rule__ClassCorrespondence__Group_0_1__3"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1570:1: rule__ClassCorrespondence__Group_0_1__3 : rule__ClassCorrespondence__Group_0_1__3__Impl ;
    public final void rule__ClassCorrespondence__Group_0_1__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1574:1: ( rule__ClassCorrespondence__Group_0_1__3__Impl )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1575:2: rule__ClassCorrespondence__Group_0_1__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_1__3__Impl_in_rule__ClassCorrespondence__Group_0_1__33235);
            rule__ClassCorrespondence__Group_0_1__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group_0_1__3"


    // $ANTLR start "rule__ClassCorrespondence__Group_0_1__3__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1581:1: rule__ClassCorrespondence__Group_0_1__3__Impl : ( ( rule__ClassCorrespondence__RightAssignment_0_1_3 ) ) ;
    public final void rule__ClassCorrespondence__Group_0_1__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1585:1: ( ( ( rule__ClassCorrespondence__RightAssignment_0_1_3 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1586:1: ( ( rule__ClassCorrespondence__RightAssignment_0_1_3 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1586:1: ( ( rule__ClassCorrespondence__RightAssignment_0_1_3 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1587:1: ( rule__ClassCorrespondence__RightAssignment_0_1_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassCorrespondenceAccess().getRightAssignment_0_1_3()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1588:1: ( rule__ClassCorrespondence__RightAssignment_0_1_3 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1588:2: rule__ClassCorrespondence__RightAssignment_0_1_3
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__RightAssignment_0_1_3_in_rule__ClassCorrespondence__Group_0_1__3__Impl3262);
            rule__ClassCorrespondence__RightAssignment_0_1_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassCorrespondenceAccess().getRightAssignment_0_1_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group_0_1__3__Impl"


    // $ANTLR start "rule__ClassCorrespondence__Group_0_2__0"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1606:1: rule__ClassCorrespondence__Group_0_2__0 : rule__ClassCorrespondence__Group_0_2__0__Impl rule__ClassCorrespondence__Group_0_2__1 ;
    public final void rule__ClassCorrespondence__Group_0_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1610:1: ( rule__ClassCorrespondence__Group_0_2__0__Impl rule__ClassCorrespondence__Group_0_2__1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1611:2: rule__ClassCorrespondence__Group_0_2__0__Impl rule__ClassCorrespondence__Group_0_2__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_2__0__Impl_in_rule__ClassCorrespondence__Group_0_2__03300);
            rule__ClassCorrespondence__Group_0_2__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_2__1_in_rule__ClassCorrespondence__Group_0_2__03303);
            rule__ClassCorrespondence__Group_0_2__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group_0_2__0"


    // $ANTLR start "rule__ClassCorrespondence__Group_0_2__0__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1618:1: rule__ClassCorrespondence__Group_0_2__0__Impl : ( () ) ;
    public final void rule__ClassCorrespondence__Group_0_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1622:1: ( ( () ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1623:1: ( () )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1623:1: ( () )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1624:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassCorrespondenceAccess().getLeftToRightCorrespondenceAction_0_2_0()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1625:1: ()
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1627:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassCorrespondenceAccess().getLeftToRightCorrespondenceAction_0_2_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group_0_2__0__Impl"


    // $ANTLR start "rule__ClassCorrespondence__Group_0_2__1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1637:1: rule__ClassCorrespondence__Group_0_2__1 : rule__ClassCorrespondence__Group_0_2__1__Impl rule__ClassCorrespondence__Group_0_2__2 ;
    public final void rule__ClassCorrespondence__Group_0_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1641:1: ( rule__ClassCorrespondence__Group_0_2__1__Impl rule__ClassCorrespondence__Group_0_2__2 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1642:2: rule__ClassCorrespondence__Group_0_2__1__Impl rule__ClassCorrespondence__Group_0_2__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_2__1__Impl_in_rule__ClassCorrespondence__Group_0_2__13361);
            rule__ClassCorrespondence__Group_0_2__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_2__2_in_rule__ClassCorrespondence__Group_0_2__13364);
            rule__ClassCorrespondence__Group_0_2__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group_0_2__1"


    // $ANTLR start "rule__ClassCorrespondence__Group_0_2__1__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1649:1: rule__ClassCorrespondence__Group_0_2__1__Impl : ( ( rule__ClassCorrespondence__LeftAssignment_0_2_1 ) ) ;
    public final void rule__ClassCorrespondence__Group_0_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1653:1: ( ( ( rule__ClassCorrespondence__LeftAssignment_0_2_1 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1654:1: ( ( rule__ClassCorrespondence__LeftAssignment_0_2_1 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1654:1: ( ( rule__ClassCorrespondence__LeftAssignment_0_2_1 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1655:1: ( rule__ClassCorrespondence__LeftAssignment_0_2_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassCorrespondenceAccess().getLeftAssignment_0_2_1()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1656:1: ( rule__ClassCorrespondence__LeftAssignment_0_2_1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1656:2: rule__ClassCorrespondence__LeftAssignment_0_2_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__LeftAssignment_0_2_1_in_rule__ClassCorrespondence__Group_0_2__1__Impl3391);
            rule__ClassCorrespondence__LeftAssignment_0_2_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassCorrespondenceAccess().getLeftAssignment_0_2_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group_0_2__1__Impl"


    // $ANTLR start "rule__ClassCorrespondence__Group_0_2__2"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1666:1: rule__ClassCorrespondence__Group_0_2__2 : rule__ClassCorrespondence__Group_0_2__2__Impl rule__ClassCorrespondence__Group_0_2__3 ;
    public final void rule__ClassCorrespondence__Group_0_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1670:1: ( rule__ClassCorrespondence__Group_0_2__2__Impl rule__ClassCorrespondence__Group_0_2__3 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1671:2: rule__ClassCorrespondence__Group_0_2__2__Impl rule__ClassCorrespondence__Group_0_2__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_2__2__Impl_in_rule__ClassCorrespondence__Group_0_2__23421);
            rule__ClassCorrespondence__Group_0_2__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_2__3_in_rule__ClassCorrespondence__Group_0_2__23424);
            rule__ClassCorrespondence__Group_0_2__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group_0_2__2"


    // $ANTLR start "rule__ClassCorrespondence__Group_0_2__2__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1678:1: rule__ClassCorrespondence__Group_0_2__2__Impl : ( '->' ) ;
    public final void rule__ClassCorrespondence__Group_0_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1682:1: ( ( '->' ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1683:1: ( '->' )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1683:1: ( '->' )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1684:1: '->'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassCorrespondenceAccess().getHyphenMinusGreaterThanSignKeyword_0_2_2()); 
            }
            match(input,16,FollowSets000.FOLLOW_16_in_rule__ClassCorrespondence__Group_0_2__2__Impl3452); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassCorrespondenceAccess().getHyphenMinusGreaterThanSignKeyword_0_2_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group_0_2__2__Impl"


    // $ANTLR start "rule__ClassCorrespondence__Group_0_2__3"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1697:1: rule__ClassCorrespondence__Group_0_2__3 : rule__ClassCorrespondence__Group_0_2__3__Impl ;
    public final void rule__ClassCorrespondence__Group_0_2__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1701:1: ( rule__ClassCorrespondence__Group_0_2__3__Impl )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1702:2: rule__ClassCorrespondence__Group_0_2__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_2__3__Impl_in_rule__ClassCorrespondence__Group_0_2__33483);
            rule__ClassCorrespondence__Group_0_2__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group_0_2__3"


    // $ANTLR start "rule__ClassCorrespondence__Group_0_2__3__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1708:1: rule__ClassCorrespondence__Group_0_2__3__Impl : ( ( rule__ClassCorrespondence__RightAssignment_0_2_3 ) ) ;
    public final void rule__ClassCorrespondence__Group_0_2__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1712:1: ( ( ( rule__ClassCorrespondence__RightAssignment_0_2_3 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1713:1: ( ( rule__ClassCorrespondence__RightAssignment_0_2_3 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1713:1: ( ( rule__ClassCorrespondence__RightAssignment_0_2_3 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1714:1: ( rule__ClassCorrespondence__RightAssignment_0_2_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassCorrespondenceAccess().getRightAssignment_0_2_3()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1715:1: ( rule__ClassCorrespondence__RightAssignment_0_2_3 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1715:2: rule__ClassCorrespondence__RightAssignment_0_2_3
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__RightAssignment_0_2_3_in_rule__ClassCorrespondence__Group_0_2__3__Impl3510);
            rule__ClassCorrespondence__RightAssignment_0_2_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassCorrespondenceAccess().getRightAssignment_0_2_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group_0_2__3__Impl"


    // $ANTLR start "rule__ClassCorrespondence__Group_0_3__0"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1733:1: rule__ClassCorrespondence__Group_0_3__0 : rule__ClassCorrespondence__Group_0_3__0__Impl rule__ClassCorrespondence__Group_0_3__1 ;
    public final void rule__ClassCorrespondence__Group_0_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1737:1: ( rule__ClassCorrespondence__Group_0_3__0__Impl rule__ClassCorrespondence__Group_0_3__1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1738:2: rule__ClassCorrespondence__Group_0_3__0__Impl rule__ClassCorrespondence__Group_0_3__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_3__0__Impl_in_rule__ClassCorrespondence__Group_0_3__03548);
            rule__ClassCorrespondence__Group_0_3__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_3__1_in_rule__ClassCorrespondence__Group_0_3__03551);
            rule__ClassCorrespondence__Group_0_3__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group_0_3__0"


    // $ANTLR start "rule__ClassCorrespondence__Group_0_3__0__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1745:1: rule__ClassCorrespondence__Group_0_3__0__Impl : ( () ) ;
    public final void rule__ClassCorrespondence__Group_0_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1749:1: ( ( () ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1750:1: ( () )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1750:1: ( () )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1751:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassCorrespondenceAccess().getRightToLeftCorrespondenceAction_0_3_0()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1752:1: ()
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1754:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassCorrespondenceAccess().getRightToLeftCorrespondenceAction_0_3_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group_0_3__0__Impl"


    // $ANTLR start "rule__ClassCorrespondence__Group_0_3__1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1764:1: rule__ClassCorrespondence__Group_0_3__1 : rule__ClassCorrespondence__Group_0_3__1__Impl rule__ClassCorrespondence__Group_0_3__2 ;
    public final void rule__ClassCorrespondence__Group_0_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1768:1: ( rule__ClassCorrespondence__Group_0_3__1__Impl rule__ClassCorrespondence__Group_0_3__2 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1769:2: rule__ClassCorrespondence__Group_0_3__1__Impl rule__ClassCorrespondence__Group_0_3__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_3__1__Impl_in_rule__ClassCorrespondence__Group_0_3__13609);
            rule__ClassCorrespondence__Group_0_3__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_3__2_in_rule__ClassCorrespondence__Group_0_3__13612);
            rule__ClassCorrespondence__Group_0_3__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group_0_3__1"


    // $ANTLR start "rule__ClassCorrespondence__Group_0_3__1__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1776:1: rule__ClassCorrespondence__Group_0_3__1__Impl : ( ( rule__ClassCorrespondence__LeftAssignment_0_3_1 ) ) ;
    public final void rule__ClassCorrespondence__Group_0_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1780:1: ( ( ( rule__ClassCorrespondence__LeftAssignment_0_3_1 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1781:1: ( ( rule__ClassCorrespondence__LeftAssignment_0_3_1 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1781:1: ( ( rule__ClassCorrespondence__LeftAssignment_0_3_1 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1782:1: ( rule__ClassCorrespondence__LeftAssignment_0_3_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassCorrespondenceAccess().getLeftAssignment_0_3_1()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1783:1: ( rule__ClassCorrespondence__LeftAssignment_0_3_1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1783:2: rule__ClassCorrespondence__LeftAssignment_0_3_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__LeftAssignment_0_3_1_in_rule__ClassCorrespondence__Group_0_3__1__Impl3639);
            rule__ClassCorrespondence__LeftAssignment_0_3_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassCorrespondenceAccess().getLeftAssignment_0_3_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group_0_3__1__Impl"


    // $ANTLR start "rule__ClassCorrespondence__Group_0_3__2"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1793:1: rule__ClassCorrespondence__Group_0_3__2 : rule__ClassCorrespondence__Group_0_3__2__Impl rule__ClassCorrespondence__Group_0_3__3 ;
    public final void rule__ClassCorrespondence__Group_0_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1797:1: ( rule__ClassCorrespondence__Group_0_3__2__Impl rule__ClassCorrespondence__Group_0_3__3 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1798:2: rule__ClassCorrespondence__Group_0_3__2__Impl rule__ClassCorrespondence__Group_0_3__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_3__2__Impl_in_rule__ClassCorrespondence__Group_0_3__23669);
            rule__ClassCorrespondence__Group_0_3__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_3__3_in_rule__ClassCorrespondence__Group_0_3__23672);
            rule__ClassCorrespondence__Group_0_3__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group_0_3__2"


    // $ANTLR start "rule__ClassCorrespondence__Group_0_3__2__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1805:1: rule__ClassCorrespondence__Group_0_3__2__Impl : ( '<-' ) ;
    public final void rule__ClassCorrespondence__Group_0_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1809:1: ( ( '<-' ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1810:1: ( '<-' )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1810:1: ( '<-' )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1811:1: '<-'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassCorrespondenceAccess().getLessThanSignHyphenMinusKeyword_0_3_2()); 
            }
            match(input,17,FollowSets000.FOLLOW_17_in_rule__ClassCorrespondence__Group_0_3__2__Impl3700); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassCorrespondenceAccess().getLessThanSignHyphenMinusKeyword_0_3_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group_0_3__2__Impl"


    // $ANTLR start "rule__ClassCorrespondence__Group_0_3__3"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1824:1: rule__ClassCorrespondence__Group_0_3__3 : rule__ClassCorrespondence__Group_0_3__3__Impl ;
    public final void rule__ClassCorrespondence__Group_0_3__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1828:1: ( rule__ClassCorrespondence__Group_0_3__3__Impl )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1829:2: rule__ClassCorrespondence__Group_0_3__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_3__3__Impl_in_rule__ClassCorrespondence__Group_0_3__33731);
            rule__ClassCorrespondence__Group_0_3__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group_0_3__3"


    // $ANTLR start "rule__ClassCorrespondence__Group_0_3__3__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1835:1: rule__ClassCorrespondence__Group_0_3__3__Impl : ( ( rule__ClassCorrespondence__RightAssignment_0_3_3 ) ) ;
    public final void rule__ClassCorrespondence__Group_0_3__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1839:1: ( ( ( rule__ClassCorrespondence__RightAssignment_0_3_3 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1840:1: ( ( rule__ClassCorrespondence__RightAssignment_0_3_3 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1840:1: ( ( rule__ClassCorrespondence__RightAssignment_0_3_3 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1841:1: ( rule__ClassCorrespondence__RightAssignment_0_3_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassCorrespondenceAccess().getRightAssignment_0_3_3()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1842:1: ( rule__ClassCorrespondence__RightAssignment_0_3_3 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1842:2: rule__ClassCorrespondence__RightAssignment_0_3_3
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__RightAssignment_0_3_3_in_rule__ClassCorrespondence__Group_0_3__3__Impl3758);
            rule__ClassCorrespondence__RightAssignment_0_3_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassCorrespondenceAccess().getRightAssignment_0_3_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__Group_0_3__3__Impl"


    // $ANTLR start "rule__FeatureCorrespondence__Group_0__0"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1860:1: rule__FeatureCorrespondence__Group_0__0 : rule__FeatureCorrespondence__Group_0__0__Impl rule__FeatureCorrespondence__Group_0__1 ;
    public final void rule__FeatureCorrespondence__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1864:1: ( rule__FeatureCorrespondence__Group_0__0__Impl rule__FeatureCorrespondence__Group_0__1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1865:2: rule__FeatureCorrespondence__Group_0__0__Impl rule__FeatureCorrespondence__Group_0__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_0__0__Impl_in_rule__FeatureCorrespondence__Group_0__03796);
            rule__FeatureCorrespondence__Group_0__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_0__1_in_rule__FeatureCorrespondence__Group_0__03799);
            rule__FeatureCorrespondence__Group_0__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__Group_0__0"


    // $ANTLR start "rule__FeatureCorrespondence__Group_0__0__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1872:1: rule__FeatureCorrespondence__Group_0__0__Impl : ( () ) ;
    public final void rule__FeatureCorrespondence__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1876:1: ( ( () ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1877:1: ( () )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1877:1: ( () )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1878:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureCorrespondenceAccess().getBidirectionalCorrespondenceAction_0_0()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1879:1: ()
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1881:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureCorrespondenceAccess().getBidirectionalCorrespondenceAction_0_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__Group_0__0__Impl"


    // $ANTLR start "rule__FeatureCorrespondence__Group_0__1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1891:1: rule__FeatureCorrespondence__Group_0__1 : rule__FeatureCorrespondence__Group_0__1__Impl rule__FeatureCorrespondence__Group_0__2 ;
    public final void rule__FeatureCorrespondence__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1895:1: ( rule__FeatureCorrespondence__Group_0__1__Impl rule__FeatureCorrespondence__Group_0__2 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1896:2: rule__FeatureCorrespondence__Group_0__1__Impl rule__FeatureCorrespondence__Group_0__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_0__1__Impl_in_rule__FeatureCorrespondence__Group_0__13857);
            rule__FeatureCorrespondence__Group_0__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_0__2_in_rule__FeatureCorrespondence__Group_0__13860);
            rule__FeatureCorrespondence__Group_0__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__Group_0__1"


    // $ANTLR start "rule__FeatureCorrespondence__Group_0__1__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1903:1: rule__FeatureCorrespondence__Group_0__1__Impl : ( ( rule__FeatureCorrespondence__LeftAssignment_0_1 ) ) ;
    public final void rule__FeatureCorrespondence__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1907:1: ( ( ( rule__FeatureCorrespondence__LeftAssignment_0_1 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1908:1: ( ( rule__FeatureCorrespondence__LeftAssignment_0_1 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1908:1: ( ( rule__FeatureCorrespondence__LeftAssignment_0_1 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1909:1: ( rule__FeatureCorrespondence__LeftAssignment_0_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureCorrespondenceAccess().getLeftAssignment_0_1()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1910:1: ( rule__FeatureCorrespondence__LeftAssignment_0_1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1910:2: rule__FeatureCorrespondence__LeftAssignment_0_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__LeftAssignment_0_1_in_rule__FeatureCorrespondence__Group_0__1__Impl3887);
            rule__FeatureCorrespondence__LeftAssignment_0_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureCorrespondenceAccess().getLeftAssignment_0_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__Group_0__1__Impl"


    // $ANTLR start "rule__FeatureCorrespondence__Group_0__2"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1920:1: rule__FeatureCorrespondence__Group_0__2 : rule__FeatureCorrespondence__Group_0__2__Impl rule__FeatureCorrespondence__Group_0__3 ;
    public final void rule__FeatureCorrespondence__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1924:1: ( rule__FeatureCorrespondence__Group_0__2__Impl rule__FeatureCorrespondence__Group_0__3 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1925:2: rule__FeatureCorrespondence__Group_0__2__Impl rule__FeatureCorrespondence__Group_0__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_0__2__Impl_in_rule__FeatureCorrespondence__Group_0__23917);
            rule__FeatureCorrespondence__Group_0__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_0__3_in_rule__FeatureCorrespondence__Group_0__23920);
            rule__FeatureCorrespondence__Group_0__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__Group_0__2"


    // $ANTLR start "rule__FeatureCorrespondence__Group_0__2__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1932:1: rule__FeatureCorrespondence__Group_0__2__Impl : ( '--' ) ;
    public final void rule__FeatureCorrespondence__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1936:1: ( ( '--' ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1937:1: ( '--' )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1937:1: ( '--' )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1938:1: '--'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureCorrespondenceAccess().getHyphenMinusHyphenMinusKeyword_0_2()); 
            }
            match(input,12,FollowSets000.FOLLOW_12_in_rule__FeatureCorrespondence__Group_0__2__Impl3948); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureCorrespondenceAccess().getHyphenMinusHyphenMinusKeyword_0_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__Group_0__2__Impl"


    // $ANTLR start "rule__FeatureCorrespondence__Group_0__3"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1951:1: rule__FeatureCorrespondence__Group_0__3 : rule__FeatureCorrespondence__Group_0__3__Impl ;
    public final void rule__FeatureCorrespondence__Group_0__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1955:1: ( rule__FeatureCorrespondence__Group_0__3__Impl )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1956:2: rule__FeatureCorrespondence__Group_0__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_0__3__Impl_in_rule__FeatureCorrespondence__Group_0__33979);
            rule__FeatureCorrespondence__Group_0__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__Group_0__3"


    // $ANTLR start "rule__FeatureCorrespondence__Group_0__3__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1962:1: rule__FeatureCorrespondence__Group_0__3__Impl : ( ( rule__FeatureCorrespondence__RightAssignment_0_3 ) ) ;
    public final void rule__FeatureCorrespondence__Group_0__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1966:1: ( ( ( rule__FeatureCorrespondence__RightAssignment_0_3 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1967:1: ( ( rule__FeatureCorrespondence__RightAssignment_0_3 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1967:1: ( ( rule__FeatureCorrespondence__RightAssignment_0_3 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1968:1: ( rule__FeatureCorrespondence__RightAssignment_0_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureCorrespondenceAccess().getRightAssignment_0_3()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1969:1: ( rule__FeatureCorrespondence__RightAssignment_0_3 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1969:2: rule__FeatureCorrespondence__RightAssignment_0_3
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__RightAssignment_0_3_in_rule__FeatureCorrespondence__Group_0__3__Impl4006);
            rule__FeatureCorrespondence__RightAssignment_0_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureCorrespondenceAccess().getRightAssignment_0_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__Group_0__3__Impl"


    // $ANTLR start "rule__FeatureCorrespondence__Group_1__0"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1987:1: rule__FeatureCorrespondence__Group_1__0 : rule__FeatureCorrespondence__Group_1__0__Impl rule__FeatureCorrespondence__Group_1__1 ;
    public final void rule__FeatureCorrespondence__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1991:1: ( rule__FeatureCorrespondence__Group_1__0__Impl rule__FeatureCorrespondence__Group_1__1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1992:2: rule__FeatureCorrespondence__Group_1__0__Impl rule__FeatureCorrespondence__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_1__0__Impl_in_rule__FeatureCorrespondence__Group_1__04044);
            rule__FeatureCorrespondence__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_1__1_in_rule__FeatureCorrespondence__Group_1__04047);
            rule__FeatureCorrespondence__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__Group_1__0"


    // $ANTLR start "rule__FeatureCorrespondence__Group_1__0__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:1999:1: rule__FeatureCorrespondence__Group_1__0__Impl : ( () ) ;
    public final void rule__FeatureCorrespondence__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2003:1: ( ( () ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2004:1: ( () )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2004:1: ( () )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2005:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureCorrespondenceAccess().getOneToOneCorrespondenceAction_1_0()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2006:1: ()
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2008:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureCorrespondenceAccess().getOneToOneCorrespondenceAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__Group_1__0__Impl"


    // $ANTLR start "rule__FeatureCorrespondence__Group_1__1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2018:1: rule__FeatureCorrespondence__Group_1__1 : rule__FeatureCorrespondence__Group_1__1__Impl rule__FeatureCorrespondence__Group_1__2 ;
    public final void rule__FeatureCorrespondence__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2022:1: ( rule__FeatureCorrespondence__Group_1__1__Impl rule__FeatureCorrespondence__Group_1__2 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2023:2: rule__FeatureCorrespondence__Group_1__1__Impl rule__FeatureCorrespondence__Group_1__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_1__1__Impl_in_rule__FeatureCorrespondence__Group_1__14105);
            rule__FeatureCorrespondence__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_1__2_in_rule__FeatureCorrespondence__Group_1__14108);
            rule__FeatureCorrespondence__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__Group_1__1"


    // $ANTLR start "rule__FeatureCorrespondence__Group_1__1__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2030:1: rule__FeatureCorrespondence__Group_1__1__Impl : ( ( rule__FeatureCorrespondence__LeftAssignment_1_1 ) ) ;
    public final void rule__FeatureCorrespondence__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2034:1: ( ( ( rule__FeatureCorrespondence__LeftAssignment_1_1 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2035:1: ( ( rule__FeatureCorrespondence__LeftAssignment_1_1 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2035:1: ( ( rule__FeatureCorrespondence__LeftAssignment_1_1 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2036:1: ( rule__FeatureCorrespondence__LeftAssignment_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureCorrespondenceAccess().getLeftAssignment_1_1()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2037:1: ( rule__FeatureCorrespondence__LeftAssignment_1_1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2037:2: rule__FeatureCorrespondence__LeftAssignment_1_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__LeftAssignment_1_1_in_rule__FeatureCorrespondence__Group_1__1__Impl4135);
            rule__FeatureCorrespondence__LeftAssignment_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureCorrespondenceAccess().getLeftAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__Group_1__1__Impl"


    // $ANTLR start "rule__FeatureCorrespondence__Group_1__2"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2047:1: rule__FeatureCorrespondence__Group_1__2 : rule__FeatureCorrespondence__Group_1__2__Impl rule__FeatureCorrespondence__Group_1__3 ;
    public final void rule__FeatureCorrespondence__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2051:1: ( rule__FeatureCorrespondence__Group_1__2__Impl rule__FeatureCorrespondence__Group_1__3 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2052:2: rule__FeatureCorrespondence__Group_1__2__Impl rule__FeatureCorrespondence__Group_1__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_1__2__Impl_in_rule__FeatureCorrespondence__Group_1__24165);
            rule__FeatureCorrespondence__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_1__3_in_rule__FeatureCorrespondence__Group_1__24168);
            rule__FeatureCorrespondence__Group_1__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__Group_1__2"


    // $ANTLR start "rule__FeatureCorrespondence__Group_1__2__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2059:1: rule__FeatureCorrespondence__Group_1__2__Impl : ( '==' ) ;
    public final void rule__FeatureCorrespondence__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2063:1: ( ( '==' ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2064:1: ( '==' )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2064:1: ( '==' )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2065:1: '=='
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureCorrespondenceAccess().getEqualsSignEqualsSignKeyword_1_2()); 
            }
            match(input,15,FollowSets000.FOLLOW_15_in_rule__FeatureCorrespondence__Group_1__2__Impl4196); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureCorrespondenceAccess().getEqualsSignEqualsSignKeyword_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__Group_1__2__Impl"


    // $ANTLR start "rule__FeatureCorrespondence__Group_1__3"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2078:1: rule__FeatureCorrespondence__Group_1__3 : rule__FeatureCorrespondence__Group_1__3__Impl ;
    public final void rule__FeatureCorrespondence__Group_1__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2082:1: ( rule__FeatureCorrespondence__Group_1__3__Impl )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2083:2: rule__FeatureCorrespondence__Group_1__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_1__3__Impl_in_rule__FeatureCorrespondence__Group_1__34227);
            rule__FeatureCorrespondence__Group_1__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__Group_1__3"


    // $ANTLR start "rule__FeatureCorrespondence__Group_1__3__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2089:1: rule__FeatureCorrespondence__Group_1__3__Impl : ( ( rule__FeatureCorrespondence__RightAssignment_1_3 ) ) ;
    public final void rule__FeatureCorrespondence__Group_1__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2093:1: ( ( ( rule__FeatureCorrespondence__RightAssignment_1_3 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2094:1: ( ( rule__FeatureCorrespondence__RightAssignment_1_3 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2094:1: ( ( rule__FeatureCorrespondence__RightAssignment_1_3 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2095:1: ( rule__FeatureCorrespondence__RightAssignment_1_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureCorrespondenceAccess().getRightAssignment_1_3()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2096:1: ( rule__FeatureCorrespondence__RightAssignment_1_3 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2096:2: rule__FeatureCorrespondence__RightAssignment_1_3
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__RightAssignment_1_3_in_rule__FeatureCorrespondence__Group_1__3__Impl4254);
            rule__FeatureCorrespondence__RightAssignment_1_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureCorrespondenceAccess().getRightAssignment_1_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__Group_1__3__Impl"


    // $ANTLR start "rule__FeatureCorrespondence__Group_2__0"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2114:1: rule__FeatureCorrespondence__Group_2__0 : rule__FeatureCorrespondence__Group_2__0__Impl rule__FeatureCorrespondence__Group_2__1 ;
    public final void rule__FeatureCorrespondence__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2118:1: ( rule__FeatureCorrespondence__Group_2__0__Impl rule__FeatureCorrespondence__Group_2__1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2119:2: rule__FeatureCorrespondence__Group_2__0__Impl rule__FeatureCorrespondence__Group_2__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_2__0__Impl_in_rule__FeatureCorrespondence__Group_2__04292);
            rule__FeatureCorrespondence__Group_2__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_2__1_in_rule__FeatureCorrespondence__Group_2__04295);
            rule__FeatureCorrespondence__Group_2__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__Group_2__0"


    // $ANTLR start "rule__FeatureCorrespondence__Group_2__0__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2126:1: rule__FeatureCorrespondence__Group_2__0__Impl : ( () ) ;
    public final void rule__FeatureCorrespondence__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2130:1: ( ( () ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2131:1: ( () )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2131:1: ( () )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2132:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureCorrespondenceAccess().getLeftToRightCorrespondenceAction_2_0()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2133:1: ()
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2135:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureCorrespondenceAccess().getLeftToRightCorrespondenceAction_2_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__Group_2__0__Impl"


    // $ANTLR start "rule__FeatureCorrespondence__Group_2__1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2145:1: rule__FeatureCorrespondence__Group_2__1 : rule__FeatureCorrespondence__Group_2__1__Impl rule__FeatureCorrespondence__Group_2__2 ;
    public final void rule__FeatureCorrespondence__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2149:1: ( rule__FeatureCorrespondence__Group_2__1__Impl rule__FeatureCorrespondence__Group_2__2 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2150:2: rule__FeatureCorrespondence__Group_2__1__Impl rule__FeatureCorrespondence__Group_2__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_2__1__Impl_in_rule__FeatureCorrespondence__Group_2__14353);
            rule__FeatureCorrespondence__Group_2__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_2__2_in_rule__FeatureCorrespondence__Group_2__14356);
            rule__FeatureCorrespondence__Group_2__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__Group_2__1"


    // $ANTLR start "rule__FeatureCorrespondence__Group_2__1__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2157:1: rule__FeatureCorrespondence__Group_2__1__Impl : ( ( rule__FeatureCorrespondence__LeftAssignment_2_1 ) ) ;
    public final void rule__FeatureCorrespondence__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2161:1: ( ( ( rule__FeatureCorrespondence__LeftAssignment_2_1 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2162:1: ( ( rule__FeatureCorrespondence__LeftAssignment_2_1 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2162:1: ( ( rule__FeatureCorrespondence__LeftAssignment_2_1 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2163:1: ( rule__FeatureCorrespondence__LeftAssignment_2_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureCorrespondenceAccess().getLeftAssignment_2_1()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2164:1: ( rule__FeatureCorrespondence__LeftAssignment_2_1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2164:2: rule__FeatureCorrespondence__LeftAssignment_2_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__LeftAssignment_2_1_in_rule__FeatureCorrespondence__Group_2__1__Impl4383);
            rule__FeatureCorrespondence__LeftAssignment_2_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureCorrespondenceAccess().getLeftAssignment_2_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__Group_2__1__Impl"


    // $ANTLR start "rule__FeatureCorrespondence__Group_2__2"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2174:1: rule__FeatureCorrespondence__Group_2__2 : rule__FeatureCorrespondence__Group_2__2__Impl rule__FeatureCorrespondence__Group_2__3 ;
    public final void rule__FeatureCorrespondence__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2178:1: ( rule__FeatureCorrespondence__Group_2__2__Impl rule__FeatureCorrespondence__Group_2__3 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2179:2: rule__FeatureCorrespondence__Group_2__2__Impl rule__FeatureCorrespondence__Group_2__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_2__2__Impl_in_rule__FeatureCorrespondence__Group_2__24413);
            rule__FeatureCorrespondence__Group_2__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_2__3_in_rule__FeatureCorrespondence__Group_2__24416);
            rule__FeatureCorrespondence__Group_2__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__Group_2__2"


    // $ANTLR start "rule__FeatureCorrespondence__Group_2__2__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2186:1: rule__FeatureCorrespondence__Group_2__2__Impl : ( '->' ) ;
    public final void rule__FeatureCorrespondence__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2190:1: ( ( '->' ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2191:1: ( '->' )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2191:1: ( '->' )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2192:1: '->'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureCorrespondenceAccess().getHyphenMinusGreaterThanSignKeyword_2_2()); 
            }
            match(input,16,FollowSets000.FOLLOW_16_in_rule__FeatureCorrespondence__Group_2__2__Impl4444); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureCorrespondenceAccess().getHyphenMinusGreaterThanSignKeyword_2_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__Group_2__2__Impl"


    // $ANTLR start "rule__FeatureCorrespondence__Group_2__3"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2205:1: rule__FeatureCorrespondence__Group_2__3 : rule__FeatureCorrespondence__Group_2__3__Impl ;
    public final void rule__FeatureCorrespondence__Group_2__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2209:1: ( rule__FeatureCorrespondence__Group_2__3__Impl )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2210:2: rule__FeatureCorrespondence__Group_2__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_2__3__Impl_in_rule__FeatureCorrespondence__Group_2__34475);
            rule__FeatureCorrespondence__Group_2__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__Group_2__3"


    // $ANTLR start "rule__FeatureCorrespondence__Group_2__3__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2216:1: rule__FeatureCorrespondence__Group_2__3__Impl : ( ( rule__FeatureCorrespondence__RightAssignment_2_3 ) ) ;
    public final void rule__FeatureCorrespondence__Group_2__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2220:1: ( ( ( rule__FeatureCorrespondence__RightAssignment_2_3 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2221:1: ( ( rule__FeatureCorrespondence__RightAssignment_2_3 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2221:1: ( ( rule__FeatureCorrespondence__RightAssignment_2_3 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2222:1: ( rule__FeatureCorrespondence__RightAssignment_2_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureCorrespondenceAccess().getRightAssignment_2_3()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2223:1: ( rule__FeatureCorrespondence__RightAssignment_2_3 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2223:2: rule__FeatureCorrespondence__RightAssignment_2_3
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__RightAssignment_2_3_in_rule__FeatureCorrespondence__Group_2__3__Impl4502);
            rule__FeatureCorrespondence__RightAssignment_2_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureCorrespondenceAccess().getRightAssignment_2_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__Group_2__3__Impl"


    // $ANTLR start "rule__FeatureCorrespondence__Group_3__0"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2241:1: rule__FeatureCorrespondence__Group_3__0 : rule__FeatureCorrespondence__Group_3__0__Impl rule__FeatureCorrespondence__Group_3__1 ;
    public final void rule__FeatureCorrespondence__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2245:1: ( rule__FeatureCorrespondence__Group_3__0__Impl rule__FeatureCorrespondence__Group_3__1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2246:2: rule__FeatureCorrespondence__Group_3__0__Impl rule__FeatureCorrespondence__Group_3__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_3__0__Impl_in_rule__FeatureCorrespondence__Group_3__04540);
            rule__FeatureCorrespondence__Group_3__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_3__1_in_rule__FeatureCorrespondence__Group_3__04543);
            rule__FeatureCorrespondence__Group_3__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__Group_3__0"


    // $ANTLR start "rule__FeatureCorrespondence__Group_3__0__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2253:1: rule__FeatureCorrespondence__Group_3__0__Impl : ( () ) ;
    public final void rule__FeatureCorrespondence__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2257:1: ( ( () ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2258:1: ( () )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2258:1: ( () )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2259:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureCorrespondenceAccess().getRightToLeftCorrespondenceAction_3_0()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2260:1: ()
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2262:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureCorrespondenceAccess().getRightToLeftCorrespondenceAction_3_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__Group_3__0__Impl"


    // $ANTLR start "rule__FeatureCorrespondence__Group_3__1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2272:1: rule__FeatureCorrespondence__Group_3__1 : rule__FeatureCorrespondence__Group_3__1__Impl rule__FeatureCorrespondence__Group_3__2 ;
    public final void rule__FeatureCorrespondence__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2276:1: ( rule__FeatureCorrespondence__Group_3__1__Impl rule__FeatureCorrespondence__Group_3__2 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2277:2: rule__FeatureCorrespondence__Group_3__1__Impl rule__FeatureCorrespondence__Group_3__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_3__1__Impl_in_rule__FeatureCorrespondence__Group_3__14601);
            rule__FeatureCorrespondence__Group_3__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_3__2_in_rule__FeatureCorrespondence__Group_3__14604);
            rule__FeatureCorrespondence__Group_3__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__Group_3__1"


    // $ANTLR start "rule__FeatureCorrespondence__Group_3__1__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2284:1: rule__FeatureCorrespondence__Group_3__1__Impl : ( ( rule__FeatureCorrespondence__LeftAssignment_3_1 ) ) ;
    public final void rule__FeatureCorrespondence__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2288:1: ( ( ( rule__FeatureCorrespondence__LeftAssignment_3_1 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2289:1: ( ( rule__FeatureCorrespondence__LeftAssignment_3_1 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2289:1: ( ( rule__FeatureCorrespondence__LeftAssignment_3_1 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2290:1: ( rule__FeatureCorrespondence__LeftAssignment_3_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureCorrespondenceAccess().getLeftAssignment_3_1()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2291:1: ( rule__FeatureCorrespondence__LeftAssignment_3_1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2291:2: rule__FeatureCorrespondence__LeftAssignment_3_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__LeftAssignment_3_1_in_rule__FeatureCorrespondence__Group_3__1__Impl4631);
            rule__FeatureCorrespondence__LeftAssignment_3_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureCorrespondenceAccess().getLeftAssignment_3_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__Group_3__1__Impl"


    // $ANTLR start "rule__FeatureCorrespondence__Group_3__2"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2301:1: rule__FeatureCorrespondence__Group_3__2 : rule__FeatureCorrespondence__Group_3__2__Impl rule__FeatureCorrespondence__Group_3__3 ;
    public final void rule__FeatureCorrespondence__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2305:1: ( rule__FeatureCorrespondence__Group_3__2__Impl rule__FeatureCorrespondence__Group_3__3 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2306:2: rule__FeatureCorrespondence__Group_3__2__Impl rule__FeatureCorrespondence__Group_3__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_3__2__Impl_in_rule__FeatureCorrespondence__Group_3__24661);
            rule__FeatureCorrespondence__Group_3__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_3__3_in_rule__FeatureCorrespondence__Group_3__24664);
            rule__FeatureCorrespondence__Group_3__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__Group_3__2"


    // $ANTLR start "rule__FeatureCorrespondence__Group_3__2__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2313:1: rule__FeatureCorrespondence__Group_3__2__Impl : ( '<-' ) ;
    public final void rule__FeatureCorrespondence__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2317:1: ( ( '<-' ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2318:1: ( '<-' )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2318:1: ( '<-' )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2319:1: '<-'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureCorrespondenceAccess().getLessThanSignHyphenMinusKeyword_3_2()); 
            }
            match(input,17,FollowSets000.FOLLOW_17_in_rule__FeatureCorrespondence__Group_3__2__Impl4692); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureCorrespondenceAccess().getLessThanSignHyphenMinusKeyword_3_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__Group_3__2__Impl"


    // $ANTLR start "rule__FeatureCorrespondence__Group_3__3"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2332:1: rule__FeatureCorrespondence__Group_3__3 : rule__FeatureCorrespondence__Group_3__3__Impl ;
    public final void rule__FeatureCorrespondence__Group_3__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2336:1: ( rule__FeatureCorrespondence__Group_3__3__Impl )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2337:2: rule__FeatureCorrespondence__Group_3__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_3__3__Impl_in_rule__FeatureCorrespondence__Group_3__34723);
            rule__FeatureCorrespondence__Group_3__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__Group_3__3"


    // $ANTLR start "rule__FeatureCorrespondence__Group_3__3__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2343:1: rule__FeatureCorrespondence__Group_3__3__Impl : ( ( rule__FeatureCorrespondence__RightAssignment_3_3 ) ) ;
    public final void rule__FeatureCorrespondence__Group_3__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2347:1: ( ( ( rule__FeatureCorrespondence__RightAssignment_3_3 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2348:1: ( ( rule__FeatureCorrespondence__RightAssignment_3_3 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2348:1: ( ( rule__FeatureCorrespondence__RightAssignment_3_3 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2349:1: ( rule__FeatureCorrespondence__RightAssignment_3_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureCorrespondenceAccess().getRightAssignment_3_3()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2350:1: ( rule__FeatureCorrespondence__RightAssignment_3_3 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2350:2: rule__FeatureCorrespondence__RightAssignment_3_3
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__RightAssignment_3_3_in_rule__FeatureCorrespondence__Group_3__3__Impl4750);
            rule__FeatureCorrespondence__RightAssignment_3_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureCorrespondenceAccess().getRightAssignment_3_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__Group_3__3__Impl"


    // $ANTLR start "rule__FeatureFunction__Group__0"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2368:1: rule__FeatureFunction__Group__0 : rule__FeatureFunction__Group__0__Impl rule__FeatureFunction__Group__1 ;
    public final void rule__FeatureFunction__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2372:1: ( rule__FeatureFunction__Group__0__Impl rule__FeatureFunction__Group__1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2373:2: rule__FeatureFunction__Group__0__Impl rule__FeatureFunction__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureFunction__Group__0__Impl_in_rule__FeatureFunction__Group__04788);
            rule__FeatureFunction__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__FeatureFunction__Group__1_in_rule__FeatureFunction__Group__04791);
            rule__FeatureFunction__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureFunction__Group__0"


    // $ANTLR start "rule__FeatureFunction__Group__0__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2380:1: rule__FeatureFunction__Group__0__Impl : ( ( rule__FeatureFunction__TermsAssignment_0 ) ) ;
    public final void rule__FeatureFunction__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2384:1: ( ( ( rule__FeatureFunction__TermsAssignment_0 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2385:1: ( ( rule__FeatureFunction__TermsAssignment_0 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2385:1: ( ( rule__FeatureFunction__TermsAssignment_0 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2386:1: ( rule__FeatureFunction__TermsAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureFunctionAccess().getTermsAssignment_0()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2387:1: ( rule__FeatureFunction__TermsAssignment_0 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2387:2: rule__FeatureFunction__TermsAssignment_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureFunction__TermsAssignment_0_in_rule__FeatureFunction__Group__0__Impl4818);
            rule__FeatureFunction__TermsAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureFunctionAccess().getTermsAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureFunction__Group__0__Impl"


    // $ANTLR start "rule__FeatureFunction__Group__1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2397:1: rule__FeatureFunction__Group__1 : rule__FeatureFunction__Group__1__Impl ;
    public final void rule__FeatureFunction__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2401:1: ( rule__FeatureFunction__Group__1__Impl )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2402:2: rule__FeatureFunction__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureFunction__Group__1__Impl_in_rule__FeatureFunction__Group__14848);
            rule__FeatureFunction__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureFunction__Group__1"


    // $ANTLR start "rule__FeatureFunction__Group__1__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2408:1: rule__FeatureFunction__Group__1__Impl : ( ( rule__FeatureFunction__Group_1__0 )* ) ;
    public final void rule__FeatureFunction__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2412:1: ( ( ( rule__FeatureFunction__Group_1__0 )* ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2413:1: ( ( rule__FeatureFunction__Group_1__0 )* )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2413:1: ( ( rule__FeatureFunction__Group_1__0 )* )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2414:1: ( rule__FeatureFunction__Group_1__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureFunctionAccess().getGroup_1()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2415:1: ( rule__FeatureFunction__Group_1__0 )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==18) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2415:2: rule__FeatureFunction__Group_1__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__FeatureFunction__Group_1__0_in_rule__FeatureFunction__Group__1__Impl4875);
            	    rule__FeatureFunction__Group_1__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureFunctionAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureFunction__Group__1__Impl"


    // $ANTLR start "rule__FeatureFunction__Group_1__0"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2429:1: rule__FeatureFunction__Group_1__0 : rule__FeatureFunction__Group_1__0__Impl rule__FeatureFunction__Group_1__1 ;
    public final void rule__FeatureFunction__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2433:1: ( rule__FeatureFunction__Group_1__0__Impl rule__FeatureFunction__Group_1__1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2434:2: rule__FeatureFunction__Group_1__0__Impl rule__FeatureFunction__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureFunction__Group_1__0__Impl_in_rule__FeatureFunction__Group_1__04910);
            rule__FeatureFunction__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__FeatureFunction__Group_1__1_in_rule__FeatureFunction__Group_1__04913);
            rule__FeatureFunction__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureFunction__Group_1__0"


    // $ANTLR start "rule__FeatureFunction__Group_1__0__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2441:1: rule__FeatureFunction__Group_1__0__Impl : ( '+' ) ;
    public final void rule__FeatureFunction__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2445:1: ( ( '+' ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2446:1: ( '+' )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2446:1: ( '+' )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2447:1: '+'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureFunctionAccess().getPlusSignKeyword_1_0()); 
            }
            match(input,18,FollowSets000.FOLLOW_18_in_rule__FeatureFunction__Group_1__0__Impl4941); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureFunctionAccess().getPlusSignKeyword_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureFunction__Group_1__0__Impl"


    // $ANTLR start "rule__FeatureFunction__Group_1__1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2460:1: rule__FeatureFunction__Group_1__1 : rule__FeatureFunction__Group_1__1__Impl ;
    public final void rule__FeatureFunction__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2464:1: ( rule__FeatureFunction__Group_1__1__Impl )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2465:2: rule__FeatureFunction__Group_1__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureFunction__Group_1__1__Impl_in_rule__FeatureFunction__Group_1__14972);
            rule__FeatureFunction__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureFunction__Group_1__1"


    // $ANTLR start "rule__FeatureFunction__Group_1__1__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2471:1: rule__FeatureFunction__Group_1__1__Impl : ( ( rule__FeatureFunction__TermsAssignment_1_1 ) ) ;
    public final void rule__FeatureFunction__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2475:1: ( ( ( rule__FeatureFunction__TermsAssignment_1_1 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2476:1: ( ( rule__FeatureFunction__TermsAssignment_1_1 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2476:1: ( ( rule__FeatureFunction__TermsAssignment_1_1 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2477:1: ( rule__FeatureFunction__TermsAssignment_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureFunctionAccess().getTermsAssignment_1_1()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2478:1: ( rule__FeatureFunction__TermsAssignment_1_1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2478:2: rule__FeatureFunction__TermsAssignment_1_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureFunction__TermsAssignment_1_1_in_rule__FeatureFunction__Group_1__1__Impl4999);
            rule__FeatureFunction__TermsAssignment_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureFunctionAccess().getTermsAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureFunction__Group_1__1__Impl"


    // $ANTLR start "rule__FeatureRef__Group__0"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2492:1: rule__FeatureRef__Group__0 : rule__FeatureRef__Group__0__Impl rule__FeatureRef__Group__1 ;
    public final void rule__FeatureRef__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2496:1: ( rule__FeatureRef__Group__0__Impl rule__FeatureRef__Group__1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2497:2: rule__FeatureRef__Group__0__Impl rule__FeatureRef__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureRef__Group__0__Impl_in_rule__FeatureRef__Group__05033);
            rule__FeatureRef__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__FeatureRef__Group__1_in_rule__FeatureRef__Group__05036);
            rule__FeatureRef__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureRef__Group__0"


    // $ANTLR start "rule__FeatureRef__Group__0__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2504:1: rule__FeatureRef__Group__0__Impl : ( ( rule__FeatureRef__Group_0__0 )? ) ;
    public final void rule__FeatureRef__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2508:1: ( ( ( rule__FeatureRef__Group_0__0 )? ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2509:1: ( ( rule__FeatureRef__Group_0__0 )? )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2509:1: ( ( rule__FeatureRef__Group_0__0 )? )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2510:1: ( rule__FeatureRef__Group_0__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureRefAccess().getGroup_0()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2511:1: ( rule__FeatureRef__Group_0__0 )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( ((LA12_0>=20 && LA12_0<=21)) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2511:2: rule__FeatureRef__Group_0__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__FeatureRef__Group_0__0_in_rule__FeatureRef__Group__0__Impl5063);
                    rule__FeatureRef__Group_0__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureRefAccess().getGroup_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureRef__Group__0__Impl"


    // $ANTLR start "rule__FeatureRef__Group__1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2521:1: rule__FeatureRef__Group__1 : rule__FeatureRef__Group__1__Impl ;
    public final void rule__FeatureRef__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2525:1: ( rule__FeatureRef__Group__1__Impl )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2526:2: rule__FeatureRef__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureRef__Group__1__Impl_in_rule__FeatureRef__Group__15094);
            rule__FeatureRef__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureRef__Group__1"


    // $ANTLR start "rule__FeatureRef__Group__1__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2532:1: rule__FeatureRef__Group__1__Impl : ( ( rule__FeatureRef__ReferencedFeatureAssignment_1 ) ) ;
    public final void rule__FeatureRef__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2536:1: ( ( ( rule__FeatureRef__ReferencedFeatureAssignment_1 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2537:1: ( ( rule__FeatureRef__ReferencedFeatureAssignment_1 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2537:1: ( ( rule__FeatureRef__ReferencedFeatureAssignment_1 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2538:1: ( rule__FeatureRef__ReferencedFeatureAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureRefAccess().getReferencedFeatureAssignment_1()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2539:1: ( rule__FeatureRef__ReferencedFeatureAssignment_1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2539:2: rule__FeatureRef__ReferencedFeatureAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureRef__ReferencedFeatureAssignment_1_in_rule__FeatureRef__Group__1__Impl5121);
            rule__FeatureRef__ReferencedFeatureAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureRefAccess().getReferencedFeatureAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureRef__Group__1__Impl"


    // $ANTLR start "rule__FeatureRef__Group_0__0"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2553:1: rule__FeatureRef__Group_0__0 : rule__FeatureRef__Group_0__0__Impl rule__FeatureRef__Group_0__1 ;
    public final void rule__FeatureRef__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2557:1: ( rule__FeatureRef__Group_0__0__Impl rule__FeatureRef__Group_0__1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2558:2: rule__FeatureRef__Group_0__0__Impl rule__FeatureRef__Group_0__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureRef__Group_0__0__Impl_in_rule__FeatureRef__Group_0__05155);
            rule__FeatureRef__Group_0__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__FeatureRef__Group_0__1_in_rule__FeatureRef__Group_0__05158);
            rule__FeatureRef__Group_0__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureRef__Group_0__0"


    // $ANTLR start "rule__FeatureRef__Group_0__0__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2565:1: rule__FeatureRef__Group_0__0__Impl : ( ( rule__FeatureRef__InstanceAssignment_0_0 ) ) ;
    public final void rule__FeatureRef__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2569:1: ( ( ( rule__FeatureRef__InstanceAssignment_0_0 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2570:1: ( ( rule__FeatureRef__InstanceAssignment_0_0 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2570:1: ( ( rule__FeatureRef__InstanceAssignment_0_0 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2571:1: ( rule__FeatureRef__InstanceAssignment_0_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureRefAccess().getInstanceAssignment_0_0()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2572:1: ( rule__FeatureRef__InstanceAssignment_0_0 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2572:2: rule__FeatureRef__InstanceAssignment_0_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureRef__InstanceAssignment_0_0_in_rule__FeatureRef__Group_0__0__Impl5185);
            rule__FeatureRef__InstanceAssignment_0_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureRefAccess().getInstanceAssignment_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureRef__Group_0__0__Impl"


    // $ANTLR start "rule__FeatureRef__Group_0__1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2582:1: rule__FeatureRef__Group_0__1 : rule__FeatureRef__Group_0__1__Impl ;
    public final void rule__FeatureRef__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2586:1: ( rule__FeatureRef__Group_0__1__Impl )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2587:2: rule__FeatureRef__Group_0__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__FeatureRef__Group_0__1__Impl_in_rule__FeatureRef__Group_0__15215);
            rule__FeatureRef__Group_0__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureRef__Group_0__1"


    // $ANTLR start "rule__FeatureRef__Group_0__1__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2593:1: rule__FeatureRef__Group_0__1__Impl : ( '.' ) ;
    public final void rule__FeatureRef__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2597:1: ( ( '.' ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2598:1: ( '.' )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2598:1: ( '.' )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2599:1: '.'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureRefAccess().getFullStopKeyword_0_1()); 
            }
            match(input,19,FollowSets000.FOLLOW_19_in_rule__FeatureRef__Group_0__1__Impl5243); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureRefAccess().getFullStopKeyword_0_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureRef__Group_0__1__Impl"


    // $ANTLR start "rule__Instance__Group__0"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2616:1: rule__Instance__Group__0 : rule__Instance__Group__0__Impl rule__Instance__Group__1 ;
    public final void rule__Instance__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2620:1: ( rule__Instance__Group__0__Impl rule__Instance__Group__1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2621:2: rule__Instance__Group__0__Impl rule__Instance__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Instance__Group__0__Impl_in_rule__Instance__Group__05278);
            rule__Instance__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Instance__Group__1_in_rule__Instance__Group__05281);
            rule__Instance__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instance__Group__0"


    // $ANTLR start "rule__Instance__Group__0__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2628:1: rule__Instance__Group__0__Impl : ( ( rule__Instance__Alternatives_0 ) ) ;
    public final void rule__Instance__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2632:1: ( ( ( rule__Instance__Alternatives_0 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2633:1: ( ( rule__Instance__Alternatives_0 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2633:1: ( ( rule__Instance__Alternatives_0 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2634:1: ( rule__Instance__Alternatives_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInstanceAccess().getAlternatives_0()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2635:1: ( rule__Instance__Alternatives_0 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2635:2: rule__Instance__Alternatives_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Instance__Alternatives_0_in_rule__Instance__Group__0__Impl5308);
            rule__Instance__Alternatives_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getInstanceAccess().getAlternatives_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instance__Group__0__Impl"


    // $ANTLR start "rule__Instance__Group__1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2645:1: rule__Instance__Group__1 : rule__Instance__Group__1__Impl rule__Instance__Group__2 ;
    public final void rule__Instance__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2649:1: ( rule__Instance__Group__1__Impl rule__Instance__Group__2 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2650:2: rule__Instance__Group__1__Impl rule__Instance__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Instance__Group__1__Impl_in_rule__Instance__Group__15338);
            rule__Instance__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Instance__Group__2_in_rule__Instance__Group__15341);
            rule__Instance__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instance__Group__1"


    // $ANTLR start "rule__Instance__Group__1__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2657:1: rule__Instance__Group__1__Impl : ( ( rule__Instance__TypeAssignment_1 ) ) ;
    public final void rule__Instance__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2661:1: ( ( ( rule__Instance__TypeAssignment_1 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2662:1: ( ( rule__Instance__TypeAssignment_1 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2662:1: ( ( rule__Instance__TypeAssignment_1 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2663:1: ( rule__Instance__TypeAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInstanceAccess().getTypeAssignment_1()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2664:1: ( rule__Instance__TypeAssignment_1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2664:2: rule__Instance__TypeAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Instance__TypeAssignment_1_in_rule__Instance__Group__1__Impl5368);
            rule__Instance__TypeAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getInstanceAccess().getTypeAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instance__Group__1__Impl"


    // $ANTLR start "rule__Instance__Group__2"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2674:1: rule__Instance__Group__2 : rule__Instance__Group__2__Impl rule__Instance__Group__3 ;
    public final void rule__Instance__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2678:1: ( rule__Instance__Group__2__Impl rule__Instance__Group__3 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2679:2: rule__Instance__Group__2__Impl rule__Instance__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Instance__Group__2__Impl_in_rule__Instance__Group__25398);
            rule__Instance__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Instance__Group__3_in_rule__Instance__Group__25401);
            rule__Instance__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instance__Group__2"


    // $ANTLR start "rule__Instance__Group__2__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2686:1: rule__Instance__Group__2__Impl : ( ( rule__Instance__ManyAssignment_2 )? ) ;
    public final void rule__Instance__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2690:1: ( ( ( rule__Instance__ManyAssignment_2 )? ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2691:1: ( ( rule__Instance__ManyAssignment_2 )? )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2691:1: ( ( rule__Instance__ManyAssignment_2 )? )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2692:1: ( rule__Instance__ManyAssignment_2 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInstanceAccess().getManyAssignment_2()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2693:1: ( rule__Instance__ManyAssignment_2 )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==27) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2693:2: rule__Instance__ManyAssignment_2
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Instance__ManyAssignment_2_in_rule__Instance__Group__2__Impl5428);
                    rule__Instance__ManyAssignment_2();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getInstanceAccess().getManyAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instance__Group__2__Impl"


    // $ANTLR start "rule__Instance__Group__3"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2703:1: rule__Instance__Group__3 : rule__Instance__Group__3__Impl rule__Instance__Group__4 ;
    public final void rule__Instance__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2707:1: ( rule__Instance__Group__3__Impl rule__Instance__Group__4 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2708:2: rule__Instance__Group__3__Impl rule__Instance__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Instance__Group__3__Impl_in_rule__Instance__Group__35459);
            rule__Instance__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Instance__Group__4_in_rule__Instance__Group__35462);
            rule__Instance__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instance__Group__3"


    // $ANTLR start "rule__Instance__Group__3__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2715:1: rule__Instance__Group__3__Impl : ( '{' ) ;
    public final void rule__Instance__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2719:1: ( ( '{' ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2720:1: ( '{' )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2720:1: ( '{' )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2721:1: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInstanceAccess().getLeftCurlyBracketKeyword_3()); 
            }
            match(input,13,FollowSets000.FOLLOW_13_in_rule__Instance__Group__3__Impl5490); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getInstanceAccess().getLeftCurlyBracketKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instance__Group__3__Impl"


    // $ANTLR start "rule__Instance__Group__4"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2734:1: rule__Instance__Group__4 : rule__Instance__Group__4__Impl rule__Instance__Group__5 ;
    public final void rule__Instance__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2738:1: ( rule__Instance__Group__4__Impl rule__Instance__Group__5 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2739:2: rule__Instance__Group__4__Impl rule__Instance__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_rule__Instance__Group__4__Impl_in_rule__Instance__Group__45521);
            rule__Instance__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Instance__Group__5_in_rule__Instance__Group__45524);
            rule__Instance__Group__5();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instance__Group__4"


    // $ANTLR start "rule__Instance__Group__4__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2746:1: rule__Instance__Group__4__Impl : ( ( rule__Instance__AssignmentsAssignment_4 )* ) ;
    public final void rule__Instance__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2750:1: ( ( ( rule__Instance__AssignmentsAssignment_4 )* ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2751:1: ( ( rule__Instance__AssignmentsAssignment_4 )* )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2751:1: ( ( rule__Instance__AssignmentsAssignment_4 )* )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2752:1: ( rule__Instance__AssignmentsAssignment_4 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInstanceAccess().getAssignmentsAssignment_4()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2753:1: ( rule__Instance__AssignmentsAssignment_4 )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==RULE_ID) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2753:2: rule__Instance__AssignmentsAssignment_4
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Instance__AssignmentsAssignment_4_in_rule__Instance__Group__4__Impl5551);
            	    rule__Instance__AssignmentsAssignment_4();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getInstanceAccess().getAssignmentsAssignment_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instance__Group__4__Impl"


    // $ANTLR start "rule__Instance__Group__5"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2763:1: rule__Instance__Group__5 : rule__Instance__Group__5__Impl ;
    public final void rule__Instance__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2767:1: ( rule__Instance__Group__5__Impl )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2768:2: rule__Instance__Group__5__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Instance__Group__5__Impl_in_rule__Instance__Group__55582);
            rule__Instance__Group__5__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instance__Group__5"


    // $ANTLR start "rule__Instance__Group__5__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2774:1: rule__Instance__Group__5__Impl : ( '}' ) ;
    public final void rule__Instance__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2778:1: ( ( '}' ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2779:1: ( '}' )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2779:1: ( '}' )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2780:1: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInstanceAccess().getRightCurlyBracketKeyword_5()); 
            }
            match(input,14,FollowSets000.FOLLOW_14_in_rule__Instance__Group__5__Impl5610); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getInstanceAccess().getRightCurlyBracketKeyword_5()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instance__Group__5__Impl"


    // $ANTLR start "rule__Instance__Group_0_0__0"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2805:1: rule__Instance__Group_0_0__0 : rule__Instance__Group_0_0__0__Impl rule__Instance__Group_0_0__1 ;
    public final void rule__Instance__Group_0_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2809:1: ( rule__Instance__Group_0_0__0__Impl rule__Instance__Group_0_0__1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2810:2: rule__Instance__Group_0_0__0__Impl rule__Instance__Group_0_0__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Instance__Group_0_0__0__Impl_in_rule__Instance__Group_0_0__05653);
            rule__Instance__Group_0_0__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Instance__Group_0_0__1_in_rule__Instance__Group_0_0__05656);
            rule__Instance__Group_0_0__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instance__Group_0_0__0"


    // $ANTLR start "rule__Instance__Group_0_0__0__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2817:1: rule__Instance__Group_0_0__0__Impl : ( () ) ;
    public final void rule__Instance__Group_0_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2821:1: ( ( () ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2822:1: ( () )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2822:1: ( () )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2823:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInstanceAccess().getExistingInstanceAction_0_0_0()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2824:1: ()
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2826:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getInstanceAccess().getExistingInstanceAction_0_0_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instance__Group_0_0__0__Impl"


    // $ANTLR start "rule__Instance__Group_0_0__1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2836:1: rule__Instance__Group_0_0__1 : rule__Instance__Group_0_0__1__Impl ;
    public final void rule__Instance__Group_0_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2840:1: ( rule__Instance__Group_0_0__1__Impl )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2841:2: rule__Instance__Group_0_0__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Instance__Group_0_0__1__Impl_in_rule__Instance__Group_0_0__15714);
            rule__Instance__Group_0_0__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instance__Group_0_0__1"


    // $ANTLR start "rule__Instance__Group_0_0__1__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2847:1: rule__Instance__Group_0_0__1__Impl : ( 'existing' ) ;
    public final void rule__Instance__Group_0_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2851:1: ( ( 'existing' ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2852:1: ( 'existing' )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2852:1: ( 'existing' )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2853:1: 'existing'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInstanceAccess().getExistingKeyword_0_0_1()); 
            }
            match(input,20,FollowSets000.FOLLOW_20_in_rule__Instance__Group_0_0__1__Impl5742); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getInstanceAccess().getExistingKeyword_0_0_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instance__Group_0_0__1__Impl"


    // $ANTLR start "rule__Instance__Group_0_1__0"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2870:1: rule__Instance__Group_0_1__0 : rule__Instance__Group_0_1__0__Impl rule__Instance__Group_0_1__1 ;
    public final void rule__Instance__Group_0_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2874:1: ( rule__Instance__Group_0_1__0__Impl rule__Instance__Group_0_1__1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2875:2: rule__Instance__Group_0_1__0__Impl rule__Instance__Group_0_1__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Instance__Group_0_1__0__Impl_in_rule__Instance__Group_0_1__05777);
            rule__Instance__Group_0_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Instance__Group_0_1__1_in_rule__Instance__Group_0_1__05780);
            rule__Instance__Group_0_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instance__Group_0_1__0"


    // $ANTLR start "rule__Instance__Group_0_1__0__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2882:1: rule__Instance__Group_0_1__0__Impl : ( () ) ;
    public final void rule__Instance__Group_0_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2886:1: ( ( () ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2887:1: ( () )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2887:1: ( () )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2888:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInstanceAccess().getNewInstanceAction_0_1_0()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2889:1: ()
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2891:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getInstanceAccess().getNewInstanceAction_0_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instance__Group_0_1__0__Impl"


    // $ANTLR start "rule__Instance__Group_0_1__1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2901:1: rule__Instance__Group_0_1__1 : rule__Instance__Group_0_1__1__Impl ;
    public final void rule__Instance__Group_0_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2905:1: ( rule__Instance__Group_0_1__1__Impl )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2906:2: rule__Instance__Group_0_1__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Instance__Group_0_1__1__Impl_in_rule__Instance__Group_0_1__15838);
            rule__Instance__Group_0_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instance__Group_0_1__1"


    // $ANTLR start "rule__Instance__Group_0_1__1__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2912:1: rule__Instance__Group_0_1__1__Impl : ( 'new' ) ;
    public final void rule__Instance__Group_0_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2916:1: ( ( 'new' ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2917:1: ( 'new' )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2917:1: ( 'new' )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2918:1: 'new'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInstanceAccess().getNewKeyword_0_1_1()); 
            }
            match(input,21,FollowSets000.FOLLOW_21_in_rule__Instance__Group_0_1__1__Impl5866); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getInstanceAccess().getNewKeyword_0_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instance__Group_0_1__1__Impl"


    // $ANTLR start "rule__Assignment__Group__0"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2935:1: rule__Assignment__Group__0 : rule__Assignment__Group__0__Impl rule__Assignment__Group__1 ;
    public final void rule__Assignment__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2939:1: ( rule__Assignment__Group__0__Impl rule__Assignment__Group__1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2940:2: rule__Assignment__Group__0__Impl rule__Assignment__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Assignment__Group__0__Impl_in_rule__Assignment__Group__05901);
            rule__Assignment__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Assignment__Group__1_in_rule__Assignment__Group__05904);
            rule__Assignment__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__Group__0"


    // $ANTLR start "rule__Assignment__Group__0__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2947:1: rule__Assignment__Group__0__Impl : ( ( rule__Assignment__FeatureAssignment_0 ) ) ;
    public final void rule__Assignment__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2951:1: ( ( ( rule__Assignment__FeatureAssignment_0 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2952:1: ( ( rule__Assignment__FeatureAssignment_0 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2952:1: ( ( rule__Assignment__FeatureAssignment_0 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2953:1: ( rule__Assignment__FeatureAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAssignmentAccess().getFeatureAssignment_0()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2954:1: ( rule__Assignment__FeatureAssignment_0 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2954:2: rule__Assignment__FeatureAssignment_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Assignment__FeatureAssignment_0_in_rule__Assignment__Group__0__Impl5931);
            rule__Assignment__FeatureAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAssignmentAccess().getFeatureAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__Group__0__Impl"


    // $ANTLR start "rule__Assignment__Group__1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2964:1: rule__Assignment__Group__1 : rule__Assignment__Group__1__Impl rule__Assignment__Group__2 ;
    public final void rule__Assignment__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2968:1: ( rule__Assignment__Group__1__Impl rule__Assignment__Group__2 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2969:2: rule__Assignment__Group__1__Impl rule__Assignment__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Assignment__Group__1__Impl_in_rule__Assignment__Group__15961);
            rule__Assignment__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Assignment__Group__2_in_rule__Assignment__Group__15964);
            rule__Assignment__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__Group__1"


    // $ANTLR start "rule__Assignment__Group__1__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2976:1: rule__Assignment__Group__1__Impl : ( '=' ) ;
    public final void rule__Assignment__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2980:1: ( ( '=' ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2981:1: ( '=' )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2981:1: ( '=' )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2982:1: '='
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAssignmentAccess().getEqualsSignKeyword_1()); 
            }
            match(input,22,FollowSets000.FOLLOW_22_in_rule__Assignment__Group__1__Impl5992); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAssignmentAccess().getEqualsSignKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__Group__1__Impl"


    // $ANTLR start "rule__Assignment__Group__2"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2995:1: rule__Assignment__Group__2 : rule__Assignment__Group__2__Impl ;
    public final void rule__Assignment__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:2999:1: ( rule__Assignment__Group__2__Impl )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3000:2: rule__Assignment__Group__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Assignment__Group__2__Impl_in_rule__Assignment__Group__26023);
            rule__Assignment__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__Group__2"


    // $ANTLR start "rule__Assignment__Group__2__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3006:1: rule__Assignment__Group__2__Impl : ( ( rule__Assignment__ValueAssignment_2 ) ) ;
    public final void rule__Assignment__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3010:1: ( ( ( rule__Assignment__ValueAssignment_2 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3011:1: ( ( rule__Assignment__ValueAssignment_2 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3011:1: ( ( rule__Assignment__ValueAssignment_2 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3012:1: ( rule__Assignment__ValueAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAssignmentAccess().getValueAssignment_2()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3013:1: ( rule__Assignment__ValueAssignment_2 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3013:2: rule__Assignment__ValueAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Assignment__ValueAssignment_2_in_rule__Assignment__Group__2__Impl6050);
            rule__Assignment__ValueAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAssignmentAccess().getValueAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__Group__2__Impl"


    // $ANTLR start "rule__ThisRef__Group__0"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3029:1: rule__ThisRef__Group__0 : rule__ThisRef__Group__0__Impl rule__ThisRef__Group__1 ;
    public final void rule__ThisRef__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3033:1: ( rule__ThisRef__Group__0__Impl rule__ThisRef__Group__1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3034:2: rule__ThisRef__Group__0__Impl rule__ThisRef__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__ThisRef__Group__0__Impl_in_rule__ThisRef__Group__06086);
            rule__ThisRef__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__ThisRef__Group__1_in_rule__ThisRef__Group__06089);
            rule__ThisRef__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThisRef__Group__0"


    // $ANTLR start "rule__ThisRef__Group__0__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3041:1: rule__ThisRef__Group__0__Impl : ( () ) ;
    public final void rule__ThisRef__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3045:1: ( ( () ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3046:1: ( () )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3046:1: ( () )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3047:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getThisRefAccess().getThisRefAction_0()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3048:1: ()
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3050:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getThisRefAccess().getThisRefAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThisRef__Group__0__Impl"


    // $ANTLR start "rule__ThisRef__Group__1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3060:1: rule__ThisRef__Group__1 : rule__ThisRef__Group__1__Impl ;
    public final void rule__ThisRef__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3064:1: ( rule__ThisRef__Group__1__Impl )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3065:2: rule__ThisRef__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__ThisRef__Group__1__Impl_in_rule__ThisRef__Group__16147);
            rule__ThisRef__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThisRef__Group__1"


    // $ANTLR start "rule__ThisRef__Group__1__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3071:1: rule__ThisRef__Group__1__Impl : ( 'this' ) ;
    public final void rule__ThisRef__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3075:1: ( ( 'this' ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3076:1: ( 'this' )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3076:1: ( 'this' )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3077:1: 'this'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getThisRefAccess().getThisKeyword_1()); 
            }
            match(input,23,FollowSets000.FOLLOW_23_in_rule__ThisRef__Group__1__Impl6175); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getThisRefAccess().getThisKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThisRef__Group__1__Impl"


    // $ANTLR start "rule__CollectionValue__Group__0"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3094:1: rule__CollectionValue__Group__0 : rule__CollectionValue__Group__0__Impl rule__CollectionValue__Group__1 ;
    public final void rule__CollectionValue__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3098:1: ( rule__CollectionValue__Group__0__Impl rule__CollectionValue__Group__1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3099:2: rule__CollectionValue__Group__0__Impl rule__CollectionValue__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__CollectionValue__Group__0__Impl_in_rule__CollectionValue__Group__06210);
            rule__CollectionValue__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__CollectionValue__Group__1_in_rule__CollectionValue__Group__06213);
            rule__CollectionValue__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionValue__Group__0"


    // $ANTLR start "rule__CollectionValue__Group__0__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3106:1: rule__CollectionValue__Group__0__Impl : ( '[' ) ;
    public final void rule__CollectionValue__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3110:1: ( ( '[' ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3111:1: ( '[' )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3111:1: ( '[' )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3112:1: '['
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionValueAccess().getLeftSquareBracketKeyword_0()); 
            }
            match(input,24,FollowSets000.FOLLOW_24_in_rule__CollectionValue__Group__0__Impl6241); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionValueAccess().getLeftSquareBracketKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionValue__Group__0__Impl"


    // $ANTLR start "rule__CollectionValue__Group__1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3125:1: rule__CollectionValue__Group__1 : rule__CollectionValue__Group__1__Impl rule__CollectionValue__Group__2 ;
    public final void rule__CollectionValue__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3129:1: ( rule__CollectionValue__Group__1__Impl rule__CollectionValue__Group__2 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3130:2: rule__CollectionValue__Group__1__Impl rule__CollectionValue__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__CollectionValue__Group__1__Impl_in_rule__CollectionValue__Group__16272);
            rule__CollectionValue__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__CollectionValue__Group__2_in_rule__CollectionValue__Group__16275);
            rule__CollectionValue__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionValue__Group__1"


    // $ANTLR start "rule__CollectionValue__Group__1__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3137:1: rule__CollectionValue__Group__1__Impl : ( ( rule__CollectionValue__ContentsAssignment_1 ) ) ;
    public final void rule__CollectionValue__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3141:1: ( ( ( rule__CollectionValue__ContentsAssignment_1 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3142:1: ( ( rule__CollectionValue__ContentsAssignment_1 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3142:1: ( ( rule__CollectionValue__ContentsAssignment_1 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3143:1: ( rule__CollectionValue__ContentsAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionValueAccess().getContentsAssignment_1()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3144:1: ( rule__CollectionValue__ContentsAssignment_1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3144:2: rule__CollectionValue__ContentsAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__CollectionValue__ContentsAssignment_1_in_rule__CollectionValue__Group__1__Impl6302);
            rule__CollectionValue__ContentsAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionValueAccess().getContentsAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionValue__Group__1__Impl"


    // $ANTLR start "rule__CollectionValue__Group__2"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3154:1: rule__CollectionValue__Group__2 : rule__CollectionValue__Group__2__Impl rule__CollectionValue__Group__3 ;
    public final void rule__CollectionValue__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3158:1: ( rule__CollectionValue__Group__2__Impl rule__CollectionValue__Group__3 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3159:2: rule__CollectionValue__Group__2__Impl rule__CollectionValue__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__CollectionValue__Group__2__Impl_in_rule__CollectionValue__Group__26332);
            rule__CollectionValue__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__CollectionValue__Group__3_in_rule__CollectionValue__Group__26335);
            rule__CollectionValue__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionValue__Group__2"


    // $ANTLR start "rule__CollectionValue__Group__2__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3166:1: rule__CollectionValue__Group__2__Impl : ( ( rule__CollectionValue__Group_2__0 )* ) ;
    public final void rule__CollectionValue__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3170:1: ( ( ( rule__CollectionValue__Group_2__0 )* ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3171:1: ( ( rule__CollectionValue__Group_2__0 )* )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3171:1: ( ( rule__CollectionValue__Group_2__0 )* )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3172:1: ( rule__CollectionValue__Group_2__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionValueAccess().getGroup_2()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3173:1: ( rule__CollectionValue__Group_2__0 )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==26) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3173:2: rule__CollectionValue__Group_2__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__CollectionValue__Group_2__0_in_rule__CollectionValue__Group__2__Impl6362);
            	    rule__CollectionValue__Group_2__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionValueAccess().getGroup_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionValue__Group__2__Impl"


    // $ANTLR start "rule__CollectionValue__Group__3"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3183:1: rule__CollectionValue__Group__3 : rule__CollectionValue__Group__3__Impl ;
    public final void rule__CollectionValue__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3187:1: ( rule__CollectionValue__Group__3__Impl )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3188:2: rule__CollectionValue__Group__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__CollectionValue__Group__3__Impl_in_rule__CollectionValue__Group__36393);
            rule__CollectionValue__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionValue__Group__3"


    // $ANTLR start "rule__CollectionValue__Group__3__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3194:1: rule__CollectionValue__Group__3__Impl : ( ']' ) ;
    public final void rule__CollectionValue__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3198:1: ( ( ']' ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3199:1: ( ']' )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3199:1: ( ']' )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3200:1: ']'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionValueAccess().getRightSquareBracketKeyword_3()); 
            }
            match(input,25,FollowSets000.FOLLOW_25_in_rule__CollectionValue__Group__3__Impl6421); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionValueAccess().getRightSquareBracketKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionValue__Group__3__Impl"


    // $ANTLR start "rule__CollectionValue__Group_2__0"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3221:1: rule__CollectionValue__Group_2__0 : rule__CollectionValue__Group_2__0__Impl rule__CollectionValue__Group_2__1 ;
    public final void rule__CollectionValue__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3225:1: ( rule__CollectionValue__Group_2__0__Impl rule__CollectionValue__Group_2__1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3226:2: rule__CollectionValue__Group_2__0__Impl rule__CollectionValue__Group_2__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__CollectionValue__Group_2__0__Impl_in_rule__CollectionValue__Group_2__06460);
            rule__CollectionValue__Group_2__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__CollectionValue__Group_2__1_in_rule__CollectionValue__Group_2__06463);
            rule__CollectionValue__Group_2__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionValue__Group_2__0"


    // $ANTLR start "rule__CollectionValue__Group_2__0__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3233:1: rule__CollectionValue__Group_2__0__Impl : ( ',' ) ;
    public final void rule__CollectionValue__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3237:1: ( ( ',' ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3238:1: ( ',' )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3238:1: ( ',' )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3239:1: ','
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionValueAccess().getCommaKeyword_2_0()); 
            }
            match(input,26,FollowSets000.FOLLOW_26_in_rule__CollectionValue__Group_2__0__Impl6491); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionValueAccess().getCommaKeyword_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionValue__Group_2__0__Impl"


    // $ANTLR start "rule__CollectionValue__Group_2__1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3252:1: rule__CollectionValue__Group_2__1 : rule__CollectionValue__Group_2__1__Impl ;
    public final void rule__CollectionValue__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3256:1: ( rule__CollectionValue__Group_2__1__Impl )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3257:2: rule__CollectionValue__Group_2__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__CollectionValue__Group_2__1__Impl_in_rule__CollectionValue__Group_2__16522);
            rule__CollectionValue__Group_2__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionValue__Group_2__1"


    // $ANTLR start "rule__CollectionValue__Group_2__1__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3263:1: rule__CollectionValue__Group_2__1__Impl : ( ( rule__CollectionValue__ContentsAssignment_2_1 ) ) ;
    public final void rule__CollectionValue__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3267:1: ( ( ( rule__CollectionValue__ContentsAssignment_2_1 ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3268:1: ( ( rule__CollectionValue__ContentsAssignment_2_1 ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3268:1: ( ( rule__CollectionValue__ContentsAssignment_2_1 ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3269:1: ( rule__CollectionValue__ContentsAssignment_2_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionValueAccess().getContentsAssignment_2_1()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3270:1: ( rule__CollectionValue__ContentsAssignment_2_1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3270:2: rule__CollectionValue__ContentsAssignment_2_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__CollectionValue__ContentsAssignment_2_1_in_rule__CollectionValue__Group_2__1__Impl6549);
            rule__CollectionValue__ContentsAssignment_2_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionValueAccess().getContentsAssignment_2_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionValue__Group_2__1__Impl"


    // $ANTLR start "rule__QualifiedName__Group__0"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3284:1: rule__QualifiedName__Group__0 : rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 ;
    public final void rule__QualifiedName__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3288:1: ( rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3289:2: rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__QualifiedName__Group__0__Impl_in_rule__QualifiedName__Group__06583);
            rule__QualifiedName__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__QualifiedName__Group__1_in_rule__QualifiedName__Group__06586);
            rule__QualifiedName__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0"


    // $ANTLR start "rule__QualifiedName__Group__0__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3296:1: rule__QualifiedName__Group__0__Impl : ( RULE_ID ) ;
    public final void rule__QualifiedName__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3300:1: ( ( RULE_ID ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3301:1: ( RULE_ID )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3301:1: ( RULE_ID )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3302:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rule__QualifiedName__Group__0__Impl6613); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0__Impl"


    // $ANTLR start "rule__QualifiedName__Group__1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3313:1: rule__QualifiedName__Group__1 : rule__QualifiedName__Group__1__Impl ;
    public final void rule__QualifiedName__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3317:1: ( rule__QualifiedName__Group__1__Impl )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3318:2: rule__QualifiedName__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__QualifiedName__Group__1__Impl_in_rule__QualifiedName__Group__16642);
            rule__QualifiedName__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1"


    // $ANTLR start "rule__QualifiedName__Group__1__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3324:1: rule__QualifiedName__Group__1__Impl : ( ( rule__QualifiedName__Group_1__0 )* ) ;
    public final void rule__QualifiedName__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3328:1: ( ( ( rule__QualifiedName__Group_1__0 )* ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3329:1: ( ( rule__QualifiedName__Group_1__0 )* )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3329:1: ( ( rule__QualifiedName__Group_1__0 )* )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3330:1: ( rule__QualifiedName__Group_1__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getQualifiedNameAccess().getGroup_1()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3331:1: ( rule__QualifiedName__Group_1__0 )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==19) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3331:2: rule__QualifiedName__Group_1__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__QualifiedName__Group_1__0_in_rule__QualifiedName__Group__1__Impl6669);
            	    rule__QualifiedName__Group_1__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getQualifiedNameAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1__Impl"


    // $ANTLR start "rule__QualifiedName__Group_1__0"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3345:1: rule__QualifiedName__Group_1__0 : rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1 ;
    public final void rule__QualifiedName__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3349:1: ( rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1 )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3350:2: rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__QualifiedName__Group_1__0__Impl_in_rule__QualifiedName__Group_1__06704);
            rule__QualifiedName__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__QualifiedName__Group_1__1_in_rule__QualifiedName__Group_1__06707);
            rule__QualifiedName__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__0"


    // $ANTLR start "rule__QualifiedName__Group_1__0__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3357:1: rule__QualifiedName__Group_1__0__Impl : ( '.' ) ;
    public final void rule__QualifiedName__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3361:1: ( ( '.' ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3362:1: ( '.' )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3362:1: ( '.' )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3363:1: '.'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 
            }
            match(input,19,FollowSets000.FOLLOW_19_in_rule__QualifiedName__Group_1__0__Impl6735); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__0__Impl"


    // $ANTLR start "rule__QualifiedName__Group_1__1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3376:1: rule__QualifiedName__Group_1__1 : rule__QualifiedName__Group_1__1__Impl ;
    public final void rule__QualifiedName__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3380:1: ( rule__QualifiedName__Group_1__1__Impl )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3381:2: rule__QualifiedName__Group_1__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__QualifiedName__Group_1__1__Impl_in_rule__QualifiedName__Group_1__16766);
            rule__QualifiedName__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__1"


    // $ANTLR start "rule__QualifiedName__Group_1__1__Impl"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3387:1: rule__QualifiedName__Group_1__1__Impl : ( RULE_ID ) ;
    public final void rule__QualifiedName__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3391:1: ( ( RULE_ID ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3392:1: ( RULE_ID )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3392:1: ( RULE_ID )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3393:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rule__QualifiedName__Group_1__1__Impl6793); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__1__Impl"


    // $ANTLR start "rule__Correspondences__ImportsAssignment_0"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3410:1: rule__Correspondences__ImportsAssignment_0 : ( ruleImport ) ;
    public final void rule__Correspondences__ImportsAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3414:1: ( ( ruleImport ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3415:1: ( ruleImport )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3415:1: ( ruleImport )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3416:1: ruleImport
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCorrespondencesAccess().getImportsImportParserRuleCall_0_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleImport_in_rule__Correspondences__ImportsAssignment_06832);
            ruleImport();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCorrespondencesAccess().getImportsImportParserRuleCall_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Correspondences__ImportsAssignment_0"


    // $ANTLR start "rule__Correspondences__CorrespondencesAssignment_1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3425:1: rule__Correspondences__CorrespondencesAssignment_1 : ( ruleModelCorrespondence ) ;
    public final void rule__Correspondences__CorrespondencesAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3429:1: ( ( ruleModelCorrespondence ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3430:1: ( ruleModelCorrespondence )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3430:1: ( ruleModelCorrespondence )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3431:1: ruleModelCorrespondence
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCorrespondencesAccess().getCorrespondencesModelCorrespondenceParserRuleCall_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleModelCorrespondence_in_rule__Correspondences__CorrespondencesAssignment_16863);
            ruleModelCorrespondence();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCorrespondencesAccess().getCorrespondencesModelCorrespondenceParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Correspondences__CorrespondencesAssignment_1"


    // $ANTLR start "rule__Import__ImportURIAssignment_1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3440:1: rule__Import__ImportURIAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Import__ImportURIAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3444:1: ( ( RULE_STRING ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3445:1: ( RULE_STRING )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3445:1: ( RULE_STRING )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3446:1: RULE_STRING
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); 
            }
            match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_rule__Import__ImportURIAssignment_16894); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__ImportURIAssignment_1"


    // $ANTLR start "rule__ModelCorrespondence__LeftAssignment_1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3455:1: rule__ModelCorrespondence__LeftAssignment_1 : ( ruleModelFunction ) ;
    public final void rule__ModelCorrespondence__LeftAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3459:1: ( ( ruleModelFunction ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3460:1: ( ruleModelFunction )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3460:1: ( ruleModelFunction )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3461:1: ruleModelFunction
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModelCorrespondenceAccess().getLeftModelFunctionParserRuleCall_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleModelFunction_in_rule__ModelCorrespondence__LeftAssignment_16925);
            ruleModelFunction();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getModelCorrespondenceAccess().getLeftModelFunctionParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModelCorrespondence__LeftAssignment_1"


    // $ANTLR start "rule__ModelCorrespondence__RightAssignment_3"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3470:1: rule__ModelCorrespondence__RightAssignment_3 : ( ruleModelFunction ) ;
    public final void rule__ModelCorrespondence__RightAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3474:1: ( ( ruleModelFunction ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3475:1: ( ruleModelFunction )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3475:1: ( ruleModelFunction )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3476:1: ruleModelFunction
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModelCorrespondenceAccess().getRightModelFunctionParserRuleCall_3_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleModelFunction_in_rule__ModelCorrespondence__RightAssignment_36956);
            ruleModelFunction();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getModelCorrespondenceAccess().getRightModelFunctionParserRuleCall_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModelCorrespondence__RightAssignment_3"


    // $ANTLR start "rule__ModelCorrespondence__ChildrenAssignment_5"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3485:1: rule__ModelCorrespondence__ChildrenAssignment_5 : ( ruleClassCorrespondence ) ;
    public final void rule__ModelCorrespondence__ChildrenAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3489:1: ( ( ruleClassCorrespondence ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3490:1: ( ruleClassCorrespondence )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3490:1: ( ruleClassCorrespondence )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3491:1: ruleClassCorrespondence
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModelCorrespondenceAccess().getChildrenClassCorrespondenceParserRuleCall_5_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleClassCorrespondence_in_rule__ModelCorrespondence__ChildrenAssignment_56987);
            ruleClassCorrespondence();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getModelCorrespondenceAccess().getChildrenClassCorrespondenceParserRuleCall_5_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModelCorrespondence__ChildrenAssignment_5"


    // $ANTLR start "rule__ModelFunction__TermsAssignment"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3500:1: rule__ModelFunction__TermsAssignment : ( rulePackageRef ) ;
    public final void rule__ModelFunction__TermsAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3504:1: ( ( rulePackageRef ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3505:1: ( rulePackageRef )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3505:1: ( rulePackageRef )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3506:1: rulePackageRef
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModelFunctionAccess().getTermsPackageRefParserRuleCall_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_rulePackageRef_in_rule__ModelFunction__TermsAssignment7018);
            rulePackageRef();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getModelFunctionAccess().getTermsPackageRefParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModelFunction__TermsAssignment"


    // $ANTLR start "rule__PackageRef__ReferencedPackageAssignment"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3515:1: rule__PackageRef__ReferencedPackageAssignment : ( ( RULE_ID ) ) ;
    public final void rule__PackageRef__ReferencedPackageAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3519:1: ( ( ( RULE_ID ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3520:1: ( ( RULE_ID ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3520:1: ( ( RULE_ID ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3521:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPackageRefAccess().getReferencedPackageEPackageCrossReference_0()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3522:1: ( RULE_ID )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3523:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPackageRefAccess().getReferencedPackageEPackageIDTerminalRuleCall_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rule__PackageRef__ReferencedPackageAssignment7053); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getPackageRefAccess().getReferencedPackageEPackageIDTerminalRuleCall_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getPackageRefAccess().getReferencedPackageEPackageCrossReference_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PackageRef__ReferencedPackageAssignment"


    // $ANTLR start "rule__ClassCorrespondence__LeftAssignment_0_0_1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3534:1: rule__ClassCorrespondence__LeftAssignment_0_0_1 : ( ruleClassFunction ) ;
    public final void rule__ClassCorrespondence__LeftAssignment_0_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3538:1: ( ( ruleClassFunction ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3539:1: ( ruleClassFunction )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3539:1: ( ruleClassFunction )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3540:1: ruleClassFunction
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassCorrespondenceAccess().getLeftClassFunctionParserRuleCall_0_0_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleClassFunction_in_rule__ClassCorrespondence__LeftAssignment_0_0_17088);
            ruleClassFunction();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassCorrespondenceAccess().getLeftClassFunctionParserRuleCall_0_0_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__LeftAssignment_0_0_1"


    // $ANTLR start "rule__ClassCorrespondence__RightAssignment_0_0_3"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3549:1: rule__ClassCorrespondence__RightAssignment_0_0_3 : ( ruleClassFunction ) ;
    public final void rule__ClassCorrespondence__RightAssignment_0_0_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3553:1: ( ( ruleClassFunction ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3554:1: ( ruleClassFunction )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3554:1: ( ruleClassFunction )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3555:1: ruleClassFunction
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassCorrespondenceAccess().getRightClassFunctionParserRuleCall_0_0_3_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleClassFunction_in_rule__ClassCorrespondence__RightAssignment_0_0_37119);
            ruleClassFunction();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassCorrespondenceAccess().getRightClassFunctionParserRuleCall_0_0_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__RightAssignment_0_0_3"


    // $ANTLR start "rule__ClassCorrespondence__LeftAssignment_0_1_1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3564:1: rule__ClassCorrespondence__LeftAssignment_0_1_1 : ( ruleClassFunction ) ;
    public final void rule__ClassCorrespondence__LeftAssignment_0_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3568:1: ( ( ruleClassFunction ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3569:1: ( ruleClassFunction )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3569:1: ( ruleClassFunction )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3570:1: ruleClassFunction
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassCorrespondenceAccess().getLeftClassFunctionParserRuleCall_0_1_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleClassFunction_in_rule__ClassCorrespondence__LeftAssignment_0_1_17150);
            ruleClassFunction();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassCorrespondenceAccess().getLeftClassFunctionParserRuleCall_0_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__LeftAssignment_0_1_1"


    // $ANTLR start "rule__ClassCorrespondence__RightAssignment_0_1_3"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3579:1: rule__ClassCorrespondence__RightAssignment_0_1_3 : ( ruleClassFunction ) ;
    public final void rule__ClassCorrespondence__RightAssignment_0_1_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3583:1: ( ( ruleClassFunction ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3584:1: ( ruleClassFunction )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3584:1: ( ruleClassFunction )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3585:1: ruleClassFunction
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassCorrespondenceAccess().getRightClassFunctionParserRuleCall_0_1_3_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleClassFunction_in_rule__ClassCorrespondence__RightAssignment_0_1_37181);
            ruleClassFunction();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassCorrespondenceAccess().getRightClassFunctionParserRuleCall_0_1_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__RightAssignment_0_1_3"


    // $ANTLR start "rule__ClassCorrespondence__LeftAssignment_0_2_1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3594:1: rule__ClassCorrespondence__LeftAssignment_0_2_1 : ( ruleClassFunction ) ;
    public final void rule__ClassCorrespondence__LeftAssignment_0_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3598:1: ( ( ruleClassFunction ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3599:1: ( ruleClassFunction )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3599:1: ( ruleClassFunction )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3600:1: ruleClassFunction
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassCorrespondenceAccess().getLeftClassFunctionParserRuleCall_0_2_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleClassFunction_in_rule__ClassCorrespondence__LeftAssignment_0_2_17212);
            ruleClassFunction();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassCorrespondenceAccess().getLeftClassFunctionParserRuleCall_0_2_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__LeftAssignment_0_2_1"


    // $ANTLR start "rule__ClassCorrespondence__RightAssignment_0_2_3"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3609:1: rule__ClassCorrespondence__RightAssignment_0_2_3 : ( ruleClassFunction ) ;
    public final void rule__ClassCorrespondence__RightAssignment_0_2_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3613:1: ( ( ruleClassFunction ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3614:1: ( ruleClassFunction )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3614:1: ( ruleClassFunction )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3615:1: ruleClassFunction
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassCorrespondenceAccess().getRightClassFunctionParserRuleCall_0_2_3_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleClassFunction_in_rule__ClassCorrespondence__RightAssignment_0_2_37243);
            ruleClassFunction();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassCorrespondenceAccess().getRightClassFunctionParserRuleCall_0_2_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__RightAssignment_0_2_3"


    // $ANTLR start "rule__ClassCorrespondence__LeftAssignment_0_3_1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3624:1: rule__ClassCorrespondence__LeftAssignment_0_3_1 : ( ruleClassFunction ) ;
    public final void rule__ClassCorrespondence__LeftAssignment_0_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3628:1: ( ( ruleClassFunction ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3629:1: ( ruleClassFunction )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3629:1: ( ruleClassFunction )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3630:1: ruleClassFunction
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassCorrespondenceAccess().getLeftClassFunctionParserRuleCall_0_3_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleClassFunction_in_rule__ClassCorrespondence__LeftAssignment_0_3_17274);
            ruleClassFunction();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassCorrespondenceAccess().getLeftClassFunctionParserRuleCall_0_3_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__LeftAssignment_0_3_1"


    // $ANTLR start "rule__ClassCorrespondence__RightAssignment_0_3_3"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3639:1: rule__ClassCorrespondence__RightAssignment_0_3_3 : ( ruleClassFunction ) ;
    public final void rule__ClassCorrespondence__RightAssignment_0_3_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3643:1: ( ( ruleClassFunction ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3644:1: ( ruleClassFunction )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3644:1: ( ruleClassFunction )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3645:1: ruleClassFunction
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassCorrespondenceAccess().getRightClassFunctionParserRuleCall_0_3_3_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleClassFunction_in_rule__ClassCorrespondence__RightAssignment_0_3_37305);
            ruleClassFunction();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassCorrespondenceAccess().getRightClassFunctionParserRuleCall_0_3_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__RightAssignment_0_3_3"


    // $ANTLR start "rule__ClassCorrespondence__ChildrenAssignment_2"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3654:1: rule__ClassCorrespondence__ChildrenAssignment_2 : ( ruleFeatureCorrespondence ) ;
    public final void rule__ClassCorrespondence__ChildrenAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3658:1: ( ( ruleFeatureCorrespondence ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3659:1: ( ruleFeatureCorrespondence )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3659:1: ( ruleFeatureCorrespondence )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3660:1: ruleFeatureCorrespondence
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassCorrespondenceAccess().getChildrenFeatureCorrespondenceParserRuleCall_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleFeatureCorrespondence_in_rule__ClassCorrespondence__ChildrenAssignment_27336);
            ruleFeatureCorrespondence();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassCorrespondenceAccess().getChildrenFeatureCorrespondenceParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCorrespondence__ChildrenAssignment_2"


    // $ANTLR start "rule__ClassFunction__TermsAssignment"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3669:1: rule__ClassFunction__TermsAssignment : ( ruleClassTerm ) ;
    public final void rule__ClassFunction__TermsAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3673:1: ( ( ruleClassTerm ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3674:1: ( ruleClassTerm )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3674:1: ( ruleClassTerm )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3675:1: ruleClassTerm
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassFunctionAccess().getTermsClassTermParserRuleCall_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleClassTerm_in_rule__ClassFunction__TermsAssignment7367);
            ruleClassTerm();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassFunctionAccess().getTermsClassTermParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassFunction__TermsAssignment"


    // $ANTLR start "rule__ClassRef__ReferencedClassAssignment"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3684:1: rule__ClassRef__ReferencedClassAssignment : ( ( RULE_ID ) ) ;
    public final void rule__ClassRef__ReferencedClassAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3688:1: ( ( ( RULE_ID ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3689:1: ( ( RULE_ID ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3689:1: ( ( RULE_ID ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3690:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassRefAccess().getReferencedClassEClassCrossReference_0()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3691:1: ( RULE_ID )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3692:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassRefAccess().getReferencedClassEClassIDTerminalRuleCall_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rule__ClassRef__ReferencedClassAssignment7402); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassRefAccess().getReferencedClassEClassIDTerminalRuleCall_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassRefAccess().getReferencedClassEClassCrossReference_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassRef__ReferencedClassAssignment"


    // $ANTLR start "rule__FeatureCorrespondence__LeftAssignment_0_1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3703:1: rule__FeatureCorrespondence__LeftAssignment_0_1 : ( ruleFeatureFunction ) ;
    public final void rule__FeatureCorrespondence__LeftAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3707:1: ( ( ruleFeatureFunction ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3708:1: ( ruleFeatureFunction )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3708:1: ( ruleFeatureFunction )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3709:1: ruleFeatureFunction
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureCorrespondenceAccess().getLeftFeatureFunctionParserRuleCall_0_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleFeatureFunction_in_rule__FeatureCorrespondence__LeftAssignment_0_17437);
            ruleFeatureFunction();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureCorrespondenceAccess().getLeftFeatureFunctionParserRuleCall_0_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__LeftAssignment_0_1"


    // $ANTLR start "rule__FeatureCorrespondence__RightAssignment_0_3"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3718:1: rule__FeatureCorrespondence__RightAssignment_0_3 : ( ruleFeatureFunction ) ;
    public final void rule__FeatureCorrespondence__RightAssignment_0_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3722:1: ( ( ruleFeatureFunction ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3723:1: ( ruleFeatureFunction )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3723:1: ( ruleFeatureFunction )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3724:1: ruleFeatureFunction
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureCorrespondenceAccess().getRightFeatureFunctionParserRuleCall_0_3_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleFeatureFunction_in_rule__FeatureCorrespondence__RightAssignment_0_37468);
            ruleFeatureFunction();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureCorrespondenceAccess().getRightFeatureFunctionParserRuleCall_0_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__RightAssignment_0_3"


    // $ANTLR start "rule__FeatureCorrespondence__LeftAssignment_1_1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3733:1: rule__FeatureCorrespondence__LeftAssignment_1_1 : ( ruleFeatureFunction ) ;
    public final void rule__FeatureCorrespondence__LeftAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3737:1: ( ( ruleFeatureFunction ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3738:1: ( ruleFeatureFunction )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3738:1: ( ruleFeatureFunction )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3739:1: ruleFeatureFunction
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureCorrespondenceAccess().getLeftFeatureFunctionParserRuleCall_1_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleFeatureFunction_in_rule__FeatureCorrespondence__LeftAssignment_1_17499);
            ruleFeatureFunction();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureCorrespondenceAccess().getLeftFeatureFunctionParserRuleCall_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__LeftAssignment_1_1"


    // $ANTLR start "rule__FeatureCorrespondence__RightAssignment_1_3"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3748:1: rule__FeatureCorrespondence__RightAssignment_1_3 : ( ruleFeatureFunction ) ;
    public final void rule__FeatureCorrespondence__RightAssignment_1_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3752:1: ( ( ruleFeatureFunction ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3753:1: ( ruleFeatureFunction )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3753:1: ( ruleFeatureFunction )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3754:1: ruleFeatureFunction
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureCorrespondenceAccess().getRightFeatureFunctionParserRuleCall_1_3_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleFeatureFunction_in_rule__FeatureCorrespondence__RightAssignment_1_37530);
            ruleFeatureFunction();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureCorrespondenceAccess().getRightFeatureFunctionParserRuleCall_1_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__RightAssignment_1_3"


    // $ANTLR start "rule__FeatureCorrespondence__LeftAssignment_2_1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3763:1: rule__FeatureCorrespondence__LeftAssignment_2_1 : ( ruleFeatureFunction ) ;
    public final void rule__FeatureCorrespondence__LeftAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3767:1: ( ( ruleFeatureFunction ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3768:1: ( ruleFeatureFunction )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3768:1: ( ruleFeatureFunction )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3769:1: ruleFeatureFunction
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureCorrespondenceAccess().getLeftFeatureFunctionParserRuleCall_2_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleFeatureFunction_in_rule__FeatureCorrespondence__LeftAssignment_2_17561);
            ruleFeatureFunction();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureCorrespondenceAccess().getLeftFeatureFunctionParserRuleCall_2_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__LeftAssignment_2_1"


    // $ANTLR start "rule__FeatureCorrespondence__RightAssignment_2_3"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3778:1: rule__FeatureCorrespondence__RightAssignment_2_3 : ( ruleFeatureFunction ) ;
    public final void rule__FeatureCorrespondence__RightAssignment_2_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3782:1: ( ( ruleFeatureFunction ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3783:1: ( ruleFeatureFunction )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3783:1: ( ruleFeatureFunction )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3784:1: ruleFeatureFunction
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureCorrespondenceAccess().getRightFeatureFunctionParserRuleCall_2_3_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleFeatureFunction_in_rule__FeatureCorrespondence__RightAssignment_2_37592);
            ruleFeatureFunction();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureCorrespondenceAccess().getRightFeatureFunctionParserRuleCall_2_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__RightAssignment_2_3"


    // $ANTLR start "rule__FeatureCorrespondence__LeftAssignment_3_1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3793:1: rule__FeatureCorrespondence__LeftAssignment_3_1 : ( ruleFeatureFunction ) ;
    public final void rule__FeatureCorrespondence__LeftAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3797:1: ( ( ruleFeatureFunction ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3798:1: ( ruleFeatureFunction )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3798:1: ( ruleFeatureFunction )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3799:1: ruleFeatureFunction
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureCorrespondenceAccess().getLeftFeatureFunctionParserRuleCall_3_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleFeatureFunction_in_rule__FeatureCorrespondence__LeftAssignment_3_17623);
            ruleFeatureFunction();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureCorrespondenceAccess().getLeftFeatureFunctionParserRuleCall_3_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__LeftAssignment_3_1"


    // $ANTLR start "rule__FeatureCorrespondence__RightAssignment_3_3"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3808:1: rule__FeatureCorrespondence__RightAssignment_3_3 : ( ruleFeatureFunction ) ;
    public final void rule__FeatureCorrespondence__RightAssignment_3_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3812:1: ( ( ruleFeatureFunction ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3813:1: ( ruleFeatureFunction )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3813:1: ( ruleFeatureFunction )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3814:1: ruleFeatureFunction
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureCorrespondenceAccess().getRightFeatureFunctionParserRuleCall_3_3_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleFeatureFunction_in_rule__FeatureCorrespondence__RightAssignment_3_37654);
            ruleFeatureFunction();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureCorrespondenceAccess().getRightFeatureFunctionParserRuleCall_3_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCorrespondence__RightAssignment_3_3"


    // $ANTLR start "rule__FeatureFunction__TermsAssignment_0"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3823:1: rule__FeatureFunction__TermsAssignment_0 : ( ruleFeatureTerm ) ;
    public final void rule__FeatureFunction__TermsAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3827:1: ( ( ruleFeatureTerm ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3828:1: ( ruleFeatureTerm )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3828:1: ( ruleFeatureTerm )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3829:1: ruleFeatureTerm
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureFunctionAccess().getTermsFeatureTermParserRuleCall_0_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleFeatureTerm_in_rule__FeatureFunction__TermsAssignment_07685);
            ruleFeatureTerm();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureFunctionAccess().getTermsFeatureTermParserRuleCall_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureFunction__TermsAssignment_0"


    // $ANTLR start "rule__FeatureFunction__TermsAssignment_1_1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3838:1: rule__FeatureFunction__TermsAssignment_1_1 : ( ruleFeatureTerm ) ;
    public final void rule__FeatureFunction__TermsAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3842:1: ( ( ruleFeatureTerm ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3843:1: ( ruleFeatureTerm )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3843:1: ( ruleFeatureTerm )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3844:1: ruleFeatureTerm
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureFunctionAccess().getTermsFeatureTermParserRuleCall_1_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleFeatureTerm_in_rule__FeatureFunction__TermsAssignment_1_17716);
            ruleFeatureTerm();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureFunctionAccess().getTermsFeatureTermParserRuleCall_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureFunction__TermsAssignment_1_1"


    // $ANTLR start "rule__FeatureRef__InstanceAssignment_0_0"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3853:1: rule__FeatureRef__InstanceAssignment_0_0 : ( ruleInstance ) ;
    public final void rule__FeatureRef__InstanceAssignment_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3857:1: ( ( ruleInstance ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3858:1: ( ruleInstance )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3858:1: ( ruleInstance )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3859:1: ruleInstance
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureRefAccess().getInstanceInstanceParserRuleCall_0_0_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleInstance_in_rule__FeatureRef__InstanceAssignment_0_07747);
            ruleInstance();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureRefAccess().getInstanceInstanceParserRuleCall_0_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureRef__InstanceAssignment_0_0"


    // $ANTLR start "rule__FeatureRef__ReferencedFeatureAssignment_1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3868:1: rule__FeatureRef__ReferencedFeatureAssignment_1 : ( ( RULE_ID ) ) ;
    public final void rule__FeatureRef__ReferencedFeatureAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3872:1: ( ( ( RULE_ID ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3873:1: ( ( RULE_ID ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3873:1: ( ( RULE_ID ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3874:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureRefAccess().getReferencedFeatureEStructuralFeatureCrossReference_1_0()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3875:1: ( RULE_ID )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3876:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureRefAccess().getReferencedFeatureEStructuralFeatureIDTerminalRuleCall_1_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rule__FeatureRef__ReferencedFeatureAssignment_17782); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureRefAccess().getReferencedFeatureEStructuralFeatureIDTerminalRuleCall_1_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureRefAccess().getReferencedFeatureEStructuralFeatureCrossReference_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureRef__ReferencedFeatureAssignment_1"


    // $ANTLR start "rule__Instance__TypeAssignment_1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3887:1: rule__Instance__TypeAssignment_1 : ( ( RULE_ID ) ) ;
    public final void rule__Instance__TypeAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3891:1: ( ( ( RULE_ID ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3892:1: ( ( RULE_ID ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3892:1: ( ( RULE_ID ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3893:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInstanceAccess().getTypeEClassCrossReference_1_0()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3894:1: ( RULE_ID )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3895:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInstanceAccess().getTypeEClassIDTerminalRuleCall_1_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rule__Instance__TypeAssignment_17821); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getInstanceAccess().getTypeEClassIDTerminalRuleCall_1_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getInstanceAccess().getTypeEClassCrossReference_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instance__TypeAssignment_1"


    // $ANTLR start "rule__Instance__ManyAssignment_2"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3906:1: rule__Instance__ManyAssignment_2 : ( ( '*' ) ) ;
    public final void rule__Instance__ManyAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3910:1: ( ( ( '*' ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3911:1: ( ( '*' ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3911:1: ( ( '*' ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3912:1: ( '*' )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInstanceAccess().getManyAsteriskKeyword_2_0()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3913:1: ( '*' )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3914:1: '*'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInstanceAccess().getManyAsteriskKeyword_2_0()); 
            }
            match(input,27,FollowSets000.FOLLOW_27_in_rule__Instance__ManyAssignment_27861); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getInstanceAccess().getManyAsteriskKeyword_2_0()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getInstanceAccess().getManyAsteriskKeyword_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instance__ManyAssignment_2"


    // $ANTLR start "rule__Instance__AssignmentsAssignment_4"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3929:1: rule__Instance__AssignmentsAssignment_4 : ( ruleAssignment ) ;
    public final void rule__Instance__AssignmentsAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3933:1: ( ( ruleAssignment ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3934:1: ( ruleAssignment )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3934:1: ( ruleAssignment )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3935:1: ruleAssignment
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInstanceAccess().getAssignmentsAssignmentParserRuleCall_4_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleAssignment_in_rule__Instance__AssignmentsAssignment_47900);
            ruleAssignment();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getInstanceAccess().getAssignmentsAssignmentParserRuleCall_4_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instance__AssignmentsAssignment_4"


    // $ANTLR start "rule__Assignment__FeatureAssignment_0"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3944:1: rule__Assignment__FeatureAssignment_0 : ( ( RULE_ID ) ) ;
    public final void rule__Assignment__FeatureAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3948:1: ( ( ( RULE_ID ) ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3949:1: ( ( RULE_ID ) )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3949:1: ( ( RULE_ID ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3950:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAssignmentAccess().getFeatureEStructuralFeatureCrossReference_0_0()); 
            }
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3951:1: ( RULE_ID )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3952:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAssignmentAccess().getFeatureEStructuralFeatureIDTerminalRuleCall_0_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rule__Assignment__FeatureAssignment_07935); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAssignmentAccess().getFeatureEStructuralFeatureIDTerminalRuleCall_0_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAssignmentAccess().getFeatureEStructuralFeatureCrossReference_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__FeatureAssignment_0"


    // $ANTLR start "rule__Assignment__ValueAssignment_2"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3963:1: rule__Assignment__ValueAssignment_2 : ( ruleTerm ) ;
    public final void rule__Assignment__ValueAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3967:1: ( ( ruleTerm ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3968:1: ( ruleTerm )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3968:1: ( ruleTerm )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3969:1: ruleTerm
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAssignmentAccess().getValueTermParserRuleCall_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleTerm_in_rule__Assignment__ValueAssignment_27970);
            ruleTerm();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAssignmentAccess().getValueTermParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__ValueAssignment_2"


    // $ANTLR start "rule__CollectionValue__ContentsAssignment_1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3978:1: rule__CollectionValue__ContentsAssignment_1 : ( ruleTerm ) ;
    public final void rule__CollectionValue__ContentsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3982:1: ( ( ruleTerm ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3983:1: ( ruleTerm )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3983:1: ( ruleTerm )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3984:1: ruleTerm
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionValueAccess().getContentsTermParserRuleCall_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleTerm_in_rule__CollectionValue__ContentsAssignment_18001);
            ruleTerm();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionValueAccess().getContentsTermParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionValue__ContentsAssignment_1"


    // $ANTLR start "rule__CollectionValue__ContentsAssignment_2_1"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3993:1: rule__CollectionValue__ContentsAssignment_2_1 : ( ruleTerm ) ;
    public final void rule__CollectionValue__ContentsAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3997:1: ( ( ruleTerm ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3998:1: ( ruleTerm )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3998:1: ( ruleTerm )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:3999:1: ruleTerm
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionValueAccess().getContentsTermParserRuleCall_2_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleTerm_in_rule__CollectionValue__ContentsAssignment_2_18032);
            ruleTerm();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionValueAccess().getContentsTermParserRuleCall_2_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionValue__ContentsAssignment_2_1"


    // $ANTLR start "rule__StringValue__ValueAssignment"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:4008:1: rule__StringValue__ValueAssignment : ( RULE_STRING ) ;
    public final void rule__StringValue__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:4012:1: ( ( RULE_STRING ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:4013:1: ( RULE_STRING )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:4013:1: ( RULE_STRING )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:4014:1: RULE_STRING
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStringValueAccess().getValueSTRINGTerminalRuleCall_0()); 
            }
            match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_rule__StringValue__ValueAssignment8063); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getStringValueAccess().getValueSTRINGTerminalRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringValue__ValueAssignment"


    // $ANTLR start "rule__IntValue__ValueAssignment"
    // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:4023:1: rule__IntValue__ValueAssignment : ( RULE_INT ) ;
    public final void rule__IntValue__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:4027:1: ( ( RULE_INT ) )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:4028:1: ( RULE_INT )
            {
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:4028:1: ( RULE_INT )
            // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:4029:1: RULE_INT
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIntValueAccess().getValueINTTerminalRuleCall_0()); 
            }
            match(input,RULE_INT,FollowSets000.FOLLOW_RULE_INT_in_rule__IntValue__ValueAssignment8094); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getIntValueAccess().getValueINTTerminalRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntValue__ValueAssignment"

    // $ANTLR start synpred1_InternalCorrespondence
    public final void synpred1_InternalCorrespondence_fragment() throws RecognitionException {   
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:684:1: ( ( ( rule__ClassCorrespondence__Group_0_0__0 ) ) )
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:684:1: ( ( rule__ClassCorrespondence__Group_0_0__0 ) )
        {
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:684:1: ( ( rule__ClassCorrespondence__Group_0_0__0 ) )
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:685:1: ( rule__ClassCorrespondence__Group_0_0__0 )
        {
        if ( state.backtracking==0 ) {
           before(grammarAccess.getClassCorrespondenceAccess().getGroup_0_0()); 
        }
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:686:1: ( rule__ClassCorrespondence__Group_0_0__0 )
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:686:2: rule__ClassCorrespondence__Group_0_0__0
        {
        pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_0__0_in_synpred1_InternalCorrespondence1398);
        rule__ClassCorrespondence__Group_0_0__0();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }
    }
    // $ANTLR end synpred1_InternalCorrespondence

    // $ANTLR start synpred2_InternalCorrespondence
    public final void synpred2_InternalCorrespondence_fragment() throws RecognitionException {   
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:690:6: ( ( ( rule__ClassCorrespondence__Group_0_1__0 ) ) )
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:690:6: ( ( rule__ClassCorrespondence__Group_0_1__0 ) )
        {
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:690:6: ( ( rule__ClassCorrespondence__Group_0_1__0 ) )
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:691:1: ( rule__ClassCorrespondence__Group_0_1__0 )
        {
        if ( state.backtracking==0 ) {
           before(grammarAccess.getClassCorrespondenceAccess().getGroup_0_1()); 
        }
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:692:1: ( rule__ClassCorrespondence__Group_0_1__0 )
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:692:2: rule__ClassCorrespondence__Group_0_1__0
        {
        pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_1__0_in_synpred2_InternalCorrespondence1416);
        rule__ClassCorrespondence__Group_0_1__0();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }
    }
    // $ANTLR end synpred2_InternalCorrespondence

    // $ANTLR start synpred3_InternalCorrespondence
    public final void synpred3_InternalCorrespondence_fragment() throws RecognitionException {   
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:696:6: ( ( ( rule__ClassCorrespondence__Group_0_2__0 ) ) )
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:696:6: ( ( rule__ClassCorrespondence__Group_0_2__0 ) )
        {
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:696:6: ( ( rule__ClassCorrespondence__Group_0_2__0 ) )
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:697:1: ( rule__ClassCorrespondence__Group_0_2__0 )
        {
        if ( state.backtracking==0 ) {
           before(grammarAccess.getClassCorrespondenceAccess().getGroup_0_2()); 
        }
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:698:1: ( rule__ClassCorrespondence__Group_0_2__0 )
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:698:2: rule__ClassCorrespondence__Group_0_2__0
        {
        pushFollow(FollowSets000.FOLLOW_rule__ClassCorrespondence__Group_0_2__0_in_synpred3_InternalCorrespondence1434);
        rule__ClassCorrespondence__Group_0_2__0();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }
    }
    // $ANTLR end synpred3_InternalCorrespondence

    // $ANTLR start synpred5_InternalCorrespondence
    public final void synpred5_InternalCorrespondence_fragment() throws RecognitionException {   
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:740:1: ( ( ( rule__FeatureCorrespondence__Group_0__0 ) ) )
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:740:1: ( ( rule__FeatureCorrespondence__Group_0__0 ) )
        {
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:740:1: ( ( rule__FeatureCorrespondence__Group_0__0 ) )
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:741:1: ( rule__FeatureCorrespondence__Group_0__0 )
        {
        if ( state.backtracking==0 ) {
           before(grammarAccess.getFeatureCorrespondenceAccess().getGroup_0()); 
        }
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:742:1: ( rule__FeatureCorrespondence__Group_0__0 )
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:742:2: rule__FeatureCorrespondence__Group_0__0
        {
        pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_0__0_in_synpred5_InternalCorrespondence1534);
        rule__FeatureCorrespondence__Group_0__0();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }
    }
    // $ANTLR end synpred5_InternalCorrespondence

    // $ANTLR start synpred6_InternalCorrespondence
    public final void synpred6_InternalCorrespondence_fragment() throws RecognitionException {   
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:746:6: ( ( ( rule__FeatureCorrespondence__Group_1__0 ) ) )
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:746:6: ( ( rule__FeatureCorrespondence__Group_1__0 ) )
        {
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:746:6: ( ( rule__FeatureCorrespondence__Group_1__0 ) )
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:747:1: ( rule__FeatureCorrespondence__Group_1__0 )
        {
        if ( state.backtracking==0 ) {
           before(grammarAccess.getFeatureCorrespondenceAccess().getGroup_1()); 
        }
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:748:1: ( rule__FeatureCorrespondence__Group_1__0 )
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:748:2: rule__FeatureCorrespondence__Group_1__0
        {
        pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_1__0_in_synpred6_InternalCorrespondence1552);
        rule__FeatureCorrespondence__Group_1__0();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }
    }
    // $ANTLR end synpred6_InternalCorrespondence

    // $ANTLR start synpred7_InternalCorrespondence
    public final void synpred7_InternalCorrespondence_fragment() throws RecognitionException {   
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:752:6: ( ( ( rule__FeatureCorrespondence__Group_2__0 ) ) )
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:752:6: ( ( rule__FeatureCorrespondence__Group_2__0 ) )
        {
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:752:6: ( ( rule__FeatureCorrespondence__Group_2__0 ) )
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:753:1: ( rule__FeatureCorrespondence__Group_2__0 )
        {
        if ( state.backtracking==0 ) {
           before(grammarAccess.getFeatureCorrespondenceAccess().getGroup_2()); 
        }
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:754:1: ( rule__FeatureCorrespondence__Group_2__0 )
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:754:2: rule__FeatureCorrespondence__Group_2__0
        {
        pushFollow(FollowSets000.FOLLOW_rule__FeatureCorrespondence__Group_2__0_in_synpred7_InternalCorrespondence1570);
        rule__FeatureCorrespondence__Group_2__0();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }
    }
    // $ANTLR end synpred7_InternalCorrespondence

    // $ANTLR start synpred8_InternalCorrespondence
    public final void synpred8_InternalCorrespondence_fragment() throws RecognitionException {   
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:774:1: ( ( ruleFeatureRef ) )
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:774:1: ( ruleFeatureRef )
        {
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:774:1: ( ruleFeatureRef )
        // ../correspondence.xtext.ui/src-gen/correspondence/xtext/ui/contentassist/antlr/internal/InternalCorrespondence.g:775:1: ruleFeatureRef
        {
        if ( state.backtracking==0 ) {
           before(grammarAccess.getFeatureTermAccess().getFeatureRefParserRuleCall_0()); 
        }
        pushFollow(FollowSets000.FOLLOW_ruleFeatureRef_in_synpred8_InternalCorrespondence1621);
        ruleFeatureRef();

        state._fsp--;
        if (state.failed) return ;

        }


        }
    }
    // $ANTLR end synpred8_InternalCorrespondence

    // Delegated rules

    public final boolean synpred5_InternalCorrespondence() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred5_InternalCorrespondence_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred2_InternalCorrespondence() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred2_InternalCorrespondence_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred8_InternalCorrespondence() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred8_InternalCorrespondence_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred3_InternalCorrespondence() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred3_InternalCorrespondence_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred7_InternalCorrespondence() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred7_InternalCorrespondence_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred1_InternalCorrespondence() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred1_InternalCorrespondence_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred6_InternalCorrespondence() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred6_InternalCorrespondence_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


    protected DFA3 dfa3 = new DFA3(this);
    static final String DFA3_eotS =
        "\13\uffff";
    static final String DFA3_eofS =
        "\13\uffff";
    static final String DFA3_minS =
        "\1\4\6\0\4\uffff";
    static final String DFA3_maxS =
        "\1\30\6\0\4\uffff";
    static final String DFA3_acceptS =
        "\7\uffff\1\1\1\2\1\3\1\4";
    static final String DFA3_specialS =
        "\1\uffff\1\0\1\1\1\2\1\3\1\4\1\5\4\uffff}>";
    static final String[] DFA3_transitionS = {
            "\1\3\1\4\1\5\15\uffff\1\1\1\2\2\uffff\1\6",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA3_eot = DFA.unpackEncodedString(DFA3_eotS);
    static final short[] DFA3_eof = DFA.unpackEncodedString(DFA3_eofS);
    static final char[] DFA3_min = DFA.unpackEncodedStringToUnsignedChars(DFA3_minS);
    static final char[] DFA3_max = DFA.unpackEncodedStringToUnsignedChars(DFA3_maxS);
    static final short[] DFA3_accept = DFA.unpackEncodedString(DFA3_acceptS);
    static final short[] DFA3_special = DFA.unpackEncodedString(DFA3_specialS);
    static final short[][] DFA3_transition;

    static {
        int numStates = DFA3_transitionS.length;
        DFA3_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA3_transition[i] = DFA.unpackEncodedString(DFA3_transitionS[i]);
        }
    }

    class DFA3 extends DFA {

        public DFA3(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 3;
            this.eot = DFA3_eot;
            this.eof = DFA3_eof;
            this.min = DFA3_min;
            this.max = DFA3_max;
            this.accept = DFA3_accept;
            this.special = DFA3_special;
            this.transition = DFA3_transition;
        }
        public String getDescription() {
            return "735:1: rule__FeatureCorrespondence__Alternatives : ( ( ( rule__FeatureCorrespondence__Group_0__0 ) ) | ( ( rule__FeatureCorrespondence__Group_1__0 ) ) | ( ( rule__FeatureCorrespondence__Group_2__0 ) ) | ( ( rule__FeatureCorrespondence__Group_3__0 ) ) );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA3_1 = input.LA(1);

                         
                        int index3_1 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred5_InternalCorrespondence()) ) {s = 7;}

                        else if ( (synpred6_InternalCorrespondence()) ) {s = 8;}

                        else if ( (synpred7_InternalCorrespondence()) ) {s = 9;}

                        else if ( (true) ) {s = 10;}

                         
                        input.seek(index3_1);
                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA3_2 = input.LA(1);

                         
                        int index3_2 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred5_InternalCorrespondence()) ) {s = 7;}

                        else if ( (synpred6_InternalCorrespondence()) ) {s = 8;}

                        else if ( (synpred7_InternalCorrespondence()) ) {s = 9;}

                        else if ( (true) ) {s = 10;}

                         
                        input.seek(index3_2);
                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA3_3 = input.LA(1);

                         
                        int index3_3 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred5_InternalCorrespondence()) ) {s = 7;}

                        else if ( (synpred6_InternalCorrespondence()) ) {s = 8;}

                        else if ( (synpred7_InternalCorrespondence()) ) {s = 9;}

                        else if ( (true) ) {s = 10;}

                         
                        input.seek(index3_3);
                        if ( s>=0 ) return s;
                        break;
                    case 3 : 
                        int LA3_4 = input.LA(1);

                         
                        int index3_4 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred5_InternalCorrespondence()) ) {s = 7;}

                        else if ( (synpred6_InternalCorrespondence()) ) {s = 8;}

                        else if ( (synpred7_InternalCorrespondence()) ) {s = 9;}

                        else if ( (true) ) {s = 10;}

                         
                        input.seek(index3_4);
                        if ( s>=0 ) return s;
                        break;
                    case 4 : 
                        int LA3_5 = input.LA(1);

                         
                        int index3_5 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred5_InternalCorrespondence()) ) {s = 7;}

                        else if ( (synpred6_InternalCorrespondence()) ) {s = 8;}

                        else if ( (synpred7_InternalCorrespondence()) ) {s = 9;}

                        else if ( (true) ) {s = 10;}

                         
                        input.seek(index3_5);
                        if ( s>=0 ) return s;
                        break;
                    case 5 : 
                        int LA3_6 = input.LA(1);

                         
                        int index3_6 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred5_InternalCorrespondence()) ) {s = 7;}

                        else if ( (synpred6_InternalCorrespondence()) ) {s = 8;}

                        else if ( (synpred7_InternalCorrespondence()) ) {s = 9;}

                        else if ( (true) ) {s = 10;}

                         
                        input.seek(index3_6);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 3, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_ruleCorrespondences_in_entryRuleCorrespondences67 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleCorrespondences74 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Correspondences__Group__0_in_ruleCorrespondences100 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleImport_in_entryRuleImport127 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleImport134 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Import__Group__0_in_ruleImport160 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleModelCorrespondence_in_entryRuleModelCorrespondence187 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleModelCorrespondence194 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ModelCorrespondence__Group__0_in_ruleModelCorrespondence220 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleModelFunction_in_entryRuleModelFunction247 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleModelFunction254 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ModelFunction__TermsAssignment_in_ruleModelFunction280 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rulePackageRef_in_entryRulePackageRef307 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRulePackageRef314 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PackageRef__ReferencedPackageAssignment_in_rulePackageRef340 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClassCorrespondence_in_entryRuleClassCorrespondence367 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleClassCorrespondence374 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group__0_in_ruleClassCorrespondence400 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClassFunction_in_entryRuleClassFunction427 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleClassFunction434 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassFunction__TermsAssignment_in_ruleClassFunction460 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClassTerm_in_entryRuleClassTerm487 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleClassTerm494 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassTerm__Alternatives_in_ruleClassTerm520 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClassRef_in_entryRuleClassRef547 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleClassRef554 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassRef__ReferencedClassAssignment_in_ruleClassRef580 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureCorrespondence_in_entryRuleFeatureCorrespondence607 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFeatureCorrespondence614 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Alternatives_in_ruleFeatureCorrespondence640 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureFunction_in_entryRuleFeatureFunction667 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFeatureFunction674 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureFunction__Group__0_in_ruleFeatureFunction700 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureTerm_in_entryRuleFeatureTerm727 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFeatureTerm734 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureTerm__Alternatives_in_ruleFeatureTerm760 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureRef_in_entryRuleFeatureRef787 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFeatureRef794 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureRef__Group__0_in_ruleFeatureRef820 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleInstance_in_entryRuleInstance847 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleInstance854 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Instance__Group__0_in_ruleInstance880 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAssignment_in_entryRuleAssignment907 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleAssignment914 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Assignment__Group__0_in_ruleAssignment940 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleTerm_in_entryRuleTerm967 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleTerm974 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Term__Alternatives_in_ruleTerm1000 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleThisRef_in_entryRuleThisRef1027 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleThisRef1034 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ThisRef__Group__0_in_ruleThisRef1060 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleValue_in_entryRuleValue1087 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleValue1094 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Value__Alternatives_in_ruleValue1120 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleCollectionValue_in_entryRuleCollectionValue1147 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleCollectionValue1154 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__CollectionValue__Group__0_in_ruleCollectionValue1180 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleStringValue_in_entryRuleStringValue1207 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleStringValue1214 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__StringValue__ValueAssignment_in_ruleStringValue1240 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleIntValue_in_entryRuleIntValue1267 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleIntValue1274 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__IntValue__ValueAssignment_in_ruleIntValue1300 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleQualifiedName_in_entryRuleQualifiedName1327 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleQualifiedName1334 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__QualifiedName__Group__0_in_ruleQualifiedName1360 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_0__0_in_rule__ClassCorrespondence__Alternatives_01398 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_1__0_in_rule__ClassCorrespondence__Alternatives_01416 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_2__0_in_rule__ClassCorrespondence__Alternatives_01434 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_3__0_in_rule__ClassCorrespondence__Alternatives_01452 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClassRef_in_rule__ClassTerm__Alternatives1485 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleInstance_in_rule__ClassTerm__Alternatives1502 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_0__0_in_rule__FeatureCorrespondence__Alternatives1534 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_1__0_in_rule__FeatureCorrespondence__Alternatives1552 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_2__0_in_rule__FeatureCorrespondence__Alternatives1570 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_3__0_in_rule__FeatureCorrespondence__Alternatives1588 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureRef_in_rule__FeatureTerm__Alternatives1621 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleValue_in_rule__FeatureTerm__Alternatives1638 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleInstance_in_rule__FeatureTerm__Alternatives1655 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Instance__Group_0_0__0_in_rule__Instance__Alternatives_01687 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Instance__Group_0_1__0_in_rule__Instance__Alternatives_01705 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleValue_in_rule__Term__Alternatives1738 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleInstance_in_rule__Term__Alternatives1756 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleThisRef_in_rule__Term__Alternatives1774 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleStringValue_in_rule__Value__Alternatives1806 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleIntValue_in_rule__Value__Alternatives1823 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleCollectionValue_in_rule__Value__Alternatives1840 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Correspondences__Group__0__Impl_in_rule__Correspondences__Group__01870 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_rule__Correspondences__Group__1_in_rule__Correspondences__Group__01873 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Correspondences__ImportsAssignment_0_in_rule__Correspondences__Group__0__Impl1900 = new BitSet(new long[]{0x0000000000000802L});
        public static final BitSet FOLLOW_rule__Correspondences__Group__1__Impl_in_rule__Correspondences__Group__11931 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Correspondences__CorrespondencesAssignment_1_in_rule__Correspondences__Group__1__Impl1958 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Import__Group__0__Impl_in_rule__Import__Group__01992 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_rule__Import__Group__1_in_rule__Import__Group__01995 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_11_in_rule__Import__Group__0__Impl2023 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Import__Group__1__Impl_in_rule__Import__Group__12054 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Import__ImportURIAssignment_1_in_rule__Import__Group__1__Impl2081 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ModelCorrespondence__Group__0__Impl_in_rule__ModelCorrespondence__Group__02115 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_rule__ModelCorrespondence__Group__1_in_rule__ModelCorrespondence__Group__02118 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ModelCorrespondence__Group__1__Impl_in_rule__ModelCorrespondence__Group__12176 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_rule__ModelCorrespondence__Group__2_in_rule__ModelCorrespondence__Group__12179 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ModelCorrespondence__LeftAssignment_1_in_rule__ModelCorrespondence__Group__1__Impl2206 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ModelCorrespondence__Group__2__Impl_in_rule__ModelCorrespondence__Group__22236 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_rule__ModelCorrespondence__Group__3_in_rule__ModelCorrespondence__Group__22239 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_12_in_rule__ModelCorrespondence__Group__2__Impl2267 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ModelCorrespondence__Group__3__Impl_in_rule__ModelCorrespondence__Group__32298 = new BitSet(new long[]{0x0000000000002000L});
        public static final BitSet FOLLOW_rule__ModelCorrespondence__Group__4_in_rule__ModelCorrespondence__Group__32301 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ModelCorrespondence__RightAssignment_3_in_rule__ModelCorrespondence__Group__3__Impl2328 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ModelCorrespondence__Group__4__Impl_in_rule__ModelCorrespondence__Group__42358 = new BitSet(new long[]{0x0000000000304010L});
        public static final BitSet FOLLOW_rule__ModelCorrespondence__Group__5_in_rule__ModelCorrespondence__Group__42361 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_13_in_rule__ModelCorrespondence__Group__4__Impl2389 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ModelCorrespondence__Group__5__Impl_in_rule__ModelCorrespondence__Group__52420 = new BitSet(new long[]{0x0000000000304010L});
        public static final BitSet FOLLOW_rule__ModelCorrespondence__Group__6_in_rule__ModelCorrespondence__Group__52423 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ModelCorrespondence__ChildrenAssignment_5_in_rule__ModelCorrespondence__Group__5__Impl2450 = new BitSet(new long[]{0x0000000000300012L});
        public static final BitSet FOLLOW_rule__ModelCorrespondence__Group__6__Impl_in_rule__ModelCorrespondence__Group__62481 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_14_in_rule__ModelCorrespondence__Group__6__Impl2509 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group__0__Impl_in_rule__ClassCorrespondence__Group__02554 = new BitSet(new long[]{0x0000000000002000L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group__1_in_rule__ClassCorrespondence__Group__02557 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Alternatives_0_in_rule__ClassCorrespondence__Group__0__Impl2584 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group__1__Impl_in_rule__ClassCorrespondence__Group__12614 = new BitSet(new long[]{0x0000000001304070L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group__2_in_rule__ClassCorrespondence__Group__12617 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_13_in_rule__ClassCorrespondence__Group__1__Impl2645 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group__2__Impl_in_rule__ClassCorrespondence__Group__22676 = new BitSet(new long[]{0x0000000001304070L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group__3_in_rule__ClassCorrespondence__Group__22679 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__ChildrenAssignment_2_in_rule__ClassCorrespondence__Group__2__Impl2706 = new BitSet(new long[]{0x0000000001300072L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group__3__Impl_in_rule__ClassCorrespondence__Group__32737 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_14_in_rule__ClassCorrespondence__Group__3__Impl2765 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_0__0__Impl_in_rule__ClassCorrespondence__Group_0_0__02804 = new BitSet(new long[]{0x0000000000300010L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_0__1_in_rule__ClassCorrespondence__Group_0_0__02807 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_0__1__Impl_in_rule__ClassCorrespondence__Group_0_0__12865 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_0__2_in_rule__ClassCorrespondence__Group_0_0__12868 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__LeftAssignment_0_0_1_in_rule__ClassCorrespondence__Group_0_0__1__Impl2895 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_0__2__Impl_in_rule__ClassCorrespondence__Group_0_0__22925 = new BitSet(new long[]{0x0000000000300010L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_0__3_in_rule__ClassCorrespondence__Group_0_0__22928 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_12_in_rule__ClassCorrespondence__Group_0_0__2__Impl2956 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_0__3__Impl_in_rule__ClassCorrespondence__Group_0_0__32987 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__RightAssignment_0_0_3_in_rule__ClassCorrespondence__Group_0_0__3__Impl3014 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_1__0__Impl_in_rule__ClassCorrespondence__Group_0_1__03052 = new BitSet(new long[]{0x0000000000300010L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_1__1_in_rule__ClassCorrespondence__Group_0_1__03055 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_1__1__Impl_in_rule__ClassCorrespondence__Group_0_1__13113 = new BitSet(new long[]{0x0000000000008000L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_1__2_in_rule__ClassCorrespondence__Group_0_1__13116 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__LeftAssignment_0_1_1_in_rule__ClassCorrespondence__Group_0_1__1__Impl3143 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_1__2__Impl_in_rule__ClassCorrespondence__Group_0_1__23173 = new BitSet(new long[]{0x0000000000300010L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_1__3_in_rule__ClassCorrespondence__Group_0_1__23176 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_15_in_rule__ClassCorrespondence__Group_0_1__2__Impl3204 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_1__3__Impl_in_rule__ClassCorrespondence__Group_0_1__33235 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__RightAssignment_0_1_3_in_rule__ClassCorrespondence__Group_0_1__3__Impl3262 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_2__0__Impl_in_rule__ClassCorrespondence__Group_0_2__03300 = new BitSet(new long[]{0x0000000000300010L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_2__1_in_rule__ClassCorrespondence__Group_0_2__03303 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_2__1__Impl_in_rule__ClassCorrespondence__Group_0_2__13361 = new BitSet(new long[]{0x0000000000010000L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_2__2_in_rule__ClassCorrespondence__Group_0_2__13364 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__LeftAssignment_0_2_1_in_rule__ClassCorrespondence__Group_0_2__1__Impl3391 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_2__2__Impl_in_rule__ClassCorrespondence__Group_0_2__23421 = new BitSet(new long[]{0x0000000000300010L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_2__3_in_rule__ClassCorrespondence__Group_0_2__23424 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_16_in_rule__ClassCorrespondence__Group_0_2__2__Impl3452 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_2__3__Impl_in_rule__ClassCorrespondence__Group_0_2__33483 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__RightAssignment_0_2_3_in_rule__ClassCorrespondence__Group_0_2__3__Impl3510 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_3__0__Impl_in_rule__ClassCorrespondence__Group_0_3__03548 = new BitSet(new long[]{0x0000000000300010L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_3__1_in_rule__ClassCorrespondence__Group_0_3__03551 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_3__1__Impl_in_rule__ClassCorrespondence__Group_0_3__13609 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_3__2_in_rule__ClassCorrespondence__Group_0_3__13612 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__LeftAssignment_0_3_1_in_rule__ClassCorrespondence__Group_0_3__1__Impl3639 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_3__2__Impl_in_rule__ClassCorrespondence__Group_0_3__23669 = new BitSet(new long[]{0x0000000000300010L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_3__3_in_rule__ClassCorrespondence__Group_0_3__23672 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_17_in_rule__ClassCorrespondence__Group_0_3__2__Impl3700 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_3__3__Impl_in_rule__ClassCorrespondence__Group_0_3__33731 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__RightAssignment_0_3_3_in_rule__ClassCorrespondence__Group_0_3__3__Impl3758 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_0__0__Impl_in_rule__FeatureCorrespondence__Group_0__03796 = new BitSet(new long[]{0x0000000001300070L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_0__1_in_rule__FeatureCorrespondence__Group_0__03799 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_0__1__Impl_in_rule__FeatureCorrespondence__Group_0__13857 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_0__2_in_rule__FeatureCorrespondence__Group_0__13860 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__LeftAssignment_0_1_in_rule__FeatureCorrespondence__Group_0__1__Impl3887 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_0__2__Impl_in_rule__FeatureCorrespondence__Group_0__23917 = new BitSet(new long[]{0x0000000001300070L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_0__3_in_rule__FeatureCorrespondence__Group_0__23920 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_12_in_rule__FeatureCorrespondence__Group_0__2__Impl3948 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_0__3__Impl_in_rule__FeatureCorrespondence__Group_0__33979 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__RightAssignment_0_3_in_rule__FeatureCorrespondence__Group_0__3__Impl4006 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_1__0__Impl_in_rule__FeatureCorrespondence__Group_1__04044 = new BitSet(new long[]{0x0000000001300070L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_1__1_in_rule__FeatureCorrespondence__Group_1__04047 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_1__1__Impl_in_rule__FeatureCorrespondence__Group_1__14105 = new BitSet(new long[]{0x0000000000008000L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_1__2_in_rule__FeatureCorrespondence__Group_1__14108 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__LeftAssignment_1_1_in_rule__FeatureCorrespondence__Group_1__1__Impl4135 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_1__2__Impl_in_rule__FeatureCorrespondence__Group_1__24165 = new BitSet(new long[]{0x0000000001300070L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_1__3_in_rule__FeatureCorrespondence__Group_1__24168 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_15_in_rule__FeatureCorrespondence__Group_1__2__Impl4196 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_1__3__Impl_in_rule__FeatureCorrespondence__Group_1__34227 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__RightAssignment_1_3_in_rule__FeatureCorrespondence__Group_1__3__Impl4254 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_2__0__Impl_in_rule__FeatureCorrespondence__Group_2__04292 = new BitSet(new long[]{0x0000000001300070L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_2__1_in_rule__FeatureCorrespondence__Group_2__04295 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_2__1__Impl_in_rule__FeatureCorrespondence__Group_2__14353 = new BitSet(new long[]{0x0000000000010000L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_2__2_in_rule__FeatureCorrespondence__Group_2__14356 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__LeftAssignment_2_1_in_rule__FeatureCorrespondence__Group_2__1__Impl4383 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_2__2__Impl_in_rule__FeatureCorrespondence__Group_2__24413 = new BitSet(new long[]{0x0000000001300070L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_2__3_in_rule__FeatureCorrespondence__Group_2__24416 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_16_in_rule__FeatureCorrespondence__Group_2__2__Impl4444 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_2__3__Impl_in_rule__FeatureCorrespondence__Group_2__34475 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__RightAssignment_2_3_in_rule__FeatureCorrespondence__Group_2__3__Impl4502 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_3__0__Impl_in_rule__FeatureCorrespondence__Group_3__04540 = new BitSet(new long[]{0x0000000001300070L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_3__1_in_rule__FeatureCorrespondence__Group_3__04543 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_3__1__Impl_in_rule__FeatureCorrespondence__Group_3__14601 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_3__2_in_rule__FeatureCorrespondence__Group_3__14604 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__LeftAssignment_3_1_in_rule__FeatureCorrespondence__Group_3__1__Impl4631 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_3__2__Impl_in_rule__FeatureCorrespondence__Group_3__24661 = new BitSet(new long[]{0x0000000001300070L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_3__3_in_rule__FeatureCorrespondence__Group_3__24664 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_17_in_rule__FeatureCorrespondence__Group_3__2__Impl4692 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_3__3__Impl_in_rule__FeatureCorrespondence__Group_3__34723 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__RightAssignment_3_3_in_rule__FeatureCorrespondence__Group_3__3__Impl4750 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureFunction__Group__0__Impl_in_rule__FeatureFunction__Group__04788 = new BitSet(new long[]{0x0000000000040000L});
        public static final BitSet FOLLOW_rule__FeatureFunction__Group__1_in_rule__FeatureFunction__Group__04791 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureFunction__TermsAssignment_0_in_rule__FeatureFunction__Group__0__Impl4818 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureFunction__Group__1__Impl_in_rule__FeatureFunction__Group__14848 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureFunction__Group_1__0_in_rule__FeatureFunction__Group__1__Impl4875 = new BitSet(new long[]{0x0000000000040002L});
        public static final BitSet FOLLOW_rule__FeatureFunction__Group_1__0__Impl_in_rule__FeatureFunction__Group_1__04910 = new BitSet(new long[]{0x0000000001300070L});
        public static final BitSet FOLLOW_rule__FeatureFunction__Group_1__1_in_rule__FeatureFunction__Group_1__04913 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_18_in_rule__FeatureFunction__Group_1__0__Impl4941 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureFunction__Group_1__1__Impl_in_rule__FeatureFunction__Group_1__14972 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureFunction__TermsAssignment_1_1_in_rule__FeatureFunction__Group_1__1__Impl4999 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureRef__Group__0__Impl_in_rule__FeatureRef__Group__05033 = new BitSet(new long[]{0x0000000000300010L});
        public static final BitSet FOLLOW_rule__FeatureRef__Group__1_in_rule__FeatureRef__Group__05036 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureRef__Group_0__0_in_rule__FeatureRef__Group__0__Impl5063 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureRef__Group__1__Impl_in_rule__FeatureRef__Group__15094 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureRef__ReferencedFeatureAssignment_1_in_rule__FeatureRef__Group__1__Impl5121 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureRef__Group_0__0__Impl_in_rule__FeatureRef__Group_0__05155 = new BitSet(new long[]{0x0000000000080000L});
        public static final BitSet FOLLOW_rule__FeatureRef__Group_0__1_in_rule__FeatureRef__Group_0__05158 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureRef__InstanceAssignment_0_0_in_rule__FeatureRef__Group_0__0__Impl5185 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureRef__Group_0__1__Impl_in_rule__FeatureRef__Group_0__15215 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_19_in_rule__FeatureRef__Group_0__1__Impl5243 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Instance__Group__0__Impl_in_rule__Instance__Group__05278 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_rule__Instance__Group__1_in_rule__Instance__Group__05281 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Instance__Alternatives_0_in_rule__Instance__Group__0__Impl5308 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Instance__Group__1__Impl_in_rule__Instance__Group__15338 = new BitSet(new long[]{0x0000000008002000L});
        public static final BitSet FOLLOW_rule__Instance__Group__2_in_rule__Instance__Group__15341 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Instance__TypeAssignment_1_in_rule__Instance__Group__1__Impl5368 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Instance__Group__2__Impl_in_rule__Instance__Group__25398 = new BitSet(new long[]{0x0000000008002000L});
        public static final BitSet FOLLOW_rule__Instance__Group__3_in_rule__Instance__Group__25401 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Instance__ManyAssignment_2_in_rule__Instance__Group__2__Impl5428 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Instance__Group__3__Impl_in_rule__Instance__Group__35459 = new BitSet(new long[]{0x0000000000004010L});
        public static final BitSet FOLLOW_rule__Instance__Group__4_in_rule__Instance__Group__35462 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_13_in_rule__Instance__Group__3__Impl5490 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Instance__Group__4__Impl_in_rule__Instance__Group__45521 = new BitSet(new long[]{0x0000000000004010L});
        public static final BitSet FOLLOW_rule__Instance__Group__5_in_rule__Instance__Group__45524 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Instance__AssignmentsAssignment_4_in_rule__Instance__Group__4__Impl5551 = new BitSet(new long[]{0x0000000000000012L});
        public static final BitSet FOLLOW_rule__Instance__Group__5__Impl_in_rule__Instance__Group__55582 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_14_in_rule__Instance__Group__5__Impl5610 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Instance__Group_0_0__0__Impl_in_rule__Instance__Group_0_0__05653 = new BitSet(new long[]{0x0000000000100000L});
        public static final BitSet FOLLOW_rule__Instance__Group_0_0__1_in_rule__Instance__Group_0_0__05656 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Instance__Group_0_0__1__Impl_in_rule__Instance__Group_0_0__15714 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_20_in_rule__Instance__Group_0_0__1__Impl5742 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Instance__Group_0_1__0__Impl_in_rule__Instance__Group_0_1__05777 = new BitSet(new long[]{0x0000000000300010L});
        public static final BitSet FOLLOW_rule__Instance__Group_0_1__1_in_rule__Instance__Group_0_1__05780 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Instance__Group_0_1__1__Impl_in_rule__Instance__Group_0_1__15838 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_21_in_rule__Instance__Group_0_1__1__Impl5866 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Assignment__Group__0__Impl_in_rule__Assignment__Group__05901 = new BitSet(new long[]{0x0000000000400000L});
        public static final BitSet FOLLOW_rule__Assignment__Group__1_in_rule__Assignment__Group__05904 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Assignment__FeatureAssignment_0_in_rule__Assignment__Group__0__Impl5931 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Assignment__Group__1__Impl_in_rule__Assignment__Group__15961 = new BitSet(new long[]{0x0000000001B00070L});
        public static final BitSet FOLLOW_rule__Assignment__Group__2_in_rule__Assignment__Group__15964 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_22_in_rule__Assignment__Group__1__Impl5992 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Assignment__Group__2__Impl_in_rule__Assignment__Group__26023 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Assignment__ValueAssignment_2_in_rule__Assignment__Group__2__Impl6050 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ThisRef__Group__0__Impl_in_rule__ThisRef__Group__06086 = new BitSet(new long[]{0x0000000001B00070L});
        public static final BitSet FOLLOW_rule__ThisRef__Group__1_in_rule__ThisRef__Group__06089 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ThisRef__Group__1__Impl_in_rule__ThisRef__Group__16147 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_23_in_rule__ThisRef__Group__1__Impl6175 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__CollectionValue__Group__0__Impl_in_rule__CollectionValue__Group__06210 = new BitSet(new long[]{0x0000000001B00070L});
        public static final BitSet FOLLOW_rule__CollectionValue__Group__1_in_rule__CollectionValue__Group__06213 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_24_in_rule__CollectionValue__Group__0__Impl6241 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__CollectionValue__Group__1__Impl_in_rule__CollectionValue__Group__16272 = new BitSet(new long[]{0x0000000006000000L});
        public static final BitSet FOLLOW_rule__CollectionValue__Group__2_in_rule__CollectionValue__Group__16275 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__CollectionValue__ContentsAssignment_1_in_rule__CollectionValue__Group__1__Impl6302 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__CollectionValue__Group__2__Impl_in_rule__CollectionValue__Group__26332 = new BitSet(new long[]{0x0000000006000000L});
        public static final BitSet FOLLOW_rule__CollectionValue__Group__3_in_rule__CollectionValue__Group__26335 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__CollectionValue__Group_2__0_in_rule__CollectionValue__Group__2__Impl6362 = new BitSet(new long[]{0x0000000004000002L});
        public static final BitSet FOLLOW_rule__CollectionValue__Group__3__Impl_in_rule__CollectionValue__Group__36393 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_25_in_rule__CollectionValue__Group__3__Impl6421 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__CollectionValue__Group_2__0__Impl_in_rule__CollectionValue__Group_2__06460 = new BitSet(new long[]{0x0000000001B00070L});
        public static final BitSet FOLLOW_rule__CollectionValue__Group_2__1_in_rule__CollectionValue__Group_2__06463 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_26_in_rule__CollectionValue__Group_2__0__Impl6491 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__CollectionValue__Group_2__1__Impl_in_rule__CollectionValue__Group_2__16522 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__CollectionValue__ContentsAssignment_2_1_in_rule__CollectionValue__Group_2__1__Impl6549 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__QualifiedName__Group__0__Impl_in_rule__QualifiedName__Group__06583 = new BitSet(new long[]{0x0000000000080000L});
        public static final BitSet FOLLOW_rule__QualifiedName__Group__1_in_rule__QualifiedName__Group__06586 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rule__QualifiedName__Group__0__Impl6613 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__QualifiedName__Group__1__Impl_in_rule__QualifiedName__Group__16642 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__QualifiedName__Group_1__0_in_rule__QualifiedName__Group__1__Impl6669 = new BitSet(new long[]{0x0000000000080002L});
        public static final BitSet FOLLOW_rule__QualifiedName__Group_1__0__Impl_in_rule__QualifiedName__Group_1__06704 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_rule__QualifiedName__Group_1__1_in_rule__QualifiedName__Group_1__06707 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_19_in_rule__QualifiedName__Group_1__0__Impl6735 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__QualifiedName__Group_1__1__Impl_in_rule__QualifiedName__Group_1__16766 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rule__QualifiedName__Group_1__1__Impl6793 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleImport_in_rule__Correspondences__ImportsAssignment_06832 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleModelCorrespondence_in_rule__Correspondences__CorrespondencesAssignment_16863 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_STRING_in_rule__Import__ImportURIAssignment_16894 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleModelFunction_in_rule__ModelCorrespondence__LeftAssignment_16925 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleModelFunction_in_rule__ModelCorrespondence__RightAssignment_36956 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClassCorrespondence_in_rule__ModelCorrespondence__ChildrenAssignment_56987 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rulePackageRef_in_rule__ModelFunction__TermsAssignment7018 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rule__PackageRef__ReferencedPackageAssignment7053 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClassFunction_in_rule__ClassCorrespondence__LeftAssignment_0_0_17088 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClassFunction_in_rule__ClassCorrespondence__RightAssignment_0_0_37119 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClassFunction_in_rule__ClassCorrespondence__LeftAssignment_0_1_17150 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClassFunction_in_rule__ClassCorrespondence__RightAssignment_0_1_37181 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClassFunction_in_rule__ClassCorrespondence__LeftAssignment_0_2_17212 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClassFunction_in_rule__ClassCorrespondence__RightAssignment_0_2_37243 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClassFunction_in_rule__ClassCorrespondence__LeftAssignment_0_3_17274 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClassFunction_in_rule__ClassCorrespondence__RightAssignment_0_3_37305 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureCorrespondence_in_rule__ClassCorrespondence__ChildrenAssignment_27336 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClassTerm_in_rule__ClassFunction__TermsAssignment7367 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rule__ClassRef__ReferencedClassAssignment7402 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureFunction_in_rule__FeatureCorrespondence__LeftAssignment_0_17437 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureFunction_in_rule__FeatureCorrespondence__RightAssignment_0_37468 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureFunction_in_rule__FeatureCorrespondence__LeftAssignment_1_17499 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureFunction_in_rule__FeatureCorrespondence__RightAssignment_1_37530 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureFunction_in_rule__FeatureCorrespondence__LeftAssignment_2_17561 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureFunction_in_rule__FeatureCorrespondence__RightAssignment_2_37592 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureFunction_in_rule__FeatureCorrespondence__LeftAssignment_3_17623 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureFunction_in_rule__FeatureCorrespondence__RightAssignment_3_37654 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureTerm_in_rule__FeatureFunction__TermsAssignment_07685 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureTerm_in_rule__FeatureFunction__TermsAssignment_1_17716 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleInstance_in_rule__FeatureRef__InstanceAssignment_0_07747 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rule__FeatureRef__ReferencedFeatureAssignment_17782 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rule__Instance__TypeAssignment_17821 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_27_in_rule__Instance__ManyAssignment_27861 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAssignment_in_rule__Instance__AssignmentsAssignment_47900 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rule__Assignment__FeatureAssignment_07935 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleTerm_in_rule__Assignment__ValueAssignment_27970 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleTerm_in_rule__CollectionValue__ContentsAssignment_18001 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleTerm_in_rule__CollectionValue__ContentsAssignment_2_18032 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_STRING_in_rule__StringValue__ValueAssignment8063 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_INT_in_rule__IntValue__ValueAssignment8094 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_0__0_in_synpred1_InternalCorrespondence1398 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_1__0_in_synpred2_InternalCorrespondence1416 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassCorrespondence__Group_0_2__0_in_synpred3_InternalCorrespondence1434 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_0__0_in_synpred5_InternalCorrespondence1534 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_1__0_in_synpred6_InternalCorrespondence1552 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FeatureCorrespondence__Group_2__0_in_synpred7_InternalCorrespondence1570 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureRef_in_synpred8_InternalCorrespondence1621 = new BitSet(new long[]{0x0000000000000002L});
    }


}