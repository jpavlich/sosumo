package correspondence.xtext.generator;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import correspondence.BidirectionalCorrespondence;
import correspondence.ClassRef;
import correspondence.Correspondence;
import correspondence.Correspondences;
import correspondence.FeatureRef;
import correspondence.Import;
import correspondence.LeftToRightCorrespondence;

public class Composer {

	public static final byte LEFT = 0;
	public static final byte RIGHT = 1;
	ResourceSet inputResourceSet = new ResourceSetImpl();
	Resource leftModel;
	Resource rightModel;
	CompositionFactory factory = new CompositionFactory();

	public void doGenerate(Resource resource) {

		Correspondences r = this.getCorrespondences(resource);

		Import leftImport = (Import) r.eContents().get(LEFT);
		Import rightImport = (Import) r.eContents().get(RIGHT);
		System.out.println("Left and right models:");
		System.out.println(leftImport.getImportURI());
		System.out.println(rightImport.getImportURI());

		Correspondence correspondenceSet = null;

		for (int i = 0; i < r.eContents().size(); i++) {
			if (r.eContents().get(i) instanceof Correspondence) {
				correspondenceSet = (Correspondence) r.eContents().get(i);
			}
		}

		leftModel = inputResourceSet.getResource(URI.createURI(leftImport.getImportURI()), true);
		rightModel = inputResourceSet.getResource(URI.createURI(rightImport.getImportURI()), true);

		this.addCorrespondencesBetweeenClasses(correspondenceSet);
		this.addCorrespondencesBetweenFeatures(correspondenceSet, null);

		Utils.saveModel(leftModel);
		Utils.saveModel(rightModel);

		System.out.println("End...");

	}

	public void addCorrespondencesBetweeenClasses(Correspondence parentCorrespondence) {

		if (parentCorrespondence instanceof BidirectionalCorrespondence) {
			if (parentCorrespondence.getLeft().getTerms().get(0) instanceof ClassRef
					&& parentCorrespondence.getRight().getTerms().get(0) instanceof ClassRef) {
				factory.createBidirectionalClassCorrespondence(
						(ClassRef) parentCorrespondence.getLeft().getTerms().get(0),
						(ClassRef) parentCorrespondence.getRight().getTerms().get(0), leftModel, rightModel);
			}

		} else if (parentCorrespondence instanceof LeftToRightCorrespondence) {
			if (parentCorrespondence.getLeft().getTerms().get(0) instanceof ClassRef
					&& parentCorrespondence.getRight().getTerms().get(0) instanceof ClassRef) {
				factory.createLefToRightClassCorrespondence((ClassRef) parentCorrespondence.getLeft().getTerms().get(0),
						(ClassRef) parentCorrespondence.getRight().getTerms().get(0), leftModel, rightModel);
			}
		}

		for (int j = 0; j < parentCorrespondence.getChildren().size(); j++) {
			addCorrespondencesBetweeenClasses(parentCorrespondence.getChildren().get(j));
		}
	}

	public void addCorrespondencesBetweenFeatures(Correspondence childCorrespondence,
			Correspondence parentCorrespondence) {
		if (childCorrespondence.getLeft().getTerms().get(0) instanceof FeatureRef
				&& childCorrespondence.getRight().getTerms().get(0) instanceof FeatureRef) {
			if (parentCorrespondence.getLeft().getTerms().get(0) instanceof ClassRef
					&& parentCorrespondence.getRight().getTerms().get(0) instanceof ClassRef) {
				ClassRef leftC = (ClassRef) parentCorrespondence.getLeft().getTerms().get(0);
				ClassRef rightC = (ClassRef) parentCorrespondence.getRight().getTerms().get(0);
				FeatureRef left = (FeatureRef) childCorrespondence.getLeft().getTerms().get(0);
				FeatureRef right = (FeatureRef) childCorrespondence.getRight().getTerms().get(0);
				if (childCorrespondence instanceof BidirectionalCorrespondence) {
					factory.createBidirectionalFeatureCorrespondence(leftC, rightC, left, right, leftModel, rightModel);
				}
				else if (childCorrespondence instanceof LeftToRightCorrespondence) {
					factory.createLeftToRightFeatureCorrespondence(leftC, rightC, left, right, leftModel, rightModel);
				}
			}
		}
		for (int j = 0; j < childCorrespondence.getChildren().size(); j++) {
			addCorrespondencesBetweenFeatures(childCorrespondence.getChildren().get(j), childCorrespondence);
		}
	}

	public Correspondences getCorrespondences(Resource resource) {
		return (Correspondences) resource.getContents().get(0);
	}

}
