package correspondence.xtext.generator;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.resource.Resource;

public class Validator {

	public EAttribute searchAttribute(EClass originalClass, String name) {
		for (EAttribute attribute : originalClass.getEAllAttributes())
			if (attribute.getName().equals(name))
				return attribute;

		return null;
	}

	public EClass searchClassInModel(Resource model, String name) {
		for (EObject object : model.getContents().get(0).eContents()) {
			if (object instanceof EClass) {
				EClass eclass = (EClass) object;
				if (eclass.getName().equals(name)) {
					return eclass;
				}
			}
		}
		return null;
	}

	public EAttribute searchAttributeInClass(EClass eclass, String name) {
		for (EAttribute attribute : eclass.getEAllAttributes()) {
			if (attribute.getName().equals(name)) {
				return attribute;
			}
		}
		return null;
	}
	
	public EReference searchReferenceInClass(EClass eclass, String name) {
		for (EReference reference : eclass.getEAllReferences()) {
			if (reference.getName().equals(name)) {
				return reference;
			}
		}
		return null;
	}

}
