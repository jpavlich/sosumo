package correspondence.xtext.generator;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import correspondence.ClassRef;
import correspondence.FeatureRef;

public class CompositionFactory {

	ResourceSet inputResourceSet = new ResourceSetImpl();
	ResourceSet outputResourceSet = new ResourceSetImpl();
	Validator validator = new Validator();
	Set<String> referencesNames = new HashSet<String>();

	public Validator getValidator() {
		return validator;
	}

	public void setValidator(Validator validator) {
		this.validator = validator;
	}

	public EAnnotation createAnnotation(EObject reference, String id) {
		EAnnotation annotation = EcoreFactory.eINSTANCE.createEAnnotation();
		annotation.setSource(id);
		annotation.getReferences().add(reference);
		return annotation;
	}

	public String addReferenceName(String name) {
		if (!referencesNames.contains(name)) {
			referencesNames.add(name);
			return name;
		}
		// else {
		// String addedName = name + "_";
		// return this.addReferenceName(addedName);
		// // return addedName;
		// }
		return null;
	}

	public void createBidirectionalFeatureCorrespondence(ClassRef leftClassRef, ClassRef rightClassRef,
			FeatureRef leftFeature, FeatureRef rightFeature, Resource leftModel, Resource rightModel) {
		EClass leftClass = validator.searchClassInModel(leftModel, leftClassRef.getReferencedClass().getName());
		EStructuralFeature leftAttribute = validator.searchAttributeInClass(leftClass,
				leftFeature.getReferencedFeature().getName());
		if (leftAttribute == null) {
			leftAttribute = validator.searchReferenceInClass(leftClass, leftFeature.getReferencedFeature().getName());
		}
		EClass rightClass = validator.searchClassInModel(rightModel, rightClassRef.getReferencedClass().getName());
		EStructuralFeature rightAttribute = validator.searchAttributeInClass(rightClass,
				rightFeature.getReferencedFeature().getName());
		if (rightAttribute == null) {
			rightAttribute = validator.searchReferenceInClass(rightClass,
					rightFeature.getReferencedFeature().getName());
		}
		if (leftClass != null && leftAttribute != null && rightClass != null && rightAttribute != null) {
			String namel = this.addReferenceName(leftClass.getName() + "_" + leftAttribute.getName() + "2" + rightClass.getName() + "_" + rightAttribute.getName());
			String namer = this.addReferenceName(rightClass.getName() + "_" + rightAttribute.getName() + "2" + leftClass.getName() + "_" + leftAttribute.getName());
			if (namel != null && namer != null) {
				EAnnotation annotationl = this.createAnnotation(rightAttribute, namel);
				leftAttribute.getEAnnotations().add(annotationl);
				EAnnotation annotationr = this.createAnnotation(leftAttribute, namer);
				rightAttribute.getEAnnotations().add(annotationr);
			}
		}
	}

	public void createLeftToRightFeatureCorrespondence(ClassRef leftClassRef, ClassRef rightClassRef,
			FeatureRef leftFeature, FeatureRef rightFeature, Resource leftModel, Resource rightModel) {
		EClass leftClass = validator.searchClassInModel(leftModel, leftClassRef.getReferencedClass().getName());
		EStructuralFeature leftAttribute = validator.searchAttributeInClass(leftClass,
				leftFeature.getReferencedFeature().getName());
		if (leftAttribute == null) {
			leftAttribute = validator.searchReferenceInClass(leftClass, leftFeature.getReferencedFeature().getName());
		}
		EClass rightClass = validator.searchClassInModel(rightModel, rightClassRef.getReferencedClass().getName());
		EStructuralFeature rightAttribute = validator.searchAttributeInClass(rightClass,
				rightFeature.getReferencedFeature().getName());
		if (rightAttribute == null) {
			rightAttribute = validator.searchReferenceInClass(rightClass,
					rightFeature.getReferencedFeature().getName());
		}
		if (leftClass != null && leftAttribute != null && rightClass != null && rightAttribute != null) {
			String namel = this.addReferenceName(leftClass.getName() + "_" + leftAttribute.getName() + "2" + rightClass.getName() + "_" + rightAttribute.getName());
			if (namel != null) {
				EAnnotation annotationl = this.createAnnotation(rightAttribute, namel);
				leftAttribute.getEAnnotations().add(annotationl);
			}
		}
	}

	public void createBidirectionalClassCorrespondence(ClassRef leftClass, ClassRef rightClass, Resource leftModel,
			Resource rightModel) {
		EClass left = validator.searchClassInModel(leftModel, leftClass.getReferencedClass().getName());
		EClass right = validator.searchClassInModel(rightModel, rightClass.getReferencedClass().getName());
		if (left != null && right != null) {
			String namel = this.addReferenceName(left.getName() + "2" + right.getName());
			String namer = this.addReferenceName(right.getName() + "2" + left.getName());
			if (namel != null && namer != null) {
				EAnnotation annotationl = this.createAnnotation(right, namel);
				EAnnotation annotationr = this.createAnnotation(left, namer);
				left.getEAnnotations().add(annotationl);
				right.getEAnnotations().add(annotationr);
			}
		}
	}

	public void createLefToRightClassCorrespondence(ClassRef leftClass, ClassRef rightClass, Resource leftModel,
			Resource rightModel) {
		EClass left = validator.searchClassInModel(leftModel, leftClass.getReferencedClass().getName());
		EClass right = validator.searchClassInModel(rightModel, rightClass.getReferencedClass().getName());
		if (left != null && right != null) {
			String namel = this.addReferenceName(left.getName() + "2" + right.getName());
			if (namel != null) {
				EAnnotation annotationl = this.createAnnotation(right, namel);
				left.getEAnnotations().add(annotationl);
			}
		}
	}
}
