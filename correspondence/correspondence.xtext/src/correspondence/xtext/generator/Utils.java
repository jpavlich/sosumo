package correspondence.xtext.generator;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.eclipse.emf.ecore.resource.Resource;

public class Utils {

	public static void saveModel(Resource model) {
		try {
			model.save(null);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		catch (IOException e) {
			e.printStackTrace();
		}

	}

}
