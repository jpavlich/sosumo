package correspondence.xtext.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import correspondence.xtext.services.CorrespondenceGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalCorrespondenceParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'import'", "'--'", "'{'", "'}'", "'=='", "'->'", "'<-'", "'+'", "'.'", "'existing'", "'new'", "'*'", "'='", "'this'", "'['", "','", "']'"
    };
    public static final int RULE_ID=5;
    public static final int T__27=27;
    public static final int T__26=26;
    public static final int T__25=25;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__19=19;
    public static final int RULE_STRING=4;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=6;
    public static final int RULE_WS=9;

    // delegates
    // delegators


        public InternalCorrespondenceParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalCorrespondenceParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalCorrespondenceParser.tokenNames; }
    public String getGrammarFileName() { return "../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g"; }



    /*
      This grammar contains a lot of empty actions to work around a bug in ANTLR.
      Otherwise the ANTLR tool will create synpreds that cannot be compiled in some rare cases.
    */
     
     	private CorrespondenceGrammarAccess grammarAccess;
     	
        public InternalCorrespondenceParser(TokenStream input, CorrespondenceGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Correspondences";	
       	}
       	
       	@Override
       	protected CorrespondenceGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleCorrespondences"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:73:1: entryRuleCorrespondences returns [EObject current=null] : iv_ruleCorrespondences= ruleCorrespondences EOF ;
    public final EObject entryRuleCorrespondences() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCorrespondences = null;


        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:74:2: (iv_ruleCorrespondences= ruleCorrespondences EOF )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:75:2: iv_ruleCorrespondences= ruleCorrespondences EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCorrespondencesRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleCorrespondences_in_entryRuleCorrespondences81);
            iv_ruleCorrespondences=ruleCorrespondences();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCorrespondences; 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleCorrespondences91); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCorrespondences"


    // $ANTLR start "ruleCorrespondences"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:82:1: ruleCorrespondences returns [EObject current=null] : ( ( (lv_imports_0_0= ruleImport ) )* ( (lv_correspondences_1_0= ruleModelCorrespondence ) ) ) ;
    public final EObject ruleCorrespondences() throws RecognitionException {
        EObject current = null;

        EObject lv_imports_0_0 = null;

        EObject lv_correspondences_1_0 = null;


         enterRule(); 
            
        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:85:28: ( ( ( (lv_imports_0_0= ruleImport ) )* ( (lv_correspondences_1_0= ruleModelCorrespondence ) ) ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:86:1: ( ( (lv_imports_0_0= ruleImport ) )* ( (lv_correspondences_1_0= ruleModelCorrespondence ) ) )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:86:1: ( ( (lv_imports_0_0= ruleImport ) )* ( (lv_correspondences_1_0= ruleModelCorrespondence ) ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:86:2: ( (lv_imports_0_0= ruleImport ) )* ( (lv_correspondences_1_0= ruleModelCorrespondence ) )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:86:2: ( (lv_imports_0_0= ruleImport ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==11) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:87:1: (lv_imports_0_0= ruleImport )
            	    {
            	    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:87:1: (lv_imports_0_0= ruleImport )
            	    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:88:3: lv_imports_0_0= ruleImport
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getCorrespondencesAccess().getImportsImportParserRuleCall_0_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_ruleImport_in_ruleCorrespondences137);
            	    lv_imports_0_0=ruleImport();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getCorrespondencesRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"imports",
            	              		lv_imports_0_0, 
            	              		"Import");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:104:3: ( (lv_correspondences_1_0= ruleModelCorrespondence ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:105:1: (lv_correspondences_1_0= ruleModelCorrespondence )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:105:1: (lv_correspondences_1_0= ruleModelCorrespondence )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:106:3: lv_correspondences_1_0= ruleModelCorrespondence
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getCorrespondencesAccess().getCorrespondencesModelCorrespondenceParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_ruleModelCorrespondence_in_ruleCorrespondences159);
            lv_correspondences_1_0=ruleModelCorrespondence();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getCorrespondencesRule());
              	        }
                     		add(
                     			current, 
                     			"correspondences",
                      		lv_correspondences_1_0, 
                      		"ModelCorrespondence");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCorrespondences"


    // $ANTLR start "entryRuleImport"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:130:1: entryRuleImport returns [EObject current=null] : iv_ruleImport= ruleImport EOF ;
    public final EObject entryRuleImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImport = null;


        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:131:2: (iv_ruleImport= ruleImport EOF )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:132:2: iv_ruleImport= ruleImport EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getImportRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleImport_in_entryRuleImport195);
            iv_ruleImport=ruleImport();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleImport; 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleImport205); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:139:1: ruleImport returns [EObject current=null] : (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_importURI_1_0=null;

         enterRule(); 
            
        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:142:28: ( (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:143:1: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:143:1: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:143:3: otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,11,FollowSets000.FOLLOW_11_in_ruleImport242); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getImportAccess().getImportKeyword_0());
                  
            }
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:147:1: ( (lv_importURI_1_0= RULE_STRING ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:148:1: (lv_importURI_1_0= RULE_STRING )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:148:1: (lv_importURI_1_0= RULE_STRING )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:149:3: lv_importURI_1_0= RULE_STRING
            {
            lv_importURI_1_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_ruleImport259); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_importURI_1_0, grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getImportRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"importURI",
                      		lv_importURI_1_0, 
                      		"STRING");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleModelCorrespondence"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:173:1: entryRuleModelCorrespondence returns [EObject current=null] : iv_ruleModelCorrespondence= ruleModelCorrespondence EOF ;
    public final EObject entryRuleModelCorrespondence() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModelCorrespondence = null;


        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:174:2: (iv_ruleModelCorrespondence= ruleModelCorrespondence EOF )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:175:2: iv_ruleModelCorrespondence= ruleModelCorrespondence EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getModelCorrespondenceRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleModelCorrespondence_in_entryRuleModelCorrespondence300);
            iv_ruleModelCorrespondence=ruleModelCorrespondence();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleModelCorrespondence; 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleModelCorrespondence310); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModelCorrespondence"


    // $ANTLR start "ruleModelCorrespondence"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:182:1: ruleModelCorrespondence returns [EObject current=null] : ( () ( (lv_left_1_0= ruleModelFunction ) ) otherlv_2= '--' ( (lv_right_3_0= ruleModelFunction ) ) otherlv_4= '{' ( (lv_children_5_0= ruleClassCorrespondence ) )* otherlv_6= '}' ) ;
    public final EObject ruleModelCorrespondence() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_left_1_0 = null;

        EObject lv_right_3_0 = null;

        EObject lv_children_5_0 = null;


         enterRule(); 
            
        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:185:28: ( ( () ( (lv_left_1_0= ruleModelFunction ) ) otherlv_2= '--' ( (lv_right_3_0= ruleModelFunction ) ) otherlv_4= '{' ( (lv_children_5_0= ruleClassCorrespondence ) )* otherlv_6= '}' ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:186:1: ( () ( (lv_left_1_0= ruleModelFunction ) ) otherlv_2= '--' ( (lv_right_3_0= ruleModelFunction ) ) otherlv_4= '{' ( (lv_children_5_0= ruleClassCorrespondence ) )* otherlv_6= '}' )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:186:1: ( () ( (lv_left_1_0= ruleModelFunction ) ) otherlv_2= '--' ( (lv_right_3_0= ruleModelFunction ) ) otherlv_4= '{' ( (lv_children_5_0= ruleClassCorrespondence ) )* otherlv_6= '}' )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:186:2: () ( (lv_left_1_0= ruleModelFunction ) ) otherlv_2= '--' ( (lv_right_3_0= ruleModelFunction ) ) otherlv_4= '{' ( (lv_children_5_0= ruleClassCorrespondence ) )* otherlv_6= '}'
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:186:2: ()
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:187:2: 
            {
            if ( state.backtracking==0 ) {
               
              	  /* */ 
              	
            }
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getModelCorrespondenceAccess().getBidirectionalCorrespondenceAction_0(),
                          current);
                  
            }

            }

            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:195:2: ( (lv_left_1_0= ruleModelFunction ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:196:1: (lv_left_1_0= ruleModelFunction )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:196:1: (lv_left_1_0= ruleModelFunction )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:197:3: lv_left_1_0= ruleModelFunction
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getModelCorrespondenceAccess().getLeftModelFunctionParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_ruleModelFunction_in_ruleModelCorrespondence368);
            lv_left_1_0=ruleModelFunction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getModelCorrespondenceRule());
              	        }
                     		set(
                     			current, 
                     			"left",
                      		lv_left_1_0, 
                      		"ModelFunction");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleModelCorrespondence380); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getModelCorrespondenceAccess().getHyphenMinusHyphenMinusKeyword_2());
                  
            }
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:217:1: ( (lv_right_3_0= ruleModelFunction ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:218:1: (lv_right_3_0= ruleModelFunction )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:218:1: (lv_right_3_0= ruleModelFunction )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:219:3: lv_right_3_0= ruleModelFunction
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getModelCorrespondenceAccess().getRightModelFunctionParserRuleCall_3_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_ruleModelFunction_in_ruleModelCorrespondence401);
            lv_right_3_0=ruleModelFunction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getModelCorrespondenceRule());
              	        }
                     		set(
                     			current, 
                     			"right",
                      		lv_right_3_0, 
                      		"ModelFunction");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_4=(Token)match(input,13,FollowSets000.FOLLOW_13_in_ruleModelCorrespondence413); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getModelCorrespondenceAccess().getLeftCurlyBracketKeyword_4());
                  
            }
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:239:1: ( (lv_children_5_0= ruleClassCorrespondence ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==RULE_ID||(LA2_0>=20 && LA2_0<=21)) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:240:1: (lv_children_5_0= ruleClassCorrespondence )
            	    {
            	    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:240:1: (lv_children_5_0= ruleClassCorrespondence )
            	    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:241:3: lv_children_5_0= ruleClassCorrespondence
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getModelCorrespondenceAccess().getChildrenClassCorrespondenceParserRuleCall_5_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_ruleClassCorrespondence_in_ruleModelCorrespondence434);
            	    lv_children_5_0=ruleClassCorrespondence();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getModelCorrespondenceRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"children",
            	              		lv_children_5_0, 
            	              		"ClassCorrespondence");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            otherlv_6=(Token)match(input,14,FollowSets000.FOLLOW_14_in_ruleModelCorrespondence447); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_6, grammarAccess.getModelCorrespondenceAccess().getRightCurlyBracketKeyword_6());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModelCorrespondence"


    // $ANTLR start "entryRuleModelFunction"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:269:1: entryRuleModelFunction returns [EObject current=null] : iv_ruleModelFunction= ruleModelFunction EOF ;
    public final EObject entryRuleModelFunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModelFunction = null;


        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:270:2: (iv_ruleModelFunction= ruleModelFunction EOF )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:271:2: iv_ruleModelFunction= ruleModelFunction EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getModelFunctionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleModelFunction_in_entryRuleModelFunction483);
            iv_ruleModelFunction=ruleModelFunction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleModelFunction; 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleModelFunction493); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModelFunction"


    // $ANTLR start "ruleModelFunction"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:278:1: ruleModelFunction returns [EObject current=null] : ( (lv_terms_0_0= rulePackageRef ) ) ;
    public final EObject ruleModelFunction() throws RecognitionException {
        EObject current = null;

        EObject lv_terms_0_0 = null;


         enterRule(); 
            
        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:281:28: ( ( (lv_terms_0_0= rulePackageRef ) ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:282:1: ( (lv_terms_0_0= rulePackageRef ) )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:282:1: ( (lv_terms_0_0= rulePackageRef ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:283:1: (lv_terms_0_0= rulePackageRef )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:283:1: (lv_terms_0_0= rulePackageRef )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:284:3: lv_terms_0_0= rulePackageRef
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getModelFunctionAccess().getTermsPackageRefParserRuleCall_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_rulePackageRef_in_ruleModelFunction538);
            lv_terms_0_0=rulePackageRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getModelFunctionRule());
              	        }
                     		add(
                     			current, 
                     			"terms",
                      		lv_terms_0_0, 
                      		"PackageRef");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModelFunction"


    // $ANTLR start "entryRulePackageRef"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:308:1: entryRulePackageRef returns [EObject current=null] : iv_rulePackageRef= rulePackageRef EOF ;
    public final EObject entryRulePackageRef() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePackageRef = null;


        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:309:2: (iv_rulePackageRef= rulePackageRef EOF )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:310:2: iv_rulePackageRef= rulePackageRef EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getPackageRefRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_rulePackageRef_in_entryRulePackageRef573);
            iv_rulePackageRef=rulePackageRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_rulePackageRef; 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRulePackageRef583); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePackageRef"


    // $ANTLR start "rulePackageRef"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:317:1: rulePackageRef returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject rulePackageRef() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:320:28: ( ( (otherlv_0= RULE_ID ) ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:321:1: ( (otherlv_0= RULE_ID ) )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:321:1: ( (otherlv_0= RULE_ID ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:322:1: (otherlv_0= RULE_ID )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:322:1: (otherlv_0= RULE_ID )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:323:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {
               
              		  /* */ 
              		
            }
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getPackageRefRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rulePackageRef631); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getPackageRefAccess().getReferencedPackageEPackageCrossReference_0()); 
              	
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePackageRef"


    // $ANTLR start "entryRuleClassCorrespondence"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:345:1: entryRuleClassCorrespondence returns [EObject current=null] : iv_ruleClassCorrespondence= ruleClassCorrespondence EOF ;
    public final EObject entryRuleClassCorrespondence() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClassCorrespondence = null;


        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:346:2: (iv_ruleClassCorrespondence= ruleClassCorrespondence EOF )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:347:2: iv_ruleClassCorrespondence= ruleClassCorrespondence EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getClassCorrespondenceRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleClassCorrespondence_in_entryRuleClassCorrespondence666);
            iv_ruleClassCorrespondence=ruleClassCorrespondence();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleClassCorrespondence; 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleClassCorrespondence676); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClassCorrespondence"


    // $ANTLR start "ruleClassCorrespondence"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:354:1: ruleClassCorrespondence returns [EObject current=null] : ( ( ( () ( (lv_left_1_0= ruleClassFunction ) ) otherlv_2= '--' ( (lv_right_3_0= ruleClassFunction ) ) ) | ( () ( (lv_left_5_0= ruleClassFunction ) ) otherlv_6= '==' ( (lv_right_7_0= ruleClassFunction ) ) ) | ( () ( (lv_left_9_0= ruleClassFunction ) ) otherlv_10= '->' ( (lv_right_11_0= ruleClassFunction ) ) ) | ( () ( (lv_left_13_0= ruleClassFunction ) ) otherlv_14= '<-' ( (lv_right_15_0= ruleClassFunction ) ) ) ) otherlv_16= '{' ( (lv_children_17_0= ruleFeatureCorrespondence ) )* otherlv_18= '}' ) ;
    public final EObject ruleClassCorrespondence() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_6=null;
        Token otherlv_10=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_18=null;
        EObject lv_left_1_0 = null;

        EObject lv_right_3_0 = null;

        EObject lv_left_5_0 = null;

        EObject lv_right_7_0 = null;

        EObject lv_left_9_0 = null;

        EObject lv_right_11_0 = null;

        EObject lv_left_13_0 = null;

        EObject lv_right_15_0 = null;

        EObject lv_children_17_0 = null;


         enterRule(); 
            
        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:357:28: ( ( ( ( () ( (lv_left_1_0= ruleClassFunction ) ) otherlv_2= '--' ( (lv_right_3_0= ruleClassFunction ) ) ) | ( () ( (lv_left_5_0= ruleClassFunction ) ) otherlv_6= '==' ( (lv_right_7_0= ruleClassFunction ) ) ) | ( () ( (lv_left_9_0= ruleClassFunction ) ) otherlv_10= '->' ( (lv_right_11_0= ruleClassFunction ) ) ) | ( () ( (lv_left_13_0= ruleClassFunction ) ) otherlv_14= '<-' ( (lv_right_15_0= ruleClassFunction ) ) ) ) otherlv_16= '{' ( (lv_children_17_0= ruleFeatureCorrespondence ) )* otherlv_18= '}' ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:358:1: ( ( ( () ( (lv_left_1_0= ruleClassFunction ) ) otherlv_2= '--' ( (lv_right_3_0= ruleClassFunction ) ) ) | ( () ( (lv_left_5_0= ruleClassFunction ) ) otherlv_6= '==' ( (lv_right_7_0= ruleClassFunction ) ) ) | ( () ( (lv_left_9_0= ruleClassFunction ) ) otherlv_10= '->' ( (lv_right_11_0= ruleClassFunction ) ) ) | ( () ( (lv_left_13_0= ruleClassFunction ) ) otherlv_14= '<-' ( (lv_right_15_0= ruleClassFunction ) ) ) ) otherlv_16= '{' ( (lv_children_17_0= ruleFeatureCorrespondence ) )* otherlv_18= '}' )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:358:1: ( ( ( () ( (lv_left_1_0= ruleClassFunction ) ) otherlv_2= '--' ( (lv_right_3_0= ruleClassFunction ) ) ) | ( () ( (lv_left_5_0= ruleClassFunction ) ) otherlv_6= '==' ( (lv_right_7_0= ruleClassFunction ) ) ) | ( () ( (lv_left_9_0= ruleClassFunction ) ) otherlv_10= '->' ( (lv_right_11_0= ruleClassFunction ) ) ) | ( () ( (lv_left_13_0= ruleClassFunction ) ) otherlv_14= '<-' ( (lv_right_15_0= ruleClassFunction ) ) ) ) otherlv_16= '{' ( (lv_children_17_0= ruleFeatureCorrespondence ) )* otherlv_18= '}' )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:358:2: ( ( () ( (lv_left_1_0= ruleClassFunction ) ) otherlv_2= '--' ( (lv_right_3_0= ruleClassFunction ) ) ) | ( () ( (lv_left_5_0= ruleClassFunction ) ) otherlv_6= '==' ( (lv_right_7_0= ruleClassFunction ) ) ) | ( () ( (lv_left_9_0= ruleClassFunction ) ) otherlv_10= '->' ( (lv_right_11_0= ruleClassFunction ) ) ) | ( () ( (lv_left_13_0= ruleClassFunction ) ) otherlv_14= '<-' ( (lv_right_15_0= ruleClassFunction ) ) ) ) otherlv_16= '{' ( (lv_children_17_0= ruleFeatureCorrespondence ) )* otherlv_18= '}'
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:358:2: ( ( () ( (lv_left_1_0= ruleClassFunction ) ) otherlv_2= '--' ( (lv_right_3_0= ruleClassFunction ) ) ) | ( () ( (lv_left_5_0= ruleClassFunction ) ) otherlv_6= '==' ( (lv_right_7_0= ruleClassFunction ) ) ) | ( () ( (lv_left_9_0= ruleClassFunction ) ) otherlv_10= '->' ( (lv_right_11_0= ruleClassFunction ) ) ) | ( () ( (lv_left_13_0= ruleClassFunction ) ) otherlv_14= '<-' ( (lv_right_15_0= ruleClassFunction ) ) ) )
            int alt3=4;
            switch ( input.LA(1) ) {
            case RULE_ID:
                {
                int LA3_1 = input.LA(2);

                if ( (synpred3_InternalCorrespondence()) ) {
                    alt3=1;
                }
                else if ( (synpred4_InternalCorrespondence()) ) {
                    alt3=2;
                }
                else if ( (synpred5_InternalCorrespondence()) ) {
                    alt3=3;
                }
                else if ( (true) ) {
                    alt3=4;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 3, 1, input);

                    throw nvae;
                }
                }
                break;
            case 20:
                {
                int LA3_2 = input.LA(2);

                if ( (synpred3_InternalCorrespondence()) ) {
                    alt3=1;
                }
                else if ( (synpred4_InternalCorrespondence()) ) {
                    alt3=2;
                }
                else if ( (synpred5_InternalCorrespondence()) ) {
                    alt3=3;
                }
                else if ( (true) ) {
                    alt3=4;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 3, 2, input);

                    throw nvae;
                }
                }
                break;
            case 21:
                {
                int LA3_3 = input.LA(2);

                if ( (synpred3_InternalCorrespondence()) ) {
                    alt3=1;
                }
                else if ( (synpred4_InternalCorrespondence()) ) {
                    alt3=2;
                }
                else if ( (synpred5_InternalCorrespondence()) ) {
                    alt3=3;
                }
                else if ( (true) ) {
                    alt3=4;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 3, 3, input);

                    throw nvae;
                }
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:358:3: ( () ( (lv_left_1_0= ruleClassFunction ) ) otherlv_2= '--' ( (lv_right_3_0= ruleClassFunction ) ) )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:358:3: ( () ( (lv_left_1_0= ruleClassFunction ) ) otherlv_2= '--' ( (lv_right_3_0= ruleClassFunction ) ) )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:358:4: () ( (lv_left_1_0= ruleClassFunction ) ) otherlv_2= '--' ( (lv_right_3_0= ruleClassFunction ) )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:358:4: ()
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:359:2: 
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getClassCorrespondenceAccess().getBidirectionalCorrespondenceAction_0_0_0(),
                                  current);
                          
                    }

                    }

                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:367:2: ( (lv_left_1_0= ruleClassFunction ) )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:368:1: (lv_left_1_0= ruleClassFunction )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:368:1: (lv_left_1_0= ruleClassFunction )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:369:3: lv_left_1_0= ruleClassFunction
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getClassCorrespondenceAccess().getLeftClassFunctionParserRuleCall_0_0_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleClassFunction_in_ruleClassCorrespondence736);
                    lv_left_1_0=ruleClassFunction();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getClassCorrespondenceRule());
                      	        }
                             		set(
                             			current, 
                             			"left",
                              		lv_left_1_0, 
                              		"ClassFunction");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    otherlv_2=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleClassCorrespondence748); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_2, grammarAccess.getClassCorrespondenceAccess().getHyphenMinusHyphenMinusKeyword_0_0_2());
                          
                    }
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:389:1: ( (lv_right_3_0= ruleClassFunction ) )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:390:1: (lv_right_3_0= ruleClassFunction )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:390:1: (lv_right_3_0= ruleClassFunction )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:391:3: lv_right_3_0= ruleClassFunction
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getClassCorrespondenceAccess().getRightClassFunctionParserRuleCall_0_0_3_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleClassFunction_in_ruleClassCorrespondence769);
                    lv_right_3_0=ruleClassFunction();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getClassCorrespondenceRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"ClassFunction");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:408:6: ( () ( (lv_left_5_0= ruleClassFunction ) ) otherlv_6= '==' ( (lv_right_7_0= ruleClassFunction ) ) )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:408:6: ( () ( (lv_left_5_0= ruleClassFunction ) ) otherlv_6= '==' ( (lv_right_7_0= ruleClassFunction ) ) )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:408:7: () ( (lv_left_5_0= ruleClassFunction ) ) otherlv_6= '==' ( (lv_right_7_0= ruleClassFunction ) )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:408:7: ()
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:409:2: 
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getClassCorrespondenceAccess().getOneToOneCorrespondenceAction_0_1_0(),
                                  current);
                          
                    }

                    }

                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:417:2: ( (lv_left_5_0= ruleClassFunction ) )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:418:1: (lv_left_5_0= ruleClassFunction )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:418:1: (lv_left_5_0= ruleClassFunction )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:419:3: lv_left_5_0= ruleClassFunction
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getClassCorrespondenceAccess().getLeftClassFunctionParserRuleCall_0_1_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleClassFunction_in_ruleClassCorrespondence810);
                    lv_left_5_0=ruleClassFunction();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getClassCorrespondenceRule());
                      	        }
                             		set(
                             			current, 
                             			"left",
                              		lv_left_5_0, 
                              		"ClassFunction");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    otherlv_6=(Token)match(input,15,FollowSets000.FOLLOW_15_in_ruleClassCorrespondence822); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_6, grammarAccess.getClassCorrespondenceAccess().getEqualsSignEqualsSignKeyword_0_1_2());
                          
                    }
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:439:1: ( (lv_right_7_0= ruleClassFunction ) )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:440:1: (lv_right_7_0= ruleClassFunction )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:440:1: (lv_right_7_0= ruleClassFunction )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:441:3: lv_right_7_0= ruleClassFunction
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getClassCorrespondenceAccess().getRightClassFunctionParserRuleCall_0_1_3_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleClassFunction_in_ruleClassCorrespondence843);
                    lv_right_7_0=ruleClassFunction();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getClassCorrespondenceRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_7_0, 
                              		"ClassFunction");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:458:6: ( () ( (lv_left_9_0= ruleClassFunction ) ) otherlv_10= '->' ( (lv_right_11_0= ruleClassFunction ) ) )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:458:6: ( () ( (lv_left_9_0= ruleClassFunction ) ) otherlv_10= '->' ( (lv_right_11_0= ruleClassFunction ) ) )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:458:7: () ( (lv_left_9_0= ruleClassFunction ) ) otherlv_10= '->' ( (lv_right_11_0= ruleClassFunction ) )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:458:7: ()
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:459:2: 
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getClassCorrespondenceAccess().getLeftToRightCorrespondenceAction_0_2_0(),
                                  current);
                          
                    }

                    }

                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:467:2: ( (lv_left_9_0= ruleClassFunction ) )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:468:1: (lv_left_9_0= ruleClassFunction )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:468:1: (lv_left_9_0= ruleClassFunction )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:469:3: lv_left_9_0= ruleClassFunction
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getClassCorrespondenceAccess().getLeftClassFunctionParserRuleCall_0_2_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleClassFunction_in_ruleClassCorrespondence884);
                    lv_left_9_0=ruleClassFunction();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getClassCorrespondenceRule());
                      	        }
                             		set(
                             			current, 
                             			"left",
                              		lv_left_9_0, 
                              		"ClassFunction");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    otherlv_10=(Token)match(input,16,FollowSets000.FOLLOW_16_in_ruleClassCorrespondence896); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_10, grammarAccess.getClassCorrespondenceAccess().getHyphenMinusGreaterThanSignKeyword_0_2_2());
                          
                    }
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:489:1: ( (lv_right_11_0= ruleClassFunction ) )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:490:1: (lv_right_11_0= ruleClassFunction )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:490:1: (lv_right_11_0= ruleClassFunction )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:491:3: lv_right_11_0= ruleClassFunction
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getClassCorrespondenceAccess().getRightClassFunctionParserRuleCall_0_2_3_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleClassFunction_in_ruleClassCorrespondence917);
                    lv_right_11_0=ruleClassFunction();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getClassCorrespondenceRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_11_0, 
                              		"ClassFunction");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 4 :
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:508:6: ( () ( (lv_left_13_0= ruleClassFunction ) ) otherlv_14= '<-' ( (lv_right_15_0= ruleClassFunction ) ) )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:508:6: ( () ( (lv_left_13_0= ruleClassFunction ) ) otherlv_14= '<-' ( (lv_right_15_0= ruleClassFunction ) ) )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:508:7: () ( (lv_left_13_0= ruleClassFunction ) ) otherlv_14= '<-' ( (lv_right_15_0= ruleClassFunction ) )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:508:7: ()
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:509:2: 
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getClassCorrespondenceAccess().getRightToLeftCorrespondenceAction_0_3_0(),
                                  current);
                          
                    }

                    }

                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:517:2: ( (lv_left_13_0= ruleClassFunction ) )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:518:1: (lv_left_13_0= ruleClassFunction )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:518:1: (lv_left_13_0= ruleClassFunction )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:519:3: lv_left_13_0= ruleClassFunction
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getClassCorrespondenceAccess().getLeftClassFunctionParserRuleCall_0_3_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleClassFunction_in_ruleClassCorrespondence958);
                    lv_left_13_0=ruleClassFunction();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getClassCorrespondenceRule());
                      	        }
                             		set(
                             			current, 
                             			"left",
                              		lv_left_13_0, 
                              		"ClassFunction");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    otherlv_14=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleClassCorrespondence970); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_14, grammarAccess.getClassCorrespondenceAccess().getLessThanSignHyphenMinusKeyword_0_3_2());
                          
                    }
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:539:1: ( (lv_right_15_0= ruleClassFunction ) )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:540:1: (lv_right_15_0= ruleClassFunction )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:540:1: (lv_right_15_0= ruleClassFunction )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:541:3: lv_right_15_0= ruleClassFunction
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getClassCorrespondenceAccess().getRightClassFunctionParserRuleCall_0_3_3_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleClassFunction_in_ruleClassCorrespondence991);
                    lv_right_15_0=ruleClassFunction();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getClassCorrespondenceRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_15_0, 
                              		"ClassFunction");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;

            }

            otherlv_16=(Token)match(input,13,FollowSets000.FOLLOW_13_in_ruleClassCorrespondence1005); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_16, grammarAccess.getClassCorrespondenceAccess().getLeftCurlyBracketKeyword_1());
                  
            }
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:561:1: ( (lv_children_17_0= ruleFeatureCorrespondence ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( ((LA4_0>=RULE_STRING && LA4_0<=RULE_INT)||(LA4_0>=20 && LA4_0<=21)||LA4_0==25) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:562:1: (lv_children_17_0= ruleFeatureCorrespondence )
            	    {
            	    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:562:1: (lv_children_17_0= ruleFeatureCorrespondence )
            	    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:563:3: lv_children_17_0= ruleFeatureCorrespondence
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getClassCorrespondenceAccess().getChildrenFeatureCorrespondenceParserRuleCall_2_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_ruleFeatureCorrespondence_in_ruleClassCorrespondence1026);
            	    lv_children_17_0=ruleFeatureCorrespondence();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getClassCorrespondenceRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"children",
            	              		lv_children_17_0, 
            	              		"FeatureCorrespondence");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            otherlv_18=(Token)match(input,14,FollowSets000.FOLLOW_14_in_ruleClassCorrespondence1039); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_18, grammarAccess.getClassCorrespondenceAccess().getRightCurlyBracketKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClassCorrespondence"


    // $ANTLR start "entryRuleClassFunction"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:591:1: entryRuleClassFunction returns [EObject current=null] : iv_ruleClassFunction= ruleClassFunction EOF ;
    public final EObject entryRuleClassFunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClassFunction = null;


        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:592:2: (iv_ruleClassFunction= ruleClassFunction EOF )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:593:2: iv_ruleClassFunction= ruleClassFunction EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getClassFunctionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleClassFunction_in_entryRuleClassFunction1075);
            iv_ruleClassFunction=ruleClassFunction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleClassFunction; 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleClassFunction1085); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClassFunction"


    // $ANTLR start "ruleClassFunction"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:600:1: ruleClassFunction returns [EObject current=null] : ( (lv_terms_0_0= ruleClassTerm ) ) ;
    public final EObject ruleClassFunction() throws RecognitionException {
        EObject current = null;

        EObject lv_terms_0_0 = null;


         enterRule(); 
            
        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:603:28: ( ( (lv_terms_0_0= ruleClassTerm ) ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:604:1: ( (lv_terms_0_0= ruleClassTerm ) )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:604:1: ( (lv_terms_0_0= ruleClassTerm ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:605:1: (lv_terms_0_0= ruleClassTerm )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:605:1: (lv_terms_0_0= ruleClassTerm )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:606:3: lv_terms_0_0= ruleClassTerm
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getClassFunctionAccess().getTermsClassTermParserRuleCall_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_ruleClassTerm_in_ruleClassFunction1130);
            lv_terms_0_0=ruleClassTerm();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getClassFunctionRule());
              	        }
                     		add(
                     			current, 
                     			"terms",
                      		lv_terms_0_0, 
                      		"ClassTerm");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClassFunction"


    // $ANTLR start "entryRuleClassTerm"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:630:1: entryRuleClassTerm returns [EObject current=null] : iv_ruleClassTerm= ruleClassTerm EOF ;
    public final EObject entryRuleClassTerm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClassTerm = null;


        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:631:2: (iv_ruleClassTerm= ruleClassTerm EOF )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:632:2: iv_ruleClassTerm= ruleClassTerm EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getClassTermRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleClassTerm_in_entryRuleClassTerm1165);
            iv_ruleClassTerm=ruleClassTerm();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleClassTerm; 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleClassTerm1175); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClassTerm"


    // $ANTLR start "ruleClassTerm"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:639:1: ruleClassTerm returns [EObject current=null] : (this_ClassRef_0= ruleClassRef | this_Instance_1= ruleInstance ) ;
    public final EObject ruleClassTerm() throws RecognitionException {
        EObject current = null;

        EObject this_ClassRef_0 = null;

        EObject this_Instance_1 = null;


         enterRule(); 
            
        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:642:28: ( (this_ClassRef_0= ruleClassRef | this_Instance_1= ruleInstance ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:643:1: (this_ClassRef_0= ruleClassRef | this_Instance_1= ruleInstance )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:643:1: (this_ClassRef_0= ruleClassRef | this_Instance_1= ruleInstance )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==RULE_ID) ) {
                alt5=1;
            }
            else if ( ((LA5_0>=20 && LA5_0<=21)) ) {
                alt5=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:644:2: this_ClassRef_0= ruleClassRef
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getClassTermAccess().getClassRefParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleClassRef_in_ruleClassTerm1225);
                    this_ClassRef_0=ruleClassRef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ClassRef_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:657:2: this_Instance_1= ruleInstance
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getClassTermAccess().getInstanceParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleInstance_in_ruleClassTerm1255);
                    this_Instance_1=ruleInstance();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Instance_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClassTerm"


    // $ANTLR start "entryRuleClassRef"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:676:1: entryRuleClassRef returns [EObject current=null] : iv_ruleClassRef= ruleClassRef EOF ;
    public final EObject entryRuleClassRef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClassRef = null;


        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:677:2: (iv_ruleClassRef= ruleClassRef EOF )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:678:2: iv_ruleClassRef= ruleClassRef EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getClassRefRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleClassRef_in_entryRuleClassRef1290);
            iv_ruleClassRef=ruleClassRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleClassRef; 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleClassRef1300); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClassRef"


    // $ANTLR start "ruleClassRef"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:685:1: ruleClassRef returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleClassRef() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:688:28: ( ( (otherlv_0= RULE_ID ) ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:689:1: ( (otherlv_0= RULE_ID ) )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:689:1: ( (otherlv_0= RULE_ID ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:690:1: (otherlv_0= RULE_ID )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:690:1: (otherlv_0= RULE_ID )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:691:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {
               
              		  /* */ 
              		
            }
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getClassRefRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_ruleClassRef1348); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getClassRefAccess().getReferencedClassEClassCrossReference_0()); 
              	
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClassRef"


    // $ANTLR start "entryRuleFeatureCorrespondence"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:713:1: entryRuleFeatureCorrespondence returns [EObject current=null] : iv_ruleFeatureCorrespondence= ruleFeatureCorrespondence EOF ;
    public final EObject entryRuleFeatureCorrespondence() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeatureCorrespondence = null;


        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:714:2: (iv_ruleFeatureCorrespondence= ruleFeatureCorrespondence EOF )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:715:2: iv_ruleFeatureCorrespondence= ruleFeatureCorrespondence EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFeatureCorrespondenceRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleFeatureCorrespondence_in_entryRuleFeatureCorrespondence1383);
            iv_ruleFeatureCorrespondence=ruleFeatureCorrespondence();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFeatureCorrespondence; 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFeatureCorrespondence1393); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeatureCorrespondence"


    // $ANTLR start "ruleFeatureCorrespondence"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:722:1: ruleFeatureCorrespondence returns [EObject current=null] : ( ( () ( (lv_left_1_0= ruleFeatureFunction ) ) otherlv_2= '--' ( (lv_right_3_0= ruleFeatureFunction ) ) ) | ( () ( (lv_left_5_0= ruleFeatureFunction ) ) otherlv_6= '==' ( (lv_right_7_0= ruleFeatureFunction ) ) ) | ( () ( (lv_left_9_0= ruleFeatureFunction ) ) otherlv_10= '->' ( (lv_right_11_0= ruleFeatureFunction ) ) ) | ( () ( (lv_left_13_0= ruleFeatureFunction ) ) otherlv_14= '<-' ( (lv_right_15_0= ruleFeatureFunction ) ) ) ) ;
    public final EObject ruleFeatureCorrespondence() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_6=null;
        Token otherlv_10=null;
        Token otherlv_14=null;
        EObject lv_left_1_0 = null;

        EObject lv_right_3_0 = null;

        EObject lv_left_5_0 = null;

        EObject lv_right_7_0 = null;

        EObject lv_left_9_0 = null;

        EObject lv_right_11_0 = null;

        EObject lv_left_13_0 = null;

        EObject lv_right_15_0 = null;


         enterRule(); 
            
        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:725:28: ( ( ( () ( (lv_left_1_0= ruleFeatureFunction ) ) otherlv_2= '--' ( (lv_right_3_0= ruleFeatureFunction ) ) ) | ( () ( (lv_left_5_0= ruleFeatureFunction ) ) otherlv_6= '==' ( (lv_right_7_0= ruleFeatureFunction ) ) ) | ( () ( (lv_left_9_0= ruleFeatureFunction ) ) otherlv_10= '->' ( (lv_right_11_0= ruleFeatureFunction ) ) ) | ( () ( (lv_left_13_0= ruleFeatureFunction ) ) otherlv_14= '<-' ( (lv_right_15_0= ruleFeatureFunction ) ) ) ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:726:1: ( ( () ( (lv_left_1_0= ruleFeatureFunction ) ) otherlv_2= '--' ( (lv_right_3_0= ruleFeatureFunction ) ) ) | ( () ( (lv_left_5_0= ruleFeatureFunction ) ) otherlv_6= '==' ( (lv_right_7_0= ruleFeatureFunction ) ) ) | ( () ( (lv_left_9_0= ruleFeatureFunction ) ) otherlv_10= '->' ( (lv_right_11_0= ruleFeatureFunction ) ) ) | ( () ( (lv_left_13_0= ruleFeatureFunction ) ) otherlv_14= '<-' ( (lv_right_15_0= ruleFeatureFunction ) ) ) )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:726:1: ( ( () ( (lv_left_1_0= ruleFeatureFunction ) ) otherlv_2= '--' ( (lv_right_3_0= ruleFeatureFunction ) ) ) | ( () ( (lv_left_5_0= ruleFeatureFunction ) ) otherlv_6= '==' ( (lv_right_7_0= ruleFeatureFunction ) ) ) | ( () ( (lv_left_9_0= ruleFeatureFunction ) ) otherlv_10= '->' ( (lv_right_11_0= ruleFeatureFunction ) ) ) | ( () ( (lv_left_13_0= ruleFeatureFunction ) ) otherlv_14= '<-' ( (lv_right_15_0= ruleFeatureFunction ) ) ) )
            int alt6=4;
            alt6 = dfa6.predict(input);
            switch (alt6) {
                case 1 :
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:726:2: ( () ( (lv_left_1_0= ruleFeatureFunction ) ) otherlv_2= '--' ( (lv_right_3_0= ruleFeatureFunction ) ) )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:726:2: ( () ( (lv_left_1_0= ruleFeatureFunction ) ) otherlv_2= '--' ( (lv_right_3_0= ruleFeatureFunction ) ) )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:726:3: () ( (lv_left_1_0= ruleFeatureFunction ) ) otherlv_2= '--' ( (lv_right_3_0= ruleFeatureFunction ) )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:726:3: ()
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:727:2: 
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getFeatureCorrespondenceAccess().getBidirectionalCorrespondenceAction_0_0(),
                                  current);
                          
                    }

                    }

                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:735:2: ( (lv_left_1_0= ruleFeatureFunction ) )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:736:1: (lv_left_1_0= ruleFeatureFunction )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:736:1: (lv_left_1_0= ruleFeatureFunction )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:737:3: lv_left_1_0= ruleFeatureFunction
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getFeatureCorrespondenceAccess().getLeftFeatureFunctionParserRuleCall_0_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleFeatureFunction_in_ruleFeatureCorrespondence1452);
                    lv_left_1_0=ruleFeatureFunction();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getFeatureCorrespondenceRule());
                      	        }
                             		set(
                             			current, 
                             			"left",
                              		lv_left_1_0, 
                              		"FeatureFunction");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    otherlv_2=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleFeatureCorrespondence1464); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_2, grammarAccess.getFeatureCorrespondenceAccess().getHyphenMinusHyphenMinusKeyword_0_2());
                          
                    }
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:757:1: ( (lv_right_3_0= ruleFeatureFunction ) )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:758:1: (lv_right_3_0= ruleFeatureFunction )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:758:1: (lv_right_3_0= ruleFeatureFunction )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:759:3: lv_right_3_0= ruleFeatureFunction
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getFeatureCorrespondenceAccess().getRightFeatureFunctionParserRuleCall_0_3_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleFeatureFunction_in_ruleFeatureCorrespondence1485);
                    lv_right_3_0=ruleFeatureFunction();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getFeatureCorrespondenceRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"FeatureFunction");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:776:6: ( () ( (lv_left_5_0= ruleFeatureFunction ) ) otherlv_6= '==' ( (lv_right_7_0= ruleFeatureFunction ) ) )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:776:6: ( () ( (lv_left_5_0= ruleFeatureFunction ) ) otherlv_6= '==' ( (lv_right_7_0= ruleFeatureFunction ) ) )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:776:7: () ( (lv_left_5_0= ruleFeatureFunction ) ) otherlv_6= '==' ( (lv_right_7_0= ruleFeatureFunction ) )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:776:7: ()
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:777:2: 
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getFeatureCorrespondenceAccess().getOneToOneCorrespondenceAction_1_0(),
                                  current);
                          
                    }

                    }

                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:785:2: ( (lv_left_5_0= ruleFeatureFunction ) )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:786:1: (lv_left_5_0= ruleFeatureFunction )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:786:1: (lv_left_5_0= ruleFeatureFunction )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:787:3: lv_left_5_0= ruleFeatureFunction
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getFeatureCorrespondenceAccess().getLeftFeatureFunctionParserRuleCall_1_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleFeatureFunction_in_ruleFeatureCorrespondence1526);
                    lv_left_5_0=ruleFeatureFunction();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getFeatureCorrespondenceRule());
                      	        }
                             		set(
                             			current, 
                             			"left",
                              		lv_left_5_0, 
                              		"FeatureFunction");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    otherlv_6=(Token)match(input,15,FollowSets000.FOLLOW_15_in_ruleFeatureCorrespondence1538); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_6, grammarAccess.getFeatureCorrespondenceAccess().getEqualsSignEqualsSignKeyword_1_2());
                          
                    }
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:807:1: ( (lv_right_7_0= ruleFeatureFunction ) )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:808:1: (lv_right_7_0= ruleFeatureFunction )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:808:1: (lv_right_7_0= ruleFeatureFunction )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:809:3: lv_right_7_0= ruleFeatureFunction
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getFeatureCorrespondenceAccess().getRightFeatureFunctionParserRuleCall_1_3_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleFeatureFunction_in_ruleFeatureCorrespondence1559);
                    lv_right_7_0=ruleFeatureFunction();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getFeatureCorrespondenceRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_7_0, 
                              		"FeatureFunction");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:826:6: ( () ( (lv_left_9_0= ruleFeatureFunction ) ) otherlv_10= '->' ( (lv_right_11_0= ruleFeatureFunction ) ) )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:826:6: ( () ( (lv_left_9_0= ruleFeatureFunction ) ) otherlv_10= '->' ( (lv_right_11_0= ruleFeatureFunction ) ) )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:826:7: () ( (lv_left_9_0= ruleFeatureFunction ) ) otherlv_10= '->' ( (lv_right_11_0= ruleFeatureFunction ) )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:826:7: ()
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:827:2: 
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getFeatureCorrespondenceAccess().getLeftToRightCorrespondenceAction_2_0(),
                                  current);
                          
                    }

                    }

                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:835:2: ( (lv_left_9_0= ruleFeatureFunction ) )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:836:1: (lv_left_9_0= ruleFeatureFunction )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:836:1: (lv_left_9_0= ruleFeatureFunction )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:837:3: lv_left_9_0= ruleFeatureFunction
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getFeatureCorrespondenceAccess().getLeftFeatureFunctionParserRuleCall_2_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleFeatureFunction_in_ruleFeatureCorrespondence1600);
                    lv_left_9_0=ruleFeatureFunction();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getFeatureCorrespondenceRule());
                      	        }
                             		set(
                             			current, 
                             			"left",
                              		lv_left_9_0, 
                              		"FeatureFunction");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    otherlv_10=(Token)match(input,16,FollowSets000.FOLLOW_16_in_ruleFeatureCorrespondence1612); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_10, grammarAccess.getFeatureCorrespondenceAccess().getHyphenMinusGreaterThanSignKeyword_2_2());
                          
                    }
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:857:1: ( (lv_right_11_0= ruleFeatureFunction ) )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:858:1: (lv_right_11_0= ruleFeatureFunction )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:858:1: (lv_right_11_0= ruleFeatureFunction )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:859:3: lv_right_11_0= ruleFeatureFunction
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getFeatureCorrespondenceAccess().getRightFeatureFunctionParserRuleCall_2_3_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleFeatureFunction_in_ruleFeatureCorrespondence1633);
                    lv_right_11_0=ruleFeatureFunction();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getFeatureCorrespondenceRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_11_0, 
                              		"FeatureFunction");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 4 :
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:876:6: ( () ( (lv_left_13_0= ruleFeatureFunction ) ) otherlv_14= '<-' ( (lv_right_15_0= ruleFeatureFunction ) ) )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:876:6: ( () ( (lv_left_13_0= ruleFeatureFunction ) ) otherlv_14= '<-' ( (lv_right_15_0= ruleFeatureFunction ) ) )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:876:7: () ( (lv_left_13_0= ruleFeatureFunction ) ) otherlv_14= '<-' ( (lv_right_15_0= ruleFeatureFunction ) )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:876:7: ()
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:877:2: 
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getFeatureCorrespondenceAccess().getRightToLeftCorrespondenceAction_3_0(),
                                  current);
                          
                    }

                    }

                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:885:2: ( (lv_left_13_0= ruleFeatureFunction ) )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:886:1: (lv_left_13_0= ruleFeatureFunction )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:886:1: (lv_left_13_0= ruleFeatureFunction )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:887:3: lv_left_13_0= ruleFeatureFunction
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getFeatureCorrespondenceAccess().getLeftFeatureFunctionParserRuleCall_3_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleFeatureFunction_in_ruleFeatureCorrespondence1674);
                    lv_left_13_0=ruleFeatureFunction();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getFeatureCorrespondenceRule());
                      	        }
                             		set(
                             			current, 
                             			"left",
                              		lv_left_13_0, 
                              		"FeatureFunction");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    otherlv_14=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleFeatureCorrespondence1686); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_14, grammarAccess.getFeatureCorrespondenceAccess().getLessThanSignHyphenMinusKeyword_3_2());
                          
                    }
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:907:1: ( (lv_right_15_0= ruleFeatureFunction ) )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:908:1: (lv_right_15_0= ruleFeatureFunction )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:908:1: (lv_right_15_0= ruleFeatureFunction )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:909:3: lv_right_15_0= ruleFeatureFunction
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getFeatureCorrespondenceAccess().getRightFeatureFunctionParserRuleCall_3_3_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleFeatureFunction_in_ruleFeatureCorrespondence1707);
                    lv_right_15_0=ruleFeatureFunction();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getFeatureCorrespondenceRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_15_0, 
                              		"FeatureFunction");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeatureCorrespondence"


    // $ANTLR start "entryRuleFeatureFunction"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:933:1: entryRuleFeatureFunction returns [EObject current=null] : iv_ruleFeatureFunction= ruleFeatureFunction EOF ;
    public final EObject entryRuleFeatureFunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeatureFunction = null;


        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:934:2: (iv_ruleFeatureFunction= ruleFeatureFunction EOF )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:935:2: iv_ruleFeatureFunction= ruleFeatureFunction EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFeatureFunctionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleFeatureFunction_in_entryRuleFeatureFunction1744);
            iv_ruleFeatureFunction=ruleFeatureFunction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFeatureFunction; 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFeatureFunction1754); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeatureFunction"


    // $ANTLR start "ruleFeatureFunction"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:942:1: ruleFeatureFunction returns [EObject current=null] : ( ( (lv_terms_0_0= ruleFeatureTerm ) ) (otherlv_1= '+' ( (lv_terms_2_0= ruleFeatureTerm ) ) )* ) ;
    public final EObject ruleFeatureFunction() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_terms_0_0 = null;

        EObject lv_terms_2_0 = null;


         enterRule(); 
            
        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:945:28: ( ( ( (lv_terms_0_0= ruleFeatureTerm ) ) (otherlv_1= '+' ( (lv_terms_2_0= ruleFeatureTerm ) ) )* ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:946:1: ( ( (lv_terms_0_0= ruleFeatureTerm ) ) (otherlv_1= '+' ( (lv_terms_2_0= ruleFeatureTerm ) ) )* )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:946:1: ( ( (lv_terms_0_0= ruleFeatureTerm ) ) (otherlv_1= '+' ( (lv_terms_2_0= ruleFeatureTerm ) ) )* )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:946:2: ( (lv_terms_0_0= ruleFeatureTerm ) ) (otherlv_1= '+' ( (lv_terms_2_0= ruleFeatureTerm ) ) )*
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:946:2: ( (lv_terms_0_0= ruleFeatureTerm ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:947:1: (lv_terms_0_0= ruleFeatureTerm )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:947:1: (lv_terms_0_0= ruleFeatureTerm )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:948:3: lv_terms_0_0= ruleFeatureTerm
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getFeatureFunctionAccess().getTermsFeatureTermParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_ruleFeatureTerm_in_ruleFeatureFunction1800);
            lv_terms_0_0=ruleFeatureTerm();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getFeatureFunctionRule());
              	        }
                     		add(
                     			current, 
                     			"terms",
                      		lv_terms_0_0, 
                      		"FeatureTerm");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:964:2: (otherlv_1= '+' ( (lv_terms_2_0= ruleFeatureTerm ) ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==18) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:964:4: otherlv_1= '+' ( (lv_terms_2_0= ruleFeatureTerm ) )
            	    {
            	    otherlv_1=(Token)match(input,18,FollowSets000.FOLLOW_18_in_ruleFeatureFunction1813); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_1, grammarAccess.getFeatureFunctionAccess().getPlusSignKeyword_1_0());
            	          
            	    }
            	    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:968:1: ( (lv_terms_2_0= ruleFeatureTerm ) )
            	    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:969:1: (lv_terms_2_0= ruleFeatureTerm )
            	    {
            	    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:969:1: (lv_terms_2_0= ruleFeatureTerm )
            	    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:970:3: lv_terms_2_0= ruleFeatureTerm
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getFeatureFunctionAccess().getTermsFeatureTermParserRuleCall_1_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_ruleFeatureTerm_in_ruleFeatureFunction1834);
            	    lv_terms_2_0=ruleFeatureTerm();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getFeatureFunctionRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"terms",
            	              		lv_terms_2_0, 
            	              		"FeatureTerm");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeatureFunction"


    // $ANTLR start "entryRuleFeatureTerm"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:994:1: entryRuleFeatureTerm returns [EObject current=null] : iv_ruleFeatureTerm= ruleFeatureTerm EOF ;
    public final EObject entryRuleFeatureTerm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeatureTerm = null;


        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:995:2: (iv_ruleFeatureTerm= ruleFeatureTerm EOF )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:996:2: iv_ruleFeatureTerm= ruleFeatureTerm EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFeatureTermRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleFeatureTerm_in_entryRuleFeatureTerm1872);
            iv_ruleFeatureTerm=ruleFeatureTerm();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFeatureTerm; 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFeatureTerm1882); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeatureTerm"


    // $ANTLR start "ruleFeatureTerm"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1003:1: ruleFeatureTerm returns [EObject current=null] : (this_FeatureRef_0= ruleFeatureRef | this_Value_1= ruleValue | this_Instance_2= ruleInstance ) ;
    public final EObject ruleFeatureTerm() throws RecognitionException {
        EObject current = null;

        EObject this_FeatureRef_0 = null;

        EObject this_Value_1 = null;

        EObject this_Instance_2 = null;


         enterRule(); 
            
        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1006:28: ( (this_FeatureRef_0= ruleFeatureRef | this_Value_1= ruleValue | this_Instance_2= ruleInstance ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1007:1: (this_FeatureRef_0= ruleFeatureRef | this_Value_1= ruleValue | this_Instance_2= ruleInstance )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1007:1: (this_FeatureRef_0= ruleFeatureRef | this_Value_1= ruleValue | this_Instance_2= ruleInstance )
            int alt8=3;
            switch ( input.LA(1) ) {
            case 20:
                {
                int LA8_1 = input.LA(2);

                if ( (synpred12_InternalCorrespondence()) ) {
                    alt8=1;
                }
                else if ( (true) ) {
                    alt8=3;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 8, 1, input);

                    throw nvae;
                }
                }
                break;
            case 21:
                {
                int LA8_2 = input.LA(2);

                if ( (synpred12_InternalCorrespondence()) ) {
                    alt8=1;
                }
                else if ( (true) ) {
                    alt8=3;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 8, 2, input);

                    throw nvae;
                }
                }
                break;
            case RULE_ID:
                {
                alt8=1;
                }
                break;
            case RULE_STRING:
            case RULE_INT:
            case 25:
                {
                alt8=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }

            switch (alt8) {
                case 1 :
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1008:2: this_FeatureRef_0= ruleFeatureRef
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getFeatureTermAccess().getFeatureRefParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleFeatureRef_in_ruleFeatureTerm1932);
                    this_FeatureRef_0=ruleFeatureRef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_FeatureRef_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1021:2: this_Value_1= ruleValue
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getFeatureTermAccess().getValueParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleValue_in_ruleFeatureTerm1962);
                    this_Value_1=ruleValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Value_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1034:2: this_Instance_2= ruleInstance
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getFeatureTermAccess().getInstanceParserRuleCall_2()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleInstance_in_ruleFeatureTerm1992);
                    this_Instance_2=ruleInstance();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Instance_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeatureTerm"


    // $ANTLR start "entryRuleFeatureRef"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1053:1: entryRuleFeatureRef returns [EObject current=null] : iv_ruleFeatureRef= ruleFeatureRef EOF ;
    public final EObject entryRuleFeatureRef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeatureRef = null;


        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1054:2: (iv_ruleFeatureRef= ruleFeatureRef EOF )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1055:2: iv_ruleFeatureRef= ruleFeatureRef EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFeatureRefRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleFeatureRef_in_entryRuleFeatureRef2027);
            iv_ruleFeatureRef=ruleFeatureRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFeatureRef; 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFeatureRef2037); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeatureRef"


    // $ANTLR start "ruleFeatureRef"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1062:1: ruleFeatureRef returns [EObject current=null] : ( ( ( (lv_instance_0_0= ruleInstance ) ) otherlv_1= '.' )? ( (otherlv_2= RULE_ID ) ) ) ;
    public final EObject ruleFeatureRef() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        EObject lv_instance_0_0 = null;


         enterRule(); 
            
        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1065:28: ( ( ( ( (lv_instance_0_0= ruleInstance ) ) otherlv_1= '.' )? ( (otherlv_2= RULE_ID ) ) ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1066:1: ( ( ( (lv_instance_0_0= ruleInstance ) ) otherlv_1= '.' )? ( (otherlv_2= RULE_ID ) ) )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1066:1: ( ( ( (lv_instance_0_0= ruleInstance ) ) otherlv_1= '.' )? ( (otherlv_2= RULE_ID ) ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1066:2: ( ( (lv_instance_0_0= ruleInstance ) ) otherlv_1= '.' )? ( (otherlv_2= RULE_ID ) )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1066:2: ( ( (lv_instance_0_0= ruleInstance ) ) otherlv_1= '.' )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( ((LA9_0>=20 && LA9_0<=21)) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1066:3: ( (lv_instance_0_0= ruleInstance ) ) otherlv_1= '.'
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1066:3: ( (lv_instance_0_0= ruleInstance ) )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1067:1: (lv_instance_0_0= ruleInstance )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1067:1: (lv_instance_0_0= ruleInstance )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1068:3: lv_instance_0_0= ruleInstance
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getFeatureRefAccess().getInstanceInstanceParserRuleCall_0_0_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleInstance_in_ruleFeatureRef2084);
                    lv_instance_0_0=ruleInstance();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getFeatureRefRule());
                      	        }
                             		set(
                             			current, 
                             			"instance",
                              		lv_instance_0_0, 
                              		"Instance");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    otherlv_1=(Token)match(input,19,FollowSets000.FOLLOW_19_in_ruleFeatureRef2096); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_1, grammarAccess.getFeatureRefAccess().getFullStopKeyword_0_1());
                          
                    }

                    }
                    break;

            }

            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1088:3: ( (otherlv_2= RULE_ID ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1089:1: (otherlv_2= RULE_ID )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1089:1: (otherlv_2= RULE_ID )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1090:3: otherlv_2= RULE_ID
            {
            if ( state.backtracking==0 ) {
               
              		  /* */ 
              		
            }
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getFeatureRefRule());
              	        }
                      
            }
            otherlv_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_ruleFeatureRef2122); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_2, grammarAccess.getFeatureRefAccess().getReferencedFeatureEStructuralFeatureCrossReference_1_0()); 
              	
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeatureRef"


    // $ANTLR start "entryRuleInstance"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1112:1: entryRuleInstance returns [EObject current=null] : iv_ruleInstance= ruleInstance EOF ;
    public final EObject entryRuleInstance() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInstance = null;


        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1113:2: (iv_ruleInstance= ruleInstance EOF )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1114:2: iv_ruleInstance= ruleInstance EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInstanceRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleInstance_in_entryRuleInstance2158);
            iv_ruleInstance=ruleInstance();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInstance; 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleInstance2168); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInstance"


    // $ANTLR start "ruleInstance"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1121:1: ruleInstance returns [EObject current=null] : ( ( ( () otherlv_1= 'existing' ) | ( () otherlv_3= 'new' ) ) ( (otherlv_4= RULE_ID ) ) ( (lv_many_5_0= '*' ) )? otherlv_6= '{' ( (lv_assignments_7_0= ruleAssignment ) )* otherlv_8= '}' ) ;
    public final EObject ruleInstance() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token lv_many_5_0=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_assignments_7_0 = null;


         enterRule(); 
            
        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1124:28: ( ( ( ( () otherlv_1= 'existing' ) | ( () otherlv_3= 'new' ) ) ( (otherlv_4= RULE_ID ) ) ( (lv_many_5_0= '*' ) )? otherlv_6= '{' ( (lv_assignments_7_0= ruleAssignment ) )* otherlv_8= '}' ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1125:1: ( ( ( () otherlv_1= 'existing' ) | ( () otherlv_3= 'new' ) ) ( (otherlv_4= RULE_ID ) ) ( (lv_many_5_0= '*' ) )? otherlv_6= '{' ( (lv_assignments_7_0= ruleAssignment ) )* otherlv_8= '}' )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1125:1: ( ( ( () otherlv_1= 'existing' ) | ( () otherlv_3= 'new' ) ) ( (otherlv_4= RULE_ID ) ) ( (lv_many_5_0= '*' ) )? otherlv_6= '{' ( (lv_assignments_7_0= ruleAssignment ) )* otherlv_8= '}' )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1125:2: ( ( () otherlv_1= 'existing' ) | ( () otherlv_3= 'new' ) ) ( (otherlv_4= RULE_ID ) ) ( (lv_many_5_0= '*' ) )? otherlv_6= '{' ( (lv_assignments_7_0= ruleAssignment ) )* otherlv_8= '}'
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1125:2: ( ( () otherlv_1= 'existing' ) | ( () otherlv_3= 'new' ) )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==20) ) {
                alt10=1;
            }
            else if ( (LA10_0==21) ) {
                alt10=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1125:3: ( () otherlv_1= 'existing' )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1125:3: ( () otherlv_1= 'existing' )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1125:4: () otherlv_1= 'existing'
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1125:4: ()
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1126:2: 
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getInstanceAccess().getExistingInstanceAction_0_0_0(),
                                  current);
                          
                    }

                    }

                    otherlv_1=(Token)match(input,20,FollowSets000.FOLLOW_20_in_ruleInstance2219); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_1, grammarAccess.getInstanceAccess().getExistingKeyword_0_0_1());
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1139:6: ( () otherlv_3= 'new' )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1139:6: ( () otherlv_3= 'new' )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1139:7: () otherlv_3= 'new'
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1139:7: ()
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1140:2: 
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getInstanceAccess().getNewInstanceAction_0_1_0(),
                                  current);
                          
                    }

                    }

                    otherlv_3=(Token)match(input,21,FollowSets000.FOLLOW_21_in_ruleInstance2251); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getInstanceAccess().getNewKeyword_0_1_1());
                          
                    }

                    }


                    }
                    break;

            }

            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1152:3: ( (otherlv_4= RULE_ID ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1153:1: (otherlv_4= RULE_ID )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1153:1: (otherlv_4= RULE_ID )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1154:3: otherlv_4= RULE_ID
            {
            if ( state.backtracking==0 ) {
               
              		  /* */ 
              		
            }
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getInstanceRule());
              	        }
                      
            }
            otherlv_4=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_ruleInstance2277); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_4, grammarAccess.getInstanceAccess().getTypeEClassCrossReference_1_0()); 
              	
            }

            }


            }

            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1168:2: ( (lv_many_5_0= '*' ) )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==22) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1169:1: (lv_many_5_0= '*' )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1169:1: (lv_many_5_0= '*' )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1170:3: lv_many_5_0= '*'
                    {
                    lv_many_5_0=(Token)match(input,22,FollowSets000.FOLLOW_22_in_ruleInstance2295); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_many_5_0, grammarAccess.getInstanceAccess().getManyAsteriskKeyword_2_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getInstanceRule());
                      	        }
                             		setWithLastConsumed(current, "many", true, "*");
                      	    
                    }

                    }


                    }
                    break;

            }

            otherlv_6=(Token)match(input,13,FollowSets000.FOLLOW_13_in_ruleInstance2321); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_6, grammarAccess.getInstanceAccess().getLeftCurlyBracketKeyword_3());
                  
            }
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1187:1: ( (lv_assignments_7_0= ruleAssignment ) )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==RULE_ID) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1188:1: (lv_assignments_7_0= ruleAssignment )
            	    {
            	    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1188:1: (lv_assignments_7_0= ruleAssignment )
            	    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1189:3: lv_assignments_7_0= ruleAssignment
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getInstanceAccess().getAssignmentsAssignmentParserRuleCall_4_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_ruleAssignment_in_ruleInstance2342);
            	    lv_assignments_7_0=ruleAssignment();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getInstanceRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"assignments",
            	              		lv_assignments_7_0, 
            	              		"Assignment");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

            otherlv_8=(Token)match(input,14,FollowSets000.FOLLOW_14_in_ruleInstance2355); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_8, grammarAccess.getInstanceAccess().getRightCurlyBracketKeyword_5());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInstance"


    // $ANTLR start "entryRuleAssignment"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1217:1: entryRuleAssignment returns [EObject current=null] : iv_ruleAssignment= ruleAssignment EOF ;
    public final EObject entryRuleAssignment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssignment = null;


        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1218:2: (iv_ruleAssignment= ruleAssignment EOF )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1219:2: iv_ruleAssignment= ruleAssignment EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAssignmentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleAssignment_in_entryRuleAssignment2391);
            iv_ruleAssignment=ruleAssignment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAssignment; 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleAssignment2401); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssignment"


    // $ANTLR start "ruleAssignment"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1226:1: ruleAssignment returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_value_2_0= ruleTerm ) ) ) ;
    public final EObject ruleAssignment() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        EObject lv_value_2_0 = null;


         enterRule(); 
            
        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1229:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_value_2_0= ruleTerm ) ) ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1230:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_value_2_0= ruleTerm ) ) )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1230:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_value_2_0= ruleTerm ) ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1230:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_value_2_0= ruleTerm ) )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1230:2: ( (otherlv_0= RULE_ID ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1231:1: (otherlv_0= RULE_ID )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1231:1: (otherlv_0= RULE_ID )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1232:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {
               
              		  /* */ 
              		
            }
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getAssignmentRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_ruleAssignment2450); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getAssignmentAccess().getFeatureEStructuralFeatureCrossReference_0_0()); 
              	
            }

            }


            }

            otherlv_1=(Token)match(input,23,FollowSets000.FOLLOW_23_in_ruleAssignment2462); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getAssignmentAccess().getEqualsSignKeyword_1());
                  
            }
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1250:1: ( (lv_value_2_0= ruleTerm ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1251:1: (lv_value_2_0= ruleTerm )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1251:1: (lv_value_2_0= ruleTerm )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1252:3: lv_value_2_0= ruleTerm
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getAssignmentAccess().getValueTermParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_ruleTerm_in_ruleAssignment2483);
            lv_value_2_0=ruleTerm();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getAssignmentRule());
              	        }
                     		set(
                     			current, 
                     			"value",
                      		lv_value_2_0, 
                      		"Term");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssignment"


    // $ANTLR start "entryRuleTerm"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1276:1: entryRuleTerm returns [EObject current=null] : iv_ruleTerm= ruleTerm EOF ;
    public final EObject entryRuleTerm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTerm = null;


        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1277:2: (iv_ruleTerm= ruleTerm EOF )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1278:2: iv_ruleTerm= ruleTerm EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTermRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleTerm_in_entryRuleTerm2519);
            iv_ruleTerm=ruleTerm();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTerm; 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleTerm2529); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTerm"


    // $ANTLR start "ruleTerm"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1285:1: ruleTerm returns [EObject current=null] : (this_Value_0= ruleValue | ( ( ruleInstance )=>this_Instance_1= ruleInstance ) | this_ThisRef_2= ruleThisRef ) ;
    public final EObject ruleTerm() throws RecognitionException {
        EObject current = null;

        EObject this_Value_0 = null;

        EObject this_Instance_1 = null;

        EObject this_ThisRef_2 = null;


         enterRule(); 
            
        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1288:28: ( (this_Value_0= ruleValue | ( ( ruleInstance )=>this_Instance_1= ruleInstance ) | this_ThisRef_2= ruleThisRef ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1289:1: (this_Value_0= ruleValue | ( ( ruleInstance )=>this_Instance_1= ruleInstance ) | this_ThisRef_2= ruleThisRef )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1289:1: (this_Value_0= ruleValue | ( ( ruleInstance )=>this_Instance_1= ruleInstance ) | this_ThisRef_2= ruleThisRef )
            int alt13=3;
            switch ( input.LA(1) ) {
            case RULE_STRING:
            case RULE_INT:
            case 25:
                {
                alt13=1;
                }
                break;
            case 20:
            case 21:
                {
                alt13=2;
                }
                break;
            case 24:
                {
                alt13=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }

            switch (alt13) {
                case 1 :
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1290:2: this_Value_0= ruleValue
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getTermAccess().getValueParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleValue_in_ruleTerm2579);
                    this_Value_0=ruleValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Value_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1302:6: ( ( ruleInstance )=>this_Instance_1= ruleInstance )
                    {
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1302:6: ( ( ruleInstance )=>this_Instance_1= ruleInstance )
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1302:7: ( ruleInstance )=>this_Instance_1= ruleInstance
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getTermAccess().getInstanceParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleInstance_in_ruleTerm2615);
                    this_Instance_1=ruleInstance();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Instance_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }


                    }
                    break;
                case 3 :
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1316:2: this_ThisRef_2= ruleThisRef
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getTermAccess().getThisRefParserRuleCall_2()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleThisRef_in_ruleTerm2646);
                    this_ThisRef_2=ruleThisRef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ThisRef_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTerm"


    // $ANTLR start "entryRuleThisRef"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1335:1: entryRuleThisRef returns [EObject current=null] : iv_ruleThisRef= ruleThisRef EOF ;
    public final EObject entryRuleThisRef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleThisRef = null;


        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1336:2: (iv_ruleThisRef= ruleThisRef EOF )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1337:2: iv_ruleThisRef= ruleThisRef EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getThisRefRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleThisRef_in_entryRuleThisRef2681);
            iv_ruleThisRef=ruleThisRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleThisRef; 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleThisRef2691); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleThisRef"


    // $ANTLR start "ruleThisRef"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1344:1: ruleThisRef returns [EObject current=null] : ( () otherlv_1= 'this' ) ;
    public final EObject ruleThisRef() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;

         enterRule(); 
            
        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1347:28: ( ( () otherlv_1= 'this' ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1348:1: ( () otherlv_1= 'this' )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1348:1: ( () otherlv_1= 'this' )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1348:2: () otherlv_1= 'this'
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1348:2: ()
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1349:2: 
            {
            if ( state.backtracking==0 ) {
               
              	  /* */ 
              	
            }
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getThisRefAccess().getThisRefAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,24,FollowSets000.FOLLOW_24_in_ruleThisRef2740); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getThisRefAccess().getThisKeyword_1());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleThisRef"


    // $ANTLR start "entryRuleValue"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1369:1: entryRuleValue returns [EObject current=null] : iv_ruleValue= ruleValue EOF ;
    public final EObject entryRuleValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleValue = null;


        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1370:2: (iv_ruleValue= ruleValue EOF )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1371:2: iv_ruleValue= ruleValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleValue_in_entryRuleValue2776);
            iv_ruleValue=ruleValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleValue2786); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleValue"


    // $ANTLR start "ruleValue"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1378:1: ruleValue returns [EObject current=null] : (this_StringValue_0= ruleStringValue | this_IntValue_1= ruleIntValue | this_CollectionValue_2= ruleCollectionValue ) ;
    public final EObject ruleValue() throws RecognitionException {
        EObject current = null;

        EObject this_StringValue_0 = null;

        EObject this_IntValue_1 = null;

        EObject this_CollectionValue_2 = null;


         enterRule(); 
            
        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1381:28: ( (this_StringValue_0= ruleStringValue | this_IntValue_1= ruleIntValue | this_CollectionValue_2= ruleCollectionValue ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1382:1: (this_StringValue_0= ruleStringValue | this_IntValue_1= ruleIntValue | this_CollectionValue_2= ruleCollectionValue )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1382:1: (this_StringValue_0= ruleStringValue | this_IntValue_1= ruleIntValue | this_CollectionValue_2= ruleCollectionValue )
            int alt14=3;
            switch ( input.LA(1) ) {
            case RULE_STRING:
                {
                alt14=1;
                }
                break;
            case RULE_INT:
                {
                alt14=2;
                }
                break;
            case 25:
                {
                alt14=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }

            switch (alt14) {
                case 1 :
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1383:2: this_StringValue_0= ruleStringValue
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getStringValueParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleStringValue_in_ruleValue2836);
                    this_StringValue_0=ruleStringValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_StringValue_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1396:2: this_IntValue_1= ruleIntValue
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getIntValueParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleIntValue_in_ruleValue2866);
                    this_IntValue_1=ruleIntValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_IntValue_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1409:2: this_CollectionValue_2= ruleCollectionValue
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getCollectionValueParserRuleCall_2()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleCollectionValue_in_ruleValue2896);
                    this_CollectionValue_2=ruleCollectionValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_CollectionValue_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleValue"


    // $ANTLR start "entryRuleCollectionValue"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1428:1: entryRuleCollectionValue returns [EObject current=null] : iv_ruleCollectionValue= ruleCollectionValue EOF ;
    public final EObject entryRuleCollectionValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCollectionValue = null;


        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1429:2: (iv_ruleCollectionValue= ruleCollectionValue EOF )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1430:2: iv_ruleCollectionValue= ruleCollectionValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCollectionValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleCollectionValue_in_entryRuleCollectionValue2931);
            iv_ruleCollectionValue=ruleCollectionValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCollectionValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleCollectionValue2941); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCollectionValue"


    // $ANTLR start "ruleCollectionValue"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1437:1: ruleCollectionValue returns [EObject current=null] : (otherlv_0= '[' ( (lv_contents_1_0= ruleTerm ) ) (otherlv_2= ',' ( (lv_contents_3_0= ruleTerm ) ) )* otherlv_4= ']' ) ;
    public final EObject ruleCollectionValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_contents_1_0 = null;

        EObject lv_contents_3_0 = null;


         enterRule(); 
            
        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1440:28: ( (otherlv_0= '[' ( (lv_contents_1_0= ruleTerm ) ) (otherlv_2= ',' ( (lv_contents_3_0= ruleTerm ) ) )* otherlv_4= ']' ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1441:1: (otherlv_0= '[' ( (lv_contents_1_0= ruleTerm ) ) (otherlv_2= ',' ( (lv_contents_3_0= ruleTerm ) ) )* otherlv_4= ']' )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1441:1: (otherlv_0= '[' ( (lv_contents_1_0= ruleTerm ) ) (otherlv_2= ',' ( (lv_contents_3_0= ruleTerm ) ) )* otherlv_4= ']' )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1441:3: otherlv_0= '[' ( (lv_contents_1_0= ruleTerm ) ) (otherlv_2= ',' ( (lv_contents_3_0= ruleTerm ) ) )* otherlv_4= ']'
            {
            otherlv_0=(Token)match(input,25,FollowSets000.FOLLOW_25_in_ruleCollectionValue2978); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getCollectionValueAccess().getLeftSquareBracketKeyword_0());
                  
            }
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1445:1: ( (lv_contents_1_0= ruleTerm ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1446:1: (lv_contents_1_0= ruleTerm )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1446:1: (lv_contents_1_0= ruleTerm )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1447:3: lv_contents_1_0= ruleTerm
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getCollectionValueAccess().getContentsTermParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_ruleTerm_in_ruleCollectionValue2999);
            lv_contents_1_0=ruleTerm();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getCollectionValueRule());
              	        }
                     		add(
                     			current, 
                     			"contents",
                      		lv_contents_1_0, 
                      		"Term");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1463:2: (otherlv_2= ',' ( (lv_contents_3_0= ruleTerm ) ) )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==26) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1463:4: otherlv_2= ',' ( (lv_contents_3_0= ruleTerm ) )
            	    {
            	    otherlv_2=(Token)match(input,26,FollowSets000.FOLLOW_26_in_ruleCollectionValue3012); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_2, grammarAccess.getCollectionValueAccess().getCommaKeyword_2_0());
            	          
            	    }
            	    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1467:1: ( (lv_contents_3_0= ruleTerm ) )
            	    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1468:1: (lv_contents_3_0= ruleTerm )
            	    {
            	    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1468:1: (lv_contents_3_0= ruleTerm )
            	    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1469:3: lv_contents_3_0= ruleTerm
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getCollectionValueAccess().getContentsTermParserRuleCall_2_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_ruleTerm_in_ruleCollectionValue3033);
            	    lv_contents_3_0=ruleTerm();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getCollectionValueRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"contents",
            	              		lv_contents_3_0, 
            	              		"Term");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

            otherlv_4=(Token)match(input,27,FollowSets000.FOLLOW_27_in_ruleCollectionValue3047); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getCollectionValueAccess().getRightSquareBracketKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCollectionValue"


    // $ANTLR start "entryRuleStringValue"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1497:1: entryRuleStringValue returns [EObject current=null] : iv_ruleStringValue= ruleStringValue EOF ;
    public final EObject entryRuleStringValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringValue = null;


        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1498:2: (iv_ruleStringValue= ruleStringValue EOF )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1499:2: iv_ruleStringValue= ruleStringValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStringValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleStringValue_in_entryRuleStringValue3083);
            iv_ruleStringValue=ruleStringValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStringValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleStringValue3093); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringValue"


    // $ANTLR start "ruleStringValue"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1506:1: ruleStringValue returns [EObject current=null] : ( (lv_value_0_0= RULE_STRING ) ) ;
    public final EObject ruleStringValue() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;

         enterRule(); 
            
        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1509:28: ( ( (lv_value_0_0= RULE_STRING ) ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1510:1: ( (lv_value_0_0= RULE_STRING ) )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1510:1: ( (lv_value_0_0= RULE_STRING ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1511:1: (lv_value_0_0= RULE_STRING )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1511:1: (lv_value_0_0= RULE_STRING )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1512:3: lv_value_0_0= RULE_STRING
            {
            lv_value_0_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_ruleStringValue3134); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_0_0, grammarAccess.getStringValueAccess().getValueSTRINGTerminalRuleCall_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getStringValueRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_0_0, 
                      		"STRING");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringValue"


    // $ANTLR start "entryRuleIntValue"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1536:1: entryRuleIntValue returns [EObject current=null] : iv_ruleIntValue= ruleIntValue EOF ;
    public final EObject entryRuleIntValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntValue = null;


        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1537:2: (iv_ruleIntValue= ruleIntValue EOF )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1538:2: iv_ruleIntValue= ruleIntValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleIntValue_in_entryRuleIntValue3174);
            iv_ruleIntValue=ruleIntValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleIntValue3184); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntValue"


    // $ANTLR start "ruleIntValue"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1545:1: ruleIntValue returns [EObject current=null] : ( (lv_value_0_0= RULE_INT ) ) ;
    public final EObject ruleIntValue() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;

         enterRule(); 
            
        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1548:28: ( ( (lv_value_0_0= RULE_INT ) ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1549:1: ( (lv_value_0_0= RULE_INT ) )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1549:1: ( (lv_value_0_0= RULE_INT ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1550:1: (lv_value_0_0= RULE_INT )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1550:1: (lv_value_0_0= RULE_INT )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1551:3: lv_value_0_0= RULE_INT
            {
            lv_value_0_0=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_RULE_INT_in_ruleIntValue3225); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_0_0, grammarAccess.getIntValueAccess().getValueINTTerminalRuleCall_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getIntValueRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_0_0, 
                      		"INT");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntValue"


    // $ANTLR start "entryRuleQualifiedName"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1575:1: entryRuleQualifiedName returns [String current=null] : iv_ruleQualifiedName= ruleQualifiedName EOF ;
    public final String entryRuleQualifiedName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedName = null;


        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1576:2: (iv_ruleQualifiedName= ruleQualifiedName EOF )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1577:2: iv_ruleQualifiedName= ruleQualifiedName EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getQualifiedNameRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleQualifiedName_in_entryRuleQualifiedName3266);
            iv_ruleQualifiedName=ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleQualifiedName.getText(); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleQualifiedName3277); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1584:1: ruleQualifiedName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;

         enterRule(); 
            
        try {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1587:28: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1588:1: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1588:1: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1588:6: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_ruleQualifiedName3317); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		current.merge(this_ID_0);
                  
            }
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_ID_0, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 
                  
            }
            // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1595:1: (kw= '.' this_ID_2= RULE_ID )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==19) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1596:2: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,19,FollowSets000.FOLLOW_19_in_ruleQualifiedName3336); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	              current.merge(kw);
            	              newLeafNode(kw, grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 
            	          
            	    }
            	    this_ID_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_ruleQualifiedName3351); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      		current.merge(this_ID_2);
            	          
            	    }
            	    if ( state.backtracking==0 ) {
            	       
            	          newLeafNode(this_ID_2, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 
            	          
            	    }

            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedName"

    // $ANTLR start synpred3_InternalCorrespondence
    public final void synpred3_InternalCorrespondence_fragment() throws RecognitionException {   
        Token otherlv_2=null;
        EObject lv_left_1_0 = null;

        EObject lv_right_3_0 = null;


        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:358:3: ( ( () ( (lv_left_1_0= ruleClassFunction ) ) otherlv_2= '--' ( (lv_right_3_0= ruleClassFunction ) ) ) )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:358:3: ( () ( (lv_left_1_0= ruleClassFunction ) ) otherlv_2= '--' ( (lv_right_3_0= ruleClassFunction ) ) )
        {
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:358:3: ( () ( (lv_left_1_0= ruleClassFunction ) ) otherlv_2= '--' ( (lv_right_3_0= ruleClassFunction ) ) )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:358:4: () ( (lv_left_1_0= ruleClassFunction ) ) otherlv_2= '--' ( (lv_right_3_0= ruleClassFunction ) )
        {
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:358:4: ()
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:359:2: 
        {
        if ( state.backtracking==0 ) {
           
          	  /* */ 
          	
        }

        }

        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:367:2: ( (lv_left_1_0= ruleClassFunction ) )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:368:1: (lv_left_1_0= ruleClassFunction )
        {
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:368:1: (lv_left_1_0= ruleClassFunction )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:369:3: lv_left_1_0= ruleClassFunction
        {
        if ( state.backtracking==0 ) {
           
          	        newCompositeNode(grammarAccess.getClassCorrespondenceAccess().getLeftClassFunctionParserRuleCall_0_0_1_0()); 
          	    
        }
        pushFollow(FollowSets000.FOLLOW_ruleClassFunction_in_synpred3_InternalCorrespondence736);
        lv_left_1_0=ruleClassFunction();

        state._fsp--;
        if (state.failed) return ;

        }


        }

        otherlv_2=(Token)match(input,12,FollowSets000.FOLLOW_12_in_synpred3_InternalCorrespondence748); if (state.failed) return ;
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:389:1: ( (lv_right_3_0= ruleClassFunction ) )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:390:1: (lv_right_3_0= ruleClassFunction )
        {
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:390:1: (lv_right_3_0= ruleClassFunction )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:391:3: lv_right_3_0= ruleClassFunction
        {
        if ( state.backtracking==0 ) {
           
          	        newCompositeNode(grammarAccess.getClassCorrespondenceAccess().getRightClassFunctionParserRuleCall_0_0_3_0()); 
          	    
        }
        pushFollow(FollowSets000.FOLLOW_ruleClassFunction_in_synpred3_InternalCorrespondence769);
        lv_right_3_0=ruleClassFunction();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }


        }
    }
    // $ANTLR end synpred3_InternalCorrespondence

    // $ANTLR start synpred4_InternalCorrespondence
    public final void synpred4_InternalCorrespondence_fragment() throws RecognitionException {   
        Token otherlv_6=null;
        EObject lv_left_5_0 = null;

        EObject lv_right_7_0 = null;


        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:408:6: ( ( () ( (lv_left_5_0= ruleClassFunction ) ) otherlv_6= '==' ( (lv_right_7_0= ruleClassFunction ) ) ) )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:408:6: ( () ( (lv_left_5_0= ruleClassFunction ) ) otherlv_6= '==' ( (lv_right_7_0= ruleClassFunction ) ) )
        {
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:408:6: ( () ( (lv_left_5_0= ruleClassFunction ) ) otherlv_6= '==' ( (lv_right_7_0= ruleClassFunction ) ) )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:408:7: () ( (lv_left_5_0= ruleClassFunction ) ) otherlv_6= '==' ( (lv_right_7_0= ruleClassFunction ) )
        {
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:408:7: ()
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:409:2: 
        {
        if ( state.backtracking==0 ) {
           
          	  /* */ 
          	
        }

        }

        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:417:2: ( (lv_left_5_0= ruleClassFunction ) )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:418:1: (lv_left_5_0= ruleClassFunction )
        {
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:418:1: (lv_left_5_0= ruleClassFunction )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:419:3: lv_left_5_0= ruleClassFunction
        {
        if ( state.backtracking==0 ) {
           
          	        newCompositeNode(grammarAccess.getClassCorrespondenceAccess().getLeftClassFunctionParserRuleCall_0_1_1_0()); 
          	    
        }
        pushFollow(FollowSets000.FOLLOW_ruleClassFunction_in_synpred4_InternalCorrespondence810);
        lv_left_5_0=ruleClassFunction();

        state._fsp--;
        if (state.failed) return ;

        }


        }

        otherlv_6=(Token)match(input,15,FollowSets000.FOLLOW_15_in_synpred4_InternalCorrespondence822); if (state.failed) return ;
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:439:1: ( (lv_right_7_0= ruleClassFunction ) )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:440:1: (lv_right_7_0= ruleClassFunction )
        {
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:440:1: (lv_right_7_0= ruleClassFunction )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:441:3: lv_right_7_0= ruleClassFunction
        {
        if ( state.backtracking==0 ) {
           
          	        newCompositeNode(grammarAccess.getClassCorrespondenceAccess().getRightClassFunctionParserRuleCall_0_1_3_0()); 
          	    
        }
        pushFollow(FollowSets000.FOLLOW_ruleClassFunction_in_synpred4_InternalCorrespondence843);
        lv_right_7_0=ruleClassFunction();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }


        }
    }
    // $ANTLR end synpred4_InternalCorrespondence

    // $ANTLR start synpred5_InternalCorrespondence
    public final void synpred5_InternalCorrespondence_fragment() throws RecognitionException {   
        Token otherlv_10=null;
        EObject lv_left_9_0 = null;

        EObject lv_right_11_0 = null;


        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:458:6: ( ( () ( (lv_left_9_0= ruleClassFunction ) ) otherlv_10= '->' ( (lv_right_11_0= ruleClassFunction ) ) ) )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:458:6: ( () ( (lv_left_9_0= ruleClassFunction ) ) otherlv_10= '->' ( (lv_right_11_0= ruleClassFunction ) ) )
        {
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:458:6: ( () ( (lv_left_9_0= ruleClassFunction ) ) otherlv_10= '->' ( (lv_right_11_0= ruleClassFunction ) ) )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:458:7: () ( (lv_left_9_0= ruleClassFunction ) ) otherlv_10= '->' ( (lv_right_11_0= ruleClassFunction ) )
        {
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:458:7: ()
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:459:2: 
        {
        if ( state.backtracking==0 ) {
           
          	  /* */ 
          	
        }

        }

        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:467:2: ( (lv_left_9_0= ruleClassFunction ) )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:468:1: (lv_left_9_0= ruleClassFunction )
        {
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:468:1: (lv_left_9_0= ruleClassFunction )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:469:3: lv_left_9_0= ruleClassFunction
        {
        if ( state.backtracking==0 ) {
           
          	        newCompositeNode(grammarAccess.getClassCorrespondenceAccess().getLeftClassFunctionParserRuleCall_0_2_1_0()); 
          	    
        }
        pushFollow(FollowSets000.FOLLOW_ruleClassFunction_in_synpred5_InternalCorrespondence884);
        lv_left_9_0=ruleClassFunction();

        state._fsp--;
        if (state.failed) return ;

        }


        }

        otherlv_10=(Token)match(input,16,FollowSets000.FOLLOW_16_in_synpred5_InternalCorrespondence896); if (state.failed) return ;
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:489:1: ( (lv_right_11_0= ruleClassFunction ) )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:490:1: (lv_right_11_0= ruleClassFunction )
        {
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:490:1: (lv_right_11_0= ruleClassFunction )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:491:3: lv_right_11_0= ruleClassFunction
        {
        if ( state.backtracking==0 ) {
           
          	        newCompositeNode(grammarAccess.getClassCorrespondenceAccess().getRightClassFunctionParserRuleCall_0_2_3_0()); 
          	    
        }
        pushFollow(FollowSets000.FOLLOW_ruleClassFunction_in_synpred5_InternalCorrespondence917);
        lv_right_11_0=ruleClassFunction();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }


        }
    }
    // $ANTLR end synpred5_InternalCorrespondence

    // $ANTLR start synpred8_InternalCorrespondence
    public final void synpred8_InternalCorrespondence_fragment() throws RecognitionException {   
        Token otherlv_2=null;
        EObject lv_left_1_0 = null;

        EObject lv_right_3_0 = null;


        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:726:2: ( ( () ( (lv_left_1_0= ruleFeatureFunction ) ) otherlv_2= '--' ( (lv_right_3_0= ruleFeatureFunction ) ) ) )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:726:2: ( () ( (lv_left_1_0= ruleFeatureFunction ) ) otherlv_2= '--' ( (lv_right_3_0= ruleFeatureFunction ) ) )
        {
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:726:2: ( () ( (lv_left_1_0= ruleFeatureFunction ) ) otherlv_2= '--' ( (lv_right_3_0= ruleFeatureFunction ) ) )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:726:3: () ( (lv_left_1_0= ruleFeatureFunction ) ) otherlv_2= '--' ( (lv_right_3_0= ruleFeatureFunction ) )
        {
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:726:3: ()
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:727:2: 
        {
        if ( state.backtracking==0 ) {
           
          	  /* */ 
          	
        }

        }

        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:735:2: ( (lv_left_1_0= ruleFeatureFunction ) )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:736:1: (lv_left_1_0= ruleFeatureFunction )
        {
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:736:1: (lv_left_1_0= ruleFeatureFunction )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:737:3: lv_left_1_0= ruleFeatureFunction
        {
        if ( state.backtracking==0 ) {
           
          	        newCompositeNode(grammarAccess.getFeatureCorrespondenceAccess().getLeftFeatureFunctionParserRuleCall_0_1_0()); 
          	    
        }
        pushFollow(FollowSets000.FOLLOW_ruleFeatureFunction_in_synpred8_InternalCorrespondence1452);
        lv_left_1_0=ruleFeatureFunction();

        state._fsp--;
        if (state.failed) return ;

        }


        }

        otherlv_2=(Token)match(input,12,FollowSets000.FOLLOW_12_in_synpred8_InternalCorrespondence1464); if (state.failed) return ;
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:757:1: ( (lv_right_3_0= ruleFeatureFunction ) )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:758:1: (lv_right_3_0= ruleFeatureFunction )
        {
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:758:1: (lv_right_3_0= ruleFeatureFunction )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:759:3: lv_right_3_0= ruleFeatureFunction
        {
        if ( state.backtracking==0 ) {
           
          	        newCompositeNode(grammarAccess.getFeatureCorrespondenceAccess().getRightFeatureFunctionParserRuleCall_0_3_0()); 
          	    
        }
        pushFollow(FollowSets000.FOLLOW_ruleFeatureFunction_in_synpred8_InternalCorrespondence1485);
        lv_right_3_0=ruleFeatureFunction();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }


        }
    }
    // $ANTLR end synpred8_InternalCorrespondence

    // $ANTLR start synpred9_InternalCorrespondence
    public final void synpred9_InternalCorrespondence_fragment() throws RecognitionException {   
        Token otherlv_6=null;
        EObject lv_left_5_0 = null;

        EObject lv_right_7_0 = null;


        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:776:6: ( ( () ( (lv_left_5_0= ruleFeatureFunction ) ) otherlv_6= '==' ( (lv_right_7_0= ruleFeatureFunction ) ) ) )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:776:6: ( () ( (lv_left_5_0= ruleFeatureFunction ) ) otherlv_6= '==' ( (lv_right_7_0= ruleFeatureFunction ) ) )
        {
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:776:6: ( () ( (lv_left_5_0= ruleFeatureFunction ) ) otherlv_6= '==' ( (lv_right_7_0= ruleFeatureFunction ) ) )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:776:7: () ( (lv_left_5_0= ruleFeatureFunction ) ) otherlv_6= '==' ( (lv_right_7_0= ruleFeatureFunction ) )
        {
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:776:7: ()
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:777:2: 
        {
        if ( state.backtracking==0 ) {
           
          	  /* */ 
          	
        }

        }

        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:785:2: ( (lv_left_5_0= ruleFeatureFunction ) )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:786:1: (lv_left_5_0= ruleFeatureFunction )
        {
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:786:1: (lv_left_5_0= ruleFeatureFunction )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:787:3: lv_left_5_0= ruleFeatureFunction
        {
        if ( state.backtracking==0 ) {
           
          	        newCompositeNode(grammarAccess.getFeatureCorrespondenceAccess().getLeftFeatureFunctionParserRuleCall_1_1_0()); 
          	    
        }
        pushFollow(FollowSets000.FOLLOW_ruleFeatureFunction_in_synpred9_InternalCorrespondence1526);
        lv_left_5_0=ruleFeatureFunction();

        state._fsp--;
        if (state.failed) return ;

        }


        }

        otherlv_6=(Token)match(input,15,FollowSets000.FOLLOW_15_in_synpred9_InternalCorrespondence1538); if (state.failed) return ;
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:807:1: ( (lv_right_7_0= ruleFeatureFunction ) )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:808:1: (lv_right_7_0= ruleFeatureFunction )
        {
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:808:1: (lv_right_7_0= ruleFeatureFunction )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:809:3: lv_right_7_0= ruleFeatureFunction
        {
        if ( state.backtracking==0 ) {
           
          	        newCompositeNode(grammarAccess.getFeatureCorrespondenceAccess().getRightFeatureFunctionParserRuleCall_1_3_0()); 
          	    
        }
        pushFollow(FollowSets000.FOLLOW_ruleFeatureFunction_in_synpred9_InternalCorrespondence1559);
        lv_right_7_0=ruleFeatureFunction();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }


        }
    }
    // $ANTLR end synpred9_InternalCorrespondence

    // $ANTLR start synpred10_InternalCorrespondence
    public final void synpred10_InternalCorrespondence_fragment() throws RecognitionException {   
        Token otherlv_10=null;
        EObject lv_left_9_0 = null;

        EObject lv_right_11_0 = null;


        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:826:6: ( ( () ( (lv_left_9_0= ruleFeatureFunction ) ) otherlv_10= '->' ( (lv_right_11_0= ruleFeatureFunction ) ) ) )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:826:6: ( () ( (lv_left_9_0= ruleFeatureFunction ) ) otherlv_10= '->' ( (lv_right_11_0= ruleFeatureFunction ) ) )
        {
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:826:6: ( () ( (lv_left_9_0= ruleFeatureFunction ) ) otherlv_10= '->' ( (lv_right_11_0= ruleFeatureFunction ) ) )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:826:7: () ( (lv_left_9_0= ruleFeatureFunction ) ) otherlv_10= '->' ( (lv_right_11_0= ruleFeatureFunction ) )
        {
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:826:7: ()
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:827:2: 
        {
        if ( state.backtracking==0 ) {
           
          	  /* */ 
          	
        }

        }

        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:835:2: ( (lv_left_9_0= ruleFeatureFunction ) )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:836:1: (lv_left_9_0= ruleFeatureFunction )
        {
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:836:1: (lv_left_9_0= ruleFeatureFunction )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:837:3: lv_left_9_0= ruleFeatureFunction
        {
        if ( state.backtracking==0 ) {
           
          	        newCompositeNode(grammarAccess.getFeatureCorrespondenceAccess().getLeftFeatureFunctionParserRuleCall_2_1_0()); 
          	    
        }
        pushFollow(FollowSets000.FOLLOW_ruleFeatureFunction_in_synpred10_InternalCorrespondence1600);
        lv_left_9_0=ruleFeatureFunction();

        state._fsp--;
        if (state.failed) return ;

        }


        }

        otherlv_10=(Token)match(input,16,FollowSets000.FOLLOW_16_in_synpred10_InternalCorrespondence1612); if (state.failed) return ;
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:857:1: ( (lv_right_11_0= ruleFeatureFunction ) )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:858:1: (lv_right_11_0= ruleFeatureFunction )
        {
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:858:1: (lv_right_11_0= ruleFeatureFunction )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:859:3: lv_right_11_0= ruleFeatureFunction
        {
        if ( state.backtracking==0 ) {
           
          	        newCompositeNode(grammarAccess.getFeatureCorrespondenceAccess().getRightFeatureFunctionParserRuleCall_2_3_0()); 
          	    
        }
        pushFollow(FollowSets000.FOLLOW_ruleFeatureFunction_in_synpred10_InternalCorrespondence1633);
        lv_right_11_0=ruleFeatureFunction();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }


        }
    }
    // $ANTLR end synpred10_InternalCorrespondence

    // $ANTLR start synpred12_InternalCorrespondence
    public final void synpred12_InternalCorrespondence_fragment() throws RecognitionException {   
        EObject this_FeatureRef_0 = null;


        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1008:2: (this_FeatureRef_0= ruleFeatureRef )
        // ../correspondence.xtext/src-gen/correspondence/xtext/parser/antlr/internal/InternalCorrespondence.g:1008:2: this_FeatureRef_0= ruleFeatureRef
        {
        if ( state.backtracking==0 ) {
           
          	  /* */ 
          	
        }
        pushFollow(FollowSets000.FOLLOW_ruleFeatureRef_in_synpred12_InternalCorrespondence1932);
        this_FeatureRef_0=ruleFeatureRef();

        state._fsp--;
        if (state.failed) return ;

        }
    }
    // $ANTLR end synpred12_InternalCorrespondence

    // Delegated rules

    public final boolean synpred12_InternalCorrespondence() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred12_InternalCorrespondence_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred5_InternalCorrespondence() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred5_InternalCorrespondence_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred10_InternalCorrespondence() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred10_InternalCorrespondence_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred9_InternalCorrespondence() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred9_InternalCorrespondence_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred8_InternalCorrespondence() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred8_InternalCorrespondence_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred3_InternalCorrespondence() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred3_InternalCorrespondence_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred4_InternalCorrespondence() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred4_InternalCorrespondence_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


    protected DFA6 dfa6 = new DFA6(this);
    static final String DFA6_eotS =
        "\13\uffff";
    static final String DFA6_eofS =
        "\13\uffff";
    static final String DFA6_minS =
        "\1\4\6\0\4\uffff";
    static final String DFA6_maxS =
        "\1\31\6\0\4\uffff";
    static final String DFA6_acceptS =
        "\7\uffff\1\1\1\2\1\3\1\4";
    static final String DFA6_specialS =
        "\1\uffff\1\0\1\1\1\2\1\3\1\4\1\5\4\uffff}>";
    static final String[] DFA6_transitionS = {
            "\1\4\1\3\1\5\15\uffff\1\1\1\2\3\uffff\1\6",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA6_eot = DFA.unpackEncodedString(DFA6_eotS);
    static final short[] DFA6_eof = DFA.unpackEncodedString(DFA6_eofS);
    static final char[] DFA6_min = DFA.unpackEncodedStringToUnsignedChars(DFA6_minS);
    static final char[] DFA6_max = DFA.unpackEncodedStringToUnsignedChars(DFA6_maxS);
    static final short[] DFA6_accept = DFA.unpackEncodedString(DFA6_acceptS);
    static final short[] DFA6_special = DFA.unpackEncodedString(DFA6_specialS);
    static final short[][] DFA6_transition;

    static {
        int numStates = DFA6_transitionS.length;
        DFA6_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA6_transition[i] = DFA.unpackEncodedString(DFA6_transitionS[i]);
        }
    }

    class DFA6 extends DFA {

        public DFA6(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 6;
            this.eot = DFA6_eot;
            this.eof = DFA6_eof;
            this.min = DFA6_min;
            this.max = DFA6_max;
            this.accept = DFA6_accept;
            this.special = DFA6_special;
            this.transition = DFA6_transition;
        }
        public String getDescription() {
            return "726:1: ( ( () ( (lv_left_1_0= ruleFeatureFunction ) ) otherlv_2= '--' ( (lv_right_3_0= ruleFeatureFunction ) ) ) | ( () ( (lv_left_5_0= ruleFeatureFunction ) ) otherlv_6= '==' ( (lv_right_7_0= ruleFeatureFunction ) ) ) | ( () ( (lv_left_9_0= ruleFeatureFunction ) ) otherlv_10= '->' ( (lv_right_11_0= ruleFeatureFunction ) ) ) | ( () ( (lv_left_13_0= ruleFeatureFunction ) ) otherlv_14= '<-' ( (lv_right_15_0= ruleFeatureFunction ) ) ) )";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA6_1 = input.LA(1);

                         
                        int index6_1 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred8_InternalCorrespondence()) ) {s = 7;}

                        else if ( (synpred9_InternalCorrespondence()) ) {s = 8;}

                        else if ( (synpred10_InternalCorrespondence()) ) {s = 9;}

                        else if ( (true) ) {s = 10;}

                         
                        input.seek(index6_1);
                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA6_2 = input.LA(1);

                         
                        int index6_2 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred8_InternalCorrespondence()) ) {s = 7;}

                        else if ( (synpred9_InternalCorrespondence()) ) {s = 8;}

                        else if ( (synpred10_InternalCorrespondence()) ) {s = 9;}

                        else if ( (true) ) {s = 10;}

                         
                        input.seek(index6_2);
                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA6_3 = input.LA(1);

                         
                        int index6_3 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred8_InternalCorrespondence()) ) {s = 7;}

                        else if ( (synpred9_InternalCorrespondence()) ) {s = 8;}

                        else if ( (synpred10_InternalCorrespondence()) ) {s = 9;}

                        else if ( (true) ) {s = 10;}

                         
                        input.seek(index6_3);
                        if ( s>=0 ) return s;
                        break;
                    case 3 : 
                        int LA6_4 = input.LA(1);

                         
                        int index6_4 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred8_InternalCorrespondence()) ) {s = 7;}

                        else if ( (synpred9_InternalCorrespondence()) ) {s = 8;}

                        else if ( (synpred10_InternalCorrespondence()) ) {s = 9;}

                        else if ( (true) ) {s = 10;}

                         
                        input.seek(index6_4);
                        if ( s>=0 ) return s;
                        break;
                    case 4 : 
                        int LA6_5 = input.LA(1);

                         
                        int index6_5 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred8_InternalCorrespondence()) ) {s = 7;}

                        else if ( (synpred9_InternalCorrespondence()) ) {s = 8;}

                        else if ( (synpred10_InternalCorrespondence()) ) {s = 9;}

                        else if ( (true) ) {s = 10;}

                         
                        input.seek(index6_5);
                        if ( s>=0 ) return s;
                        break;
                    case 5 : 
                        int LA6_6 = input.LA(1);

                         
                        int index6_6 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred8_InternalCorrespondence()) ) {s = 7;}

                        else if ( (synpred9_InternalCorrespondence()) ) {s = 8;}

                        else if ( (synpred10_InternalCorrespondence()) ) {s = 9;}

                        else if ( (true) ) {s = 10;}

                         
                        input.seek(index6_6);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 6, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_ruleCorrespondences_in_entryRuleCorrespondences81 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleCorrespondences91 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleImport_in_ruleCorrespondences137 = new BitSet(new long[]{0x0000000000000820L});
        public static final BitSet FOLLOW_ruleModelCorrespondence_in_ruleCorrespondences159 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleImport_in_entryRuleImport195 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleImport205 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_11_in_ruleImport242 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_RULE_STRING_in_ruleImport259 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleModelCorrespondence_in_entryRuleModelCorrespondence300 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleModelCorrespondence310 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleModelFunction_in_ruleModelCorrespondence368 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_12_in_ruleModelCorrespondence380 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_ruleModelFunction_in_ruleModelCorrespondence401 = new BitSet(new long[]{0x0000000000002000L});
        public static final BitSet FOLLOW_13_in_ruleModelCorrespondence413 = new BitSet(new long[]{0x0000000000304020L});
        public static final BitSet FOLLOW_ruleClassCorrespondence_in_ruleModelCorrespondence434 = new BitSet(new long[]{0x0000000000304020L});
        public static final BitSet FOLLOW_14_in_ruleModelCorrespondence447 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleModelFunction_in_entryRuleModelFunction483 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleModelFunction493 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rulePackageRef_in_ruleModelFunction538 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rulePackageRef_in_entryRulePackageRef573 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRulePackageRef583 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rulePackageRef631 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClassCorrespondence_in_entryRuleClassCorrespondence666 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleClassCorrespondence676 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClassFunction_in_ruleClassCorrespondence736 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_12_in_ruleClassCorrespondence748 = new BitSet(new long[]{0x0000000000300020L});
        public static final BitSet FOLLOW_ruleClassFunction_in_ruleClassCorrespondence769 = new BitSet(new long[]{0x0000000000002000L});
        public static final BitSet FOLLOW_ruleClassFunction_in_ruleClassCorrespondence810 = new BitSet(new long[]{0x0000000000008000L});
        public static final BitSet FOLLOW_15_in_ruleClassCorrespondence822 = new BitSet(new long[]{0x0000000000300020L});
        public static final BitSet FOLLOW_ruleClassFunction_in_ruleClassCorrespondence843 = new BitSet(new long[]{0x0000000000002000L});
        public static final BitSet FOLLOW_ruleClassFunction_in_ruleClassCorrespondence884 = new BitSet(new long[]{0x0000000000010000L});
        public static final BitSet FOLLOW_16_in_ruleClassCorrespondence896 = new BitSet(new long[]{0x0000000000300020L});
        public static final BitSet FOLLOW_ruleClassFunction_in_ruleClassCorrespondence917 = new BitSet(new long[]{0x0000000000002000L});
        public static final BitSet FOLLOW_ruleClassFunction_in_ruleClassCorrespondence958 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_17_in_ruleClassCorrespondence970 = new BitSet(new long[]{0x0000000000300020L});
        public static final BitSet FOLLOW_ruleClassFunction_in_ruleClassCorrespondence991 = new BitSet(new long[]{0x0000000000002000L});
        public static final BitSet FOLLOW_13_in_ruleClassCorrespondence1005 = new BitSet(new long[]{0x0000000002304070L});
        public static final BitSet FOLLOW_ruleFeatureCorrespondence_in_ruleClassCorrespondence1026 = new BitSet(new long[]{0x0000000002304070L});
        public static final BitSet FOLLOW_14_in_ruleClassCorrespondence1039 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClassFunction_in_entryRuleClassFunction1075 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleClassFunction1085 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClassTerm_in_ruleClassFunction1130 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClassTerm_in_entryRuleClassTerm1165 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleClassTerm1175 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClassRef_in_ruleClassTerm1225 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleInstance_in_ruleClassTerm1255 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClassRef_in_entryRuleClassRef1290 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleClassRef1300 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_ruleClassRef1348 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureCorrespondence_in_entryRuleFeatureCorrespondence1383 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFeatureCorrespondence1393 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureFunction_in_ruleFeatureCorrespondence1452 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_12_in_ruleFeatureCorrespondence1464 = new BitSet(new long[]{0x0000000002300070L});
        public static final BitSet FOLLOW_ruleFeatureFunction_in_ruleFeatureCorrespondence1485 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureFunction_in_ruleFeatureCorrespondence1526 = new BitSet(new long[]{0x0000000000008000L});
        public static final BitSet FOLLOW_15_in_ruleFeatureCorrespondence1538 = new BitSet(new long[]{0x0000000002300070L});
        public static final BitSet FOLLOW_ruleFeatureFunction_in_ruleFeatureCorrespondence1559 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureFunction_in_ruleFeatureCorrespondence1600 = new BitSet(new long[]{0x0000000000010000L});
        public static final BitSet FOLLOW_16_in_ruleFeatureCorrespondence1612 = new BitSet(new long[]{0x0000000002300070L});
        public static final BitSet FOLLOW_ruleFeatureFunction_in_ruleFeatureCorrespondence1633 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureFunction_in_ruleFeatureCorrespondence1674 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_17_in_ruleFeatureCorrespondence1686 = new BitSet(new long[]{0x0000000002300070L});
        public static final BitSet FOLLOW_ruleFeatureFunction_in_ruleFeatureCorrespondence1707 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureFunction_in_entryRuleFeatureFunction1744 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFeatureFunction1754 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureTerm_in_ruleFeatureFunction1800 = new BitSet(new long[]{0x0000000000040002L});
        public static final BitSet FOLLOW_18_in_ruleFeatureFunction1813 = new BitSet(new long[]{0x0000000002300070L});
        public static final BitSet FOLLOW_ruleFeatureTerm_in_ruleFeatureFunction1834 = new BitSet(new long[]{0x0000000000040002L});
        public static final BitSet FOLLOW_ruleFeatureTerm_in_entryRuleFeatureTerm1872 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFeatureTerm1882 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureRef_in_ruleFeatureTerm1932 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleValue_in_ruleFeatureTerm1962 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleInstance_in_ruleFeatureTerm1992 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureRef_in_entryRuleFeatureRef2027 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFeatureRef2037 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleInstance_in_ruleFeatureRef2084 = new BitSet(new long[]{0x0000000000080000L});
        public static final BitSet FOLLOW_19_in_ruleFeatureRef2096 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_RULE_ID_in_ruleFeatureRef2122 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleInstance_in_entryRuleInstance2158 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleInstance2168 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_20_in_ruleInstance2219 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_21_in_ruleInstance2251 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_RULE_ID_in_ruleInstance2277 = new BitSet(new long[]{0x0000000000402000L});
        public static final BitSet FOLLOW_22_in_ruleInstance2295 = new BitSet(new long[]{0x0000000000002000L});
        public static final BitSet FOLLOW_13_in_ruleInstance2321 = new BitSet(new long[]{0x0000000000004020L});
        public static final BitSet FOLLOW_ruleAssignment_in_ruleInstance2342 = new BitSet(new long[]{0x0000000000004020L});
        public static final BitSet FOLLOW_14_in_ruleInstance2355 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAssignment_in_entryRuleAssignment2391 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleAssignment2401 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_ruleAssignment2450 = new BitSet(new long[]{0x0000000000800000L});
        public static final BitSet FOLLOW_23_in_ruleAssignment2462 = new BitSet(new long[]{0x0000000003300070L});
        public static final BitSet FOLLOW_ruleTerm_in_ruleAssignment2483 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleTerm_in_entryRuleTerm2519 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleTerm2529 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleValue_in_ruleTerm2579 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleInstance_in_ruleTerm2615 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleThisRef_in_ruleTerm2646 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleThisRef_in_entryRuleThisRef2681 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleThisRef2691 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_24_in_ruleThisRef2740 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleValue_in_entryRuleValue2776 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleValue2786 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleStringValue_in_ruleValue2836 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleIntValue_in_ruleValue2866 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleCollectionValue_in_ruleValue2896 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleCollectionValue_in_entryRuleCollectionValue2931 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleCollectionValue2941 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_25_in_ruleCollectionValue2978 = new BitSet(new long[]{0x0000000003300070L});
        public static final BitSet FOLLOW_ruleTerm_in_ruleCollectionValue2999 = new BitSet(new long[]{0x000000000C000000L});
        public static final BitSet FOLLOW_26_in_ruleCollectionValue3012 = new BitSet(new long[]{0x0000000003300070L});
        public static final BitSet FOLLOW_ruleTerm_in_ruleCollectionValue3033 = new BitSet(new long[]{0x000000000C000000L});
        public static final BitSet FOLLOW_27_in_ruleCollectionValue3047 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleStringValue_in_entryRuleStringValue3083 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleStringValue3093 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_STRING_in_ruleStringValue3134 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleIntValue_in_entryRuleIntValue3174 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleIntValue3184 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_INT_in_ruleIntValue3225 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleQualifiedName_in_entryRuleQualifiedName3266 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleQualifiedName3277 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_ruleQualifiedName3317 = new BitSet(new long[]{0x0000000000080002L});
        public static final BitSet FOLLOW_19_in_ruleQualifiedName3336 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_RULE_ID_in_ruleQualifiedName3351 = new BitSet(new long[]{0x0000000000080002L});
        public static final BitSet FOLLOW_ruleClassFunction_in_synpred3_InternalCorrespondence736 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_12_in_synpred3_InternalCorrespondence748 = new BitSet(new long[]{0x0000000000300020L});
        public static final BitSet FOLLOW_ruleClassFunction_in_synpred3_InternalCorrespondence769 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClassFunction_in_synpred4_InternalCorrespondence810 = new BitSet(new long[]{0x0000000000008000L});
        public static final BitSet FOLLOW_15_in_synpred4_InternalCorrespondence822 = new BitSet(new long[]{0x0000000000300020L});
        public static final BitSet FOLLOW_ruleClassFunction_in_synpred4_InternalCorrespondence843 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClassFunction_in_synpred5_InternalCorrespondence884 = new BitSet(new long[]{0x0000000000010000L});
        public static final BitSet FOLLOW_16_in_synpred5_InternalCorrespondence896 = new BitSet(new long[]{0x0000000000300020L});
        public static final BitSet FOLLOW_ruleClassFunction_in_synpred5_InternalCorrespondence917 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureFunction_in_synpred8_InternalCorrespondence1452 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_12_in_synpred8_InternalCorrespondence1464 = new BitSet(new long[]{0x0000000002300070L});
        public static final BitSet FOLLOW_ruleFeatureFunction_in_synpred8_InternalCorrespondence1485 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureFunction_in_synpred9_InternalCorrespondence1526 = new BitSet(new long[]{0x0000000000008000L});
        public static final BitSet FOLLOW_15_in_synpred9_InternalCorrespondence1538 = new BitSet(new long[]{0x0000000002300070L});
        public static final BitSet FOLLOW_ruleFeatureFunction_in_synpred9_InternalCorrespondence1559 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureFunction_in_synpred10_InternalCorrespondence1600 = new BitSet(new long[]{0x0000000000010000L});
        public static final BitSet FOLLOW_16_in_synpred10_InternalCorrespondence1612 = new BitSet(new long[]{0x0000000002300070L});
        public static final BitSet FOLLOW_ruleFeatureFunction_in_synpred10_InternalCorrespondence1633 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureRef_in_synpred12_InternalCorrespondence1932 = new BitSet(new long[]{0x0000000000000002L});
    }


}