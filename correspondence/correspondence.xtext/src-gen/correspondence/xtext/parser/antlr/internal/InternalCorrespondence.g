/*
 * generated by Xtext
 */
grammar InternalCorrespondence;

options {
	superClass=AbstractInternalAntlrParser;
	backtrack=true;
	
}

@lexer::header {
package correspondence.xtext.parser.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.parser.antlr.Lexer;
}

@parser::header {
package correspondence.xtext.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import correspondence.xtext.services.CorrespondenceGrammarAccess;

}

@parser::members {

/*
  This grammar contains a lot of empty actions to work around a bug in ANTLR.
  Otherwise the ANTLR tool will create synpreds that cannot be compiled in some rare cases.
*/
 
 	private CorrespondenceGrammarAccess grammarAccess;
 	
    public InternalCorrespondenceParser(TokenStream input, CorrespondenceGrammarAccess grammarAccess) {
        this(input);
        this.grammarAccess = grammarAccess;
        registerRules(grammarAccess.getGrammar());
    }
    
    @Override
    protected String getFirstRuleName() {
    	return "Correspondences";	
   	}
   	
   	@Override
   	protected CorrespondenceGrammarAccess getGrammarAccess() {
   		return grammarAccess;
   	}
}

@rulecatch { 
    catch (RecognitionException re) { 
        recover(input,re); 
        appendSkippedTokens();
    } 
}




// Entry rule entryRuleCorrespondences
entryRuleCorrespondences returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getCorrespondencesRule()); }
	 iv_ruleCorrespondences=ruleCorrespondences 
	 { $current=$iv_ruleCorrespondences.current; } 
	 EOF 
;

// Rule Correspondences
ruleCorrespondences returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
(
		{ 
	        newCompositeNode(grammarAccess.getCorrespondencesAccess().getImportsImportParserRuleCall_0_0()); 
	    }
		lv_imports_0_0=ruleImport		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getCorrespondencesRule());
	        }
       		add(
       			$current, 
       			"imports",
        		lv_imports_0_0, 
        		"Import");
	        afterParserOrEnumRuleCall();
	    }

)
)*(
(
		{ 
	        newCompositeNode(grammarAccess.getCorrespondencesAccess().getCorrespondencesModelCorrespondenceParserRuleCall_1_0()); 
	    }
		lv_correspondences_1_0=ruleModelCorrespondence		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getCorrespondencesRule());
	        }
       		add(
       			$current, 
       			"correspondences",
        		lv_correspondences_1_0, 
        		"ModelCorrespondence");
	        afterParserOrEnumRuleCall();
	    }

)
))
;





// Entry rule entryRuleImport
entryRuleImport returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getImportRule()); }
	 iv_ruleImport=ruleImport 
	 { $current=$iv_ruleImport.current; } 
	 EOF 
;

// Rule Import
ruleImport returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(	otherlv_0='import' 
    {
    	newLeafNode(otherlv_0, grammarAccess.getImportAccess().getImportKeyword_0());
    }
(
(
		lv_importURI_1_0=RULE_STRING
		{
			newLeafNode(lv_importURI_1_0, grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getImportRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"importURI",
        		lv_importURI_1_0, 
        		"STRING");
	    }

)
))
;





// Entry rule entryRuleModelCorrespondence
entryRuleModelCorrespondence returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getModelCorrespondenceRule()); }
	 iv_ruleModelCorrespondence=ruleModelCorrespondence 
	 { $current=$iv_ruleModelCorrespondence.current; } 
	 EOF 
;

// Rule ModelCorrespondence
ruleModelCorrespondence returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
	{ 
	  /* */ 
	}
    {
        $current = forceCreateModelElement(
            grammarAccess.getModelCorrespondenceAccess().getBidirectionalCorrespondenceAction_0(),
            $current);
    }
)(
(
		{ 
	        newCompositeNode(grammarAccess.getModelCorrespondenceAccess().getLeftModelFunctionParserRuleCall_1_0()); 
	    }
		lv_left_1_0=ruleModelFunction		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getModelCorrespondenceRule());
	        }
       		set(
       			$current, 
       			"left",
        		lv_left_1_0, 
        		"ModelFunction");
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_2='--' 
    {
    	newLeafNode(otherlv_2, grammarAccess.getModelCorrespondenceAccess().getHyphenMinusHyphenMinusKeyword_2());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getModelCorrespondenceAccess().getRightModelFunctionParserRuleCall_3_0()); 
	    }
		lv_right_3_0=ruleModelFunction		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getModelCorrespondenceRule());
	        }
       		set(
       			$current, 
       			"right",
        		lv_right_3_0, 
        		"ModelFunction");
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_4='{' 
    {
    	newLeafNode(otherlv_4, grammarAccess.getModelCorrespondenceAccess().getLeftCurlyBracketKeyword_4());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getModelCorrespondenceAccess().getChildrenClassCorrespondenceParserRuleCall_5_0()); 
	    }
		lv_children_5_0=ruleClassCorrespondence		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getModelCorrespondenceRule());
	        }
       		add(
       			$current, 
       			"children",
        		lv_children_5_0, 
        		"ClassCorrespondence");
	        afterParserOrEnumRuleCall();
	    }

)
)*	otherlv_6='}' 
    {
    	newLeafNode(otherlv_6, grammarAccess.getModelCorrespondenceAccess().getRightCurlyBracketKeyword_6());
    }
)
;





// Entry rule entryRuleModelFunction
entryRuleModelFunction returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getModelFunctionRule()); }
	 iv_ruleModelFunction=ruleModelFunction 
	 { $current=$iv_ruleModelFunction.current; } 
	 EOF 
;

// Rule ModelFunction
ruleModelFunction returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
(
		{ 
	        newCompositeNode(grammarAccess.getModelFunctionAccess().getTermsPackageRefParserRuleCall_0()); 
	    }
		lv_terms_0_0=rulePackageRef		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getModelFunctionRule());
	        }
       		add(
       			$current, 
       			"terms",
        		lv_terms_0_0, 
        		"PackageRef");
	        afterParserOrEnumRuleCall();
	    }

)
)
;





// Entry rule entryRulePackageRef
entryRulePackageRef returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getPackageRefRule()); }
	 iv_rulePackageRef=rulePackageRef 
	 { $current=$iv_rulePackageRef.current; } 
	 EOF 
;

// Rule PackageRef
rulePackageRef returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
(
		{ 
		  /* */ 
		}
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getPackageRefRule());
	        }
        }
	otherlv_0=RULE_ID
	{
		newLeafNode(otherlv_0, grammarAccess.getPackageRefAccess().getReferencedPackageEPackageCrossReference_0()); 
	}

)
)
;





// Entry rule entryRuleClassCorrespondence
entryRuleClassCorrespondence returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getClassCorrespondenceRule()); }
	 iv_ruleClassCorrespondence=ruleClassCorrespondence 
	 { $current=$iv_ruleClassCorrespondence.current; } 
	 EOF 
;

// Rule ClassCorrespondence
ruleClassCorrespondence returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((((
	{ 
	  /* */ 
	}
    {
        $current = forceCreateModelElement(
            grammarAccess.getClassCorrespondenceAccess().getBidirectionalCorrespondenceAction_0_0_0(),
            $current);
    }
)(
(
		{ 
	        newCompositeNode(grammarAccess.getClassCorrespondenceAccess().getLeftClassFunctionParserRuleCall_0_0_1_0()); 
	    }
		lv_left_1_0=ruleClassFunction		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getClassCorrespondenceRule());
	        }
       		set(
       			$current, 
       			"left",
        		lv_left_1_0, 
        		"ClassFunction");
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_2='--' 
    {
    	newLeafNode(otherlv_2, grammarAccess.getClassCorrespondenceAccess().getHyphenMinusHyphenMinusKeyword_0_0_2());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getClassCorrespondenceAccess().getRightClassFunctionParserRuleCall_0_0_3_0()); 
	    }
		lv_right_3_0=ruleClassFunction		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getClassCorrespondenceRule());
	        }
       		set(
       			$current, 
       			"right",
        		lv_right_3_0, 
        		"ClassFunction");
	        afterParserOrEnumRuleCall();
	    }

)
))
    |((
	{ 
	  /* */ 
	}
    {
        $current = forceCreateModelElement(
            grammarAccess.getClassCorrespondenceAccess().getOneToOneCorrespondenceAction_0_1_0(),
            $current);
    }
)(
(
		{ 
	        newCompositeNode(grammarAccess.getClassCorrespondenceAccess().getLeftClassFunctionParserRuleCall_0_1_1_0()); 
	    }
		lv_left_5_0=ruleClassFunction		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getClassCorrespondenceRule());
	        }
       		set(
       			$current, 
       			"left",
        		lv_left_5_0, 
        		"ClassFunction");
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_6='==' 
    {
    	newLeafNode(otherlv_6, grammarAccess.getClassCorrespondenceAccess().getEqualsSignEqualsSignKeyword_0_1_2());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getClassCorrespondenceAccess().getRightClassFunctionParserRuleCall_0_1_3_0()); 
	    }
		lv_right_7_0=ruleClassFunction		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getClassCorrespondenceRule());
	        }
       		set(
       			$current, 
       			"right",
        		lv_right_7_0, 
        		"ClassFunction");
	        afterParserOrEnumRuleCall();
	    }

)
))
    |((
	{ 
	  /* */ 
	}
    {
        $current = forceCreateModelElement(
            grammarAccess.getClassCorrespondenceAccess().getLeftToRightCorrespondenceAction_0_2_0(),
            $current);
    }
)(
(
		{ 
	        newCompositeNode(grammarAccess.getClassCorrespondenceAccess().getLeftClassFunctionParserRuleCall_0_2_1_0()); 
	    }
		lv_left_9_0=ruleClassFunction		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getClassCorrespondenceRule());
	        }
       		set(
       			$current, 
       			"left",
        		lv_left_9_0, 
        		"ClassFunction");
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_10='->' 
    {
    	newLeafNode(otherlv_10, grammarAccess.getClassCorrespondenceAccess().getHyphenMinusGreaterThanSignKeyword_0_2_2());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getClassCorrespondenceAccess().getRightClassFunctionParserRuleCall_0_2_3_0()); 
	    }
		lv_right_11_0=ruleClassFunction		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getClassCorrespondenceRule());
	        }
       		set(
       			$current, 
       			"right",
        		lv_right_11_0, 
        		"ClassFunction");
	        afterParserOrEnumRuleCall();
	    }

)
))
    |((
	{ 
	  /* */ 
	}
    {
        $current = forceCreateModelElement(
            grammarAccess.getClassCorrespondenceAccess().getRightToLeftCorrespondenceAction_0_3_0(),
            $current);
    }
)(
(
		{ 
	        newCompositeNode(grammarAccess.getClassCorrespondenceAccess().getLeftClassFunctionParserRuleCall_0_3_1_0()); 
	    }
		lv_left_13_0=ruleClassFunction		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getClassCorrespondenceRule());
	        }
       		set(
       			$current, 
       			"left",
        		lv_left_13_0, 
        		"ClassFunction");
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_14='<-' 
    {
    	newLeafNode(otherlv_14, grammarAccess.getClassCorrespondenceAccess().getLessThanSignHyphenMinusKeyword_0_3_2());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getClassCorrespondenceAccess().getRightClassFunctionParserRuleCall_0_3_3_0()); 
	    }
		lv_right_15_0=ruleClassFunction		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getClassCorrespondenceRule());
	        }
       		set(
       			$current, 
       			"right",
        		lv_right_15_0, 
        		"ClassFunction");
	        afterParserOrEnumRuleCall();
	    }

)
)))	otherlv_16='{' 
    {
    	newLeafNode(otherlv_16, grammarAccess.getClassCorrespondenceAccess().getLeftCurlyBracketKeyword_1());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getClassCorrespondenceAccess().getChildrenFeatureCorrespondenceParserRuleCall_2_0()); 
	    }
		lv_children_17_0=ruleFeatureCorrespondence		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getClassCorrespondenceRule());
	        }
       		add(
       			$current, 
       			"children",
        		lv_children_17_0, 
        		"FeatureCorrespondence");
	        afterParserOrEnumRuleCall();
	    }

)
)*	otherlv_18='}' 
    {
    	newLeafNode(otherlv_18, grammarAccess.getClassCorrespondenceAccess().getRightCurlyBracketKeyword_3());
    }
)
;





// Entry rule entryRuleClassFunction
entryRuleClassFunction returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getClassFunctionRule()); }
	 iv_ruleClassFunction=ruleClassFunction 
	 { $current=$iv_ruleClassFunction.current; } 
	 EOF 
;

// Rule ClassFunction
ruleClassFunction returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
(
		{ 
	        newCompositeNode(grammarAccess.getClassFunctionAccess().getTermsClassTermParserRuleCall_0()); 
	    }
		lv_terms_0_0=ruleClassTerm		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getClassFunctionRule());
	        }
       		add(
       			$current, 
       			"terms",
        		lv_terms_0_0, 
        		"ClassTerm");
	        afterParserOrEnumRuleCall();
	    }

)
)
;





// Entry rule entryRuleClassTerm
entryRuleClassTerm returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getClassTermRule()); }
	 iv_ruleClassTerm=ruleClassTerm 
	 { $current=$iv_ruleClassTerm.current; } 
	 EOF 
;

// Rule ClassTerm
ruleClassTerm returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
	{ 
	  /* */ 
	}
    { 
        newCompositeNode(grammarAccess.getClassTermAccess().getClassRefParserRuleCall_0()); 
    }
    this_ClassRef_0=ruleClassRef
    { 
        $current = $this_ClassRef_0.current; 
        afterParserOrEnumRuleCall();
    }

    |
	{ 
	  /* */ 
	}
    { 
        newCompositeNode(grammarAccess.getClassTermAccess().getInstanceParserRuleCall_1()); 
    }
    this_Instance_1=ruleInstance
    { 
        $current = $this_Instance_1.current; 
        afterParserOrEnumRuleCall();
    }
)
;





// Entry rule entryRuleClassRef
entryRuleClassRef returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getClassRefRule()); }
	 iv_ruleClassRef=ruleClassRef 
	 { $current=$iv_ruleClassRef.current; } 
	 EOF 
;

// Rule ClassRef
ruleClassRef returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
(
		{ 
		  /* */ 
		}
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getClassRefRule());
	        }
        }
	otherlv_0=RULE_ID
	{
		newLeafNode(otherlv_0, grammarAccess.getClassRefAccess().getReferencedClassEClassCrossReference_0()); 
	}

)
)
;





// Entry rule entryRuleFeatureCorrespondence
entryRuleFeatureCorrespondence returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getFeatureCorrespondenceRule()); }
	 iv_ruleFeatureCorrespondence=ruleFeatureCorrespondence 
	 { $current=$iv_ruleFeatureCorrespondence.current; } 
	 EOF 
;

// Rule FeatureCorrespondence
ruleFeatureCorrespondence returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(((
	{ 
	  /* */ 
	}
    {
        $current = forceCreateModelElement(
            grammarAccess.getFeatureCorrespondenceAccess().getBidirectionalCorrespondenceAction_0_0(),
            $current);
    }
)(
(
		{ 
	        newCompositeNode(grammarAccess.getFeatureCorrespondenceAccess().getLeftFeatureFunctionParserRuleCall_0_1_0()); 
	    }
		lv_left_1_0=ruleFeatureFunction		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getFeatureCorrespondenceRule());
	        }
       		set(
       			$current, 
       			"left",
        		lv_left_1_0, 
        		"FeatureFunction");
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_2='--' 
    {
    	newLeafNode(otherlv_2, grammarAccess.getFeatureCorrespondenceAccess().getHyphenMinusHyphenMinusKeyword_0_2());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getFeatureCorrespondenceAccess().getRightFeatureFunctionParserRuleCall_0_3_0()); 
	    }
		lv_right_3_0=ruleFeatureFunction		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getFeatureCorrespondenceRule());
	        }
       		set(
       			$current, 
       			"right",
        		lv_right_3_0, 
        		"FeatureFunction");
	        afterParserOrEnumRuleCall();
	    }

)
))
    |((
	{ 
	  /* */ 
	}
    {
        $current = forceCreateModelElement(
            grammarAccess.getFeatureCorrespondenceAccess().getOneToOneCorrespondenceAction_1_0(),
            $current);
    }
)(
(
		{ 
	        newCompositeNode(grammarAccess.getFeatureCorrespondenceAccess().getLeftFeatureFunctionParserRuleCall_1_1_0()); 
	    }
		lv_left_5_0=ruleFeatureFunction		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getFeatureCorrespondenceRule());
	        }
       		set(
       			$current, 
       			"left",
        		lv_left_5_0, 
        		"FeatureFunction");
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_6='==' 
    {
    	newLeafNode(otherlv_6, grammarAccess.getFeatureCorrespondenceAccess().getEqualsSignEqualsSignKeyword_1_2());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getFeatureCorrespondenceAccess().getRightFeatureFunctionParserRuleCall_1_3_0()); 
	    }
		lv_right_7_0=ruleFeatureFunction		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getFeatureCorrespondenceRule());
	        }
       		set(
       			$current, 
       			"right",
        		lv_right_7_0, 
        		"FeatureFunction");
	        afterParserOrEnumRuleCall();
	    }

)
))
    |((
	{ 
	  /* */ 
	}
    {
        $current = forceCreateModelElement(
            grammarAccess.getFeatureCorrespondenceAccess().getLeftToRightCorrespondenceAction_2_0(),
            $current);
    }
)(
(
		{ 
	        newCompositeNode(grammarAccess.getFeatureCorrespondenceAccess().getLeftFeatureFunctionParserRuleCall_2_1_0()); 
	    }
		lv_left_9_0=ruleFeatureFunction		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getFeatureCorrespondenceRule());
	        }
       		set(
       			$current, 
       			"left",
        		lv_left_9_0, 
        		"FeatureFunction");
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_10='->' 
    {
    	newLeafNode(otherlv_10, grammarAccess.getFeatureCorrespondenceAccess().getHyphenMinusGreaterThanSignKeyword_2_2());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getFeatureCorrespondenceAccess().getRightFeatureFunctionParserRuleCall_2_3_0()); 
	    }
		lv_right_11_0=ruleFeatureFunction		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getFeatureCorrespondenceRule());
	        }
       		set(
       			$current, 
       			"right",
        		lv_right_11_0, 
        		"FeatureFunction");
	        afterParserOrEnumRuleCall();
	    }

)
))
    |((
	{ 
	  /* */ 
	}
    {
        $current = forceCreateModelElement(
            grammarAccess.getFeatureCorrespondenceAccess().getRightToLeftCorrespondenceAction_3_0(),
            $current);
    }
)(
(
		{ 
	        newCompositeNode(grammarAccess.getFeatureCorrespondenceAccess().getLeftFeatureFunctionParserRuleCall_3_1_0()); 
	    }
		lv_left_13_0=ruleFeatureFunction		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getFeatureCorrespondenceRule());
	        }
       		set(
       			$current, 
       			"left",
        		lv_left_13_0, 
        		"FeatureFunction");
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_14='<-' 
    {
    	newLeafNode(otherlv_14, grammarAccess.getFeatureCorrespondenceAccess().getLessThanSignHyphenMinusKeyword_3_2());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getFeatureCorrespondenceAccess().getRightFeatureFunctionParserRuleCall_3_3_0()); 
	    }
		lv_right_15_0=ruleFeatureFunction		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getFeatureCorrespondenceRule());
	        }
       		set(
       			$current, 
       			"right",
        		lv_right_15_0, 
        		"FeatureFunction");
	        afterParserOrEnumRuleCall();
	    }

)
)))
;





// Entry rule entryRuleFeatureFunction
entryRuleFeatureFunction returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getFeatureFunctionRule()); }
	 iv_ruleFeatureFunction=ruleFeatureFunction 
	 { $current=$iv_ruleFeatureFunction.current; } 
	 EOF 
;

// Rule FeatureFunction
ruleFeatureFunction returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
(
		{ 
	        newCompositeNode(grammarAccess.getFeatureFunctionAccess().getTermsFeatureTermParserRuleCall_0_0()); 
	    }
		lv_terms_0_0=ruleFeatureTerm		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getFeatureFunctionRule());
	        }
       		add(
       			$current, 
       			"terms",
        		lv_terms_0_0, 
        		"FeatureTerm");
	        afterParserOrEnumRuleCall();
	    }

)
)(	otherlv_1='+' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getFeatureFunctionAccess().getPlusSignKeyword_1_0());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getFeatureFunctionAccess().getTermsFeatureTermParserRuleCall_1_1_0()); 
	    }
		lv_terms_2_0=ruleFeatureTerm		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getFeatureFunctionRule());
	        }
       		add(
       			$current, 
       			"terms",
        		lv_terms_2_0, 
        		"FeatureTerm");
	        afterParserOrEnumRuleCall();
	    }

)
))*)
;





// Entry rule entryRuleFeatureTerm
entryRuleFeatureTerm returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getFeatureTermRule()); }
	 iv_ruleFeatureTerm=ruleFeatureTerm 
	 { $current=$iv_ruleFeatureTerm.current; } 
	 EOF 
;

// Rule FeatureTerm
ruleFeatureTerm returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
	{ 
	  /* */ 
	}
    { 
        newCompositeNode(grammarAccess.getFeatureTermAccess().getFeatureRefParserRuleCall_0()); 
    }
    this_FeatureRef_0=ruleFeatureRef
    { 
        $current = $this_FeatureRef_0.current; 
        afterParserOrEnumRuleCall();
    }

    |
	{ 
	  /* */ 
	}
    { 
        newCompositeNode(grammarAccess.getFeatureTermAccess().getValueParserRuleCall_1()); 
    }
    this_Value_1=ruleValue
    { 
        $current = $this_Value_1.current; 
        afterParserOrEnumRuleCall();
    }

    |
	{ 
	  /* */ 
	}
    { 
        newCompositeNode(grammarAccess.getFeatureTermAccess().getInstanceParserRuleCall_2()); 
    }
    this_Instance_2=ruleInstance
    { 
        $current = $this_Instance_2.current; 
        afterParserOrEnumRuleCall();
    }
)
;





// Entry rule entryRuleFeatureRef
entryRuleFeatureRef returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getFeatureRefRule()); }
	 iv_ruleFeatureRef=ruleFeatureRef 
	 { $current=$iv_ruleFeatureRef.current; } 
	 EOF 
;

// Rule FeatureRef
ruleFeatureRef returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(((
(
		{ 
	        newCompositeNode(grammarAccess.getFeatureRefAccess().getInstanceInstanceParserRuleCall_0_0_0()); 
	    }
		lv_instance_0_0=ruleInstance		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getFeatureRefRule());
	        }
       		set(
       			$current, 
       			"instance",
        		lv_instance_0_0, 
        		"Instance");
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_1='.' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getFeatureRefAccess().getFullStopKeyword_0_1());
    }
)?(
(
		{ 
		  /* */ 
		}
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getFeatureRefRule());
	        }
        }
	otherlv_2=RULE_ID
	{
		newLeafNode(otherlv_2, grammarAccess.getFeatureRefAccess().getReferencedFeatureEStructuralFeatureCrossReference_1_0()); 
	}

)
))
;





// Entry rule entryRuleInstance
entryRuleInstance returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getInstanceRule()); }
	 iv_ruleInstance=ruleInstance 
	 { $current=$iv_ruleInstance.current; } 
	 EOF 
;

// Rule Instance
ruleInstance returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((((
	{ 
	  /* */ 
	}
    {
        $current = forceCreateModelElement(
            grammarAccess.getInstanceAccess().getExistingInstanceAction_0_0_0(),
            $current);
    }
)	otherlv_1='existing' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getInstanceAccess().getExistingKeyword_0_0_1());
    }
)
    |((
	{ 
	  /* */ 
	}
    {
        $current = forceCreateModelElement(
            grammarAccess.getInstanceAccess().getNewInstanceAction_0_1_0(),
            $current);
    }
)	otherlv_3='new' 
    {
    	newLeafNode(otherlv_3, grammarAccess.getInstanceAccess().getNewKeyword_0_1_1());
    }
))(
(
		{ 
		  /* */ 
		}
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getInstanceRule());
	        }
        }
	otherlv_4=RULE_ID
	{
		newLeafNode(otherlv_4, grammarAccess.getInstanceAccess().getTypeEClassCrossReference_1_0()); 
	}

)
)(
(
		lv_many_5_0=	'*' 
    {
        newLeafNode(lv_many_5_0, grammarAccess.getInstanceAccess().getManyAsteriskKeyword_2_0());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getInstanceRule());
	        }
       		setWithLastConsumed($current, "many", true, "*");
	    }

)
)?	otherlv_6='{' 
    {
    	newLeafNode(otherlv_6, grammarAccess.getInstanceAccess().getLeftCurlyBracketKeyword_3());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getInstanceAccess().getAssignmentsAssignmentParserRuleCall_4_0()); 
	    }
		lv_assignments_7_0=ruleAssignment		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getInstanceRule());
	        }
       		add(
       			$current, 
       			"assignments",
        		lv_assignments_7_0, 
        		"Assignment");
	        afterParserOrEnumRuleCall();
	    }

)
)*	otherlv_8='}' 
    {
    	newLeafNode(otherlv_8, grammarAccess.getInstanceAccess().getRightCurlyBracketKeyword_5());
    }
)
;





// Entry rule entryRuleAssignment
entryRuleAssignment returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getAssignmentRule()); }
	 iv_ruleAssignment=ruleAssignment 
	 { $current=$iv_ruleAssignment.current; } 
	 EOF 
;

// Rule Assignment
ruleAssignment returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
(
		{ 
		  /* */ 
		}
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getAssignmentRule());
	        }
        }
	otherlv_0=RULE_ID
	{
		newLeafNode(otherlv_0, grammarAccess.getAssignmentAccess().getFeatureEStructuralFeatureCrossReference_0_0()); 
	}

)
)	otherlv_1='=' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getAssignmentAccess().getEqualsSignKeyword_1());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getAssignmentAccess().getValueTermParserRuleCall_2_0()); 
	    }
		lv_value_2_0=ruleTerm		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getAssignmentRule());
	        }
       		set(
       			$current, 
       			"value",
        		lv_value_2_0, 
        		"Term");
	        afterParserOrEnumRuleCall();
	    }

)
))
;





// Entry rule entryRuleTerm
entryRuleTerm returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getTermRule()); }
	 iv_ruleTerm=ruleTerm 
	 { $current=$iv_ruleTerm.current; } 
	 EOF 
;

// Rule Term
ruleTerm returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
	{ 
	  /* */ 
	}
    { 
        newCompositeNode(grammarAccess.getTermAccess().getValueParserRuleCall_0()); 
    }
    this_Value_0=ruleValue
    { 
        $current = $this_Value_0.current; 
        afterParserOrEnumRuleCall();
    }

    |((	ruleInstance)=>
	{ 
	  /* */ 
	}
    { 
        newCompositeNode(grammarAccess.getTermAccess().getInstanceParserRuleCall_1()); 
    }
    this_Instance_1=ruleInstance
    { 
        $current = $this_Instance_1.current; 
        afterParserOrEnumRuleCall();
    }
)
    |
	{ 
	  /* */ 
	}
    { 
        newCompositeNode(grammarAccess.getTermAccess().getThisRefParserRuleCall_2()); 
    }
    this_ThisRef_2=ruleThisRef
    { 
        $current = $this_ThisRef_2.current; 
        afterParserOrEnumRuleCall();
    }
)
;





// Entry rule entryRuleThisRef
entryRuleThisRef returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getThisRefRule()); }
	 iv_ruleThisRef=ruleThisRef 
	 { $current=$iv_ruleThisRef.current; } 
	 EOF 
;

// Rule ThisRef
ruleThisRef returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
	{ 
	  /* */ 
	}
    {
        $current = forceCreateModelElement(
            grammarAccess.getThisRefAccess().getThisRefAction_0(),
            $current);
    }
)	otherlv_1='this' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getThisRefAccess().getThisKeyword_1());
    }
)
;





// Entry rule entryRuleValue
entryRuleValue returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getValueRule()); }
	 iv_ruleValue=ruleValue 
	 { $current=$iv_ruleValue.current; } 
	 EOF 
;

// Rule Value
ruleValue returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
	{ 
	  /* */ 
	}
    { 
        newCompositeNode(grammarAccess.getValueAccess().getStringValueParserRuleCall_0()); 
    }
    this_StringValue_0=ruleStringValue
    { 
        $current = $this_StringValue_0.current; 
        afterParserOrEnumRuleCall();
    }

    |
	{ 
	  /* */ 
	}
    { 
        newCompositeNode(grammarAccess.getValueAccess().getIntValueParserRuleCall_1()); 
    }
    this_IntValue_1=ruleIntValue
    { 
        $current = $this_IntValue_1.current; 
        afterParserOrEnumRuleCall();
    }

    |
	{ 
	  /* */ 
	}
    { 
        newCompositeNode(grammarAccess.getValueAccess().getCollectionValueParserRuleCall_2()); 
    }
    this_CollectionValue_2=ruleCollectionValue
    { 
        $current = $this_CollectionValue_2.current; 
        afterParserOrEnumRuleCall();
    }
)
;





// Entry rule entryRuleCollectionValue
entryRuleCollectionValue returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getCollectionValueRule()); }
	 iv_ruleCollectionValue=ruleCollectionValue 
	 { $current=$iv_ruleCollectionValue.current; } 
	 EOF 
;

// Rule CollectionValue
ruleCollectionValue returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(	otherlv_0='[' 
    {
    	newLeafNode(otherlv_0, grammarAccess.getCollectionValueAccess().getLeftSquareBracketKeyword_0());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getCollectionValueAccess().getContentsTermParserRuleCall_1_0()); 
	    }
		lv_contents_1_0=ruleTerm		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getCollectionValueRule());
	        }
       		add(
       			$current, 
       			"contents",
        		lv_contents_1_0, 
        		"Term");
	        afterParserOrEnumRuleCall();
	    }

)
)(	otherlv_2=',' 
    {
    	newLeafNode(otherlv_2, grammarAccess.getCollectionValueAccess().getCommaKeyword_2_0());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getCollectionValueAccess().getContentsTermParserRuleCall_2_1_0()); 
	    }
		lv_contents_3_0=ruleTerm		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getCollectionValueRule());
	        }
       		add(
       			$current, 
       			"contents",
        		lv_contents_3_0, 
        		"Term");
	        afterParserOrEnumRuleCall();
	    }

)
))*	otherlv_4=']' 
    {
    	newLeafNode(otherlv_4, grammarAccess.getCollectionValueAccess().getRightSquareBracketKeyword_3());
    }
)
;





// Entry rule entryRuleStringValue
entryRuleStringValue returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getStringValueRule()); }
	 iv_ruleStringValue=ruleStringValue 
	 { $current=$iv_ruleStringValue.current; } 
	 EOF 
;

// Rule StringValue
ruleStringValue returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
(
		lv_value_0_0=RULE_STRING
		{
			newLeafNode(lv_value_0_0, grammarAccess.getStringValueAccess().getValueSTRINGTerminalRuleCall_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getStringValueRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"value",
        		lv_value_0_0, 
        		"STRING");
	    }

)
)
;





// Entry rule entryRuleIntValue
entryRuleIntValue returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getIntValueRule()); }
	 iv_ruleIntValue=ruleIntValue 
	 { $current=$iv_ruleIntValue.current; } 
	 EOF 
;

// Rule IntValue
ruleIntValue returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
(
		lv_value_0_0=RULE_INT
		{
			newLeafNode(lv_value_0_0, grammarAccess.getIntValueAccess().getValueINTTerminalRuleCall_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getIntValueRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"value",
        		lv_value_0_0, 
        		"INT");
	    }

)
)
;





// Entry rule entryRuleQualifiedName
entryRuleQualifiedName returns [String current=null] 
	:
	{ newCompositeNode(grammarAccess.getQualifiedNameRule()); } 
	 iv_ruleQualifiedName=ruleQualifiedName 
	 { $current=$iv_ruleQualifiedName.current.getText(); }  
	 EOF 
;

// Rule QualifiedName
ruleQualifiedName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(    this_ID_0=RULE_ID    {
		$current.merge(this_ID_0);
    }

    { 
    newLeafNode(this_ID_0, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 
    }
(
	kw='.' 
    {
        $current.merge(kw);
        newLeafNode(kw, grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 
    }
    this_ID_2=RULE_ID    {
		$current.merge(this_ID_2);
    }

    { 
    newLeafNode(this_ID_2, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 
    }
)*)
    ;







RULE_ID : '^'? ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*;

RULE_INT : ('0'..'9')+;

RULE_STRING : ('"' ('\\' .|~(('\\'|'"')))* '"'|'\'' ('\\' .|~(('\\'|'\'')))* '\'');

RULE_ML_COMMENT : '/*' ( options {greedy=false;} : . )*'*/';

RULE_SL_COMMENT : '//' ~(('\n'|'\r'))* ('\r'? '\n')?;

RULE_WS : (' '|'\t'|'\r'|'\n')+;

RULE_ANY_OTHER : .;


