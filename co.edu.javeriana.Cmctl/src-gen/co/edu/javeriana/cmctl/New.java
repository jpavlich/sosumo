/**
 */
package co.edu.javeriana.cmctl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>New</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link co.edu.javeriana.cmctl.New#getType <em>Type</em>}</li>
 *   <li>{@link co.edu.javeriana.cmctl.New#getArgs <em>Args</em>}</li>
 * </ul>
 *
 * @see co.edu.javeriana.cmctl.CmctlPackage#getNew()
 * @model
 * @generated
 */
public interface New extends Expression
{
  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(ClassType)
   * @see co.edu.javeriana.cmctl.CmctlPackage#getNew_Type()
   * @model containment="true"
   * @generated
   */
  ClassType getType();

  /**
   * Sets the value of the '{@link co.edu.javeriana.cmctl.New#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(ClassType value);

  /**
   * Returns the value of the '<em><b>Args</b></em>' containment reference list.
   * The list contents are of type {@link co.edu.javeriana.cmctl.Argument}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Args</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Args</em>' containment reference list.
   * @see co.edu.javeriana.cmctl.CmctlPackage#getNew_Args()
   * @model containment="true"
   * @generated
   */
  EList<Argument> getArgs();

} // New
