/**
 */
package co.edu.javeriana.cmctl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>If Template</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link co.edu.javeriana.cmctl.IfTemplate#getTemplateCondition <em>Template Condition</em>}</li>
 * </ul>
 *
 * @see co.edu.javeriana.cmctl.CmctlPackage#getIfTemplate()
 * @model
 * @generated
 */
public interface IfTemplate extends TemplateStatement
{
  /**
   * Returns the value of the '<em><b>Template Condition</b></em>' containment reference list.
   * The list contents are of type {@link co.edu.javeriana.cmctl.BoolConstant}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Template Condition</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Template Condition</em>' containment reference list.
   * @see co.edu.javeriana.cmctl.CmctlPackage#getIfTemplate_TemplateCondition()
   * @model containment="true"
   * @generated
   */
  EList<BoolConstant> getTemplateCondition();

} // IfTemplate
