/**
 */
package co.edu.javeriana.cmctl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Argument</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see co.edu.javeriana.cmctl.CmctlPackage#getArgument()
 * @model
 * @generated
 */
public interface Argument extends EObject
{
} // Argument
