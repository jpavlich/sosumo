/**
 */
package co.edu.javeriana.cmctl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see co.edu.javeriana.cmctl.CmctlPackage#getExpression()
 * @model
 * @generated
 */
public interface Expression extends Argument
{
} // Expression
