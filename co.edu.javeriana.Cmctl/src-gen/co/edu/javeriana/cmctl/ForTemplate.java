/**
 */
package co.edu.javeriana.cmctl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>For Template</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see co.edu.javeriana.cmctl.CmctlPackage#getForTemplate()
 * @model
 * @generated
 */
public interface ForTemplate extends TemplateStatement
{
} // ForTemplate
