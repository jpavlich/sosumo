/**
 */
package co.edu.javeriana.cmctl;

import org.eclipse.emf.ecore.ETypedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Template Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see co.edu.javeriana.cmctl.CmctlPackage#getTemplateParameter()
 * @model
 * @generated
 */
public interface TemplateParameter extends ETypedElement
{
} // TemplateParameter
