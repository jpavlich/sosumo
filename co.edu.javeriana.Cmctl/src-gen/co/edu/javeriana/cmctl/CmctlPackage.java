/**
 */
package co.edu.javeriana.cmctl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see co.edu.javeriana.cmctl.CmctlFactory
 * @model kind="package"
 * @generated
 */
public interface CmctlPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "cmctl";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.edu.co/javeriana/Cmctl";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "cmctl";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  CmctlPackage eINSTANCE = co.edu.javeriana.cmctl.impl.CmctlPackageImpl.init();

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.TemplateImpl <em>Template</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.TemplateImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getTemplate()
   * @generated
   */
  int TEMPLATE = 0;

  /**
   * The feature id for the '<em><b>Imports</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE__IMPORTS = 0;

  /**
   * The feature id for the '<em><b>Template Functions</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE__TEMPLATE_FUNCTIONS = 1;

  /**
   * The number of structural features of the '<em>Template</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.ImportImpl <em>Import</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.ImportImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getImport()
   * @generated
   */
  int IMPORT = 1;

  /**
   * The feature id for the '<em><b>Imported Namespace</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT__IMPORTED_NAMESPACE = 0;

  /**
   * The number of structural features of the '<em>Import</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.TemplateFunctionImpl <em>Template Function</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.TemplateFunctionImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getTemplateFunction()
   * @generated
   */
  int TEMPLATE_FUNCTION = 2;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_FUNCTION__NAME = 0;

  /**
   * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_FUNCTION__PARAMETERS = 1;

  /**
   * The feature id for the '<em><b>Statements</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_FUNCTION__STATEMENTS = 2;

  /**
   * The number of structural features of the '<em>Template Function</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_FUNCTION_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.TemplateParameterImpl <em>Template Parameter</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.TemplateParameterImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getTemplateParameter()
   * @generated
   */
  int TEMPLATE_PARAMETER = 3;

  /**
   * The feature id for the '<em><b>EAnnotations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_PARAMETER__EANNOTATIONS = EcorePackage.ETYPED_ELEMENT__EANNOTATIONS;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_PARAMETER__NAME = EcorePackage.ETYPED_ELEMENT__NAME;

  /**
   * The feature id for the '<em><b>Ordered</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_PARAMETER__ORDERED = EcorePackage.ETYPED_ELEMENT__ORDERED;

  /**
   * The feature id for the '<em><b>Unique</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_PARAMETER__UNIQUE = EcorePackage.ETYPED_ELEMENT__UNIQUE;

  /**
   * The feature id for the '<em><b>Lower Bound</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_PARAMETER__LOWER_BOUND = EcorePackage.ETYPED_ELEMENT__LOWER_BOUND;

  /**
   * The feature id for the '<em><b>Upper Bound</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_PARAMETER__UPPER_BOUND = EcorePackage.ETYPED_ELEMENT__UPPER_BOUND;

  /**
   * The feature id for the '<em><b>Many</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_PARAMETER__MANY = EcorePackage.ETYPED_ELEMENT__MANY;

  /**
   * The feature id for the '<em><b>Required</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_PARAMETER__REQUIRED = EcorePackage.ETYPED_ELEMENT__REQUIRED;

  /**
   * The feature id for the '<em><b>EType</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_PARAMETER__ETYPE = EcorePackage.ETYPED_ELEMENT__ETYPE;

  /**
   * The feature id for the '<em><b>EGeneric Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_PARAMETER__EGENERIC_TYPE = EcorePackage.ETYPED_ELEMENT__EGENERIC_TYPE;

  /**
   * The number of structural features of the '<em>Template Parameter</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_PARAMETER_FEATURE_COUNT = EcorePackage.ETYPED_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.CompositeStatementImpl <em>Composite Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.CompositeStatementImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getCompositeStatement()
   * @generated
   */
  int COMPOSITE_STATEMENT = 4;

  /**
   * The number of structural features of the '<em>Composite Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPOSITE_STATEMENT_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.TemplateStatementImpl <em>Template Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.TemplateStatementImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getTemplateStatement()
   * @generated
   */
  int TEMPLATE_STATEMENT = 5;

  /**
   * The feature id for the '<em><b>Statements</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_STATEMENT__STATEMENTS = COMPOSITE_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Template Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_STATEMENT_FEATURE_COUNT = COMPOSITE_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.IfTemplateImpl <em>If Template</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.IfTemplateImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getIfTemplate()
   * @generated
   */
  int IF_TEMPLATE = 6;

  /**
   * The feature id for the '<em><b>Statements</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_TEMPLATE__STATEMENTS = TEMPLATE_STATEMENT__STATEMENTS;

  /**
   * The feature id for the '<em><b>Template Condition</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_TEMPLATE__TEMPLATE_CONDITION = TEMPLATE_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>If Template</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_TEMPLATE_FEATURE_COUNT = TEMPLATE_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.ForTemplateImpl <em>For Template</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.ForTemplateImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getForTemplate()
   * @generated
   */
  int FOR_TEMPLATE = 7;

  /**
   * The feature id for the '<em><b>Statements</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOR_TEMPLATE__STATEMENTS = TEMPLATE_STATEMENT__STATEMENTS;

  /**
   * The number of structural features of the '<em>For Template</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOR_TEMPLATE_FEATURE_COUNT = TEMPLATE_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.TemplateCallImpl <em>Template Call</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.TemplateCallImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getTemplateCall()
   * @generated
   */
  int TEMPLATE_CALL = 8;

  /**
   * The feature id for the '<em><b>Call</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_CALL__CALL = 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_CALL__TYPE = 1;

  /**
   * The number of structural features of the '<em>Template Call</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_CALL_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.TemplateReferenceImpl <em>Template Reference</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.TemplateReferenceImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getTemplateReference()
   * @generated
   */
  int TEMPLATE_REFERENCE = 9;

  /**
   * The feature id for the '<em><b>Referenced Element</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_REFERENCE__REFERENCED_ELEMENT = 0;

  /**
   * The feature id for the '<em><b>Tail</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_REFERENCE__TAIL = 1;

  /**
   * The number of structural features of the '<em>Template Reference</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEMPLATE_REFERENCE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.TypeImpl <em>Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.TypeImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getType()
   * @generated
   */
  int TYPE = 10;

  /**
   * The number of structural features of the '<em>Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.BasicTypeImpl <em>Basic Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.BasicTypeImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getBasicType()
   * @generated
   */
  int BASIC_TYPE = 11;

  /**
   * The feature id for the '<em><b>Basic</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASIC_TYPE__BASIC = TYPE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Basic Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASIC_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.ClassTypeImpl <em>Class Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.ClassTypeImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getClassType()
   * @generated
   */
  int CLASS_TYPE = 12;

  /**
   * The feature id for the '<em><b>Classref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS_TYPE__CLASSREF = TYPE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Class Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.TypedElementImpl <em>Typed Element</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.TypedElementImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getTypedElement()
   * @generated
   */
  int TYPED_ELEMENT = 13;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPED_ELEMENT__TYPE = 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPED_ELEMENT__NAME = 1;

  /**
   * The number of structural features of the '<em>Typed Element</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPED_ELEMENT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.ClassImpl <em>Class</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.ClassImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getClass_()
   * @generated
   */
  int CLASS = 14;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS__NAME = COMPOSITE_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Extends</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS__EXTENDS = COMPOSITE_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Fields</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS__FIELDS = COMPOSITE_STATEMENT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Methods</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS__METHODS = COMPOSITE_STATEMENT_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>Class</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS_FEATURE_COUNT = COMPOSITE_STATEMENT_FEATURE_COUNT + 4;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.FieldImpl <em>Field</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.FieldImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getField()
   * @generated
   */
  int FIELD = 15;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD__TYPE = TYPED_ELEMENT__TYPE;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD__NAME = TYPED_ELEMENT__NAME;

  /**
   * The number of structural features of the '<em>Field</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_FEATURE_COUNT = TYPED_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.ParameterImpl <em>Parameter</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.ParameterImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getParameter()
   * @generated
   */
  int PARAMETER = 16;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER__TYPE = TYPED_ELEMENT__TYPE;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER__NAME = TYPED_ELEMENT__NAME;

  /**
   * The number of structural features of the '<em>Parameter</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER_FEATURE_COUNT = TYPED_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.MethodImpl <em>Method</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.MethodImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getMethod()
   * @generated
   */
  int METHOD = 17;

  /**
   * The feature id for the '<em><b>Returntype</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD__RETURNTYPE = 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD__NAME = 1;

  /**
   * The feature id for the '<em><b>Params</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD__PARAMS = 2;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD__BODY = 3;

  /**
   * The number of structural features of the '<em>Method</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.CompositeClassImpl <em>Composite Class</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.CompositeClassImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getCompositeClass()
   * @generated
   */
  int COMPOSITE_CLASS = 18;

  /**
   * The feature id for the '<em><b>Name</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPOSITE_CLASS__NAME = COMPOSITE_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Extends</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPOSITE_CLASS__EXTENDS = COMPOSITE_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Compositefields</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPOSITE_CLASS__COMPOSITEFIELDS = COMPOSITE_STATEMENT_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Composite Class</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPOSITE_CLASS_FEATURE_COUNT = COMPOSITE_STATEMENT_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.CompositeFieldImpl <em>Composite Field</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.CompositeFieldImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getCompositeField()
   * @generated
   */
  int COMPOSITE_FIELD = 19;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPOSITE_FIELD__TYPE = 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPOSITE_FIELD__NAME = 1;

  /**
   * The number of structural features of the '<em>Composite Field</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPOSITE_FIELD_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.CompositeMethodImpl <em>Composite Method</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.CompositeMethodImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getCompositeMethod()
   * @generated
   */
  int COMPOSITE_METHOD = 20;

  /**
   * The feature id for the '<em><b>Returntype</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPOSITE_METHOD__RETURNTYPE = 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPOSITE_METHOD__NAME = 1;

  /**
   * The number of structural features of the '<em>Composite Method</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPOSITE_METHOD_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.MethodBodyImpl <em>Method Body</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.MethodBodyImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getMethodBody()
   * @generated
   */
  int METHOD_BODY = 21;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_BODY__EXPRESSION = 0;

  /**
   * The number of structural features of the '<em>Method Body</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_BODY_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.ArgumentImpl <em>Argument</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.ArgumentImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getArgument()
   * @generated
   */
  int ARGUMENT = 34;

  /**
   * The number of structural features of the '<em>Argument</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARGUMENT_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.ExpressionImpl <em>Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.ExpressionImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getExpression()
   * @generated
   */
  int EXPRESSION = 22;

  /**
   * The number of structural features of the '<em>Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_FEATURE_COUNT = ARGUMENT_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.MessageImpl <em>Message</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.MessageImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getMessage()
   * @generated
   */
  int MESSAGE = 23;

  /**
   * The number of structural features of the '<em>Message</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MESSAGE_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.MethodCallImpl <em>Method Call</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.MethodCallImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getMethodCall()
   * @generated
   */
  int METHOD_CALL = 24;

  /**
   * The feature id for the '<em><b>Name</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_CALL__NAME = MESSAGE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Args</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_CALL__ARGS = MESSAGE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Method Call</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_CALL_FEATURE_COUNT = MESSAGE_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.FieldSelectionImpl <em>Field Selection</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.FieldSelectionImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getFieldSelection()
   * @generated
   */
  int FIELD_SELECTION = 25;

  /**
   * The feature id for the '<em><b>Name</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_SELECTION__NAME = MESSAGE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Field Selection</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_SELECTION_FEATURE_COUNT = MESSAGE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.ThisImpl <em>This</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.ThisImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getThis()
   * @generated
   */
  int THIS = 26;

  /**
   * The feature id for the '<em><b>Variable</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THIS__VARIABLE = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>This</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THIS_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.VariableImpl <em>Variable</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.VariableImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getVariable()
   * @generated
   */
  int VARIABLE = 27;

  /**
   * The feature id for the '<em><b>Paramref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE__PARAMREF = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Variable</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.NewImpl <em>New</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.NewImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getNew()
   * @generated
   */
  int NEW = 28;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NEW__TYPE = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Args</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NEW__ARGS = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>New</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NEW_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.CastImpl <em>Cast</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.CastImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getCast()
   * @generated
   */
  int CAST = 29;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CAST__TYPE = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Object</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CAST__OBJECT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Cast</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CAST_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.ConstantImpl <em>Constant</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.ConstantImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getConstant()
   * @generated
   */
  int CONSTANT = 30;

  /**
   * The number of structural features of the '<em>Constant</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTANT_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.StringConstantImpl <em>String Constant</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.StringConstantImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getStringConstant()
   * @generated
   */
  int STRING_CONSTANT = 31;

  /**
   * The feature id for the '<em><b>Constant</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRING_CONSTANT__CONSTANT = CONSTANT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>String Constant</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRING_CONSTANT_FEATURE_COUNT = CONSTANT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.IntConstantImpl <em>Int Constant</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.IntConstantImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getIntConstant()
   * @generated
   */
  int INT_CONSTANT = 32;

  /**
   * The feature id for the '<em><b>Constant</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INT_CONSTANT__CONSTANT = CONSTANT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Int Constant</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INT_CONSTANT_FEATURE_COUNT = CONSTANT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.BoolConstantImpl <em>Bool Constant</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.BoolConstantImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getBoolConstant()
   * @generated
   */
  int BOOL_CONSTANT = 33;

  /**
   * The feature id for the '<em><b>Constant</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOL_CONSTANT__CONSTANT = CONSTANT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Bool Constant</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOL_CONSTANT_FEATURE_COUNT = CONSTANT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link co.edu.javeriana.cmctl.impl.SelectionImpl <em>Selection</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see co.edu.javeriana.cmctl.impl.SelectionImpl
   * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getSelection()
   * @generated
   */
  int SELECTION = 35;

  /**
   * The feature id for the '<em><b>Receiver</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECTION__RECEIVER = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Message</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECTION__MESSAGE = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Selection</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECTION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;


  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.Template <em>Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Template</em>'.
   * @see co.edu.javeriana.cmctl.Template
   * @generated
   */
  EClass getTemplate();

  /**
   * Returns the meta object for the containment reference list '{@link co.edu.javeriana.cmctl.Template#getImports <em>Imports</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Imports</em>'.
   * @see co.edu.javeriana.cmctl.Template#getImports()
   * @see #getTemplate()
   * @generated
   */
  EReference getTemplate_Imports();

  /**
   * Returns the meta object for the containment reference list '{@link co.edu.javeriana.cmctl.Template#getTemplateFunctions <em>Template Functions</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Template Functions</em>'.
   * @see co.edu.javeriana.cmctl.Template#getTemplateFunctions()
   * @see #getTemplate()
   * @generated
   */
  EReference getTemplate_TemplateFunctions();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.Import <em>Import</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Import</em>'.
   * @see co.edu.javeriana.cmctl.Import
   * @generated
   */
  EClass getImport();

  /**
   * Returns the meta object for the attribute '{@link co.edu.javeriana.cmctl.Import#getImportedNamespace <em>Imported Namespace</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Imported Namespace</em>'.
   * @see co.edu.javeriana.cmctl.Import#getImportedNamespace()
   * @see #getImport()
   * @generated
   */
  EAttribute getImport_ImportedNamespace();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.TemplateFunction <em>Template Function</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Template Function</em>'.
   * @see co.edu.javeriana.cmctl.TemplateFunction
   * @generated
   */
  EClass getTemplateFunction();

  /**
   * Returns the meta object for the attribute '{@link co.edu.javeriana.cmctl.TemplateFunction#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see co.edu.javeriana.cmctl.TemplateFunction#getName()
   * @see #getTemplateFunction()
   * @generated
   */
  EAttribute getTemplateFunction_Name();

  /**
   * Returns the meta object for the containment reference list '{@link co.edu.javeriana.cmctl.TemplateFunction#getParameters <em>Parameters</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Parameters</em>'.
   * @see co.edu.javeriana.cmctl.TemplateFunction#getParameters()
   * @see #getTemplateFunction()
   * @generated
   */
  EReference getTemplateFunction_Parameters();

  /**
   * Returns the meta object for the containment reference list '{@link co.edu.javeriana.cmctl.TemplateFunction#getStatements <em>Statements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Statements</em>'.
   * @see co.edu.javeriana.cmctl.TemplateFunction#getStatements()
   * @see #getTemplateFunction()
   * @generated
   */
  EReference getTemplateFunction_Statements();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.TemplateParameter <em>Template Parameter</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Template Parameter</em>'.
   * @see co.edu.javeriana.cmctl.TemplateParameter
   * @generated
   */
  EClass getTemplateParameter();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.CompositeStatement <em>Composite Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Composite Statement</em>'.
   * @see co.edu.javeriana.cmctl.CompositeStatement
   * @generated
   */
  EClass getCompositeStatement();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.TemplateStatement <em>Template Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Template Statement</em>'.
   * @see co.edu.javeriana.cmctl.TemplateStatement
   * @generated
   */
  EClass getTemplateStatement();

  /**
   * Returns the meta object for the containment reference list '{@link co.edu.javeriana.cmctl.TemplateStatement#getStatements <em>Statements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Statements</em>'.
   * @see co.edu.javeriana.cmctl.TemplateStatement#getStatements()
   * @see #getTemplateStatement()
   * @generated
   */
  EReference getTemplateStatement_Statements();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.IfTemplate <em>If Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>If Template</em>'.
   * @see co.edu.javeriana.cmctl.IfTemplate
   * @generated
   */
  EClass getIfTemplate();

  /**
   * Returns the meta object for the containment reference list '{@link co.edu.javeriana.cmctl.IfTemplate#getTemplateCondition <em>Template Condition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Template Condition</em>'.
   * @see co.edu.javeriana.cmctl.IfTemplate#getTemplateCondition()
   * @see #getIfTemplate()
   * @generated
   */
  EReference getIfTemplate_TemplateCondition();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.ForTemplate <em>For Template</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>For Template</em>'.
   * @see co.edu.javeriana.cmctl.ForTemplate
   * @generated
   */
  EClass getForTemplate();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.TemplateCall <em>Template Call</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Template Call</em>'.
   * @see co.edu.javeriana.cmctl.TemplateCall
   * @generated
   */
  EClass getTemplateCall();

  /**
   * Returns the meta object for the attribute '{@link co.edu.javeriana.cmctl.TemplateCall#getCall <em>Call</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Call</em>'.
   * @see co.edu.javeriana.cmctl.TemplateCall#getCall()
   * @see #getTemplateCall()
   * @generated
   */
  EAttribute getTemplateCall_Call();

  /**
   * Returns the meta object for the containment reference '{@link co.edu.javeriana.cmctl.TemplateCall#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see co.edu.javeriana.cmctl.TemplateCall#getType()
   * @see #getTemplateCall()
   * @generated
   */
  EReference getTemplateCall_Type();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.TemplateReference <em>Template Reference</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Template Reference</em>'.
   * @see co.edu.javeriana.cmctl.TemplateReference
   * @generated
   */
  EClass getTemplateReference();

  /**
   * Returns the meta object for the reference '{@link co.edu.javeriana.cmctl.TemplateReference#getReferencedElement <em>Referenced Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Referenced Element</em>'.
   * @see co.edu.javeriana.cmctl.TemplateReference#getReferencedElement()
   * @see #getTemplateReference()
   * @generated
   */
  EReference getTemplateReference_ReferencedElement();

  /**
   * Returns the meta object for the containment reference '{@link co.edu.javeriana.cmctl.TemplateReference#getTail <em>Tail</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Tail</em>'.
   * @see co.edu.javeriana.cmctl.TemplateReference#getTail()
   * @see #getTemplateReference()
   * @generated
   */
  EReference getTemplateReference_Tail();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.Type <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type</em>'.
   * @see co.edu.javeriana.cmctl.Type
   * @generated
   */
  EClass getType();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.BasicType <em>Basic Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Basic Type</em>'.
   * @see co.edu.javeriana.cmctl.BasicType
   * @generated
   */
  EClass getBasicType();

  /**
   * Returns the meta object for the attribute '{@link co.edu.javeriana.cmctl.BasicType#getBasic <em>Basic</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Basic</em>'.
   * @see co.edu.javeriana.cmctl.BasicType#getBasic()
   * @see #getBasicType()
   * @generated
   */
  EAttribute getBasicType_Basic();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.ClassType <em>Class Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Class Type</em>'.
   * @see co.edu.javeriana.cmctl.ClassType
   * @generated
   */
  EClass getClassType();

  /**
   * Returns the meta object for the reference '{@link co.edu.javeriana.cmctl.ClassType#getClassref <em>Classref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Classref</em>'.
   * @see co.edu.javeriana.cmctl.ClassType#getClassref()
   * @see #getClassType()
   * @generated
   */
  EReference getClassType_Classref();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.TypedElement <em>Typed Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Typed Element</em>'.
   * @see co.edu.javeriana.cmctl.TypedElement
   * @generated
   */
  EClass getTypedElement();

  /**
   * Returns the meta object for the containment reference '{@link co.edu.javeriana.cmctl.TypedElement#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see co.edu.javeriana.cmctl.TypedElement#getType()
   * @see #getTypedElement()
   * @generated
   */
  EReference getTypedElement_Type();

  /**
   * Returns the meta object for the attribute '{@link co.edu.javeriana.cmctl.TypedElement#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see co.edu.javeriana.cmctl.TypedElement#getName()
   * @see #getTypedElement()
   * @generated
   */
  EAttribute getTypedElement_Name();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.Class <em>Class</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Class</em>'.
   * @see co.edu.javeriana.cmctl.Class
   * @generated
   */
  EClass getClass_();

  /**
   * Returns the meta object for the attribute '{@link co.edu.javeriana.cmctl.Class#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see co.edu.javeriana.cmctl.Class#getName()
   * @see #getClass_()
   * @generated
   */
  EAttribute getClass_Name();

  /**
   * Returns the meta object for the reference '{@link co.edu.javeriana.cmctl.Class#getExtends <em>Extends</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Extends</em>'.
   * @see co.edu.javeriana.cmctl.Class#getExtends()
   * @see #getClass_()
   * @generated
   */
  EReference getClass_Extends();

  /**
   * Returns the meta object for the containment reference list '{@link co.edu.javeriana.cmctl.Class#getFields <em>Fields</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Fields</em>'.
   * @see co.edu.javeriana.cmctl.Class#getFields()
   * @see #getClass_()
   * @generated
   */
  EReference getClass_Fields();

  /**
   * Returns the meta object for the containment reference list '{@link co.edu.javeriana.cmctl.Class#getMethods <em>Methods</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Methods</em>'.
   * @see co.edu.javeriana.cmctl.Class#getMethods()
   * @see #getClass_()
   * @generated
   */
  EReference getClass_Methods();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.Field <em>Field</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Field</em>'.
   * @see co.edu.javeriana.cmctl.Field
   * @generated
   */
  EClass getField();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.Parameter <em>Parameter</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Parameter</em>'.
   * @see co.edu.javeriana.cmctl.Parameter
   * @generated
   */
  EClass getParameter();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.Method <em>Method</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Method</em>'.
   * @see co.edu.javeriana.cmctl.Method
   * @generated
   */
  EClass getMethod();

  /**
   * Returns the meta object for the containment reference '{@link co.edu.javeriana.cmctl.Method#getReturntype <em>Returntype</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Returntype</em>'.
   * @see co.edu.javeriana.cmctl.Method#getReturntype()
   * @see #getMethod()
   * @generated
   */
  EReference getMethod_Returntype();

  /**
   * Returns the meta object for the attribute '{@link co.edu.javeriana.cmctl.Method#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see co.edu.javeriana.cmctl.Method#getName()
   * @see #getMethod()
   * @generated
   */
  EAttribute getMethod_Name();

  /**
   * Returns the meta object for the containment reference list '{@link co.edu.javeriana.cmctl.Method#getParams <em>Params</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Params</em>'.
   * @see co.edu.javeriana.cmctl.Method#getParams()
   * @see #getMethod()
   * @generated
   */
  EReference getMethod_Params();

  /**
   * Returns the meta object for the containment reference '{@link co.edu.javeriana.cmctl.Method#getBody <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Body</em>'.
   * @see co.edu.javeriana.cmctl.Method#getBody()
   * @see #getMethod()
   * @generated
   */
  EReference getMethod_Body();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.CompositeClass <em>Composite Class</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Composite Class</em>'.
   * @see co.edu.javeriana.cmctl.CompositeClass
   * @generated
   */
  EClass getCompositeClass();

  /**
   * Returns the meta object for the containment reference '{@link co.edu.javeriana.cmctl.CompositeClass#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Name</em>'.
   * @see co.edu.javeriana.cmctl.CompositeClass#getName()
   * @see #getCompositeClass()
   * @generated
   */
  EReference getCompositeClass_Name();

  /**
   * Returns the meta object for the reference '{@link co.edu.javeriana.cmctl.CompositeClass#getExtends <em>Extends</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Extends</em>'.
   * @see co.edu.javeriana.cmctl.CompositeClass#getExtends()
   * @see #getCompositeClass()
   * @generated
   */
  EReference getCompositeClass_Extends();

  /**
   * Returns the meta object for the containment reference list '{@link co.edu.javeriana.cmctl.CompositeClass#getCompositefields <em>Compositefields</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Compositefields</em>'.
   * @see co.edu.javeriana.cmctl.CompositeClass#getCompositefields()
   * @see #getCompositeClass()
   * @generated
   */
  EReference getCompositeClass_Compositefields();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.CompositeField <em>Composite Field</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Composite Field</em>'.
   * @see co.edu.javeriana.cmctl.CompositeField
   * @generated
   */
  EClass getCompositeField();

  /**
   * Returns the meta object for the containment reference '{@link co.edu.javeriana.cmctl.CompositeField#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see co.edu.javeriana.cmctl.CompositeField#getType()
   * @see #getCompositeField()
   * @generated
   */
  EReference getCompositeField_Type();

  /**
   * Returns the meta object for the containment reference '{@link co.edu.javeriana.cmctl.CompositeField#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Name</em>'.
   * @see co.edu.javeriana.cmctl.CompositeField#getName()
   * @see #getCompositeField()
   * @generated
   */
  EReference getCompositeField_Name();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.CompositeMethod <em>Composite Method</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Composite Method</em>'.
   * @see co.edu.javeriana.cmctl.CompositeMethod
   * @generated
   */
  EClass getCompositeMethod();

  /**
   * Returns the meta object for the containment reference '{@link co.edu.javeriana.cmctl.CompositeMethod#getReturntype <em>Returntype</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Returntype</em>'.
   * @see co.edu.javeriana.cmctl.CompositeMethod#getReturntype()
   * @see #getCompositeMethod()
   * @generated
   */
  EReference getCompositeMethod_Returntype();

  /**
   * Returns the meta object for the containment reference '{@link co.edu.javeriana.cmctl.CompositeMethod#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Name</em>'.
   * @see co.edu.javeriana.cmctl.CompositeMethod#getName()
   * @see #getCompositeMethod()
   * @generated
   */
  EReference getCompositeMethod_Name();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.MethodBody <em>Method Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Method Body</em>'.
   * @see co.edu.javeriana.cmctl.MethodBody
   * @generated
   */
  EClass getMethodBody();

  /**
   * Returns the meta object for the containment reference '{@link co.edu.javeriana.cmctl.MethodBody#getExpression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expression</em>'.
   * @see co.edu.javeriana.cmctl.MethodBody#getExpression()
   * @see #getMethodBody()
   * @generated
   */
  EReference getMethodBody_Expression();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.Expression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expression</em>'.
   * @see co.edu.javeriana.cmctl.Expression
   * @generated
   */
  EClass getExpression();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.Message <em>Message</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Message</em>'.
   * @see co.edu.javeriana.cmctl.Message
   * @generated
   */
  EClass getMessage();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.MethodCall <em>Method Call</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Method Call</em>'.
   * @see co.edu.javeriana.cmctl.MethodCall
   * @generated
   */
  EClass getMethodCall();

  /**
   * Returns the meta object for the reference '{@link co.edu.javeriana.cmctl.MethodCall#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Name</em>'.
   * @see co.edu.javeriana.cmctl.MethodCall#getName()
   * @see #getMethodCall()
   * @generated
   */
  EReference getMethodCall_Name();

  /**
   * Returns the meta object for the containment reference list '{@link co.edu.javeriana.cmctl.MethodCall#getArgs <em>Args</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Args</em>'.
   * @see co.edu.javeriana.cmctl.MethodCall#getArgs()
   * @see #getMethodCall()
   * @generated
   */
  EReference getMethodCall_Args();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.FieldSelection <em>Field Selection</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Field Selection</em>'.
   * @see co.edu.javeriana.cmctl.FieldSelection
   * @generated
   */
  EClass getFieldSelection();

  /**
   * Returns the meta object for the reference '{@link co.edu.javeriana.cmctl.FieldSelection#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Name</em>'.
   * @see co.edu.javeriana.cmctl.FieldSelection#getName()
   * @see #getFieldSelection()
   * @generated
   */
  EReference getFieldSelection_Name();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.This <em>This</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>This</em>'.
   * @see co.edu.javeriana.cmctl.This
   * @generated
   */
  EClass getThis();

  /**
   * Returns the meta object for the attribute '{@link co.edu.javeriana.cmctl.This#getVariable <em>Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Variable</em>'.
   * @see co.edu.javeriana.cmctl.This#getVariable()
   * @see #getThis()
   * @generated
   */
  EAttribute getThis_Variable();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.Variable <em>Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Variable</em>'.
   * @see co.edu.javeriana.cmctl.Variable
   * @generated
   */
  EClass getVariable();

  /**
   * Returns the meta object for the reference '{@link co.edu.javeriana.cmctl.Variable#getParamref <em>Paramref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Paramref</em>'.
   * @see co.edu.javeriana.cmctl.Variable#getParamref()
   * @see #getVariable()
   * @generated
   */
  EReference getVariable_Paramref();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.New <em>New</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>New</em>'.
   * @see co.edu.javeriana.cmctl.New
   * @generated
   */
  EClass getNew();

  /**
   * Returns the meta object for the containment reference '{@link co.edu.javeriana.cmctl.New#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see co.edu.javeriana.cmctl.New#getType()
   * @see #getNew()
   * @generated
   */
  EReference getNew_Type();

  /**
   * Returns the meta object for the containment reference list '{@link co.edu.javeriana.cmctl.New#getArgs <em>Args</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Args</em>'.
   * @see co.edu.javeriana.cmctl.New#getArgs()
   * @see #getNew()
   * @generated
   */
  EReference getNew_Args();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.Cast <em>Cast</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Cast</em>'.
   * @see co.edu.javeriana.cmctl.Cast
   * @generated
   */
  EClass getCast();

  /**
   * Returns the meta object for the containment reference '{@link co.edu.javeriana.cmctl.Cast#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see co.edu.javeriana.cmctl.Cast#getType()
   * @see #getCast()
   * @generated
   */
  EReference getCast_Type();

  /**
   * Returns the meta object for the containment reference '{@link co.edu.javeriana.cmctl.Cast#getObject <em>Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Object</em>'.
   * @see co.edu.javeriana.cmctl.Cast#getObject()
   * @see #getCast()
   * @generated
   */
  EReference getCast_Object();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.Constant <em>Constant</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Constant</em>'.
   * @see co.edu.javeriana.cmctl.Constant
   * @generated
   */
  EClass getConstant();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.StringConstant <em>String Constant</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>String Constant</em>'.
   * @see co.edu.javeriana.cmctl.StringConstant
   * @generated
   */
  EClass getStringConstant();

  /**
   * Returns the meta object for the attribute '{@link co.edu.javeriana.cmctl.StringConstant#getConstant <em>Constant</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Constant</em>'.
   * @see co.edu.javeriana.cmctl.StringConstant#getConstant()
   * @see #getStringConstant()
   * @generated
   */
  EAttribute getStringConstant_Constant();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.IntConstant <em>Int Constant</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Int Constant</em>'.
   * @see co.edu.javeriana.cmctl.IntConstant
   * @generated
   */
  EClass getIntConstant();

  /**
   * Returns the meta object for the attribute '{@link co.edu.javeriana.cmctl.IntConstant#getConstant <em>Constant</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Constant</em>'.
   * @see co.edu.javeriana.cmctl.IntConstant#getConstant()
   * @see #getIntConstant()
   * @generated
   */
  EAttribute getIntConstant_Constant();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.BoolConstant <em>Bool Constant</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Bool Constant</em>'.
   * @see co.edu.javeriana.cmctl.BoolConstant
   * @generated
   */
  EClass getBoolConstant();

  /**
   * Returns the meta object for the attribute '{@link co.edu.javeriana.cmctl.BoolConstant#getConstant <em>Constant</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Constant</em>'.
   * @see co.edu.javeriana.cmctl.BoolConstant#getConstant()
   * @see #getBoolConstant()
   * @generated
   */
  EAttribute getBoolConstant_Constant();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.Argument <em>Argument</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Argument</em>'.
   * @see co.edu.javeriana.cmctl.Argument
   * @generated
   */
  EClass getArgument();

  /**
   * Returns the meta object for class '{@link co.edu.javeriana.cmctl.Selection <em>Selection</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Selection</em>'.
   * @see co.edu.javeriana.cmctl.Selection
   * @generated
   */
  EClass getSelection();

  /**
   * Returns the meta object for the containment reference '{@link co.edu.javeriana.cmctl.Selection#getReceiver <em>Receiver</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Receiver</em>'.
   * @see co.edu.javeriana.cmctl.Selection#getReceiver()
   * @see #getSelection()
   * @generated
   */
  EReference getSelection_Receiver();

  /**
   * Returns the meta object for the containment reference '{@link co.edu.javeriana.cmctl.Selection#getMessage <em>Message</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Message</em>'.
   * @see co.edu.javeriana.cmctl.Selection#getMessage()
   * @see #getSelection()
   * @generated
   */
  EReference getSelection_Message();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  CmctlFactory getCmctlFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.TemplateImpl <em>Template</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.TemplateImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getTemplate()
     * @generated
     */
    EClass TEMPLATE = eINSTANCE.getTemplate();

    /**
     * The meta object literal for the '<em><b>Imports</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TEMPLATE__IMPORTS = eINSTANCE.getTemplate_Imports();

    /**
     * The meta object literal for the '<em><b>Template Functions</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TEMPLATE__TEMPLATE_FUNCTIONS = eINSTANCE.getTemplate_TemplateFunctions();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.ImportImpl <em>Import</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.ImportImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getImport()
     * @generated
     */
    EClass IMPORT = eINSTANCE.getImport();

    /**
     * The meta object literal for the '<em><b>Imported Namespace</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute IMPORT__IMPORTED_NAMESPACE = eINSTANCE.getImport_ImportedNamespace();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.TemplateFunctionImpl <em>Template Function</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.TemplateFunctionImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getTemplateFunction()
     * @generated
     */
    EClass TEMPLATE_FUNCTION = eINSTANCE.getTemplateFunction();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TEMPLATE_FUNCTION__NAME = eINSTANCE.getTemplateFunction_Name();

    /**
     * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TEMPLATE_FUNCTION__PARAMETERS = eINSTANCE.getTemplateFunction_Parameters();

    /**
     * The meta object literal for the '<em><b>Statements</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TEMPLATE_FUNCTION__STATEMENTS = eINSTANCE.getTemplateFunction_Statements();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.TemplateParameterImpl <em>Template Parameter</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.TemplateParameterImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getTemplateParameter()
     * @generated
     */
    EClass TEMPLATE_PARAMETER = eINSTANCE.getTemplateParameter();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.CompositeStatementImpl <em>Composite Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.CompositeStatementImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getCompositeStatement()
     * @generated
     */
    EClass COMPOSITE_STATEMENT = eINSTANCE.getCompositeStatement();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.TemplateStatementImpl <em>Template Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.TemplateStatementImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getTemplateStatement()
     * @generated
     */
    EClass TEMPLATE_STATEMENT = eINSTANCE.getTemplateStatement();

    /**
     * The meta object literal for the '<em><b>Statements</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TEMPLATE_STATEMENT__STATEMENTS = eINSTANCE.getTemplateStatement_Statements();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.IfTemplateImpl <em>If Template</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.IfTemplateImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getIfTemplate()
     * @generated
     */
    EClass IF_TEMPLATE = eINSTANCE.getIfTemplate();

    /**
     * The meta object literal for the '<em><b>Template Condition</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IF_TEMPLATE__TEMPLATE_CONDITION = eINSTANCE.getIfTemplate_TemplateCondition();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.ForTemplateImpl <em>For Template</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.ForTemplateImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getForTemplate()
     * @generated
     */
    EClass FOR_TEMPLATE = eINSTANCE.getForTemplate();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.TemplateCallImpl <em>Template Call</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.TemplateCallImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getTemplateCall()
     * @generated
     */
    EClass TEMPLATE_CALL = eINSTANCE.getTemplateCall();

    /**
     * The meta object literal for the '<em><b>Call</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TEMPLATE_CALL__CALL = eINSTANCE.getTemplateCall_Call();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TEMPLATE_CALL__TYPE = eINSTANCE.getTemplateCall_Type();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.TemplateReferenceImpl <em>Template Reference</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.TemplateReferenceImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getTemplateReference()
     * @generated
     */
    EClass TEMPLATE_REFERENCE = eINSTANCE.getTemplateReference();

    /**
     * The meta object literal for the '<em><b>Referenced Element</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TEMPLATE_REFERENCE__REFERENCED_ELEMENT = eINSTANCE.getTemplateReference_ReferencedElement();

    /**
     * The meta object literal for the '<em><b>Tail</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TEMPLATE_REFERENCE__TAIL = eINSTANCE.getTemplateReference_Tail();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.TypeImpl <em>Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.TypeImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getType()
     * @generated
     */
    EClass TYPE = eINSTANCE.getType();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.BasicTypeImpl <em>Basic Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.BasicTypeImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getBasicType()
     * @generated
     */
    EClass BASIC_TYPE = eINSTANCE.getBasicType();

    /**
     * The meta object literal for the '<em><b>Basic</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BASIC_TYPE__BASIC = eINSTANCE.getBasicType_Basic();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.ClassTypeImpl <em>Class Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.ClassTypeImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getClassType()
     * @generated
     */
    EClass CLASS_TYPE = eINSTANCE.getClassType();

    /**
     * The meta object literal for the '<em><b>Classref</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CLASS_TYPE__CLASSREF = eINSTANCE.getClassType_Classref();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.TypedElementImpl <em>Typed Element</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.TypedElementImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getTypedElement()
     * @generated
     */
    EClass TYPED_ELEMENT = eINSTANCE.getTypedElement();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TYPED_ELEMENT__TYPE = eINSTANCE.getTypedElement_Type();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TYPED_ELEMENT__NAME = eINSTANCE.getTypedElement_Name();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.ClassImpl <em>Class</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.ClassImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getClass_()
     * @generated
     */
    EClass CLASS = eINSTANCE.getClass_();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CLASS__NAME = eINSTANCE.getClass_Name();

    /**
     * The meta object literal for the '<em><b>Extends</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CLASS__EXTENDS = eINSTANCE.getClass_Extends();

    /**
     * The meta object literal for the '<em><b>Fields</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CLASS__FIELDS = eINSTANCE.getClass_Fields();

    /**
     * The meta object literal for the '<em><b>Methods</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CLASS__METHODS = eINSTANCE.getClass_Methods();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.FieldImpl <em>Field</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.FieldImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getField()
     * @generated
     */
    EClass FIELD = eINSTANCE.getField();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.ParameterImpl <em>Parameter</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.ParameterImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getParameter()
     * @generated
     */
    EClass PARAMETER = eINSTANCE.getParameter();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.MethodImpl <em>Method</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.MethodImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getMethod()
     * @generated
     */
    EClass METHOD = eINSTANCE.getMethod();

    /**
     * The meta object literal for the '<em><b>Returntype</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference METHOD__RETURNTYPE = eINSTANCE.getMethod_Returntype();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute METHOD__NAME = eINSTANCE.getMethod_Name();

    /**
     * The meta object literal for the '<em><b>Params</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference METHOD__PARAMS = eINSTANCE.getMethod_Params();

    /**
     * The meta object literal for the '<em><b>Body</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference METHOD__BODY = eINSTANCE.getMethod_Body();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.CompositeClassImpl <em>Composite Class</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.CompositeClassImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getCompositeClass()
     * @generated
     */
    EClass COMPOSITE_CLASS = eINSTANCE.getCompositeClass();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference COMPOSITE_CLASS__NAME = eINSTANCE.getCompositeClass_Name();

    /**
     * The meta object literal for the '<em><b>Extends</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference COMPOSITE_CLASS__EXTENDS = eINSTANCE.getCompositeClass_Extends();

    /**
     * The meta object literal for the '<em><b>Compositefields</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference COMPOSITE_CLASS__COMPOSITEFIELDS = eINSTANCE.getCompositeClass_Compositefields();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.CompositeFieldImpl <em>Composite Field</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.CompositeFieldImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getCompositeField()
     * @generated
     */
    EClass COMPOSITE_FIELD = eINSTANCE.getCompositeField();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference COMPOSITE_FIELD__TYPE = eINSTANCE.getCompositeField_Type();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference COMPOSITE_FIELD__NAME = eINSTANCE.getCompositeField_Name();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.CompositeMethodImpl <em>Composite Method</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.CompositeMethodImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getCompositeMethod()
     * @generated
     */
    EClass COMPOSITE_METHOD = eINSTANCE.getCompositeMethod();

    /**
     * The meta object literal for the '<em><b>Returntype</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference COMPOSITE_METHOD__RETURNTYPE = eINSTANCE.getCompositeMethod_Returntype();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference COMPOSITE_METHOD__NAME = eINSTANCE.getCompositeMethod_Name();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.MethodBodyImpl <em>Method Body</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.MethodBodyImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getMethodBody()
     * @generated
     */
    EClass METHOD_BODY = eINSTANCE.getMethodBody();

    /**
     * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference METHOD_BODY__EXPRESSION = eINSTANCE.getMethodBody_Expression();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.ExpressionImpl <em>Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.ExpressionImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getExpression()
     * @generated
     */
    EClass EXPRESSION = eINSTANCE.getExpression();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.MessageImpl <em>Message</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.MessageImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getMessage()
     * @generated
     */
    EClass MESSAGE = eINSTANCE.getMessage();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.MethodCallImpl <em>Method Call</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.MethodCallImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getMethodCall()
     * @generated
     */
    EClass METHOD_CALL = eINSTANCE.getMethodCall();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference METHOD_CALL__NAME = eINSTANCE.getMethodCall_Name();

    /**
     * The meta object literal for the '<em><b>Args</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference METHOD_CALL__ARGS = eINSTANCE.getMethodCall_Args();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.FieldSelectionImpl <em>Field Selection</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.FieldSelectionImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getFieldSelection()
     * @generated
     */
    EClass FIELD_SELECTION = eINSTANCE.getFieldSelection();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FIELD_SELECTION__NAME = eINSTANCE.getFieldSelection_Name();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.ThisImpl <em>This</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.ThisImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getThis()
     * @generated
     */
    EClass THIS = eINSTANCE.getThis();

    /**
     * The meta object literal for the '<em><b>Variable</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute THIS__VARIABLE = eINSTANCE.getThis_Variable();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.VariableImpl <em>Variable</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.VariableImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getVariable()
     * @generated
     */
    EClass VARIABLE = eINSTANCE.getVariable();

    /**
     * The meta object literal for the '<em><b>Paramref</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VARIABLE__PARAMREF = eINSTANCE.getVariable_Paramref();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.NewImpl <em>New</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.NewImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getNew()
     * @generated
     */
    EClass NEW = eINSTANCE.getNew();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NEW__TYPE = eINSTANCE.getNew_Type();

    /**
     * The meta object literal for the '<em><b>Args</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NEW__ARGS = eINSTANCE.getNew_Args();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.CastImpl <em>Cast</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.CastImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getCast()
     * @generated
     */
    EClass CAST = eINSTANCE.getCast();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CAST__TYPE = eINSTANCE.getCast_Type();

    /**
     * The meta object literal for the '<em><b>Object</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CAST__OBJECT = eINSTANCE.getCast_Object();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.ConstantImpl <em>Constant</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.ConstantImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getConstant()
     * @generated
     */
    EClass CONSTANT = eINSTANCE.getConstant();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.StringConstantImpl <em>String Constant</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.StringConstantImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getStringConstant()
     * @generated
     */
    EClass STRING_CONSTANT = eINSTANCE.getStringConstant();

    /**
     * The meta object literal for the '<em><b>Constant</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute STRING_CONSTANT__CONSTANT = eINSTANCE.getStringConstant_Constant();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.IntConstantImpl <em>Int Constant</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.IntConstantImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getIntConstant()
     * @generated
     */
    EClass INT_CONSTANT = eINSTANCE.getIntConstant();

    /**
     * The meta object literal for the '<em><b>Constant</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INT_CONSTANT__CONSTANT = eINSTANCE.getIntConstant_Constant();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.BoolConstantImpl <em>Bool Constant</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.BoolConstantImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getBoolConstant()
     * @generated
     */
    EClass BOOL_CONSTANT = eINSTANCE.getBoolConstant();

    /**
     * The meta object literal for the '<em><b>Constant</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BOOL_CONSTANT__CONSTANT = eINSTANCE.getBoolConstant_Constant();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.ArgumentImpl <em>Argument</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.ArgumentImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getArgument()
     * @generated
     */
    EClass ARGUMENT = eINSTANCE.getArgument();

    /**
     * The meta object literal for the '{@link co.edu.javeriana.cmctl.impl.SelectionImpl <em>Selection</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see co.edu.javeriana.cmctl.impl.SelectionImpl
     * @see co.edu.javeriana.cmctl.impl.CmctlPackageImpl#getSelection()
     * @generated
     */
    EClass SELECTION = eINSTANCE.getSelection();

    /**
     * The meta object literal for the '<em><b>Receiver</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SELECTION__RECEIVER = eINSTANCE.getSelection_Receiver();

    /**
     * The meta object literal for the '<em><b>Message</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SELECTION__MESSAGE = eINSTANCE.getSelection_Message();

  }

} //CmctlPackage
