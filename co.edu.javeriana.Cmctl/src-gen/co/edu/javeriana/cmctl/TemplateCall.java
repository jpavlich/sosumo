/**
 */
package co.edu.javeriana.cmctl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Template Call</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link co.edu.javeriana.cmctl.TemplateCall#getCall <em>Call</em>}</li>
 *   <li>{@link co.edu.javeriana.cmctl.TemplateCall#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see co.edu.javeriana.cmctl.CmctlPackage#getTemplateCall()
 * @model
 * @generated
 */
public interface TemplateCall extends EObject
{
  /**
   * Returns the value of the '<em><b>Call</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Call</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Call</em>' attribute.
   * @see #setCall(String)
   * @see co.edu.javeriana.cmctl.CmctlPackage#getTemplateCall_Call()
   * @model
   * @generated
   */
  String getCall();

  /**
   * Sets the value of the '{@link co.edu.javeriana.cmctl.TemplateCall#getCall <em>Call</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Call</em>' attribute.
   * @see #getCall()
   * @generated
   */
  void setCall(String value);

  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(TemplateReference)
   * @see co.edu.javeriana.cmctl.CmctlPackage#getTemplateCall_Type()
   * @model containment="true"
   * @generated
   */
  TemplateReference getType();

  /**
   * Sets the value of the '{@link co.edu.javeriana.cmctl.TemplateCall#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(TemplateReference value);

} // TemplateCall
