/**
 */
package co.edu.javeriana.cmctl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Composite Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link co.edu.javeriana.cmctl.CompositeClass#getName <em>Name</em>}</li>
 *   <li>{@link co.edu.javeriana.cmctl.CompositeClass#getExtends <em>Extends</em>}</li>
 *   <li>{@link co.edu.javeriana.cmctl.CompositeClass#getCompositefields <em>Compositefields</em>}</li>
 * </ul>
 *
 * @see co.edu.javeriana.cmctl.CmctlPackage#getCompositeClass()
 * @model
 * @generated
 */
public interface CompositeClass extends CompositeStatement
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' containment reference.
   * @see #setName(TemplateCall)
   * @see co.edu.javeriana.cmctl.CmctlPackage#getCompositeClass_Name()
   * @model containment="true"
   * @generated
   */
  TemplateCall getName();

  /**
   * Sets the value of the '{@link co.edu.javeriana.cmctl.CompositeClass#getName <em>Name</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' containment reference.
   * @see #getName()
   * @generated
   */
  void setName(TemplateCall value);

  /**
   * Returns the value of the '<em><b>Extends</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Extends</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Extends</em>' reference.
   * @see #setExtends(co.edu.javeriana.cmctl.Class)
   * @see co.edu.javeriana.cmctl.CmctlPackage#getCompositeClass_Extends()
   * @model
   * @generated
   */
  co.edu.javeriana.cmctl.Class getExtends();

  /**
   * Sets the value of the '{@link co.edu.javeriana.cmctl.CompositeClass#getExtends <em>Extends</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Extends</em>' reference.
   * @see #getExtends()
   * @generated
   */
  void setExtends(co.edu.javeriana.cmctl.Class value);

  /**
   * Returns the value of the '<em><b>Compositefields</b></em>' containment reference list.
   * The list contents are of type {@link co.edu.javeriana.cmctl.CompositeField}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Compositefields</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Compositefields</em>' containment reference list.
   * @see co.edu.javeriana.cmctl.CmctlPackage#getCompositeClass_Compositefields()
   * @model containment="true"
   * @generated
   */
  EList<CompositeField> getCompositefields();

} // CompositeClass
