/**
 */
package co.edu.javeriana.cmctl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see co.edu.javeriana.cmctl.CmctlPackage#getType()
 * @model
 * @generated
 */
public interface Type extends EObject
{
} // Type
