/**
 */
package co.edu.javeriana.cmctl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link co.edu.javeriana.cmctl.ClassType#getClassref <em>Classref</em>}</li>
 * </ul>
 *
 * @see co.edu.javeriana.cmctl.CmctlPackage#getClassType()
 * @model
 * @generated
 */
public interface ClassType extends Type
{
  /**
   * Returns the value of the '<em><b>Classref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Classref</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Classref</em>' reference.
   * @see #setClassref(co.edu.javeriana.cmctl.Class)
   * @see co.edu.javeriana.cmctl.CmctlPackage#getClassType_Classref()
   * @model
   * @generated
   */
  co.edu.javeriana.cmctl.Class getClassref();

  /**
   * Sets the value of the '{@link co.edu.javeriana.cmctl.ClassType#getClassref <em>Classref</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Classref</em>' reference.
   * @see #getClassref()
   * @generated
   */
  void setClassref(co.edu.javeriana.cmctl.Class value);

} // ClassType
