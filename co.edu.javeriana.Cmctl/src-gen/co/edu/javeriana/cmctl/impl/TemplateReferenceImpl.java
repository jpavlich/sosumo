/**
 */
package co.edu.javeriana.cmctl.impl;

import co.edu.javeriana.cmctl.CmctlPackage;
import co.edu.javeriana.cmctl.TemplateReference;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Template Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link co.edu.javeriana.cmctl.impl.TemplateReferenceImpl#getReferencedElement <em>Referenced Element</em>}</li>
 *   <li>{@link co.edu.javeriana.cmctl.impl.TemplateReferenceImpl#getTail <em>Tail</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TemplateReferenceImpl extends MinimalEObjectImpl.Container implements TemplateReference
{
  /**
   * The cached value of the '{@link #getReferencedElement() <em>Referenced Element</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getReferencedElement()
   * @generated
   * @ordered
   */
  protected ETypedElement referencedElement;

  /**
   * The cached value of the '{@link #getTail() <em>Tail</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTail()
   * @generated
   * @ordered
   */
  protected TemplateReference tail;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TemplateReferenceImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return CmctlPackage.Literals.TEMPLATE_REFERENCE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ETypedElement getReferencedElement()
  {
    if (referencedElement != null && referencedElement.eIsProxy())
    {
      InternalEObject oldReferencedElement = (InternalEObject)referencedElement;
      referencedElement = (ETypedElement)eResolveProxy(oldReferencedElement);
      if (referencedElement != oldReferencedElement)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, CmctlPackage.TEMPLATE_REFERENCE__REFERENCED_ELEMENT, oldReferencedElement, referencedElement));
      }
    }
    return referencedElement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ETypedElement basicGetReferencedElement()
  {
    return referencedElement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setReferencedElement(ETypedElement newReferencedElement)
  {
    ETypedElement oldReferencedElement = referencedElement;
    referencedElement = newReferencedElement;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CmctlPackage.TEMPLATE_REFERENCE__REFERENCED_ELEMENT, oldReferencedElement, referencedElement));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TemplateReference getTail()
  {
    return tail;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTail(TemplateReference newTail, NotificationChain msgs)
  {
    TemplateReference oldTail = tail;
    tail = newTail;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CmctlPackage.TEMPLATE_REFERENCE__TAIL, oldTail, newTail);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTail(TemplateReference newTail)
  {
    if (newTail != tail)
    {
      NotificationChain msgs = null;
      if (tail != null)
        msgs = ((InternalEObject)tail).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CmctlPackage.TEMPLATE_REFERENCE__TAIL, null, msgs);
      if (newTail != null)
        msgs = ((InternalEObject)newTail).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CmctlPackage.TEMPLATE_REFERENCE__TAIL, null, msgs);
      msgs = basicSetTail(newTail, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CmctlPackage.TEMPLATE_REFERENCE__TAIL, newTail, newTail));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case CmctlPackage.TEMPLATE_REFERENCE__TAIL:
        return basicSetTail(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case CmctlPackage.TEMPLATE_REFERENCE__REFERENCED_ELEMENT:
        if (resolve) return getReferencedElement();
        return basicGetReferencedElement();
      case CmctlPackage.TEMPLATE_REFERENCE__TAIL:
        return getTail();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case CmctlPackage.TEMPLATE_REFERENCE__REFERENCED_ELEMENT:
        setReferencedElement((ETypedElement)newValue);
        return;
      case CmctlPackage.TEMPLATE_REFERENCE__TAIL:
        setTail((TemplateReference)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case CmctlPackage.TEMPLATE_REFERENCE__REFERENCED_ELEMENT:
        setReferencedElement((ETypedElement)null);
        return;
      case CmctlPackage.TEMPLATE_REFERENCE__TAIL:
        setTail((TemplateReference)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case CmctlPackage.TEMPLATE_REFERENCE__REFERENCED_ELEMENT:
        return referencedElement != null;
      case CmctlPackage.TEMPLATE_REFERENCE__TAIL:
        return tail != null;
    }
    return super.eIsSet(featureID);
  }

} //TemplateReferenceImpl
