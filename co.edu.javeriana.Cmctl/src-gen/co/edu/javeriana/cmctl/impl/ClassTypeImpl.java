/**
 */
package co.edu.javeriana.cmctl.impl;

import co.edu.javeriana.cmctl.ClassType;
import co.edu.javeriana.cmctl.CmctlPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Class Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link co.edu.javeriana.cmctl.impl.ClassTypeImpl#getClassref <em>Classref</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ClassTypeImpl extends TypeImpl implements ClassType
{
  /**
   * The cached value of the '{@link #getClassref() <em>Classref</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getClassref()
   * @generated
   * @ordered
   */
  protected co.edu.javeriana.cmctl.Class classref;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ClassTypeImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return CmctlPackage.Literals.CLASS_TYPE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public co.edu.javeriana.cmctl.Class getClassref()
  {
    if (classref != null && classref.eIsProxy())
    {
      InternalEObject oldClassref = (InternalEObject)classref;
      classref = (co.edu.javeriana.cmctl.Class)eResolveProxy(oldClassref);
      if (classref != oldClassref)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, CmctlPackage.CLASS_TYPE__CLASSREF, oldClassref, classref));
      }
    }
    return classref;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public co.edu.javeriana.cmctl.Class basicGetClassref()
  {
    return classref;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setClassref(co.edu.javeriana.cmctl.Class newClassref)
  {
    co.edu.javeriana.cmctl.Class oldClassref = classref;
    classref = newClassref;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CmctlPackage.CLASS_TYPE__CLASSREF, oldClassref, classref));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case CmctlPackage.CLASS_TYPE__CLASSREF:
        if (resolve) return getClassref();
        return basicGetClassref();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case CmctlPackage.CLASS_TYPE__CLASSREF:
        setClassref((co.edu.javeriana.cmctl.Class)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case CmctlPackage.CLASS_TYPE__CLASSREF:
        setClassref((co.edu.javeriana.cmctl.Class)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case CmctlPackage.CLASS_TYPE__CLASSREF:
        return classref != null;
    }
    return super.eIsSet(featureID);
  }

} //ClassTypeImpl
