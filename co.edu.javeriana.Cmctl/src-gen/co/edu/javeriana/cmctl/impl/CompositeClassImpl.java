/**
 */
package co.edu.javeriana.cmctl.impl;

import co.edu.javeriana.cmctl.CmctlPackage;
import co.edu.javeriana.cmctl.CompositeClass;
import co.edu.javeriana.cmctl.CompositeField;
import co.edu.javeriana.cmctl.TemplateCall;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Composite Class</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link co.edu.javeriana.cmctl.impl.CompositeClassImpl#getName <em>Name</em>}</li>
 *   <li>{@link co.edu.javeriana.cmctl.impl.CompositeClassImpl#getExtends <em>Extends</em>}</li>
 *   <li>{@link co.edu.javeriana.cmctl.impl.CompositeClassImpl#getCompositefields <em>Compositefields</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CompositeClassImpl extends CompositeStatementImpl implements CompositeClass
{
  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected TemplateCall name;

  /**
   * The cached value of the '{@link #getExtends() <em>Extends</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExtends()
   * @generated
   * @ordered
   */
  protected co.edu.javeriana.cmctl.Class extends_;

  /**
   * The cached value of the '{@link #getCompositefields() <em>Compositefields</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCompositefields()
   * @generated
   * @ordered
   */
  protected EList<CompositeField> compositefields;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected CompositeClassImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return CmctlPackage.Literals.COMPOSITE_CLASS;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TemplateCall getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetName(TemplateCall newName, NotificationChain msgs)
  {
    TemplateCall oldName = name;
    name = newName;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CmctlPackage.COMPOSITE_CLASS__NAME, oldName, newName);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(TemplateCall newName)
  {
    if (newName != name)
    {
      NotificationChain msgs = null;
      if (name != null)
        msgs = ((InternalEObject)name).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CmctlPackage.COMPOSITE_CLASS__NAME, null, msgs);
      if (newName != null)
        msgs = ((InternalEObject)newName).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CmctlPackage.COMPOSITE_CLASS__NAME, null, msgs);
      msgs = basicSetName(newName, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CmctlPackage.COMPOSITE_CLASS__NAME, newName, newName));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public co.edu.javeriana.cmctl.Class getExtends()
  {
    if (extends_ != null && extends_.eIsProxy())
    {
      InternalEObject oldExtends = (InternalEObject)extends_;
      extends_ = (co.edu.javeriana.cmctl.Class)eResolveProxy(oldExtends);
      if (extends_ != oldExtends)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, CmctlPackage.COMPOSITE_CLASS__EXTENDS, oldExtends, extends_));
      }
    }
    return extends_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public co.edu.javeriana.cmctl.Class basicGetExtends()
  {
    return extends_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setExtends(co.edu.javeriana.cmctl.Class newExtends)
  {
    co.edu.javeriana.cmctl.Class oldExtends = extends_;
    extends_ = newExtends;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CmctlPackage.COMPOSITE_CLASS__EXTENDS, oldExtends, extends_));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<CompositeField> getCompositefields()
  {
    if (compositefields == null)
    {
      compositefields = new EObjectContainmentEList<CompositeField>(CompositeField.class, this, CmctlPackage.COMPOSITE_CLASS__COMPOSITEFIELDS);
    }
    return compositefields;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case CmctlPackage.COMPOSITE_CLASS__NAME:
        return basicSetName(null, msgs);
      case CmctlPackage.COMPOSITE_CLASS__COMPOSITEFIELDS:
        return ((InternalEList<?>)getCompositefields()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case CmctlPackage.COMPOSITE_CLASS__NAME:
        return getName();
      case CmctlPackage.COMPOSITE_CLASS__EXTENDS:
        if (resolve) return getExtends();
        return basicGetExtends();
      case CmctlPackage.COMPOSITE_CLASS__COMPOSITEFIELDS:
        return getCompositefields();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case CmctlPackage.COMPOSITE_CLASS__NAME:
        setName((TemplateCall)newValue);
        return;
      case CmctlPackage.COMPOSITE_CLASS__EXTENDS:
        setExtends((co.edu.javeriana.cmctl.Class)newValue);
        return;
      case CmctlPackage.COMPOSITE_CLASS__COMPOSITEFIELDS:
        getCompositefields().clear();
        getCompositefields().addAll((Collection<? extends CompositeField>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case CmctlPackage.COMPOSITE_CLASS__NAME:
        setName((TemplateCall)null);
        return;
      case CmctlPackage.COMPOSITE_CLASS__EXTENDS:
        setExtends((co.edu.javeriana.cmctl.Class)null);
        return;
      case CmctlPackage.COMPOSITE_CLASS__COMPOSITEFIELDS:
        getCompositefields().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case CmctlPackage.COMPOSITE_CLASS__NAME:
        return name != null;
      case CmctlPackage.COMPOSITE_CLASS__EXTENDS:
        return extends_ != null;
      case CmctlPackage.COMPOSITE_CLASS__COMPOSITEFIELDS:
        return compositefields != null && !compositefields.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //CompositeClassImpl
