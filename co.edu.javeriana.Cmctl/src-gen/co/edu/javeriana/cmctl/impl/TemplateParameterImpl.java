/**
 */
package co.edu.javeriana.cmctl.impl;

import co.edu.javeriana.cmctl.CmctlPackage;
import co.edu.javeriana.cmctl.TemplateParameter;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ETypedElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Template Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TemplateParameterImpl extends ETypedElementImpl implements TemplateParameter
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TemplateParameterImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return CmctlPackage.Literals.TEMPLATE_PARAMETER;
  }

} //TemplateParameterImpl
