/**
 */
package co.edu.javeriana.cmctl.impl;

import co.edu.javeriana.cmctl.CmctlPackage;
import co.edu.javeriana.cmctl.Parameter;
import co.edu.javeriana.cmctl.Variable;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Variable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link co.edu.javeriana.cmctl.impl.VariableImpl#getParamref <em>Paramref</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VariableImpl extends ExpressionImpl implements Variable
{
  /**
   * The cached value of the '{@link #getParamref() <em>Paramref</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParamref()
   * @generated
   * @ordered
   */
  protected Parameter paramref;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected VariableImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return CmctlPackage.Literals.VARIABLE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Parameter getParamref()
  {
    if (paramref != null && paramref.eIsProxy())
    {
      InternalEObject oldParamref = (InternalEObject)paramref;
      paramref = (Parameter)eResolveProxy(oldParamref);
      if (paramref != oldParamref)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, CmctlPackage.VARIABLE__PARAMREF, oldParamref, paramref));
      }
    }
    return paramref;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Parameter basicGetParamref()
  {
    return paramref;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setParamref(Parameter newParamref)
  {
    Parameter oldParamref = paramref;
    paramref = newParamref;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CmctlPackage.VARIABLE__PARAMREF, oldParamref, paramref));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case CmctlPackage.VARIABLE__PARAMREF:
        if (resolve) return getParamref();
        return basicGetParamref();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case CmctlPackage.VARIABLE__PARAMREF:
        setParamref((Parameter)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case CmctlPackage.VARIABLE__PARAMREF:
        setParamref((Parameter)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case CmctlPackage.VARIABLE__PARAMREF:
        return paramref != null;
    }
    return super.eIsSet(featureID);
  }

} //VariableImpl
