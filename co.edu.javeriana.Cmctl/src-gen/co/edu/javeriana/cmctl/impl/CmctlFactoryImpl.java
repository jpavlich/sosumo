/**
 */
package co.edu.javeriana.cmctl.impl;

import co.edu.javeriana.cmctl.Argument;
import co.edu.javeriana.cmctl.BasicType;
import co.edu.javeriana.cmctl.BoolConstant;
import co.edu.javeriana.cmctl.Cast;
import co.edu.javeriana.cmctl.ClassType;
import co.edu.javeriana.cmctl.CmctlFactory;
import co.edu.javeriana.cmctl.CmctlPackage;
import co.edu.javeriana.cmctl.CompositeClass;
import co.edu.javeriana.cmctl.CompositeField;
import co.edu.javeriana.cmctl.CompositeMethod;
import co.edu.javeriana.cmctl.CompositeStatement;
import co.edu.javeriana.cmctl.Constant;
import co.edu.javeriana.cmctl.Expression;
import co.edu.javeriana.cmctl.Field;
import co.edu.javeriana.cmctl.FieldSelection;
import co.edu.javeriana.cmctl.ForTemplate;
import co.edu.javeriana.cmctl.IfTemplate;
import co.edu.javeriana.cmctl.Import;
import co.edu.javeriana.cmctl.IntConstant;
import co.edu.javeriana.cmctl.Message;
import co.edu.javeriana.cmctl.Method;
import co.edu.javeriana.cmctl.MethodBody;
import co.edu.javeriana.cmctl.MethodCall;
import co.edu.javeriana.cmctl.New;
import co.edu.javeriana.cmctl.Parameter;
import co.edu.javeriana.cmctl.Selection;
import co.edu.javeriana.cmctl.StringConstant;
import co.edu.javeriana.cmctl.Template;
import co.edu.javeriana.cmctl.TemplateCall;
import co.edu.javeriana.cmctl.TemplateFunction;
import co.edu.javeriana.cmctl.TemplateParameter;
import co.edu.javeriana.cmctl.TemplateReference;
import co.edu.javeriana.cmctl.TemplateStatement;
import co.edu.javeriana.cmctl.This;
import co.edu.javeriana.cmctl.Type;
import co.edu.javeriana.cmctl.TypedElement;
import co.edu.javeriana.cmctl.Variable;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CmctlFactoryImpl extends EFactoryImpl implements CmctlFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static CmctlFactory init()
  {
    try
    {
      CmctlFactory theCmctlFactory = (CmctlFactory)EPackage.Registry.INSTANCE.getEFactory(CmctlPackage.eNS_URI);
      if (theCmctlFactory != null)
      {
        return theCmctlFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new CmctlFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CmctlFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case CmctlPackage.TEMPLATE: return createTemplate();
      case CmctlPackage.IMPORT: return createImport();
      case CmctlPackage.TEMPLATE_FUNCTION: return createTemplateFunction();
      case CmctlPackage.TEMPLATE_PARAMETER: return createTemplateParameter();
      case CmctlPackage.COMPOSITE_STATEMENT: return createCompositeStatement();
      case CmctlPackage.TEMPLATE_STATEMENT: return createTemplateStatement();
      case CmctlPackage.IF_TEMPLATE: return createIfTemplate();
      case CmctlPackage.FOR_TEMPLATE: return createForTemplate();
      case CmctlPackage.TEMPLATE_CALL: return createTemplateCall();
      case CmctlPackage.TEMPLATE_REFERENCE: return createTemplateReference();
      case CmctlPackage.TYPE: return createType();
      case CmctlPackage.BASIC_TYPE: return createBasicType();
      case CmctlPackage.CLASS_TYPE: return createClassType();
      case CmctlPackage.TYPED_ELEMENT: return createTypedElement();
      case CmctlPackage.CLASS: return createClass();
      case CmctlPackage.FIELD: return createField();
      case CmctlPackage.PARAMETER: return createParameter();
      case CmctlPackage.METHOD: return createMethod();
      case CmctlPackage.COMPOSITE_CLASS: return createCompositeClass();
      case CmctlPackage.COMPOSITE_FIELD: return createCompositeField();
      case CmctlPackage.COMPOSITE_METHOD: return createCompositeMethod();
      case CmctlPackage.METHOD_BODY: return createMethodBody();
      case CmctlPackage.EXPRESSION: return createExpression();
      case CmctlPackage.MESSAGE: return createMessage();
      case CmctlPackage.METHOD_CALL: return createMethodCall();
      case CmctlPackage.FIELD_SELECTION: return createFieldSelection();
      case CmctlPackage.THIS: return createThis();
      case CmctlPackage.VARIABLE: return createVariable();
      case CmctlPackage.NEW: return createNew();
      case CmctlPackage.CAST: return createCast();
      case CmctlPackage.CONSTANT: return createConstant();
      case CmctlPackage.STRING_CONSTANT: return createStringConstant();
      case CmctlPackage.INT_CONSTANT: return createIntConstant();
      case CmctlPackage.BOOL_CONSTANT: return createBoolConstant();
      case CmctlPackage.ARGUMENT: return createArgument();
      case CmctlPackage.SELECTION: return createSelection();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Template createTemplate()
  {
    TemplateImpl template = new TemplateImpl();
    return template;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Import createImport()
  {
    ImportImpl import_ = new ImportImpl();
    return import_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TemplateFunction createTemplateFunction()
  {
    TemplateFunctionImpl templateFunction = new TemplateFunctionImpl();
    return templateFunction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TemplateParameter createTemplateParameter()
  {
    TemplateParameterImpl templateParameter = new TemplateParameterImpl();
    return templateParameter;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CompositeStatement createCompositeStatement()
  {
    CompositeStatementImpl compositeStatement = new CompositeStatementImpl();
    return compositeStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TemplateStatement createTemplateStatement()
  {
    TemplateStatementImpl templateStatement = new TemplateStatementImpl();
    return templateStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IfTemplate createIfTemplate()
  {
    IfTemplateImpl ifTemplate = new IfTemplateImpl();
    return ifTemplate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ForTemplate createForTemplate()
  {
    ForTemplateImpl forTemplate = new ForTemplateImpl();
    return forTemplate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TemplateCall createTemplateCall()
  {
    TemplateCallImpl templateCall = new TemplateCallImpl();
    return templateCall;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TemplateReference createTemplateReference()
  {
    TemplateReferenceImpl templateReference = new TemplateReferenceImpl();
    return templateReference;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Type createType()
  {
    TypeImpl type = new TypeImpl();
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BasicType createBasicType()
  {
    BasicTypeImpl basicType = new BasicTypeImpl();
    return basicType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ClassType createClassType()
  {
    ClassTypeImpl classType = new ClassTypeImpl();
    return classType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TypedElement createTypedElement()
  {
    TypedElementImpl typedElement = new TypedElementImpl();
    return typedElement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public co.edu.javeriana.cmctl.Class createClass()
  {
    ClassImpl class_ = new ClassImpl();
    return class_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Field createField()
  {
    FieldImpl field = new FieldImpl();
    return field;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Parameter createParameter()
  {
    ParameterImpl parameter = new ParameterImpl();
    return parameter;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Method createMethod()
  {
    MethodImpl method = new MethodImpl();
    return method;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CompositeClass createCompositeClass()
  {
    CompositeClassImpl compositeClass = new CompositeClassImpl();
    return compositeClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CompositeField createCompositeField()
  {
    CompositeFieldImpl compositeField = new CompositeFieldImpl();
    return compositeField;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CompositeMethod createCompositeMethod()
  {
    CompositeMethodImpl compositeMethod = new CompositeMethodImpl();
    return compositeMethod;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MethodBody createMethodBody()
  {
    MethodBodyImpl methodBody = new MethodBodyImpl();
    return methodBody;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Expression createExpression()
  {
    ExpressionImpl expression = new ExpressionImpl();
    return expression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Message createMessage()
  {
    MessageImpl message = new MessageImpl();
    return message;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MethodCall createMethodCall()
  {
    MethodCallImpl methodCall = new MethodCallImpl();
    return methodCall;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FieldSelection createFieldSelection()
  {
    FieldSelectionImpl fieldSelection = new FieldSelectionImpl();
    return fieldSelection;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public This createThis()
  {
    ThisImpl this_ = new ThisImpl();
    return this_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Variable createVariable()
  {
    VariableImpl variable = new VariableImpl();
    return variable;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public New createNew()
  {
    NewImpl new_ = new NewImpl();
    return new_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Cast createCast()
  {
    CastImpl cast = new CastImpl();
    return cast;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Constant createConstant()
  {
    ConstantImpl constant = new ConstantImpl();
    return constant;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StringConstant createStringConstant()
  {
    StringConstantImpl stringConstant = new StringConstantImpl();
    return stringConstant;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IntConstant createIntConstant()
  {
    IntConstantImpl intConstant = new IntConstantImpl();
    return intConstant;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BoolConstant createBoolConstant()
  {
    BoolConstantImpl boolConstant = new BoolConstantImpl();
    return boolConstant;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Argument createArgument()
  {
    ArgumentImpl argument = new ArgumentImpl();
    return argument;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Selection createSelection()
  {
    SelectionImpl selection = new SelectionImpl();
    return selection;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CmctlPackage getCmctlPackage()
  {
    return (CmctlPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static CmctlPackage getPackage()
  {
    return CmctlPackage.eINSTANCE;
  }

} //CmctlFactoryImpl
