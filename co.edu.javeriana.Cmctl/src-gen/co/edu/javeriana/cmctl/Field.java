/**
 */
package co.edu.javeriana.cmctl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Field</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see co.edu.javeriana.cmctl.CmctlPackage#getField()
 * @model
 * @generated
 */
public interface Field extends TypedElement
{
} // Field
