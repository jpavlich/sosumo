/**
 */
package co.edu.javeriana.cmctl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constant</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see co.edu.javeriana.cmctl.CmctlPackage#getConstant()
 * @model
 * @generated
 */
public interface Constant extends Expression
{
} // Constant
