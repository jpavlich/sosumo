/**
 */
package co.edu.javeriana.cmctl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Method Call</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link co.edu.javeriana.cmctl.MethodCall#getName <em>Name</em>}</li>
 *   <li>{@link co.edu.javeriana.cmctl.MethodCall#getArgs <em>Args</em>}</li>
 * </ul>
 *
 * @see co.edu.javeriana.cmctl.CmctlPackage#getMethodCall()
 * @model
 * @generated
 */
public interface MethodCall extends Message
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' reference.
   * @see #setName(Method)
   * @see co.edu.javeriana.cmctl.CmctlPackage#getMethodCall_Name()
   * @model
   * @generated
   */
  Method getName();

  /**
   * Sets the value of the '{@link co.edu.javeriana.cmctl.MethodCall#getName <em>Name</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' reference.
   * @see #getName()
   * @generated
   */
  void setName(Method value);

  /**
   * Returns the value of the '<em><b>Args</b></em>' containment reference list.
   * The list contents are of type {@link co.edu.javeriana.cmctl.Argument}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Args</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Args</em>' containment reference list.
   * @see co.edu.javeriana.cmctl.CmctlPackage#getMethodCall_Args()
   * @model containment="true"
   * @generated
   */
  EList<Argument> getArgs();

} // MethodCall
