/**
 */
package co.edu.javeriana.cmctl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Basic Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link co.edu.javeriana.cmctl.BasicType#getBasic <em>Basic</em>}</li>
 * </ul>
 *
 * @see co.edu.javeriana.cmctl.CmctlPackage#getBasicType()
 * @model
 * @generated
 */
public interface BasicType extends Type
{
  /**
   * Returns the value of the '<em><b>Basic</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Basic</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Basic</em>' attribute.
   * @see #setBasic(String)
   * @see co.edu.javeriana.cmctl.CmctlPackage#getBasicType_Basic()
   * @model
   * @generated
   */
  String getBasic();

  /**
   * Sets the value of the '{@link co.edu.javeriana.cmctl.BasicType#getBasic <em>Basic</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Basic</em>' attribute.
   * @see #getBasic()
   * @generated
   */
  void setBasic(String value);

} // BasicType
