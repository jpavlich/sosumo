/**
 */
package co.edu.javeriana.cmctl;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.ETypedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Template Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link co.edu.javeriana.cmctl.TemplateReference#getReferencedElement <em>Referenced Element</em>}</li>
 *   <li>{@link co.edu.javeriana.cmctl.TemplateReference#getTail <em>Tail</em>}</li>
 * </ul>
 *
 * @see co.edu.javeriana.cmctl.CmctlPackage#getTemplateReference()
 * @model
 * @generated
 */
public interface TemplateReference extends EObject
{
  /**
   * Returns the value of the '<em><b>Referenced Element</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Referenced Element</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Referenced Element</em>' reference.
   * @see #setReferencedElement(ETypedElement)
   * @see co.edu.javeriana.cmctl.CmctlPackage#getTemplateReference_ReferencedElement()
   * @model
   * @generated
   */
  ETypedElement getReferencedElement();

  /**
   * Sets the value of the '{@link co.edu.javeriana.cmctl.TemplateReference#getReferencedElement <em>Referenced Element</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Referenced Element</em>' reference.
   * @see #getReferencedElement()
   * @generated
   */
  void setReferencedElement(ETypedElement value);

  /**
   * Returns the value of the '<em><b>Tail</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Tail</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Tail</em>' containment reference.
   * @see #setTail(TemplateReference)
   * @see co.edu.javeriana.cmctl.CmctlPackage#getTemplateReference_Tail()
   * @model containment="true"
   * @generated
   */
  TemplateReference getTail();

  /**
   * Sets the value of the '{@link co.edu.javeriana.cmctl.TemplateReference#getTail <em>Tail</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Tail</em>' containment reference.
   * @see #getTail()
   * @generated
   */
  void setTail(TemplateReference value);

} // TemplateReference
