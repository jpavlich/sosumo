package adaptertestplugin;

import java.lang.reflect.Method;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EFactory;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EContentAdapter;

import fjava.FjavaFactory;
import fjava.FjavaPackage;
import miniuml.MiniumlFactory;
import miniuml.MiniumlPackage;

public class CentralModelAdapter extends EContentAdapter {
	private boolean inProgress = false;
	private EFactory[] factories = new EFactory[] { MiniumlFactory.eINSTANCE, FjavaFactory.eINSTANCE };

	private EPackage[] packages = new EPackage[] { MiniumlPackage.eINSTANCE, FjavaPackage.eINSTANCE };
	
	private static CentralModelAdapter INSTANCE;
	public static CentralModelAdapter getInstance()  {
		if (INSTANCE == null) {
			INSTANCE = new CentralModelAdapter();
		}
		return INSTANCE;
	}

	@Override
	public void notifyChanged(Notification notification) {
		super.notifyChanged(notification);
		EObject notifier = (EObject) notification.getNotifier();
		System.out.println(notification.getEventType() + " " + notification.getNewValue());
		if (notifier.eResource() != null && !((Resource.Internal) notifier.eResource()).isLoading() && !inProgress) {
			inProgress = true;
			if (notification.getEventType() == Notification.ADD) {
				EObject container = (EObject) notification.getNotifier();
				EStructuralFeature feature = (EStructuralFeature) notification.getFeature();
				EObject value = (EObject) notification.getNewValue();
				EObject oppositeContainer = (EObject) getOpposite(container);
				EStructuralFeature oppositeFeature = (EStructuralFeature) getOpposite(feature);
				EClass oppositeClass = (EClass) getOpposite(value.eClass());

				EObject oppositeValue = create(oppositeClass);

				System.out.println(container);
				System.out.println(feature);
				System.out.println(value);
				System.out.println(oppositeContainer);
				System.out.println(oppositeFeature);
				System.out.println(oppositeValue);

				@SuppressWarnings("unchecked")
				EList<Object> rightList = (EList<Object>) oppositeContainer.eGet(oppositeFeature);
				rightList.add(oppositeValue);
			}
			inProgress = false;

		}
	}

	private EObject create(EClass c) {
		try {
			EFactory factory = getFactory(c);
			Method creator = factory.getClass().getMethod("create" + c.getName(), new Class<?>[0]);
			return (EObject) creator.invoke(factory, new Object[0]);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private EFactory getFactory(EClass c) {
		EPackage pkg = (EPackage) c.eContainer();
		if (pkg == packages[0]) {
			return factories[0];
		} else {
			return factories[1];
		}
	}

	private EFactory getOppositeFactory(EObject obj) {
		if (obj instanceof EClass) {
			return getOppositeFactory((EClass) obj);
		}
		return getOppositeFactory(obj.eClass());
	}

	private EFactory getOppositeFactory(EClass c) {
		EPackage pkg = (EPackage) c.eContainer();
		if (pkg == packages[0]) {
			return factories[1];
		} else {
			return factories[0];
		}
	}

	private EObject getOpposite(EObject obj) {
		EModelElement modelElement = (EModelElement) obj;
		if (modelElement.getEAnnotations().size() > 0) {
			EAnnotation annotation = modelElement.getEAnnotations().get(0);
			return annotation.getReferences().get(0);
		} else {
			return null;
		}

	}
}
