package adaptertestplugin;

import java.util.Iterator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.xtext.resource.IResourceDescriptions;
import org.eclipse.xtext.ui.resource.IResourceSetProvider;

import com.google.inject.Inject;

import miniuml.Class;
import miniuml.MiniumlFactory;
import miniuml.Model;

public class CentralModelExecutableExtensionFactoryHandler extends AbstractHandler implements IHandler {
	@Inject
	IResourceSetProvider resourceSetProvider;

	@Inject
	IResourceDescriptions resourceDescriptions;

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection structuredSelection = (IStructuredSelection) selection;
			if (structuredSelection.size() == 2) {
				Object[] selectedElements = structuredSelection.toArray();
				System.out.println(selectedElements[0].getClass());
				System.out.println(selectedElements[1].getClass());
				if (selectedElements[0] instanceof IFile && selectedElements[1] instanceof IFile) {
					IFile f1 = (IFile) selectedElements[0];
					IFile f2 = (IFile) selectedElements[1];
					IProject proj1 = f1.getProject();

					// ResourceSet rs = resourceSetProvider.get(proj1);
					ResourceSet rs = new ResourceSetImpl();

					URI uri = URI.createPlatformResourceURI(f1.getFullPath().toString(), true);
					Resource r1 = rs.getResource(uri, true);
					r1.eAdapters().add(CentralModelAdapter.getInstance());
					Iterator<EObject> it = r1.getAllContents();
					while (it.hasNext()) {
						it.next().eAdapters().add(CentralModelAdapter.getInstance());
					}

					Class c = MiniumlFactory.eINSTANCE.createClass();
					Model m = (Model) r1.getContents().iterator().next();
					m.getClasses().add(c);

					System.out.println(r1);

					URI uri2 = URI.createPlatformResourceURI(f2.getFullPath().toString(), true);
					Resource r2 = rs.getResource(uri2, true);
					r2.eAdapters().add(CentralModelAdapter.getInstance());

					Iterator<EObject> it2 = r2.getAllContents();
					while (it2.hasNext()) {
						it2.next().eAdapters().add(CentralModelAdapter.getInstance());
					}

					System.out.println(r2);
				}

			}
		}
		return null;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
	


	private EditingDomain getEditingDomainFor(EObject object) {
		Resource resource = object.eResource();
		if (resource != null) {
			IEditingDomainProvider editingDomainProvider = (IEditingDomainProvider) EcoreUtil
					.getExistingAdapter(resource, IEditingDomainProvider.class);
			if (editingDomainProvider != null) {
				return editingDomainProvider.getEditingDomain();
			} else {
				ResourceSet resourceSet = resource.getResourceSet();
				if (resourceSet instanceof IEditingDomainProvider) {
					EditingDomain editingDomain = ((IEditingDomainProvider) resourceSet).getEditingDomain();
					return editingDomain;
				} else if (resourceSet != null) {
					editingDomainProvider = (IEditingDomainProvider) EcoreUtil.getExistingAdapter(resourceSet,
							IEditingDomainProvider.class);
					if (editingDomainProvider != null) {
						return editingDomainProvider.getEditingDomain();
					}
				}
			}
		}

		return null;
	}

}
