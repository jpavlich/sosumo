/**
 */
package miniuml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Association End</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link miniuml.AssociationEnd#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see miniuml.MiniumlPackage#getAssociationEnd()
 * @model
 * @generated
 */
public interface AssociationEnd extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(miniuml.Class)
	 * @see miniuml.MiniumlPackage#getAssociationEnd_Type()
	 * @model
	 * @generated
	 */
	miniuml.Class getType();

	/**
	 * Sets the value of the '{@link miniuml.AssociationEnd#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(miniuml.Class value);

} // AssociationEnd
