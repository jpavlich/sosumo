package miniuml.text.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import miniuml.text.services.MiniumlGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMiniumlParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Model'", "'{'", "'}'", "'classes'", "','", "'Class'", "'parents'", "'('", "')'", "'attributes'", "'Attribute'"
    };
    public static final int RULE_ID=5;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__19=19;
    public static final int RULE_STRING=4;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=6;
    public static final int RULE_WS=9;

    // delegates
    // delegators


        public InternalMiniumlParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMiniumlParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMiniumlParser.tokenNames; }
    public String getGrammarFileName() { return "../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g"; }


     
     	private MiniumlGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(MiniumlGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleModel"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:60:1: entryRuleModel : ruleModel EOF ;
    public final void entryRuleModel() throws RecognitionException {
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:61:1: ( ruleModel EOF )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:62:1: ruleModel EOF
            {
             before(grammarAccess.getModelRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleModel_in_entryRuleModel61);
            ruleModel();

            state._fsp--;

             after(grammarAccess.getModelRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleModel68); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:69:1: ruleModel : ( ( rule__Model__Group__0 ) ) ;
    public final void ruleModel() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:73:2: ( ( ( rule__Model__Group__0 ) ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:74:1: ( ( rule__Model__Group__0 ) )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:74:1: ( ( rule__Model__Group__0 ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:75:1: ( rule__Model__Group__0 )
            {
             before(grammarAccess.getModelAccess().getGroup()); 
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:76:1: ( rule__Model__Group__0 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:76:2: rule__Model__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Model__Group__0_in_ruleModel94);
            rule__Model__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getModelAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleClass"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:88:1: entryRuleClass : ruleClass EOF ;
    public final void entryRuleClass() throws RecognitionException {
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:89:1: ( ruleClass EOF )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:90:1: ruleClass EOF
            {
             before(grammarAccess.getClassRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleClass_in_entryRuleClass121);
            ruleClass();

            state._fsp--;

             after(grammarAccess.getClassRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleClass128); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleClass"


    // $ANTLR start "ruleClass"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:97:1: ruleClass : ( ( rule__Class__Group__0 ) ) ;
    public final void ruleClass() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:101:2: ( ( ( rule__Class__Group__0 ) ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:102:1: ( ( rule__Class__Group__0 ) )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:102:1: ( ( rule__Class__Group__0 ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:103:1: ( rule__Class__Group__0 )
            {
             before(grammarAccess.getClassAccess().getGroup()); 
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:104:1: ( rule__Class__Group__0 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:104:2: rule__Class__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group__0_in_ruleClass154);
            rule__Class__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getClassAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleClass"


    // $ANTLR start "entryRuleEString"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:116:1: entryRuleEString : ruleEString EOF ;
    public final void entryRuleEString() throws RecognitionException {
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:117:1: ( ruleEString EOF )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:118:1: ruleEString EOF
            {
             before(grammarAccess.getEStringRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_entryRuleEString181);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getEStringRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleEString188); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:125:1: ruleEString : ( ( rule__EString__Alternatives ) ) ;
    public final void ruleEString() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:129:2: ( ( ( rule__EString__Alternatives ) ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:130:1: ( ( rule__EString__Alternatives ) )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:130:1: ( ( rule__EString__Alternatives ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:131:1: ( rule__EString__Alternatives )
            {
             before(grammarAccess.getEStringAccess().getAlternatives()); 
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:132:1: ( rule__EString__Alternatives )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:132:2: rule__EString__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__EString__Alternatives_in_ruleEString214);
            rule__EString__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getEStringAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleAttribute"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:144:1: entryRuleAttribute : ruleAttribute EOF ;
    public final void entryRuleAttribute() throws RecognitionException {
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:145:1: ( ruleAttribute EOF )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:146:1: ruleAttribute EOF
            {
             before(grammarAccess.getAttributeRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleAttribute_in_entryRuleAttribute241);
            ruleAttribute();

            state._fsp--;

             after(grammarAccess.getAttributeRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleAttribute248); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAttribute"


    // $ANTLR start "ruleAttribute"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:153:1: ruleAttribute : ( ( rule__Attribute__Group__0 ) ) ;
    public final void ruleAttribute() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:157:2: ( ( ( rule__Attribute__Group__0 ) ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:158:1: ( ( rule__Attribute__Group__0 ) )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:158:1: ( ( rule__Attribute__Group__0 ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:159:1: ( rule__Attribute__Group__0 )
            {
             before(grammarAccess.getAttributeAccess().getGroup()); 
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:160:1: ( rule__Attribute__Group__0 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:160:2: rule__Attribute__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__0_in_ruleAttribute274);
            rule__Attribute__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAttributeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAttribute"


    // $ANTLR start "rule__EString__Alternatives"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:172:1: rule__EString__Alternatives : ( ( RULE_STRING ) | ( RULE_ID ) );
    public final void rule__EString__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:176:1: ( ( RULE_STRING ) | ( RULE_ID ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==RULE_STRING) ) {
                alt1=1;
            }
            else if ( (LA1_0==RULE_ID) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:177:1: ( RULE_STRING )
                    {
                    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:177:1: ( RULE_STRING )
                    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:178:1: RULE_STRING
                    {
                     before(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 
                    match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_rule__EString__Alternatives310); 
                     after(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:183:6: ( RULE_ID )
                    {
                    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:183:6: ( RULE_ID )
                    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:184:1: RULE_ID
                    {
                     before(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 
                    match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rule__EString__Alternatives327); 
                     after(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Alternatives"


    // $ANTLR start "rule__Model__Group__0"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:196:1: rule__Model__Group__0 : rule__Model__Group__0__Impl rule__Model__Group__1 ;
    public final void rule__Model__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:200:1: ( rule__Model__Group__0__Impl rule__Model__Group__1 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:201:2: rule__Model__Group__0__Impl rule__Model__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Model__Group__0__Impl_in_rule__Model__Group__0357);
            rule__Model__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Model__Group__1_in_rule__Model__Group__0360);
            rule__Model__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__0"


    // $ANTLR start "rule__Model__Group__0__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:208:1: rule__Model__Group__0__Impl : ( () ) ;
    public final void rule__Model__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:212:1: ( ( () ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:213:1: ( () )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:213:1: ( () )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:214:1: ()
            {
             before(grammarAccess.getModelAccess().getModelAction_0()); 
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:215:1: ()
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:217:1: 
            {
            }

             after(grammarAccess.getModelAccess().getModelAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__0__Impl"


    // $ANTLR start "rule__Model__Group__1"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:227:1: rule__Model__Group__1 : rule__Model__Group__1__Impl rule__Model__Group__2 ;
    public final void rule__Model__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:231:1: ( rule__Model__Group__1__Impl rule__Model__Group__2 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:232:2: rule__Model__Group__1__Impl rule__Model__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Model__Group__1__Impl_in_rule__Model__Group__1418);
            rule__Model__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Model__Group__2_in_rule__Model__Group__1421);
            rule__Model__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__1"


    // $ANTLR start "rule__Model__Group__1__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:239:1: rule__Model__Group__1__Impl : ( 'Model' ) ;
    public final void rule__Model__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:243:1: ( ( 'Model' ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:244:1: ( 'Model' )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:244:1: ( 'Model' )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:245:1: 'Model'
            {
             before(grammarAccess.getModelAccess().getModelKeyword_1()); 
            match(input,11,FollowSets000.FOLLOW_11_in_rule__Model__Group__1__Impl449); 
             after(grammarAccess.getModelAccess().getModelKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__1__Impl"


    // $ANTLR start "rule__Model__Group__2"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:258:1: rule__Model__Group__2 : rule__Model__Group__2__Impl rule__Model__Group__3 ;
    public final void rule__Model__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:262:1: ( rule__Model__Group__2__Impl rule__Model__Group__3 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:263:2: rule__Model__Group__2__Impl rule__Model__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Model__Group__2__Impl_in_rule__Model__Group__2480);
            rule__Model__Group__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Model__Group__3_in_rule__Model__Group__2483);
            rule__Model__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__2"


    // $ANTLR start "rule__Model__Group__2__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:270:1: rule__Model__Group__2__Impl : ( '{' ) ;
    public final void rule__Model__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:274:1: ( ( '{' ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:275:1: ( '{' )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:275:1: ( '{' )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:276:1: '{'
            {
             before(grammarAccess.getModelAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,12,FollowSets000.FOLLOW_12_in_rule__Model__Group__2__Impl511); 
             after(grammarAccess.getModelAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__2__Impl"


    // $ANTLR start "rule__Model__Group__3"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:289:1: rule__Model__Group__3 : rule__Model__Group__3__Impl rule__Model__Group__4 ;
    public final void rule__Model__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:293:1: ( rule__Model__Group__3__Impl rule__Model__Group__4 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:294:2: rule__Model__Group__3__Impl rule__Model__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Model__Group__3__Impl_in_rule__Model__Group__3542);
            rule__Model__Group__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Model__Group__4_in_rule__Model__Group__3545);
            rule__Model__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__3"


    // $ANTLR start "rule__Model__Group__3__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:301:1: rule__Model__Group__3__Impl : ( ( rule__Model__Group_3__0 )? ) ;
    public final void rule__Model__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:305:1: ( ( ( rule__Model__Group_3__0 )? ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:306:1: ( ( rule__Model__Group_3__0 )? )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:306:1: ( ( rule__Model__Group_3__0 )? )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:307:1: ( rule__Model__Group_3__0 )?
            {
             before(grammarAccess.getModelAccess().getGroup_3()); 
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:308:1: ( rule__Model__Group_3__0 )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==14) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:308:2: rule__Model__Group_3__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Model__Group_3__0_in_rule__Model__Group__3__Impl572);
                    rule__Model__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getModelAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__3__Impl"


    // $ANTLR start "rule__Model__Group__4"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:318:1: rule__Model__Group__4 : rule__Model__Group__4__Impl ;
    public final void rule__Model__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:322:1: ( rule__Model__Group__4__Impl )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:323:2: rule__Model__Group__4__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Model__Group__4__Impl_in_rule__Model__Group__4603);
            rule__Model__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__4"


    // $ANTLR start "rule__Model__Group__4__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:329:1: rule__Model__Group__4__Impl : ( '}' ) ;
    public final void rule__Model__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:333:1: ( ( '}' ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:334:1: ( '}' )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:334:1: ( '}' )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:335:1: '}'
            {
             before(grammarAccess.getModelAccess().getRightCurlyBracketKeyword_4()); 
            match(input,13,FollowSets000.FOLLOW_13_in_rule__Model__Group__4__Impl631); 
             after(grammarAccess.getModelAccess().getRightCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__4__Impl"


    // $ANTLR start "rule__Model__Group_3__0"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:358:1: rule__Model__Group_3__0 : rule__Model__Group_3__0__Impl rule__Model__Group_3__1 ;
    public final void rule__Model__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:362:1: ( rule__Model__Group_3__0__Impl rule__Model__Group_3__1 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:363:2: rule__Model__Group_3__0__Impl rule__Model__Group_3__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Model__Group_3__0__Impl_in_rule__Model__Group_3__0672);
            rule__Model__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Model__Group_3__1_in_rule__Model__Group_3__0675);
            rule__Model__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group_3__0"


    // $ANTLR start "rule__Model__Group_3__0__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:370:1: rule__Model__Group_3__0__Impl : ( 'classes' ) ;
    public final void rule__Model__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:374:1: ( ( 'classes' ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:375:1: ( 'classes' )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:375:1: ( 'classes' )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:376:1: 'classes'
            {
             before(grammarAccess.getModelAccess().getClassesKeyword_3_0()); 
            match(input,14,FollowSets000.FOLLOW_14_in_rule__Model__Group_3__0__Impl703); 
             after(grammarAccess.getModelAccess().getClassesKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group_3__0__Impl"


    // $ANTLR start "rule__Model__Group_3__1"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:389:1: rule__Model__Group_3__1 : rule__Model__Group_3__1__Impl rule__Model__Group_3__2 ;
    public final void rule__Model__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:393:1: ( rule__Model__Group_3__1__Impl rule__Model__Group_3__2 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:394:2: rule__Model__Group_3__1__Impl rule__Model__Group_3__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Model__Group_3__1__Impl_in_rule__Model__Group_3__1734);
            rule__Model__Group_3__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Model__Group_3__2_in_rule__Model__Group_3__1737);
            rule__Model__Group_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group_3__1"


    // $ANTLR start "rule__Model__Group_3__1__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:401:1: rule__Model__Group_3__1__Impl : ( '{' ) ;
    public final void rule__Model__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:405:1: ( ( '{' ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:406:1: ( '{' )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:406:1: ( '{' )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:407:1: '{'
            {
             before(grammarAccess.getModelAccess().getLeftCurlyBracketKeyword_3_1()); 
            match(input,12,FollowSets000.FOLLOW_12_in_rule__Model__Group_3__1__Impl765); 
             after(grammarAccess.getModelAccess().getLeftCurlyBracketKeyword_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group_3__1__Impl"


    // $ANTLR start "rule__Model__Group_3__2"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:420:1: rule__Model__Group_3__2 : rule__Model__Group_3__2__Impl rule__Model__Group_3__3 ;
    public final void rule__Model__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:424:1: ( rule__Model__Group_3__2__Impl rule__Model__Group_3__3 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:425:2: rule__Model__Group_3__2__Impl rule__Model__Group_3__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Model__Group_3__2__Impl_in_rule__Model__Group_3__2796);
            rule__Model__Group_3__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Model__Group_3__3_in_rule__Model__Group_3__2799);
            rule__Model__Group_3__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group_3__2"


    // $ANTLR start "rule__Model__Group_3__2__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:432:1: rule__Model__Group_3__2__Impl : ( ( rule__Model__ClassesAssignment_3_2 ) ) ;
    public final void rule__Model__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:436:1: ( ( ( rule__Model__ClassesAssignment_3_2 ) ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:437:1: ( ( rule__Model__ClassesAssignment_3_2 ) )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:437:1: ( ( rule__Model__ClassesAssignment_3_2 ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:438:1: ( rule__Model__ClassesAssignment_3_2 )
            {
             before(grammarAccess.getModelAccess().getClassesAssignment_3_2()); 
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:439:1: ( rule__Model__ClassesAssignment_3_2 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:439:2: rule__Model__ClassesAssignment_3_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Model__ClassesAssignment_3_2_in_rule__Model__Group_3__2__Impl826);
            rule__Model__ClassesAssignment_3_2();

            state._fsp--;


            }

             after(grammarAccess.getModelAccess().getClassesAssignment_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group_3__2__Impl"


    // $ANTLR start "rule__Model__Group_3__3"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:449:1: rule__Model__Group_3__3 : rule__Model__Group_3__3__Impl rule__Model__Group_3__4 ;
    public final void rule__Model__Group_3__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:453:1: ( rule__Model__Group_3__3__Impl rule__Model__Group_3__4 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:454:2: rule__Model__Group_3__3__Impl rule__Model__Group_3__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Model__Group_3__3__Impl_in_rule__Model__Group_3__3856);
            rule__Model__Group_3__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Model__Group_3__4_in_rule__Model__Group_3__3859);
            rule__Model__Group_3__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group_3__3"


    // $ANTLR start "rule__Model__Group_3__3__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:461:1: rule__Model__Group_3__3__Impl : ( ( rule__Model__Group_3_3__0 )* ) ;
    public final void rule__Model__Group_3__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:465:1: ( ( ( rule__Model__Group_3_3__0 )* ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:466:1: ( ( rule__Model__Group_3_3__0 )* )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:466:1: ( ( rule__Model__Group_3_3__0 )* )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:467:1: ( rule__Model__Group_3_3__0 )*
            {
             before(grammarAccess.getModelAccess().getGroup_3_3()); 
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:468:1: ( rule__Model__Group_3_3__0 )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==15) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:468:2: rule__Model__Group_3_3__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Model__Group_3_3__0_in_rule__Model__Group_3__3__Impl886);
            	    rule__Model__Group_3_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

             after(grammarAccess.getModelAccess().getGroup_3_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group_3__3__Impl"


    // $ANTLR start "rule__Model__Group_3__4"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:478:1: rule__Model__Group_3__4 : rule__Model__Group_3__4__Impl ;
    public final void rule__Model__Group_3__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:482:1: ( rule__Model__Group_3__4__Impl )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:483:2: rule__Model__Group_3__4__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Model__Group_3__4__Impl_in_rule__Model__Group_3__4917);
            rule__Model__Group_3__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group_3__4"


    // $ANTLR start "rule__Model__Group_3__4__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:489:1: rule__Model__Group_3__4__Impl : ( '}' ) ;
    public final void rule__Model__Group_3__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:493:1: ( ( '}' ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:494:1: ( '}' )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:494:1: ( '}' )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:495:1: '}'
            {
             before(grammarAccess.getModelAccess().getRightCurlyBracketKeyword_3_4()); 
            match(input,13,FollowSets000.FOLLOW_13_in_rule__Model__Group_3__4__Impl945); 
             after(grammarAccess.getModelAccess().getRightCurlyBracketKeyword_3_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group_3__4__Impl"


    // $ANTLR start "rule__Model__Group_3_3__0"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:518:1: rule__Model__Group_3_3__0 : rule__Model__Group_3_3__0__Impl rule__Model__Group_3_3__1 ;
    public final void rule__Model__Group_3_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:522:1: ( rule__Model__Group_3_3__0__Impl rule__Model__Group_3_3__1 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:523:2: rule__Model__Group_3_3__0__Impl rule__Model__Group_3_3__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Model__Group_3_3__0__Impl_in_rule__Model__Group_3_3__0986);
            rule__Model__Group_3_3__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Model__Group_3_3__1_in_rule__Model__Group_3_3__0989);
            rule__Model__Group_3_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group_3_3__0"


    // $ANTLR start "rule__Model__Group_3_3__0__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:530:1: rule__Model__Group_3_3__0__Impl : ( ',' ) ;
    public final void rule__Model__Group_3_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:534:1: ( ( ',' ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:535:1: ( ',' )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:535:1: ( ',' )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:536:1: ','
            {
             before(grammarAccess.getModelAccess().getCommaKeyword_3_3_0()); 
            match(input,15,FollowSets000.FOLLOW_15_in_rule__Model__Group_3_3__0__Impl1017); 
             after(grammarAccess.getModelAccess().getCommaKeyword_3_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group_3_3__0__Impl"


    // $ANTLR start "rule__Model__Group_3_3__1"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:549:1: rule__Model__Group_3_3__1 : rule__Model__Group_3_3__1__Impl ;
    public final void rule__Model__Group_3_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:553:1: ( rule__Model__Group_3_3__1__Impl )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:554:2: rule__Model__Group_3_3__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Model__Group_3_3__1__Impl_in_rule__Model__Group_3_3__11048);
            rule__Model__Group_3_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group_3_3__1"


    // $ANTLR start "rule__Model__Group_3_3__1__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:560:1: rule__Model__Group_3_3__1__Impl : ( ( rule__Model__ClassesAssignment_3_3_1 ) ) ;
    public final void rule__Model__Group_3_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:564:1: ( ( ( rule__Model__ClassesAssignment_3_3_1 ) ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:565:1: ( ( rule__Model__ClassesAssignment_3_3_1 ) )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:565:1: ( ( rule__Model__ClassesAssignment_3_3_1 ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:566:1: ( rule__Model__ClassesAssignment_3_3_1 )
            {
             before(grammarAccess.getModelAccess().getClassesAssignment_3_3_1()); 
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:567:1: ( rule__Model__ClassesAssignment_3_3_1 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:567:2: rule__Model__ClassesAssignment_3_3_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Model__ClassesAssignment_3_3_1_in_rule__Model__Group_3_3__1__Impl1075);
            rule__Model__ClassesAssignment_3_3_1();

            state._fsp--;


            }

             after(grammarAccess.getModelAccess().getClassesAssignment_3_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group_3_3__1__Impl"


    // $ANTLR start "rule__Class__Group__0"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:581:1: rule__Class__Group__0 : rule__Class__Group__0__Impl rule__Class__Group__1 ;
    public final void rule__Class__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:585:1: ( rule__Class__Group__0__Impl rule__Class__Group__1 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:586:2: rule__Class__Group__0__Impl rule__Class__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group__0__Impl_in_rule__Class__Group__01109);
            rule__Class__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Class__Group__1_in_rule__Class__Group__01112);
            rule__Class__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__0"


    // $ANTLR start "rule__Class__Group__0__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:593:1: rule__Class__Group__0__Impl : ( () ) ;
    public final void rule__Class__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:597:1: ( ( () ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:598:1: ( () )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:598:1: ( () )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:599:1: ()
            {
             before(grammarAccess.getClassAccess().getClassAction_0()); 
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:600:1: ()
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:602:1: 
            {
            }

             after(grammarAccess.getClassAccess().getClassAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__0__Impl"


    // $ANTLR start "rule__Class__Group__1"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:612:1: rule__Class__Group__1 : rule__Class__Group__1__Impl rule__Class__Group__2 ;
    public final void rule__Class__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:616:1: ( rule__Class__Group__1__Impl rule__Class__Group__2 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:617:2: rule__Class__Group__1__Impl rule__Class__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group__1__Impl_in_rule__Class__Group__11170);
            rule__Class__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Class__Group__2_in_rule__Class__Group__11173);
            rule__Class__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__1"


    // $ANTLR start "rule__Class__Group__1__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:624:1: rule__Class__Group__1__Impl : ( 'Class' ) ;
    public final void rule__Class__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:628:1: ( ( 'Class' ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:629:1: ( 'Class' )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:629:1: ( 'Class' )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:630:1: 'Class'
            {
             before(grammarAccess.getClassAccess().getClassKeyword_1()); 
            match(input,16,FollowSets000.FOLLOW_16_in_rule__Class__Group__1__Impl1201); 
             after(grammarAccess.getClassAccess().getClassKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__1__Impl"


    // $ANTLR start "rule__Class__Group__2"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:643:1: rule__Class__Group__2 : rule__Class__Group__2__Impl rule__Class__Group__3 ;
    public final void rule__Class__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:647:1: ( rule__Class__Group__2__Impl rule__Class__Group__3 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:648:2: rule__Class__Group__2__Impl rule__Class__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group__2__Impl_in_rule__Class__Group__21232);
            rule__Class__Group__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Class__Group__3_in_rule__Class__Group__21235);
            rule__Class__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__2"


    // $ANTLR start "rule__Class__Group__2__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:655:1: rule__Class__Group__2__Impl : ( ( rule__Class__NameAssignment_2 ) ) ;
    public final void rule__Class__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:659:1: ( ( ( rule__Class__NameAssignment_2 ) ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:660:1: ( ( rule__Class__NameAssignment_2 ) )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:660:1: ( ( rule__Class__NameAssignment_2 ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:661:1: ( rule__Class__NameAssignment_2 )
            {
             before(grammarAccess.getClassAccess().getNameAssignment_2()); 
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:662:1: ( rule__Class__NameAssignment_2 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:662:2: rule__Class__NameAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__NameAssignment_2_in_rule__Class__Group__2__Impl1262);
            rule__Class__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getClassAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__2__Impl"


    // $ANTLR start "rule__Class__Group__3"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:672:1: rule__Class__Group__3 : rule__Class__Group__3__Impl rule__Class__Group__4 ;
    public final void rule__Class__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:676:1: ( rule__Class__Group__3__Impl rule__Class__Group__4 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:677:2: rule__Class__Group__3__Impl rule__Class__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group__3__Impl_in_rule__Class__Group__31292);
            rule__Class__Group__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Class__Group__4_in_rule__Class__Group__31295);
            rule__Class__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__3"


    // $ANTLR start "rule__Class__Group__3__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:684:1: rule__Class__Group__3__Impl : ( '{' ) ;
    public final void rule__Class__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:688:1: ( ( '{' ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:689:1: ( '{' )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:689:1: ( '{' )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:690:1: '{'
            {
             before(grammarAccess.getClassAccess().getLeftCurlyBracketKeyword_3()); 
            match(input,12,FollowSets000.FOLLOW_12_in_rule__Class__Group__3__Impl1323); 
             after(grammarAccess.getClassAccess().getLeftCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__3__Impl"


    // $ANTLR start "rule__Class__Group__4"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:703:1: rule__Class__Group__4 : rule__Class__Group__4__Impl rule__Class__Group__5 ;
    public final void rule__Class__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:707:1: ( rule__Class__Group__4__Impl rule__Class__Group__5 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:708:2: rule__Class__Group__4__Impl rule__Class__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group__4__Impl_in_rule__Class__Group__41354);
            rule__Class__Group__4__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Class__Group__5_in_rule__Class__Group__41357);
            rule__Class__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__4"


    // $ANTLR start "rule__Class__Group__4__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:715:1: rule__Class__Group__4__Impl : ( ( rule__Class__Group_4__0 )? ) ;
    public final void rule__Class__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:719:1: ( ( ( rule__Class__Group_4__0 )? ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:720:1: ( ( rule__Class__Group_4__0 )? )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:720:1: ( ( rule__Class__Group_4__0 )? )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:721:1: ( rule__Class__Group_4__0 )?
            {
             before(grammarAccess.getClassAccess().getGroup_4()); 
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:722:1: ( rule__Class__Group_4__0 )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==17) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:722:2: rule__Class__Group_4__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Class__Group_4__0_in_rule__Class__Group__4__Impl1384);
                    rule__Class__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getClassAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__4__Impl"


    // $ANTLR start "rule__Class__Group__5"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:732:1: rule__Class__Group__5 : rule__Class__Group__5__Impl rule__Class__Group__6 ;
    public final void rule__Class__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:736:1: ( rule__Class__Group__5__Impl rule__Class__Group__6 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:737:2: rule__Class__Group__5__Impl rule__Class__Group__6
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group__5__Impl_in_rule__Class__Group__51415);
            rule__Class__Group__5__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Class__Group__6_in_rule__Class__Group__51418);
            rule__Class__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__5"


    // $ANTLR start "rule__Class__Group__5__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:744:1: rule__Class__Group__5__Impl : ( ( rule__Class__Group_5__0 )? ) ;
    public final void rule__Class__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:748:1: ( ( ( rule__Class__Group_5__0 )? ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:749:1: ( ( rule__Class__Group_5__0 )? )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:749:1: ( ( rule__Class__Group_5__0 )? )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:750:1: ( rule__Class__Group_5__0 )?
            {
             before(grammarAccess.getClassAccess().getGroup_5()); 
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:751:1: ( rule__Class__Group_5__0 )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==20) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:751:2: rule__Class__Group_5__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Class__Group_5__0_in_rule__Class__Group__5__Impl1445);
                    rule__Class__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getClassAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__5__Impl"


    // $ANTLR start "rule__Class__Group__6"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:761:1: rule__Class__Group__6 : rule__Class__Group__6__Impl ;
    public final void rule__Class__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:765:1: ( rule__Class__Group__6__Impl )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:766:2: rule__Class__Group__6__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group__6__Impl_in_rule__Class__Group__61476);
            rule__Class__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__6"


    // $ANTLR start "rule__Class__Group__6__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:772:1: rule__Class__Group__6__Impl : ( '}' ) ;
    public final void rule__Class__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:776:1: ( ( '}' ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:777:1: ( '}' )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:777:1: ( '}' )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:778:1: '}'
            {
             before(grammarAccess.getClassAccess().getRightCurlyBracketKeyword_6()); 
            match(input,13,FollowSets000.FOLLOW_13_in_rule__Class__Group__6__Impl1504); 
             after(grammarAccess.getClassAccess().getRightCurlyBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__6__Impl"


    // $ANTLR start "rule__Class__Group_4__0"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:805:1: rule__Class__Group_4__0 : rule__Class__Group_4__0__Impl rule__Class__Group_4__1 ;
    public final void rule__Class__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:809:1: ( rule__Class__Group_4__0__Impl rule__Class__Group_4__1 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:810:2: rule__Class__Group_4__0__Impl rule__Class__Group_4__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group_4__0__Impl_in_rule__Class__Group_4__01549);
            rule__Class__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Class__Group_4__1_in_rule__Class__Group_4__01552);
            rule__Class__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_4__0"


    // $ANTLR start "rule__Class__Group_4__0__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:817:1: rule__Class__Group_4__0__Impl : ( 'parents' ) ;
    public final void rule__Class__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:821:1: ( ( 'parents' ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:822:1: ( 'parents' )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:822:1: ( 'parents' )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:823:1: 'parents'
            {
             before(grammarAccess.getClassAccess().getParentsKeyword_4_0()); 
            match(input,17,FollowSets000.FOLLOW_17_in_rule__Class__Group_4__0__Impl1580); 
             after(grammarAccess.getClassAccess().getParentsKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_4__0__Impl"


    // $ANTLR start "rule__Class__Group_4__1"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:836:1: rule__Class__Group_4__1 : rule__Class__Group_4__1__Impl rule__Class__Group_4__2 ;
    public final void rule__Class__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:840:1: ( rule__Class__Group_4__1__Impl rule__Class__Group_4__2 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:841:2: rule__Class__Group_4__1__Impl rule__Class__Group_4__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group_4__1__Impl_in_rule__Class__Group_4__11611);
            rule__Class__Group_4__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Class__Group_4__2_in_rule__Class__Group_4__11614);
            rule__Class__Group_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_4__1"


    // $ANTLR start "rule__Class__Group_4__1__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:848:1: rule__Class__Group_4__1__Impl : ( '(' ) ;
    public final void rule__Class__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:852:1: ( ( '(' ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:853:1: ( '(' )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:853:1: ( '(' )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:854:1: '('
            {
             before(grammarAccess.getClassAccess().getLeftParenthesisKeyword_4_1()); 
            match(input,18,FollowSets000.FOLLOW_18_in_rule__Class__Group_4__1__Impl1642); 
             after(grammarAccess.getClassAccess().getLeftParenthesisKeyword_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_4__1__Impl"


    // $ANTLR start "rule__Class__Group_4__2"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:867:1: rule__Class__Group_4__2 : rule__Class__Group_4__2__Impl rule__Class__Group_4__3 ;
    public final void rule__Class__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:871:1: ( rule__Class__Group_4__2__Impl rule__Class__Group_4__3 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:872:2: rule__Class__Group_4__2__Impl rule__Class__Group_4__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group_4__2__Impl_in_rule__Class__Group_4__21673);
            rule__Class__Group_4__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Class__Group_4__3_in_rule__Class__Group_4__21676);
            rule__Class__Group_4__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_4__2"


    // $ANTLR start "rule__Class__Group_4__2__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:879:1: rule__Class__Group_4__2__Impl : ( ( rule__Class__ParentsAssignment_4_2 ) ) ;
    public final void rule__Class__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:883:1: ( ( ( rule__Class__ParentsAssignment_4_2 ) ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:884:1: ( ( rule__Class__ParentsAssignment_4_2 ) )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:884:1: ( ( rule__Class__ParentsAssignment_4_2 ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:885:1: ( rule__Class__ParentsAssignment_4_2 )
            {
             before(grammarAccess.getClassAccess().getParentsAssignment_4_2()); 
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:886:1: ( rule__Class__ParentsAssignment_4_2 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:886:2: rule__Class__ParentsAssignment_4_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__ParentsAssignment_4_2_in_rule__Class__Group_4__2__Impl1703);
            rule__Class__ParentsAssignment_4_2();

            state._fsp--;


            }

             after(grammarAccess.getClassAccess().getParentsAssignment_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_4__2__Impl"


    // $ANTLR start "rule__Class__Group_4__3"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:896:1: rule__Class__Group_4__3 : rule__Class__Group_4__3__Impl rule__Class__Group_4__4 ;
    public final void rule__Class__Group_4__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:900:1: ( rule__Class__Group_4__3__Impl rule__Class__Group_4__4 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:901:2: rule__Class__Group_4__3__Impl rule__Class__Group_4__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group_4__3__Impl_in_rule__Class__Group_4__31733);
            rule__Class__Group_4__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Class__Group_4__4_in_rule__Class__Group_4__31736);
            rule__Class__Group_4__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_4__3"


    // $ANTLR start "rule__Class__Group_4__3__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:908:1: rule__Class__Group_4__3__Impl : ( ( rule__Class__Group_4_3__0 )* ) ;
    public final void rule__Class__Group_4__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:912:1: ( ( ( rule__Class__Group_4_3__0 )* ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:913:1: ( ( rule__Class__Group_4_3__0 )* )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:913:1: ( ( rule__Class__Group_4_3__0 )* )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:914:1: ( rule__Class__Group_4_3__0 )*
            {
             before(grammarAccess.getClassAccess().getGroup_4_3()); 
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:915:1: ( rule__Class__Group_4_3__0 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==15) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:915:2: rule__Class__Group_4_3__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Class__Group_4_3__0_in_rule__Class__Group_4__3__Impl1763);
            	    rule__Class__Group_4_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getClassAccess().getGroup_4_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_4__3__Impl"


    // $ANTLR start "rule__Class__Group_4__4"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:925:1: rule__Class__Group_4__4 : rule__Class__Group_4__4__Impl ;
    public final void rule__Class__Group_4__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:929:1: ( rule__Class__Group_4__4__Impl )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:930:2: rule__Class__Group_4__4__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group_4__4__Impl_in_rule__Class__Group_4__41794);
            rule__Class__Group_4__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_4__4"


    // $ANTLR start "rule__Class__Group_4__4__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:936:1: rule__Class__Group_4__4__Impl : ( ')' ) ;
    public final void rule__Class__Group_4__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:940:1: ( ( ')' ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:941:1: ( ')' )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:941:1: ( ')' )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:942:1: ')'
            {
             before(grammarAccess.getClassAccess().getRightParenthesisKeyword_4_4()); 
            match(input,19,FollowSets000.FOLLOW_19_in_rule__Class__Group_4__4__Impl1822); 
             after(grammarAccess.getClassAccess().getRightParenthesisKeyword_4_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_4__4__Impl"


    // $ANTLR start "rule__Class__Group_4_3__0"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:965:1: rule__Class__Group_4_3__0 : rule__Class__Group_4_3__0__Impl rule__Class__Group_4_3__1 ;
    public final void rule__Class__Group_4_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:969:1: ( rule__Class__Group_4_3__0__Impl rule__Class__Group_4_3__1 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:970:2: rule__Class__Group_4_3__0__Impl rule__Class__Group_4_3__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group_4_3__0__Impl_in_rule__Class__Group_4_3__01863);
            rule__Class__Group_4_3__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Class__Group_4_3__1_in_rule__Class__Group_4_3__01866);
            rule__Class__Group_4_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_4_3__0"


    // $ANTLR start "rule__Class__Group_4_3__0__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:977:1: rule__Class__Group_4_3__0__Impl : ( ',' ) ;
    public final void rule__Class__Group_4_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:981:1: ( ( ',' ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:982:1: ( ',' )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:982:1: ( ',' )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:983:1: ','
            {
             before(grammarAccess.getClassAccess().getCommaKeyword_4_3_0()); 
            match(input,15,FollowSets000.FOLLOW_15_in_rule__Class__Group_4_3__0__Impl1894); 
             after(grammarAccess.getClassAccess().getCommaKeyword_4_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_4_3__0__Impl"


    // $ANTLR start "rule__Class__Group_4_3__1"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:996:1: rule__Class__Group_4_3__1 : rule__Class__Group_4_3__1__Impl ;
    public final void rule__Class__Group_4_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1000:1: ( rule__Class__Group_4_3__1__Impl )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1001:2: rule__Class__Group_4_3__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group_4_3__1__Impl_in_rule__Class__Group_4_3__11925);
            rule__Class__Group_4_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_4_3__1"


    // $ANTLR start "rule__Class__Group_4_3__1__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1007:1: rule__Class__Group_4_3__1__Impl : ( ( rule__Class__ParentsAssignment_4_3_1 ) ) ;
    public final void rule__Class__Group_4_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1011:1: ( ( ( rule__Class__ParentsAssignment_4_3_1 ) ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1012:1: ( ( rule__Class__ParentsAssignment_4_3_1 ) )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1012:1: ( ( rule__Class__ParentsAssignment_4_3_1 ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1013:1: ( rule__Class__ParentsAssignment_4_3_1 )
            {
             before(grammarAccess.getClassAccess().getParentsAssignment_4_3_1()); 
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1014:1: ( rule__Class__ParentsAssignment_4_3_1 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1014:2: rule__Class__ParentsAssignment_4_3_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__ParentsAssignment_4_3_1_in_rule__Class__Group_4_3__1__Impl1952);
            rule__Class__ParentsAssignment_4_3_1();

            state._fsp--;


            }

             after(grammarAccess.getClassAccess().getParentsAssignment_4_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_4_3__1__Impl"


    // $ANTLR start "rule__Class__Group_5__0"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1028:1: rule__Class__Group_5__0 : rule__Class__Group_5__0__Impl rule__Class__Group_5__1 ;
    public final void rule__Class__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1032:1: ( rule__Class__Group_5__0__Impl rule__Class__Group_5__1 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1033:2: rule__Class__Group_5__0__Impl rule__Class__Group_5__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group_5__0__Impl_in_rule__Class__Group_5__01986);
            rule__Class__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Class__Group_5__1_in_rule__Class__Group_5__01989);
            rule__Class__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_5__0"


    // $ANTLR start "rule__Class__Group_5__0__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1040:1: rule__Class__Group_5__0__Impl : ( 'attributes' ) ;
    public final void rule__Class__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1044:1: ( ( 'attributes' ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1045:1: ( 'attributes' )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1045:1: ( 'attributes' )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1046:1: 'attributes'
            {
             before(grammarAccess.getClassAccess().getAttributesKeyword_5_0()); 
            match(input,20,FollowSets000.FOLLOW_20_in_rule__Class__Group_5__0__Impl2017); 
             after(grammarAccess.getClassAccess().getAttributesKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_5__0__Impl"


    // $ANTLR start "rule__Class__Group_5__1"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1059:1: rule__Class__Group_5__1 : rule__Class__Group_5__1__Impl rule__Class__Group_5__2 ;
    public final void rule__Class__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1063:1: ( rule__Class__Group_5__1__Impl rule__Class__Group_5__2 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1064:2: rule__Class__Group_5__1__Impl rule__Class__Group_5__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group_5__1__Impl_in_rule__Class__Group_5__12048);
            rule__Class__Group_5__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Class__Group_5__2_in_rule__Class__Group_5__12051);
            rule__Class__Group_5__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_5__1"


    // $ANTLR start "rule__Class__Group_5__1__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1071:1: rule__Class__Group_5__1__Impl : ( '{' ) ;
    public final void rule__Class__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1075:1: ( ( '{' ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1076:1: ( '{' )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1076:1: ( '{' )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1077:1: '{'
            {
             before(grammarAccess.getClassAccess().getLeftCurlyBracketKeyword_5_1()); 
            match(input,12,FollowSets000.FOLLOW_12_in_rule__Class__Group_5__1__Impl2079); 
             after(grammarAccess.getClassAccess().getLeftCurlyBracketKeyword_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_5__1__Impl"


    // $ANTLR start "rule__Class__Group_5__2"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1090:1: rule__Class__Group_5__2 : rule__Class__Group_5__2__Impl rule__Class__Group_5__3 ;
    public final void rule__Class__Group_5__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1094:1: ( rule__Class__Group_5__2__Impl rule__Class__Group_5__3 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1095:2: rule__Class__Group_5__2__Impl rule__Class__Group_5__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group_5__2__Impl_in_rule__Class__Group_5__22110);
            rule__Class__Group_5__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Class__Group_5__3_in_rule__Class__Group_5__22113);
            rule__Class__Group_5__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_5__2"


    // $ANTLR start "rule__Class__Group_5__2__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1102:1: rule__Class__Group_5__2__Impl : ( ( rule__Class__AttributesAssignment_5_2 ) ) ;
    public final void rule__Class__Group_5__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1106:1: ( ( ( rule__Class__AttributesAssignment_5_2 ) ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1107:1: ( ( rule__Class__AttributesAssignment_5_2 ) )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1107:1: ( ( rule__Class__AttributesAssignment_5_2 ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1108:1: ( rule__Class__AttributesAssignment_5_2 )
            {
             before(grammarAccess.getClassAccess().getAttributesAssignment_5_2()); 
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1109:1: ( rule__Class__AttributesAssignment_5_2 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1109:2: rule__Class__AttributesAssignment_5_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__AttributesAssignment_5_2_in_rule__Class__Group_5__2__Impl2140);
            rule__Class__AttributesAssignment_5_2();

            state._fsp--;


            }

             after(grammarAccess.getClassAccess().getAttributesAssignment_5_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_5__2__Impl"


    // $ANTLR start "rule__Class__Group_5__3"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1119:1: rule__Class__Group_5__3 : rule__Class__Group_5__3__Impl rule__Class__Group_5__4 ;
    public final void rule__Class__Group_5__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1123:1: ( rule__Class__Group_5__3__Impl rule__Class__Group_5__4 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1124:2: rule__Class__Group_5__3__Impl rule__Class__Group_5__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group_5__3__Impl_in_rule__Class__Group_5__32170);
            rule__Class__Group_5__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Class__Group_5__4_in_rule__Class__Group_5__32173);
            rule__Class__Group_5__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_5__3"


    // $ANTLR start "rule__Class__Group_5__3__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1131:1: rule__Class__Group_5__3__Impl : ( ( rule__Class__Group_5_3__0 )* ) ;
    public final void rule__Class__Group_5__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1135:1: ( ( ( rule__Class__Group_5_3__0 )* ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1136:1: ( ( rule__Class__Group_5_3__0 )* )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1136:1: ( ( rule__Class__Group_5_3__0 )* )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1137:1: ( rule__Class__Group_5_3__0 )*
            {
             before(grammarAccess.getClassAccess().getGroup_5_3()); 
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1138:1: ( rule__Class__Group_5_3__0 )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==15) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1138:2: rule__Class__Group_5_3__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Class__Group_5_3__0_in_rule__Class__Group_5__3__Impl2200);
            	    rule__Class__Group_5_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

             after(grammarAccess.getClassAccess().getGroup_5_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_5__3__Impl"


    // $ANTLR start "rule__Class__Group_5__4"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1148:1: rule__Class__Group_5__4 : rule__Class__Group_5__4__Impl ;
    public final void rule__Class__Group_5__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1152:1: ( rule__Class__Group_5__4__Impl )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1153:2: rule__Class__Group_5__4__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group_5__4__Impl_in_rule__Class__Group_5__42231);
            rule__Class__Group_5__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_5__4"


    // $ANTLR start "rule__Class__Group_5__4__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1159:1: rule__Class__Group_5__4__Impl : ( '}' ) ;
    public final void rule__Class__Group_5__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1163:1: ( ( '}' ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1164:1: ( '}' )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1164:1: ( '}' )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1165:1: '}'
            {
             before(grammarAccess.getClassAccess().getRightCurlyBracketKeyword_5_4()); 
            match(input,13,FollowSets000.FOLLOW_13_in_rule__Class__Group_5__4__Impl2259); 
             after(grammarAccess.getClassAccess().getRightCurlyBracketKeyword_5_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_5__4__Impl"


    // $ANTLR start "rule__Class__Group_5_3__0"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1188:1: rule__Class__Group_5_3__0 : rule__Class__Group_5_3__0__Impl rule__Class__Group_5_3__1 ;
    public final void rule__Class__Group_5_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1192:1: ( rule__Class__Group_5_3__0__Impl rule__Class__Group_5_3__1 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1193:2: rule__Class__Group_5_3__0__Impl rule__Class__Group_5_3__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group_5_3__0__Impl_in_rule__Class__Group_5_3__02300);
            rule__Class__Group_5_3__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Class__Group_5_3__1_in_rule__Class__Group_5_3__02303);
            rule__Class__Group_5_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_5_3__0"


    // $ANTLR start "rule__Class__Group_5_3__0__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1200:1: rule__Class__Group_5_3__0__Impl : ( ',' ) ;
    public final void rule__Class__Group_5_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1204:1: ( ( ',' ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1205:1: ( ',' )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1205:1: ( ',' )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1206:1: ','
            {
             before(grammarAccess.getClassAccess().getCommaKeyword_5_3_0()); 
            match(input,15,FollowSets000.FOLLOW_15_in_rule__Class__Group_5_3__0__Impl2331); 
             after(grammarAccess.getClassAccess().getCommaKeyword_5_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_5_3__0__Impl"


    // $ANTLR start "rule__Class__Group_5_3__1"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1219:1: rule__Class__Group_5_3__1 : rule__Class__Group_5_3__1__Impl ;
    public final void rule__Class__Group_5_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1223:1: ( rule__Class__Group_5_3__1__Impl )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1224:2: rule__Class__Group_5_3__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group_5_3__1__Impl_in_rule__Class__Group_5_3__12362);
            rule__Class__Group_5_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_5_3__1"


    // $ANTLR start "rule__Class__Group_5_3__1__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1230:1: rule__Class__Group_5_3__1__Impl : ( ( rule__Class__AttributesAssignment_5_3_1 ) ) ;
    public final void rule__Class__Group_5_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1234:1: ( ( ( rule__Class__AttributesAssignment_5_3_1 ) ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1235:1: ( ( rule__Class__AttributesAssignment_5_3_1 ) )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1235:1: ( ( rule__Class__AttributesAssignment_5_3_1 ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1236:1: ( rule__Class__AttributesAssignment_5_3_1 )
            {
             before(grammarAccess.getClassAccess().getAttributesAssignment_5_3_1()); 
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1237:1: ( rule__Class__AttributesAssignment_5_3_1 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1237:2: rule__Class__AttributesAssignment_5_3_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__AttributesAssignment_5_3_1_in_rule__Class__Group_5_3__1__Impl2389);
            rule__Class__AttributesAssignment_5_3_1();

            state._fsp--;


            }

             after(grammarAccess.getClassAccess().getAttributesAssignment_5_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_5_3__1__Impl"


    // $ANTLR start "rule__Attribute__Group__0"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1251:1: rule__Attribute__Group__0 : rule__Attribute__Group__0__Impl rule__Attribute__Group__1 ;
    public final void rule__Attribute__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1255:1: ( rule__Attribute__Group__0__Impl rule__Attribute__Group__1 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1256:2: rule__Attribute__Group__0__Impl rule__Attribute__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__0__Impl_in_rule__Attribute__Group__02423);
            rule__Attribute__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__1_in_rule__Attribute__Group__02426);
            rule__Attribute__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__0"


    // $ANTLR start "rule__Attribute__Group__0__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1263:1: rule__Attribute__Group__0__Impl : ( () ) ;
    public final void rule__Attribute__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1267:1: ( ( () ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1268:1: ( () )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1268:1: ( () )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1269:1: ()
            {
             before(grammarAccess.getAttributeAccess().getAttributeAction_0()); 
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1270:1: ()
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1272:1: 
            {
            }

             after(grammarAccess.getAttributeAccess().getAttributeAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__0__Impl"


    // $ANTLR start "rule__Attribute__Group__1"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1282:1: rule__Attribute__Group__1 : rule__Attribute__Group__1__Impl rule__Attribute__Group__2 ;
    public final void rule__Attribute__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1286:1: ( rule__Attribute__Group__1__Impl rule__Attribute__Group__2 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1287:2: rule__Attribute__Group__1__Impl rule__Attribute__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__1__Impl_in_rule__Attribute__Group__12484);
            rule__Attribute__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__2_in_rule__Attribute__Group__12487);
            rule__Attribute__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__1"


    // $ANTLR start "rule__Attribute__Group__1__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1294:1: rule__Attribute__Group__1__Impl : ( 'Attribute' ) ;
    public final void rule__Attribute__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1298:1: ( ( 'Attribute' ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1299:1: ( 'Attribute' )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1299:1: ( 'Attribute' )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1300:1: 'Attribute'
            {
             before(grammarAccess.getAttributeAccess().getAttributeKeyword_1()); 
            match(input,21,FollowSets000.FOLLOW_21_in_rule__Attribute__Group__1__Impl2515); 
             after(grammarAccess.getAttributeAccess().getAttributeKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__1__Impl"


    // $ANTLR start "rule__Attribute__Group__2"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1313:1: rule__Attribute__Group__2 : rule__Attribute__Group__2__Impl ;
    public final void rule__Attribute__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1317:1: ( rule__Attribute__Group__2__Impl )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1318:2: rule__Attribute__Group__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__2__Impl_in_rule__Attribute__Group__22546);
            rule__Attribute__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__2"


    // $ANTLR start "rule__Attribute__Group__2__Impl"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1324:1: rule__Attribute__Group__2__Impl : ( ( rule__Attribute__NameAssignment_2 ) ) ;
    public final void rule__Attribute__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1328:1: ( ( ( rule__Attribute__NameAssignment_2 ) ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1329:1: ( ( rule__Attribute__NameAssignment_2 ) )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1329:1: ( ( rule__Attribute__NameAssignment_2 ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1330:1: ( rule__Attribute__NameAssignment_2 )
            {
             before(grammarAccess.getAttributeAccess().getNameAssignment_2()); 
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1331:1: ( rule__Attribute__NameAssignment_2 )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1331:2: rule__Attribute__NameAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__NameAssignment_2_in_rule__Attribute__Group__2__Impl2573);
            rule__Attribute__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getAttributeAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__2__Impl"


    // $ANTLR start "rule__Model__ClassesAssignment_3_2"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1348:1: rule__Model__ClassesAssignment_3_2 : ( ruleClass ) ;
    public final void rule__Model__ClassesAssignment_3_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1352:1: ( ( ruleClass ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1353:1: ( ruleClass )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1353:1: ( ruleClass )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1354:1: ruleClass
            {
             before(grammarAccess.getModelAccess().getClassesClassParserRuleCall_3_2_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleClass_in_rule__Model__ClassesAssignment_3_22614);
            ruleClass();

            state._fsp--;

             after(grammarAccess.getModelAccess().getClassesClassParserRuleCall_3_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__ClassesAssignment_3_2"


    // $ANTLR start "rule__Model__ClassesAssignment_3_3_1"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1363:1: rule__Model__ClassesAssignment_3_3_1 : ( ruleClass ) ;
    public final void rule__Model__ClassesAssignment_3_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1367:1: ( ( ruleClass ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1368:1: ( ruleClass )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1368:1: ( ruleClass )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1369:1: ruleClass
            {
             before(grammarAccess.getModelAccess().getClassesClassParserRuleCall_3_3_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleClass_in_rule__Model__ClassesAssignment_3_3_12645);
            ruleClass();

            state._fsp--;

             after(grammarAccess.getModelAccess().getClassesClassParserRuleCall_3_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__ClassesAssignment_3_3_1"


    // $ANTLR start "rule__Class__NameAssignment_2"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1378:1: rule__Class__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__Class__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1382:1: ( ( ruleEString ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1383:1: ( ruleEString )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1383:1: ( ruleEString )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1384:1: ruleEString
            {
             before(grammarAccess.getClassAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__Class__NameAssignment_22676);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getClassAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__NameAssignment_2"


    // $ANTLR start "rule__Class__ParentsAssignment_4_2"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1393:1: rule__Class__ParentsAssignment_4_2 : ( ( ruleEString ) ) ;
    public final void rule__Class__ParentsAssignment_4_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1397:1: ( ( ( ruleEString ) ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1398:1: ( ( ruleEString ) )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1398:1: ( ( ruleEString ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1399:1: ( ruleEString )
            {
             before(grammarAccess.getClassAccess().getParentsClassCrossReference_4_2_0()); 
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1400:1: ( ruleEString )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1401:1: ruleEString
            {
             before(grammarAccess.getClassAccess().getParentsClassEStringParserRuleCall_4_2_0_1()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__Class__ParentsAssignment_4_22711);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getClassAccess().getParentsClassEStringParserRuleCall_4_2_0_1()); 

            }

             after(grammarAccess.getClassAccess().getParentsClassCrossReference_4_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__ParentsAssignment_4_2"


    // $ANTLR start "rule__Class__ParentsAssignment_4_3_1"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1412:1: rule__Class__ParentsAssignment_4_3_1 : ( ( ruleEString ) ) ;
    public final void rule__Class__ParentsAssignment_4_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1416:1: ( ( ( ruleEString ) ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1417:1: ( ( ruleEString ) )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1417:1: ( ( ruleEString ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1418:1: ( ruleEString )
            {
             before(grammarAccess.getClassAccess().getParentsClassCrossReference_4_3_1_0()); 
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1419:1: ( ruleEString )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1420:1: ruleEString
            {
             before(grammarAccess.getClassAccess().getParentsClassEStringParserRuleCall_4_3_1_0_1()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__Class__ParentsAssignment_4_3_12750);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getClassAccess().getParentsClassEStringParserRuleCall_4_3_1_0_1()); 

            }

             after(grammarAccess.getClassAccess().getParentsClassCrossReference_4_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__ParentsAssignment_4_3_1"


    // $ANTLR start "rule__Class__AttributesAssignment_5_2"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1431:1: rule__Class__AttributesAssignment_5_2 : ( ruleAttribute ) ;
    public final void rule__Class__AttributesAssignment_5_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1435:1: ( ( ruleAttribute ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1436:1: ( ruleAttribute )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1436:1: ( ruleAttribute )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1437:1: ruleAttribute
            {
             before(grammarAccess.getClassAccess().getAttributesAttributeParserRuleCall_5_2_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleAttribute_in_rule__Class__AttributesAssignment_5_22785);
            ruleAttribute();

            state._fsp--;

             after(grammarAccess.getClassAccess().getAttributesAttributeParserRuleCall_5_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__AttributesAssignment_5_2"


    // $ANTLR start "rule__Class__AttributesAssignment_5_3_1"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1446:1: rule__Class__AttributesAssignment_5_3_1 : ( ruleAttribute ) ;
    public final void rule__Class__AttributesAssignment_5_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1450:1: ( ( ruleAttribute ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1451:1: ( ruleAttribute )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1451:1: ( ruleAttribute )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1452:1: ruleAttribute
            {
             before(grammarAccess.getClassAccess().getAttributesAttributeParserRuleCall_5_3_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleAttribute_in_rule__Class__AttributesAssignment_5_3_12816);
            ruleAttribute();

            state._fsp--;

             after(grammarAccess.getClassAccess().getAttributesAttributeParserRuleCall_5_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__AttributesAssignment_5_3_1"


    // $ANTLR start "rule__Attribute__NameAssignment_2"
    // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1461:1: rule__Attribute__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__Attribute__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1465:1: ( ( ruleEString ) )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1466:1: ( ruleEString )
            {
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1466:1: ( ruleEString )
            // ../miniuml.text.ui/src-gen/miniuml/text/ui/contentassist/antlr/internal/InternalMiniuml.g:1467:1: ruleEString
            {
             before(grammarAccess.getAttributeAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__Attribute__NameAssignment_22847);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getAttributeAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__NameAssignment_2"

    // Delegated rules


 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_ruleModel_in_entryRuleModel61 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleModel68 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Model__Group__0_in_ruleModel94 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClass_in_entryRuleClass121 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleClass128 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__Group__0_in_ruleClass154 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_entryRuleEString181 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleEString188 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EString__Alternatives_in_ruleEString214 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAttribute_in_entryRuleAttribute241 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleAttribute248 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group__0_in_ruleAttribute274 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_STRING_in_rule__EString__Alternatives310 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rule__EString__Alternatives327 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Model__Group__0__Impl_in_rule__Model__Group__0357 = new BitSet(new long[]{0x0000000000000800L});
        public static final BitSet FOLLOW_rule__Model__Group__1_in_rule__Model__Group__0360 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Model__Group__1__Impl_in_rule__Model__Group__1418 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_rule__Model__Group__2_in_rule__Model__Group__1421 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_11_in_rule__Model__Group__1__Impl449 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Model__Group__2__Impl_in_rule__Model__Group__2480 = new BitSet(new long[]{0x0000000000006000L});
        public static final BitSet FOLLOW_rule__Model__Group__3_in_rule__Model__Group__2483 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_12_in_rule__Model__Group__2__Impl511 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Model__Group__3__Impl_in_rule__Model__Group__3542 = new BitSet(new long[]{0x0000000000006000L});
        public static final BitSet FOLLOW_rule__Model__Group__4_in_rule__Model__Group__3545 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Model__Group_3__0_in_rule__Model__Group__3__Impl572 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Model__Group__4__Impl_in_rule__Model__Group__4603 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_13_in_rule__Model__Group__4__Impl631 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Model__Group_3__0__Impl_in_rule__Model__Group_3__0672 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_rule__Model__Group_3__1_in_rule__Model__Group_3__0675 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_14_in_rule__Model__Group_3__0__Impl703 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Model__Group_3__1__Impl_in_rule__Model__Group_3__1734 = new BitSet(new long[]{0x0000000000010000L});
        public static final BitSet FOLLOW_rule__Model__Group_3__2_in_rule__Model__Group_3__1737 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_12_in_rule__Model__Group_3__1__Impl765 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Model__Group_3__2__Impl_in_rule__Model__Group_3__2796 = new BitSet(new long[]{0x000000000000A000L});
        public static final BitSet FOLLOW_rule__Model__Group_3__3_in_rule__Model__Group_3__2799 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Model__ClassesAssignment_3_2_in_rule__Model__Group_3__2__Impl826 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Model__Group_3__3__Impl_in_rule__Model__Group_3__3856 = new BitSet(new long[]{0x000000000000A000L});
        public static final BitSet FOLLOW_rule__Model__Group_3__4_in_rule__Model__Group_3__3859 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Model__Group_3_3__0_in_rule__Model__Group_3__3__Impl886 = new BitSet(new long[]{0x0000000000008002L});
        public static final BitSet FOLLOW_rule__Model__Group_3__4__Impl_in_rule__Model__Group_3__4917 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_13_in_rule__Model__Group_3__4__Impl945 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Model__Group_3_3__0__Impl_in_rule__Model__Group_3_3__0986 = new BitSet(new long[]{0x0000000000010000L});
        public static final BitSet FOLLOW_rule__Model__Group_3_3__1_in_rule__Model__Group_3_3__0989 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_15_in_rule__Model__Group_3_3__0__Impl1017 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Model__Group_3_3__1__Impl_in_rule__Model__Group_3_3__11048 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Model__ClassesAssignment_3_3_1_in_rule__Model__Group_3_3__1__Impl1075 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__Group__0__Impl_in_rule__Class__Group__01109 = new BitSet(new long[]{0x0000000000010000L});
        public static final BitSet FOLLOW_rule__Class__Group__1_in_rule__Class__Group__01112 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__Group__1__Impl_in_rule__Class__Group__11170 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__Class__Group__2_in_rule__Class__Group__11173 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_16_in_rule__Class__Group__1__Impl1201 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__Group__2__Impl_in_rule__Class__Group__21232 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_rule__Class__Group__3_in_rule__Class__Group__21235 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__NameAssignment_2_in_rule__Class__Group__2__Impl1262 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__Group__3__Impl_in_rule__Class__Group__31292 = new BitSet(new long[]{0x0000000000122000L});
        public static final BitSet FOLLOW_rule__Class__Group__4_in_rule__Class__Group__31295 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_12_in_rule__Class__Group__3__Impl1323 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__Group__4__Impl_in_rule__Class__Group__41354 = new BitSet(new long[]{0x0000000000122000L});
        public static final BitSet FOLLOW_rule__Class__Group__5_in_rule__Class__Group__41357 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__Group_4__0_in_rule__Class__Group__4__Impl1384 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__Group__5__Impl_in_rule__Class__Group__51415 = new BitSet(new long[]{0x0000000000122000L});
        public static final BitSet FOLLOW_rule__Class__Group__6_in_rule__Class__Group__51418 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__Group_5__0_in_rule__Class__Group__5__Impl1445 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__Group__6__Impl_in_rule__Class__Group__61476 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_13_in_rule__Class__Group__6__Impl1504 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__Group_4__0__Impl_in_rule__Class__Group_4__01549 = new BitSet(new long[]{0x0000000000040000L});
        public static final BitSet FOLLOW_rule__Class__Group_4__1_in_rule__Class__Group_4__01552 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_17_in_rule__Class__Group_4__0__Impl1580 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__Group_4__1__Impl_in_rule__Class__Group_4__11611 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__Class__Group_4__2_in_rule__Class__Group_4__11614 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_18_in_rule__Class__Group_4__1__Impl1642 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__Group_4__2__Impl_in_rule__Class__Group_4__21673 = new BitSet(new long[]{0x0000000000088000L});
        public static final BitSet FOLLOW_rule__Class__Group_4__3_in_rule__Class__Group_4__21676 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__ParentsAssignment_4_2_in_rule__Class__Group_4__2__Impl1703 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__Group_4__3__Impl_in_rule__Class__Group_4__31733 = new BitSet(new long[]{0x0000000000088000L});
        public static final BitSet FOLLOW_rule__Class__Group_4__4_in_rule__Class__Group_4__31736 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__Group_4_3__0_in_rule__Class__Group_4__3__Impl1763 = new BitSet(new long[]{0x0000000000008002L});
        public static final BitSet FOLLOW_rule__Class__Group_4__4__Impl_in_rule__Class__Group_4__41794 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_19_in_rule__Class__Group_4__4__Impl1822 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__Group_4_3__0__Impl_in_rule__Class__Group_4_3__01863 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__Class__Group_4_3__1_in_rule__Class__Group_4_3__01866 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_15_in_rule__Class__Group_4_3__0__Impl1894 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__Group_4_3__1__Impl_in_rule__Class__Group_4_3__11925 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__ParentsAssignment_4_3_1_in_rule__Class__Group_4_3__1__Impl1952 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__Group_5__0__Impl_in_rule__Class__Group_5__01986 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_rule__Class__Group_5__1_in_rule__Class__Group_5__01989 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_20_in_rule__Class__Group_5__0__Impl2017 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__Group_5__1__Impl_in_rule__Class__Group_5__12048 = new BitSet(new long[]{0x0000000000200000L});
        public static final BitSet FOLLOW_rule__Class__Group_5__2_in_rule__Class__Group_5__12051 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_12_in_rule__Class__Group_5__1__Impl2079 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__Group_5__2__Impl_in_rule__Class__Group_5__22110 = new BitSet(new long[]{0x000000000000A000L});
        public static final BitSet FOLLOW_rule__Class__Group_5__3_in_rule__Class__Group_5__22113 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__AttributesAssignment_5_2_in_rule__Class__Group_5__2__Impl2140 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__Group_5__3__Impl_in_rule__Class__Group_5__32170 = new BitSet(new long[]{0x000000000000A000L});
        public static final BitSet FOLLOW_rule__Class__Group_5__4_in_rule__Class__Group_5__32173 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__Group_5_3__0_in_rule__Class__Group_5__3__Impl2200 = new BitSet(new long[]{0x0000000000008002L});
        public static final BitSet FOLLOW_rule__Class__Group_5__4__Impl_in_rule__Class__Group_5__42231 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_13_in_rule__Class__Group_5__4__Impl2259 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__Group_5_3__0__Impl_in_rule__Class__Group_5_3__02300 = new BitSet(new long[]{0x0000000000200000L});
        public static final BitSet FOLLOW_rule__Class__Group_5_3__1_in_rule__Class__Group_5_3__02303 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_15_in_rule__Class__Group_5_3__0__Impl2331 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__Group_5_3__1__Impl_in_rule__Class__Group_5_3__12362 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__AttributesAssignment_5_3_1_in_rule__Class__Group_5_3__1__Impl2389 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group__0__Impl_in_rule__Attribute__Group__02423 = new BitSet(new long[]{0x0000000000200000L});
        public static final BitSet FOLLOW_rule__Attribute__Group__1_in_rule__Attribute__Group__02426 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group__1__Impl_in_rule__Attribute__Group__12484 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__Attribute__Group__2_in_rule__Attribute__Group__12487 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_21_in_rule__Attribute__Group__1__Impl2515 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group__2__Impl_in_rule__Attribute__Group__22546 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__NameAssignment_2_in_rule__Attribute__Group__2__Impl2573 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClass_in_rule__Model__ClassesAssignment_3_22614 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClass_in_rule__Model__ClassesAssignment_3_3_12645 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__Class__NameAssignment_22676 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__Class__ParentsAssignment_4_22711 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__Class__ParentsAssignment_4_3_12750 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAttribute_in_rule__Class__AttributesAssignment_5_22785 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAttribute_in_rule__Class__AttributesAssignment_5_3_12816 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__Attribute__NameAssignment_22847 = new BitSet(new long[]{0x0000000000000002L});
    }


}