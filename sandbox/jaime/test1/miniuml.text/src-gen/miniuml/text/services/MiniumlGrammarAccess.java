/*
 * generated by Xtext
 */
package miniuml.text.services;

import com.google.inject.Singleton;
import com.google.inject.Inject;

import java.util.List;

import org.eclipse.xtext.*;
import org.eclipse.xtext.service.GrammarProvider;
import org.eclipse.xtext.service.AbstractElementFinder.*;

import org.eclipse.xtext.common.services.TerminalsGrammarAccess;

@Singleton
public class MiniumlGrammarAccess extends AbstractGrammarElementFinder {
	
	
	public class ModelElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "Model");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Action cModelAction_0 = (Action)cGroup.eContents().get(0);
		private final Keyword cModelKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Keyword cLeftCurlyBracketKeyword_2 = (Keyword)cGroup.eContents().get(2);
		private final Group cGroup_3 = (Group)cGroup.eContents().get(3);
		private final Keyword cClassesKeyword_3_0 = (Keyword)cGroup_3.eContents().get(0);
		private final Keyword cLeftCurlyBracketKeyword_3_1 = (Keyword)cGroup_3.eContents().get(1);
		private final Assignment cClassesAssignment_3_2 = (Assignment)cGroup_3.eContents().get(2);
		private final RuleCall cClassesClassParserRuleCall_3_2_0 = (RuleCall)cClassesAssignment_3_2.eContents().get(0);
		private final Group cGroup_3_3 = (Group)cGroup_3.eContents().get(3);
		private final Keyword cCommaKeyword_3_3_0 = (Keyword)cGroup_3_3.eContents().get(0);
		private final Assignment cClassesAssignment_3_3_1 = (Assignment)cGroup_3_3.eContents().get(1);
		private final RuleCall cClassesClassParserRuleCall_3_3_1_0 = (RuleCall)cClassesAssignment_3_3_1.eContents().get(0);
		private final Keyword cRightCurlyBracketKeyword_3_4 = (Keyword)cGroup_3.eContents().get(4);
		private final Keyword cRightCurlyBracketKeyword_4 = (Keyword)cGroup.eContents().get(4);
		
		//Model:
		//	{Model} "Model" "{" ("classes" "{" classes+=Class ("," classes+=Class)* "}")? "}";
		@Override public ParserRule getRule() { return rule; }

		//{Model} "Model" "{" ("classes" "{" classes+=Class ("," classes+=Class)* "}")? "}"
		public Group getGroup() { return cGroup; }

		//{Model}
		public Action getModelAction_0() { return cModelAction_0; }

		//"Model"
		public Keyword getModelKeyword_1() { return cModelKeyword_1; }

		//"{"
		public Keyword getLeftCurlyBracketKeyword_2() { return cLeftCurlyBracketKeyword_2; }

		//("classes" "{" classes+=Class ("," classes+=Class)* "}")?
		public Group getGroup_3() { return cGroup_3; }

		//"classes"
		public Keyword getClassesKeyword_3_0() { return cClassesKeyword_3_0; }

		//"{"
		public Keyword getLeftCurlyBracketKeyword_3_1() { return cLeftCurlyBracketKeyword_3_1; }

		//classes+=Class
		public Assignment getClassesAssignment_3_2() { return cClassesAssignment_3_2; }

		//Class
		public RuleCall getClassesClassParserRuleCall_3_2_0() { return cClassesClassParserRuleCall_3_2_0; }

		//("," classes+=Class)*
		public Group getGroup_3_3() { return cGroup_3_3; }

		//","
		public Keyword getCommaKeyword_3_3_0() { return cCommaKeyword_3_3_0; }

		//classes+=Class
		public Assignment getClassesAssignment_3_3_1() { return cClassesAssignment_3_3_1; }

		//Class
		public RuleCall getClassesClassParserRuleCall_3_3_1_0() { return cClassesClassParserRuleCall_3_3_1_0; }

		//"}"
		public Keyword getRightCurlyBracketKeyword_3_4() { return cRightCurlyBracketKeyword_3_4; }

		//"}"
		public Keyword getRightCurlyBracketKeyword_4() { return cRightCurlyBracketKeyword_4; }
	}

	public class ClassElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "Class");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Action cClassAction_0 = (Action)cGroup.eContents().get(0);
		private final Keyword cClassKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Assignment cNameAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final RuleCall cNameEStringParserRuleCall_2_0 = (RuleCall)cNameAssignment_2.eContents().get(0);
		private final Keyword cLeftCurlyBracketKeyword_3 = (Keyword)cGroup.eContents().get(3);
		private final Group cGroup_4 = (Group)cGroup.eContents().get(4);
		private final Keyword cParentsKeyword_4_0 = (Keyword)cGroup_4.eContents().get(0);
		private final Keyword cLeftParenthesisKeyword_4_1 = (Keyword)cGroup_4.eContents().get(1);
		private final Assignment cParentsAssignment_4_2 = (Assignment)cGroup_4.eContents().get(2);
		private final CrossReference cParentsClassCrossReference_4_2_0 = (CrossReference)cParentsAssignment_4_2.eContents().get(0);
		private final RuleCall cParentsClassEStringParserRuleCall_4_2_0_1 = (RuleCall)cParentsClassCrossReference_4_2_0.eContents().get(1);
		private final Group cGroup_4_3 = (Group)cGroup_4.eContents().get(3);
		private final Keyword cCommaKeyword_4_3_0 = (Keyword)cGroup_4_3.eContents().get(0);
		private final Assignment cParentsAssignment_4_3_1 = (Assignment)cGroup_4_3.eContents().get(1);
		private final CrossReference cParentsClassCrossReference_4_3_1_0 = (CrossReference)cParentsAssignment_4_3_1.eContents().get(0);
		private final RuleCall cParentsClassEStringParserRuleCall_4_3_1_0_1 = (RuleCall)cParentsClassCrossReference_4_3_1_0.eContents().get(1);
		private final Keyword cRightParenthesisKeyword_4_4 = (Keyword)cGroup_4.eContents().get(4);
		private final Group cGroup_5 = (Group)cGroup.eContents().get(5);
		private final Keyword cAttributesKeyword_5_0 = (Keyword)cGroup_5.eContents().get(0);
		private final Keyword cLeftCurlyBracketKeyword_5_1 = (Keyword)cGroup_5.eContents().get(1);
		private final Assignment cAttributesAssignment_5_2 = (Assignment)cGroup_5.eContents().get(2);
		private final RuleCall cAttributesAttributeParserRuleCall_5_2_0 = (RuleCall)cAttributesAssignment_5_2.eContents().get(0);
		private final Group cGroup_5_3 = (Group)cGroup_5.eContents().get(3);
		private final Keyword cCommaKeyword_5_3_0 = (Keyword)cGroup_5_3.eContents().get(0);
		private final Assignment cAttributesAssignment_5_3_1 = (Assignment)cGroup_5_3.eContents().get(1);
		private final RuleCall cAttributesAttributeParserRuleCall_5_3_1_0 = (RuleCall)cAttributesAssignment_5_3_1.eContents().get(0);
		private final Keyword cRightCurlyBracketKeyword_5_4 = (Keyword)cGroup_5.eContents().get(4);
		private final Keyword cRightCurlyBracketKeyword_6 = (Keyword)cGroup.eContents().get(6);
		
		//Class:
		//	{Class} "Class" name=EString "{" ("parents" "(" parents+=[Class|EString] ("," parents+=[Class|EString])* ")")?
		//	("attributes" "{" attributes+=Attribute ("," attributes+=Attribute)* "}")? "}";
		@Override public ParserRule getRule() { return rule; }

		//{Class} "Class" name=EString "{" ("parents" "(" parents+=[Class|EString] ("," parents+=[Class|EString])* ")")?
		//("attributes" "{" attributes+=Attribute ("," attributes+=Attribute)* "}")? "}"
		public Group getGroup() { return cGroup; }

		//{Class}
		public Action getClassAction_0() { return cClassAction_0; }

		//"Class"
		public Keyword getClassKeyword_1() { return cClassKeyword_1; }

		//name=EString
		public Assignment getNameAssignment_2() { return cNameAssignment_2; }

		//EString
		public RuleCall getNameEStringParserRuleCall_2_0() { return cNameEStringParserRuleCall_2_0; }

		//"{"
		public Keyword getLeftCurlyBracketKeyword_3() { return cLeftCurlyBracketKeyword_3; }

		//("parents" "(" parents+=[Class|EString] ("," parents+=[Class|EString])* ")")?
		public Group getGroup_4() { return cGroup_4; }

		//"parents"
		public Keyword getParentsKeyword_4_0() { return cParentsKeyword_4_0; }

		//"("
		public Keyword getLeftParenthesisKeyword_4_1() { return cLeftParenthesisKeyword_4_1; }

		//parents+=[Class|EString]
		public Assignment getParentsAssignment_4_2() { return cParentsAssignment_4_2; }

		//[Class|EString]
		public CrossReference getParentsClassCrossReference_4_2_0() { return cParentsClassCrossReference_4_2_0; }

		//EString
		public RuleCall getParentsClassEStringParserRuleCall_4_2_0_1() { return cParentsClassEStringParserRuleCall_4_2_0_1; }

		//("," parents+=[Class|EString])*
		public Group getGroup_4_3() { return cGroup_4_3; }

		//","
		public Keyword getCommaKeyword_4_3_0() { return cCommaKeyword_4_3_0; }

		//parents+=[Class|EString]
		public Assignment getParentsAssignment_4_3_1() { return cParentsAssignment_4_3_1; }

		//[Class|EString]
		public CrossReference getParentsClassCrossReference_4_3_1_0() { return cParentsClassCrossReference_4_3_1_0; }

		//EString
		public RuleCall getParentsClassEStringParserRuleCall_4_3_1_0_1() { return cParentsClassEStringParserRuleCall_4_3_1_0_1; }

		//")"
		public Keyword getRightParenthesisKeyword_4_4() { return cRightParenthesisKeyword_4_4; }

		//("attributes" "{" attributes+=Attribute ("," attributes+=Attribute)* "}")?
		public Group getGroup_5() { return cGroup_5; }

		//"attributes"
		public Keyword getAttributesKeyword_5_0() { return cAttributesKeyword_5_0; }

		//"{"
		public Keyword getLeftCurlyBracketKeyword_5_1() { return cLeftCurlyBracketKeyword_5_1; }

		//attributes+=Attribute
		public Assignment getAttributesAssignment_5_2() { return cAttributesAssignment_5_2; }

		//Attribute
		public RuleCall getAttributesAttributeParserRuleCall_5_2_0() { return cAttributesAttributeParserRuleCall_5_2_0; }

		//("," attributes+=Attribute)*
		public Group getGroup_5_3() { return cGroup_5_3; }

		//","
		public Keyword getCommaKeyword_5_3_0() { return cCommaKeyword_5_3_0; }

		//attributes+=Attribute
		public Assignment getAttributesAssignment_5_3_1() { return cAttributesAssignment_5_3_1; }

		//Attribute
		public RuleCall getAttributesAttributeParserRuleCall_5_3_1_0() { return cAttributesAttributeParserRuleCall_5_3_1_0; }

		//"}"
		public Keyword getRightCurlyBracketKeyword_5_4() { return cRightCurlyBracketKeyword_5_4; }

		//"}"
		public Keyword getRightCurlyBracketKeyword_6() { return cRightCurlyBracketKeyword_6; }
	}

	public class EStringElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "EString");
		private final Alternatives cAlternatives = (Alternatives)rule.eContents().get(1);
		private final RuleCall cSTRINGTerminalRuleCall_0 = (RuleCall)cAlternatives.eContents().get(0);
		private final RuleCall cIDTerminalRuleCall_1 = (RuleCall)cAlternatives.eContents().get(1);
		
		//EString returns ecore::EString:
		//	STRING | ID;
		@Override public ParserRule getRule() { return rule; }

		//STRING | ID
		public Alternatives getAlternatives() { return cAlternatives; }

		//STRING
		public RuleCall getSTRINGTerminalRuleCall_0() { return cSTRINGTerminalRuleCall_0; }

		//ID
		public RuleCall getIDTerminalRuleCall_1() { return cIDTerminalRuleCall_1; }
	}

	public class AttributeElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "Attribute");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Action cAttributeAction_0 = (Action)cGroup.eContents().get(0);
		private final Keyword cAttributeKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Assignment cNameAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final RuleCall cNameEStringParserRuleCall_2_0 = (RuleCall)cNameAssignment_2.eContents().get(0);
		
		//Attribute:
		//	{Attribute} "Attribute" name=EString;
		@Override public ParserRule getRule() { return rule; }

		//{Attribute} "Attribute" name=EString
		public Group getGroup() { return cGroup; }

		//{Attribute}
		public Action getAttributeAction_0() { return cAttributeAction_0; }

		//"Attribute"
		public Keyword getAttributeKeyword_1() { return cAttributeKeyword_1; }

		//name=EString
		public Assignment getNameAssignment_2() { return cNameAssignment_2; }

		//EString
		public RuleCall getNameEStringParserRuleCall_2_0() { return cNameEStringParserRuleCall_2_0; }
	}
	
	
	private final ModelElements pModel;
	private final ClassElements pClass;
	private final EStringElements pEString;
	private final AttributeElements pAttribute;
	
	private final Grammar grammar;

	private final TerminalsGrammarAccess gaTerminals;

	@Inject
	public MiniumlGrammarAccess(GrammarProvider grammarProvider,
		TerminalsGrammarAccess gaTerminals) {
		this.grammar = internalFindGrammar(grammarProvider);
		this.gaTerminals = gaTerminals;
		this.pModel = new ModelElements();
		this.pClass = new ClassElements();
		this.pEString = new EStringElements();
		this.pAttribute = new AttributeElements();
	}
	
	protected Grammar internalFindGrammar(GrammarProvider grammarProvider) {
		Grammar grammar = grammarProvider.getGrammar(this);
		while (grammar != null) {
			if ("miniuml.text.Miniuml".equals(grammar.getName())) {
				return grammar;
			}
			List<Grammar> grammars = grammar.getUsedGrammars();
			if (!grammars.isEmpty()) {
				grammar = grammars.iterator().next();
			} else {
				return null;
			}
		}
		return grammar;
	}
	
	@Override
	public Grammar getGrammar() {
		return grammar;
	}
	

	public TerminalsGrammarAccess getTerminalsGrammarAccess() {
		return gaTerminals;
	}

	
	//Model:
	//	{Model} "Model" "{" ("classes" "{" classes+=Class ("," classes+=Class)* "}")? "}";
	public ModelElements getModelAccess() {
		return pModel;
	}
	
	public ParserRule getModelRule() {
		return getModelAccess().getRule();
	}

	//Class:
	//	{Class} "Class" name=EString "{" ("parents" "(" parents+=[Class|EString] ("," parents+=[Class|EString])* ")")?
	//	("attributes" "{" attributes+=Attribute ("," attributes+=Attribute)* "}")? "}";
	public ClassElements getClassAccess() {
		return pClass;
	}
	
	public ParserRule getClassRule() {
		return getClassAccess().getRule();
	}

	//EString returns ecore::EString:
	//	STRING | ID;
	public EStringElements getEStringAccess() {
		return pEString;
	}
	
	public ParserRule getEStringRule() {
		return getEStringAccess().getRule();
	}

	//Attribute:
	//	{Attribute} "Attribute" name=EString;
	public AttributeElements getAttributeAccess() {
		return pAttribute;
	}
	
	public ParserRule getAttributeRule() {
		return getAttributeAccess().getRule();
	}

	//terminal ID:
	//	"^"? ("a".."z" | "A".."Z" | "_") ("a".."z" | "A".."Z" | "_" | "0".."9")*;
	public TerminalRule getIDRule() {
		return gaTerminals.getIDRule();
	} 

	//terminal INT returns ecore::EInt:
	//	"0".."9"+;
	public TerminalRule getINTRule() {
		return gaTerminals.getINTRule();
	} 

	//terminal STRING:
	//	"\"" ("\\" . / * 'b'|'t'|'n'|'f'|'r'|'u'|'"'|"'"|'\\' * / | !("\\" | "\""))* "\"" | "\'" ("\\" .
	//	/ * 'b'|'t'|'n'|'f'|'r'|'u'|'"'|"'"|'\\' * / | !("\\" | "\'"))* "\'";
	public TerminalRule getSTRINGRule() {
		return gaTerminals.getSTRINGRule();
	} 

	//terminal ML_COMMENT:
	//	"/ *"->"* /";
	public TerminalRule getML_COMMENTRule() {
		return gaTerminals.getML_COMMENTRule();
	} 

	//terminal SL_COMMENT:
	//	"//" !("\n" | "\r")* ("\r"? "\n")?;
	public TerminalRule getSL_COMMENTRule() {
		return gaTerminals.getSL_COMMENTRule();
	} 

	//terminal WS:
	//	(" " | "\t" | "\r" | "\n")+;
	public TerminalRule getWSRule() {
		return gaTerminals.getWSRule();
	} 

	//terminal ANY_OTHER:
	//	.;
	public TerminalRule getANY_OTHERRule() {
		return gaTerminals.getANY_OTHERRule();
	} 
}
