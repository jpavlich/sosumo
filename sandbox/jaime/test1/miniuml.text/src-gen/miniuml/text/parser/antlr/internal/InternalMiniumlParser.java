package miniuml.text.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import miniuml.text.services.MiniumlGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMiniumlParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Model'", "'{'", "'classes'", "','", "'}'", "'Class'", "'parents'", "'('", "')'", "'attributes'", "'Attribute'"
    };
    public static final int RULE_ID=5;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__19=19;
    public static final int RULE_STRING=4;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=6;
    public static final int RULE_WS=9;

    // delegates
    // delegators


        public InternalMiniumlParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMiniumlParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMiniumlParser.tokenNames; }
    public String getGrammarFileName() { return "../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g"; }



     	private MiniumlGrammarAccess grammarAccess;
     	
        public InternalMiniumlParser(TokenStream input, MiniumlGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Model";	
       	}
       	
       	@Override
       	protected MiniumlGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleModel"
    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:67:1: entryRuleModel returns [EObject current=null] : iv_ruleModel= ruleModel EOF ;
    public final EObject entryRuleModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModel = null;


        try {
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:68:2: (iv_ruleModel= ruleModel EOF )
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:69:2: iv_ruleModel= ruleModel EOF
            {
             newCompositeNode(grammarAccess.getModelRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleModel_in_entryRuleModel75);
            iv_ruleModel=ruleModel();

            state._fsp--;

             current =iv_ruleModel; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleModel85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:76:1: ruleModel returns [EObject current=null] : ( () otherlv_1= 'Model' otherlv_2= '{' (otherlv_3= 'classes' otherlv_4= '{' ( (lv_classes_5_0= ruleClass ) ) (otherlv_6= ',' ( (lv_classes_7_0= ruleClass ) ) )* otherlv_8= '}' )? otherlv_9= '}' ) ;
    public final EObject ruleModel() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        EObject lv_classes_5_0 = null;

        EObject lv_classes_7_0 = null;


         enterRule(); 
            
        try {
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:79:28: ( ( () otherlv_1= 'Model' otherlv_2= '{' (otherlv_3= 'classes' otherlv_4= '{' ( (lv_classes_5_0= ruleClass ) ) (otherlv_6= ',' ( (lv_classes_7_0= ruleClass ) ) )* otherlv_8= '}' )? otherlv_9= '}' ) )
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:80:1: ( () otherlv_1= 'Model' otherlv_2= '{' (otherlv_3= 'classes' otherlv_4= '{' ( (lv_classes_5_0= ruleClass ) ) (otherlv_6= ',' ( (lv_classes_7_0= ruleClass ) ) )* otherlv_8= '}' )? otherlv_9= '}' )
            {
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:80:1: ( () otherlv_1= 'Model' otherlv_2= '{' (otherlv_3= 'classes' otherlv_4= '{' ( (lv_classes_5_0= ruleClass ) ) (otherlv_6= ',' ( (lv_classes_7_0= ruleClass ) ) )* otherlv_8= '}' )? otherlv_9= '}' )
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:80:2: () otherlv_1= 'Model' otherlv_2= '{' (otherlv_3= 'classes' otherlv_4= '{' ( (lv_classes_5_0= ruleClass ) ) (otherlv_6= ',' ( (lv_classes_7_0= ruleClass ) ) )* otherlv_8= '}' )? otherlv_9= '}'
            {
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:80:2: ()
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:81:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getModelAccess().getModelAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,11,FollowSets000.FOLLOW_11_in_ruleModel131); 

                	newLeafNode(otherlv_1, grammarAccess.getModelAccess().getModelKeyword_1());
                
            otherlv_2=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleModel143); 

                	newLeafNode(otherlv_2, grammarAccess.getModelAccess().getLeftCurlyBracketKeyword_2());
                
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:94:1: (otherlv_3= 'classes' otherlv_4= '{' ( (lv_classes_5_0= ruleClass ) ) (otherlv_6= ',' ( (lv_classes_7_0= ruleClass ) ) )* otherlv_8= '}' )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==13) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:94:3: otherlv_3= 'classes' otherlv_4= '{' ( (lv_classes_5_0= ruleClass ) ) (otherlv_6= ',' ( (lv_classes_7_0= ruleClass ) ) )* otherlv_8= '}'
                    {
                    otherlv_3=(Token)match(input,13,FollowSets000.FOLLOW_13_in_ruleModel156); 

                        	newLeafNode(otherlv_3, grammarAccess.getModelAccess().getClassesKeyword_3_0());
                        
                    otherlv_4=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleModel168); 

                        	newLeafNode(otherlv_4, grammarAccess.getModelAccess().getLeftCurlyBracketKeyword_3_1());
                        
                    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:102:1: ( (lv_classes_5_0= ruleClass ) )
                    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:103:1: (lv_classes_5_0= ruleClass )
                    {
                    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:103:1: (lv_classes_5_0= ruleClass )
                    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:104:3: lv_classes_5_0= ruleClass
                    {
                     
                    	        newCompositeNode(grammarAccess.getModelAccess().getClassesClassParserRuleCall_3_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleClass_in_ruleModel189);
                    lv_classes_5_0=ruleClass();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getModelRule());
                    	        }
                           		add(
                           			current, 
                           			"classes",
                            		lv_classes_5_0, 
                            		"Class");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:120:2: (otherlv_6= ',' ( (lv_classes_7_0= ruleClass ) ) )*
                    loop1:
                    do {
                        int alt1=2;
                        int LA1_0 = input.LA(1);

                        if ( (LA1_0==14) ) {
                            alt1=1;
                        }


                        switch (alt1) {
                    	case 1 :
                    	    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:120:4: otherlv_6= ',' ( (lv_classes_7_0= ruleClass ) )
                    	    {
                    	    otherlv_6=(Token)match(input,14,FollowSets000.FOLLOW_14_in_ruleModel202); 

                    	        	newLeafNode(otherlv_6, grammarAccess.getModelAccess().getCommaKeyword_3_3_0());
                    	        
                    	    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:124:1: ( (lv_classes_7_0= ruleClass ) )
                    	    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:125:1: (lv_classes_7_0= ruleClass )
                    	    {
                    	    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:125:1: (lv_classes_7_0= ruleClass )
                    	    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:126:3: lv_classes_7_0= ruleClass
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getModelAccess().getClassesClassParserRuleCall_3_3_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleClass_in_ruleModel223);
                    	    lv_classes_7_0=ruleClass();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getModelRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"classes",
                    	            		lv_classes_7_0, 
                    	            		"Class");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop1;
                        }
                    } while (true);

                    otherlv_8=(Token)match(input,15,FollowSets000.FOLLOW_15_in_ruleModel237); 

                        	newLeafNode(otherlv_8, grammarAccess.getModelAccess().getRightCurlyBracketKeyword_3_4());
                        

                    }
                    break;

            }

            otherlv_9=(Token)match(input,15,FollowSets000.FOLLOW_15_in_ruleModel251); 

                	newLeafNode(otherlv_9, grammarAccess.getModelAccess().getRightCurlyBracketKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleClass"
    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:158:1: entryRuleClass returns [EObject current=null] : iv_ruleClass= ruleClass EOF ;
    public final EObject entryRuleClass() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClass = null;


        try {
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:159:2: (iv_ruleClass= ruleClass EOF )
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:160:2: iv_ruleClass= ruleClass EOF
            {
             newCompositeNode(grammarAccess.getClassRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleClass_in_entryRuleClass287);
            iv_ruleClass=ruleClass();

            state._fsp--;

             current =iv_ruleClass; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleClass297); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClass"


    // $ANTLR start "ruleClass"
    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:167:1: ruleClass returns [EObject current=null] : ( () otherlv_1= 'Class' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'parents' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleAttribute ) ) )* otherlv_15= '}' )? otherlv_16= '}' ) ;
    public final EObject ruleClass() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_attributes_12_0 = null;

        EObject lv_attributes_14_0 = null;


         enterRule(); 
            
        try {
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:170:28: ( ( () otherlv_1= 'Class' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'parents' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleAttribute ) ) )* otherlv_15= '}' )? otherlv_16= '}' ) )
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:171:1: ( () otherlv_1= 'Class' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'parents' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleAttribute ) ) )* otherlv_15= '}' )? otherlv_16= '}' )
            {
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:171:1: ( () otherlv_1= 'Class' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'parents' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleAttribute ) ) )* otherlv_15= '}' )? otherlv_16= '}' )
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:171:2: () otherlv_1= 'Class' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'parents' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleAttribute ) ) )* otherlv_15= '}' )? otherlv_16= '}'
            {
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:171:2: ()
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:172:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getClassAccess().getClassAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,16,FollowSets000.FOLLOW_16_in_ruleClass343); 

                	newLeafNode(otherlv_1, grammarAccess.getClassAccess().getClassKeyword_1());
                
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:181:1: ( (lv_name_2_0= ruleEString ) )
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:182:1: (lv_name_2_0= ruleEString )
            {
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:182:1: (lv_name_2_0= ruleEString )
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:183:3: lv_name_2_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getClassAccess().getNameEStringParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleClass364);
            lv_name_2_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getClassRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_2_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_3=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleClass376); 

                	newLeafNode(otherlv_3, grammarAccess.getClassAccess().getLeftCurlyBracketKeyword_3());
                
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:203:1: (otherlv_4= 'parents' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==17) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:203:3: otherlv_4= 'parents' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')'
                    {
                    otherlv_4=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleClass389); 

                        	newLeafNode(otherlv_4, grammarAccess.getClassAccess().getParentsKeyword_4_0());
                        
                    otherlv_5=(Token)match(input,18,FollowSets000.FOLLOW_18_in_ruleClass401); 

                        	newLeafNode(otherlv_5, grammarAccess.getClassAccess().getLeftParenthesisKeyword_4_1());
                        
                    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:211:1: ( ( ruleEString ) )
                    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:212:1: ( ruleEString )
                    {
                    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:212:1: ( ruleEString )
                    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:213:3: ruleEString
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getClassRule());
                    	        }
                            
                     
                    	        newCompositeNode(grammarAccess.getClassAccess().getParentsClassCrossReference_4_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleClass424);
                    ruleEString();

                    state._fsp--;

                     
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:226:2: (otherlv_7= ',' ( ( ruleEString ) ) )*
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( (LA3_0==14) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:226:4: otherlv_7= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_7=(Token)match(input,14,FollowSets000.FOLLOW_14_in_ruleClass437); 

                    	        	newLeafNode(otherlv_7, grammarAccess.getClassAccess().getCommaKeyword_4_3_0());
                    	        
                    	    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:230:1: ( ( ruleEString ) )
                    	    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:231:1: ( ruleEString )
                    	    {
                    	    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:231:1: ( ruleEString )
                    	    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:232:3: ruleEString
                    	    {

                    	    			if (current==null) {
                    	    	            current = createModelElement(grammarAccess.getClassRule());
                    	    	        }
                    	            
                    	     
                    	    	        newCompositeNode(grammarAccess.getClassAccess().getParentsClassCrossReference_4_3_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleClass460);
                    	    ruleEString();

                    	    state._fsp--;

                    	     
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop3;
                        }
                    } while (true);

                    otherlv_9=(Token)match(input,19,FollowSets000.FOLLOW_19_in_ruleClass474); 

                        	newLeafNode(otherlv_9, grammarAccess.getClassAccess().getRightParenthesisKeyword_4_4());
                        

                    }
                    break;

            }

            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:249:3: (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleAttribute ) ) )* otherlv_15= '}' )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==20) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:249:5: otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleAttribute ) ) )* otherlv_15= '}'
                    {
                    otherlv_10=(Token)match(input,20,FollowSets000.FOLLOW_20_in_ruleClass489); 

                        	newLeafNode(otherlv_10, grammarAccess.getClassAccess().getAttributesKeyword_5_0());
                        
                    otherlv_11=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleClass501); 

                        	newLeafNode(otherlv_11, grammarAccess.getClassAccess().getLeftCurlyBracketKeyword_5_1());
                        
                    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:257:1: ( (lv_attributes_12_0= ruleAttribute ) )
                    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:258:1: (lv_attributes_12_0= ruleAttribute )
                    {
                    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:258:1: (lv_attributes_12_0= ruleAttribute )
                    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:259:3: lv_attributes_12_0= ruleAttribute
                    {
                     
                    	        newCompositeNode(grammarAccess.getClassAccess().getAttributesAttributeParserRuleCall_5_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleAttribute_in_ruleClass522);
                    lv_attributes_12_0=ruleAttribute();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getClassRule());
                    	        }
                           		add(
                           			current, 
                           			"attributes",
                            		lv_attributes_12_0, 
                            		"Attribute");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:275:2: (otherlv_13= ',' ( (lv_attributes_14_0= ruleAttribute ) ) )*
                    loop5:
                    do {
                        int alt5=2;
                        int LA5_0 = input.LA(1);

                        if ( (LA5_0==14) ) {
                            alt5=1;
                        }


                        switch (alt5) {
                    	case 1 :
                    	    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:275:4: otherlv_13= ',' ( (lv_attributes_14_0= ruleAttribute ) )
                    	    {
                    	    otherlv_13=(Token)match(input,14,FollowSets000.FOLLOW_14_in_ruleClass535); 

                    	        	newLeafNode(otherlv_13, grammarAccess.getClassAccess().getCommaKeyword_5_3_0());
                    	        
                    	    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:279:1: ( (lv_attributes_14_0= ruleAttribute ) )
                    	    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:280:1: (lv_attributes_14_0= ruleAttribute )
                    	    {
                    	    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:280:1: (lv_attributes_14_0= ruleAttribute )
                    	    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:281:3: lv_attributes_14_0= ruleAttribute
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getClassAccess().getAttributesAttributeParserRuleCall_5_3_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleAttribute_in_ruleClass556);
                    	    lv_attributes_14_0=ruleAttribute();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getClassRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"attributes",
                    	            		lv_attributes_14_0, 
                    	            		"Attribute");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop5;
                        }
                    } while (true);

                    otherlv_15=(Token)match(input,15,FollowSets000.FOLLOW_15_in_ruleClass570); 

                        	newLeafNode(otherlv_15, grammarAccess.getClassAccess().getRightCurlyBracketKeyword_5_4());
                        

                    }
                    break;

            }

            otherlv_16=(Token)match(input,15,FollowSets000.FOLLOW_15_in_ruleClass584); 

                	newLeafNode(otherlv_16, grammarAccess.getClassAccess().getRightCurlyBracketKeyword_6());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClass"


    // $ANTLR start "entryRuleEString"
    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:313:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:314:2: (iv_ruleEString= ruleEString EOF )
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:315:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_entryRuleEString621);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleEString632); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:322:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;

         enterRule(); 
            
        try {
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:325:28: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:326:1: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:326:1: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==RULE_STRING) ) {
                alt7=1;
            }
            else if ( (LA7_0==RULE_ID) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:326:6: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_ruleEString672); 

                    		current.merge(this_STRING_0);
                        
                     
                        newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 
                        

                    }
                    break;
                case 2 :
                    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:334:10: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_ruleEString698); 

                    		current.merge(this_ID_1);
                        
                     
                        newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleAttribute"
    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:349:1: entryRuleAttribute returns [EObject current=null] : iv_ruleAttribute= ruleAttribute EOF ;
    public final EObject entryRuleAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAttribute = null;


        try {
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:350:2: (iv_ruleAttribute= ruleAttribute EOF )
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:351:2: iv_ruleAttribute= ruleAttribute EOF
            {
             newCompositeNode(grammarAccess.getAttributeRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleAttribute_in_entryRuleAttribute743);
            iv_ruleAttribute=ruleAttribute();

            state._fsp--;

             current =iv_ruleAttribute; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleAttribute753); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAttribute"


    // $ANTLR start "ruleAttribute"
    // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:358:1: ruleAttribute returns [EObject current=null] : ( () otherlv_1= 'Attribute' ( (lv_name_2_0= ruleEString ) ) ) ;
    public final EObject ruleAttribute() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;


         enterRule(); 
            
        try {
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:361:28: ( ( () otherlv_1= 'Attribute' ( (lv_name_2_0= ruleEString ) ) ) )
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:362:1: ( () otherlv_1= 'Attribute' ( (lv_name_2_0= ruleEString ) ) )
            {
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:362:1: ( () otherlv_1= 'Attribute' ( (lv_name_2_0= ruleEString ) ) )
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:362:2: () otherlv_1= 'Attribute' ( (lv_name_2_0= ruleEString ) )
            {
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:362:2: ()
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:363:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getAttributeAccess().getAttributeAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,21,FollowSets000.FOLLOW_21_in_ruleAttribute799); 

                	newLeafNode(otherlv_1, grammarAccess.getAttributeAccess().getAttributeKeyword_1());
                
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:372:1: ( (lv_name_2_0= ruleEString ) )
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:373:1: (lv_name_2_0= ruleEString )
            {
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:373:1: (lv_name_2_0= ruleEString )
            // ../miniuml.text/src-gen/miniuml/text/parser/antlr/internal/InternalMiniuml.g:374:3: lv_name_2_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getAttributeAccess().getNameEStringParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleAttribute820);
            lv_name_2_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getAttributeRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_2_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAttribute"

    // Delegated rules


 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_ruleModel_in_entryRuleModel75 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleModel85 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_11_in_ruleModel131 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_12_in_ruleModel143 = new BitSet(new long[]{0x000000000000A000L});
        public static final BitSet FOLLOW_13_in_ruleModel156 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_12_in_ruleModel168 = new BitSet(new long[]{0x0000000000010000L});
        public static final BitSet FOLLOW_ruleClass_in_ruleModel189 = new BitSet(new long[]{0x000000000000C000L});
        public static final BitSet FOLLOW_14_in_ruleModel202 = new BitSet(new long[]{0x0000000000010000L});
        public static final BitSet FOLLOW_ruleClass_in_ruleModel223 = new BitSet(new long[]{0x000000000000C000L});
        public static final BitSet FOLLOW_15_in_ruleModel237 = new BitSet(new long[]{0x0000000000008000L});
        public static final BitSet FOLLOW_15_in_ruleModel251 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClass_in_entryRuleClass287 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleClass297 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_16_in_ruleClass343 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleClass364 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_12_in_ruleClass376 = new BitSet(new long[]{0x0000000000128000L});
        public static final BitSet FOLLOW_17_in_ruleClass389 = new BitSet(new long[]{0x0000000000040000L});
        public static final BitSet FOLLOW_18_in_ruleClass401 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleClass424 = new BitSet(new long[]{0x0000000000084000L});
        public static final BitSet FOLLOW_14_in_ruleClass437 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleClass460 = new BitSet(new long[]{0x0000000000084000L});
        public static final BitSet FOLLOW_19_in_ruleClass474 = new BitSet(new long[]{0x0000000000108000L});
        public static final BitSet FOLLOW_20_in_ruleClass489 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_12_in_ruleClass501 = new BitSet(new long[]{0x0000000000200000L});
        public static final BitSet FOLLOW_ruleAttribute_in_ruleClass522 = new BitSet(new long[]{0x000000000000C000L});
        public static final BitSet FOLLOW_14_in_ruleClass535 = new BitSet(new long[]{0x0000000000200000L});
        public static final BitSet FOLLOW_ruleAttribute_in_ruleClass556 = new BitSet(new long[]{0x000000000000C000L});
        public static final BitSet FOLLOW_15_in_ruleClass570 = new BitSet(new long[]{0x0000000000008000L});
        public static final BitSet FOLLOW_15_in_ruleClass584 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_entryRuleEString621 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleEString632 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_STRING_in_ruleEString672 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_ruleEString698 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAttribute_in_entryRuleAttribute743 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleAttribute753 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_21_in_ruleAttribute799 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleAttribute820 = new BitSet(new long[]{0x0000000000000002L});
    }


}