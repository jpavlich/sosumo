/*
 * generated by Xtext
 */
package miniuml.text;

import org.eclipse.xtext.junit4.IInjectorProvider;

import com.google.inject.Injector;

public class MiniumlUiInjectorProvider implements IInjectorProvider {
	
	@Override
	public Injector getInjector() {
		return miniuml.text.ui.internal.MiniumlActivator.getInstance().getInjector("miniuml.text.Miniuml");
	}
	
}
