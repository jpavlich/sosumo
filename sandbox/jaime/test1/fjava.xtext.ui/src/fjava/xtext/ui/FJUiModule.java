/*
 * generated by Xtext
 */
package fjava.xtext.ui;

import org.eclipse.ui.plugin.AbstractUIPlugin;

/**
 * Use this class to register components to be used within the IDE.
 */
public class FJUiModule extends fjava.xtext.ui.AbstractFJUiModule {
	public FJUiModule(AbstractUIPlugin plugin) {
		super(plugin);
	}
}
