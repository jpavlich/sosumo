package fjava.xtext.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import fjava.xtext.services.FJGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalFJParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'int'", "'boolean'", "'String'", "'true'", "'false'", "'class'", "'{'", "'}'", "'extends'", "';'", "'('", "')'", "','", "'return'", "'.'", "'new'", "'this'"
    };
    public static final int RULE_ID=4;
    public static final int T__27=27;
    public static final int T__26=26;
    public static final int T__25=25;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__19=19;
    public static final int RULE_STRING=5;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=6;
    public static final int RULE_WS=9;

    // delegates
    // delegators


        public InternalFJParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalFJParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalFJParser.tokenNames; }
    public String getGrammarFileName() { return "../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g"; }


     
     	private FJGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(FJGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleProgram"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:61:1: entryRuleProgram : ruleProgram EOF ;
    public final void entryRuleProgram() throws RecognitionException {
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:62:1: ( ruleProgram EOF )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:63:1: ruleProgram EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getProgramRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleProgram_in_entryRuleProgram67);
            ruleProgram();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getProgramRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleProgram74); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleProgram"


    // $ANTLR start "ruleProgram"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:70:1: ruleProgram : ( ( rule__Program__Group__0 ) ) ;
    public final void ruleProgram() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:74:2: ( ( ( rule__Program__Group__0 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:75:1: ( ( rule__Program__Group__0 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:75:1: ( ( rule__Program__Group__0 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:76:1: ( rule__Program__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getProgramAccess().getGroup()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:77:1: ( rule__Program__Group__0 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:77:2: rule__Program__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Program__Group__0_in_ruleProgram100);
            rule__Program__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getProgramAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleProgram"


    // $ANTLR start "entryRuleType"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:89:1: entryRuleType : ruleType EOF ;
    public final void entryRuleType() throws RecognitionException {
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:90:1: ( ruleType EOF )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:91:1: ruleType EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypeRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleType_in_entryRuleType127);
            ruleType();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypeRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleType134); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:98:1: ruleType : ( ( rule__Type__Alternatives ) ) ;
    public final void ruleType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:102:2: ( ( ( rule__Type__Alternatives ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:103:1: ( ( rule__Type__Alternatives ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:103:1: ( ( rule__Type__Alternatives ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:104:1: ( rule__Type__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypeAccess().getAlternatives()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:105:1: ( rule__Type__Alternatives )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:105:2: rule__Type__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__Type__Alternatives_in_ruleType160);
            rule__Type__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypeAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRuleBasicType"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:117:1: entryRuleBasicType : ruleBasicType EOF ;
    public final void entryRuleBasicType() throws RecognitionException {
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:118:1: ( ruleBasicType EOF )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:119:1: ruleBasicType EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicTypeRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleBasicType_in_entryRuleBasicType187);
            ruleBasicType();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicTypeRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleBasicType194); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBasicType"


    // $ANTLR start "ruleBasicType"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:126:1: ruleBasicType : ( ( rule__BasicType__BasicAssignment ) ) ;
    public final void ruleBasicType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:130:2: ( ( ( rule__BasicType__BasicAssignment ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:131:1: ( ( rule__BasicType__BasicAssignment ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:131:1: ( ( rule__BasicType__BasicAssignment ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:132:1: ( rule__BasicType__BasicAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicTypeAccess().getBasicAssignment()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:133:1: ( rule__BasicType__BasicAssignment )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:133:2: rule__BasicType__BasicAssignment
            {
            pushFollow(FollowSets000.FOLLOW_rule__BasicType__BasicAssignment_in_ruleBasicType220);
            rule__BasicType__BasicAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicTypeAccess().getBasicAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBasicType"


    // $ANTLR start "entryRuleClassType"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:145:1: entryRuleClassType : ruleClassType EOF ;
    public final void entryRuleClassType() throws RecognitionException {
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:146:1: ( ruleClassType EOF )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:147:1: ruleClassType EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassTypeRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleClassType_in_entryRuleClassType247);
            ruleClassType();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassTypeRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleClassType254); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleClassType"


    // $ANTLR start "ruleClassType"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:154:1: ruleClassType : ( ( rule__ClassType__ClassrefAssignment ) ) ;
    public final void ruleClassType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:158:2: ( ( ( rule__ClassType__ClassrefAssignment ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:159:1: ( ( rule__ClassType__ClassrefAssignment ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:159:1: ( ( rule__ClassType__ClassrefAssignment ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:160:1: ( rule__ClassType__ClassrefAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassTypeAccess().getClassrefAssignment()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:161:1: ( rule__ClassType__ClassrefAssignment )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:161:2: rule__ClassType__ClassrefAssignment
            {
            pushFollow(FollowSets000.FOLLOW_rule__ClassType__ClassrefAssignment_in_ruleClassType280);
            rule__ClassType__ClassrefAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassTypeAccess().getClassrefAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleClassType"


    // $ANTLR start "entryRuleClass"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:175:1: entryRuleClass : ruleClass EOF ;
    public final void entryRuleClass() throws RecognitionException {
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:176:1: ( ruleClass EOF )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:177:1: ruleClass EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleClass_in_entryRuleClass309);
            ruleClass();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleClass316); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleClass"


    // $ANTLR start "ruleClass"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:184:1: ruleClass : ( ( rule__Class__Group__0 ) ) ;
    public final void ruleClass() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:188:2: ( ( ( rule__Class__Group__0 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:189:1: ( ( rule__Class__Group__0 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:189:1: ( ( rule__Class__Group__0 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:190:1: ( rule__Class__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassAccess().getGroup()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:191:1: ( rule__Class__Group__0 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:191:2: rule__Class__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group__0_in_ruleClass342);
            rule__Class__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleClass"


    // $ANTLR start "entryRuleField"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:203:1: entryRuleField : ruleField EOF ;
    public final void entryRuleField() throws RecognitionException {
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:204:1: ( ruleField EOF )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:205:1: ruleField EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFieldRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleField_in_entryRuleField369);
            ruleField();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFieldRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleField376); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleField"


    // $ANTLR start "ruleField"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:212:1: ruleField : ( ( rule__Field__Group__0 ) ) ;
    public final void ruleField() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:216:2: ( ( ( rule__Field__Group__0 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:217:1: ( ( rule__Field__Group__0 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:217:1: ( ( rule__Field__Group__0 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:218:1: ( rule__Field__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFieldAccess().getGroup()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:219:1: ( rule__Field__Group__0 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:219:2: rule__Field__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Field__Group__0_in_ruleField402);
            rule__Field__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFieldAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleField"


    // $ANTLR start "entryRuleParameter"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:231:1: entryRuleParameter : ruleParameter EOF ;
    public final void entryRuleParameter() throws RecognitionException {
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:232:1: ( ruleParameter EOF )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:233:1: ruleParameter EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParameterRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleParameter_in_entryRuleParameter429);
            ruleParameter();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getParameterRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleParameter436); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleParameter"


    // $ANTLR start "ruleParameter"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:240:1: ruleParameter : ( ( rule__Parameter__Group__0 ) ) ;
    public final void ruleParameter() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:244:2: ( ( ( rule__Parameter__Group__0 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:245:1: ( ( rule__Parameter__Group__0 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:245:1: ( ( rule__Parameter__Group__0 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:246:1: ( rule__Parameter__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParameterAccess().getGroup()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:247:1: ( rule__Parameter__Group__0 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:247:2: rule__Parameter__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Parameter__Group__0_in_ruleParameter462);
            rule__Parameter__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getParameterAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleParameter"


    // $ANTLR start "entryRuleMethod"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:259:1: entryRuleMethod : ruleMethod EOF ;
    public final void entryRuleMethod() throws RecognitionException {
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:260:1: ( ruleMethod EOF )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:261:1: ruleMethod EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleMethod_in_entryRuleMethod489);
            ruleMethod();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleMethod496); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMethod"


    // $ANTLR start "ruleMethod"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:268:1: ruleMethod : ( ( rule__Method__Group__0 ) ) ;
    public final void ruleMethod() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:272:2: ( ( ( rule__Method__Group__0 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:273:1: ( ( rule__Method__Group__0 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:273:1: ( ( rule__Method__Group__0 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:274:1: ( rule__Method__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getGroup()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:275:1: ( rule__Method__Group__0 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:275:2: rule__Method__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Method__Group__0_in_ruleMethod522);
            rule__Method__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMethod"


    // $ANTLR start "entryRuleMethodBody"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:287:1: entryRuleMethodBody : ruleMethodBody EOF ;
    public final void entryRuleMethodBody() throws RecognitionException {
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:288:1: ( ruleMethodBody EOF )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:289:1: ruleMethodBody EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodBodyRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleMethodBody_in_entryRuleMethodBody549);
            ruleMethodBody();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodBodyRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleMethodBody556); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMethodBody"


    // $ANTLR start "ruleMethodBody"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:296:1: ruleMethodBody : ( ( rule__MethodBody__Group__0 ) ) ;
    public final void ruleMethodBody() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:300:2: ( ( ( rule__MethodBody__Group__0 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:301:1: ( ( rule__MethodBody__Group__0 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:301:1: ( ( rule__MethodBody__Group__0 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:302:1: ( rule__MethodBody__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodBodyAccess().getGroup()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:303:1: ( rule__MethodBody__Group__0 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:303:2: rule__MethodBody__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__MethodBody__Group__0_in_ruleMethodBody582);
            rule__MethodBody__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodBodyAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMethodBody"


    // $ANTLR start "entryRuleExpression"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:315:1: entryRuleExpression : ruleExpression EOF ;
    public final void entryRuleExpression() throws RecognitionException {
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:316:1: ( ruleExpression EOF )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:317:1: ruleExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleExpression_in_entryRuleExpression609);
            ruleExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleExpression616); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:324:1: ruleExpression : ( ( rule__Expression__Group__0 ) ) ;
    public final void ruleExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:328:2: ( ( ( rule__Expression__Group__0 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:329:1: ( ( rule__Expression__Group__0 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:329:1: ( ( rule__Expression__Group__0 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:330:1: ( rule__Expression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionAccess().getGroup()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:331:1: ( rule__Expression__Group__0 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:331:2: rule__Expression__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Expression__Group__0_in_ruleExpression642);
            rule__Expression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleMessage"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:343:1: entryRuleMessage : ruleMessage EOF ;
    public final void entryRuleMessage() throws RecognitionException {
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:344:1: ( ruleMessage EOF )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:345:1: ruleMessage EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMessageRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleMessage_in_entryRuleMessage669);
            ruleMessage();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMessageRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleMessage676); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMessage"


    // $ANTLR start "ruleMessage"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:352:1: ruleMessage : ( ( rule__Message__Alternatives ) ) ;
    public final void ruleMessage() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:356:2: ( ( ( rule__Message__Alternatives ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:357:1: ( ( rule__Message__Alternatives ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:357:1: ( ( rule__Message__Alternatives ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:358:1: ( rule__Message__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMessageAccess().getAlternatives()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:359:1: ( rule__Message__Alternatives )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:359:2: rule__Message__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__Message__Alternatives_in_ruleMessage702);
            rule__Message__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMessageAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMessage"


    // $ANTLR start "entryRuleMethodCall"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:371:1: entryRuleMethodCall : ruleMethodCall EOF ;
    public final void entryRuleMethodCall() throws RecognitionException {
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:372:1: ( ruleMethodCall EOF )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:373:1: ruleMethodCall EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodCallRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleMethodCall_in_entryRuleMethodCall729);
            ruleMethodCall();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodCallRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleMethodCall736); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMethodCall"


    // $ANTLR start "ruleMethodCall"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:380:1: ruleMethodCall : ( ( rule__MethodCall__Group__0 ) ) ;
    public final void ruleMethodCall() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:384:2: ( ( ( rule__MethodCall__Group__0 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:385:1: ( ( rule__MethodCall__Group__0 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:385:1: ( ( rule__MethodCall__Group__0 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:386:1: ( rule__MethodCall__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodCallAccess().getGroup()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:387:1: ( rule__MethodCall__Group__0 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:387:2: rule__MethodCall__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__MethodCall__Group__0_in_ruleMethodCall762);
            rule__MethodCall__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodCallAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMethodCall"


    // $ANTLR start "entryRuleFieldSelection"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:399:1: entryRuleFieldSelection : ruleFieldSelection EOF ;
    public final void entryRuleFieldSelection() throws RecognitionException {
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:400:1: ( ruleFieldSelection EOF )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:401:1: ruleFieldSelection EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFieldSelectionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleFieldSelection_in_entryRuleFieldSelection789);
            ruleFieldSelection();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFieldSelectionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFieldSelection796); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFieldSelection"


    // $ANTLR start "ruleFieldSelection"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:408:1: ruleFieldSelection : ( ( rule__FieldSelection__NameAssignment ) ) ;
    public final void ruleFieldSelection() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:412:2: ( ( ( rule__FieldSelection__NameAssignment ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:413:1: ( ( rule__FieldSelection__NameAssignment ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:413:1: ( ( rule__FieldSelection__NameAssignment ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:414:1: ( rule__FieldSelection__NameAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFieldSelectionAccess().getNameAssignment()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:415:1: ( rule__FieldSelection__NameAssignment )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:415:2: rule__FieldSelection__NameAssignment
            {
            pushFollow(FollowSets000.FOLLOW_rule__FieldSelection__NameAssignment_in_ruleFieldSelection822);
            rule__FieldSelection__NameAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFieldSelectionAccess().getNameAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFieldSelection"


    // $ANTLR start "entryRuleTerminalExpression"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:427:1: entryRuleTerminalExpression : ruleTerminalExpression EOF ;
    public final void entryRuleTerminalExpression() throws RecognitionException {
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:428:1: ( ruleTerminalExpression EOF )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:429:1: ruleTerminalExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTerminalExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleTerminalExpression_in_entryRuleTerminalExpression849);
            ruleTerminalExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTerminalExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleTerminalExpression856); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTerminalExpression"


    // $ANTLR start "ruleTerminalExpression"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:436:1: ruleTerminalExpression : ( ( rule__TerminalExpression__Alternatives ) ) ;
    public final void ruleTerminalExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:440:2: ( ( ( rule__TerminalExpression__Alternatives ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:441:1: ( ( rule__TerminalExpression__Alternatives ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:441:1: ( ( rule__TerminalExpression__Alternatives ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:442:1: ( rule__TerminalExpression__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTerminalExpressionAccess().getAlternatives()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:443:1: ( rule__TerminalExpression__Alternatives )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:443:2: rule__TerminalExpression__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__TerminalExpression__Alternatives_in_ruleTerminalExpression882);
            rule__TerminalExpression__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTerminalExpressionAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTerminalExpression"


    // $ANTLR start "entryRuleThis"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:455:1: entryRuleThis : ruleThis EOF ;
    public final void entryRuleThis() throws RecognitionException {
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:456:1: ( ruleThis EOF )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:457:1: ruleThis EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getThisRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleThis_in_entryRuleThis909);
            ruleThis();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getThisRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleThis916); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleThis"


    // $ANTLR start "ruleThis"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:464:1: ruleThis : ( ( rule__This__VariableAssignment ) ) ;
    public final void ruleThis() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:468:2: ( ( ( rule__This__VariableAssignment ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:469:1: ( ( rule__This__VariableAssignment ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:469:1: ( ( rule__This__VariableAssignment ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:470:1: ( rule__This__VariableAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getThisAccess().getVariableAssignment()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:471:1: ( rule__This__VariableAssignment )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:471:2: rule__This__VariableAssignment
            {
            pushFollow(FollowSets000.FOLLOW_rule__This__VariableAssignment_in_ruleThis942);
            rule__This__VariableAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getThisAccess().getVariableAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleThis"


    // $ANTLR start "entryRuleVariable"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:483:1: entryRuleVariable : ruleVariable EOF ;
    public final void entryRuleVariable() throws RecognitionException {
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:484:1: ( ruleVariable EOF )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:485:1: ruleVariable EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleVariable_in_entryRuleVariable969);
            ruleVariable();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleVariable976); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVariable"


    // $ANTLR start "ruleVariable"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:492:1: ruleVariable : ( ( rule__Variable__ParamrefAssignment ) ) ;
    public final void ruleVariable() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:496:2: ( ( ( rule__Variable__ParamrefAssignment ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:497:1: ( ( rule__Variable__ParamrefAssignment ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:497:1: ( ( rule__Variable__ParamrefAssignment ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:498:1: ( rule__Variable__ParamrefAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAccess().getParamrefAssignment()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:499:1: ( rule__Variable__ParamrefAssignment )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:499:2: rule__Variable__ParamrefAssignment
            {
            pushFollow(FollowSets000.FOLLOW_rule__Variable__ParamrefAssignment_in_ruleVariable1002);
            rule__Variable__ParamrefAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAccess().getParamrefAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVariable"


    // $ANTLR start "entryRuleNew"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:511:1: entryRuleNew : ruleNew EOF ;
    public final void entryRuleNew() throws RecognitionException {
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:512:1: ( ruleNew EOF )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:513:1: ruleNew EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNewRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleNew_in_entryRuleNew1029);
            ruleNew();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNewRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleNew1036); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNew"


    // $ANTLR start "ruleNew"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:520:1: ruleNew : ( ( rule__New__Group__0 ) ) ;
    public final void ruleNew() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:524:2: ( ( ( rule__New__Group__0 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:525:1: ( ( rule__New__Group__0 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:525:1: ( ( rule__New__Group__0 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:526:1: ( rule__New__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNewAccess().getGroup()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:527:1: ( rule__New__Group__0 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:527:2: rule__New__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__New__Group__0_in_ruleNew1062);
            rule__New__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNewAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNew"


    // $ANTLR start "entryRuleCast"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:539:1: entryRuleCast : ruleCast EOF ;
    public final void entryRuleCast() throws RecognitionException {
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:540:1: ( ruleCast EOF )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:541:1: ruleCast EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCastRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleCast_in_entryRuleCast1089);
            ruleCast();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCastRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleCast1096); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCast"


    // $ANTLR start "ruleCast"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:548:1: ruleCast : ( ( rule__Cast__Group__0 ) ) ;
    public final void ruleCast() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:552:2: ( ( ( rule__Cast__Group__0 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:553:1: ( ( rule__Cast__Group__0 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:553:1: ( ( rule__Cast__Group__0 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:554:1: ( rule__Cast__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCastAccess().getGroup()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:555:1: ( rule__Cast__Group__0 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:555:2: rule__Cast__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Cast__Group__0_in_ruleCast1122);
            rule__Cast__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCastAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCast"


    // $ANTLR start "entryRuleParen"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:567:1: entryRuleParen : ruleParen EOF ;
    public final void entryRuleParen() throws RecognitionException {
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:568:1: ( ruleParen EOF )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:569:1: ruleParen EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParenRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleParen_in_entryRuleParen1149);
            ruleParen();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getParenRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleParen1156); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleParen"


    // $ANTLR start "ruleParen"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:576:1: ruleParen : ( ( rule__Paren__Group__0 ) ) ;
    public final void ruleParen() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:580:2: ( ( ( rule__Paren__Group__0 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:581:1: ( ( rule__Paren__Group__0 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:581:1: ( ( rule__Paren__Group__0 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:582:1: ( rule__Paren__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParenAccess().getGroup()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:583:1: ( rule__Paren__Group__0 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:583:2: rule__Paren__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Paren__Group__0_in_ruleParen1182);
            rule__Paren__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getParenAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleParen"


    // $ANTLR start "entryRuleConstant"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:595:1: entryRuleConstant : ruleConstant EOF ;
    public final void entryRuleConstant() throws RecognitionException {
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:596:1: ( ruleConstant EOF )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:597:1: ruleConstant EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstantRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleConstant_in_entryRuleConstant1209);
            ruleConstant();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstantRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleConstant1216); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConstant"


    // $ANTLR start "ruleConstant"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:604:1: ruleConstant : ( ( rule__Constant__Alternatives ) ) ;
    public final void ruleConstant() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:608:2: ( ( ( rule__Constant__Alternatives ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:609:1: ( ( rule__Constant__Alternatives ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:609:1: ( ( rule__Constant__Alternatives ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:610:1: ( rule__Constant__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstantAccess().getAlternatives()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:611:1: ( rule__Constant__Alternatives )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:611:2: rule__Constant__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__Constant__Alternatives_in_ruleConstant1242);
            rule__Constant__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstantAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConstant"


    // $ANTLR start "entryRuleStringConstant"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:623:1: entryRuleStringConstant : ruleStringConstant EOF ;
    public final void entryRuleStringConstant() throws RecognitionException {
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:624:1: ( ruleStringConstant EOF )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:625:1: ruleStringConstant EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStringConstantRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleStringConstant_in_entryRuleStringConstant1269);
            ruleStringConstant();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getStringConstantRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleStringConstant1276); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStringConstant"


    // $ANTLR start "ruleStringConstant"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:632:1: ruleStringConstant : ( ( rule__StringConstant__ConstantAssignment ) ) ;
    public final void ruleStringConstant() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:636:2: ( ( ( rule__StringConstant__ConstantAssignment ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:637:1: ( ( rule__StringConstant__ConstantAssignment ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:637:1: ( ( rule__StringConstant__ConstantAssignment ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:638:1: ( rule__StringConstant__ConstantAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStringConstantAccess().getConstantAssignment()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:639:1: ( rule__StringConstant__ConstantAssignment )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:639:2: rule__StringConstant__ConstantAssignment
            {
            pushFollow(FollowSets000.FOLLOW_rule__StringConstant__ConstantAssignment_in_ruleStringConstant1302);
            rule__StringConstant__ConstantAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getStringConstantAccess().getConstantAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStringConstant"


    // $ANTLR start "entryRuleIntConstant"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:651:1: entryRuleIntConstant : ruleIntConstant EOF ;
    public final void entryRuleIntConstant() throws RecognitionException {
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:652:1: ( ruleIntConstant EOF )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:653:1: ruleIntConstant EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIntConstantRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleIntConstant_in_entryRuleIntConstant1329);
            ruleIntConstant();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getIntConstantRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleIntConstant1336); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIntConstant"


    // $ANTLR start "ruleIntConstant"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:660:1: ruleIntConstant : ( ( rule__IntConstant__ConstantAssignment ) ) ;
    public final void ruleIntConstant() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:664:2: ( ( ( rule__IntConstant__ConstantAssignment ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:665:1: ( ( rule__IntConstant__ConstantAssignment ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:665:1: ( ( rule__IntConstant__ConstantAssignment ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:666:1: ( rule__IntConstant__ConstantAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIntConstantAccess().getConstantAssignment()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:667:1: ( rule__IntConstant__ConstantAssignment )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:667:2: rule__IntConstant__ConstantAssignment
            {
            pushFollow(FollowSets000.FOLLOW_rule__IntConstant__ConstantAssignment_in_ruleIntConstant1362);
            rule__IntConstant__ConstantAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getIntConstantAccess().getConstantAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIntConstant"


    // $ANTLR start "entryRuleBoolConstant"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:679:1: entryRuleBoolConstant : ruleBoolConstant EOF ;
    public final void entryRuleBoolConstant() throws RecognitionException {
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:680:1: ( ruleBoolConstant EOF )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:681:1: ruleBoolConstant EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBoolConstantRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleBoolConstant_in_entryRuleBoolConstant1389);
            ruleBoolConstant();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBoolConstantRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleBoolConstant1396); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBoolConstant"


    // $ANTLR start "ruleBoolConstant"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:688:1: ruleBoolConstant : ( ( rule__BoolConstant__ConstantAssignment ) ) ;
    public final void ruleBoolConstant() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:692:2: ( ( ( rule__BoolConstant__ConstantAssignment ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:693:1: ( ( rule__BoolConstant__ConstantAssignment ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:693:1: ( ( rule__BoolConstant__ConstantAssignment ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:694:1: ( rule__BoolConstant__ConstantAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBoolConstantAccess().getConstantAssignment()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:695:1: ( rule__BoolConstant__ConstantAssignment )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:695:2: rule__BoolConstant__ConstantAssignment
            {
            pushFollow(FollowSets000.FOLLOW_rule__BoolConstant__ConstantAssignment_in_ruleBoolConstant1422);
            rule__BoolConstant__ConstantAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBoolConstantAccess().getConstantAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBoolConstant"


    // $ANTLR start "entryRuleArgument"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:707:1: entryRuleArgument : ruleArgument EOF ;
    public final void entryRuleArgument() throws RecognitionException {
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:708:1: ( ruleArgument EOF )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:709:1: ruleArgument EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getArgumentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleArgument_in_entryRuleArgument1449);
            ruleArgument();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getArgumentRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleArgument1456); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleArgument"


    // $ANTLR start "ruleArgument"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:716:1: ruleArgument : ( ruleExpression ) ;
    public final void ruleArgument() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:720:2: ( ( ruleExpression ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:721:1: ( ruleExpression )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:721:1: ( ruleExpression )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:722:1: ruleExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getArgumentAccess().getExpressionParserRuleCall()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleExpression_in_ruleArgument1482);
            ruleExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getArgumentAccess().getExpressionParserRuleCall()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleArgument"


    // $ANTLR start "rule__Type__Alternatives"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:735:1: rule__Type__Alternatives : ( ( ruleBasicType ) | ( ruleClassType ) );
    public final void rule__Type__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:739:1: ( ( ruleBasicType ) | ( ruleClassType ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( ((LA1_0>=11 && LA1_0<=13)) ) {
                alt1=1;
            }
            else if ( (LA1_0==RULE_ID) ) {
                alt1=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:740:1: ( ruleBasicType )
                    {
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:740:1: ( ruleBasicType )
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:741:1: ruleBasicType
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTypeAccess().getBasicTypeParserRuleCall_0()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleBasicType_in_rule__Type__Alternatives1517);
                    ruleBasicType();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTypeAccess().getBasicTypeParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:746:6: ( ruleClassType )
                    {
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:746:6: ( ruleClassType )
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:747:1: ruleClassType
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTypeAccess().getClassTypeParserRuleCall_1()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleClassType_in_rule__Type__Alternatives1534);
                    ruleClassType();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTypeAccess().getClassTypeParserRuleCall_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Alternatives"


    // $ANTLR start "rule__BasicType__BasicAlternatives_0"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:757:1: rule__BasicType__BasicAlternatives_0 : ( ( 'int' ) | ( 'boolean' ) | ( 'String' ) );
    public final void rule__BasicType__BasicAlternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:761:1: ( ( 'int' ) | ( 'boolean' ) | ( 'String' ) )
            int alt2=3;
            switch ( input.LA(1) ) {
            case 11:
                {
                alt2=1;
                }
                break;
            case 12:
                {
                alt2=2;
                }
                break;
            case 13:
                {
                alt2=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:762:1: ( 'int' )
                    {
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:762:1: ( 'int' )
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:763:1: 'int'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBasicTypeAccess().getBasicIntKeyword_0_0()); 
                    }
                    match(input,11,FollowSets000.FOLLOW_11_in_rule__BasicType__BasicAlternatives_01567); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBasicTypeAccess().getBasicIntKeyword_0_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:770:6: ( 'boolean' )
                    {
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:770:6: ( 'boolean' )
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:771:1: 'boolean'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBasicTypeAccess().getBasicBooleanKeyword_0_1()); 
                    }
                    match(input,12,FollowSets000.FOLLOW_12_in_rule__BasicType__BasicAlternatives_01587); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBasicTypeAccess().getBasicBooleanKeyword_0_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:778:6: ( 'String' )
                    {
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:778:6: ( 'String' )
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:779:1: 'String'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBasicTypeAccess().getBasicStringKeyword_0_2()); 
                    }
                    match(input,13,FollowSets000.FOLLOW_13_in_rule__BasicType__BasicAlternatives_01607); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBasicTypeAccess().getBasicStringKeyword_0_2()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicType__BasicAlternatives_0"


    // $ANTLR start "rule__Message__Alternatives"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:792:1: rule__Message__Alternatives : ( ( ruleMethodCall ) | ( ruleFieldSelection ) );
    public final void rule__Message__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:796:1: ( ( ruleMethodCall ) | ( ruleFieldSelection ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==RULE_ID) ) {
                int LA3_1 = input.LA(2);

                if ( (LA3_1==21) ) {
                    alt3=1;
                }
                else if ( (LA3_1==EOF||LA3_1==20||(LA3_1>=22 && LA3_1<=23)||LA3_1==25) ) {
                    alt3=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return ;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 3, 1, input);

                    throw nvae;
                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:797:1: ( ruleMethodCall )
                    {
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:797:1: ( ruleMethodCall )
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:798:1: ruleMethodCall
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getMessageAccess().getMethodCallParserRuleCall_0()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleMethodCall_in_rule__Message__Alternatives1642);
                    ruleMethodCall();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getMessageAccess().getMethodCallParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:803:6: ( ruleFieldSelection )
                    {
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:803:6: ( ruleFieldSelection )
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:804:1: ruleFieldSelection
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getMessageAccess().getFieldSelectionParserRuleCall_1()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleFieldSelection_in_rule__Message__Alternatives1659);
                    ruleFieldSelection();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getMessageAccess().getFieldSelectionParserRuleCall_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Message__Alternatives"


    // $ANTLR start "rule__TerminalExpression__Alternatives"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:814:1: rule__TerminalExpression__Alternatives : ( ( ruleThis ) | ( ruleVariable ) | ( ruleNew ) | ( ( ruleCast ) ) | ( ruleConstant ) | ( ruleParen ) );
    public final void rule__TerminalExpression__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:818:1: ( ( ruleThis ) | ( ruleVariable ) | ( ruleNew ) | ( ( ruleCast ) ) | ( ruleConstant ) | ( ruleParen ) )
            int alt4=6;
            alt4 = dfa4.predict(input);
            switch (alt4) {
                case 1 :
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:819:1: ( ruleThis )
                    {
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:819:1: ( ruleThis )
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:820:1: ruleThis
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTerminalExpressionAccess().getThisParserRuleCall_0()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleThis_in_rule__TerminalExpression__Alternatives1691);
                    ruleThis();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTerminalExpressionAccess().getThisParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:825:6: ( ruleVariable )
                    {
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:825:6: ( ruleVariable )
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:826:1: ruleVariable
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTerminalExpressionAccess().getVariableParserRuleCall_1()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleVariable_in_rule__TerminalExpression__Alternatives1708);
                    ruleVariable();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTerminalExpressionAccess().getVariableParserRuleCall_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:831:6: ( ruleNew )
                    {
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:831:6: ( ruleNew )
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:832:1: ruleNew
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTerminalExpressionAccess().getNewParserRuleCall_2()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleNew_in_rule__TerminalExpression__Alternatives1725);
                    ruleNew();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTerminalExpressionAccess().getNewParserRuleCall_2()); 
                    }

                    }


                    }
                    break;
                case 4 :
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:837:6: ( ( ruleCast ) )
                    {
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:837:6: ( ( ruleCast ) )
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:838:1: ( ruleCast )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTerminalExpressionAccess().getCastParserRuleCall_3()); 
                    }
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:839:1: ( ruleCast )
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:839:3: ruleCast
                    {
                    pushFollow(FollowSets000.FOLLOW_ruleCast_in_rule__TerminalExpression__Alternatives1743);
                    ruleCast();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTerminalExpressionAccess().getCastParserRuleCall_3()); 
                    }

                    }


                    }
                    break;
                case 5 :
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:843:6: ( ruleConstant )
                    {
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:843:6: ( ruleConstant )
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:844:1: ruleConstant
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTerminalExpressionAccess().getConstantParserRuleCall_4()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleConstant_in_rule__TerminalExpression__Alternatives1761);
                    ruleConstant();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTerminalExpressionAccess().getConstantParserRuleCall_4()); 
                    }

                    }


                    }
                    break;
                case 6 :
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:849:6: ( ruleParen )
                    {
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:849:6: ( ruleParen )
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:850:1: ruleParen
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTerminalExpressionAccess().getParenParserRuleCall_5()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleParen_in_rule__TerminalExpression__Alternatives1778);
                    ruleParen();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTerminalExpressionAccess().getParenParserRuleCall_5()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TerminalExpression__Alternatives"


    // $ANTLR start "rule__Constant__Alternatives"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:860:1: rule__Constant__Alternatives : ( ( ruleIntConstant ) | ( ruleBoolConstant ) | ( ruleStringConstant ) );
    public final void rule__Constant__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:864:1: ( ( ruleIntConstant ) | ( ruleBoolConstant ) | ( ruleStringConstant ) )
            int alt5=3;
            switch ( input.LA(1) ) {
            case RULE_INT:
                {
                alt5=1;
                }
                break;
            case 14:
            case 15:
                {
                alt5=2;
                }
                break;
            case RULE_STRING:
                {
                alt5=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:865:1: ( ruleIntConstant )
                    {
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:865:1: ( ruleIntConstant )
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:866:1: ruleIntConstant
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getConstantAccess().getIntConstantParserRuleCall_0()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleIntConstant_in_rule__Constant__Alternatives1810);
                    ruleIntConstant();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getConstantAccess().getIntConstantParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:871:6: ( ruleBoolConstant )
                    {
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:871:6: ( ruleBoolConstant )
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:872:1: ruleBoolConstant
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getConstantAccess().getBoolConstantParserRuleCall_1()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleBoolConstant_in_rule__Constant__Alternatives1827);
                    ruleBoolConstant();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getConstantAccess().getBoolConstantParserRuleCall_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:877:6: ( ruleStringConstant )
                    {
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:877:6: ( ruleStringConstant )
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:878:1: ruleStringConstant
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getConstantAccess().getStringConstantParserRuleCall_2()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_ruleStringConstant_in_rule__Constant__Alternatives1844);
                    ruleStringConstant();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getConstantAccess().getStringConstantParserRuleCall_2()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Alternatives"


    // $ANTLR start "rule__BoolConstant__ConstantAlternatives_0"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:888:1: rule__BoolConstant__ConstantAlternatives_0 : ( ( 'true' ) | ( 'false' ) );
    public final void rule__BoolConstant__ConstantAlternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:892:1: ( ( 'true' ) | ( 'false' ) )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==14) ) {
                alt6=1;
            }
            else if ( (LA6_0==15) ) {
                alt6=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:893:1: ( 'true' )
                    {
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:893:1: ( 'true' )
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:894:1: 'true'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBoolConstantAccess().getConstantTrueKeyword_0_0()); 
                    }
                    match(input,14,FollowSets000.FOLLOW_14_in_rule__BoolConstant__ConstantAlternatives_01877); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBoolConstantAccess().getConstantTrueKeyword_0_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:901:6: ( 'false' )
                    {
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:901:6: ( 'false' )
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:902:1: 'false'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBoolConstantAccess().getConstantFalseKeyword_0_1()); 
                    }
                    match(input,15,FollowSets000.FOLLOW_15_in_rule__BoolConstant__ConstantAlternatives_01897); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBoolConstantAccess().getConstantFalseKeyword_0_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BoolConstant__ConstantAlternatives_0"


    // $ANTLR start "rule__Program__Group__0"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:916:1: rule__Program__Group__0 : rule__Program__Group__0__Impl rule__Program__Group__1 ;
    public final void rule__Program__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:920:1: ( rule__Program__Group__0__Impl rule__Program__Group__1 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:921:2: rule__Program__Group__0__Impl rule__Program__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Program__Group__0__Impl_in_rule__Program__Group__01929);
            rule__Program__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Program__Group__1_in_rule__Program__Group__01932);
            rule__Program__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Program__Group__0"


    // $ANTLR start "rule__Program__Group__0__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:928:1: rule__Program__Group__0__Impl : ( ( rule__Program__ClassesAssignment_0 )* ) ;
    public final void rule__Program__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:932:1: ( ( ( rule__Program__ClassesAssignment_0 )* ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:933:1: ( ( rule__Program__ClassesAssignment_0 )* )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:933:1: ( ( rule__Program__ClassesAssignment_0 )* )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:934:1: ( rule__Program__ClassesAssignment_0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getProgramAccess().getClassesAssignment_0()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:935:1: ( rule__Program__ClassesAssignment_0 )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==16) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:935:2: rule__Program__ClassesAssignment_0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Program__ClassesAssignment_0_in_rule__Program__Group__0__Impl1959);
            	    rule__Program__ClassesAssignment_0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getProgramAccess().getClassesAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Program__Group__0__Impl"


    // $ANTLR start "rule__Program__Group__1"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:945:1: rule__Program__Group__1 : rule__Program__Group__1__Impl ;
    public final void rule__Program__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:949:1: ( rule__Program__Group__1__Impl )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:950:2: rule__Program__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Program__Group__1__Impl_in_rule__Program__Group__11990);
            rule__Program__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Program__Group__1"


    // $ANTLR start "rule__Program__Group__1__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:956:1: rule__Program__Group__1__Impl : ( ( rule__Program__MainAssignment_1 )? ) ;
    public final void rule__Program__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:960:1: ( ( ( rule__Program__MainAssignment_1 )? ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:961:1: ( ( rule__Program__MainAssignment_1 )? )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:961:1: ( ( rule__Program__MainAssignment_1 )? )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:962:1: ( rule__Program__MainAssignment_1 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getProgramAccess().getMainAssignment_1()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:963:1: ( rule__Program__MainAssignment_1 )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( ((LA8_0>=RULE_ID && LA8_0<=RULE_INT)||(LA8_0>=14 && LA8_0<=15)||LA8_0==21||(LA8_0>=26 && LA8_0<=27)) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:963:2: rule__Program__MainAssignment_1
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Program__MainAssignment_1_in_rule__Program__Group__1__Impl2017);
                    rule__Program__MainAssignment_1();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getProgramAccess().getMainAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Program__Group__1__Impl"


    // $ANTLR start "rule__Class__Group__0"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:977:1: rule__Class__Group__0 : rule__Class__Group__0__Impl rule__Class__Group__1 ;
    public final void rule__Class__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:981:1: ( rule__Class__Group__0__Impl rule__Class__Group__1 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:982:2: rule__Class__Group__0__Impl rule__Class__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group__0__Impl_in_rule__Class__Group__02052);
            rule__Class__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group__1_in_rule__Class__Group__02055);
            rule__Class__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__0"


    // $ANTLR start "rule__Class__Group__0__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:989:1: rule__Class__Group__0__Impl : ( 'class' ) ;
    public final void rule__Class__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:993:1: ( ( 'class' ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:994:1: ( 'class' )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:994:1: ( 'class' )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:995:1: 'class'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassAccess().getClassKeyword_0()); 
            }
            match(input,16,FollowSets000.FOLLOW_16_in_rule__Class__Group__0__Impl2083); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassAccess().getClassKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__0__Impl"


    // $ANTLR start "rule__Class__Group__1"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1008:1: rule__Class__Group__1 : rule__Class__Group__1__Impl rule__Class__Group__2 ;
    public final void rule__Class__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1012:1: ( rule__Class__Group__1__Impl rule__Class__Group__2 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1013:2: rule__Class__Group__1__Impl rule__Class__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group__1__Impl_in_rule__Class__Group__12114);
            rule__Class__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group__2_in_rule__Class__Group__12117);
            rule__Class__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__1"


    // $ANTLR start "rule__Class__Group__1__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1020:1: rule__Class__Group__1__Impl : ( ( rule__Class__NameAssignment_1 ) ) ;
    public final void rule__Class__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1024:1: ( ( ( rule__Class__NameAssignment_1 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1025:1: ( ( rule__Class__NameAssignment_1 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1025:1: ( ( rule__Class__NameAssignment_1 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1026:1: ( rule__Class__NameAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassAccess().getNameAssignment_1()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1027:1: ( rule__Class__NameAssignment_1 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1027:2: rule__Class__NameAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__NameAssignment_1_in_rule__Class__Group__1__Impl2144);
            rule__Class__NameAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassAccess().getNameAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__1__Impl"


    // $ANTLR start "rule__Class__Group__2"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1037:1: rule__Class__Group__2 : rule__Class__Group__2__Impl rule__Class__Group__3 ;
    public final void rule__Class__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1041:1: ( rule__Class__Group__2__Impl rule__Class__Group__3 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1042:2: rule__Class__Group__2__Impl rule__Class__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group__2__Impl_in_rule__Class__Group__22174);
            rule__Class__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group__3_in_rule__Class__Group__22177);
            rule__Class__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__2"


    // $ANTLR start "rule__Class__Group__2__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1049:1: rule__Class__Group__2__Impl : ( ( rule__Class__Group_2__0 )? ) ;
    public final void rule__Class__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1053:1: ( ( ( rule__Class__Group_2__0 )? ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1054:1: ( ( rule__Class__Group_2__0 )? )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1054:1: ( ( rule__Class__Group_2__0 )? )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1055:1: ( rule__Class__Group_2__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassAccess().getGroup_2()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1056:1: ( rule__Class__Group_2__0 )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==19) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1056:2: rule__Class__Group_2__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Class__Group_2__0_in_rule__Class__Group__2__Impl2204);
                    rule__Class__Group_2__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassAccess().getGroup_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__2__Impl"


    // $ANTLR start "rule__Class__Group__3"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1066:1: rule__Class__Group__3 : rule__Class__Group__3__Impl rule__Class__Group__4 ;
    public final void rule__Class__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1070:1: ( rule__Class__Group__3__Impl rule__Class__Group__4 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1071:2: rule__Class__Group__3__Impl rule__Class__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group__3__Impl_in_rule__Class__Group__32235);
            rule__Class__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group__4_in_rule__Class__Group__32238);
            rule__Class__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__3"


    // $ANTLR start "rule__Class__Group__3__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1078:1: rule__Class__Group__3__Impl : ( '{' ) ;
    public final void rule__Class__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1082:1: ( ( '{' ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1083:1: ( '{' )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1083:1: ( '{' )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1084:1: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassAccess().getLeftCurlyBracketKeyword_3()); 
            }
            match(input,17,FollowSets000.FOLLOW_17_in_rule__Class__Group__3__Impl2266); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassAccess().getLeftCurlyBracketKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__3__Impl"


    // $ANTLR start "rule__Class__Group__4"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1097:1: rule__Class__Group__4 : rule__Class__Group__4__Impl rule__Class__Group__5 ;
    public final void rule__Class__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1101:1: ( rule__Class__Group__4__Impl rule__Class__Group__5 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1102:2: rule__Class__Group__4__Impl rule__Class__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group__4__Impl_in_rule__Class__Group__42297);
            rule__Class__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group__5_in_rule__Class__Group__42300);
            rule__Class__Group__5();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__4"


    // $ANTLR start "rule__Class__Group__4__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1109:1: rule__Class__Group__4__Impl : ( ( rule__Class__FieldsAssignment_4 )* ) ;
    public final void rule__Class__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1113:1: ( ( ( rule__Class__FieldsAssignment_4 )* ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1114:1: ( ( rule__Class__FieldsAssignment_4 )* )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1114:1: ( ( rule__Class__FieldsAssignment_4 )* )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1115:1: ( rule__Class__FieldsAssignment_4 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassAccess().getFieldsAssignment_4()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1116:1: ( rule__Class__FieldsAssignment_4 )*
            loop10:
            do {
                int alt10=2;
                switch ( input.LA(1) ) {
                case 11:
                    {
                    int LA10_1 = input.LA(2);

                    if ( (LA10_1==RULE_ID) ) {
                        int LA10_6 = input.LA(3);

                        if ( (LA10_6==20) ) {
                            alt10=1;
                        }


                    }


                    }
                    break;
                case 12:
                    {
                    int LA10_2 = input.LA(2);

                    if ( (LA10_2==RULE_ID) ) {
                        int LA10_6 = input.LA(3);

                        if ( (LA10_6==20) ) {
                            alt10=1;
                        }


                    }


                    }
                    break;
                case 13:
                    {
                    int LA10_3 = input.LA(2);

                    if ( (LA10_3==RULE_ID) ) {
                        int LA10_6 = input.LA(3);

                        if ( (LA10_6==20) ) {
                            alt10=1;
                        }


                    }


                    }
                    break;
                case RULE_ID:
                    {
                    int LA10_4 = input.LA(2);

                    if ( (LA10_4==RULE_ID) ) {
                        int LA10_6 = input.LA(3);

                        if ( (LA10_6==20) ) {
                            alt10=1;
                        }


                    }


                    }
                    break;

                }

                switch (alt10) {
            	case 1 :
            	    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1116:2: rule__Class__FieldsAssignment_4
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Class__FieldsAssignment_4_in_rule__Class__Group__4__Impl2327);
            	    rule__Class__FieldsAssignment_4();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassAccess().getFieldsAssignment_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__4__Impl"


    // $ANTLR start "rule__Class__Group__5"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1126:1: rule__Class__Group__5 : rule__Class__Group__5__Impl rule__Class__Group__6 ;
    public final void rule__Class__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1130:1: ( rule__Class__Group__5__Impl rule__Class__Group__6 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1131:2: rule__Class__Group__5__Impl rule__Class__Group__6
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group__5__Impl_in_rule__Class__Group__52358);
            rule__Class__Group__5__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group__6_in_rule__Class__Group__52361);
            rule__Class__Group__6();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__5"


    // $ANTLR start "rule__Class__Group__5__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1138:1: rule__Class__Group__5__Impl : ( ( rule__Class__MethodsAssignment_5 )* ) ;
    public final void rule__Class__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1142:1: ( ( ( rule__Class__MethodsAssignment_5 )* ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1143:1: ( ( rule__Class__MethodsAssignment_5 )* )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1143:1: ( ( rule__Class__MethodsAssignment_5 )* )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1144:1: ( rule__Class__MethodsAssignment_5 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassAccess().getMethodsAssignment_5()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1145:1: ( rule__Class__MethodsAssignment_5 )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==RULE_ID||(LA11_0>=11 && LA11_0<=13)) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1145:2: rule__Class__MethodsAssignment_5
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Class__MethodsAssignment_5_in_rule__Class__Group__5__Impl2388);
            	    rule__Class__MethodsAssignment_5();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassAccess().getMethodsAssignment_5()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__5__Impl"


    // $ANTLR start "rule__Class__Group__6"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1155:1: rule__Class__Group__6 : rule__Class__Group__6__Impl ;
    public final void rule__Class__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1159:1: ( rule__Class__Group__6__Impl )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1160:2: rule__Class__Group__6__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group__6__Impl_in_rule__Class__Group__62419);
            rule__Class__Group__6__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__6"


    // $ANTLR start "rule__Class__Group__6__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1166:1: rule__Class__Group__6__Impl : ( '}' ) ;
    public final void rule__Class__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1170:1: ( ( '}' ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1171:1: ( '}' )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1171:1: ( '}' )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1172:1: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassAccess().getRightCurlyBracketKeyword_6()); 
            }
            match(input,18,FollowSets000.FOLLOW_18_in_rule__Class__Group__6__Impl2447); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassAccess().getRightCurlyBracketKeyword_6()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__6__Impl"


    // $ANTLR start "rule__Class__Group_2__0"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1199:1: rule__Class__Group_2__0 : rule__Class__Group_2__0__Impl rule__Class__Group_2__1 ;
    public final void rule__Class__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1203:1: ( rule__Class__Group_2__0__Impl rule__Class__Group_2__1 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1204:2: rule__Class__Group_2__0__Impl rule__Class__Group_2__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group_2__0__Impl_in_rule__Class__Group_2__02492);
            rule__Class__Group_2__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group_2__1_in_rule__Class__Group_2__02495);
            rule__Class__Group_2__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_2__0"


    // $ANTLR start "rule__Class__Group_2__0__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1211:1: rule__Class__Group_2__0__Impl : ( 'extends' ) ;
    public final void rule__Class__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1215:1: ( ( 'extends' ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1216:1: ( 'extends' )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1216:1: ( 'extends' )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1217:1: 'extends'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassAccess().getExtendsKeyword_2_0()); 
            }
            match(input,19,FollowSets000.FOLLOW_19_in_rule__Class__Group_2__0__Impl2523); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassAccess().getExtendsKeyword_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_2__0__Impl"


    // $ANTLR start "rule__Class__Group_2__1"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1230:1: rule__Class__Group_2__1 : rule__Class__Group_2__1__Impl ;
    public final void rule__Class__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1234:1: ( rule__Class__Group_2__1__Impl )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1235:2: rule__Class__Group_2__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__Group_2__1__Impl_in_rule__Class__Group_2__12554);
            rule__Class__Group_2__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_2__1"


    // $ANTLR start "rule__Class__Group_2__1__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1241:1: rule__Class__Group_2__1__Impl : ( ( rule__Class__ExtendsAssignment_2_1 ) ) ;
    public final void rule__Class__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1245:1: ( ( ( rule__Class__ExtendsAssignment_2_1 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1246:1: ( ( rule__Class__ExtendsAssignment_2_1 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1246:1: ( ( rule__Class__ExtendsAssignment_2_1 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1247:1: ( rule__Class__ExtendsAssignment_2_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassAccess().getExtendsAssignment_2_1()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1248:1: ( rule__Class__ExtendsAssignment_2_1 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1248:2: rule__Class__ExtendsAssignment_2_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Class__ExtendsAssignment_2_1_in_rule__Class__Group_2__1__Impl2581);
            rule__Class__ExtendsAssignment_2_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassAccess().getExtendsAssignment_2_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_2__1__Impl"


    // $ANTLR start "rule__Field__Group__0"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1262:1: rule__Field__Group__0 : rule__Field__Group__0__Impl rule__Field__Group__1 ;
    public final void rule__Field__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1266:1: ( rule__Field__Group__0__Impl rule__Field__Group__1 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1267:2: rule__Field__Group__0__Impl rule__Field__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Field__Group__0__Impl_in_rule__Field__Group__02615);
            rule__Field__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Field__Group__1_in_rule__Field__Group__02618);
            rule__Field__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Field__Group__0"


    // $ANTLR start "rule__Field__Group__0__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1274:1: rule__Field__Group__0__Impl : ( ( rule__Field__TypeAssignment_0 ) ) ;
    public final void rule__Field__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1278:1: ( ( ( rule__Field__TypeAssignment_0 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1279:1: ( ( rule__Field__TypeAssignment_0 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1279:1: ( ( rule__Field__TypeAssignment_0 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1280:1: ( rule__Field__TypeAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFieldAccess().getTypeAssignment_0()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1281:1: ( rule__Field__TypeAssignment_0 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1281:2: rule__Field__TypeAssignment_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Field__TypeAssignment_0_in_rule__Field__Group__0__Impl2645);
            rule__Field__TypeAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFieldAccess().getTypeAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Field__Group__0__Impl"


    // $ANTLR start "rule__Field__Group__1"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1291:1: rule__Field__Group__1 : rule__Field__Group__1__Impl rule__Field__Group__2 ;
    public final void rule__Field__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1295:1: ( rule__Field__Group__1__Impl rule__Field__Group__2 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1296:2: rule__Field__Group__1__Impl rule__Field__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Field__Group__1__Impl_in_rule__Field__Group__12675);
            rule__Field__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Field__Group__2_in_rule__Field__Group__12678);
            rule__Field__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Field__Group__1"


    // $ANTLR start "rule__Field__Group__1__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1303:1: rule__Field__Group__1__Impl : ( ( rule__Field__NameAssignment_1 ) ) ;
    public final void rule__Field__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1307:1: ( ( ( rule__Field__NameAssignment_1 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1308:1: ( ( rule__Field__NameAssignment_1 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1308:1: ( ( rule__Field__NameAssignment_1 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1309:1: ( rule__Field__NameAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFieldAccess().getNameAssignment_1()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1310:1: ( rule__Field__NameAssignment_1 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1310:2: rule__Field__NameAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Field__NameAssignment_1_in_rule__Field__Group__1__Impl2705);
            rule__Field__NameAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFieldAccess().getNameAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Field__Group__1__Impl"


    // $ANTLR start "rule__Field__Group__2"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1320:1: rule__Field__Group__2 : rule__Field__Group__2__Impl ;
    public final void rule__Field__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1324:1: ( rule__Field__Group__2__Impl )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1325:2: rule__Field__Group__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Field__Group__2__Impl_in_rule__Field__Group__22735);
            rule__Field__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Field__Group__2"


    // $ANTLR start "rule__Field__Group__2__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1331:1: rule__Field__Group__2__Impl : ( ';' ) ;
    public final void rule__Field__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1335:1: ( ( ';' ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1336:1: ( ';' )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1336:1: ( ';' )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1337:1: ';'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFieldAccess().getSemicolonKeyword_2()); 
            }
            match(input,20,FollowSets000.FOLLOW_20_in_rule__Field__Group__2__Impl2763); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFieldAccess().getSemicolonKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Field__Group__2__Impl"


    // $ANTLR start "rule__Parameter__Group__0"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1356:1: rule__Parameter__Group__0 : rule__Parameter__Group__0__Impl rule__Parameter__Group__1 ;
    public final void rule__Parameter__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1360:1: ( rule__Parameter__Group__0__Impl rule__Parameter__Group__1 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1361:2: rule__Parameter__Group__0__Impl rule__Parameter__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Parameter__Group__0__Impl_in_rule__Parameter__Group__02800);
            rule__Parameter__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Parameter__Group__1_in_rule__Parameter__Group__02803);
            rule__Parameter__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__0"


    // $ANTLR start "rule__Parameter__Group__0__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1368:1: rule__Parameter__Group__0__Impl : ( ( rule__Parameter__TypeAssignment_0 ) ) ;
    public final void rule__Parameter__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1372:1: ( ( ( rule__Parameter__TypeAssignment_0 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1373:1: ( ( rule__Parameter__TypeAssignment_0 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1373:1: ( ( rule__Parameter__TypeAssignment_0 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1374:1: ( rule__Parameter__TypeAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParameterAccess().getTypeAssignment_0()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1375:1: ( rule__Parameter__TypeAssignment_0 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1375:2: rule__Parameter__TypeAssignment_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Parameter__TypeAssignment_0_in_rule__Parameter__Group__0__Impl2830);
            rule__Parameter__TypeAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getParameterAccess().getTypeAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__0__Impl"


    // $ANTLR start "rule__Parameter__Group__1"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1385:1: rule__Parameter__Group__1 : rule__Parameter__Group__1__Impl ;
    public final void rule__Parameter__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1389:1: ( rule__Parameter__Group__1__Impl )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1390:2: rule__Parameter__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Parameter__Group__1__Impl_in_rule__Parameter__Group__12860);
            rule__Parameter__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__1"


    // $ANTLR start "rule__Parameter__Group__1__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1396:1: rule__Parameter__Group__1__Impl : ( ( rule__Parameter__NameAssignment_1 ) ) ;
    public final void rule__Parameter__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1400:1: ( ( ( rule__Parameter__NameAssignment_1 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1401:1: ( ( rule__Parameter__NameAssignment_1 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1401:1: ( ( rule__Parameter__NameAssignment_1 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1402:1: ( rule__Parameter__NameAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParameterAccess().getNameAssignment_1()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1403:1: ( rule__Parameter__NameAssignment_1 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1403:2: rule__Parameter__NameAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Parameter__NameAssignment_1_in_rule__Parameter__Group__1__Impl2887);
            rule__Parameter__NameAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getParameterAccess().getNameAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__1__Impl"


    // $ANTLR start "rule__Method__Group__0"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1417:1: rule__Method__Group__0 : rule__Method__Group__0__Impl rule__Method__Group__1 ;
    public final void rule__Method__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1421:1: ( rule__Method__Group__0__Impl rule__Method__Group__1 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1422:2: rule__Method__Group__0__Impl rule__Method__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Method__Group__0__Impl_in_rule__Method__Group__02921);
            rule__Method__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Method__Group__1_in_rule__Method__Group__02924);
            rule__Method__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__0"


    // $ANTLR start "rule__Method__Group__0__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1429:1: rule__Method__Group__0__Impl : ( ( rule__Method__ReturntypeAssignment_0 ) ) ;
    public final void rule__Method__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1433:1: ( ( ( rule__Method__ReturntypeAssignment_0 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1434:1: ( ( rule__Method__ReturntypeAssignment_0 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1434:1: ( ( rule__Method__ReturntypeAssignment_0 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1435:1: ( rule__Method__ReturntypeAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getReturntypeAssignment_0()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1436:1: ( rule__Method__ReturntypeAssignment_0 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1436:2: rule__Method__ReturntypeAssignment_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Method__ReturntypeAssignment_0_in_rule__Method__Group__0__Impl2951);
            rule__Method__ReturntypeAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getReturntypeAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__0__Impl"


    // $ANTLR start "rule__Method__Group__1"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1446:1: rule__Method__Group__1 : rule__Method__Group__1__Impl rule__Method__Group__2 ;
    public final void rule__Method__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1450:1: ( rule__Method__Group__1__Impl rule__Method__Group__2 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1451:2: rule__Method__Group__1__Impl rule__Method__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Method__Group__1__Impl_in_rule__Method__Group__12981);
            rule__Method__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Method__Group__2_in_rule__Method__Group__12984);
            rule__Method__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__1"


    // $ANTLR start "rule__Method__Group__1__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1458:1: rule__Method__Group__1__Impl : ( ( rule__Method__NameAssignment_1 ) ) ;
    public final void rule__Method__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1462:1: ( ( ( rule__Method__NameAssignment_1 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1463:1: ( ( rule__Method__NameAssignment_1 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1463:1: ( ( rule__Method__NameAssignment_1 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1464:1: ( rule__Method__NameAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getNameAssignment_1()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1465:1: ( rule__Method__NameAssignment_1 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1465:2: rule__Method__NameAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Method__NameAssignment_1_in_rule__Method__Group__1__Impl3011);
            rule__Method__NameAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getNameAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__1__Impl"


    // $ANTLR start "rule__Method__Group__2"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1475:1: rule__Method__Group__2 : rule__Method__Group__2__Impl rule__Method__Group__3 ;
    public final void rule__Method__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1479:1: ( rule__Method__Group__2__Impl rule__Method__Group__3 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1480:2: rule__Method__Group__2__Impl rule__Method__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Method__Group__2__Impl_in_rule__Method__Group__23041);
            rule__Method__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Method__Group__3_in_rule__Method__Group__23044);
            rule__Method__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__2"


    // $ANTLR start "rule__Method__Group__2__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1487:1: rule__Method__Group__2__Impl : ( '(' ) ;
    public final void rule__Method__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1491:1: ( ( '(' ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1492:1: ( '(' )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1492:1: ( '(' )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1493:1: '('
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getLeftParenthesisKeyword_2()); 
            }
            match(input,21,FollowSets000.FOLLOW_21_in_rule__Method__Group__2__Impl3072); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getLeftParenthesisKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__2__Impl"


    // $ANTLR start "rule__Method__Group__3"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1506:1: rule__Method__Group__3 : rule__Method__Group__3__Impl rule__Method__Group__4 ;
    public final void rule__Method__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1510:1: ( rule__Method__Group__3__Impl rule__Method__Group__4 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1511:2: rule__Method__Group__3__Impl rule__Method__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Method__Group__3__Impl_in_rule__Method__Group__33103);
            rule__Method__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Method__Group__4_in_rule__Method__Group__33106);
            rule__Method__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__3"


    // $ANTLR start "rule__Method__Group__3__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1518:1: rule__Method__Group__3__Impl : ( ( rule__Method__Group_3__0 )? ) ;
    public final void rule__Method__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1522:1: ( ( ( rule__Method__Group_3__0 )? ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1523:1: ( ( rule__Method__Group_3__0 )? )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1523:1: ( ( rule__Method__Group_3__0 )? )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1524:1: ( rule__Method__Group_3__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getGroup_3()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1525:1: ( rule__Method__Group_3__0 )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==RULE_ID||(LA12_0>=11 && LA12_0<=13)) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1525:2: rule__Method__Group_3__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Method__Group_3__0_in_rule__Method__Group__3__Impl3133);
                    rule__Method__Group_3__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getGroup_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__3__Impl"


    // $ANTLR start "rule__Method__Group__4"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1535:1: rule__Method__Group__4 : rule__Method__Group__4__Impl rule__Method__Group__5 ;
    public final void rule__Method__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1539:1: ( rule__Method__Group__4__Impl rule__Method__Group__5 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1540:2: rule__Method__Group__4__Impl rule__Method__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_rule__Method__Group__4__Impl_in_rule__Method__Group__43164);
            rule__Method__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Method__Group__5_in_rule__Method__Group__43167);
            rule__Method__Group__5();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__4"


    // $ANTLR start "rule__Method__Group__4__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1547:1: rule__Method__Group__4__Impl : ( ')' ) ;
    public final void rule__Method__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1551:1: ( ( ')' ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1552:1: ( ')' )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1552:1: ( ')' )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1553:1: ')'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getRightParenthesisKeyword_4()); 
            }
            match(input,22,FollowSets000.FOLLOW_22_in_rule__Method__Group__4__Impl3195); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getRightParenthesisKeyword_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__4__Impl"


    // $ANTLR start "rule__Method__Group__5"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1566:1: rule__Method__Group__5 : rule__Method__Group__5__Impl rule__Method__Group__6 ;
    public final void rule__Method__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1570:1: ( rule__Method__Group__5__Impl rule__Method__Group__6 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1571:2: rule__Method__Group__5__Impl rule__Method__Group__6
            {
            pushFollow(FollowSets000.FOLLOW_rule__Method__Group__5__Impl_in_rule__Method__Group__53226);
            rule__Method__Group__5__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Method__Group__6_in_rule__Method__Group__53229);
            rule__Method__Group__6();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__5"


    // $ANTLR start "rule__Method__Group__5__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1578:1: rule__Method__Group__5__Impl : ( '{' ) ;
    public final void rule__Method__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1582:1: ( ( '{' ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1583:1: ( '{' )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1583:1: ( '{' )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1584:1: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getLeftCurlyBracketKeyword_5()); 
            }
            match(input,17,FollowSets000.FOLLOW_17_in_rule__Method__Group__5__Impl3257); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getLeftCurlyBracketKeyword_5()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__5__Impl"


    // $ANTLR start "rule__Method__Group__6"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1597:1: rule__Method__Group__6 : rule__Method__Group__6__Impl rule__Method__Group__7 ;
    public final void rule__Method__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1601:1: ( rule__Method__Group__6__Impl rule__Method__Group__7 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1602:2: rule__Method__Group__6__Impl rule__Method__Group__7
            {
            pushFollow(FollowSets000.FOLLOW_rule__Method__Group__6__Impl_in_rule__Method__Group__63288);
            rule__Method__Group__6__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Method__Group__7_in_rule__Method__Group__63291);
            rule__Method__Group__7();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__6"


    // $ANTLR start "rule__Method__Group__6__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1609:1: rule__Method__Group__6__Impl : ( ( rule__Method__BodyAssignment_6 ) ) ;
    public final void rule__Method__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1613:1: ( ( ( rule__Method__BodyAssignment_6 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1614:1: ( ( rule__Method__BodyAssignment_6 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1614:1: ( ( rule__Method__BodyAssignment_6 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1615:1: ( rule__Method__BodyAssignment_6 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getBodyAssignment_6()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1616:1: ( rule__Method__BodyAssignment_6 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1616:2: rule__Method__BodyAssignment_6
            {
            pushFollow(FollowSets000.FOLLOW_rule__Method__BodyAssignment_6_in_rule__Method__Group__6__Impl3318);
            rule__Method__BodyAssignment_6();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getBodyAssignment_6()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__6__Impl"


    // $ANTLR start "rule__Method__Group__7"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1626:1: rule__Method__Group__7 : rule__Method__Group__7__Impl ;
    public final void rule__Method__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1630:1: ( rule__Method__Group__7__Impl )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1631:2: rule__Method__Group__7__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Method__Group__7__Impl_in_rule__Method__Group__73348);
            rule__Method__Group__7__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__7"


    // $ANTLR start "rule__Method__Group__7__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1637:1: rule__Method__Group__7__Impl : ( '}' ) ;
    public final void rule__Method__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1641:1: ( ( '}' ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1642:1: ( '}' )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1642:1: ( '}' )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1643:1: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getRightCurlyBracketKeyword_7()); 
            }
            match(input,18,FollowSets000.FOLLOW_18_in_rule__Method__Group__7__Impl3376); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getRightCurlyBracketKeyword_7()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__7__Impl"


    // $ANTLR start "rule__Method__Group_3__0"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1672:1: rule__Method__Group_3__0 : rule__Method__Group_3__0__Impl rule__Method__Group_3__1 ;
    public final void rule__Method__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1676:1: ( rule__Method__Group_3__0__Impl rule__Method__Group_3__1 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1677:2: rule__Method__Group_3__0__Impl rule__Method__Group_3__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Method__Group_3__0__Impl_in_rule__Method__Group_3__03423);
            rule__Method__Group_3__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Method__Group_3__1_in_rule__Method__Group_3__03426);
            rule__Method__Group_3__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_3__0"


    // $ANTLR start "rule__Method__Group_3__0__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1684:1: rule__Method__Group_3__0__Impl : ( ( rule__Method__ParamsAssignment_3_0 ) ) ;
    public final void rule__Method__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1688:1: ( ( ( rule__Method__ParamsAssignment_3_0 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1689:1: ( ( rule__Method__ParamsAssignment_3_0 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1689:1: ( ( rule__Method__ParamsAssignment_3_0 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1690:1: ( rule__Method__ParamsAssignment_3_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getParamsAssignment_3_0()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1691:1: ( rule__Method__ParamsAssignment_3_0 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1691:2: rule__Method__ParamsAssignment_3_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Method__ParamsAssignment_3_0_in_rule__Method__Group_3__0__Impl3453);
            rule__Method__ParamsAssignment_3_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getParamsAssignment_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_3__0__Impl"


    // $ANTLR start "rule__Method__Group_3__1"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1701:1: rule__Method__Group_3__1 : rule__Method__Group_3__1__Impl ;
    public final void rule__Method__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1705:1: ( rule__Method__Group_3__1__Impl )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1706:2: rule__Method__Group_3__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Method__Group_3__1__Impl_in_rule__Method__Group_3__13483);
            rule__Method__Group_3__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_3__1"


    // $ANTLR start "rule__Method__Group_3__1__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1712:1: rule__Method__Group_3__1__Impl : ( ( rule__Method__Group_3_1__0 )* ) ;
    public final void rule__Method__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1716:1: ( ( ( rule__Method__Group_3_1__0 )* ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1717:1: ( ( rule__Method__Group_3_1__0 )* )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1717:1: ( ( rule__Method__Group_3_1__0 )* )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1718:1: ( rule__Method__Group_3_1__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getGroup_3_1()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1719:1: ( rule__Method__Group_3_1__0 )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==23) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1719:2: rule__Method__Group_3_1__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Method__Group_3_1__0_in_rule__Method__Group_3__1__Impl3510);
            	    rule__Method__Group_3_1__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getGroup_3_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_3__1__Impl"


    // $ANTLR start "rule__Method__Group_3_1__0"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1733:1: rule__Method__Group_3_1__0 : rule__Method__Group_3_1__0__Impl rule__Method__Group_3_1__1 ;
    public final void rule__Method__Group_3_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1737:1: ( rule__Method__Group_3_1__0__Impl rule__Method__Group_3_1__1 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1738:2: rule__Method__Group_3_1__0__Impl rule__Method__Group_3_1__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Method__Group_3_1__0__Impl_in_rule__Method__Group_3_1__03545);
            rule__Method__Group_3_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Method__Group_3_1__1_in_rule__Method__Group_3_1__03548);
            rule__Method__Group_3_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_3_1__0"


    // $ANTLR start "rule__Method__Group_3_1__0__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1745:1: rule__Method__Group_3_1__0__Impl : ( ',' ) ;
    public final void rule__Method__Group_3_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1749:1: ( ( ',' ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1750:1: ( ',' )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1750:1: ( ',' )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1751:1: ','
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getCommaKeyword_3_1_0()); 
            }
            match(input,23,FollowSets000.FOLLOW_23_in_rule__Method__Group_3_1__0__Impl3576); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getCommaKeyword_3_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_3_1__0__Impl"


    // $ANTLR start "rule__Method__Group_3_1__1"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1764:1: rule__Method__Group_3_1__1 : rule__Method__Group_3_1__1__Impl ;
    public final void rule__Method__Group_3_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1768:1: ( rule__Method__Group_3_1__1__Impl )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1769:2: rule__Method__Group_3_1__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Method__Group_3_1__1__Impl_in_rule__Method__Group_3_1__13607);
            rule__Method__Group_3_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_3_1__1"


    // $ANTLR start "rule__Method__Group_3_1__1__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1775:1: rule__Method__Group_3_1__1__Impl : ( ( rule__Method__ParamsAssignment_3_1_1 ) ) ;
    public final void rule__Method__Group_3_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1779:1: ( ( ( rule__Method__ParamsAssignment_3_1_1 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1780:1: ( ( rule__Method__ParamsAssignment_3_1_1 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1780:1: ( ( rule__Method__ParamsAssignment_3_1_1 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1781:1: ( rule__Method__ParamsAssignment_3_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getParamsAssignment_3_1_1()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1782:1: ( rule__Method__ParamsAssignment_3_1_1 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1782:2: rule__Method__ParamsAssignment_3_1_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Method__ParamsAssignment_3_1_1_in_rule__Method__Group_3_1__1__Impl3634);
            rule__Method__ParamsAssignment_3_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getParamsAssignment_3_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_3_1__1__Impl"


    // $ANTLR start "rule__MethodBody__Group__0"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1796:1: rule__MethodBody__Group__0 : rule__MethodBody__Group__0__Impl rule__MethodBody__Group__1 ;
    public final void rule__MethodBody__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1800:1: ( rule__MethodBody__Group__0__Impl rule__MethodBody__Group__1 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1801:2: rule__MethodBody__Group__0__Impl rule__MethodBody__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__MethodBody__Group__0__Impl_in_rule__MethodBody__Group__03668);
            rule__MethodBody__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__MethodBody__Group__1_in_rule__MethodBody__Group__03671);
            rule__MethodBody__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodBody__Group__0"


    // $ANTLR start "rule__MethodBody__Group__0__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1808:1: rule__MethodBody__Group__0__Impl : ( 'return' ) ;
    public final void rule__MethodBody__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1812:1: ( ( 'return' ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1813:1: ( 'return' )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1813:1: ( 'return' )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1814:1: 'return'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodBodyAccess().getReturnKeyword_0()); 
            }
            match(input,24,FollowSets000.FOLLOW_24_in_rule__MethodBody__Group__0__Impl3699); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodBodyAccess().getReturnKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodBody__Group__0__Impl"


    // $ANTLR start "rule__MethodBody__Group__1"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1827:1: rule__MethodBody__Group__1 : rule__MethodBody__Group__1__Impl rule__MethodBody__Group__2 ;
    public final void rule__MethodBody__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1831:1: ( rule__MethodBody__Group__1__Impl rule__MethodBody__Group__2 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1832:2: rule__MethodBody__Group__1__Impl rule__MethodBody__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__MethodBody__Group__1__Impl_in_rule__MethodBody__Group__13730);
            rule__MethodBody__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__MethodBody__Group__2_in_rule__MethodBody__Group__13733);
            rule__MethodBody__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodBody__Group__1"


    // $ANTLR start "rule__MethodBody__Group__1__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1839:1: rule__MethodBody__Group__1__Impl : ( ( rule__MethodBody__ExpressionAssignment_1 ) ) ;
    public final void rule__MethodBody__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1843:1: ( ( ( rule__MethodBody__ExpressionAssignment_1 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1844:1: ( ( rule__MethodBody__ExpressionAssignment_1 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1844:1: ( ( rule__MethodBody__ExpressionAssignment_1 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1845:1: ( rule__MethodBody__ExpressionAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodBodyAccess().getExpressionAssignment_1()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1846:1: ( rule__MethodBody__ExpressionAssignment_1 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1846:2: rule__MethodBody__ExpressionAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__MethodBody__ExpressionAssignment_1_in_rule__MethodBody__Group__1__Impl3760);
            rule__MethodBody__ExpressionAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodBodyAccess().getExpressionAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodBody__Group__1__Impl"


    // $ANTLR start "rule__MethodBody__Group__2"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1856:1: rule__MethodBody__Group__2 : rule__MethodBody__Group__2__Impl ;
    public final void rule__MethodBody__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1860:1: ( rule__MethodBody__Group__2__Impl )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1861:2: rule__MethodBody__Group__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__MethodBody__Group__2__Impl_in_rule__MethodBody__Group__23790);
            rule__MethodBody__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodBody__Group__2"


    // $ANTLR start "rule__MethodBody__Group__2__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1867:1: rule__MethodBody__Group__2__Impl : ( ';' ) ;
    public final void rule__MethodBody__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1871:1: ( ( ';' ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1872:1: ( ';' )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1872:1: ( ';' )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1873:1: ';'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodBodyAccess().getSemicolonKeyword_2()); 
            }
            match(input,20,FollowSets000.FOLLOW_20_in_rule__MethodBody__Group__2__Impl3818); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodBodyAccess().getSemicolonKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodBody__Group__2__Impl"


    // $ANTLR start "rule__Expression__Group__0"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1892:1: rule__Expression__Group__0 : rule__Expression__Group__0__Impl rule__Expression__Group__1 ;
    public final void rule__Expression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1896:1: ( rule__Expression__Group__0__Impl rule__Expression__Group__1 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1897:2: rule__Expression__Group__0__Impl rule__Expression__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Expression__Group__0__Impl_in_rule__Expression__Group__03855);
            rule__Expression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Expression__Group__1_in_rule__Expression__Group__03858);
            rule__Expression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Group__0"


    // $ANTLR start "rule__Expression__Group__0__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1904:1: rule__Expression__Group__0__Impl : ( ruleTerminalExpression ) ;
    public final void rule__Expression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1908:1: ( ( ruleTerminalExpression ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1909:1: ( ruleTerminalExpression )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1909:1: ( ruleTerminalExpression )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1910:1: ruleTerminalExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionAccess().getTerminalExpressionParserRuleCall_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleTerminalExpression_in_rule__Expression__Group__0__Impl3885);
            ruleTerminalExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionAccess().getTerminalExpressionParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Group__0__Impl"


    // $ANTLR start "rule__Expression__Group__1"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1921:1: rule__Expression__Group__1 : rule__Expression__Group__1__Impl ;
    public final void rule__Expression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1925:1: ( rule__Expression__Group__1__Impl )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1926:2: rule__Expression__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Expression__Group__1__Impl_in_rule__Expression__Group__13914);
            rule__Expression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Group__1"


    // $ANTLR start "rule__Expression__Group__1__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1932:1: rule__Expression__Group__1__Impl : ( ( rule__Expression__Group_1__0 )* ) ;
    public final void rule__Expression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1936:1: ( ( ( rule__Expression__Group_1__0 )* ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1937:1: ( ( rule__Expression__Group_1__0 )* )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1937:1: ( ( rule__Expression__Group_1__0 )* )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1938:1: ( rule__Expression__Group_1__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionAccess().getGroup_1()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1939:1: ( rule__Expression__Group_1__0 )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==25) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1939:2: rule__Expression__Group_1__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Expression__Group_1__0_in_rule__Expression__Group__1__Impl3941);
            	    rule__Expression__Group_1__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Group__1__Impl"


    // $ANTLR start "rule__Expression__Group_1__0"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1953:1: rule__Expression__Group_1__0 : rule__Expression__Group_1__0__Impl rule__Expression__Group_1__1 ;
    public final void rule__Expression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1957:1: ( rule__Expression__Group_1__0__Impl rule__Expression__Group_1__1 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1958:2: rule__Expression__Group_1__0__Impl rule__Expression__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Expression__Group_1__0__Impl_in_rule__Expression__Group_1__03976);
            rule__Expression__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Expression__Group_1__1_in_rule__Expression__Group_1__03979);
            rule__Expression__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Group_1__0"


    // $ANTLR start "rule__Expression__Group_1__0__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1965:1: rule__Expression__Group_1__0__Impl : ( () ) ;
    public final void rule__Expression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1969:1: ( ( () ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1970:1: ( () )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1970:1: ( () )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1971:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionAccess().getSelectionReceiverAction_1_0()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1972:1: ()
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1974:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionAccess().getSelectionReceiverAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Group_1__0__Impl"


    // $ANTLR start "rule__Expression__Group_1__1"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1984:1: rule__Expression__Group_1__1 : rule__Expression__Group_1__1__Impl rule__Expression__Group_1__2 ;
    public final void rule__Expression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1988:1: ( rule__Expression__Group_1__1__Impl rule__Expression__Group_1__2 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1989:2: rule__Expression__Group_1__1__Impl rule__Expression__Group_1__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Expression__Group_1__1__Impl_in_rule__Expression__Group_1__14037);
            rule__Expression__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Expression__Group_1__2_in_rule__Expression__Group_1__14040);
            rule__Expression__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Group_1__1"


    // $ANTLR start "rule__Expression__Group_1__1__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:1996:1: rule__Expression__Group_1__1__Impl : ( '.' ) ;
    public final void rule__Expression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2000:1: ( ( '.' ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2001:1: ( '.' )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2001:1: ( '.' )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2002:1: '.'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionAccess().getFullStopKeyword_1_1()); 
            }
            match(input,25,FollowSets000.FOLLOW_25_in_rule__Expression__Group_1__1__Impl4068); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionAccess().getFullStopKeyword_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Group_1__1__Impl"


    // $ANTLR start "rule__Expression__Group_1__2"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2015:1: rule__Expression__Group_1__2 : rule__Expression__Group_1__2__Impl ;
    public final void rule__Expression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2019:1: ( rule__Expression__Group_1__2__Impl )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2020:2: rule__Expression__Group_1__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Expression__Group_1__2__Impl_in_rule__Expression__Group_1__24099);
            rule__Expression__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Group_1__2"


    // $ANTLR start "rule__Expression__Group_1__2__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2026:1: rule__Expression__Group_1__2__Impl : ( ( rule__Expression__MessageAssignment_1_2 ) ) ;
    public final void rule__Expression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2030:1: ( ( ( rule__Expression__MessageAssignment_1_2 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2031:1: ( ( rule__Expression__MessageAssignment_1_2 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2031:1: ( ( rule__Expression__MessageAssignment_1_2 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2032:1: ( rule__Expression__MessageAssignment_1_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionAccess().getMessageAssignment_1_2()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2033:1: ( rule__Expression__MessageAssignment_1_2 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2033:2: rule__Expression__MessageAssignment_1_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Expression__MessageAssignment_1_2_in_rule__Expression__Group_1__2__Impl4126);
            rule__Expression__MessageAssignment_1_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionAccess().getMessageAssignment_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Group_1__2__Impl"


    // $ANTLR start "rule__MethodCall__Group__0"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2049:1: rule__MethodCall__Group__0 : rule__MethodCall__Group__0__Impl rule__MethodCall__Group__1 ;
    public final void rule__MethodCall__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2053:1: ( rule__MethodCall__Group__0__Impl rule__MethodCall__Group__1 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2054:2: rule__MethodCall__Group__0__Impl rule__MethodCall__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__MethodCall__Group__0__Impl_in_rule__MethodCall__Group__04162);
            rule__MethodCall__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__MethodCall__Group__1_in_rule__MethodCall__Group__04165);
            rule__MethodCall__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__Group__0"


    // $ANTLR start "rule__MethodCall__Group__0__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2061:1: rule__MethodCall__Group__0__Impl : ( ( rule__MethodCall__NameAssignment_0 ) ) ;
    public final void rule__MethodCall__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2065:1: ( ( ( rule__MethodCall__NameAssignment_0 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2066:1: ( ( rule__MethodCall__NameAssignment_0 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2066:1: ( ( rule__MethodCall__NameAssignment_0 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2067:1: ( rule__MethodCall__NameAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodCallAccess().getNameAssignment_0()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2068:1: ( rule__MethodCall__NameAssignment_0 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2068:2: rule__MethodCall__NameAssignment_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__MethodCall__NameAssignment_0_in_rule__MethodCall__Group__0__Impl4192);
            rule__MethodCall__NameAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodCallAccess().getNameAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__Group__0__Impl"


    // $ANTLR start "rule__MethodCall__Group__1"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2078:1: rule__MethodCall__Group__1 : rule__MethodCall__Group__1__Impl rule__MethodCall__Group__2 ;
    public final void rule__MethodCall__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2082:1: ( rule__MethodCall__Group__1__Impl rule__MethodCall__Group__2 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2083:2: rule__MethodCall__Group__1__Impl rule__MethodCall__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__MethodCall__Group__1__Impl_in_rule__MethodCall__Group__14222);
            rule__MethodCall__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__MethodCall__Group__2_in_rule__MethodCall__Group__14225);
            rule__MethodCall__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__Group__1"


    // $ANTLR start "rule__MethodCall__Group__1__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2090:1: rule__MethodCall__Group__1__Impl : ( '(' ) ;
    public final void rule__MethodCall__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2094:1: ( ( '(' ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2095:1: ( '(' )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2095:1: ( '(' )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2096:1: '('
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodCallAccess().getLeftParenthesisKeyword_1()); 
            }
            match(input,21,FollowSets000.FOLLOW_21_in_rule__MethodCall__Group__1__Impl4253); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodCallAccess().getLeftParenthesisKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__Group__1__Impl"


    // $ANTLR start "rule__MethodCall__Group__2"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2109:1: rule__MethodCall__Group__2 : rule__MethodCall__Group__2__Impl rule__MethodCall__Group__3 ;
    public final void rule__MethodCall__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2113:1: ( rule__MethodCall__Group__2__Impl rule__MethodCall__Group__3 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2114:2: rule__MethodCall__Group__2__Impl rule__MethodCall__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__MethodCall__Group__2__Impl_in_rule__MethodCall__Group__24284);
            rule__MethodCall__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__MethodCall__Group__3_in_rule__MethodCall__Group__24287);
            rule__MethodCall__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__Group__2"


    // $ANTLR start "rule__MethodCall__Group__2__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2121:1: rule__MethodCall__Group__2__Impl : ( ( rule__MethodCall__Group_2__0 )? ) ;
    public final void rule__MethodCall__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2125:1: ( ( ( rule__MethodCall__Group_2__0 )? ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2126:1: ( ( rule__MethodCall__Group_2__0 )? )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2126:1: ( ( rule__MethodCall__Group_2__0 )? )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2127:1: ( rule__MethodCall__Group_2__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodCallAccess().getGroup_2()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2128:1: ( rule__MethodCall__Group_2__0 )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( ((LA15_0>=RULE_ID && LA15_0<=RULE_INT)||(LA15_0>=14 && LA15_0<=15)||LA15_0==21||(LA15_0>=26 && LA15_0<=27)) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2128:2: rule__MethodCall__Group_2__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__MethodCall__Group_2__0_in_rule__MethodCall__Group__2__Impl4314);
                    rule__MethodCall__Group_2__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodCallAccess().getGroup_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__Group__2__Impl"


    // $ANTLR start "rule__MethodCall__Group__3"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2138:1: rule__MethodCall__Group__3 : rule__MethodCall__Group__3__Impl ;
    public final void rule__MethodCall__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2142:1: ( rule__MethodCall__Group__3__Impl )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2143:2: rule__MethodCall__Group__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__MethodCall__Group__3__Impl_in_rule__MethodCall__Group__34345);
            rule__MethodCall__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__Group__3"


    // $ANTLR start "rule__MethodCall__Group__3__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2149:1: rule__MethodCall__Group__3__Impl : ( ')' ) ;
    public final void rule__MethodCall__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2153:1: ( ( ')' ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2154:1: ( ')' )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2154:1: ( ')' )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2155:1: ')'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodCallAccess().getRightParenthesisKeyword_3()); 
            }
            match(input,22,FollowSets000.FOLLOW_22_in_rule__MethodCall__Group__3__Impl4373); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodCallAccess().getRightParenthesisKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__Group__3__Impl"


    // $ANTLR start "rule__MethodCall__Group_2__0"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2176:1: rule__MethodCall__Group_2__0 : rule__MethodCall__Group_2__0__Impl rule__MethodCall__Group_2__1 ;
    public final void rule__MethodCall__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2180:1: ( rule__MethodCall__Group_2__0__Impl rule__MethodCall__Group_2__1 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2181:2: rule__MethodCall__Group_2__0__Impl rule__MethodCall__Group_2__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__MethodCall__Group_2__0__Impl_in_rule__MethodCall__Group_2__04412);
            rule__MethodCall__Group_2__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__MethodCall__Group_2__1_in_rule__MethodCall__Group_2__04415);
            rule__MethodCall__Group_2__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__Group_2__0"


    // $ANTLR start "rule__MethodCall__Group_2__0__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2188:1: rule__MethodCall__Group_2__0__Impl : ( ( rule__MethodCall__ArgsAssignment_2_0 ) ) ;
    public final void rule__MethodCall__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2192:1: ( ( ( rule__MethodCall__ArgsAssignment_2_0 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2193:1: ( ( rule__MethodCall__ArgsAssignment_2_0 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2193:1: ( ( rule__MethodCall__ArgsAssignment_2_0 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2194:1: ( rule__MethodCall__ArgsAssignment_2_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodCallAccess().getArgsAssignment_2_0()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2195:1: ( rule__MethodCall__ArgsAssignment_2_0 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2195:2: rule__MethodCall__ArgsAssignment_2_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__MethodCall__ArgsAssignment_2_0_in_rule__MethodCall__Group_2__0__Impl4442);
            rule__MethodCall__ArgsAssignment_2_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodCallAccess().getArgsAssignment_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__Group_2__0__Impl"


    // $ANTLR start "rule__MethodCall__Group_2__1"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2205:1: rule__MethodCall__Group_2__1 : rule__MethodCall__Group_2__1__Impl ;
    public final void rule__MethodCall__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2209:1: ( rule__MethodCall__Group_2__1__Impl )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2210:2: rule__MethodCall__Group_2__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__MethodCall__Group_2__1__Impl_in_rule__MethodCall__Group_2__14472);
            rule__MethodCall__Group_2__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__Group_2__1"


    // $ANTLR start "rule__MethodCall__Group_2__1__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2216:1: rule__MethodCall__Group_2__1__Impl : ( ( rule__MethodCall__Group_2_1__0 )* ) ;
    public final void rule__MethodCall__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2220:1: ( ( ( rule__MethodCall__Group_2_1__0 )* ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2221:1: ( ( rule__MethodCall__Group_2_1__0 )* )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2221:1: ( ( rule__MethodCall__Group_2_1__0 )* )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2222:1: ( rule__MethodCall__Group_2_1__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodCallAccess().getGroup_2_1()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2223:1: ( rule__MethodCall__Group_2_1__0 )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==23) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2223:2: rule__MethodCall__Group_2_1__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__MethodCall__Group_2_1__0_in_rule__MethodCall__Group_2__1__Impl4499);
            	    rule__MethodCall__Group_2_1__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodCallAccess().getGroup_2_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__Group_2__1__Impl"


    // $ANTLR start "rule__MethodCall__Group_2_1__0"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2237:1: rule__MethodCall__Group_2_1__0 : rule__MethodCall__Group_2_1__0__Impl rule__MethodCall__Group_2_1__1 ;
    public final void rule__MethodCall__Group_2_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2241:1: ( rule__MethodCall__Group_2_1__0__Impl rule__MethodCall__Group_2_1__1 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2242:2: rule__MethodCall__Group_2_1__0__Impl rule__MethodCall__Group_2_1__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__MethodCall__Group_2_1__0__Impl_in_rule__MethodCall__Group_2_1__04534);
            rule__MethodCall__Group_2_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__MethodCall__Group_2_1__1_in_rule__MethodCall__Group_2_1__04537);
            rule__MethodCall__Group_2_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__Group_2_1__0"


    // $ANTLR start "rule__MethodCall__Group_2_1__0__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2249:1: rule__MethodCall__Group_2_1__0__Impl : ( ',' ) ;
    public final void rule__MethodCall__Group_2_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2253:1: ( ( ',' ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2254:1: ( ',' )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2254:1: ( ',' )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2255:1: ','
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodCallAccess().getCommaKeyword_2_1_0()); 
            }
            match(input,23,FollowSets000.FOLLOW_23_in_rule__MethodCall__Group_2_1__0__Impl4565); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodCallAccess().getCommaKeyword_2_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__Group_2_1__0__Impl"


    // $ANTLR start "rule__MethodCall__Group_2_1__1"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2268:1: rule__MethodCall__Group_2_1__1 : rule__MethodCall__Group_2_1__1__Impl ;
    public final void rule__MethodCall__Group_2_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2272:1: ( rule__MethodCall__Group_2_1__1__Impl )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2273:2: rule__MethodCall__Group_2_1__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__MethodCall__Group_2_1__1__Impl_in_rule__MethodCall__Group_2_1__14596);
            rule__MethodCall__Group_2_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__Group_2_1__1"


    // $ANTLR start "rule__MethodCall__Group_2_1__1__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2279:1: rule__MethodCall__Group_2_1__1__Impl : ( ( rule__MethodCall__ArgsAssignment_2_1_1 ) ) ;
    public final void rule__MethodCall__Group_2_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2283:1: ( ( ( rule__MethodCall__ArgsAssignment_2_1_1 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2284:1: ( ( rule__MethodCall__ArgsAssignment_2_1_1 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2284:1: ( ( rule__MethodCall__ArgsAssignment_2_1_1 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2285:1: ( rule__MethodCall__ArgsAssignment_2_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodCallAccess().getArgsAssignment_2_1_1()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2286:1: ( rule__MethodCall__ArgsAssignment_2_1_1 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2286:2: rule__MethodCall__ArgsAssignment_2_1_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__MethodCall__ArgsAssignment_2_1_1_in_rule__MethodCall__Group_2_1__1__Impl4623);
            rule__MethodCall__ArgsAssignment_2_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodCallAccess().getArgsAssignment_2_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__Group_2_1__1__Impl"


    // $ANTLR start "rule__New__Group__0"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2300:1: rule__New__Group__0 : rule__New__Group__0__Impl rule__New__Group__1 ;
    public final void rule__New__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2304:1: ( rule__New__Group__0__Impl rule__New__Group__1 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2305:2: rule__New__Group__0__Impl rule__New__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__New__Group__0__Impl_in_rule__New__Group__04657);
            rule__New__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__New__Group__1_in_rule__New__Group__04660);
            rule__New__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group__0"


    // $ANTLR start "rule__New__Group__0__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2312:1: rule__New__Group__0__Impl : ( 'new' ) ;
    public final void rule__New__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2316:1: ( ( 'new' ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2317:1: ( 'new' )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2317:1: ( 'new' )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2318:1: 'new'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNewAccess().getNewKeyword_0()); 
            }
            match(input,26,FollowSets000.FOLLOW_26_in_rule__New__Group__0__Impl4688); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNewAccess().getNewKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group__0__Impl"


    // $ANTLR start "rule__New__Group__1"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2331:1: rule__New__Group__1 : rule__New__Group__1__Impl rule__New__Group__2 ;
    public final void rule__New__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2335:1: ( rule__New__Group__1__Impl rule__New__Group__2 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2336:2: rule__New__Group__1__Impl rule__New__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__New__Group__1__Impl_in_rule__New__Group__14719);
            rule__New__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__New__Group__2_in_rule__New__Group__14722);
            rule__New__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group__1"


    // $ANTLR start "rule__New__Group__1__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2343:1: rule__New__Group__1__Impl : ( ( rule__New__TypeAssignment_1 ) ) ;
    public final void rule__New__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2347:1: ( ( ( rule__New__TypeAssignment_1 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2348:1: ( ( rule__New__TypeAssignment_1 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2348:1: ( ( rule__New__TypeAssignment_1 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2349:1: ( rule__New__TypeAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNewAccess().getTypeAssignment_1()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2350:1: ( rule__New__TypeAssignment_1 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2350:2: rule__New__TypeAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__New__TypeAssignment_1_in_rule__New__Group__1__Impl4749);
            rule__New__TypeAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNewAccess().getTypeAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group__1__Impl"


    // $ANTLR start "rule__New__Group__2"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2360:1: rule__New__Group__2 : rule__New__Group__2__Impl rule__New__Group__3 ;
    public final void rule__New__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2364:1: ( rule__New__Group__2__Impl rule__New__Group__3 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2365:2: rule__New__Group__2__Impl rule__New__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__New__Group__2__Impl_in_rule__New__Group__24779);
            rule__New__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__New__Group__3_in_rule__New__Group__24782);
            rule__New__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group__2"


    // $ANTLR start "rule__New__Group__2__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2372:1: rule__New__Group__2__Impl : ( '(' ) ;
    public final void rule__New__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2376:1: ( ( '(' ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2377:1: ( '(' )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2377:1: ( '(' )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2378:1: '('
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNewAccess().getLeftParenthesisKeyword_2()); 
            }
            match(input,21,FollowSets000.FOLLOW_21_in_rule__New__Group__2__Impl4810); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNewAccess().getLeftParenthesisKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group__2__Impl"


    // $ANTLR start "rule__New__Group__3"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2391:1: rule__New__Group__3 : rule__New__Group__3__Impl rule__New__Group__4 ;
    public final void rule__New__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2395:1: ( rule__New__Group__3__Impl rule__New__Group__4 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2396:2: rule__New__Group__3__Impl rule__New__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__New__Group__3__Impl_in_rule__New__Group__34841);
            rule__New__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__New__Group__4_in_rule__New__Group__34844);
            rule__New__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group__3"


    // $ANTLR start "rule__New__Group__3__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2403:1: rule__New__Group__3__Impl : ( ( rule__New__Group_3__0 )? ) ;
    public final void rule__New__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2407:1: ( ( ( rule__New__Group_3__0 )? ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2408:1: ( ( rule__New__Group_3__0 )? )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2408:1: ( ( rule__New__Group_3__0 )? )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2409:1: ( rule__New__Group_3__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNewAccess().getGroup_3()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2410:1: ( rule__New__Group_3__0 )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( ((LA17_0>=RULE_ID && LA17_0<=RULE_INT)||(LA17_0>=14 && LA17_0<=15)||LA17_0==21||(LA17_0>=26 && LA17_0<=27)) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2410:2: rule__New__Group_3__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__New__Group_3__0_in_rule__New__Group__3__Impl4871);
                    rule__New__Group_3__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNewAccess().getGroup_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group__3__Impl"


    // $ANTLR start "rule__New__Group__4"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2420:1: rule__New__Group__4 : rule__New__Group__4__Impl ;
    public final void rule__New__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2424:1: ( rule__New__Group__4__Impl )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2425:2: rule__New__Group__4__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__New__Group__4__Impl_in_rule__New__Group__44902);
            rule__New__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group__4"


    // $ANTLR start "rule__New__Group__4__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2431:1: rule__New__Group__4__Impl : ( ')' ) ;
    public final void rule__New__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2435:1: ( ( ')' ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2436:1: ( ')' )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2436:1: ( ')' )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2437:1: ')'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNewAccess().getRightParenthesisKeyword_4()); 
            }
            match(input,22,FollowSets000.FOLLOW_22_in_rule__New__Group__4__Impl4930); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNewAccess().getRightParenthesisKeyword_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group__4__Impl"


    // $ANTLR start "rule__New__Group_3__0"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2460:1: rule__New__Group_3__0 : rule__New__Group_3__0__Impl rule__New__Group_3__1 ;
    public final void rule__New__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2464:1: ( rule__New__Group_3__0__Impl rule__New__Group_3__1 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2465:2: rule__New__Group_3__0__Impl rule__New__Group_3__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__New__Group_3__0__Impl_in_rule__New__Group_3__04971);
            rule__New__Group_3__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__New__Group_3__1_in_rule__New__Group_3__04974);
            rule__New__Group_3__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group_3__0"


    // $ANTLR start "rule__New__Group_3__0__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2472:1: rule__New__Group_3__0__Impl : ( ( rule__New__ArgsAssignment_3_0 ) ) ;
    public final void rule__New__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2476:1: ( ( ( rule__New__ArgsAssignment_3_0 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2477:1: ( ( rule__New__ArgsAssignment_3_0 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2477:1: ( ( rule__New__ArgsAssignment_3_0 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2478:1: ( rule__New__ArgsAssignment_3_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNewAccess().getArgsAssignment_3_0()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2479:1: ( rule__New__ArgsAssignment_3_0 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2479:2: rule__New__ArgsAssignment_3_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__New__ArgsAssignment_3_0_in_rule__New__Group_3__0__Impl5001);
            rule__New__ArgsAssignment_3_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNewAccess().getArgsAssignment_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group_3__0__Impl"


    // $ANTLR start "rule__New__Group_3__1"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2489:1: rule__New__Group_3__1 : rule__New__Group_3__1__Impl ;
    public final void rule__New__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2493:1: ( rule__New__Group_3__1__Impl )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2494:2: rule__New__Group_3__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__New__Group_3__1__Impl_in_rule__New__Group_3__15031);
            rule__New__Group_3__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group_3__1"


    // $ANTLR start "rule__New__Group_3__1__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2500:1: rule__New__Group_3__1__Impl : ( ( rule__New__Group_3_1__0 )* ) ;
    public final void rule__New__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2504:1: ( ( ( rule__New__Group_3_1__0 )* ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2505:1: ( ( rule__New__Group_3_1__0 )* )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2505:1: ( ( rule__New__Group_3_1__0 )* )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2506:1: ( rule__New__Group_3_1__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNewAccess().getGroup_3_1()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2507:1: ( rule__New__Group_3_1__0 )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( (LA18_0==23) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2507:2: rule__New__Group_3_1__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__New__Group_3_1__0_in_rule__New__Group_3__1__Impl5058);
            	    rule__New__Group_3_1__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNewAccess().getGroup_3_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group_3__1__Impl"


    // $ANTLR start "rule__New__Group_3_1__0"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2521:1: rule__New__Group_3_1__0 : rule__New__Group_3_1__0__Impl rule__New__Group_3_1__1 ;
    public final void rule__New__Group_3_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2525:1: ( rule__New__Group_3_1__0__Impl rule__New__Group_3_1__1 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2526:2: rule__New__Group_3_1__0__Impl rule__New__Group_3_1__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__New__Group_3_1__0__Impl_in_rule__New__Group_3_1__05093);
            rule__New__Group_3_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__New__Group_3_1__1_in_rule__New__Group_3_1__05096);
            rule__New__Group_3_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group_3_1__0"


    // $ANTLR start "rule__New__Group_3_1__0__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2533:1: rule__New__Group_3_1__0__Impl : ( ',' ) ;
    public final void rule__New__Group_3_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2537:1: ( ( ',' ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2538:1: ( ',' )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2538:1: ( ',' )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2539:1: ','
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNewAccess().getCommaKeyword_3_1_0()); 
            }
            match(input,23,FollowSets000.FOLLOW_23_in_rule__New__Group_3_1__0__Impl5124); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNewAccess().getCommaKeyword_3_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group_3_1__0__Impl"


    // $ANTLR start "rule__New__Group_3_1__1"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2552:1: rule__New__Group_3_1__1 : rule__New__Group_3_1__1__Impl ;
    public final void rule__New__Group_3_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2556:1: ( rule__New__Group_3_1__1__Impl )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2557:2: rule__New__Group_3_1__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__New__Group_3_1__1__Impl_in_rule__New__Group_3_1__15155);
            rule__New__Group_3_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group_3_1__1"


    // $ANTLR start "rule__New__Group_3_1__1__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2563:1: rule__New__Group_3_1__1__Impl : ( ( rule__New__ArgsAssignment_3_1_1 ) ) ;
    public final void rule__New__Group_3_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2567:1: ( ( ( rule__New__ArgsAssignment_3_1_1 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2568:1: ( ( rule__New__ArgsAssignment_3_1_1 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2568:1: ( ( rule__New__ArgsAssignment_3_1_1 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2569:1: ( rule__New__ArgsAssignment_3_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNewAccess().getArgsAssignment_3_1_1()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2570:1: ( rule__New__ArgsAssignment_3_1_1 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2570:2: rule__New__ArgsAssignment_3_1_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__New__ArgsAssignment_3_1_1_in_rule__New__Group_3_1__1__Impl5182);
            rule__New__ArgsAssignment_3_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNewAccess().getArgsAssignment_3_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group_3_1__1__Impl"


    // $ANTLR start "rule__Cast__Group__0"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2584:1: rule__Cast__Group__0 : rule__Cast__Group__0__Impl rule__Cast__Group__1 ;
    public final void rule__Cast__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2588:1: ( rule__Cast__Group__0__Impl rule__Cast__Group__1 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2589:2: rule__Cast__Group__0__Impl rule__Cast__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Cast__Group__0__Impl_in_rule__Cast__Group__05216);
            rule__Cast__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Cast__Group__1_in_rule__Cast__Group__05219);
            rule__Cast__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cast__Group__0"


    // $ANTLR start "rule__Cast__Group__0__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2596:1: rule__Cast__Group__0__Impl : ( '(' ) ;
    public final void rule__Cast__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2600:1: ( ( '(' ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2601:1: ( '(' )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2601:1: ( '(' )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2602:1: '('
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCastAccess().getLeftParenthesisKeyword_0()); 
            }
            match(input,21,FollowSets000.FOLLOW_21_in_rule__Cast__Group__0__Impl5247); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCastAccess().getLeftParenthesisKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cast__Group__0__Impl"


    // $ANTLR start "rule__Cast__Group__1"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2615:1: rule__Cast__Group__1 : rule__Cast__Group__1__Impl rule__Cast__Group__2 ;
    public final void rule__Cast__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2619:1: ( rule__Cast__Group__1__Impl rule__Cast__Group__2 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2620:2: rule__Cast__Group__1__Impl rule__Cast__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Cast__Group__1__Impl_in_rule__Cast__Group__15278);
            rule__Cast__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Cast__Group__2_in_rule__Cast__Group__15281);
            rule__Cast__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cast__Group__1"


    // $ANTLR start "rule__Cast__Group__1__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2627:1: rule__Cast__Group__1__Impl : ( ( rule__Cast__TypeAssignment_1 ) ) ;
    public final void rule__Cast__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2631:1: ( ( ( rule__Cast__TypeAssignment_1 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2632:1: ( ( rule__Cast__TypeAssignment_1 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2632:1: ( ( rule__Cast__TypeAssignment_1 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2633:1: ( rule__Cast__TypeAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCastAccess().getTypeAssignment_1()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2634:1: ( rule__Cast__TypeAssignment_1 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2634:2: rule__Cast__TypeAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Cast__TypeAssignment_1_in_rule__Cast__Group__1__Impl5308);
            rule__Cast__TypeAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCastAccess().getTypeAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cast__Group__1__Impl"


    // $ANTLR start "rule__Cast__Group__2"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2644:1: rule__Cast__Group__2 : rule__Cast__Group__2__Impl rule__Cast__Group__3 ;
    public final void rule__Cast__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2648:1: ( rule__Cast__Group__2__Impl rule__Cast__Group__3 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2649:2: rule__Cast__Group__2__Impl rule__Cast__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Cast__Group__2__Impl_in_rule__Cast__Group__25338);
            rule__Cast__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Cast__Group__3_in_rule__Cast__Group__25341);
            rule__Cast__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cast__Group__2"


    // $ANTLR start "rule__Cast__Group__2__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2656:1: rule__Cast__Group__2__Impl : ( ')' ) ;
    public final void rule__Cast__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2660:1: ( ( ')' ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2661:1: ( ')' )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2661:1: ( ')' )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2662:1: ')'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCastAccess().getRightParenthesisKeyword_2()); 
            }
            match(input,22,FollowSets000.FOLLOW_22_in_rule__Cast__Group__2__Impl5369); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCastAccess().getRightParenthesisKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cast__Group__2__Impl"


    // $ANTLR start "rule__Cast__Group__3"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2675:1: rule__Cast__Group__3 : rule__Cast__Group__3__Impl ;
    public final void rule__Cast__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2679:1: ( rule__Cast__Group__3__Impl )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2680:2: rule__Cast__Group__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Cast__Group__3__Impl_in_rule__Cast__Group__35400);
            rule__Cast__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cast__Group__3"


    // $ANTLR start "rule__Cast__Group__3__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2686:1: rule__Cast__Group__3__Impl : ( ( rule__Cast__ObjectAssignment_3 ) ) ;
    public final void rule__Cast__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2690:1: ( ( ( rule__Cast__ObjectAssignment_3 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2691:1: ( ( rule__Cast__ObjectAssignment_3 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2691:1: ( ( rule__Cast__ObjectAssignment_3 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2692:1: ( rule__Cast__ObjectAssignment_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCastAccess().getObjectAssignment_3()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2693:1: ( rule__Cast__ObjectAssignment_3 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2693:2: rule__Cast__ObjectAssignment_3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Cast__ObjectAssignment_3_in_rule__Cast__Group__3__Impl5427);
            rule__Cast__ObjectAssignment_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCastAccess().getObjectAssignment_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cast__Group__3__Impl"


    // $ANTLR start "rule__Paren__Group__0"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2711:1: rule__Paren__Group__0 : rule__Paren__Group__0__Impl rule__Paren__Group__1 ;
    public final void rule__Paren__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2715:1: ( rule__Paren__Group__0__Impl rule__Paren__Group__1 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2716:2: rule__Paren__Group__0__Impl rule__Paren__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Paren__Group__0__Impl_in_rule__Paren__Group__05465);
            rule__Paren__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Paren__Group__1_in_rule__Paren__Group__05468);
            rule__Paren__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Paren__Group__0"


    // $ANTLR start "rule__Paren__Group__0__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2723:1: rule__Paren__Group__0__Impl : ( '(' ) ;
    public final void rule__Paren__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2727:1: ( ( '(' ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2728:1: ( '(' )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2728:1: ( '(' )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2729:1: '('
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParenAccess().getLeftParenthesisKeyword_0()); 
            }
            match(input,21,FollowSets000.FOLLOW_21_in_rule__Paren__Group__0__Impl5496); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getParenAccess().getLeftParenthesisKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Paren__Group__0__Impl"


    // $ANTLR start "rule__Paren__Group__1"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2742:1: rule__Paren__Group__1 : rule__Paren__Group__1__Impl rule__Paren__Group__2 ;
    public final void rule__Paren__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2746:1: ( rule__Paren__Group__1__Impl rule__Paren__Group__2 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2747:2: rule__Paren__Group__1__Impl rule__Paren__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Paren__Group__1__Impl_in_rule__Paren__Group__15527);
            rule__Paren__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_rule__Paren__Group__2_in_rule__Paren__Group__15530);
            rule__Paren__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Paren__Group__1"


    // $ANTLR start "rule__Paren__Group__1__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2754:1: rule__Paren__Group__1__Impl : ( ruleExpression ) ;
    public final void rule__Paren__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2758:1: ( ( ruleExpression ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2759:1: ( ruleExpression )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2759:1: ( ruleExpression )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2760:1: ruleExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParenAccess().getExpressionParserRuleCall_1()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleExpression_in_rule__Paren__Group__1__Impl5557);
            ruleExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getParenAccess().getExpressionParserRuleCall_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Paren__Group__1__Impl"


    // $ANTLR start "rule__Paren__Group__2"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2771:1: rule__Paren__Group__2 : rule__Paren__Group__2__Impl ;
    public final void rule__Paren__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2775:1: ( rule__Paren__Group__2__Impl )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2776:2: rule__Paren__Group__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Paren__Group__2__Impl_in_rule__Paren__Group__25586);
            rule__Paren__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Paren__Group__2"


    // $ANTLR start "rule__Paren__Group__2__Impl"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2782:1: rule__Paren__Group__2__Impl : ( ')' ) ;
    public final void rule__Paren__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2786:1: ( ( ')' ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2787:1: ( ')' )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2787:1: ( ')' )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2788:1: ')'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParenAccess().getRightParenthesisKeyword_2()); 
            }
            match(input,22,FollowSets000.FOLLOW_22_in_rule__Paren__Group__2__Impl5614); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getParenAccess().getRightParenthesisKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Paren__Group__2__Impl"


    // $ANTLR start "rule__Program__ClassesAssignment_0"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2808:1: rule__Program__ClassesAssignment_0 : ( ruleClass ) ;
    public final void rule__Program__ClassesAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2812:1: ( ( ruleClass ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2813:1: ( ruleClass )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2813:1: ( ruleClass )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2814:1: ruleClass
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getProgramAccess().getClassesClassParserRuleCall_0_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleClass_in_rule__Program__ClassesAssignment_05656);
            ruleClass();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getProgramAccess().getClassesClassParserRuleCall_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Program__ClassesAssignment_0"


    // $ANTLR start "rule__Program__MainAssignment_1"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2823:1: rule__Program__MainAssignment_1 : ( ruleExpression ) ;
    public final void rule__Program__MainAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2827:1: ( ( ruleExpression ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2828:1: ( ruleExpression )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2828:1: ( ruleExpression )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2829:1: ruleExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getProgramAccess().getMainExpressionParserRuleCall_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleExpression_in_rule__Program__MainAssignment_15687);
            ruleExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getProgramAccess().getMainExpressionParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Program__MainAssignment_1"


    // $ANTLR start "rule__BasicType__BasicAssignment"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2838:1: rule__BasicType__BasicAssignment : ( ( rule__BasicType__BasicAlternatives_0 ) ) ;
    public final void rule__BasicType__BasicAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2842:1: ( ( ( rule__BasicType__BasicAlternatives_0 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2843:1: ( ( rule__BasicType__BasicAlternatives_0 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2843:1: ( ( rule__BasicType__BasicAlternatives_0 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2844:1: ( rule__BasicType__BasicAlternatives_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicTypeAccess().getBasicAlternatives_0()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2845:1: ( rule__BasicType__BasicAlternatives_0 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2845:2: rule__BasicType__BasicAlternatives_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__BasicType__BasicAlternatives_0_in_rule__BasicType__BasicAssignment5718);
            rule__BasicType__BasicAlternatives_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicTypeAccess().getBasicAlternatives_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicType__BasicAssignment"


    // $ANTLR start "rule__ClassType__ClassrefAssignment"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2854:1: rule__ClassType__ClassrefAssignment : ( ( RULE_ID ) ) ;
    public final void rule__ClassType__ClassrefAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2858:1: ( ( ( RULE_ID ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2859:1: ( ( RULE_ID ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2859:1: ( ( RULE_ID ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2860:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassTypeAccess().getClassrefClassCrossReference_0()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2861:1: ( RULE_ID )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2862:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassTypeAccess().getClassrefClassIDTerminalRuleCall_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rule__ClassType__ClassrefAssignment5755); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassTypeAccess().getClassrefClassIDTerminalRuleCall_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassTypeAccess().getClassrefClassCrossReference_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassType__ClassrefAssignment"


    // $ANTLR start "rule__Class__NameAssignment_1"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2873:1: rule__Class__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Class__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2877:1: ( ( RULE_ID ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2878:1: ( RULE_ID )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2878:1: ( RULE_ID )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2879:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassAccess().getNameIDTerminalRuleCall_1_0()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rule__Class__NameAssignment_15790); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassAccess().getNameIDTerminalRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__NameAssignment_1"


    // $ANTLR start "rule__Class__ExtendsAssignment_2_1"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2888:1: rule__Class__ExtendsAssignment_2_1 : ( ( RULE_ID ) ) ;
    public final void rule__Class__ExtendsAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2892:1: ( ( ( RULE_ID ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2893:1: ( ( RULE_ID ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2893:1: ( ( RULE_ID ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2894:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassAccess().getExtendsClassCrossReference_2_1_0()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2895:1: ( RULE_ID )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2896:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassAccess().getExtendsClassIDTerminalRuleCall_2_1_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rule__Class__ExtendsAssignment_2_15825); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassAccess().getExtendsClassIDTerminalRuleCall_2_1_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassAccess().getExtendsClassCrossReference_2_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__ExtendsAssignment_2_1"


    // $ANTLR start "rule__Class__FieldsAssignment_4"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2907:1: rule__Class__FieldsAssignment_4 : ( ruleField ) ;
    public final void rule__Class__FieldsAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2911:1: ( ( ruleField ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2912:1: ( ruleField )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2912:1: ( ruleField )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2913:1: ruleField
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassAccess().getFieldsFieldParserRuleCall_4_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleField_in_rule__Class__FieldsAssignment_45860);
            ruleField();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassAccess().getFieldsFieldParserRuleCall_4_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__FieldsAssignment_4"


    // $ANTLR start "rule__Class__MethodsAssignment_5"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2922:1: rule__Class__MethodsAssignment_5 : ( ruleMethod ) ;
    public final void rule__Class__MethodsAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2926:1: ( ( ruleMethod ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2927:1: ( ruleMethod )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2927:1: ( ruleMethod )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2928:1: ruleMethod
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassAccess().getMethodsMethodParserRuleCall_5_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleMethod_in_rule__Class__MethodsAssignment_55891);
            ruleMethod();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassAccess().getMethodsMethodParserRuleCall_5_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__MethodsAssignment_5"


    // $ANTLR start "rule__Field__TypeAssignment_0"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2937:1: rule__Field__TypeAssignment_0 : ( ruleType ) ;
    public final void rule__Field__TypeAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2941:1: ( ( ruleType ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2942:1: ( ruleType )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2942:1: ( ruleType )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2943:1: ruleType
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFieldAccess().getTypeTypeParserRuleCall_0_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleType_in_rule__Field__TypeAssignment_05922);
            ruleType();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFieldAccess().getTypeTypeParserRuleCall_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Field__TypeAssignment_0"


    // $ANTLR start "rule__Field__NameAssignment_1"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2952:1: rule__Field__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Field__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2956:1: ( ( RULE_ID ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2957:1: ( RULE_ID )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2957:1: ( RULE_ID )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2958:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFieldAccess().getNameIDTerminalRuleCall_1_0()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rule__Field__NameAssignment_15953); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFieldAccess().getNameIDTerminalRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Field__NameAssignment_1"


    // $ANTLR start "rule__Parameter__TypeAssignment_0"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2967:1: rule__Parameter__TypeAssignment_0 : ( ruleType ) ;
    public final void rule__Parameter__TypeAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2971:1: ( ( ruleType ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2972:1: ( ruleType )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2972:1: ( ruleType )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2973:1: ruleType
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParameterAccess().getTypeTypeParserRuleCall_0_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleType_in_rule__Parameter__TypeAssignment_05984);
            ruleType();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getParameterAccess().getTypeTypeParserRuleCall_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__TypeAssignment_0"


    // $ANTLR start "rule__Parameter__NameAssignment_1"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2982:1: rule__Parameter__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Parameter__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2986:1: ( ( RULE_ID ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2987:1: ( RULE_ID )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2987:1: ( RULE_ID )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2988:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParameterAccess().getNameIDTerminalRuleCall_1_0()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rule__Parameter__NameAssignment_16015); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getParameterAccess().getNameIDTerminalRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__NameAssignment_1"


    // $ANTLR start "rule__Method__ReturntypeAssignment_0"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:2997:1: rule__Method__ReturntypeAssignment_0 : ( ruleType ) ;
    public final void rule__Method__ReturntypeAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3001:1: ( ( ruleType ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3002:1: ( ruleType )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3002:1: ( ruleType )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3003:1: ruleType
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getReturntypeTypeParserRuleCall_0_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleType_in_rule__Method__ReturntypeAssignment_06046);
            ruleType();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getReturntypeTypeParserRuleCall_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__ReturntypeAssignment_0"


    // $ANTLR start "rule__Method__NameAssignment_1"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3012:1: rule__Method__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Method__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3016:1: ( ( RULE_ID ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3017:1: ( RULE_ID )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3017:1: ( RULE_ID )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3018:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getNameIDTerminalRuleCall_1_0()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rule__Method__NameAssignment_16077); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getNameIDTerminalRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__NameAssignment_1"


    // $ANTLR start "rule__Method__ParamsAssignment_3_0"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3027:1: rule__Method__ParamsAssignment_3_0 : ( ruleParameter ) ;
    public final void rule__Method__ParamsAssignment_3_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3031:1: ( ( ruleParameter ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3032:1: ( ruleParameter )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3032:1: ( ruleParameter )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3033:1: ruleParameter
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getParamsParameterParserRuleCall_3_0_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleParameter_in_rule__Method__ParamsAssignment_3_06108);
            ruleParameter();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getParamsParameterParserRuleCall_3_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__ParamsAssignment_3_0"


    // $ANTLR start "rule__Method__ParamsAssignment_3_1_1"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3042:1: rule__Method__ParamsAssignment_3_1_1 : ( ruleParameter ) ;
    public final void rule__Method__ParamsAssignment_3_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3046:1: ( ( ruleParameter ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3047:1: ( ruleParameter )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3047:1: ( ruleParameter )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3048:1: ruleParameter
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getParamsParameterParserRuleCall_3_1_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleParameter_in_rule__Method__ParamsAssignment_3_1_16139);
            ruleParameter();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getParamsParameterParserRuleCall_3_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__ParamsAssignment_3_1_1"


    // $ANTLR start "rule__Method__BodyAssignment_6"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3057:1: rule__Method__BodyAssignment_6 : ( ruleMethodBody ) ;
    public final void rule__Method__BodyAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3061:1: ( ( ruleMethodBody ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3062:1: ( ruleMethodBody )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3062:1: ( ruleMethodBody )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3063:1: ruleMethodBody
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getBodyMethodBodyParserRuleCall_6_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleMethodBody_in_rule__Method__BodyAssignment_66170);
            ruleMethodBody();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getBodyMethodBodyParserRuleCall_6_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__BodyAssignment_6"


    // $ANTLR start "rule__MethodBody__ExpressionAssignment_1"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3072:1: rule__MethodBody__ExpressionAssignment_1 : ( ruleExpression ) ;
    public final void rule__MethodBody__ExpressionAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3076:1: ( ( ruleExpression ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3077:1: ( ruleExpression )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3077:1: ( ruleExpression )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3078:1: ruleExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodBodyAccess().getExpressionExpressionParserRuleCall_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleExpression_in_rule__MethodBody__ExpressionAssignment_16201);
            ruleExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodBodyAccess().getExpressionExpressionParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodBody__ExpressionAssignment_1"


    // $ANTLR start "rule__Expression__MessageAssignment_1_2"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3087:1: rule__Expression__MessageAssignment_1_2 : ( ruleMessage ) ;
    public final void rule__Expression__MessageAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3091:1: ( ( ruleMessage ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3092:1: ( ruleMessage )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3092:1: ( ruleMessage )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3093:1: ruleMessage
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionAccess().getMessageMessageParserRuleCall_1_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleMessage_in_rule__Expression__MessageAssignment_1_26232);
            ruleMessage();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionAccess().getMessageMessageParserRuleCall_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__MessageAssignment_1_2"


    // $ANTLR start "rule__MethodCall__NameAssignment_0"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3102:1: rule__MethodCall__NameAssignment_0 : ( ( RULE_ID ) ) ;
    public final void rule__MethodCall__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3106:1: ( ( ( RULE_ID ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3107:1: ( ( RULE_ID ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3107:1: ( ( RULE_ID ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3108:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodCallAccess().getNameMethodCrossReference_0_0()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3109:1: ( RULE_ID )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3110:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodCallAccess().getNameMethodIDTerminalRuleCall_0_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rule__MethodCall__NameAssignment_06267); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodCallAccess().getNameMethodIDTerminalRuleCall_0_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodCallAccess().getNameMethodCrossReference_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__NameAssignment_0"


    // $ANTLR start "rule__MethodCall__ArgsAssignment_2_0"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3121:1: rule__MethodCall__ArgsAssignment_2_0 : ( ruleArgument ) ;
    public final void rule__MethodCall__ArgsAssignment_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3125:1: ( ( ruleArgument ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3126:1: ( ruleArgument )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3126:1: ( ruleArgument )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3127:1: ruleArgument
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodCallAccess().getArgsArgumentParserRuleCall_2_0_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleArgument_in_rule__MethodCall__ArgsAssignment_2_06302);
            ruleArgument();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodCallAccess().getArgsArgumentParserRuleCall_2_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__ArgsAssignment_2_0"


    // $ANTLR start "rule__MethodCall__ArgsAssignment_2_1_1"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3136:1: rule__MethodCall__ArgsAssignment_2_1_1 : ( ruleArgument ) ;
    public final void rule__MethodCall__ArgsAssignment_2_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3140:1: ( ( ruleArgument ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3141:1: ( ruleArgument )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3141:1: ( ruleArgument )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3142:1: ruleArgument
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodCallAccess().getArgsArgumentParserRuleCall_2_1_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleArgument_in_rule__MethodCall__ArgsAssignment_2_1_16333);
            ruleArgument();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodCallAccess().getArgsArgumentParserRuleCall_2_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__ArgsAssignment_2_1_1"


    // $ANTLR start "rule__FieldSelection__NameAssignment"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3151:1: rule__FieldSelection__NameAssignment : ( ( RULE_ID ) ) ;
    public final void rule__FieldSelection__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3155:1: ( ( ( RULE_ID ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3156:1: ( ( RULE_ID ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3156:1: ( ( RULE_ID ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3157:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFieldSelectionAccess().getNameFieldCrossReference_0()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3158:1: ( RULE_ID )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3159:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFieldSelectionAccess().getNameFieldIDTerminalRuleCall_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rule__FieldSelection__NameAssignment6368); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFieldSelectionAccess().getNameFieldIDTerminalRuleCall_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFieldSelectionAccess().getNameFieldCrossReference_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FieldSelection__NameAssignment"


    // $ANTLR start "rule__This__VariableAssignment"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3170:1: rule__This__VariableAssignment : ( ( 'this' ) ) ;
    public final void rule__This__VariableAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3174:1: ( ( ( 'this' ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3175:1: ( ( 'this' ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3175:1: ( ( 'this' ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3176:1: ( 'this' )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getThisAccess().getVariableThisKeyword_0()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3177:1: ( 'this' )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3178:1: 'this'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getThisAccess().getVariableThisKeyword_0()); 
            }
            match(input,27,FollowSets000.FOLLOW_27_in_rule__This__VariableAssignment6408); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getThisAccess().getVariableThisKeyword_0()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getThisAccess().getVariableThisKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__This__VariableAssignment"


    // $ANTLR start "rule__Variable__ParamrefAssignment"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3193:1: rule__Variable__ParamrefAssignment : ( ( RULE_ID ) ) ;
    public final void rule__Variable__ParamrefAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3197:1: ( ( ( RULE_ID ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3198:1: ( ( RULE_ID ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3198:1: ( ( RULE_ID ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3199:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAccess().getParamrefParameterCrossReference_0()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3200:1: ( RULE_ID )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3201:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAccess().getParamrefParameterIDTerminalRuleCall_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rule__Variable__ParamrefAssignment6451); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAccess().getParamrefParameterIDTerminalRuleCall_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAccess().getParamrefParameterCrossReference_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable__ParamrefAssignment"


    // $ANTLR start "rule__New__TypeAssignment_1"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3212:1: rule__New__TypeAssignment_1 : ( ruleClassType ) ;
    public final void rule__New__TypeAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3216:1: ( ( ruleClassType ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3217:1: ( ruleClassType )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3217:1: ( ruleClassType )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3218:1: ruleClassType
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNewAccess().getTypeClassTypeParserRuleCall_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleClassType_in_rule__New__TypeAssignment_16486);
            ruleClassType();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNewAccess().getTypeClassTypeParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__TypeAssignment_1"


    // $ANTLR start "rule__New__ArgsAssignment_3_0"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3227:1: rule__New__ArgsAssignment_3_0 : ( ruleArgument ) ;
    public final void rule__New__ArgsAssignment_3_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3231:1: ( ( ruleArgument ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3232:1: ( ruleArgument )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3232:1: ( ruleArgument )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3233:1: ruleArgument
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNewAccess().getArgsArgumentParserRuleCall_3_0_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleArgument_in_rule__New__ArgsAssignment_3_06517);
            ruleArgument();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNewAccess().getArgsArgumentParserRuleCall_3_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__ArgsAssignment_3_0"


    // $ANTLR start "rule__New__ArgsAssignment_3_1_1"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3242:1: rule__New__ArgsAssignment_3_1_1 : ( ruleArgument ) ;
    public final void rule__New__ArgsAssignment_3_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3246:1: ( ( ruleArgument ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3247:1: ( ruleArgument )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3247:1: ( ruleArgument )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3248:1: ruleArgument
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNewAccess().getArgsArgumentParserRuleCall_3_1_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleArgument_in_rule__New__ArgsAssignment_3_1_16548);
            ruleArgument();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNewAccess().getArgsArgumentParserRuleCall_3_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__ArgsAssignment_3_1_1"


    // $ANTLR start "rule__Cast__TypeAssignment_1"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3257:1: rule__Cast__TypeAssignment_1 : ( ruleClassType ) ;
    public final void rule__Cast__TypeAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3261:1: ( ( ruleClassType ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3262:1: ( ruleClassType )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3262:1: ( ruleClassType )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3263:1: ruleClassType
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCastAccess().getTypeClassTypeParserRuleCall_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleClassType_in_rule__Cast__TypeAssignment_16579);
            ruleClassType();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCastAccess().getTypeClassTypeParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cast__TypeAssignment_1"


    // $ANTLR start "rule__Cast__ObjectAssignment_3"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3272:1: rule__Cast__ObjectAssignment_3 : ( ruleTerminalExpression ) ;
    public final void rule__Cast__ObjectAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3276:1: ( ( ruleTerminalExpression ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3277:1: ( ruleTerminalExpression )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3277:1: ( ruleTerminalExpression )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3278:1: ruleTerminalExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCastAccess().getObjectTerminalExpressionParserRuleCall_3_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_ruleTerminalExpression_in_rule__Cast__ObjectAssignment_36610);
            ruleTerminalExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCastAccess().getObjectTerminalExpressionParserRuleCall_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cast__ObjectAssignment_3"


    // $ANTLR start "rule__StringConstant__ConstantAssignment"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3287:1: rule__StringConstant__ConstantAssignment : ( RULE_STRING ) ;
    public final void rule__StringConstant__ConstantAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3291:1: ( ( RULE_STRING ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3292:1: ( RULE_STRING )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3292:1: ( RULE_STRING )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3293:1: RULE_STRING
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStringConstantAccess().getConstantSTRINGTerminalRuleCall_0()); 
            }
            match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_rule__StringConstant__ConstantAssignment6641); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getStringConstantAccess().getConstantSTRINGTerminalRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringConstant__ConstantAssignment"


    // $ANTLR start "rule__IntConstant__ConstantAssignment"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3302:1: rule__IntConstant__ConstantAssignment : ( RULE_INT ) ;
    public final void rule__IntConstant__ConstantAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3306:1: ( ( RULE_INT ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3307:1: ( RULE_INT )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3307:1: ( RULE_INT )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3308:1: RULE_INT
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIntConstantAccess().getConstantINTTerminalRuleCall_0()); 
            }
            match(input,RULE_INT,FollowSets000.FOLLOW_RULE_INT_in_rule__IntConstant__ConstantAssignment6672); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getIntConstantAccess().getConstantINTTerminalRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntConstant__ConstantAssignment"


    // $ANTLR start "rule__BoolConstant__ConstantAssignment"
    // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3317:1: rule__BoolConstant__ConstantAssignment : ( ( rule__BoolConstant__ConstantAlternatives_0 ) ) ;
    public final void rule__BoolConstant__ConstantAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3321:1: ( ( ( rule__BoolConstant__ConstantAlternatives_0 ) ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3322:1: ( ( rule__BoolConstant__ConstantAlternatives_0 ) )
            {
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3322:1: ( ( rule__BoolConstant__ConstantAlternatives_0 ) )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3323:1: ( rule__BoolConstant__ConstantAlternatives_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBoolConstantAccess().getConstantAlternatives_0()); 
            }
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3324:1: ( rule__BoolConstant__ConstantAlternatives_0 )
            // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:3324:2: rule__BoolConstant__ConstantAlternatives_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__BoolConstant__ConstantAlternatives_0_in_rule__BoolConstant__ConstantAssignment6703);
            rule__BoolConstant__ConstantAlternatives_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBoolConstantAccess().getConstantAlternatives_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BoolConstant__ConstantAssignment"

    // $ANTLR start synpred8_InternalFJ
    public final void synpred8_InternalFJ_fragment() throws RecognitionException {   
        // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:837:6: ( ( ( ruleCast ) ) )
        // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:837:6: ( ( ruleCast ) )
        {
        // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:837:6: ( ( ruleCast ) )
        // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:838:1: ( ruleCast )
        {
        if ( state.backtracking==0 ) {
           before(grammarAccess.getTerminalExpressionAccess().getCastParserRuleCall_3()); 
        }
        // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:839:1: ( ruleCast )
        // ../fjava.xtext.ui/src-gen/fjava/xtext/ui/contentassist/antlr/internal/InternalFJ.g:839:3: ruleCast
        {
        pushFollow(FollowSets000.FOLLOW_ruleCast_in_synpred8_InternalFJ1743);
        ruleCast();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }
    }
    // $ANTLR end synpred8_InternalFJ

    // Delegated rules

    public final boolean synpred8_InternalFJ() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred8_InternalFJ_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


    protected DFA4 dfa4 = new DFA4(this);
    static final String DFA4_eotS =
        "\13\uffff";
    static final String DFA4_eofS =
        "\13\uffff";
    static final String DFA4_minS =
        "\1\4\3\uffff\1\0\6\uffff";
    static final String DFA4_maxS =
        "\1\33\3\uffff\1\0\6\uffff";
    static final String DFA4_acceptS =
        "\1\uffff\1\1\1\2\1\3\1\uffff\1\5\3\uffff\1\4\1\6";
    static final String DFA4_specialS =
        "\4\uffff\1\0\6\uffff}>";
    static final String[] DFA4_transitionS = {
            "\1\2\2\5\7\uffff\2\5\5\uffff\1\4\4\uffff\1\3\1\1",
            "",
            "",
            "",
            "\1\uffff",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA4_eot = DFA.unpackEncodedString(DFA4_eotS);
    static final short[] DFA4_eof = DFA.unpackEncodedString(DFA4_eofS);
    static final char[] DFA4_min = DFA.unpackEncodedStringToUnsignedChars(DFA4_minS);
    static final char[] DFA4_max = DFA.unpackEncodedStringToUnsignedChars(DFA4_maxS);
    static final short[] DFA4_accept = DFA.unpackEncodedString(DFA4_acceptS);
    static final short[] DFA4_special = DFA.unpackEncodedString(DFA4_specialS);
    static final short[][] DFA4_transition;

    static {
        int numStates = DFA4_transitionS.length;
        DFA4_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA4_transition[i] = DFA.unpackEncodedString(DFA4_transitionS[i]);
        }
    }

    class DFA4 extends DFA {

        public DFA4(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 4;
            this.eot = DFA4_eot;
            this.eof = DFA4_eof;
            this.min = DFA4_min;
            this.max = DFA4_max;
            this.accept = DFA4_accept;
            this.special = DFA4_special;
            this.transition = DFA4_transition;
        }
        public String getDescription() {
            return "814:1: rule__TerminalExpression__Alternatives : ( ( ruleThis ) | ( ruleVariable ) | ( ruleNew ) | ( ( ruleCast ) ) | ( ruleConstant ) | ( ruleParen ) );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA4_4 = input.LA(1);

                         
                        int index4_4 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred8_InternalFJ()) ) {s = 9;}

                        else if ( (true) ) {s = 10;}

                         
                        input.seek(index4_4);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 4, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_ruleProgram_in_entryRuleProgram67 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleProgram74 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Program__Group__0_in_ruleProgram100 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleType_in_entryRuleType127 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleType134 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Type__Alternatives_in_ruleType160 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBasicType_in_entryRuleBasicType187 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleBasicType194 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__BasicType__BasicAssignment_in_ruleBasicType220 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClassType_in_entryRuleClassType247 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleClassType254 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ClassType__ClassrefAssignment_in_ruleClassType280 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClass_in_entryRuleClass309 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleClass316 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__Group__0_in_ruleClass342 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleField_in_entryRuleField369 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleField376 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Field__Group__0_in_ruleField402 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleParameter_in_entryRuleParameter429 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleParameter436 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Parameter__Group__0_in_ruleParameter462 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleMethod_in_entryRuleMethod489 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleMethod496 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Method__Group__0_in_ruleMethod522 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleMethodBody_in_entryRuleMethodBody549 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleMethodBody556 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MethodBody__Group__0_in_ruleMethodBody582 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleExpression_in_entryRuleExpression609 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleExpression616 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Expression__Group__0_in_ruleExpression642 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleMessage_in_entryRuleMessage669 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleMessage676 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Message__Alternatives_in_ruleMessage702 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleMethodCall_in_entryRuleMethodCall729 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleMethodCall736 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MethodCall__Group__0_in_ruleMethodCall762 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFieldSelection_in_entryRuleFieldSelection789 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFieldSelection796 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FieldSelection__NameAssignment_in_ruleFieldSelection822 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleTerminalExpression_in_entryRuleTerminalExpression849 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleTerminalExpression856 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__TerminalExpression__Alternatives_in_ruleTerminalExpression882 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleThis_in_entryRuleThis909 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleThis916 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__This__VariableAssignment_in_ruleThis942 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleVariable_in_entryRuleVariable969 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleVariable976 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Variable__ParamrefAssignment_in_ruleVariable1002 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleNew_in_entryRuleNew1029 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleNew1036 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__New__Group__0_in_ruleNew1062 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleCast_in_entryRuleCast1089 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleCast1096 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Cast__Group__0_in_ruleCast1122 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleParen_in_entryRuleParen1149 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleParen1156 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Paren__Group__0_in_ruleParen1182 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleConstant_in_entryRuleConstant1209 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleConstant1216 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Constant__Alternatives_in_ruleConstant1242 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleStringConstant_in_entryRuleStringConstant1269 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleStringConstant1276 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__StringConstant__ConstantAssignment_in_ruleStringConstant1302 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleIntConstant_in_entryRuleIntConstant1329 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleIntConstant1336 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__IntConstant__ConstantAssignment_in_ruleIntConstant1362 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBoolConstant_in_entryRuleBoolConstant1389 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleBoolConstant1396 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__BoolConstant__ConstantAssignment_in_ruleBoolConstant1422 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleArgument_in_entryRuleArgument1449 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleArgument1456 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleExpression_in_ruleArgument1482 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBasicType_in_rule__Type__Alternatives1517 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClassType_in_rule__Type__Alternatives1534 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_11_in_rule__BasicType__BasicAlternatives_01567 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_12_in_rule__BasicType__BasicAlternatives_01587 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_13_in_rule__BasicType__BasicAlternatives_01607 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleMethodCall_in_rule__Message__Alternatives1642 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFieldSelection_in_rule__Message__Alternatives1659 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleThis_in_rule__TerminalExpression__Alternatives1691 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleVariable_in_rule__TerminalExpression__Alternatives1708 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleNew_in_rule__TerminalExpression__Alternatives1725 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleCast_in_rule__TerminalExpression__Alternatives1743 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleConstant_in_rule__TerminalExpression__Alternatives1761 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleParen_in_rule__TerminalExpression__Alternatives1778 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleIntConstant_in_rule__Constant__Alternatives1810 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBoolConstant_in_rule__Constant__Alternatives1827 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleStringConstant_in_rule__Constant__Alternatives1844 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_14_in_rule__BoolConstant__ConstantAlternatives_01877 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_15_in_rule__BoolConstant__ConstantAlternatives_01897 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Program__Group__0__Impl_in_rule__Program__Group__01929 = new BitSet(new long[]{0x000000000C20C070L});
        public static final BitSet FOLLOW_rule__Program__Group__1_in_rule__Program__Group__01932 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Program__ClassesAssignment_0_in_rule__Program__Group__0__Impl1959 = new BitSet(new long[]{0x0000000000010002L});
        public static final BitSet FOLLOW_rule__Program__Group__1__Impl_in_rule__Program__Group__11990 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Program__MainAssignment_1_in_rule__Program__Group__1__Impl2017 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__Group__0__Impl_in_rule__Class__Group__02052 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_rule__Class__Group__1_in_rule__Class__Group__02055 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_16_in_rule__Class__Group__0__Impl2083 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__Group__1__Impl_in_rule__Class__Group__12114 = new BitSet(new long[]{0x00000000000A0000L});
        public static final BitSet FOLLOW_rule__Class__Group__2_in_rule__Class__Group__12117 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__NameAssignment_1_in_rule__Class__Group__1__Impl2144 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__Group__2__Impl_in_rule__Class__Group__22174 = new BitSet(new long[]{0x00000000000A0000L});
        public static final BitSet FOLLOW_rule__Class__Group__3_in_rule__Class__Group__22177 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__Group_2__0_in_rule__Class__Group__2__Impl2204 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__Group__3__Impl_in_rule__Class__Group__32235 = new BitSet(new long[]{0x0000000000043810L});
        public static final BitSet FOLLOW_rule__Class__Group__4_in_rule__Class__Group__32238 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_17_in_rule__Class__Group__3__Impl2266 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__Group__4__Impl_in_rule__Class__Group__42297 = new BitSet(new long[]{0x0000000000043810L});
        public static final BitSet FOLLOW_rule__Class__Group__5_in_rule__Class__Group__42300 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__FieldsAssignment_4_in_rule__Class__Group__4__Impl2327 = new BitSet(new long[]{0x0000000000003812L});
        public static final BitSet FOLLOW_rule__Class__Group__5__Impl_in_rule__Class__Group__52358 = new BitSet(new long[]{0x0000000000043810L});
        public static final BitSet FOLLOW_rule__Class__Group__6_in_rule__Class__Group__52361 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__MethodsAssignment_5_in_rule__Class__Group__5__Impl2388 = new BitSet(new long[]{0x0000000000003812L});
        public static final BitSet FOLLOW_rule__Class__Group__6__Impl_in_rule__Class__Group__62419 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_18_in_rule__Class__Group__6__Impl2447 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__Group_2__0__Impl_in_rule__Class__Group_2__02492 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_rule__Class__Group_2__1_in_rule__Class__Group_2__02495 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_19_in_rule__Class__Group_2__0__Impl2523 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__Group_2__1__Impl_in_rule__Class__Group_2__12554 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Class__ExtendsAssignment_2_1_in_rule__Class__Group_2__1__Impl2581 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Field__Group__0__Impl_in_rule__Field__Group__02615 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_rule__Field__Group__1_in_rule__Field__Group__02618 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Field__TypeAssignment_0_in_rule__Field__Group__0__Impl2645 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Field__Group__1__Impl_in_rule__Field__Group__12675 = new BitSet(new long[]{0x0000000000100000L});
        public static final BitSet FOLLOW_rule__Field__Group__2_in_rule__Field__Group__12678 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Field__NameAssignment_1_in_rule__Field__Group__1__Impl2705 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Field__Group__2__Impl_in_rule__Field__Group__22735 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_20_in_rule__Field__Group__2__Impl2763 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Parameter__Group__0__Impl_in_rule__Parameter__Group__02800 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_rule__Parameter__Group__1_in_rule__Parameter__Group__02803 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Parameter__TypeAssignment_0_in_rule__Parameter__Group__0__Impl2830 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Parameter__Group__1__Impl_in_rule__Parameter__Group__12860 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Parameter__NameAssignment_1_in_rule__Parameter__Group__1__Impl2887 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Method__Group__0__Impl_in_rule__Method__Group__02921 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_rule__Method__Group__1_in_rule__Method__Group__02924 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Method__ReturntypeAssignment_0_in_rule__Method__Group__0__Impl2951 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Method__Group__1__Impl_in_rule__Method__Group__12981 = new BitSet(new long[]{0x0000000000200000L});
        public static final BitSet FOLLOW_rule__Method__Group__2_in_rule__Method__Group__12984 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Method__NameAssignment_1_in_rule__Method__Group__1__Impl3011 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Method__Group__2__Impl_in_rule__Method__Group__23041 = new BitSet(new long[]{0x0000000000403810L});
        public static final BitSet FOLLOW_rule__Method__Group__3_in_rule__Method__Group__23044 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_21_in_rule__Method__Group__2__Impl3072 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Method__Group__3__Impl_in_rule__Method__Group__33103 = new BitSet(new long[]{0x0000000000403810L});
        public static final BitSet FOLLOW_rule__Method__Group__4_in_rule__Method__Group__33106 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Method__Group_3__0_in_rule__Method__Group__3__Impl3133 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Method__Group__4__Impl_in_rule__Method__Group__43164 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_rule__Method__Group__5_in_rule__Method__Group__43167 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_22_in_rule__Method__Group__4__Impl3195 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Method__Group__5__Impl_in_rule__Method__Group__53226 = new BitSet(new long[]{0x0000000001000000L});
        public static final BitSet FOLLOW_rule__Method__Group__6_in_rule__Method__Group__53229 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_17_in_rule__Method__Group__5__Impl3257 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Method__Group__6__Impl_in_rule__Method__Group__63288 = new BitSet(new long[]{0x0000000000040000L});
        public static final BitSet FOLLOW_rule__Method__Group__7_in_rule__Method__Group__63291 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Method__BodyAssignment_6_in_rule__Method__Group__6__Impl3318 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Method__Group__7__Impl_in_rule__Method__Group__73348 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_18_in_rule__Method__Group__7__Impl3376 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Method__Group_3__0__Impl_in_rule__Method__Group_3__03423 = new BitSet(new long[]{0x0000000000800000L});
        public static final BitSet FOLLOW_rule__Method__Group_3__1_in_rule__Method__Group_3__03426 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Method__ParamsAssignment_3_0_in_rule__Method__Group_3__0__Impl3453 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Method__Group_3__1__Impl_in_rule__Method__Group_3__13483 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Method__Group_3_1__0_in_rule__Method__Group_3__1__Impl3510 = new BitSet(new long[]{0x0000000000800002L});
        public static final BitSet FOLLOW_rule__Method__Group_3_1__0__Impl_in_rule__Method__Group_3_1__03545 = new BitSet(new long[]{0x0000000000003810L});
        public static final BitSet FOLLOW_rule__Method__Group_3_1__1_in_rule__Method__Group_3_1__03548 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_23_in_rule__Method__Group_3_1__0__Impl3576 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Method__Group_3_1__1__Impl_in_rule__Method__Group_3_1__13607 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Method__ParamsAssignment_3_1_1_in_rule__Method__Group_3_1__1__Impl3634 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MethodBody__Group__0__Impl_in_rule__MethodBody__Group__03668 = new BitSet(new long[]{0x000000000C20C070L});
        public static final BitSet FOLLOW_rule__MethodBody__Group__1_in_rule__MethodBody__Group__03671 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_24_in_rule__MethodBody__Group__0__Impl3699 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MethodBody__Group__1__Impl_in_rule__MethodBody__Group__13730 = new BitSet(new long[]{0x0000000000100000L});
        public static final BitSet FOLLOW_rule__MethodBody__Group__2_in_rule__MethodBody__Group__13733 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MethodBody__ExpressionAssignment_1_in_rule__MethodBody__Group__1__Impl3760 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MethodBody__Group__2__Impl_in_rule__MethodBody__Group__23790 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_20_in_rule__MethodBody__Group__2__Impl3818 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Expression__Group__0__Impl_in_rule__Expression__Group__03855 = new BitSet(new long[]{0x0000000002000000L});
        public static final BitSet FOLLOW_rule__Expression__Group__1_in_rule__Expression__Group__03858 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleTerminalExpression_in_rule__Expression__Group__0__Impl3885 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Expression__Group__1__Impl_in_rule__Expression__Group__13914 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Expression__Group_1__0_in_rule__Expression__Group__1__Impl3941 = new BitSet(new long[]{0x0000000002000002L});
        public static final BitSet FOLLOW_rule__Expression__Group_1__0__Impl_in_rule__Expression__Group_1__03976 = new BitSet(new long[]{0x0000000002000000L});
        public static final BitSet FOLLOW_rule__Expression__Group_1__1_in_rule__Expression__Group_1__03979 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Expression__Group_1__1__Impl_in_rule__Expression__Group_1__14037 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_rule__Expression__Group_1__2_in_rule__Expression__Group_1__14040 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_25_in_rule__Expression__Group_1__1__Impl4068 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Expression__Group_1__2__Impl_in_rule__Expression__Group_1__24099 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Expression__MessageAssignment_1_2_in_rule__Expression__Group_1__2__Impl4126 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MethodCall__Group__0__Impl_in_rule__MethodCall__Group__04162 = new BitSet(new long[]{0x0000000000200000L});
        public static final BitSet FOLLOW_rule__MethodCall__Group__1_in_rule__MethodCall__Group__04165 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MethodCall__NameAssignment_0_in_rule__MethodCall__Group__0__Impl4192 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MethodCall__Group__1__Impl_in_rule__MethodCall__Group__14222 = new BitSet(new long[]{0x000000000C60C070L});
        public static final BitSet FOLLOW_rule__MethodCall__Group__2_in_rule__MethodCall__Group__14225 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_21_in_rule__MethodCall__Group__1__Impl4253 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MethodCall__Group__2__Impl_in_rule__MethodCall__Group__24284 = new BitSet(new long[]{0x000000000C60C070L});
        public static final BitSet FOLLOW_rule__MethodCall__Group__3_in_rule__MethodCall__Group__24287 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MethodCall__Group_2__0_in_rule__MethodCall__Group__2__Impl4314 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MethodCall__Group__3__Impl_in_rule__MethodCall__Group__34345 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_22_in_rule__MethodCall__Group__3__Impl4373 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MethodCall__Group_2__0__Impl_in_rule__MethodCall__Group_2__04412 = new BitSet(new long[]{0x0000000000800000L});
        public static final BitSet FOLLOW_rule__MethodCall__Group_2__1_in_rule__MethodCall__Group_2__04415 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MethodCall__ArgsAssignment_2_0_in_rule__MethodCall__Group_2__0__Impl4442 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MethodCall__Group_2__1__Impl_in_rule__MethodCall__Group_2__14472 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MethodCall__Group_2_1__0_in_rule__MethodCall__Group_2__1__Impl4499 = new BitSet(new long[]{0x0000000000800002L});
        public static final BitSet FOLLOW_rule__MethodCall__Group_2_1__0__Impl_in_rule__MethodCall__Group_2_1__04534 = new BitSet(new long[]{0x000000000C20C070L});
        public static final BitSet FOLLOW_rule__MethodCall__Group_2_1__1_in_rule__MethodCall__Group_2_1__04537 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_23_in_rule__MethodCall__Group_2_1__0__Impl4565 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MethodCall__Group_2_1__1__Impl_in_rule__MethodCall__Group_2_1__14596 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MethodCall__ArgsAssignment_2_1_1_in_rule__MethodCall__Group_2_1__1__Impl4623 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__New__Group__0__Impl_in_rule__New__Group__04657 = new BitSet(new long[]{0x0000000000003810L});
        public static final BitSet FOLLOW_rule__New__Group__1_in_rule__New__Group__04660 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_26_in_rule__New__Group__0__Impl4688 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__New__Group__1__Impl_in_rule__New__Group__14719 = new BitSet(new long[]{0x0000000000200000L});
        public static final BitSet FOLLOW_rule__New__Group__2_in_rule__New__Group__14722 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__New__TypeAssignment_1_in_rule__New__Group__1__Impl4749 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__New__Group__2__Impl_in_rule__New__Group__24779 = new BitSet(new long[]{0x000000000C60C070L});
        public static final BitSet FOLLOW_rule__New__Group__3_in_rule__New__Group__24782 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_21_in_rule__New__Group__2__Impl4810 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__New__Group__3__Impl_in_rule__New__Group__34841 = new BitSet(new long[]{0x000000000C60C070L});
        public static final BitSet FOLLOW_rule__New__Group__4_in_rule__New__Group__34844 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__New__Group_3__0_in_rule__New__Group__3__Impl4871 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__New__Group__4__Impl_in_rule__New__Group__44902 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_22_in_rule__New__Group__4__Impl4930 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__New__Group_3__0__Impl_in_rule__New__Group_3__04971 = new BitSet(new long[]{0x0000000000800000L});
        public static final BitSet FOLLOW_rule__New__Group_3__1_in_rule__New__Group_3__04974 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__New__ArgsAssignment_3_0_in_rule__New__Group_3__0__Impl5001 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__New__Group_3__1__Impl_in_rule__New__Group_3__15031 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__New__Group_3_1__0_in_rule__New__Group_3__1__Impl5058 = new BitSet(new long[]{0x0000000000800002L});
        public static final BitSet FOLLOW_rule__New__Group_3_1__0__Impl_in_rule__New__Group_3_1__05093 = new BitSet(new long[]{0x000000000C20C070L});
        public static final BitSet FOLLOW_rule__New__Group_3_1__1_in_rule__New__Group_3_1__05096 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_23_in_rule__New__Group_3_1__0__Impl5124 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__New__Group_3_1__1__Impl_in_rule__New__Group_3_1__15155 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__New__ArgsAssignment_3_1_1_in_rule__New__Group_3_1__1__Impl5182 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Cast__Group__0__Impl_in_rule__Cast__Group__05216 = new BitSet(new long[]{0x0000000000003810L});
        public static final BitSet FOLLOW_rule__Cast__Group__1_in_rule__Cast__Group__05219 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_21_in_rule__Cast__Group__0__Impl5247 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Cast__Group__1__Impl_in_rule__Cast__Group__15278 = new BitSet(new long[]{0x0000000000400000L});
        public static final BitSet FOLLOW_rule__Cast__Group__2_in_rule__Cast__Group__15281 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Cast__TypeAssignment_1_in_rule__Cast__Group__1__Impl5308 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Cast__Group__2__Impl_in_rule__Cast__Group__25338 = new BitSet(new long[]{0x000000000C20C070L});
        public static final BitSet FOLLOW_rule__Cast__Group__3_in_rule__Cast__Group__25341 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_22_in_rule__Cast__Group__2__Impl5369 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Cast__Group__3__Impl_in_rule__Cast__Group__35400 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Cast__ObjectAssignment_3_in_rule__Cast__Group__3__Impl5427 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Paren__Group__0__Impl_in_rule__Paren__Group__05465 = new BitSet(new long[]{0x000000000C20C070L});
        public static final BitSet FOLLOW_rule__Paren__Group__1_in_rule__Paren__Group__05468 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_21_in_rule__Paren__Group__0__Impl5496 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Paren__Group__1__Impl_in_rule__Paren__Group__15527 = new BitSet(new long[]{0x0000000000400000L});
        public static final BitSet FOLLOW_rule__Paren__Group__2_in_rule__Paren__Group__15530 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleExpression_in_rule__Paren__Group__1__Impl5557 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Paren__Group__2__Impl_in_rule__Paren__Group__25586 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_22_in_rule__Paren__Group__2__Impl5614 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClass_in_rule__Program__ClassesAssignment_05656 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleExpression_in_rule__Program__MainAssignment_15687 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__BasicType__BasicAlternatives_0_in_rule__BasicType__BasicAssignment5718 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rule__ClassType__ClassrefAssignment5755 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rule__Class__NameAssignment_15790 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rule__Class__ExtendsAssignment_2_15825 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleField_in_rule__Class__FieldsAssignment_45860 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleMethod_in_rule__Class__MethodsAssignment_55891 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleType_in_rule__Field__TypeAssignment_05922 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rule__Field__NameAssignment_15953 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleType_in_rule__Parameter__TypeAssignment_05984 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rule__Parameter__NameAssignment_16015 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleType_in_rule__Method__ReturntypeAssignment_06046 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rule__Method__NameAssignment_16077 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleParameter_in_rule__Method__ParamsAssignment_3_06108 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleParameter_in_rule__Method__ParamsAssignment_3_1_16139 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleMethodBody_in_rule__Method__BodyAssignment_66170 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleExpression_in_rule__MethodBody__ExpressionAssignment_16201 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleMessage_in_rule__Expression__MessageAssignment_1_26232 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rule__MethodCall__NameAssignment_06267 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleArgument_in_rule__MethodCall__ArgsAssignment_2_06302 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleArgument_in_rule__MethodCall__ArgsAssignment_2_1_16333 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rule__FieldSelection__NameAssignment6368 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_27_in_rule__This__VariableAssignment6408 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rule__Variable__ParamrefAssignment6451 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClassType_in_rule__New__TypeAssignment_16486 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleArgument_in_rule__New__ArgsAssignment_3_06517 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleArgument_in_rule__New__ArgsAssignment_3_1_16548 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleClassType_in_rule__Cast__TypeAssignment_16579 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleTerminalExpression_in_rule__Cast__ObjectAssignment_36610 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_STRING_in_rule__StringConstant__ConstantAssignment6641 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_INT_in_rule__IntConstant__ConstantAssignment6672 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__BoolConstant__ConstantAlternatives_0_in_rule__BoolConstant__ConstantAssignment6703 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleCast_in_synpred8_InternalFJ1743 = new BitSet(new long[]{0x0000000000000002L});
    }


}