/**
 */
package fjava;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Message</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fjava.FjavaPackage#getMessage()
 * @model
 * @generated
 */
public interface Message extends EObject {
} // Message
