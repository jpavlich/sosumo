/**
 */
package fjava;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fjava.FjavaPackage#getParameter()
 * @model
 * @generated
 */
public interface Parameter extends TypedElement {
} // Parameter
