/**
 */
package fjava;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Method Call</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fjava.MethodCall#getName <em>Name</em>}</li>
 *   <li>{@link fjava.MethodCall#getArgs <em>Args</em>}</li>
 * </ul>
 *
 * @see fjava.FjavaPackage#getMethodCall()
 * @model
 * @generated
 */
public interface MethodCall extends Message {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' reference.
	 * @see #setName(Method)
	 * @see fjava.FjavaPackage#getMethodCall_Name()
	 * @model
	 * @generated
	 */
	Method getName();

	/**
	 * Sets the value of the '{@link fjava.MethodCall#getName <em>Name</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' reference.
	 * @see #getName()
	 * @generated
	 */
	void setName(Method value);

	/**
	 * Returns the value of the '<em><b>Args</b></em>' containment reference list.
	 * The list contents are of type {@link fjava.Argument}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Args</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Args</em>' containment reference list.
	 * @see fjava.FjavaPackage#getMethodCall_Args()
	 * @model containment="true"
	 * @generated
	 */
	EList<Argument> getArgs();

} // MethodCall
