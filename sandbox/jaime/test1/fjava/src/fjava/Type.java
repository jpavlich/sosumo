/**
 */
package fjava;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fjava.FjavaPackage#getType()
 * @model
 * @generated
 */
public interface Type extends EObject {
} // Type
