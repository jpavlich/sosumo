/**
 */
package fjava;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fjava.FjavaPackage#getExpression()
 * @model
 * @generated
 */
public interface Expression extends Argument {
} // Expression
