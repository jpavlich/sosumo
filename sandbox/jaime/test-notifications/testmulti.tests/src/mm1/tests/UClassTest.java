/**
 */
package mm1.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import mm1.Mm1Factory;
import mm1.UClass;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>UClass</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class UClassTest extends TestCase {

	/**
	 * The fixture for this UClass test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UClass fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UClassTest.class);
	}

	/**
	 * Constructs a new UClass test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UClassTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this UClass test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(UClass fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this UClass test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UClass getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(Mm1Factory.eINSTANCE.createUClass());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //UClassTest
