/**
 */
package mm2.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import mm2.JavaProgram;
import mm2.Mm2Factory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Java Program</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class JavaProgramTest extends TestCase {

	/**
	 * The fixture for this Java Program test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JavaProgram fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(JavaProgramTest.class);
	}

	/**
	 * Constructs a new Java Program test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JavaProgramTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Java Program test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(JavaProgram fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Java Program test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JavaProgram getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(Mm2Factory.eINSTANCE.createJavaProgram());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //JavaProgramTest
