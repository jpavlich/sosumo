package testmulti.adapter;

import org.eclipse.emf.ecore.EObject;

public aspect AdapterAspect {
	private CentralModelAdapter adapter;
	

	public AdapterAspect() {
		adapter = new CentralModelAdapter();
	}

	/**
	 * Adds code to insert Adapters into every EObject created
	 * 
	 * @return
	 */
	Object around() : execution(EObject+ *.*.create*()) && !cflow(adviceexecution()) {

		EObject obj = (EObject) proceed();
		obj.eAdapters().add(adapter);
		return obj;
	}

}