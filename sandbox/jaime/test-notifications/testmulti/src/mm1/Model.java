/**
 */
package mm1;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EModelElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mm1.Model#getUclass <em>Uclass</em>}</li>
 * </ul>
 *
 * @see mm1.Mm1Package#getModel()
 * @model
 * @generated
 */
public interface Model extends EModelElement {
	/**
	 * Returns the value of the '<em><b>Uclass</b></em>' containment reference list.
	 * The list contents are of type {@link mm1.UClass}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Uclass</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uclass</em>' containment reference list.
	 * @see mm1.Mm1Package#getModel_Uclass()
	 * @model containment="true"
	 * @generated
	 */
	EList<UClass> getUclass();

} // Model
