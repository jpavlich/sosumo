/**
 */
package mm1;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UClass</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see mm1.Mm1Package#getUClass()
 * @model
 * @generated
 */
public interface UClass extends EObject {
} // UClass
