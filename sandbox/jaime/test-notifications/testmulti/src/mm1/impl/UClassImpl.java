/**
 */
package mm1.impl;

import mm1.Mm1Package;
import mm1.UClass;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>UClass</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class UClassImpl extends MinimalEObjectImpl.Container implements UClass {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UClassImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Mm1Package.Literals.UCLASS;
	}

} //UClassImpl
