/**
 */
package mm1.impl;

import mm1.Mm1Factory;
import mm1.Mm1Package;
import mm1.Model;
import mm1.UClass;

import mm2.Mm2Package;
import mm2.impl.Mm2PackageImpl;
import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Mm1PackageImpl extends EPackageImpl implements Mm1Package {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass uClassEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modelEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see mm1.Mm1Package#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private Mm1PackageImpl() {
		super(eNS_URI, Mm1Factory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link Mm1Package#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static Mm1Package init() {
		if (isInited) return (Mm1Package)EPackage.Registry.INSTANCE.getEPackage(Mm1Package.eNS_URI);

		// Obtain or create and register package
		Mm1PackageImpl theMm1Package = (Mm1PackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof Mm1PackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new Mm1PackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		Mm2PackageImpl theMm2Package = (Mm2PackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Mm2Package.eNS_URI) instanceof Mm2PackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Mm2Package.eNS_URI) : Mm2Package.eINSTANCE);

		// Create package meta-data objects
		theMm1Package.createPackageContents();
		theMm2Package.createPackageContents();

		// Initialize created meta-data
		theMm1Package.initializePackageContents();
		theMm2Package.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theMm1Package.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(Mm1Package.eNS_URI, theMm1Package);
		return theMm1Package;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUClass() {
		return uClassEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getModel() {
		return modelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModel_Uclass() {
		return (EReference)modelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Mm1Factory getMm1Factory() {
		return (Mm1Factory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		uClassEClass = createEClass(UCLASS);

		modelEClass = createEClass(MODEL);
		createEReference(modelEClass, MODEL__UCLASS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		modelEClass.getESuperTypes().add(theEcorePackage.getEModelElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(uClassEClass, UClass.class, "UClass", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(modelEClass, Model.class, "Model", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getModel_Uclass(), this.getUClass(), null, "uclass", null, 0, -1, Model.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// jclass
		createJclassAnnotations();
		// javaProgram
		createJavaProgramAnnotations();
		// jclasses
		createJclassesAnnotations();
	}

	/**
	 * Initializes the annotations for <b>jclass</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createJclassAnnotations() {
		String source = "jclass";	
		addAnnotation
		  (uClassEClass, 
		   source, 
		   new String[] {
		   },
		   new URI[] {
			 URI.createURI(Mm2Package.eNS_URI).appendFragment("//JavaClass")
		   });
	}

	/**
	 * Initializes the annotations for <b>javaProgram</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createJavaProgramAnnotations() {
		String source = "javaProgram";	
		addAnnotation
		  (modelEClass, 
		   source, 
		   new String[] {
		   },
		   new URI[] {
			 URI.createURI(Mm2Package.eNS_URI).appendFragment("//JavaProgram")
		   });
	}

	/**
	 * Initializes the annotations for <b>jclasses</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createJclassesAnnotations() {
		String source = "jclasses";	
		addAnnotation
		  (getModel_Uclass(), 
		   source, 
		   new String[] {
		   },
		   new URI[] {
			 URI.createURI(Mm2Package.eNS_URI).appendFragment("//JavaProgram/javaclass")
		   });
	}

} //Mm1PackageImpl
