/**
 */
package mm2;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EModelElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Java Program</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mm2.JavaProgram#getJavaclass <em>Javaclass</em>}</li>
 * </ul>
 *
 * @see mm2.Mm2Package#getJavaProgram()
 * @model
 * @generated
 */
public interface JavaProgram extends EModelElement {
	/**
	 * Returns the value of the '<em><b>Javaclass</b></em>' containment reference list.
	 * The list contents are of type {@link mm2.JavaClass}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Javaclass</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Javaclass</em>' containment reference list.
	 * @see mm2.Mm2Package#getJavaProgram_Javaclass()
	 * @model containment="true"
	 * @generated
	 */
	EList<JavaClass> getJavaclass();

} // JavaProgram
