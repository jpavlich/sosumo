/**
 */
package mm2;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Java Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see mm2.Mm2Package#getJavaClass()
 * @model
 * @generated
 */
public interface JavaClass extends EObject {
} // JavaClass
