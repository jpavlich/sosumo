/**
 */
package mm2.impl;

import java.util.Collection;

import mm2.JavaClass;
import mm2.JavaProgram;
import mm2.Mm2Package;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.EModelElementImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Java Program</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mm2.impl.JavaProgramImpl#getJavaclass <em>Javaclass</em>}</li>
 * </ul>
 *
 * @generated
 */
public class JavaProgramImpl extends EModelElementImpl implements JavaProgram {
	/**
	 * The cached value of the '{@link #getJavaclass() <em>Javaclass</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getJavaclass()
	 * @generated
	 * @ordered
	 */
	protected EList<JavaClass> javaclass;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JavaProgramImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Mm2Package.Literals.JAVA_PROGRAM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<JavaClass> getJavaclass() {
		if (javaclass == null) {
			javaclass = new EObjectContainmentEList<JavaClass>(JavaClass.class, this, Mm2Package.JAVA_PROGRAM__JAVACLASS);
		}
		return javaclass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Mm2Package.JAVA_PROGRAM__JAVACLASS:
				return ((InternalEList<?>)getJavaclass()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Mm2Package.JAVA_PROGRAM__JAVACLASS:
				return getJavaclass();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Mm2Package.JAVA_PROGRAM__JAVACLASS:
				getJavaclass().clear();
				getJavaclass().addAll((Collection<? extends JavaClass>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Mm2Package.JAVA_PROGRAM__JAVACLASS:
				getJavaclass().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Mm2Package.JAVA_PROGRAM__JAVACLASS:
				return javaclass != null && !javaclass.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //JavaProgramImpl
