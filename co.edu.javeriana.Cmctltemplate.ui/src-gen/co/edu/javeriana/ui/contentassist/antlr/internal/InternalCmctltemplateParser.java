package co.edu.javeriana.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import co.edu.javeriana.services.CmctltemplateGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalCmctltemplateParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_CHARACTER", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'true'", "'false'", "'import'", "';'", "'\\u00ABtemplate'", "'['", "']'", "'\\u00BB'", "'\\u00ABendTemplate\\u00BB'", "','", "'\\u00AB'", "'if'", "'\\u00ABendIf\\u00BB'", "'for'", "'\\u00ABendFor\\u00BB'", "'.'", "'.*'"
    };
    public static final int RULE_ID=4;
    public static final int T__28=28;
    public static final int T__27=27;
    public static final int T__26=26;
    public static final int T__25=25;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=11;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int RULE_CHARACTER=5;
    public static final int RULE_SL_COMMENT=9;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=8;
    public static final int T__19=19;
    public static final int RULE_STRING=7;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=6;
    public static final int RULE_WS=10;

    // delegates
    // delegators


        public InternalCmctltemplateParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalCmctltemplateParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalCmctltemplateParser.tokenNames; }
    public String getGrammarFileName() { return "../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g"; }


     
     	private CmctltemplateGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(CmctltemplateGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleTemplate"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:60:1: entryRuleTemplate : ruleTemplate EOF ;
    public final void entryRuleTemplate() throws RecognitionException {
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:61:1: ( ruleTemplate EOF )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:62:1: ruleTemplate EOF
            {
             before(grammarAccess.getTemplateRule()); 
            pushFollow(FOLLOW_ruleTemplate_in_entryRuleTemplate61);
            ruleTemplate();

            state._fsp--;

             after(grammarAccess.getTemplateRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleTemplate68); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTemplate"


    // $ANTLR start "ruleTemplate"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:69:1: ruleTemplate : ( ( rule__Template__Group__0 ) ) ;
    public final void ruleTemplate() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:73:2: ( ( ( rule__Template__Group__0 ) ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:74:1: ( ( rule__Template__Group__0 ) )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:74:1: ( ( rule__Template__Group__0 ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:75:1: ( rule__Template__Group__0 )
            {
             before(grammarAccess.getTemplateAccess().getGroup()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:76:1: ( rule__Template__Group__0 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:76:2: rule__Template__Group__0
            {
            pushFollow(FOLLOW_rule__Template__Group__0_in_ruleTemplate94);
            rule__Template__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTemplateAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTemplate"


    // $ANTLR start "entryRuleImport"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:88:1: entryRuleImport : ruleImport EOF ;
    public final void entryRuleImport() throws RecognitionException {
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:89:1: ( ruleImport EOF )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:90:1: ruleImport EOF
            {
             before(grammarAccess.getImportRule()); 
            pushFollow(FOLLOW_ruleImport_in_entryRuleImport121);
            ruleImport();

            state._fsp--;

             after(grammarAccess.getImportRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleImport128); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:97:1: ruleImport : ( ( rule__Import__Group__0 ) ) ;
    public final void ruleImport() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:101:2: ( ( ( rule__Import__Group__0 ) ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:102:1: ( ( rule__Import__Group__0 ) )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:102:1: ( ( rule__Import__Group__0 ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:103:1: ( rule__Import__Group__0 )
            {
             before(grammarAccess.getImportAccess().getGroup()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:104:1: ( rule__Import__Group__0 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:104:2: rule__Import__Group__0
            {
            pushFollow(FOLLOW_rule__Import__Group__0_in_ruleImport154);
            rule__Import__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getImportAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleTemplateFunction"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:116:1: entryRuleTemplateFunction : ruleTemplateFunction EOF ;
    public final void entryRuleTemplateFunction() throws RecognitionException {
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:117:1: ( ruleTemplateFunction EOF )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:118:1: ruleTemplateFunction EOF
            {
             before(grammarAccess.getTemplateFunctionRule()); 
            pushFollow(FOLLOW_ruleTemplateFunction_in_entryRuleTemplateFunction181);
            ruleTemplateFunction();

            state._fsp--;

             after(grammarAccess.getTemplateFunctionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleTemplateFunction188); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTemplateFunction"


    // $ANTLR start "ruleTemplateFunction"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:125:1: ruleTemplateFunction : ( ( rule__TemplateFunction__Group__0 ) ) ;
    public final void ruleTemplateFunction() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:129:2: ( ( ( rule__TemplateFunction__Group__0 ) ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:130:1: ( ( rule__TemplateFunction__Group__0 ) )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:130:1: ( ( rule__TemplateFunction__Group__0 ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:131:1: ( rule__TemplateFunction__Group__0 )
            {
             before(grammarAccess.getTemplateFunctionAccess().getGroup()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:132:1: ( rule__TemplateFunction__Group__0 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:132:2: rule__TemplateFunction__Group__0
            {
            pushFollow(FOLLOW_rule__TemplateFunction__Group__0_in_ruleTemplateFunction214);
            rule__TemplateFunction__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTemplateFunctionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTemplateFunction"


    // $ANTLR start "entryRuleStatement"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:144:1: entryRuleStatement : ruleStatement EOF ;
    public final void entryRuleStatement() throws RecognitionException {
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:145:1: ( ruleStatement EOF )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:146:1: ruleStatement EOF
            {
             before(grammarAccess.getStatementRule()); 
            pushFollow(FOLLOW_ruleStatement_in_entryRuleStatement241);
            ruleStatement();

            state._fsp--;

             after(grammarAccess.getStatementRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleStatement248); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStatement"


    // $ANTLR start "ruleStatement"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:153:1: ruleStatement : ( ( rule__Statement__Alternatives ) ) ;
    public final void ruleStatement() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:157:2: ( ( ( rule__Statement__Alternatives ) ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:158:1: ( ( rule__Statement__Alternatives ) )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:158:1: ( ( rule__Statement__Alternatives ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:159:1: ( rule__Statement__Alternatives )
            {
             before(grammarAccess.getStatementAccess().getAlternatives()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:160:1: ( rule__Statement__Alternatives )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:160:2: rule__Statement__Alternatives
            {
            pushFollow(FOLLOW_rule__Statement__Alternatives_in_ruleStatement274);
            rule__Statement__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getStatementAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStatement"


    // $ANTLR start "entryRuleTemplateStatement"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:172:1: entryRuleTemplateStatement : ruleTemplateStatement EOF ;
    public final void entryRuleTemplateStatement() throws RecognitionException {
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:173:1: ( ruleTemplateStatement EOF )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:174:1: ruleTemplateStatement EOF
            {
             before(grammarAccess.getTemplateStatementRule()); 
            pushFollow(FOLLOW_ruleTemplateStatement_in_entryRuleTemplateStatement301);
            ruleTemplateStatement();

            state._fsp--;

             after(grammarAccess.getTemplateStatementRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleTemplateStatement308); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTemplateStatement"


    // $ANTLR start "ruleTemplateStatement"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:181:1: ruleTemplateStatement : ( ( rule__TemplateStatement__Alternatives ) ) ;
    public final void ruleTemplateStatement() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:185:2: ( ( ( rule__TemplateStatement__Alternatives ) ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:186:1: ( ( rule__TemplateStatement__Alternatives ) )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:186:1: ( ( rule__TemplateStatement__Alternatives ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:187:1: ( rule__TemplateStatement__Alternatives )
            {
             before(grammarAccess.getTemplateStatementAccess().getAlternatives()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:188:1: ( rule__TemplateStatement__Alternatives )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:188:2: rule__TemplateStatement__Alternatives
            {
            pushFollow(FOLLOW_rule__TemplateStatement__Alternatives_in_ruleTemplateStatement334);
            rule__TemplateStatement__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getTemplateStatementAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTemplateStatement"


    // $ANTLR start "entryRuleIfTemplate"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:200:1: entryRuleIfTemplate : ruleIfTemplate EOF ;
    public final void entryRuleIfTemplate() throws RecognitionException {
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:201:1: ( ruleIfTemplate EOF )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:202:1: ruleIfTemplate EOF
            {
             before(grammarAccess.getIfTemplateRule()); 
            pushFollow(FOLLOW_ruleIfTemplate_in_entryRuleIfTemplate361);
            ruleIfTemplate();

            state._fsp--;

             after(grammarAccess.getIfTemplateRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleIfTemplate368); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIfTemplate"


    // $ANTLR start "ruleIfTemplate"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:209:1: ruleIfTemplate : ( ( rule__IfTemplate__Group__0 ) ) ;
    public final void ruleIfTemplate() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:213:2: ( ( ( rule__IfTemplate__Group__0 ) ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:214:1: ( ( rule__IfTemplate__Group__0 ) )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:214:1: ( ( rule__IfTemplate__Group__0 ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:215:1: ( rule__IfTemplate__Group__0 )
            {
             before(grammarAccess.getIfTemplateAccess().getGroup()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:216:1: ( rule__IfTemplate__Group__0 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:216:2: rule__IfTemplate__Group__0
            {
            pushFollow(FOLLOW_rule__IfTemplate__Group__0_in_ruleIfTemplate394);
            rule__IfTemplate__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getIfTemplateAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIfTemplate"


    // $ANTLR start "entryRuleForTemplate"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:228:1: entryRuleForTemplate : ruleForTemplate EOF ;
    public final void entryRuleForTemplate() throws RecognitionException {
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:229:1: ( ruleForTemplate EOF )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:230:1: ruleForTemplate EOF
            {
             before(grammarAccess.getForTemplateRule()); 
            pushFollow(FOLLOW_ruleForTemplate_in_entryRuleForTemplate421);
            ruleForTemplate();

            state._fsp--;

             after(grammarAccess.getForTemplateRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleForTemplate428); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleForTemplate"


    // $ANTLR start "ruleForTemplate"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:237:1: ruleForTemplate : ( ( rule__ForTemplate__Group__0 ) ) ;
    public final void ruleForTemplate() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:241:2: ( ( ( rule__ForTemplate__Group__0 ) ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:242:1: ( ( rule__ForTemplate__Group__0 ) )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:242:1: ( ( rule__ForTemplate__Group__0 ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:243:1: ( rule__ForTemplate__Group__0 )
            {
             before(grammarAccess.getForTemplateAccess().getGroup()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:244:1: ( rule__ForTemplate__Group__0 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:244:2: rule__ForTemplate__Group__0
            {
            pushFollow(FOLLOW_rule__ForTemplate__Group__0_in_ruleForTemplate454);
            rule__ForTemplate__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getForTemplateAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleForTemplate"


    // $ANTLR start "entryRuleTemplateReference"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:256:1: entryRuleTemplateReference : ruleTemplateReference EOF ;
    public final void entryRuleTemplateReference() throws RecognitionException {
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:257:1: ( ruleTemplateReference EOF )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:258:1: ruleTemplateReference EOF
            {
             before(grammarAccess.getTemplateReferenceRule()); 
            pushFollow(FOLLOW_ruleTemplateReference_in_entryRuleTemplateReference481);
            ruleTemplateReference();

            state._fsp--;

             after(grammarAccess.getTemplateReferenceRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleTemplateReference488); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTemplateReference"


    // $ANTLR start "ruleTemplateReference"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:265:1: ruleTemplateReference : ( ( rule__TemplateReference__Group__0 ) ) ;
    public final void ruleTemplateReference() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:269:2: ( ( ( rule__TemplateReference__Group__0 ) ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:270:1: ( ( rule__TemplateReference__Group__0 ) )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:270:1: ( ( rule__TemplateReference__Group__0 ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:271:1: ( rule__TemplateReference__Group__0 )
            {
             before(grammarAccess.getTemplateReferenceAccess().getGroup()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:272:1: ( rule__TemplateReference__Group__0 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:272:2: rule__TemplateReference__Group__0
            {
            pushFollow(FOLLOW_rule__TemplateReference__Group__0_in_ruleTemplateReference514);
            rule__TemplateReference__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTemplateReferenceAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTemplateReference"


    // $ANTLR start "entryRuleETypedElement"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:286:1: entryRuleETypedElement : ruleETypedElement EOF ;
    public final void entryRuleETypedElement() throws RecognitionException {
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:287:1: ( ruleETypedElement EOF )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:288:1: ruleETypedElement EOF
            {
             before(grammarAccess.getETypedElementRule()); 
            pushFollow(FOLLOW_ruleETypedElement_in_entryRuleETypedElement543);
            ruleETypedElement();

            state._fsp--;

             after(grammarAccess.getETypedElementRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleETypedElement550); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleETypedElement"


    // $ANTLR start "ruleETypedElement"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:295:1: ruleETypedElement : ( ruleParameter ) ;
    public final void ruleETypedElement() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:299:2: ( ( ruleParameter ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:300:1: ( ruleParameter )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:300:1: ( ruleParameter )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:301:1: ruleParameter
            {
             before(grammarAccess.getETypedElementAccess().getParameterParserRuleCall()); 
            pushFollow(FOLLOW_ruleParameter_in_ruleETypedElement576);
            ruleParameter();

            state._fsp--;

             after(grammarAccess.getETypedElementAccess().getParameterParserRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleETypedElement"


    // $ANTLR start "entryRuleParameter"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:314:1: entryRuleParameter : ruleParameter EOF ;
    public final void entryRuleParameter() throws RecognitionException {
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:315:1: ( ruleParameter EOF )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:316:1: ruleParameter EOF
            {
             before(grammarAccess.getParameterRule()); 
            pushFollow(FOLLOW_ruleParameter_in_entryRuleParameter602);
            ruleParameter();

            state._fsp--;

             after(grammarAccess.getParameterRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleParameter609); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleParameter"


    // $ANTLR start "ruleParameter"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:323:1: ruleParameter : ( ( rule__Parameter__Group__0 ) ) ;
    public final void ruleParameter() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:327:2: ( ( ( rule__Parameter__Group__0 ) ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:328:1: ( ( rule__Parameter__Group__0 ) )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:328:1: ( ( rule__Parameter__Group__0 ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:329:1: ( rule__Parameter__Group__0 )
            {
             before(grammarAccess.getParameterAccess().getGroup()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:330:1: ( rule__Parameter__Group__0 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:330:2: rule__Parameter__Group__0
            {
            pushFollow(FOLLOW_rule__Parameter__Group__0_in_ruleParameter635);
            rule__Parameter__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getParameterAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleParameter"


    // $ANTLR start "entryRuleReference"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:342:1: entryRuleReference : ruleReference EOF ;
    public final void entryRuleReference() throws RecognitionException {
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:343:1: ( ruleReference EOF )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:344:1: ruleReference EOF
            {
             before(grammarAccess.getReferenceRule()); 
            pushFollow(FOLLOW_ruleReference_in_entryRuleReference662);
            ruleReference();

            state._fsp--;

             after(grammarAccess.getReferenceRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleReference669); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleReference"


    // $ANTLR start "ruleReference"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:351:1: ruleReference : ( ( rule__Reference__Group__0 ) ) ;
    public final void ruleReference() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:355:2: ( ( ( rule__Reference__Group__0 ) ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:356:1: ( ( rule__Reference__Group__0 ) )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:356:1: ( ( rule__Reference__Group__0 ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:357:1: ( rule__Reference__Group__0 )
            {
             before(grammarAccess.getReferenceAccess().getGroup()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:358:1: ( rule__Reference__Group__0 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:358:2: rule__Reference__Group__0
            {
            pushFollow(FOLLOW_rule__Reference__Group__0_in_ruleReference695);
            rule__Reference__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getReferenceAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleReference"


    // $ANTLR start "entryRuleReferenceTail"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:370:1: entryRuleReferenceTail : ruleReferenceTail EOF ;
    public final void entryRuleReferenceTail() throws RecognitionException {
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:371:1: ( ruleReferenceTail EOF )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:372:1: ruleReferenceTail EOF
            {
             before(grammarAccess.getReferenceTailRule()); 
            pushFollow(FOLLOW_ruleReferenceTail_in_entryRuleReferenceTail722);
            ruleReferenceTail();

            state._fsp--;

             after(grammarAccess.getReferenceTailRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleReferenceTail729); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleReferenceTail"


    // $ANTLR start "ruleReferenceTail"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:379:1: ruleReferenceTail : ( ( rule__ReferenceTail__Group__0 ) ) ;
    public final void ruleReferenceTail() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:383:2: ( ( ( rule__ReferenceTail__Group__0 ) ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:384:1: ( ( rule__ReferenceTail__Group__0 ) )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:384:1: ( ( rule__ReferenceTail__Group__0 ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:385:1: ( rule__ReferenceTail__Group__0 )
            {
             before(grammarAccess.getReferenceTailAccess().getGroup()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:386:1: ( rule__ReferenceTail__Group__0 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:386:2: rule__ReferenceTail__Group__0
            {
            pushFollow(FOLLOW_rule__ReferenceTail__Group__0_in_ruleReferenceTail755);
            rule__ReferenceTail__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getReferenceTailAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleReferenceTail"


    // $ANTLR start "entryRuleBoolValue"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:398:1: entryRuleBoolValue : ruleBoolValue EOF ;
    public final void entryRuleBoolValue() throws RecognitionException {
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:399:1: ( ruleBoolValue EOF )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:400:1: ruleBoolValue EOF
            {
             before(grammarAccess.getBoolValueRule()); 
            pushFollow(FOLLOW_ruleBoolValue_in_entryRuleBoolValue782);
            ruleBoolValue();

            state._fsp--;

             after(grammarAccess.getBoolValueRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleBoolValue789); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBoolValue"


    // $ANTLR start "ruleBoolValue"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:407:1: ruleBoolValue : ( ( rule__BoolValue__TypeAssignment ) ) ;
    public final void ruleBoolValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:411:2: ( ( ( rule__BoolValue__TypeAssignment ) ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:412:1: ( ( rule__BoolValue__TypeAssignment ) )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:412:1: ( ( rule__BoolValue__TypeAssignment ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:413:1: ( rule__BoolValue__TypeAssignment )
            {
             before(grammarAccess.getBoolValueAccess().getTypeAssignment()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:414:1: ( rule__BoolValue__TypeAssignment )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:414:2: rule__BoolValue__TypeAssignment
            {
            pushFollow(FOLLOW_rule__BoolValue__TypeAssignment_in_ruleBoolValue815);
            rule__BoolValue__TypeAssignment();

            state._fsp--;


            }

             after(grammarAccess.getBoolValueAccess().getTypeAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBoolValue"


    // $ANTLR start "entryRuleQualifiedNameWithWildcard"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:426:1: entryRuleQualifiedNameWithWildcard : ruleQualifiedNameWithWildcard EOF ;
    public final void entryRuleQualifiedNameWithWildcard() throws RecognitionException {
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:427:1: ( ruleQualifiedNameWithWildcard EOF )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:428:1: ruleQualifiedNameWithWildcard EOF
            {
             before(grammarAccess.getQualifiedNameWithWildcardRule()); 
            pushFollow(FOLLOW_ruleQualifiedNameWithWildcard_in_entryRuleQualifiedNameWithWildcard842);
            ruleQualifiedNameWithWildcard();

            state._fsp--;

             after(grammarAccess.getQualifiedNameWithWildcardRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQualifiedNameWithWildcard849); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQualifiedNameWithWildcard"


    // $ANTLR start "ruleQualifiedNameWithWildcard"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:435:1: ruleQualifiedNameWithWildcard : ( ( rule__QualifiedNameWithWildcard__Group__0 ) ) ;
    public final void ruleQualifiedNameWithWildcard() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:439:2: ( ( ( rule__QualifiedNameWithWildcard__Group__0 ) ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:440:1: ( ( rule__QualifiedNameWithWildcard__Group__0 ) )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:440:1: ( ( rule__QualifiedNameWithWildcard__Group__0 ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:441:1: ( rule__QualifiedNameWithWildcard__Group__0 )
            {
             before(grammarAccess.getQualifiedNameWithWildcardAccess().getGroup()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:442:1: ( rule__QualifiedNameWithWildcard__Group__0 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:442:2: rule__QualifiedNameWithWildcard__Group__0
            {
            pushFollow(FOLLOW_rule__QualifiedNameWithWildcard__Group__0_in_ruleQualifiedNameWithWildcard875);
            rule__QualifiedNameWithWildcard__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQualifiedNameWithWildcardAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQualifiedNameWithWildcard"


    // $ANTLR start "entryRuleQUALIFIED_NAME"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:454:1: entryRuleQUALIFIED_NAME : ruleQUALIFIED_NAME EOF ;
    public final void entryRuleQUALIFIED_NAME() throws RecognitionException {
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:455:1: ( ruleQUALIFIED_NAME EOF )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:456:1: ruleQUALIFIED_NAME EOF
            {
             before(grammarAccess.getQUALIFIED_NAMERule()); 
            pushFollow(FOLLOW_ruleQUALIFIED_NAME_in_entryRuleQUALIFIED_NAME902);
            ruleQUALIFIED_NAME();

            state._fsp--;

             after(grammarAccess.getQUALIFIED_NAMERule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQUALIFIED_NAME909); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQUALIFIED_NAME"


    // $ANTLR start "ruleQUALIFIED_NAME"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:463:1: ruleQUALIFIED_NAME : ( ( rule__QUALIFIED_NAME__Group__0 ) ) ;
    public final void ruleQUALIFIED_NAME() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:467:2: ( ( ( rule__QUALIFIED_NAME__Group__0 ) ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:468:1: ( ( rule__QUALIFIED_NAME__Group__0 ) )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:468:1: ( ( rule__QUALIFIED_NAME__Group__0 ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:469:1: ( rule__QUALIFIED_NAME__Group__0 )
            {
             before(grammarAccess.getQUALIFIED_NAMEAccess().getGroup()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:470:1: ( rule__QUALIFIED_NAME__Group__0 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:470:2: rule__QUALIFIED_NAME__Group__0
            {
            pushFollow(FOLLOW_rule__QUALIFIED_NAME__Group__0_in_ruleQUALIFIED_NAME935);
            rule__QUALIFIED_NAME__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQUALIFIED_NAMEAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQUALIFIED_NAME"


    // $ANTLR start "rule__Statement__Alternatives"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:482:1: rule__Statement__Alternatives : ( ( ( rule__Statement__Group_0__0 ) ) | ( RULE_ID ) | ( ruleTemplateStatement ) | ( ruleTemplateReference ) );
    public final void rule__Statement__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:486:1: ( ( ( rule__Statement__Group_0__0 ) ) | ( RULE_ID ) | ( ruleTemplateStatement ) | ( ruleTemplateReference ) )
            int alt1=4;
            switch ( input.LA(1) ) {
            case RULE_CHARACTER:
                {
                alt1=1;
                }
                break;
            case RULE_ID:
                {
                alt1=2;
                }
                break;
            case 22:
                {
                int LA1_3 = input.LA(2);

                if ( (LA1_3==23||LA1_3==25) ) {
                    alt1=3;
                }
                else if ( (LA1_3==RULE_ID) ) {
                    alt1=4;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 1, 3, input);

                    throw nvae;
                }
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:487:1: ( ( rule__Statement__Group_0__0 ) )
                    {
                    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:487:1: ( ( rule__Statement__Group_0__0 ) )
                    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:488:1: ( rule__Statement__Group_0__0 )
                    {
                     before(grammarAccess.getStatementAccess().getGroup_0()); 
                    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:489:1: ( rule__Statement__Group_0__0 )
                    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:489:2: rule__Statement__Group_0__0
                    {
                    pushFollow(FOLLOW_rule__Statement__Group_0__0_in_rule__Statement__Alternatives971);
                    rule__Statement__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getStatementAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:493:6: ( RULE_ID )
                    {
                    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:493:6: ( RULE_ID )
                    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:494:1: RULE_ID
                    {
                     before(grammarAccess.getStatementAccess().getIDTerminalRuleCall_1()); 
                    match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Statement__Alternatives989); 
                     after(grammarAccess.getStatementAccess().getIDTerminalRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:499:6: ( ruleTemplateStatement )
                    {
                    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:499:6: ( ruleTemplateStatement )
                    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:500:1: ruleTemplateStatement
                    {
                     before(grammarAccess.getStatementAccess().getTemplateStatementParserRuleCall_2()); 
                    pushFollow(FOLLOW_ruleTemplateStatement_in_rule__Statement__Alternatives1006);
                    ruleTemplateStatement();

                    state._fsp--;

                     after(grammarAccess.getStatementAccess().getTemplateStatementParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:505:6: ( ruleTemplateReference )
                    {
                    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:505:6: ( ruleTemplateReference )
                    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:506:1: ruleTemplateReference
                    {
                     before(grammarAccess.getStatementAccess().getTemplateReferenceParserRuleCall_3()); 
                    pushFollow(FOLLOW_ruleTemplateReference_in_rule__Statement__Alternatives1023);
                    ruleTemplateReference();

                    state._fsp--;

                     after(grammarAccess.getStatementAccess().getTemplateReferenceParserRuleCall_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Statement__Alternatives"


    // $ANTLR start "rule__TemplateStatement__Alternatives"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:516:1: rule__TemplateStatement__Alternatives : ( ( ruleIfTemplate ) | ( ruleForTemplate ) );
    public final void rule__TemplateStatement__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:520:1: ( ( ruleIfTemplate ) | ( ruleForTemplate ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==22) ) {
                int LA2_1 = input.LA(2);

                if ( (LA2_1==23) ) {
                    alt2=1;
                }
                else if ( (LA2_1==25) ) {
                    alt2=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 2, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:521:1: ( ruleIfTemplate )
                    {
                    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:521:1: ( ruleIfTemplate )
                    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:522:1: ruleIfTemplate
                    {
                     before(grammarAccess.getTemplateStatementAccess().getIfTemplateParserRuleCall_0()); 
                    pushFollow(FOLLOW_ruleIfTemplate_in_rule__TemplateStatement__Alternatives1055);
                    ruleIfTemplate();

                    state._fsp--;

                     after(grammarAccess.getTemplateStatementAccess().getIfTemplateParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:527:6: ( ruleForTemplate )
                    {
                    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:527:6: ( ruleForTemplate )
                    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:528:1: ruleForTemplate
                    {
                     before(grammarAccess.getTemplateStatementAccess().getForTemplateParserRuleCall_1()); 
                    pushFollow(FOLLOW_ruleForTemplate_in_rule__TemplateStatement__Alternatives1072);
                    ruleForTemplate();

                    state._fsp--;

                     after(grammarAccess.getTemplateStatementAccess().getForTemplateParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateStatement__Alternatives"


    // $ANTLR start "rule__BoolValue__TypeAlternatives_0"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:538:1: rule__BoolValue__TypeAlternatives_0 : ( ( 'true' ) | ( 'false' ) );
    public final void rule__BoolValue__TypeAlternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:542:1: ( ( 'true' ) | ( 'false' ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==12) ) {
                alt3=1;
            }
            else if ( (LA3_0==13) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:543:1: ( 'true' )
                    {
                    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:543:1: ( 'true' )
                    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:544:1: 'true'
                    {
                     before(grammarAccess.getBoolValueAccess().getTypeTrueKeyword_0_0()); 
                    match(input,12,FOLLOW_12_in_rule__BoolValue__TypeAlternatives_01105); 
                     after(grammarAccess.getBoolValueAccess().getTypeTrueKeyword_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:551:6: ( 'false' )
                    {
                    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:551:6: ( 'false' )
                    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:552:1: 'false'
                    {
                     before(grammarAccess.getBoolValueAccess().getTypeFalseKeyword_0_1()); 
                    match(input,13,FOLLOW_13_in_rule__BoolValue__TypeAlternatives_01125); 
                     after(grammarAccess.getBoolValueAccess().getTypeFalseKeyword_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BoolValue__TypeAlternatives_0"


    // $ANTLR start "rule__Template__Group__0"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:566:1: rule__Template__Group__0 : rule__Template__Group__0__Impl rule__Template__Group__1 ;
    public final void rule__Template__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:570:1: ( rule__Template__Group__0__Impl rule__Template__Group__1 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:571:2: rule__Template__Group__0__Impl rule__Template__Group__1
            {
            pushFollow(FOLLOW_rule__Template__Group__0__Impl_in_rule__Template__Group__01157);
            rule__Template__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Template__Group__1_in_rule__Template__Group__01160);
            rule__Template__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Template__Group__0"


    // $ANTLR start "rule__Template__Group__0__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:578:1: rule__Template__Group__0__Impl : ( ( rule__Template__ImportsAssignment_0 )* ) ;
    public final void rule__Template__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:582:1: ( ( ( rule__Template__ImportsAssignment_0 )* ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:583:1: ( ( rule__Template__ImportsAssignment_0 )* )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:583:1: ( ( rule__Template__ImportsAssignment_0 )* )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:584:1: ( rule__Template__ImportsAssignment_0 )*
            {
             before(grammarAccess.getTemplateAccess().getImportsAssignment_0()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:585:1: ( rule__Template__ImportsAssignment_0 )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==14) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:585:2: rule__Template__ImportsAssignment_0
            	    {
            	    pushFollow(FOLLOW_rule__Template__ImportsAssignment_0_in_rule__Template__Group__0__Impl1187);
            	    rule__Template__ImportsAssignment_0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             after(grammarAccess.getTemplateAccess().getImportsAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Template__Group__0__Impl"


    // $ANTLR start "rule__Template__Group__1"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:595:1: rule__Template__Group__1 : rule__Template__Group__1__Impl ;
    public final void rule__Template__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:599:1: ( rule__Template__Group__1__Impl )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:600:2: rule__Template__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Template__Group__1__Impl_in_rule__Template__Group__11218);
            rule__Template__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Template__Group__1"


    // $ANTLR start "rule__Template__Group__1__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:606:1: rule__Template__Group__1__Impl : ( ( rule__Template__TemplateFunctionsAssignment_1 )* ) ;
    public final void rule__Template__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:610:1: ( ( ( rule__Template__TemplateFunctionsAssignment_1 )* ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:611:1: ( ( rule__Template__TemplateFunctionsAssignment_1 )* )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:611:1: ( ( rule__Template__TemplateFunctionsAssignment_1 )* )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:612:1: ( rule__Template__TemplateFunctionsAssignment_1 )*
            {
             before(grammarAccess.getTemplateAccess().getTemplateFunctionsAssignment_1()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:613:1: ( rule__Template__TemplateFunctionsAssignment_1 )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==16) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:613:2: rule__Template__TemplateFunctionsAssignment_1
            	    {
            	    pushFollow(FOLLOW_rule__Template__TemplateFunctionsAssignment_1_in_rule__Template__Group__1__Impl1245);
            	    rule__Template__TemplateFunctionsAssignment_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

             after(grammarAccess.getTemplateAccess().getTemplateFunctionsAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Template__Group__1__Impl"


    // $ANTLR start "rule__Import__Group__0"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:627:1: rule__Import__Group__0 : rule__Import__Group__0__Impl rule__Import__Group__1 ;
    public final void rule__Import__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:631:1: ( rule__Import__Group__0__Impl rule__Import__Group__1 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:632:2: rule__Import__Group__0__Impl rule__Import__Group__1
            {
            pushFollow(FOLLOW_rule__Import__Group__0__Impl_in_rule__Import__Group__01280);
            rule__Import__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Import__Group__1_in_rule__Import__Group__01283);
            rule__Import__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0"


    // $ANTLR start "rule__Import__Group__0__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:639:1: rule__Import__Group__0__Impl : ( 'import' ) ;
    public final void rule__Import__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:643:1: ( ( 'import' ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:644:1: ( 'import' )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:644:1: ( 'import' )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:645:1: 'import'
            {
             before(grammarAccess.getImportAccess().getImportKeyword_0()); 
            match(input,14,FOLLOW_14_in_rule__Import__Group__0__Impl1311); 
             after(grammarAccess.getImportAccess().getImportKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0__Impl"


    // $ANTLR start "rule__Import__Group__1"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:658:1: rule__Import__Group__1 : rule__Import__Group__1__Impl rule__Import__Group__2 ;
    public final void rule__Import__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:662:1: ( rule__Import__Group__1__Impl rule__Import__Group__2 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:663:2: rule__Import__Group__1__Impl rule__Import__Group__2
            {
            pushFollow(FOLLOW_rule__Import__Group__1__Impl_in_rule__Import__Group__11342);
            rule__Import__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Import__Group__2_in_rule__Import__Group__11345);
            rule__Import__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1"


    // $ANTLR start "rule__Import__Group__1__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:670:1: rule__Import__Group__1__Impl : ( ( rule__Import__ImportedNamespaceAssignment_1 ) ) ;
    public final void rule__Import__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:674:1: ( ( ( rule__Import__ImportedNamespaceAssignment_1 ) ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:675:1: ( ( rule__Import__ImportedNamespaceAssignment_1 ) )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:675:1: ( ( rule__Import__ImportedNamespaceAssignment_1 ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:676:1: ( rule__Import__ImportedNamespaceAssignment_1 )
            {
             before(grammarAccess.getImportAccess().getImportedNamespaceAssignment_1()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:677:1: ( rule__Import__ImportedNamespaceAssignment_1 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:677:2: rule__Import__ImportedNamespaceAssignment_1
            {
            pushFollow(FOLLOW_rule__Import__ImportedNamespaceAssignment_1_in_rule__Import__Group__1__Impl1372);
            rule__Import__ImportedNamespaceAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getImportAccess().getImportedNamespaceAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1__Impl"


    // $ANTLR start "rule__Import__Group__2"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:687:1: rule__Import__Group__2 : rule__Import__Group__2__Impl ;
    public final void rule__Import__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:691:1: ( rule__Import__Group__2__Impl )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:692:2: rule__Import__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__Import__Group__2__Impl_in_rule__Import__Group__21402);
            rule__Import__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__2"


    // $ANTLR start "rule__Import__Group__2__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:698:1: rule__Import__Group__2__Impl : ( ';' ) ;
    public final void rule__Import__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:702:1: ( ( ';' ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:703:1: ( ';' )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:703:1: ( ';' )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:704:1: ';'
            {
             before(grammarAccess.getImportAccess().getSemicolonKeyword_2()); 
            match(input,15,FOLLOW_15_in_rule__Import__Group__2__Impl1430); 
             after(grammarAccess.getImportAccess().getSemicolonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__2__Impl"


    // $ANTLR start "rule__TemplateFunction__Group__0"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:723:1: rule__TemplateFunction__Group__0 : rule__TemplateFunction__Group__0__Impl rule__TemplateFunction__Group__1 ;
    public final void rule__TemplateFunction__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:727:1: ( rule__TemplateFunction__Group__0__Impl rule__TemplateFunction__Group__1 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:728:2: rule__TemplateFunction__Group__0__Impl rule__TemplateFunction__Group__1
            {
            pushFollow(FOLLOW_rule__TemplateFunction__Group__0__Impl_in_rule__TemplateFunction__Group__01467);
            rule__TemplateFunction__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__TemplateFunction__Group__1_in_rule__TemplateFunction__Group__01470);
            rule__TemplateFunction__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateFunction__Group__0"


    // $ANTLR start "rule__TemplateFunction__Group__0__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:735:1: rule__TemplateFunction__Group__0__Impl : ( '\\u00ABtemplate' ) ;
    public final void rule__TemplateFunction__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:739:1: ( ( '\\u00ABtemplate' ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:740:1: ( '\\u00ABtemplate' )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:740:1: ( '\\u00ABtemplate' )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:741:1: '\\u00ABtemplate'
            {
             before(grammarAccess.getTemplateFunctionAccess().getTemplateKeyword_0()); 
            match(input,16,FOLLOW_16_in_rule__TemplateFunction__Group__0__Impl1498); 
             after(grammarAccess.getTemplateFunctionAccess().getTemplateKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateFunction__Group__0__Impl"


    // $ANTLR start "rule__TemplateFunction__Group__1"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:754:1: rule__TemplateFunction__Group__1 : rule__TemplateFunction__Group__1__Impl rule__TemplateFunction__Group__2 ;
    public final void rule__TemplateFunction__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:758:1: ( rule__TemplateFunction__Group__1__Impl rule__TemplateFunction__Group__2 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:759:2: rule__TemplateFunction__Group__1__Impl rule__TemplateFunction__Group__2
            {
            pushFollow(FOLLOW_rule__TemplateFunction__Group__1__Impl_in_rule__TemplateFunction__Group__11529);
            rule__TemplateFunction__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__TemplateFunction__Group__2_in_rule__TemplateFunction__Group__11532);
            rule__TemplateFunction__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateFunction__Group__1"


    // $ANTLR start "rule__TemplateFunction__Group__1__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:766:1: rule__TemplateFunction__Group__1__Impl : ( ( rule__TemplateFunction__NameAssignment_1 ) ) ;
    public final void rule__TemplateFunction__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:770:1: ( ( ( rule__TemplateFunction__NameAssignment_1 ) ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:771:1: ( ( rule__TemplateFunction__NameAssignment_1 ) )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:771:1: ( ( rule__TemplateFunction__NameAssignment_1 ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:772:1: ( rule__TemplateFunction__NameAssignment_1 )
            {
             before(grammarAccess.getTemplateFunctionAccess().getNameAssignment_1()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:773:1: ( rule__TemplateFunction__NameAssignment_1 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:773:2: rule__TemplateFunction__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__TemplateFunction__NameAssignment_1_in_rule__TemplateFunction__Group__1__Impl1559);
            rule__TemplateFunction__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getTemplateFunctionAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateFunction__Group__1__Impl"


    // $ANTLR start "rule__TemplateFunction__Group__2"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:783:1: rule__TemplateFunction__Group__2 : rule__TemplateFunction__Group__2__Impl rule__TemplateFunction__Group__3 ;
    public final void rule__TemplateFunction__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:787:1: ( rule__TemplateFunction__Group__2__Impl rule__TemplateFunction__Group__3 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:788:2: rule__TemplateFunction__Group__2__Impl rule__TemplateFunction__Group__3
            {
            pushFollow(FOLLOW_rule__TemplateFunction__Group__2__Impl_in_rule__TemplateFunction__Group__21589);
            rule__TemplateFunction__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__TemplateFunction__Group__3_in_rule__TemplateFunction__Group__21592);
            rule__TemplateFunction__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateFunction__Group__2"


    // $ANTLR start "rule__TemplateFunction__Group__2__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:795:1: rule__TemplateFunction__Group__2__Impl : ( '[' ) ;
    public final void rule__TemplateFunction__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:799:1: ( ( '[' ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:800:1: ( '[' )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:800:1: ( '[' )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:801:1: '['
            {
             before(grammarAccess.getTemplateFunctionAccess().getLeftSquareBracketKeyword_2()); 
            match(input,17,FOLLOW_17_in_rule__TemplateFunction__Group__2__Impl1620); 
             after(grammarAccess.getTemplateFunctionAccess().getLeftSquareBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateFunction__Group__2__Impl"


    // $ANTLR start "rule__TemplateFunction__Group__3"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:814:1: rule__TemplateFunction__Group__3 : rule__TemplateFunction__Group__3__Impl rule__TemplateFunction__Group__4 ;
    public final void rule__TemplateFunction__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:818:1: ( rule__TemplateFunction__Group__3__Impl rule__TemplateFunction__Group__4 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:819:2: rule__TemplateFunction__Group__3__Impl rule__TemplateFunction__Group__4
            {
            pushFollow(FOLLOW_rule__TemplateFunction__Group__3__Impl_in_rule__TemplateFunction__Group__31651);
            rule__TemplateFunction__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__TemplateFunction__Group__4_in_rule__TemplateFunction__Group__31654);
            rule__TemplateFunction__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateFunction__Group__3"


    // $ANTLR start "rule__TemplateFunction__Group__3__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:826:1: rule__TemplateFunction__Group__3__Impl : ( ( rule__TemplateFunction__ParametersAssignment_3 ) ) ;
    public final void rule__TemplateFunction__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:830:1: ( ( ( rule__TemplateFunction__ParametersAssignment_3 ) ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:831:1: ( ( rule__TemplateFunction__ParametersAssignment_3 ) )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:831:1: ( ( rule__TemplateFunction__ParametersAssignment_3 ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:832:1: ( rule__TemplateFunction__ParametersAssignment_3 )
            {
             before(grammarAccess.getTemplateFunctionAccess().getParametersAssignment_3()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:833:1: ( rule__TemplateFunction__ParametersAssignment_3 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:833:2: rule__TemplateFunction__ParametersAssignment_3
            {
            pushFollow(FOLLOW_rule__TemplateFunction__ParametersAssignment_3_in_rule__TemplateFunction__Group__3__Impl1681);
            rule__TemplateFunction__ParametersAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getTemplateFunctionAccess().getParametersAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateFunction__Group__3__Impl"


    // $ANTLR start "rule__TemplateFunction__Group__4"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:843:1: rule__TemplateFunction__Group__4 : rule__TemplateFunction__Group__4__Impl rule__TemplateFunction__Group__5 ;
    public final void rule__TemplateFunction__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:847:1: ( rule__TemplateFunction__Group__4__Impl rule__TemplateFunction__Group__5 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:848:2: rule__TemplateFunction__Group__4__Impl rule__TemplateFunction__Group__5
            {
            pushFollow(FOLLOW_rule__TemplateFunction__Group__4__Impl_in_rule__TemplateFunction__Group__41711);
            rule__TemplateFunction__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__TemplateFunction__Group__5_in_rule__TemplateFunction__Group__41714);
            rule__TemplateFunction__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateFunction__Group__4"


    // $ANTLR start "rule__TemplateFunction__Group__4__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:855:1: rule__TemplateFunction__Group__4__Impl : ( ( rule__TemplateFunction__Group_4__0 )* ) ;
    public final void rule__TemplateFunction__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:859:1: ( ( ( rule__TemplateFunction__Group_4__0 )* ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:860:1: ( ( rule__TemplateFunction__Group_4__0 )* )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:860:1: ( ( rule__TemplateFunction__Group_4__0 )* )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:861:1: ( rule__TemplateFunction__Group_4__0 )*
            {
             before(grammarAccess.getTemplateFunctionAccess().getGroup_4()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:862:1: ( rule__TemplateFunction__Group_4__0 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==21) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:862:2: rule__TemplateFunction__Group_4__0
            	    {
            	    pushFollow(FOLLOW_rule__TemplateFunction__Group_4__0_in_rule__TemplateFunction__Group__4__Impl1741);
            	    rule__TemplateFunction__Group_4__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getTemplateFunctionAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateFunction__Group__4__Impl"


    // $ANTLR start "rule__TemplateFunction__Group__5"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:872:1: rule__TemplateFunction__Group__5 : rule__TemplateFunction__Group__5__Impl rule__TemplateFunction__Group__6 ;
    public final void rule__TemplateFunction__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:876:1: ( rule__TemplateFunction__Group__5__Impl rule__TemplateFunction__Group__6 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:877:2: rule__TemplateFunction__Group__5__Impl rule__TemplateFunction__Group__6
            {
            pushFollow(FOLLOW_rule__TemplateFunction__Group__5__Impl_in_rule__TemplateFunction__Group__51772);
            rule__TemplateFunction__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__TemplateFunction__Group__6_in_rule__TemplateFunction__Group__51775);
            rule__TemplateFunction__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateFunction__Group__5"


    // $ANTLR start "rule__TemplateFunction__Group__5__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:884:1: rule__TemplateFunction__Group__5__Impl : ( ']' ) ;
    public final void rule__TemplateFunction__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:888:1: ( ( ']' ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:889:1: ( ']' )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:889:1: ( ']' )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:890:1: ']'
            {
             before(grammarAccess.getTemplateFunctionAccess().getRightSquareBracketKeyword_5()); 
            match(input,18,FOLLOW_18_in_rule__TemplateFunction__Group__5__Impl1803); 
             after(grammarAccess.getTemplateFunctionAccess().getRightSquareBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateFunction__Group__5__Impl"


    // $ANTLR start "rule__TemplateFunction__Group__6"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:903:1: rule__TemplateFunction__Group__6 : rule__TemplateFunction__Group__6__Impl rule__TemplateFunction__Group__7 ;
    public final void rule__TemplateFunction__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:907:1: ( rule__TemplateFunction__Group__6__Impl rule__TemplateFunction__Group__7 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:908:2: rule__TemplateFunction__Group__6__Impl rule__TemplateFunction__Group__7
            {
            pushFollow(FOLLOW_rule__TemplateFunction__Group__6__Impl_in_rule__TemplateFunction__Group__61834);
            rule__TemplateFunction__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__TemplateFunction__Group__7_in_rule__TemplateFunction__Group__61837);
            rule__TemplateFunction__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateFunction__Group__6"


    // $ANTLR start "rule__TemplateFunction__Group__6__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:915:1: rule__TemplateFunction__Group__6__Impl : ( '\\u00BB' ) ;
    public final void rule__TemplateFunction__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:919:1: ( ( '\\u00BB' ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:920:1: ( '\\u00BB' )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:920:1: ( '\\u00BB' )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:921:1: '\\u00BB'
            {
             before(grammarAccess.getTemplateFunctionAccess().getRightPointingDoubleAngleQuotationMarkKeyword_6()); 
            match(input,19,FOLLOW_19_in_rule__TemplateFunction__Group__6__Impl1865); 
             after(grammarAccess.getTemplateFunctionAccess().getRightPointingDoubleAngleQuotationMarkKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateFunction__Group__6__Impl"


    // $ANTLR start "rule__TemplateFunction__Group__7"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:934:1: rule__TemplateFunction__Group__7 : rule__TemplateFunction__Group__7__Impl rule__TemplateFunction__Group__8 ;
    public final void rule__TemplateFunction__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:938:1: ( rule__TemplateFunction__Group__7__Impl rule__TemplateFunction__Group__8 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:939:2: rule__TemplateFunction__Group__7__Impl rule__TemplateFunction__Group__8
            {
            pushFollow(FOLLOW_rule__TemplateFunction__Group__7__Impl_in_rule__TemplateFunction__Group__71896);
            rule__TemplateFunction__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__TemplateFunction__Group__8_in_rule__TemplateFunction__Group__71899);
            rule__TemplateFunction__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateFunction__Group__7"


    // $ANTLR start "rule__TemplateFunction__Group__7__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:946:1: rule__TemplateFunction__Group__7__Impl : ( ( rule__TemplateFunction__StatementsAssignment_7 )* ) ;
    public final void rule__TemplateFunction__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:950:1: ( ( ( rule__TemplateFunction__StatementsAssignment_7 )* ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:951:1: ( ( rule__TemplateFunction__StatementsAssignment_7 )* )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:951:1: ( ( rule__TemplateFunction__StatementsAssignment_7 )* )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:952:1: ( rule__TemplateFunction__StatementsAssignment_7 )*
            {
             before(grammarAccess.getTemplateFunctionAccess().getStatementsAssignment_7()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:953:1: ( rule__TemplateFunction__StatementsAssignment_7 )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( ((LA7_0>=RULE_ID && LA7_0<=RULE_CHARACTER)||LA7_0==22) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:953:2: rule__TemplateFunction__StatementsAssignment_7
            	    {
            	    pushFollow(FOLLOW_rule__TemplateFunction__StatementsAssignment_7_in_rule__TemplateFunction__Group__7__Impl1926);
            	    rule__TemplateFunction__StatementsAssignment_7();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

             after(grammarAccess.getTemplateFunctionAccess().getStatementsAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateFunction__Group__7__Impl"


    // $ANTLR start "rule__TemplateFunction__Group__8"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:963:1: rule__TemplateFunction__Group__8 : rule__TemplateFunction__Group__8__Impl ;
    public final void rule__TemplateFunction__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:967:1: ( rule__TemplateFunction__Group__8__Impl )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:968:2: rule__TemplateFunction__Group__8__Impl
            {
            pushFollow(FOLLOW_rule__TemplateFunction__Group__8__Impl_in_rule__TemplateFunction__Group__81957);
            rule__TemplateFunction__Group__8__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateFunction__Group__8"


    // $ANTLR start "rule__TemplateFunction__Group__8__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:974:1: rule__TemplateFunction__Group__8__Impl : ( '\\u00ABendTemplate\\u00BB' ) ;
    public final void rule__TemplateFunction__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:978:1: ( ( '\\u00ABendTemplate\\u00BB' ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:979:1: ( '\\u00ABendTemplate\\u00BB' )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:979:1: ( '\\u00ABendTemplate\\u00BB' )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:980:1: '\\u00ABendTemplate\\u00BB'
            {
             before(grammarAccess.getTemplateFunctionAccess().getEndTemplateKeyword_8()); 
            match(input,20,FOLLOW_20_in_rule__TemplateFunction__Group__8__Impl1985); 
             after(grammarAccess.getTemplateFunctionAccess().getEndTemplateKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateFunction__Group__8__Impl"


    // $ANTLR start "rule__TemplateFunction__Group_4__0"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1011:1: rule__TemplateFunction__Group_4__0 : rule__TemplateFunction__Group_4__0__Impl rule__TemplateFunction__Group_4__1 ;
    public final void rule__TemplateFunction__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1015:1: ( rule__TemplateFunction__Group_4__0__Impl rule__TemplateFunction__Group_4__1 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1016:2: rule__TemplateFunction__Group_4__0__Impl rule__TemplateFunction__Group_4__1
            {
            pushFollow(FOLLOW_rule__TemplateFunction__Group_4__0__Impl_in_rule__TemplateFunction__Group_4__02034);
            rule__TemplateFunction__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__TemplateFunction__Group_4__1_in_rule__TemplateFunction__Group_4__02037);
            rule__TemplateFunction__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateFunction__Group_4__0"


    // $ANTLR start "rule__TemplateFunction__Group_4__0__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1023:1: rule__TemplateFunction__Group_4__0__Impl : ( ',' ) ;
    public final void rule__TemplateFunction__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1027:1: ( ( ',' ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1028:1: ( ',' )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1028:1: ( ',' )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1029:1: ','
            {
             before(grammarAccess.getTemplateFunctionAccess().getCommaKeyword_4_0()); 
            match(input,21,FOLLOW_21_in_rule__TemplateFunction__Group_4__0__Impl2065); 
             after(grammarAccess.getTemplateFunctionAccess().getCommaKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateFunction__Group_4__0__Impl"


    // $ANTLR start "rule__TemplateFunction__Group_4__1"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1042:1: rule__TemplateFunction__Group_4__1 : rule__TemplateFunction__Group_4__1__Impl ;
    public final void rule__TemplateFunction__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1046:1: ( rule__TemplateFunction__Group_4__1__Impl )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1047:2: rule__TemplateFunction__Group_4__1__Impl
            {
            pushFollow(FOLLOW_rule__TemplateFunction__Group_4__1__Impl_in_rule__TemplateFunction__Group_4__12096);
            rule__TemplateFunction__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateFunction__Group_4__1"


    // $ANTLR start "rule__TemplateFunction__Group_4__1__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1053:1: rule__TemplateFunction__Group_4__1__Impl : ( ( rule__TemplateFunction__ParametersAssignment_4_1 ) ) ;
    public final void rule__TemplateFunction__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1057:1: ( ( ( rule__TemplateFunction__ParametersAssignment_4_1 ) ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1058:1: ( ( rule__TemplateFunction__ParametersAssignment_4_1 ) )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1058:1: ( ( rule__TemplateFunction__ParametersAssignment_4_1 ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1059:1: ( rule__TemplateFunction__ParametersAssignment_4_1 )
            {
             before(grammarAccess.getTemplateFunctionAccess().getParametersAssignment_4_1()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1060:1: ( rule__TemplateFunction__ParametersAssignment_4_1 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1060:2: rule__TemplateFunction__ParametersAssignment_4_1
            {
            pushFollow(FOLLOW_rule__TemplateFunction__ParametersAssignment_4_1_in_rule__TemplateFunction__Group_4__1__Impl2123);
            rule__TemplateFunction__ParametersAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getTemplateFunctionAccess().getParametersAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateFunction__Group_4__1__Impl"


    // $ANTLR start "rule__Statement__Group_0__0"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1074:1: rule__Statement__Group_0__0 : rule__Statement__Group_0__0__Impl rule__Statement__Group_0__1 ;
    public final void rule__Statement__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1078:1: ( rule__Statement__Group_0__0__Impl rule__Statement__Group_0__1 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1079:2: rule__Statement__Group_0__0__Impl rule__Statement__Group_0__1
            {
            pushFollow(FOLLOW_rule__Statement__Group_0__0__Impl_in_rule__Statement__Group_0__02157);
            rule__Statement__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Statement__Group_0__1_in_rule__Statement__Group_0__02160);
            rule__Statement__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Statement__Group_0__0"


    // $ANTLR start "rule__Statement__Group_0__0__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1086:1: rule__Statement__Group_0__0__Impl : ( () ) ;
    public final void rule__Statement__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1090:1: ( ( () ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1091:1: ( () )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1091:1: ( () )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1092:1: ()
            {
             before(grammarAccess.getStatementAccess().getStatementAction_0_0()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1093:1: ()
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1095:1: 
            {
            }

             after(grammarAccess.getStatementAccess().getStatementAction_0_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Statement__Group_0__0__Impl"


    // $ANTLR start "rule__Statement__Group_0__1"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1105:1: rule__Statement__Group_0__1 : rule__Statement__Group_0__1__Impl ;
    public final void rule__Statement__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1109:1: ( rule__Statement__Group_0__1__Impl )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1110:2: rule__Statement__Group_0__1__Impl
            {
            pushFollow(FOLLOW_rule__Statement__Group_0__1__Impl_in_rule__Statement__Group_0__12218);
            rule__Statement__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Statement__Group_0__1"


    // $ANTLR start "rule__Statement__Group_0__1__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1116:1: rule__Statement__Group_0__1__Impl : ( RULE_CHARACTER ) ;
    public final void rule__Statement__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1120:1: ( ( RULE_CHARACTER ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1121:1: ( RULE_CHARACTER )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1121:1: ( RULE_CHARACTER )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1122:1: RULE_CHARACTER
            {
             before(grammarAccess.getStatementAccess().getCHARACTERTerminalRuleCall_0_1()); 
            match(input,RULE_CHARACTER,FOLLOW_RULE_CHARACTER_in_rule__Statement__Group_0__1__Impl2245); 
             after(grammarAccess.getStatementAccess().getCHARACTERTerminalRuleCall_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Statement__Group_0__1__Impl"


    // $ANTLR start "rule__IfTemplate__Group__0"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1137:1: rule__IfTemplate__Group__0 : rule__IfTemplate__Group__0__Impl rule__IfTemplate__Group__1 ;
    public final void rule__IfTemplate__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1141:1: ( rule__IfTemplate__Group__0__Impl rule__IfTemplate__Group__1 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1142:2: rule__IfTemplate__Group__0__Impl rule__IfTemplate__Group__1
            {
            pushFollow(FOLLOW_rule__IfTemplate__Group__0__Impl_in_rule__IfTemplate__Group__02278);
            rule__IfTemplate__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__IfTemplate__Group__1_in_rule__IfTemplate__Group__02281);
            rule__IfTemplate__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfTemplate__Group__0"


    // $ANTLR start "rule__IfTemplate__Group__0__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1149:1: rule__IfTemplate__Group__0__Impl : ( () ) ;
    public final void rule__IfTemplate__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1153:1: ( ( () ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1154:1: ( () )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1154:1: ( () )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1155:1: ()
            {
             before(grammarAccess.getIfTemplateAccess().getIfTemplateAction_0()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1156:1: ()
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1158:1: 
            {
            }

             after(grammarAccess.getIfTemplateAccess().getIfTemplateAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfTemplate__Group__0__Impl"


    // $ANTLR start "rule__IfTemplate__Group__1"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1168:1: rule__IfTemplate__Group__1 : rule__IfTemplate__Group__1__Impl rule__IfTemplate__Group__2 ;
    public final void rule__IfTemplate__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1172:1: ( rule__IfTemplate__Group__1__Impl rule__IfTemplate__Group__2 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1173:2: rule__IfTemplate__Group__1__Impl rule__IfTemplate__Group__2
            {
            pushFollow(FOLLOW_rule__IfTemplate__Group__1__Impl_in_rule__IfTemplate__Group__12339);
            rule__IfTemplate__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__IfTemplate__Group__2_in_rule__IfTemplate__Group__12342);
            rule__IfTemplate__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfTemplate__Group__1"


    // $ANTLR start "rule__IfTemplate__Group__1__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1180:1: rule__IfTemplate__Group__1__Impl : ( '\\u00AB' ) ;
    public final void rule__IfTemplate__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1184:1: ( ( '\\u00AB' ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1185:1: ( '\\u00AB' )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1185:1: ( '\\u00AB' )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1186:1: '\\u00AB'
            {
             before(grammarAccess.getIfTemplateAccess().getLeftPointingDoubleAngleQuotationMarkKeyword_1()); 
            match(input,22,FOLLOW_22_in_rule__IfTemplate__Group__1__Impl2370); 
             after(grammarAccess.getIfTemplateAccess().getLeftPointingDoubleAngleQuotationMarkKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfTemplate__Group__1__Impl"


    // $ANTLR start "rule__IfTemplate__Group__2"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1199:1: rule__IfTemplate__Group__2 : rule__IfTemplate__Group__2__Impl rule__IfTemplate__Group__3 ;
    public final void rule__IfTemplate__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1203:1: ( rule__IfTemplate__Group__2__Impl rule__IfTemplate__Group__3 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1204:2: rule__IfTemplate__Group__2__Impl rule__IfTemplate__Group__3
            {
            pushFollow(FOLLOW_rule__IfTemplate__Group__2__Impl_in_rule__IfTemplate__Group__22401);
            rule__IfTemplate__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__IfTemplate__Group__3_in_rule__IfTemplate__Group__22404);
            rule__IfTemplate__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfTemplate__Group__2"


    // $ANTLR start "rule__IfTemplate__Group__2__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1211:1: rule__IfTemplate__Group__2__Impl : ( 'if' ) ;
    public final void rule__IfTemplate__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1215:1: ( ( 'if' ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1216:1: ( 'if' )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1216:1: ( 'if' )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1217:1: 'if'
            {
             before(grammarAccess.getIfTemplateAccess().getIfKeyword_2()); 
            match(input,23,FOLLOW_23_in_rule__IfTemplate__Group__2__Impl2432); 
             after(grammarAccess.getIfTemplateAccess().getIfKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfTemplate__Group__2__Impl"


    // $ANTLR start "rule__IfTemplate__Group__3"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1230:1: rule__IfTemplate__Group__3 : rule__IfTemplate__Group__3__Impl rule__IfTemplate__Group__4 ;
    public final void rule__IfTemplate__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1234:1: ( rule__IfTemplate__Group__3__Impl rule__IfTemplate__Group__4 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1235:2: rule__IfTemplate__Group__3__Impl rule__IfTemplate__Group__4
            {
            pushFollow(FOLLOW_rule__IfTemplate__Group__3__Impl_in_rule__IfTemplate__Group__32463);
            rule__IfTemplate__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__IfTemplate__Group__4_in_rule__IfTemplate__Group__32466);
            rule__IfTemplate__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfTemplate__Group__3"


    // $ANTLR start "rule__IfTemplate__Group__3__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1242:1: rule__IfTemplate__Group__3__Impl : ( ( rule__IfTemplate__TemplateConditionAssignment_3 ) ) ;
    public final void rule__IfTemplate__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1246:1: ( ( ( rule__IfTemplate__TemplateConditionAssignment_3 ) ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1247:1: ( ( rule__IfTemplate__TemplateConditionAssignment_3 ) )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1247:1: ( ( rule__IfTemplate__TemplateConditionAssignment_3 ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1248:1: ( rule__IfTemplate__TemplateConditionAssignment_3 )
            {
             before(grammarAccess.getIfTemplateAccess().getTemplateConditionAssignment_3()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1249:1: ( rule__IfTemplate__TemplateConditionAssignment_3 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1249:2: rule__IfTemplate__TemplateConditionAssignment_3
            {
            pushFollow(FOLLOW_rule__IfTemplate__TemplateConditionAssignment_3_in_rule__IfTemplate__Group__3__Impl2493);
            rule__IfTemplate__TemplateConditionAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getIfTemplateAccess().getTemplateConditionAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfTemplate__Group__3__Impl"


    // $ANTLR start "rule__IfTemplate__Group__4"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1259:1: rule__IfTemplate__Group__4 : rule__IfTemplate__Group__4__Impl rule__IfTemplate__Group__5 ;
    public final void rule__IfTemplate__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1263:1: ( rule__IfTemplate__Group__4__Impl rule__IfTemplate__Group__5 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1264:2: rule__IfTemplate__Group__4__Impl rule__IfTemplate__Group__5
            {
            pushFollow(FOLLOW_rule__IfTemplate__Group__4__Impl_in_rule__IfTemplate__Group__42523);
            rule__IfTemplate__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__IfTemplate__Group__5_in_rule__IfTemplate__Group__42526);
            rule__IfTemplate__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfTemplate__Group__4"


    // $ANTLR start "rule__IfTemplate__Group__4__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1271:1: rule__IfTemplate__Group__4__Impl : ( '\\u00BB' ) ;
    public final void rule__IfTemplate__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1275:1: ( ( '\\u00BB' ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1276:1: ( '\\u00BB' )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1276:1: ( '\\u00BB' )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1277:1: '\\u00BB'
            {
             before(grammarAccess.getIfTemplateAccess().getRightPointingDoubleAngleQuotationMarkKeyword_4()); 
            match(input,19,FOLLOW_19_in_rule__IfTemplate__Group__4__Impl2554); 
             after(grammarAccess.getIfTemplateAccess().getRightPointingDoubleAngleQuotationMarkKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfTemplate__Group__4__Impl"


    // $ANTLR start "rule__IfTemplate__Group__5"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1290:1: rule__IfTemplate__Group__5 : rule__IfTemplate__Group__5__Impl rule__IfTemplate__Group__6 ;
    public final void rule__IfTemplate__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1294:1: ( rule__IfTemplate__Group__5__Impl rule__IfTemplate__Group__6 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1295:2: rule__IfTemplate__Group__5__Impl rule__IfTemplate__Group__6
            {
            pushFollow(FOLLOW_rule__IfTemplate__Group__5__Impl_in_rule__IfTemplate__Group__52585);
            rule__IfTemplate__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__IfTemplate__Group__6_in_rule__IfTemplate__Group__52588);
            rule__IfTemplate__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfTemplate__Group__5"


    // $ANTLR start "rule__IfTemplate__Group__5__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1302:1: rule__IfTemplate__Group__5__Impl : ( ( rule__IfTemplate__StatementsAssignment_5 )* ) ;
    public final void rule__IfTemplate__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1306:1: ( ( ( rule__IfTemplate__StatementsAssignment_5 )* ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1307:1: ( ( rule__IfTemplate__StatementsAssignment_5 )* )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1307:1: ( ( rule__IfTemplate__StatementsAssignment_5 )* )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1308:1: ( rule__IfTemplate__StatementsAssignment_5 )*
            {
             before(grammarAccess.getIfTemplateAccess().getStatementsAssignment_5()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1309:1: ( rule__IfTemplate__StatementsAssignment_5 )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( ((LA8_0>=RULE_ID && LA8_0<=RULE_CHARACTER)||LA8_0==22) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1309:2: rule__IfTemplate__StatementsAssignment_5
            	    {
            	    pushFollow(FOLLOW_rule__IfTemplate__StatementsAssignment_5_in_rule__IfTemplate__Group__5__Impl2615);
            	    rule__IfTemplate__StatementsAssignment_5();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

             after(grammarAccess.getIfTemplateAccess().getStatementsAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfTemplate__Group__5__Impl"


    // $ANTLR start "rule__IfTemplate__Group__6"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1319:1: rule__IfTemplate__Group__6 : rule__IfTemplate__Group__6__Impl ;
    public final void rule__IfTemplate__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1323:1: ( rule__IfTemplate__Group__6__Impl )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1324:2: rule__IfTemplate__Group__6__Impl
            {
            pushFollow(FOLLOW_rule__IfTemplate__Group__6__Impl_in_rule__IfTemplate__Group__62646);
            rule__IfTemplate__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfTemplate__Group__6"


    // $ANTLR start "rule__IfTemplate__Group__6__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1330:1: rule__IfTemplate__Group__6__Impl : ( '\\u00ABendIf\\u00BB' ) ;
    public final void rule__IfTemplate__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1334:1: ( ( '\\u00ABendIf\\u00BB' ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1335:1: ( '\\u00ABendIf\\u00BB' )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1335:1: ( '\\u00ABendIf\\u00BB' )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1336:1: '\\u00ABendIf\\u00BB'
            {
             before(grammarAccess.getIfTemplateAccess().getEndIfKeyword_6()); 
            match(input,24,FOLLOW_24_in_rule__IfTemplate__Group__6__Impl2674); 
             after(grammarAccess.getIfTemplateAccess().getEndIfKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfTemplate__Group__6__Impl"


    // $ANTLR start "rule__ForTemplate__Group__0"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1363:1: rule__ForTemplate__Group__0 : rule__ForTemplate__Group__0__Impl rule__ForTemplate__Group__1 ;
    public final void rule__ForTemplate__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1367:1: ( rule__ForTemplate__Group__0__Impl rule__ForTemplate__Group__1 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1368:2: rule__ForTemplate__Group__0__Impl rule__ForTemplate__Group__1
            {
            pushFollow(FOLLOW_rule__ForTemplate__Group__0__Impl_in_rule__ForTemplate__Group__02719);
            rule__ForTemplate__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ForTemplate__Group__1_in_rule__ForTemplate__Group__02722);
            rule__ForTemplate__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForTemplate__Group__0"


    // $ANTLR start "rule__ForTemplate__Group__0__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1375:1: rule__ForTemplate__Group__0__Impl : ( () ) ;
    public final void rule__ForTemplate__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1379:1: ( ( () ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1380:1: ( () )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1380:1: ( () )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1381:1: ()
            {
             before(grammarAccess.getForTemplateAccess().getForTemplateAction_0()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1382:1: ()
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1384:1: 
            {
            }

             after(grammarAccess.getForTemplateAccess().getForTemplateAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForTemplate__Group__0__Impl"


    // $ANTLR start "rule__ForTemplate__Group__1"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1394:1: rule__ForTemplate__Group__1 : rule__ForTemplate__Group__1__Impl rule__ForTemplate__Group__2 ;
    public final void rule__ForTemplate__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1398:1: ( rule__ForTemplate__Group__1__Impl rule__ForTemplate__Group__2 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1399:2: rule__ForTemplate__Group__1__Impl rule__ForTemplate__Group__2
            {
            pushFollow(FOLLOW_rule__ForTemplate__Group__1__Impl_in_rule__ForTemplate__Group__12780);
            rule__ForTemplate__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ForTemplate__Group__2_in_rule__ForTemplate__Group__12783);
            rule__ForTemplate__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForTemplate__Group__1"


    // $ANTLR start "rule__ForTemplate__Group__1__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1406:1: rule__ForTemplate__Group__1__Impl : ( '\\u00AB' ) ;
    public final void rule__ForTemplate__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1410:1: ( ( '\\u00AB' ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1411:1: ( '\\u00AB' )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1411:1: ( '\\u00AB' )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1412:1: '\\u00AB'
            {
             before(grammarAccess.getForTemplateAccess().getLeftPointingDoubleAngleQuotationMarkKeyword_1()); 
            match(input,22,FOLLOW_22_in_rule__ForTemplate__Group__1__Impl2811); 
             after(grammarAccess.getForTemplateAccess().getLeftPointingDoubleAngleQuotationMarkKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForTemplate__Group__1__Impl"


    // $ANTLR start "rule__ForTemplate__Group__2"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1425:1: rule__ForTemplate__Group__2 : rule__ForTemplate__Group__2__Impl rule__ForTemplate__Group__3 ;
    public final void rule__ForTemplate__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1429:1: ( rule__ForTemplate__Group__2__Impl rule__ForTemplate__Group__3 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1430:2: rule__ForTemplate__Group__2__Impl rule__ForTemplate__Group__3
            {
            pushFollow(FOLLOW_rule__ForTemplate__Group__2__Impl_in_rule__ForTemplate__Group__22842);
            rule__ForTemplate__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ForTemplate__Group__3_in_rule__ForTemplate__Group__22845);
            rule__ForTemplate__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForTemplate__Group__2"


    // $ANTLR start "rule__ForTemplate__Group__2__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1437:1: rule__ForTemplate__Group__2__Impl : ( 'for' ) ;
    public final void rule__ForTemplate__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1441:1: ( ( 'for' ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1442:1: ( 'for' )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1442:1: ( 'for' )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1443:1: 'for'
            {
             before(grammarAccess.getForTemplateAccess().getForKeyword_2()); 
            match(input,25,FOLLOW_25_in_rule__ForTemplate__Group__2__Impl2873); 
             after(grammarAccess.getForTemplateAccess().getForKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForTemplate__Group__2__Impl"


    // $ANTLR start "rule__ForTemplate__Group__3"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1456:1: rule__ForTemplate__Group__3 : rule__ForTemplate__Group__3__Impl rule__ForTemplate__Group__4 ;
    public final void rule__ForTemplate__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1460:1: ( rule__ForTemplate__Group__3__Impl rule__ForTemplate__Group__4 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1461:2: rule__ForTemplate__Group__3__Impl rule__ForTemplate__Group__4
            {
            pushFollow(FOLLOW_rule__ForTemplate__Group__3__Impl_in_rule__ForTemplate__Group__32904);
            rule__ForTemplate__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ForTemplate__Group__4_in_rule__ForTemplate__Group__32907);
            rule__ForTemplate__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForTemplate__Group__3"


    // $ANTLR start "rule__ForTemplate__Group__3__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1468:1: rule__ForTemplate__Group__3__Impl : ( '\\u00BB' ) ;
    public final void rule__ForTemplate__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1472:1: ( ( '\\u00BB' ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1473:1: ( '\\u00BB' )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1473:1: ( '\\u00BB' )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1474:1: '\\u00BB'
            {
             before(grammarAccess.getForTemplateAccess().getRightPointingDoubleAngleQuotationMarkKeyword_3()); 
            match(input,19,FOLLOW_19_in_rule__ForTemplate__Group__3__Impl2935); 
             after(grammarAccess.getForTemplateAccess().getRightPointingDoubleAngleQuotationMarkKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForTemplate__Group__3__Impl"


    // $ANTLR start "rule__ForTemplate__Group__4"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1487:1: rule__ForTemplate__Group__4 : rule__ForTemplate__Group__4__Impl rule__ForTemplate__Group__5 ;
    public final void rule__ForTemplate__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1491:1: ( rule__ForTemplate__Group__4__Impl rule__ForTemplate__Group__5 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1492:2: rule__ForTemplate__Group__4__Impl rule__ForTemplate__Group__5
            {
            pushFollow(FOLLOW_rule__ForTemplate__Group__4__Impl_in_rule__ForTemplate__Group__42966);
            rule__ForTemplate__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ForTemplate__Group__5_in_rule__ForTemplate__Group__42969);
            rule__ForTemplate__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForTemplate__Group__4"


    // $ANTLR start "rule__ForTemplate__Group__4__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1499:1: rule__ForTemplate__Group__4__Impl : ( ( rule__ForTemplate__StatementsAssignment_4 )* ) ;
    public final void rule__ForTemplate__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1503:1: ( ( ( rule__ForTemplate__StatementsAssignment_4 )* ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1504:1: ( ( rule__ForTemplate__StatementsAssignment_4 )* )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1504:1: ( ( rule__ForTemplate__StatementsAssignment_4 )* )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1505:1: ( rule__ForTemplate__StatementsAssignment_4 )*
            {
             before(grammarAccess.getForTemplateAccess().getStatementsAssignment_4()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1506:1: ( rule__ForTemplate__StatementsAssignment_4 )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( ((LA9_0>=RULE_ID && LA9_0<=RULE_CHARACTER)||LA9_0==22) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1506:2: rule__ForTemplate__StatementsAssignment_4
            	    {
            	    pushFollow(FOLLOW_rule__ForTemplate__StatementsAssignment_4_in_rule__ForTemplate__Group__4__Impl2996);
            	    rule__ForTemplate__StatementsAssignment_4();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

             after(grammarAccess.getForTemplateAccess().getStatementsAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForTemplate__Group__4__Impl"


    // $ANTLR start "rule__ForTemplate__Group__5"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1516:1: rule__ForTemplate__Group__5 : rule__ForTemplate__Group__5__Impl ;
    public final void rule__ForTemplate__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1520:1: ( rule__ForTemplate__Group__5__Impl )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1521:2: rule__ForTemplate__Group__5__Impl
            {
            pushFollow(FOLLOW_rule__ForTemplate__Group__5__Impl_in_rule__ForTemplate__Group__53027);
            rule__ForTemplate__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForTemplate__Group__5"


    // $ANTLR start "rule__ForTemplate__Group__5__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1527:1: rule__ForTemplate__Group__5__Impl : ( '\\u00ABendFor\\u00BB' ) ;
    public final void rule__ForTemplate__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1531:1: ( ( '\\u00ABendFor\\u00BB' ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1532:1: ( '\\u00ABendFor\\u00BB' )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1532:1: ( '\\u00ABendFor\\u00BB' )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1533:1: '\\u00ABendFor\\u00BB'
            {
             before(grammarAccess.getForTemplateAccess().getEndForKeyword_5()); 
            match(input,26,FOLLOW_26_in_rule__ForTemplate__Group__5__Impl3055); 
             after(grammarAccess.getForTemplateAccess().getEndForKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForTemplate__Group__5__Impl"


    // $ANTLR start "rule__TemplateReference__Group__0"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1558:1: rule__TemplateReference__Group__0 : rule__TemplateReference__Group__0__Impl rule__TemplateReference__Group__1 ;
    public final void rule__TemplateReference__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1562:1: ( rule__TemplateReference__Group__0__Impl rule__TemplateReference__Group__1 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1563:2: rule__TemplateReference__Group__0__Impl rule__TemplateReference__Group__1
            {
            pushFollow(FOLLOW_rule__TemplateReference__Group__0__Impl_in_rule__TemplateReference__Group__03098);
            rule__TemplateReference__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__TemplateReference__Group__1_in_rule__TemplateReference__Group__03101);
            rule__TemplateReference__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateReference__Group__0"


    // $ANTLR start "rule__TemplateReference__Group__0__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1570:1: rule__TemplateReference__Group__0__Impl : ( ( rule__TemplateReference__CallAssignment_0 ) ) ;
    public final void rule__TemplateReference__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1574:1: ( ( ( rule__TemplateReference__CallAssignment_0 ) ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1575:1: ( ( rule__TemplateReference__CallAssignment_0 ) )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1575:1: ( ( rule__TemplateReference__CallAssignment_0 ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1576:1: ( rule__TemplateReference__CallAssignment_0 )
            {
             before(grammarAccess.getTemplateReferenceAccess().getCallAssignment_0()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1577:1: ( rule__TemplateReference__CallAssignment_0 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1577:2: rule__TemplateReference__CallAssignment_0
            {
            pushFollow(FOLLOW_rule__TemplateReference__CallAssignment_0_in_rule__TemplateReference__Group__0__Impl3128);
            rule__TemplateReference__CallAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getTemplateReferenceAccess().getCallAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateReference__Group__0__Impl"


    // $ANTLR start "rule__TemplateReference__Group__1"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1587:1: rule__TemplateReference__Group__1 : rule__TemplateReference__Group__1__Impl rule__TemplateReference__Group__2 ;
    public final void rule__TemplateReference__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1591:1: ( rule__TemplateReference__Group__1__Impl rule__TemplateReference__Group__2 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1592:2: rule__TemplateReference__Group__1__Impl rule__TemplateReference__Group__2
            {
            pushFollow(FOLLOW_rule__TemplateReference__Group__1__Impl_in_rule__TemplateReference__Group__13158);
            rule__TemplateReference__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__TemplateReference__Group__2_in_rule__TemplateReference__Group__13161);
            rule__TemplateReference__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateReference__Group__1"


    // $ANTLR start "rule__TemplateReference__Group__1__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1599:1: rule__TemplateReference__Group__1__Impl : ( ( rule__TemplateReference__TypeAssignment_1 ) ) ;
    public final void rule__TemplateReference__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1603:1: ( ( ( rule__TemplateReference__TypeAssignment_1 ) ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1604:1: ( ( rule__TemplateReference__TypeAssignment_1 ) )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1604:1: ( ( rule__TemplateReference__TypeAssignment_1 ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1605:1: ( rule__TemplateReference__TypeAssignment_1 )
            {
             before(grammarAccess.getTemplateReferenceAccess().getTypeAssignment_1()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1606:1: ( rule__TemplateReference__TypeAssignment_1 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1606:2: rule__TemplateReference__TypeAssignment_1
            {
            pushFollow(FOLLOW_rule__TemplateReference__TypeAssignment_1_in_rule__TemplateReference__Group__1__Impl3188);
            rule__TemplateReference__TypeAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getTemplateReferenceAccess().getTypeAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateReference__Group__1__Impl"


    // $ANTLR start "rule__TemplateReference__Group__2"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1616:1: rule__TemplateReference__Group__2 : rule__TemplateReference__Group__2__Impl ;
    public final void rule__TemplateReference__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1620:1: ( rule__TemplateReference__Group__2__Impl )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1621:2: rule__TemplateReference__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__TemplateReference__Group__2__Impl_in_rule__TemplateReference__Group__23218);
            rule__TemplateReference__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateReference__Group__2"


    // $ANTLR start "rule__TemplateReference__Group__2__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1627:1: rule__TemplateReference__Group__2__Impl : ( '\\u00BB' ) ;
    public final void rule__TemplateReference__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1631:1: ( ( '\\u00BB' ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1632:1: ( '\\u00BB' )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1632:1: ( '\\u00BB' )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1633:1: '\\u00BB'
            {
             before(grammarAccess.getTemplateReferenceAccess().getRightPointingDoubleAngleQuotationMarkKeyword_2()); 
            match(input,19,FOLLOW_19_in_rule__TemplateReference__Group__2__Impl3246); 
             after(grammarAccess.getTemplateReferenceAccess().getRightPointingDoubleAngleQuotationMarkKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateReference__Group__2__Impl"


    // $ANTLR start "rule__Parameter__Group__0"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1652:1: rule__Parameter__Group__0 : rule__Parameter__Group__0__Impl rule__Parameter__Group__1 ;
    public final void rule__Parameter__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1656:1: ( rule__Parameter__Group__0__Impl rule__Parameter__Group__1 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1657:2: rule__Parameter__Group__0__Impl rule__Parameter__Group__1
            {
            pushFollow(FOLLOW_rule__Parameter__Group__0__Impl_in_rule__Parameter__Group__03283);
            rule__Parameter__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Parameter__Group__1_in_rule__Parameter__Group__03286);
            rule__Parameter__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__0"


    // $ANTLR start "rule__Parameter__Group__0__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1664:1: rule__Parameter__Group__0__Impl : ( ( rule__Parameter__ETypeAssignment_0 ) ) ;
    public final void rule__Parameter__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1668:1: ( ( ( rule__Parameter__ETypeAssignment_0 ) ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1669:1: ( ( rule__Parameter__ETypeAssignment_0 ) )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1669:1: ( ( rule__Parameter__ETypeAssignment_0 ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1670:1: ( rule__Parameter__ETypeAssignment_0 )
            {
             before(grammarAccess.getParameterAccess().getETypeAssignment_0()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1671:1: ( rule__Parameter__ETypeAssignment_0 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1671:2: rule__Parameter__ETypeAssignment_0
            {
            pushFollow(FOLLOW_rule__Parameter__ETypeAssignment_0_in_rule__Parameter__Group__0__Impl3313);
            rule__Parameter__ETypeAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getParameterAccess().getETypeAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__0__Impl"


    // $ANTLR start "rule__Parameter__Group__1"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1681:1: rule__Parameter__Group__1 : rule__Parameter__Group__1__Impl ;
    public final void rule__Parameter__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1685:1: ( rule__Parameter__Group__1__Impl )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1686:2: rule__Parameter__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Parameter__Group__1__Impl_in_rule__Parameter__Group__13343);
            rule__Parameter__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__1"


    // $ANTLR start "rule__Parameter__Group__1__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1692:1: rule__Parameter__Group__1__Impl : ( ( rule__Parameter__NameAssignment_1 ) ) ;
    public final void rule__Parameter__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1696:1: ( ( ( rule__Parameter__NameAssignment_1 ) ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1697:1: ( ( rule__Parameter__NameAssignment_1 ) )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1697:1: ( ( rule__Parameter__NameAssignment_1 ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1698:1: ( rule__Parameter__NameAssignment_1 )
            {
             before(grammarAccess.getParameterAccess().getNameAssignment_1()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1699:1: ( rule__Parameter__NameAssignment_1 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1699:2: rule__Parameter__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__Parameter__NameAssignment_1_in_rule__Parameter__Group__1__Impl3370);
            rule__Parameter__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getParameterAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__1__Impl"


    // $ANTLR start "rule__Reference__Group__0"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1713:1: rule__Reference__Group__0 : rule__Reference__Group__0__Impl rule__Reference__Group__1 ;
    public final void rule__Reference__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1717:1: ( rule__Reference__Group__0__Impl rule__Reference__Group__1 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1718:2: rule__Reference__Group__0__Impl rule__Reference__Group__1
            {
            pushFollow(FOLLOW_rule__Reference__Group__0__Impl_in_rule__Reference__Group__03404);
            rule__Reference__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Reference__Group__1_in_rule__Reference__Group__03407);
            rule__Reference__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__0"


    // $ANTLR start "rule__Reference__Group__0__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1725:1: rule__Reference__Group__0__Impl : ( ( rule__Reference__ReferencedElementAssignment_0 ) ) ;
    public final void rule__Reference__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1729:1: ( ( ( rule__Reference__ReferencedElementAssignment_0 ) ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1730:1: ( ( rule__Reference__ReferencedElementAssignment_0 ) )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1730:1: ( ( rule__Reference__ReferencedElementAssignment_0 ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1731:1: ( rule__Reference__ReferencedElementAssignment_0 )
            {
             before(grammarAccess.getReferenceAccess().getReferencedElementAssignment_0()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1732:1: ( rule__Reference__ReferencedElementAssignment_0 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1732:2: rule__Reference__ReferencedElementAssignment_0
            {
            pushFollow(FOLLOW_rule__Reference__ReferencedElementAssignment_0_in_rule__Reference__Group__0__Impl3434);
            rule__Reference__ReferencedElementAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getReferenceAccess().getReferencedElementAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__0__Impl"


    // $ANTLR start "rule__Reference__Group__1"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1742:1: rule__Reference__Group__1 : rule__Reference__Group__1__Impl ;
    public final void rule__Reference__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1746:1: ( rule__Reference__Group__1__Impl )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1747:2: rule__Reference__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Reference__Group__1__Impl_in_rule__Reference__Group__13464);
            rule__Reference__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__1"


    // $ANTLR start "rule__Reference__Group__1__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1753:1: rule__Reference__Group__1__Impl : ( ( rule__Reference__TailAssignment_1 )? ) ;
    public final void rule__Reference__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1757:1: ( ( ( rule__Reference__TailAssignment_1 )? ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1758:1: ( ( rule__Reference__TailAssignment_1 )? )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1758:1: ( ( rule__Reference__TailAssignment_1 )? )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1759:1: ( rule__Reference__TailAssignment_1 )?
            {
             before(grammarAccess.getReferenceAccess().getTailAssignment_1()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1760:1: ( rule__Reference__TailAssignment_1 )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==27) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1760:2: rule__Reference__TailAssignment_1
                    {
                    pushFollow(FOLLOW_rule__Reference__TailAssignment_1_in_rule__Reference__Group__1__Impl3491);
                    rule__Reference__TailAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getReferenceAccess().getTailAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__1__Impl"


    // $ANTLR start "rule__ReferenceTail__Group__0"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1774:1: rule__ReferenceTail__Group__0 : rule__ReferenceTail__Group__0__Impl rule__ReferenceTail__Group__1 ;
    public final void rule__ReferenceTail__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1778:1: ( rule__ReferenceTail__Group__0__Impl rule__ReferenceTail__Group__1 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1779:2: rule__ReferenceTail__Group__0__Impl rule__ReferenceTail__Group__1
            {
            pushFollow(FOLLOW_rule__ReferenceTail__Group__0__Impl_in_rule__ReferenceTail__Group__03526);
            rule__ReferenceTail__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ReferenceTail__Group__1_in_rule__ReferenceTail__Group__03529);
            rule__ReferenceTail__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferenceTail__Group__0"


    // $ANTLR start "rule__ReferenceTail__Group__0__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1786:1: rule__ReferenceTail__Group__0__Impl : ( '.' ) ;
    public final void rule__ReferenceTail__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1790:1: ( ( '.' ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1791:1: ( '.' )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1791:1: ( '.' )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1792:1: '.'
            {
             before(grammarAccess.getReferenceTailAccess().getFullStopKeyword_0()); 
            match(input,27,FOLLOW_27_in_rule__ReferenceTail__Group__0__Impl3557); 
             after(grammarAccess.getReferenceTailAccess().getFullStopKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferenceTail__Group__0__Impl"


    // $ANTLR start "rule__ReferenceTail__Group__1"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1805:1: rule__ReferenceTail__Group__1 : rule__ReferenceTail__Group__1__Impl rule__ReferenceTail__Group__2 ;
    public final void rule__ReferenceTail__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1809:1: ( rule__ReferenceTail__Group__1__Impl rule__ReferenceTail__Group__2 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1810:2: rule__ReferenceTail__Group__1__Impl rule__ReferenceTail__Group__2
            {
            pushFollow(FOLLOW_rule__ReferenceTail__Group__1__Impl_in_rule__ReferenceTail__Group__13588);
            rule__ReferenceTail__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ReferenceTail__Group__2_in_rule__ReferenceTail__Group__13591);
            rule__ReferenceTail__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferenceTail__Group__1"


    // $ANTLR start "rule__ReferenceTail__Group__1__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1817:1: rule__ReferenceTail__Group__1__Impl : ( ( rule__ReferenceTail__ReferencedElementAssignment_1 ) ) ;
    public final void rule__ReferenceTail__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1821:1: ( ( ( rule__ReferenceTail__ReferencedElementAssignment_1 ) ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1822:1: ( ( rule__ReferenceTail__ReferencedElementAssignment_1 ) )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1822:1: ( ( rule__ReferenceTail__ReferencedElementAssignment_1 ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1823:1: ( rule__ReferenceTail__ReferencedElementAssignment_1 )
            {
             before(grammarAccess.getReferenceTailAccess().getReferencedElementAssignment_1()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1824:1: ( rule__ReferenceTail__ReferencedElementAssignment_1 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1824:2: rule__ReferenceTail__ReferencedElementAssignment_1
            {
            pushFollow(FOLLOW_rule__ReferenceTail__ReferencedElementAssignment_1_in_rule__ReferenceTail__Group__1__Impl3618);
            rule__ReferenceTail__ReferencedElementAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getReferenceTailAccess().getReferencedElementAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferenceTail__Group__1__Impl"


    // $ANTLR start "rule__ReferenceTail__Group__2"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1834:1: rule__ReferenceTail__Group__2 : rule__ReferenceTail__Group__2__Impl ;
    public final void rule__ReferenceTail__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1838:1: ( rule__ReferenceTail__Group__2__Impl )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1839:2: rule__ReferenceTail__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__ReferenceTail__Group__2__Impl_in_rule__ReferenceTail__Group__23648);
            rule__ReferenceTail__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferenceTail__Group__2"


    // $ANTLR start "rule__ReferenceTail__Group__2__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1845:1: rule__ReferenceTail__Group__2__Impl : ( ( rule__ReferenceTail__TailAssignment_2 )? ) ;
    public final void rule__ReferenceTail__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1849:1: ( ( ( rule__ReferenceTail__TailAssignment_2 )? ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1850:1: ( ( rule__ReferenceTail__TailAssignment_2 )? )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1850:1: ( ( rule__ReferenceTail__TailAssignment_2 )? )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1851:1: ( rule__ReferenceTail__TailAssignment_2 )?
            {
             before(grammarAccess.getReferenceTailAccess().getTailAssignment_2()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1852:1: ( rule__ReferenceTail__TailAssignment_2 )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==27) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1852:2: rule__ReferenceTail__TailAssignment_2
                    {
                    pushFollow(FOLLOW_rule__ReferenceTail__TailAssignment_2_in_rule__ReferenceTail__Group__2__Impl3675);
                    rule__ReferenceTail__TailAssignment_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getReferenceTailAccess().getTailAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferenceTail__Group__2__Impl"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group__0"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1868:1: rule__QualifiedNameWithWildcard__Group__0 : rule__QualifiedNameWithWildcard__Group__0__Impl rule__QualifiedNameWithWildcard__Group__1 ;
    public final void rule__QualifiedNameWithWildcard__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1872:1: ( rule__QualifiedNameWithWildcard__Group__0__Impl rule__QualifiedNameWithWildcard__Group__1 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1873:2: rule__QualifiedNameWithWildcard__Group__0__Impl rule__QualifiedNameWithWildcard__Group__1
            {
            pushFollow(FOLLOW_rule__QualifiedNameWithWildcard__Group__0__Impl_in_rule__QualifiedNameWithWildcard__Group__03712);
            rule__QualifiedNameWithWildcard__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__QualifiedNameWithWildcard__Group__1_in_rule__QualifiedNameWithWildcard__Group__03715);
            rule__QualifiedNameWithWildcard__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group__0"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group__0__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1880:1: rule__QualifiedNameWithWildcard__Group__0__Impl : ( ruleQUALIFIED_NAME ) ;
    public final void rule__QualifiedNameWithWildcard__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1884:1: ( ( ruleQUALIFIED_NAME ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1885:1: ( ruleQUALIFIED_NAME )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1885:1: ( ruleQUALIFIED_NAME )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1886:1: ruleQUALIFIED_NAME
            {
             before(grammarAccess.getQualifiedNameWithWildcardAccess().getQUALIFIED_NAMEParserRuleCall_0()); 
            pushFollow(FOLLOW_ruleQUALIFIED_NAME_in_rule__QualifiedNameWithWildcard__Group__0__Impl3742);
            ruleQUALIFIED_NAME();

            state._fsp--;

             after(grammarAccess.getQualifiedNameWithWildcardAccess().getQUALIFIED_NAMEParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group__0__Impl"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group__1"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1897:1: rule__QualifiedNameWithWildcard__Group__1 : rule__QualifiedNameWithWildcard__Group__1__Impl ;
    public final void rule__QualifiedNameWithWildcard__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1901:1: ( rule__QualifiedNameWithWildcard__Group__1__Impl )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1902:2: rule__QualifiedNameWithWildcard__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__QualifiedNameWithWildcard__Group__1__Impl_in_rule__QualifiedNameWithWildcard__Group__13771);
            rule__QualifiedNameWithWildcard__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group__1"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group__1__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1908:1: rule__QualifiedNameWithWildcard__Group__1__Impl : ( ( '.*' )? ) ;
    public final void rule__QualifiedNameWithWildcard__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1912:1: ( ( ( '.*' )? ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1913:1: ( ( '.*' )? )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1913:1: ( ( '.*' )? )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1914:1: ( '.*' )?
            {
             before(grammarAccess.getQualifiedNameWithWildcardAccess().getFullStopAsteriskKeyword_1()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1915:1: ( '.*' )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==28) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1916:2: '.*'
                    {
                    match(input,28,FOLLOW_28_in_rule__QualifiedNameWithWildcard__Group__1__Impl3800); 

                    }
                    break;

            }

             after(grammarAccess.getQualifiedNameWithWildcardAccess().getFullStopAsteriskKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group__1__Impl"


    // $ANTLR start "rule__QUALIFIED_NAME__Group__0"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1931:1: rule__QUALIFIED_NAME__Group__0 : rule__QUALIFIED_NAME__Group__0__Impl rule__QUALIFIED_NAME__Group__1 ;
    public final void rule__QUALIFIED_NAME__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1935:1: ( rule__QUALIFIED_NAME__Group__0__Impl rule__QUALIFIED_NAME__Group__1 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1936:2: rule__QUALIFIED_NAME__Group__0__Impl rule__QUALIFIED_NAME__Group__1
            {
            pushFollow(FOLLOW_rule__QUALIFIED_NAME__Group__0__Impl_in_rule__QUALIFIED_NAME__Group__03837);
            rule__QUALIFIED_NAME__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__QUALIFIED_NAME__Group__1_in_rule__QUALIFIED_NAME__Group__03840);
            rule__QUALIFIED_NAME__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QUALIFIED_NAME__Group__0"


    // $ANTLR start "rule__QUALIFIED_NAME__Group__0__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1943:1: rule__QUALIFIED_NAME__Group__0__Impl : ( RULE_ID ) ;
    public final void rule__QUALIFIED_NAME__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1947:1: ( ( RULE_ID ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1948:1: ( RULE_ID )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1948:1: ( RULE_ID )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1949:1: RULE_ID
            {
             before(grammarAccess.getQUALIFIED_NAMEAccess().getIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__QUALIFIED_NAME__Group__0__Impl3867); 
             after(grammarAccess.getQUALIFIED_NAMEAccess().getIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QUALIFIED_NAME__Group__0__Impl"


    // $ANTLR start "rule__QUALIFIED_NAME__Group__1"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1960:1: rule__QUALIFIED_NAME__Group__1 : rule__QUALIFIED_NAME__Group__1__Impl ;
    public final void rule__QUALIFIED_NAME__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1964:1: ( rule__QUALIFIED_NAME__Group__1__Impl )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1965:2: rule__QUALIFIED_NAME__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__QUALIFIED_NAME__Group__1__Impl_in_rule__QUALIFIED_NAME__Group__13896);
            rule__QUALIFIED_NAME__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QUALIFIED_NAME__Group__1"


    // $ANTLR start "rule__QUALIFIED_NAME__Group__1__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1971:1: rule__QUALIFIED_NAME__Group__1__Impl : ( ( rule__QUALIFIED_NAME__Group_1__0 )* ) ;
    public final void rule__QUALIFIED_NAME__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1975:1: ( ( ( rule__QUALIFIED_NAME__Group_1__0 )* ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1976:1: ( ( rule__QUALIFIED_NAME__Group_1__0 )* )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1976:1: ( ( rule__QUALIFIED_NAME__Group_1__0 )* )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1977:1: ( rule__QUALIFIED_NAME__Group_1__0 )*
            {
             before(grammarAccess.getQUALIFIED_NAMEAccess().getGroup_1()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1978:1: ( rule__QUALIFIED_NAME__Group_1__0 )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==27) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1978:2: rule__QUALIFIED_NAME__Group_1__0
            	    {
            	    pushFollow(FOLLOW_rule__QUALIFIED_NAME__Group_1__0_in_rule__QUALIFIED_NAME__Group__1__Impl3923);
            	    rule__QUALIFIED_NAME__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

             after(grammarAccess.getQUALIFIED_NAMEAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QUALIFIED_NAME__Group__1__Impl"


    // $ANTLR start "rule__QUALIFIED_NAME__Group_1__0"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1992:1: rule__QUALIFIED_NAME__Group_1__0 : rule__QUALIFIED_NAME__Group_1__0__Impl rule__QUALIFIED_NAME__Group_1__1 ;
    public final void rule__QUALIFIED_NAME__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1996:1: ( rule__QUALIFIED_NAME__Group_1__0__Impl rule__QUALIFIED_NAME__Group_1__1 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:1997:2: rule__QUALIFIED_NAME__Group_1__0__Impl rule__QUALIFIED_NAME__Group_1__1
            {
            pushFollow(FOLLOW_rule__QUALIFIED_NAME__Group_1__0__Impl_in_rule__QUALIFIED_NAME__Group_1__03958);
            rule__QUALIFIED_NAME__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__QUALIFIED_NAME__Group_1__1_in_rule__QUALIFIED_NAME__Group_1__03961);
            rule__QUALIFIED_NAME__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QUALIFIED_NAME__Group_1__0"


    // $ANTLR start "rule__QUALIFIED_NAME__Group_1__0__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2004:1: rule__QUALIFIED_NAME__Group_1__0__Impl : ( '.' ) ;
    public final void rule__QUALIFIED_NAME__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2008:1: ( ( '.' ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2009:1: ( '.' )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2009:1: ( '.' )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2010:1: '.'
            {
             before(grammarAccess.getQUALIFIED_NAMEAccess().getFullStopKeyword_1_0()); 
            match(input,27,FOLLOW_27_in_rule__QUALIFIED_NAME__Group_1__0__Impl3989); 
             after(grammarAccess.getQUALIFIED_NAMEAccess().getFullStopKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QUALIFIED_NAME__Group_1__0__Impl"


    // $ANTLR start "rule__QUALIFIED_NAME__Group_1__1"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2023:1: rule__QUALIFIED_NAME__Group_1__1 : rule__QUALIFIED_NAME__Group_1__1__Impl ;
    public final void rule__QUALIFIED_NAME__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2027:1: ( rule__QUALIFIED_NAME__Group_1__1__Impl )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2028:2: rule__QUALIFIED_NAME__Group_1__1__Impl
            {
            pushFollow(FOLLOW_rule__QUALIFIED_NAME__Group_1__1__Impl_in_rule__QUALIFIED_NAME__Group_1__14020);
            rule__QUALIFIED_NAME__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QUALIFIED_NAME__Group_1__1"


    // $ANTLR start "rule__QUALIFIED_NAME__Group_1__1__Impl"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2034:1: rule__QUALIFIED_NAME__Group_1__1__Impl : ( RULE_ID ) ;
    public final void rule__QUALIFIED_NAME__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2038:1: ( ( RULE_ID ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2039:1: ( RULE_ID )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2039:1: ( RULE_ID )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2040:1: RULE_ID
            {
             before(grammarAccess.getQUALIFIED_NAMEAccess().getIDTerminalRuleCall_1_1()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__QUALIFIED_NAME__Group_1__1__Impl4047); 
             after(grammarAccess.getQUALIFIED_NAMEAccess().getIDTerminalRuleCall_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QUALIFIED_NAME__Group_1__1__Impl"


    // $ANTLR start "rule__Template__ImportsAssignment_0"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2056:1: rule__Template__ImportsAssignment_0 : ( ruleImport ) ;
    public final void rule__Template__ImportsAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2060:1: ( ( ruleImport ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2061:1: ( ruleImport )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2061:1: ( ruleImport )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2062:1: ruleImport
            {
             before(grammarAccess.getTemplateAccess().getImportsImportParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleImport_in_rule__Template__ImportsAssignment_04085);
            ruleImport();

            state._fsp--;

             after(grammarAccess.getTemplateAccess().getImportsImportParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Template__ImportsAssignment_0"


    // $ANTLR start "rule__Template__TemplateFunctionsAssignment_1"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2071:1: rule__Template__TemplateFunctionsAssignment_1 : ( ruleTemplateFunction ) ;
    public final void rule__Template__TemplateFunctionsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2075:1: ( ( ruleTemplateFunction ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2076:1: ( ruleTemplateFunction )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2076:1: ( ruleTemplateFunction )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2077:1: ruleTemplateFunction
            {
             before(grammarAccess.getTemplateAccess().getTemplateFunctionsTemplateFunctionParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleTemplateFunction_in_rule__Template__TemplateFunctionsAssignment_14116);
            ruleTemplateFunction();

            state._fsp--;

             after(grammarAccess.getTemplateAccess().getTemplateFunctionsTemplateFunctionParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Template__TemplateFunctionsAssignment_1"


    // $ANTLR start "rule__Import__ImportedNamespaceAssignment_1"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2086:1: rule__Import__ImportedNamespaceAssignment_1 : ( ruleQualifiedNameWithWildcard ) ;
    public final void rule__Import__ImportedNamespaceAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2090:1: ( ( ruleQualifiedNameWithWildcard ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2091:1: ( ruleQualifiedNameWithWildcard )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2091:1: ( ruleQualifiedNameWithWildcard )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2092:1: ruleQualifiedNameWithWildcard
            {
             before(grammarAccess.getImportAccess().getImportedNamespaceQualifiedNameWithWildcardParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleQualifiedNameWithWildcard_in_rule__Import__ImportedNamespaceAssignment_14147);
            ruleQualifiedNameWithWildcard();

            state._fsp--;

             after(grammarAccess.getImportAccess().getImportedNamespaceQualifiedNameWithWildcardParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__ImportedNamespaceAssignment_1"


    // $ANTLR start "rule__TemplateFunction__NameAssignment_1"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2101:1: rule__TemplateFunction__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__TemplateFunction__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2105:1: ( ( RULE_ID ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2106:1: ( RULE_ID )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2106:1: ( RULE_ID )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2107:1: RULE_ID
            {
             before(grammarAccess.getTemplateFunctionAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__TemplateFunction__NameAssignment_14178); 
             after(grammarAccess.getTemplateFunctionAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateFunction__NameAssignment_1"


    // $ANTLR start "rule__TemplateFunction__ParametersAssignment_3"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2116:1: rule__TemplateFunction__ParametersAssignment_3 : ( ruleParameter ) ;
    public final void rule__TemplateFunction__ParametersAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2120:1: ( ( ruleParameter ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2121:1: ( ruleParameter )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2121:1: ( ruleParameter )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2122:1: ruleParameter
            {
             before(grammarAccess.getTemplateFunctionAccess().getParametersParameterParserRuleCall_3_0()); 
            pushFollow(FOLLOW_ruleParameter_in_rule__TemplateFunction__ParametersAssignment_34209);
            ruleParameter();

            state._fsp--;

             after(grammarAccess.getTemplateFunctionAccess().getParametersParameterParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateFunction__ParametersAssignment_3"


    // $ANTLR start "rule__TemplateFunction__ParametersAssignment_4_1"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2131:1: rule__TemplateFunction__ParametersAssignment_4_1 : ( ruleParameter ) ;
    public final void rule__TemplateFunction__ParametersAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2135:1: ( ( ruleParameter ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2136:1: ( ruleParameter )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2136:1: ( ruleParameter )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2137:1: ruleParameter
            {
             before(grammarAccess.getTemplateFunctionAccess().getParametersParameterParserRuleCall_4_1_0()); 
            pushFollow(FOLLOW_ruleParameter_in_rule__TemplateFunction__ParametersAssignment_4_14240);
            ruleParameter();

            state._fsp--;

             after(grammarAccess.getTemplateFunctionAccess().getParametersParameterParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateFunction__ParametersAssignment_4_1"


    // $ANTLR start "rule__TemplateFunction__StatementsAssignment_7"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2146:1: rule__TemplateFunction__StatementsAssignment_7 : ( ruleStatement ) ;
    public final void rule__TemplateFunction__StatementsAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2150:1: ( ( ruleStatement ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2151:1: ( ruleStatement )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2151:1: ( ruleStatement )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2152:1: ruleStatement
            {
             before(grammarAccess.getTemplateFunctionAccess().getStatementsStatementParserRuleCall_7_0()); 
            pushFollow(FOLLOW_ruleStatement_in_rule__TemplateFunction__StatementsAssignment_74271);
            ruleStatement();

            state._fsp--;

             after(grammarAccess.getTemplateFunctionAccess().getStatementsStatementParserRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateFunction__StatementsAssignment_7"


    // $ANTLR start "rule__IfTemplate__TemplateConditionAssignment_3"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2161:1: rule__IfTemplate__TemplateConditionAssignment_3 : ( ruleBoolValue ) ;
    public final void rule__IfTemplate__TemplateConditionAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2165:1: ( ( ruleBoolValue ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2166:1: ( ruleBoolValue )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2166:1: ( ruleBoolValue )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2167:1: ruleBoolValue
            {
             before(grammarAccess.getIfTemplateAccess().getTemplateConditionBoolValueParserRuleCall_3_0()); 
            pushFollow(FOLLOW_ruleBoolValue_in_rule__IfTemplate__TemplateConditionAssignment_34302);
            ruleBoolValue();

            state._fsp--;

             after(grammarAccess.getIfTemplateAccess().getTemplateConditionBoolValueParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfTemplate__TemplateConditionAssignment_3"


    // $ANTLR start "rule__IfTemplate__StatementsAssignment_5"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2176:1: rule__IfTemplate__StatementsAssignment_5 : ( ruleStatement ) ;
    public final void rule__IfTemplate__StatementsAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2180:1: ( ( ruleStatement ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2181:1: ( ruleStatement )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2181:1: ( ruleStatement )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2182:1: ruleStatement
            {
             before(grammarAccess.getIfTemplateAccess().getStatementsStatementParserRuleCall_5_0()); 
            pushFollow(FOLLOW_ruleStatement_in_rule__IfTemplate__StatementsAssignment_54333);
            ruleStatement();

            state._fsp--;

             after(grammarAccess.getIfTemplateAccess().getStatementsStatementParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfTemplate__StatementsAssignment_5"


    // $ANTLR start "rule__ForTemplate__StatementsAssignment_4"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2191:1: rule__ForTemplate__StatementsAssignment_4 : ( ruleStatement ) ;
    public final void rule__ForTemplate__StatementsAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2195:1: ( ( ruleStatement ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2196:1: ( ruleStatement )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2196:1: ( ruleStatement )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2197:1: ruleStatement
            {
             before(grammarAccess.getForTemplateAccess().getStatementsStatementParserRuleCall_4_0()); 
            pushFollow(FOLLOW_ruleStatement_in_rule__ForTemplate__StatementsAssignment_44364);
            ruleStatement();

            state._fsp--;

             after(grammarAccess.getForTemplateAccess().getStatementsStatementParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForTemplate__StatementsAssignment_4"


    // $ANTLR start "rule__TemplateReference__CallAssignment_0"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2206:1: rule__TemplateReference__CallAssignment_0 : ( ( '\\u00AB' ) ) ;
    public final void rule__TemplateReference__CallAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2210:1: ( ( ( '\\u00AB' ) ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2211:1: ( ( '\\u00AB' ) )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2211:1: ( ( '\\u00AB' ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2212:1: ( '\\u00AB' )
            {
             before(grammarAccess.getTemplateReferenceAccess().getCallLeftPointingDoubleAngleQuotationMarkKeyword_0_0()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2213:1: ( '\\u00AB' )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2214:1: '\\u00AB'
            {
             before(grammarAccess.getTemplateReferenceAccess().getCallLeftPointingDoubleAngleQuotationMarkKeyword_0_0()); 
            match(input,22,FOLLOW_22_in_rule__TemplateReference__CallAssignment_04400); 
             after(grammarAccess.getTemplateReferenceAccess().getCallLeftPointingDoubleAngleQuotationMarkKeyword_0_0()); 

            }

             after(grammarAccess.getTemplateReferenceAccess().getCallLeftPointingDoubleAngleQuotationMarkKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateReference__CallAssignment_0"


    // $ANTLR start "rule__TemplateReference__TypeAssignment_1"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2229:1: rule__TemplateReference__TypeAssignment_1 : ( ruleReference ) ;
    public final void rule__TemplateReference__TypeAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2233:1: ( ( ruleReference ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2234:1: ( ruleReference )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2234:1: ( ruleReference )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2235:1: ruleReference
            {
             before(grammarAccess.getTemplateReferenceAccess().getTypeReferenceParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleReference_in_rule__TemplateReference__TypeAssignment_14439);
            ruleReference();

            state._fsp--;

             after(grammarAccess.getTemplateReferenceAccess().getTypeReferenceParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TemplateReference__TypeAssignment_1"


    // $ANTLR start "rule__Parameter__ETypeAssignment_0"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2244:1: rule__Parameter__ETypeAssignment_0 : ( ( ruleQUALIFIED_NAME ) ) ;
    public final void rule__Parameter__ETypeAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2248:1: ( ( ( ruleQUALIFIED_NAME ) ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2249:1: ( ( ruleQUALIFIED_NAME ) )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2249:1: ( ( ruleQUALIFIED_NAME ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2250:1: ( ruleQUALIFIED_NAME )
            {
             before(grammarAccess.getParameterAccess().getETypeEClassifierCrossReference_0_0()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2251:1: ( ruleQUALIFIED_NAME )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2252:1: ruleQUALIFIED_NAME
            {
             before(grammarAccess.getParameterAccess().getETypeEClassifierQUALIFIED_NAMEParserRuleCall_0_0_1()); 
            pushFollow(FOLLOW_ruleQUALIFIED_NAME_in_rule__Parameter__ETypeAssignment_04474);
            ruleQUALIFIED_NAME();

            state._fsp--;

             after(grammarAccess.getParameterAccess().getETypeEClassifierQUALIFIED_NAMEParserRuleCall_0_0_1()); 

            }

             after(grammarAccess.getParameterAccess().getETypeEClassifierCrossReference_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__ETypeAssignment_0"


    // $ANTLR start "rule__Parameter__NameAssignment_1"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2263:1: rule__Parameter__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Parameter__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2267:1: ( ( RULE_ID ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2268:1: ( RULE_ID )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2268:1: ( RULE_ID )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2269:1: RULE_ID
            {
             before(grammarAccess.getParameterAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Parameter__NameAssignment_14509); 
             after(grammarAccess.getParameterAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__NameAssignment_1"


    // $ANTLR start "rule__Reference__ReferencedElementAssignment_0"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2278:1: rule__Reference__ReferencedElementAssignment_0 : ( ( RULE_ID ) ) ;
    public final void rule__Reference__ReferencedElementAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2282:1: ( ( ( RULE_ID ) ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2283:1: ( ( RULE_ID ) )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2283:1: ( ( RULE_ID ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2284:1: ( RULE_ID )
            {
             before(grammarAccess.getReferenceAccess().getReferencedElementETypedElementCrossReference_0_0()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2285:1: ( RULE_ID )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2286:1: RULE_ID
            {
             before(grammarAccess.getReferenceAccess().getReferencedElementETypedElementIDTerminalRuleCall_0_0_1()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Reference__ReferencedElementAssignment_04544); 
             after(grammarAccess.getReferenceAccess().getReferencedElementETypedElementIDTerminalRuleCall_0_0_1()); 

            }

             after(grammarAccess.getReferenceAccess().getReferencedElementETypedElementCrossReference_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__ReferencedElementAssignment_0"


    // $ANTLR start "rule__Reference__TailAssignment_1"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2297:1: rule__Reference__TailAssignment_1 : ( ruleReferenceTail ) ;
    public final void rule__Reference__TailAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2301:1: ( ( ruleReferenceTail ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2302:1: ( ruleReferenceTail )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2302:1: ( ruleReferenceTail )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2303:1: ruleReferenceTail
            {
             before(grammarAccess.getReferenceAccess().getTailReferenceTailParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleReferenceTail_in_rule__Reference__TailAssignment_14579);
            ruleReferenceTail();

            state._fsp--;

             after(grammarAccess.getReferenceAccess().getTailReferenceTailParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__TailAssignment_1"


    // $ANTLR start "rule__ReferenceTail__ReferencedElementAssignment_1"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2312:1: rule__ReferenceTail__ReferencedElementAssignment_1 : ( ( RULE_ID ) ) ;
    public final void rule__ReferenceTail__ReferencedElementAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2316:1: ( ( ( RULE_ID ) ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2317:1: ( ( RULE_ID ) )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2317:1: ( ( RULE_ID ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2318:1: ( RULE_ID )
            {
             before(grammarAccess.getReferenceTailAccess().getReferencedElementETypedElementCrossReference_1_0()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2319:1: ( RULE_ID )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2320:1: RULE_ID
            {
             before(grammarAccess.getReferenceTailAccess().getReferencedElementETypedElementIDTerminalRuleCall_1_0_1()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__ReferenceTail__ReferencedElementAssignment_14614); 
             after(grammarAccess.getReferenceTailAccess().getReferencedElementETypedElementIDTerminalRuleCall_1_0_1()); 

            }

             after(grammarAccess.getReferenceTailAccess().getReferencedElementETypedElementCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferenceTail__ReferencedElementAssignment_1"


    // $ANTLR start "rule__ReferenceTail__TailAssignment_2"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2331:1: rule__ReferenceTail__TailAssignment_2 : ( ruleReferenceTail ) ;
    public final void rule__ReferenceTail__TailAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2335:1: ( ( ruleReferenceTail ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2336:1: ( ruleReferenceTail )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2336:1: ( ruleReferenceTail )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2337:1: ruleReferenceTail
            {
             before(grammarAccess.getReferenceTailAccess().getTailReferenceTailParserRuleCall_2_0()); 
            pushFollow(FOLLOW_ruleReferenceTail_in_rule__ReferenceTail__TailAssignment_24649);
            ruleReferenceTail();

            state._fsp--;

             after(grammarAccess.getReferenceTailAccess().getTailReferenceTailParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferenceTail__TailAssignment_2"


    // $ANTLR start "rule__BoolValue__TypeAssignment"
    // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2346:1: rule__BoolValue__TypeAssignment : ( ( rule__BoolValue__TypeAlternatives_0 ) ) ;
    public final void rule__BoolValue__TypeAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2350:1: ( ( ( rule__BoolValue__TypeAlternatives_0 ) ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2351:1: ( ( rule__BoolValue__TypeAlternatives_0 ) )
            {
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2351:1: ( ( rule__BoolValue__TypeAlternatives_0 ) )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2352:1: ( rule__BoolValue__TypeAlternatives_0 )
            {
             before(grammarAccess.getBoolValueAccess().getTypeAlternatives_0()); 
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2353:1: ( rule__BoolValue__TypeAlternatives_0 )
            // ../co.edu.javeriana.Cmctltemplate.ui/src-gen/co/edu/javeriana/ui/contentassist/antlr/internal/InternalCmctltemplate.g:2353:2: rule__BoolValue__TypeAlternatives_0
            {
            pushFollow(FOLLOW_rule__BoolValue__TypeAlternatives_0_in_rule__BoolValue__TypeAssignment4680);
            rule__BoolValue__TypeAlternatives_0();

            state._fsp--;


            }

             after(grammarAccess.getBoolValueAccess().getTypeAlternatives_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BoolValue__TypeAssignment"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleTemplate_in_entryRuleTemplate61 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTemplate68 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Template__Group__0_in_ruleTemplate94 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleImport_in_entryRuleImport121 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleImport128 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Import__Group__0_in_ruleImport154 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTemplateFunction_in_entryRuleTemplateFunction181 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTemplateFunction188 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TemplateFunction__Group__0_in_ruleTemplateFunction214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStatement_in_entryRuleStatement241 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleStatement248 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Statement__Alternatives_in_ruleStatement274 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTemplateStatement_in_entryRuleTemplateStatement301 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTemplateStatement308 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TemplateStatement__Alternatives_in_ruleTemplateStatement334 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIfTemplate_in_entryRuleIfTemplate361 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIfTemplate368 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IfTemplate__Group__0_in_ruleIfTemplate394 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleForTemplate_in_entryRuleForTemplate421 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleForTemplate428 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ForTemplate__Group__0_in_ruleForTemplate454 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTemplateReference_in_entryRuleTemplateReference481 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTemplateReference488 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TemplateReference__Group__0_in_ruleTemplateReference514 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleETypedElement_in_entryRuleETypedElement543 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleETypedElement550 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleParameter_in_ruleETypedElement576 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleParameter_in_entryRuleParameter602 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleParameter609 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Parameter__Group__0_in_ruleParameter635 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleReference_in_entryRuleReference662 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleReference669 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Reference__Group__0_in_ruleReference695 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleReferenceTail_in_entryRuleReferenceTail722 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleReferenceTail729 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReferenceTail__Group__0_in_ruleReferenceTail755 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBoolValue_in_entryRuleBoolValue782 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBoolValue789 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BoolValue__TypeAssignment_in_ruleBoolValue815 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedNameWithWildcard_in_entryRuleQualifiedNameWithWildcard842 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQualifiedNameWithWildcard849 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedNameWithWildcard__Group__0_in_ruleQualifiedNameWithWildcard875 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQUALIFIED_NAME_in_entryRuleQUALIFIED_NAME902 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQUALIFIED_NAME909 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QUALIFIED_NAME__Group__0_in_ruleQUALIFIED_NAME935 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Statement__Group_0__0_in_rule__Statement__Alternatives971 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Statement__Alternatives989 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTemplateStatement_in_rule__Statement__Alternatives1006 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTemplateReference_in_rule__Statement__Alternatives1023 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIfTemplate_in_rule__TemplateStatement__Alternatives1055 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleForTemplate_in_rule__TemplateStatement__Alternatives1072 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__BoolValue__TypeAlternatives_01105 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__BoolValue__TypeAlternatives_01125 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Template__Group__0__Impl_in_rule__Template__Group__01157 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_rule__Template__Group__1_in_rule__Template__Group__01160 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Template__ImportsAssignment_0_in_rule__Template__Group__0__Impl1187 = new BitSet(new long[]{0x0000000000004002L});
    public static final BitSet FOLLOW_rule__Template__Group__1__Impl_in_rule__Template__Group__11218 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Template__TemplateFunctionsAssignment_1_in_rule__Template__Group__1__Impl1245 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_rule__Import__Group__0__Impl_in_rule__Import__Group__01280 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Import__Group__1_in_rule__Import__Group__01283 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rule__Import__Group__0__Impl1311 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Import__Group__1__Impl_in_rule__Import__Group__11342 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_rule__Import__Group__2_in_rule__Import__Group__11345 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Import__ImportedNamespaceAssignment_1_in_rule__Import__Group__1__Impl1372 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Import__Group__2__Impl_in_rule__Import__Group__21402 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__Import__Group__2__Impl1430 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TemplateFunction__Group__0__Impl_in_rule__TemplateFunction__Group__01467 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__TemplateFunction__Group__1_in_rule__TemplateFunction__Group__01470 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__TemplateFunction__Group__0__Impl1498 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TemplateFunction__Group__1__Impl_in_rule__TemplateFunction__Group__11529 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_rule__TemplateFunction__Group__2_in_rule__TemplateFunction__Group__11532 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TemplateFunction__NameAssignment_1_in_rule__TemplateFunction__Group__1__Impl1559 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TemplateFunction__Group__2__Impl_in_rule__TemplateFunction__Group__21589 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__TemplateFunction__Group__3_in_rule__TemplateFunction__Group__21592 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__TemplateFunction__Group__2__Impl1620 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TemplateFunction__Group__3__Impl_in_rule__TemplateFunction__Group__31651 = new BitSet(new long[]{0x0000000000240000L});
    public static final BitSet FOLLOW_rule__TemplateFunction__Group__4_in_rule__TemplateFunction__Group__31654 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TemplateFunction__ParametersAssignment_3_in_rule__TemplateFunction__Group__3__Impl1681 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TemplateFunction__Group__4__Impl_in_rule__TemplateFunction__Group__41711 = new BitSet(new long[]{0x0000000000240000L});
    public static final BitSet FOLLOW_rule__TemplateFunction__Group__5_in_rule__TemplateFunction__Group__41714 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TemplateFunction__Group_4__0_in_rule__TemplateFunction__Group__4__Impl1741 = new BitSet(new long[]{0x0000000000200002L});
    public static final BitSet FOLLOW_rule__TemplateFunction__Group__5__Impl_in_rule__TemplateFunction__Group__51772 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_rule__TemplateFunction__Group__6_in_rule__TemplateFunction__Group__51775 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__TemplateFunction__Group__5__Impl1803 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TemplateFunction__Group__6__Impl_in_rule__TemplateFunction__Group__61834 = new BitSet(new long[]{0x0000000000500030L});
    public static final BitSet FOLLOW_rule__TemplateFunction__Group__7_in_rule__TemplateFunction__Group__61837 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__TemplateFunction__Group__6__Impl1865 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TemplateFunction__Group__7__Impl_in_rule__TemplateFunction__Group__71896 = new BitSet(new long[]{0x0000000000500030L});
    public static final BitSet FOLLOW_rule__TemplateFunction__Group__8_in_rule__TemplateFunction__Group__71899 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TemplateFunction__StatementsAssignment_7_in_rule__TemplateFunction__Group__7__Impl1926 = new BitSet(new long[]{0x0000000000400032L});
    public static final BitSet FOLLOW_rule__TemplateFunction__Group__8__Impl_in_rule__TemplateFunction__Group__81957 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__TemplateFunction__Group__8__Impl1985 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TemplateFunction__Group_4__0__Impl_in_rule__TemplateFunction__Group_4__02034 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__TemplateFunction__Group_4__1_in_rule__TemplateFunction__Group_4__02037 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__TemplateFunction__Group_4__0__Impl2065 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TemplateFunction__Group_4__1__Impl_in_rule__TemplateFunction__Group_4__12096 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TemplateFunction__ParametersAssignment_4_1_in_rule__TemplateFunction__Group_4__1__Impl2123 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Statement__Group_0__0__Impl_in_rule__Statement__Group_0__02157 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__Statement__Group_0__1_in_rule__Statement__Group_0__02160 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Statement__Group_0__1__Impl_in_rule__Statement__Group_0__12218 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_CHARACTER_in_rule__Statement__Group_0__1__Impl2245 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IfTemplate__Group__0__Impl_in_rule__IfTemplate__Group__02278 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_rule__IfTemplate__Group__1_in_rule__IfTemplate__Group__02281 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IfTemplate__Group__1__Impl_in_rule__IfTemplate__Group__12339 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_rule__IfTemplate__Group__2_in_rule__IfTemplate__Group__12342 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rule__IfTemplate__Group__1__Impl2370 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IfTemplate__Group__2__Impl_in_rule__IfTemplate__Group__22401 = new BitSet(new long[]{0x0000000000003000L});
    public static final BitSet FOLLOW_rule__IfTemplate__Group__3_in_rule__IfTemplate__Group__22404 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_rule__IfTemplate__Group__2__Impl2432 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IfTemplate__Group__3__Impl_in_rule__IfTemplate__Group__32463 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_rule__IfTemplate__Group__4_in_rule__IfTemplate__Group__32466 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IfTemplate__TemplateConditionAssignment_3_in_rule__IfTemplate__Group__3__Impl2493 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IfTemplate__Group__4__Impl_in_rule__IfTemplate__Group__42523 = new BitSet(new long[]{0x0000000001400030L});
    public static final BitSet FOLLOW_rule__IfTemplate__Group__5_in_rule__IfTemplate__Group__42526 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__IfTemplate__Group__4__Impl2554 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IfTemplate__Group__5__Impl_in_rule__IfTemplate__Group__52585 = new BitSet(new long[]{0x0000000001400030L});
    public static final BitSet FOLLOW_rule__IfTemplate__Group__6_in_rule__IfTemplate__Group__52588 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IfTemplate__StatementsAssignment_5_in_rule__IfTemplate__Group__5__Impl2615 = new BitSet(new long[]{0x0000000000400032L});
    public static final BitSet FOLLOW_rule__IfTemplate__Group__6__Impl_in_rule__IfTemplate__Group__62646 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_rule__IfTemplate__Group__6__Impl2674 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ForTemplate__Group__0__Impl_in_rule__ForTemplate__Group__02719 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_rule__ForTemplate__Group__1_in_rule__ForTemplate__Group__02722 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ForTemplate__Group__1__Impl_in_rule__ForTemplate__Group__12780 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_rule__ForTemplate__Group__2_in_rule__ForTemplate__Group__12783 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rule__ForTemplate__Group__1__Impl2811 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ForTemplate__Group__2__Impl_in_rule__ForTemplate__Group__22842 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_rule__ForTemplate__Group__3_in_rule__ForTemplate__Group__22845 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_rule__ForTemplate__Group__2__Impl2873 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ForTemplate__Group__3__Impl_in_rule__ForTemplate__Group__32904 = new BitSet(new long[]{0x0000000004400030L});
    public static final BitSet FOLLOW_rule__ForTemplate__Group__4_in_rule__ForTemplate__Group__32907 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__ForTemplate__Group__3__Impl2935 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ForTemplate__Group__4__Impl_in_rule__ForTemplate__Group__42966 = new BitSet(new long[]{0x0000000004400030L});
    public static final BitSet FOLLOW_rule__ForTemplate__Group__5_in_rule__ForTemplate__Group__42969 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ForTemplate__StatementsAssignment_4_in_rule__ForTemplate__Group__4__Impl2996 = new BitSet(new long[]{0x0000000000400032L});
    public static final BitSet FOLLOW_rule__ForTemplate__Group__5__Impl_in_rule__ForTemplate__Group__53027 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_rule__ForTemplate__Group__5__Impl3055 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TemplateReference__Group__0__Impl_in_rule__TemplateReference__Group__03098 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__TemplateReference__Group__1_in_rule__TemplateReference__Group__03101 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TemplateReference__CallAssignment_0_in_rule__TemplateReference__Group__0__Impl3128 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TemplateReference__Group__1__Impl_in_rule__TemplateReference__Group__13158 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_rule__TemplateReference__Group__2_in_rule__TemplateReference__Group__13161 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TemplateReference__TypeAssignment_1_in_rule__TemplateReference__Group__1__Impl3188 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TemplateReference__Group__2__Impl_in_rule__TemplateReference__Group__23218 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__TemplateReference__Group__2__Impl3246 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Parameter__Group__0__Impl_in_rule__Parameter__Group__03283 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Parameter__Group__1_in_rule__Parameter__Group__03286 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Parameter__ETypeAssignment_0_in_rule__Parameter__Group__0__Impl3313 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Parameter__Group__1__Impl_in_rule__Parameter__Group__13343 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Parameter__NameAssignment_1_in_rule__Parameter__Group__1__Impl3370 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Reference__Group__0__Impl_in_rule__Reference__Group__03404 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_rule__Reference__Group__1_in_rule__Reference__Group__03407 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Reference__ReferencedElementAssignment_0_in_rule__Reference__Group__0__Impl3434 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Reference__Group__1__Impl_in_rule__Reference__Group__13464 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Reference__TailAssignment_1_in_rule__Reference__Group__1__Impl3491 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReferenceTail__Group__0__Impl_in_rule__ReferenceTail__Group__03526 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__ReferenceTail__Group__1_in_rule__ReferenceTail__Group__03529 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_rule__ReferenceTail__Group__0__Impl3557 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReferenceTail__Group__1__Impl_in_rule__ReferenceTail__Group__13588 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_rule__ReferenceTail__Group__2_in_rule__ReferenceTail__Group__13591 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReferenceTail__ReferencedElementAssignment_1_in_rule__ReferenceTail__Group__1__Impl3618 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReferenceTail__Group__2__Impl_in_rule__ReferenceTail__Group__23648 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReferenceTail__TailAssignment_2_in_rule__ReferenceTail__Group__2__Impl3675 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedNameWithWildcard__Group__0__Impl_in_rule__QualifiedNameWithWildcard__Group__03712 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_rule__QualifiedNameWithWildcard__Group__1_in_rule__QualifiedNameWithWildcard__Group__03715 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQUALIFIED_NAME_in_rule__QualifiedNameWithWildcard__Group__0__Impl3742 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedNameWithWildcard__Group__1__Impl_in_rule__QualifiedNameWithWildcard__Group__13771 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_rule__QualifiedNameWithWildcard__Group__1__Impl3800 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QUALIFIED_NAME__Group__0__Impl_in_rule__QUALIFIED_NAME__Group__03837 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_rule__QUALIFIED_NAME__Group__1_in_rule__QUALIFIED_NAME__Group__03840 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__QUALIFIED_NAME__Group__0__Impl3867 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QUALIFIED_NAME__Group__1__Impl_in_rule__QUALIFIED_NAME__Group__13896 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QUALIFIED_NAME__Group_1__0_in_rule__QUALIFIED_NAME__Group__1__Impl3923 = new BitSet(new long[]{0x0000000008000002L});
    public static final BitSet FOLLOW_rule__QUALIFIED_NAME__Group_1__0__Impl_in_rule__QUALIFIED_NAME__Group_1__03958 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__QUALIFIED_NAME__Group_1__1_in_rule__QUALIFIED_NAME__Group_1__03961 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_rule__QUALIFIED_NAME__Group_1__0__Impl3989 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QUALIFIED_NAME__Group_1__1__Impl_in_rule__QUALIFIED_NAME__Group_1__14020 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__QUALIFIED_NAME__Group_1__1__Impl4047 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleImport_in_rule__Template__ImportsAssignment_04085 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTemplateFunction_in_rule__Template__TemplateFunctionsAssignment_14116 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedNameWithWildcard_in_rule__Import__ImportedNamespaceAssignment_14147 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__TemplateFunction__NameAssignment_14178 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleParameter_in_rule__TemplateFunction__ParametersAssignment_34209 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleParameter_in_rule__TemplateFunction__ParametersAssignment_4_14240 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStatement_in_rule__TemplateFunction__StatementsAssignment_74271 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBoolValue_in_rule__IfTemplate__TemplateConditionAssignment_34302 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStatement_in_rule__IfTemplate__StatementsAssignment_54333 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStatement_in_rule__ForTemplate__StatementsAssignment_44364 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rule__TemplateReference__CallAssignment_04400 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleReference_in_rule__TemplateReference__TypeAssignment_14439 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQUALIFIED_NAME_in_rule__Parameter__ETypeAssignment_04474 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Parameter__NameAssignment_14509 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Reference__ReferencedElementAssignment_04544 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleReferenceTail_in_rule__Reference__TailAssignment_14579 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__ReferenceTail__ReferencedElementAssignment_14614 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleReferenceTail_in_rule__ReferenceTail__TailAssignment_24649 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BoolValue__TypeAlternatives_0_in_rule__BoolValue__TypeAssignment4680 = new BitSet(new long[]{0x0000000000000002L});

}