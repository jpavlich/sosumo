/*
 * generated by Xtext
 */
package co.edu.javeriana.ui;

import org.eclipse.ui.plugin.AbstractUIPlugin;

/**
 * Use this class to register components to be used within the IDE.
 */
public class CmctljavaUiModule extends co.edu.javeriana.ui.AbstractCmctljavaUiModule {
	public CmctljavaUiModule(AbstractUIPlugin plugin) {
		super(plugin);
	}
}
